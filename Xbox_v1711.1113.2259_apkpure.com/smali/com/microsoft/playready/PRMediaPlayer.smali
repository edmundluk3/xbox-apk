.class public Lcom/microsoft/playready/PRMediaPlayer;
.super Landroid/media/MediaPlayer;
.source "PRMediaPlayer.java"

# interfaces
.implements Lcom/microsoft/playready/IExtensibleLicenseAcquirer;


# static fields
.field private static final TAG:Ljava/lang/String; = "PRMediaPlayer"

.field private static final c_HlsRootUrl:Ljava/lang/String; = "http://localhost"

.field private static final c_PlaylistFileName:Ljava/lang/String; = "/playlist.m3u8"


# instance fields
.field private mContentPath:Ljava/lang/String;

.field private mLegacyOnBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

.field private mLegacyOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mLegacyOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mLegacyOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

.field private mLegacyOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private mLegacyOnSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

.field private mLegacyOnVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

.field private mLicenseAcquirer:Lcom/microsoft/playready/IReactiveLicenseAcquirer;

.field private mMediaProxy:Lcom/microsoft/playready/MediaProxy;

.field private mOnBufferingUpdateListenerLockObj:Ljava/lang/Object;

.field private mOnBufferingUpdateListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/media/MediaPlayer$OnBufferingUpdateListener;",
            ">;"
        }
    .end annotation
.end field

.field private mOnCompletionListenerLockObj:Ljava/lang/Object;

.field private mOnCompletionListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/media/MediaPlayer$OnCompletionListener;",
            ">;"
        }
    .end annotation
.end field

.field private mOnErrorListenerLockObj:Ljava/lang/Object;

.field private mOnErrorListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/media/MediaPlayer$OnErrorListener;",
            ">;"
        }
    .end annotation
.end field

.field private mOnInfoListenerLockObj:Ljava/lang/Object;

.field private mOnInfoListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/media/MediaPlayer$OnInfoListener;",
            ">;"
        }
    .end annotation
.end field

.field private mOnPreparedListenerLockObj:Ljava/lang/Object;

.field private mOnPreparedListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/media/MediaPlayer$OnPreparedListener;",
            ">;"
        }
    .end annotation
.end field

.field private mOnSeekCompleteListenerLockObj:Ljava/lang/Object;

.field private mOnSeekCompleteListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/media/MediaPlayer$OnSeekCompleteListener;",
            ">;"
        }
    .end annotation
.end field

.field private mOnVideoSizeChangedListenerLockObj:Ljava/lang/Object;

.field private mOnVideoSizeChangedListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/media/MediaPlayer$OnVideoSizeChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field private mPlaylistUri:Ljava/lang/String;

.field private mProxyRequestHandler:Lcom/microsoft/playready/MediaProxyRequestHandler;

.field private mRootUri:Ljava/lang/String;

.field private m_HttpServerPort:I


# direct methods
.method protected constructor <init>(Lcom/microsoft/playready/PlayReadyFactory;)V
    .locals 2
    .param p1, "prFactory"    # Lcom/microsoft/playready/PlayReadyFactory;

    .prologue
    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Landroid/media/MediaPlayer;-><init>()V

    .line 31
    iput-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLicenseAcquirer:Lcom/microsoft/playready/IReactiveLicenseAcquirer;

    .line 33
    iput-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    .line 34
    iput-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mProxyRequestHandler:Lcom/microsoft/playready/MediaProxyRequestHandler;

    .line 36
    iput-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mContentPath:Ljava/lang/String;

    .line 37
    iput-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mRootUri:Ljava/lang/String;

    .line 38
    iput-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mPlaylistUri:Ljava/lang/String;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnPreparedListeners:Ljava/util/ArrayList;

    .line 50
    iput-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 51
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnPreparedListenerLockObj:Ljava/lang/Object;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnCompletionListeners:Ljava/util/ArrayList;

    .line 54
    iput-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 55
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnCompletionListenerLockObj:Ljava/lang/Object;

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnBufferingUpdateListeners:Ljava/util/ArrayList;

    .line 58
    iput-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    .line 59
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnBufferingUpdateListenerLockObj:Ljava/lang/Object;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnSeekCompleteListeners:Ljava/util/ArrayList;

    .line 62
    iput-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    .line 63
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnSeekCompleteListenerLockObj:Ljava/lang/Object;

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnVideoSizeChangedListeners:Ljava/util/ArrayList;

    .line 66
    iput-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    .line 67
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnVideoSizeChangedListenerLockObj:Ljava/lang/Object;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnInfoListeners:Ljava/util/ArrayList;

    .line 70
    iput-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    .line 71
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnInfoListenerLockObj:Ljava/lang/Object;

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnErrorListeners:Ljava/util/ArrayList;

    .line 74
    iput-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 75
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnErrorListenerLockObj:Ljava/lang/Object;

    .line 83
    invoke-static {}, Lcom/microsoft/playready/LicenseAcquirer;->createReactiveLicenseAcquirer()Lcom/microsoft/playready/IReactiveLicenseAcquirer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLicenseAcquirer:Lcom/microsoft/playready/IReactiveLicenseAcquirer;

    .line 85
    invoke-static {}, Lcom/microsoft/playready/DrmConfig;->getMediaProxyPort()I

    move-result v0

    iput v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->m_HttpServerPort:I

    .line 93
    new-instance v0, Lcom/microsoft/playready/PRMediaPlayer$1;

    invoke-direct {v0, p0}, Lcom/microsoft/playready/PRMediaPlayer$1;-><init>(Lcom/microsoft/playready/PRMediaPlayer;)V

    invoke-super {p0, v0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 100
    new-instance v0, Lcom/microsoft/playready/PRMediaPlayer$2;

    invoke-direct {v0, p0}, Lcom/microsoft/playready/PRMediaPlayer$2;-><init>(Lcom/microsoft/playready/PRMediaPlayer;)V

    invoke-super {p0, v0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 107
    new-instance v0, Lcom/microsoft/playready/PRMediaPlayer$3;

    invoke-direct {v0, p0}, Lcom/microsoft/playready/PRMediaPlayer$3;-><init>(Lcom/microsoft/playready/PRMediaPlayer;)V

    invoke-super {p0, v0}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    .line 114
    new-instance v0, Lcom/microsoft/playready/PRMediaPlayer$4;

    invoke-direct {v0, p0}, Lcom/microsoft/playready/PRMediaPlayer$4;-><init>(Lcom/microsoft/playready/PRMediaPlayer;)V

    invoke-super {p0, v0}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 121
    new-instance v0, Lcom/microsoft/playready/PRMediaPlayer$5;

    invoke-direct {v0, p0}, Lcom/microsoft/playready/PRMediaPlayer$5;-><init>(Lcom/microsoft/playready/PRMediaPlayer;)V

    invoke-super {p0, v0}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 128
    new-instance v0, Lcom/microsoft/playready/PRMediaPlayer$6;

    invoke-direct {v0, p0}, Lcom/microsoft/playready/PRMediaPlayer$6;-><init>(Lcom/microsoft/playready/PRMediaPlayer;)V

    invoke-super {p0, v0}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 135
    new-instance v0, Lcom/microsoft/playready/PRMediaPlayer$7;

    invoke-direct {v0, p0}, Lcom/microsoft/playready/PRMediaPlayer$7;-><init>(Lcom/microsoft/playready/PRMediaPlayer;)V

    invoke-super {p0, v0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 141
    return-void
.end method

.method static synthetic access$0(Lcom/microsoft/playready/PRMediaPlayer;)Lcom/microsoft/playready/IReactiveLicenseAcquirer;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLicenseAcquirer:Lcom/microsoft/playready/IReactiveLicenseAcquirer;

    return-object v0
.end method

.method public static createPRMediaPlayer(Lcom/microsoft/playready/PlayReadyFactory;)Lcom/microsoft/playready/PRMediaPlayer;
    .locals 1
    .param p0, "prFactory"    # Lcom/microsoft/playready/PlayReadyFactory;

    .prologue
    .line 78
    new-instance v0, Lcom/microsoft/playready/PRMediaPlayer;

    invoke-direct {v0, p0}, Lcom/microsoft/playready/PRMediaPlayer;-><init>(Lcom/microsoft/playready/PlayReadyFactory;)V

    return-object v0
.end method

.method private shutdownProxy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 236
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mProxyRequestHandler:Lcom/microsoft/playready/MediaProxyRequestHandler;

    if-eqz v1, :cond_0

    .line 238
    :try_start_0
    iget v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->m_HttpServerPort:I

    invoke-static {v1}, Lcom/microsoft/playready/HttpLocalServer;->getInstance(I)Lcom/microsoft/playready/HttpLocalServer;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/playready/PRMediaPlayer;->mProxyRequestHandler:Lcom/microsoft/playready/MediaProxyRequestHandler;

    invoke-virtual {v1, v2}, Lcom/microsoft/playready/HttpLocalServer;->unregisterMediaProxy(Lcom/microsoft/playready/MediaProxyRequestHandler;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 244
    :cond_0
    :goto_0
    iput-object v3, p0, Lcom/microsoft/playready/PRMediaPlayer;->mProxyRequestHandler:Lcom/microsoft/playready/MediaProxyRequestHandler;

    .line 246
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    if-eqz v1, :cond_1

    .line 247
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    invoke-virtual {v1}, Lcom/microsoft/playready/MediaProxy;->stop()V

    .line 249
    :cond_1
    iput-object v3, p0, Lcom/microsoft/playready/PRMediaPlayer;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    .line 250
    return-void

    .line 239
    :catch_0
    move-exception v0

    .line 240
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private startProxy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "contentPath"    # Ljava/lang/String;
    .param p2, "RootUri"    # Ljava/lang/String;
    .param p3, "PlaylistUri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    invoke-direct {p0}, Lcom/microsoft/playready/PRMediaPlayer;->shutdownProxy()V

    .line 177
    new-instance v3, Lcom/microsoft/playready/MediaProxy;

    invoke-direct {v3}, Lcom/microsoft/playready/MediaProxy;-><init>()V

    iput-object v3, p0, Lcom/microsoft/playready/PRMediaPlayer;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    .line 182
    iget-object v3, p0, Lcom/microsoft/playready/PRMediaPlayer;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    new-instance v4, Lcom/microsoft/playready/PRMediaPlayer$8;

    invoke-direct {v4, p0}, Lcom/microsoft/playready/PRMediaPlayer$8;-><init>(Lcom/microsoft/playready/PRMediaPlayer;)V

    invoke-virtual {v3, v4}, Lcom/microsoft/playready/MediaProxy;->setReactiveLicenseAcquisitionHandler(Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionHandler;)V

    .line 207
    const/4 v1, 0x2

    .line 208
    .local v1, "segmentLength":I
    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string/jumbo v4, "samsung"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    .line 209
    const-string v3, "PRMediaPlayer"

    const-string v4, "Enabling Samsung segment length hack"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    const/4 v1, 0x3

    .line 214
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/playready/PRMediaPlayer;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    int-to-long v4, v1

    invoke-virtual {v3, p1, v4, v5}, Lcom/microsoft/playready/MediaProxy;->setContent(Ljava/lang/String;J)V
    :try_end_0
    .catch Lcom/microsoft/playready/DrmException; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    :goto_0
    iget v3, p0, Lcom/microsoft/playready/PRMediaPlayer;->m_HttpServerPort:I

    invoke-static {v3}, Lcom/microsoft/playready/HttpLocalServer;->getInstance(I)Lcom/microsoft/playready/HttpLocalServer;

    move-result-object v2

    .line 221
    .local v2, "server":Lcom/microsoft/playready/HttpLocalServer;
    if-eqz v2, :cond_1

    .line 223
    :try_start_1
    new-instance v3, Lcom/microsoft/playready/MediaProxyRequestHandler;

    iget-object v4, p0, Lcom/microsoft/playready/PRMediaPlayer;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    invoke-direct {v3, p2, p3, v4}, Lcom/microsoft/playready/MediaProxyRequestHandler;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/playready/MediaProxy;)V

    iput-object v3, p0, Lcom/microsoft/playready/PRMediaPlayer;->mProxyRequestHandler:Lcom/microsoft/playready/MediaProxyRequestHandler;
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_1

    .line 228
    iget-object v3, p0, Lcom/microsoft/playready/PRMediaPlayer;->mProxyRequestHandler:Lcom/microsoft/playready/MediaProxyRequestHandler;

    invoke-virtual {v2, v3}, Lcom/microsoft/playready/HttpLocalServer;->registerMediaProxy(Lcom/microsoft/playready/MediaProxyRequestHandler;)V

    .line 229
    invoke-virtual {v2}, Lcom/microsoft/playready/HttpLocalServer;->unpause()V

    .line 233
    return-void

    .line 215
    .end local v2    # "server":Lcom/microsoft/playready/HttpLocalServer;
    :catch_0
    move-exception v0

    .line 216
    .local v0, "e":Lcom/microsoft/playready/DrmException;
    const-string v3, "PRMediaPlayer"

    invoke-virtual {v0}, Lcom/microsoft/playready/DrmException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/microsoft/playready/DrmException;->getErrorCode()I

    move-result v4

    invoke-virtual {p0, p0, v3, v4}, Lcom/microsoft/playready/PRMediaPlayer;->notifyErrorListeners(Landroid/media/MediaPlayer;II)Z

    goto :goto_0

    .line 224
    .end local v0    # "e":Lcom/microsoft/playready/DrmException;
    .restart local v2    # "server":Lcom/microsoft/playready/HttpLocalServer;
    :catch_1
    move-exception v0

    .line 225
    .local v0, "e":Ljava/net/URISyntaxException;
    const-string v3, "PRMediaPlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Invalid URI while registering media proxy: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 226
    new-instance v3, Ljava/io/IOException;

    invoke-direct {v3, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 231
    .end local v0    # "e":Ljava/net/URISyntaxException;
    :cond_1
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Failed to get HTTP Local Server instance!"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
.end method


# virtual methods
.method public addLicenseAcquirerListener(Lcom/microsoft/playready/ILicenseAcquirerListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/playready/ILicenseAcquirerListener;

    .prologue
    .line 384
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLicenseAcquirer:Lcom/microsoft/playready/IReactiveLicenseAcquirer;

    invoke-interface {v0, p1}, Lcom/microsoft/playready/IReactiveLicenseAcquirer;->addLicenseAcquirerListener(Lcom/microsoft/playready/ILicenseAcquirerListener;)V

    .line 385
    return-void
.end method

.method public addOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    .prologue
    .line 497
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnBufferingUpdateListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 498
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnBufferingUpdateListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 497
    monitor-exit v1

    .line 500
    return-void

    .line 497
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnCompletionListener;

    .prologue
    .line 448
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnCompletionListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 449
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnCompletionListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 448
    monitor-exit v1

    .line 451
    return-void

    .line 448
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnErrorListener;

    .prologue
    .line 648
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnErrorListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 649
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnErrorListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 648
    monitor-exit v1

    .line 651
    return-void

    .line 648
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnInfoListener;

    .prologue
    .line 704
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnInfoListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 705
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnInfoListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 704
    monitor-exit v1

    .line 707
    return-void

    .line 704
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnPreparedListener;

    .prologue
    .line 398
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnPreparedListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 399
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnPreparedListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 398
    monitor-exit v1

    .line 401
    return-void

    .line 398
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnSeekCompleteListener;

    .prologue
    .line 547
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnSeekCompleteListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 548
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnSeekCompleteListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 547
    monitor-exit v1

    .line 550
    return-void

    .line 547
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    .prologue
    .line 597
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnVideoSizeChangedListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 598
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnVideoSizeChangedListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 597
    monitor-exit v1

    .line 600
    return-void

    .line 597
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getAspectRatio()F
    .locals 1

    .prologue
    .line 757
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    invoke-virtual {v0}, Lcom/microsoft/playready/MediaProxy;->getAspectRatio()F

    move-result v0

    return v0
.end method

.method public getLicenseAcquisitionPlugin()Lcom/microsoft/playready/ILicenseAcquisitionPlugin;
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLicenseAcquirer:Lcom/microsoft/playready/IReactiveLicenseAcquirer;

    invoke-interface {v0}, Lcom/microsoft/playready/IReactiveLicenseAcquirer;->getLicenseAcquisitionPlugin()Lcom/microsoft/playready/ILicenseAcquisitionPlugin;

    move-result-object v0

    return-object v0
.end method

.method public getMediaDescription()Lcom/microsoft/playready/MediaDescription;
    .locals 1

    .prologue
    .line 753
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    invoke-virtual {v0}, Lcom/microsoft/playready/MediaProxy;->getMediaDescription()Lcom/microsoft/playready/MediaDescription;

    move-result-object v0

    return-object v0
.end method

.method protected notifyErrorListeners(Landroid/media/MediaPlayer;II)Z
    .locals 7
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    .line 673
    const/4 v1, 0x0

    .line 674
    .local v1, "handled":Z
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 676
    .local v3, "listeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/media/MediaPlayer$OnErrorListener;>;"
    iget-object v5, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnErrorListenerLockObj:Ljava/lang/Object;

    monitor-enter v5

    .line 677
    :try_start_0
    iget-object v4, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    if-eqz v4, :cond_0

    .line 678
    iget-object v4, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 680
    :cond_0
    iget-object v4, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnErrorListeners:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 676
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 683
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 694
    :goto_1
    return v1

    .line 676
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 683
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/MediaPlayer$OnErrorListener;

    .line 685
    .local v2, "listener":Landroid/media/MediaPlayer$OnErrorListener;
    :try_start_2
    invoke-interface {v2, p1, p2, p3}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result v5

    if-eqz v5, :cond_1

    .line 686
    const/4 v1, 0x1

    goto :goto_1

    .line 689
    :catch_0
    move-exception v0

    .line 690
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "PRMediaPlayer"

    const-string v6, "Uncaught exception thrown during OnErrorListener callback"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected notifyOnBufferingUpdateListeners(Landroid/media/MediaPlayer;I)V
    .locals 6
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "percent"    # I

    .prologue
    .line 522
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 524
    .local v2, "listeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/media/MediaPlayer$OnBufferingUpdateListener;>;"
    iget-object v4, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnBufferingUpdateListenerLockObj:Ljava/lang/Object;

    monitor-enter v4

    .line 525
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnBufferingUpdateListeners:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 526
    iget-object v3, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    if-eqz v3, :cond_0

    .line 527
    iget-object v3, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 524
    :cond_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 531
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 538
    return-void

    .line 524
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 531
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    .line 533
    .local v1, "listener":Landroid/media/MediaPlayer$OnBufferingUpdateListener;
    :try_start_2
    invoke-interface {v1, p1, p2}, Landroid/media/MediaPlayer$OnBufferingUpdateListener;->onBufferingUpdate(Landroid/media/MediaPlayer;I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 534
    :catch_0
    move-exception v0

    .line 535
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "PRMediaPlayer"

    const-string v5, "Uncaught exception thrown during OnBufferingUpdateListener callback"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected notifyOnCompletionListeners(Landroid/media/MediaPlayer;)V
    .locals 6
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 473
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 474
    .local v2, "listeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/media/MediaPlayer$OnCompletionListener;>;"
    iget-object v4, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnCompletionListenerLockObj:Ljava/lang/Object;

    monitor-enter v4

    .line 475
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnCompletionListeners:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 476
    iget-object v3, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    if-eqz v3, :cond_0

    .line 477
    iget-object v3, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 474
    :cond_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 481
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 488
    return-void

    .line 474
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 481
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/MediaPlayer$OnCompletionListener;

    .line 483
    .local v1, "listener":Landroid/media/MediaPlayer$OnCompletionListener;
    :try_start_2
    invoke-interface {v1, p1}, Landroid/media/MediaPlayer$OnCompletionListener;->onCompletion(Landroid/media/MediaPlayer;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 484
    :catch_0
    move-exception v0

    .line 485
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "PRMediaPlayer"

    const-string v5, "Uncaught exception thrown during OnCompletionListener callback"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected notifyOnInfoListeners(Landroid/media/MediaPlayer;II)Z
    .locals 7
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    .line 729
    const/4 v1, 0x0

    .line 730
    .local v1, "handled":Z
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 732
    .local v3, "listeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/media/MediaPlayer$OnInfoListener;>;"
    iget-object v5, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnInfoListenerLockObj:Ljava/lang/Object;

    monitor-enter v5

    .line 733
    :try_start_0
    iget-object v4, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnInfoListeners:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 734
    iget-object v4, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    if-eqz v4, :cond_0

    .line 735
    iget-object v4, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 732
    :cond_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 739
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 749
    return v1

    .line 732
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 739
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/MediaPlayer$OnInfoListener;

    .line 741
    .local v2, "listener":Landroid/media/MediaPlayer$OnInfoListener;
    :try_start_2
    invoke-interface {v2, p1, p2, p3}, Landroid/media/MediaPlayer$OnInfoListener;->onInfo(Landroid/media/MediaPlayer;II)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result v5

    if-eqz v5, :cond_1

    .line 742
    const/4 v1, 0x1

    goto :goto_0

    .line 744
    :catch_0
    move-exception v0

    .line 745
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "PRMediaPlayer"

    const-string v6, "Uncaught exception thrown during OnInfoListener callback"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected notifyOnPreparedListeners(Landroid/media/MediaPlayer;)V
    .locals 6
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 423
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 425
    .local v2, "listeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/media/MediaPlayer$OnPreparedListener;>;"
    iget-object v4, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnPreparedListenerLockObj:Ljava/lang/Object;

    monitor-enter v4

    .line 426
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnPreparedListeners:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 427
    iget-object v3, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    if-eqz v3, :cond_0

    .line 428
    iget-object v3, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 425
    :cond_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 432
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 439
    return-void

    .line 425
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 432
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/MediaPlayer$OnPreparedListener;

    .line 434
    .local v1, "listener":Landroid/media/MediaPlayer$OnPreparedListener;
    :try_start_2
    invoke-interface {v1, p1}, Landroid/media/MediaPlayer$OnPreparedListener;->onPrepared(Landroid/media/MediaPlayer;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 435
    :catch_0
    move-exception v0

    .line 436
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "PRMediaPlayer"

    const-string v5, "Uncaught exception thrown during OnPreparedListener callback"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected notifyOnSeekCompleteListeners(Landroid/media/MediaPlayer;)V
    .locals 6
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 572
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 574
    .local v2, "listeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/media/MediaPlayer$OnSeekCompleteListener;>;"
    iget-object v4, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnSeekCompleteListenerLockObj:Ljava/lang/Object;

    monitor-enter v4

    .line 575
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnSeekCompleteListeners:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 576
    iget-object v3, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    if-eqz v3, :cond_0

    .line 577
    iget-object v3, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 574
    :cond_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 581
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 588
    return-void

    .line 574
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 581
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/MediaPlayer$OnSeekCompleteListener;

    .line 583
    .local v1, "listener":Landroid/media/MediaPlayer$OnSeekCompleteListener;
    :try_start_2
    invoke-interface {v1, p1}, Landroid/media/MediaPlayer$OnSeekCompleteListener;->onSeekComplete(Landroid/media/MediaPlayer;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 584
    :catch_0
    move-exception v0

    .line 585
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "PRMediaPlayer"

    const-string v5, "Uncaught exception thrown during OnSeekCompleteListener callback"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected notifyOnVideoSizeChangedListeners(Landroid/media/MediaPlayer;II)V
    .locals 6
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 622
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 624
    .local v2, "listeners":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/media/MediaPlayer$OnVideoSizeChangedListener;>;"
    iget-object v4, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnVideoSizeChangedListenerLockObj:Ljava/lang/Object;

    monitor-enter v4

    .line 625
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnVideoSizeChangedListeners:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 626
    iget-object v3, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    if-eqz v3, :cond_0

    .line 627
    iget-object v3, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 624
    :cond_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 631
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 639
    return-void

    .line 624
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 631
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    .line 633
    .local v1, "listener":Landroid/media/MediaPlayer$OnVideoSizeChangedListener;
    :try_start_2
    invoke-interface {v1, p1, p2, p3}, Landroid/media/MediaPlayer$OnVideoSizeChangedListener;->onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 634
    :catch_0
    move-exception v0

    .line 635
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "PRMediaPlayer"

    const-string v5, "Uncaught exception thrown during OnVideoSizeChanged callback"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public pause()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 260
    invoke-super {p0}, Landroid/media/MediaPlayer;->pause()V

    .line 263
    :try_start_0
    iget v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->m_HttpServerPort:I

    invoke-static {v1}, Lcom/microsoft/playready/HttpLocalServer;->getInstance(I)Lcom/microsoft/playready/HttpLocalServer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/playready/HttpLocalServer;->pause()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 267
    :goto_0
    return-void

    .line 264
    :catch_0
    move-exception v0

    .line 265
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public pauseForShutdown()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 255
    invoke-super {p0}, Landroid/media/MediaPlayer;->pause()V

    .line 256
    return-void
.end method

.method public prepare()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 159
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mContentPath:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mContentPath:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mRootUri:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mRootUri:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mPlaylistUri:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mPlaylistUri:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 160
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "setDataSource(string) must be called before calls to prepare()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mContentPath:Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mRootUri:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/playready/PRMediaPlayer;->mPlaylistUri:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/microsoft/playready/PRMediaPlayer;->startProxy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    invoke-super {p0}, Landroid/media/MediaPlayer;->reset()V

    .line 165
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mPlaylistUri:Ljava/lang/String;

    invoke-super {p0, v0}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 166
    invoke-super {p0}, Landroid/media/MediaPlayer;->prepare()V

    .line 167
    return-void
.end method

.method public prepareAsync()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 172
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "PRMediaPlayer only supports synchronous Prepare"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public release()V
    .locals 3

    .prologue
    .line 288
    invoke-direct {p0}, Lcom/microsoft/playready/PRMediaPlayer;->shutdownProxy()V

    .line 290
    iget-object v2, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnPreparedListenerLockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 291
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnPreparedListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 292
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 290
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294
    iget-object v2, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnBufferingUpdateListenerLockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 295
    :try_start_1
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnBufferingUpdateListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 296
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    .line 294
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 298
    iget-object v2, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnCompletionListenerLockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 299
    :try_start_2
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnCompletionListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 300
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 298
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 302
    iget-object v2, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnSeekCompleteListenerLockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 303
    :try_start_3
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnSeekCompleteListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 304
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    .line 302
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 306
    iget-object v2, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnErrorListenerLockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 307
    :try_start_4
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnErrorListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 308
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 306
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 310
    iget-object v2, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnInfoListenerLockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 311
    :try_start_5
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnInfoListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 312
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    .line 310
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 314
    iget-object v2, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnVideoSizeChangedListenerLockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 315
    :try_start_6
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnVideoSizeChangedListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 316
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    .line 314
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    .line 322
    :try_start_7
    iget v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->m_HttpServerPort:I

    invoke-static {v1}, Lcom/microsoft/playready/HttpLocalServer;->getInstance(I)Lcom/microsoft/playready/HttpLocalServer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/playready/HttpLocalServer;->unpause()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0

    .line 327
    :goto_0
    invoke-super {p0}, Landroid/media/MediaPlayer;->release()V

    .line 329
    return-void

    .line 290
    :catchall_0
    move-exception v1

    :try_start_8
    monitor-exit v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    throw v1

    .line 294
    :catchall_1
    move-exception v1

    :try_start_9
    monitor-exit v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    throw v1

    .line 298
    :catchall_2
    move-exception v1

    :try_start_a
    monitor-exit v2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    throw v1

    .line 302
    :catchall_3
    move-exception v1

    :try_start_b
    monitor-exit v2
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    throw v1

    .line 306
    :catchall_4
    move-exception v1

    :try_start_c
    monitor-exit v2
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    throw v1

    .line 310
    :catchall_5
    move-exception v1

    :try_start_d
    monitor-exit v2
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_5

    throw v1

    .line 314
    :catchall_6
    move-exception v1

    :try_start_e
    monitor-exit v2
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_6

    throw v1

    .line 323
    :catch_0
    move-exception v0

    .line 324
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public removeLicenseAcquirerListener(Lcom/microsoft/playready/ILicenseAcquirerListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/playready/ILicenseAcquirerListener;

    .prologue
    .line 388
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLicenseAcquirer:Lcom/microsoft/playready/IReactiveLicenseAcquirer;

    invoke-interface {v0, p1}, Lcom/microsoft/playready/IReactiveLicenseAcquirer;->removeLicenseAcquirerListener(Lcom/microsoft/playready/ILicenseAcquirerListener;)V

    .line 389
    return-void
.end method

.method public removeOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    .prologue
    .line 509
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnBufferingUpdateListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 510
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnBufferingUpdateListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 509
    monitor-exit v1

    .line 512
    return-void

    .line 509
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnCompletionListener;

    .prologue
    .line 460
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnCompletionListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 461
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnCompletionListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 460
    monitor-exit v1

    .line 463
    return-void

    .line 460
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnErrorListener;

    .prologue
    .line 660
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnErrorListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 661
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnErrorListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 660
    monitor-exit v1

    .line 663
    return-void

    .line 660
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnInfoListener;

    .prologue
    .line 716
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnInfoListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 717
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnInfoListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 716
    monitor-exit v1

    .line 719
    return-void

    .line 716
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnPreparedListener;

    .prologue
    .line 410
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnPreparedListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 411
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnPreparedListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 410
    monitor-exit v1

    .line 413
    return-void

    .line 410
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnSeekCompleteListener;

    .prologue
    .line 559
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnSeekCompleteListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 560
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnSeekCompleteListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 559
    monitor-exit v1

    .line 562
    return-void

    .line 559
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    .prologue
    .line 609
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnVideoSizeChangedListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 610
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnVideoSizeChangedListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 609
    monitor-exit v1

    .line 612
    return-void

    .line 609
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public reset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 336
    :try_start_0
    iget v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->m_HttpServerPort:I

    invoke-static {v1}, Lcom/microsoft/playready/HttpLocalServer;->getInstance(I)Lcom/microsoft/playready/HttpLocalServer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/playready/HttpLocalServer;->unpause()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 342
    :goto_0
    invoke-super {p0}, Landroid/media/MediaPlayer;->reset()V

    .line 343
    invoke-direct {p0}, Lcom/microsoft/playready/PRMediaPlayer;->shutdownProxy()V

    .line 344
    iput-object v2, p0, Lcom/microsoft/playready/PRMediaPlayer;->mContentPath:Ljava/lang/String;

    .line 345
    iput-object v2, p0, Lcom/microsoft/playready/PRMediaPlayer;->mRootUri:Ljava/lang/String;

    .line 346
    iput-object v2, p0, Lcom/microsoft/playready/PRMediaPlayer;->mPlaylistUri:Ljava/lang/String;

    .line 347
    return-void

    .line 337
    :catch_0
    move-exception v0

    .line 338
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 358
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "setDataSource(String path) is the only supported overload for the PRMediaPlayer"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 352
    .local p3, "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "setDataSource(String path) is the only supported overload for the PRMediaPlayer"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setDataSource(Ljava/io/FileDescriptor;)V
    .locals 2
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 370
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "setDataSource(String path) is the only supported overload for the PRMediaPlayer"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setDataSource(Ljava/io/FileDescriptor;JJ)V
    .locals 2
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "offset"    # J
    .param p4, "length"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 364
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "setDataSource(String path) is the only supported overload for the PRMediaPlayer"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setDataSource(Ljava/lang/String;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 147
    :try_start_0
    new-instance v1, Ljava/net/URI;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "http://localhost:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/microsoft/playready/PRMediaPlayer;->m_HttpServerPort:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mRootUri:Ljava/lang/String;

    .line 148
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/microsoft/playready/PRMediaPlayer;->mRootUri:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/playlist.m3u8"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mPlaylistUri:Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    iput-object p1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mContentPath:Ljava/lang/String;

    .line 154
    return-void

    .line 149
    :catch_0
    move-exception v0

    .line 150
    .local v0, "e":Ljava/net/URISyntaxException;
    const-string v1, "PRMediaPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid URI while registering media proxy: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 151
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public setLicenseAcquisitionPlugin(Lcom/microsoft/playready/ILicenseAcquisitionPlugin;)V
    .locals 1
    .param p1, "laPlugin"    # Lcom/microsoft/playready/ILicenseAcquisitionPlugin;

    .prologue
    .line 375
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLicenseAcquirer:Lcom/microsoft/playready/IReactiveLicenseAcquirer;

    invoke-interface {v0, p1}, Lcom/microsoft/playready/IReactiveLicenseAcquirer;->setLicenseAcquisitionPlugin(Lcom/microsoft/playready/ILicenseAcquisitionPlugin;)V

    .line 376
    return-void
.end method

.method public setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    .prologue
    .line 516
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnBufferingUpdateListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 517
    :try_start_0
    iput-object p1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    .line 516
    monitor-exit v1

    .line 519
    return-void

    .line 516
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnCompletionListener;

    .prologue
    .line 467
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnCompletionListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 468
    :try_start_0
    iput-object p1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 467
    monitor-exit v1

    .line 470
    return-void

    .line 467
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnErrorListener;

    .prologue
    .line 667
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnErrorListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 668
    :try_start_0
    iput-object p1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 667
    monitor-exit v1

    .line 670
    return-void

    .line 667
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnInfoListener;

    .prologue
    .line 723
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnInfoListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 724
    :try_start_0
    iput-object p1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    .line 723
    monitor-exit v1

    .line 726
    return-void

    .line 723
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnPreparedListener;

    .prologue
    .line 417
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnPreparedListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 418
    :try_start_0
    iput-object p1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 417
    monitor-exit v1

    .line 420
    return-void

    .line 417
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnSeekCompleteListener;

    .prologue
    .line 566
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnSeekCompleteListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 567
    :try_start_0
    iput-object p1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    .line 566
    monitor-exit v1

    .line 569
    return-void

    .line 566
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    .prologue
    .line 616
    iget-object v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mOnVideoSizeChangedListenerLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 617
    :try_start_0
    iput-object p1, p0, Lcom/microsoft/playready/PRMediaPlayer;->mLegacyOnVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    .line 616
    monitor-exit v1

    .line 619
    return-void

    .line 616
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public stop()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 282
    invoke-super {p0}, Landroid/media/MediaPlayer;->stop()V

    .line 283
    invoke-direct {p0}, Lcom/microsoft/playready/PRMediaPlayer;->shutdownProxy()V

    .line 284
    return-void
.end method

.method public unpause()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 271
    :try_start_0
    iget v1, p0, Lcom/microsoft/playready/PRMediaPlayer;->m_HttpServerPort:I

    invoke-static {v1}, Lcom/microsoft/playready/HttpLocalServer;->getInstance(I)Lcom/microsoft/playready/HttpLocalServer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/playready/HttpLocalServer;->unpause()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 276
    :goto_0
    invoke-super {p0}, Landroid/media/MediaPlayer;->start()V

    .line 277
    return-void

    .line 272
    :catch_0
    move-exception v0

    .line 273
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public updateMediaSelection(Lcom/microsoft/playready/MediaDescription;)V
    .locals 3
    .param p1, "newMediaDescription"    # Lcom/microsoft/playready/MediaDescription;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 761
    invoke-virtual {p1}, Lcom/microsoft/playready/MediaDescription;->commitMediaSelectionChangeToNative()V

    .line 763
    invoke-virtual {p0}, Lcom/microsoft/playready/PRMediaPlayer;->isPlaying()Z

    move-result v1

    .line 764
    .local v1, "wasPlaying":Z
    const/4 v0, 0x0

    .line 766
    .local v0, "position":I
    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/playready/MediaDescription;->isLive()Z

    move-result v2

    if-nez v2, :cond_0

    .line 767
    invoke-virtual {p0}, Lcom/microsoft/playready/PRMediaPlayer;->getCurrentPosition()I

    move-result v0

    .line 770
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/playready/PRMediaPlayer;->stop()V

    .line 771
    invoke-virtual {p0}, Lcom/microsoft/playready/PRMediaPlayer;->prepare()V

    .line 773
    if-eqz v1, :cond_1

    .line 774
    invoke-virtual {p0}, Lcom/microsoft/playready/PRMediaPlayer;->start()V

    .line 775
    if-eqz v0, :cond_1

    .line 776
    invoke-virtual {p0, v0}, Lcom/microsoft/playready/PRMediaPlayer;->seekTo(I)V

    .line 779
    :cond_1
    return-void
.end method
