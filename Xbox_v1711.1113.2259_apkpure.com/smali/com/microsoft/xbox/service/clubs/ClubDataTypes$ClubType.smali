.class public final enum Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
.super Ljava/lang/Enum;
.source "ClubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ClubType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

.field public static final enum Closed:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "closed"
    .end annotation
.end field

.field private static final LOOKUP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum Open:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "open"
    .end annotation
.end field

.field public static final enum Secret:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "secret"
    .end annotation
.end field

.field public static final enum Unknown:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 30
    new-instance v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    const-string v2, "Open"

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Open:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 32
    new-instance v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    const-string v2, "Closed"

    invoke-direct {v1, v2, v4}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Closed:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 34
    new-instance v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    const-string v2, "Secret"

    invoke-direct {v1, v2, v5}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Secret:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 36
    new-instance v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    const-string v2, "Unknown"

    invoke-direct {v1, v2, v6}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Unknown:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 29
    const/4 v1, 0x4

    new-array v1, v1, [Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Open:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    aput-object v2, v1, v3

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Closed:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    aput-object v2, v1, v4

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Secret:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    aput-object v2, v1, v5

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Unknown:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    aput-object v2, v1, v6

    sput-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 41
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->LOOKUP:Ljava/util/Map;

    .line 43
    const-class v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-static {v1}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v0, "type":Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "type":Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 44
    .restart local v0    # "type":Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->LOOKUP:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 46
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getClubType(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    .locals 3
    .param p0, "clubType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 49
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 51
    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->LOOKUP:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 52
    .local v0, "type":Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    if-nez v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Unknown:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .end local v0    # "type":Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 29
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    return-object v0
.end method


# virtual methods
.method public getClubTypeIcon()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 72
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$1;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubType:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown club type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 81
    const-string v0, ""

    :goto_0
    return-object v0

    .line 74
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f070f64

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 76
    :pswitch_1
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f070f79

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 78
    :pswitch_2
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f070efe

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 72
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getLocalizedString()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 57
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$1;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubType:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown club type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 66
    const-string v0, ""

    :goto_0
    return-object v0

    .line 59
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f07026b

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 61
    :pswitch_1
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f070267

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 63
    :pswitch_2
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f0701f1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 57
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
