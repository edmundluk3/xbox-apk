.class Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter$1;
.super Ljava/lang/Object;
.source "TVEpisodeSeasonScreenAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;

.field final synthetic val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter$1;->val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    .line 44
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/TVEpisodeSeasonScreenAdapter$1;->val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/TVEpisodeSeasonScreenViewModel;->navigateToTvEpisodeDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;)V

    .line 45
    return-void
.end method
