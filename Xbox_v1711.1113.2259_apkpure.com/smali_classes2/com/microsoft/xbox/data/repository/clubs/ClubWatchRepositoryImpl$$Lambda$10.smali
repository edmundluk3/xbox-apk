.class final synthetic Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$10;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$10;->arg$1:Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;)Lio/reactivex/functions/Function;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$10;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$10;-><init>(Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$10;->arg$1:Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;

    check-cast p1, Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/domain/beam/BeamChannelDataMapper;->apply(Lcom/microsoft/xbox/data/service/beam/BeamDataTypes$BeamServiceChannel;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
