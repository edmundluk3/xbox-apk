.class Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter$1;
.super Ljava/lang/Object;
.source "ChooseProfileColorDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;

.field final synthetic val$itemColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter$1;->this$1:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter$1;->val$itemColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter$1;->this$1:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;)Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter$1;->val$itemColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;->setCurrentColor(Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;)V

    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter$1;->this$1:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ChooseProfileColorDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;)Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;

    move-result-object v0

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;->closeColorPickerDialog()V

    .line 124
    return-void
.end method
