.class Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$3;
.super Ljava/lang/Object;
.source "MultiplayerTelemetryService.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;->muteMember(Ljava/lang/String;Ljava/util/Map;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

.field final synthetic val$isAudioEnabled:Z

.field final synthetic val$partyMembers:Ljava/util/Map;

.field final synthetic val$xuid:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;Ljava/lang/String;Ljava/util/Map;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$3;->this$0:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    iput-object p2, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$3;->val$xuid:Ljava/lang/String;

    iput-object p3, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$3;->val$partyMembers:Ljava/util/Map;

    iput-boolean p4, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$3;->val$isAudioEnabled:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 113
    iget-object v5, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$3;->val$xuid:Ljava/lang/String;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v5, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$3;->val$partyMembers:Ljava/util/Map;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 115
    const/4 v1, 0x0

    .line 117
    .local v1, "isUserMuted":Z
    iget-object v5, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$3;->val$xuid:Ljava/lang/String;

    invoke-static {v5}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->formatXuid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 118
    .local v0, "formattedXuid":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getCurrentUser()Ljava/lang/String;

    move-result-object v3

    .line 119
    .local v3, "myXuid":Ljava/lang/String;
    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 120
    iget-boolean v5, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$3;->val$isAudioEnabled:Z

    if-nez v5, :cond_1

    move v1, v6

    .line 131
    :cond_0
    :goto_0
    new-instance v2, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 132
    .local v2, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v5, "TargetXuid"

    iget-object v6, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$3;->val$xuid:Ljava/lang/String;

    invoke-virtual {v2, v5, v6}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 134
    if-nez v1, :cond_5

    const-string v5, "Party - Mute party member"

    :goto_1
    invoke-static {v5, v2}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 135
    return-void

    .end local v2    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    :cond_1
    move v1, v7

    .line 120
    goto :goto_0

    .line 122
    :cond_2
    iget-object v5, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$3;->val$partyMembers:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 123
    .local v4, "partyMemberEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->xuid()Ljava/lang/String;

    move-result-object v5

    iget-object v9, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$3;->val$xuid:Ljava/lang/String;

    invoke-static {v5, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 124
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->privacyPermissions()Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->privacyPermissions()Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;->isAllowed()Z

    move-result v5

    if-nez v5, :cond_4

    move v1, v6

    .line 125
    :goto_2
    goto :goto_0

    :cond_4
    move v1, v7

    .line 124
    goto :goto_2

    .line 134
    .end local v4    # "partyMemberEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    .restart local v2    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    :cond_5
    const-string v5, "Party - UnMute party member"

    goto :goto_1
.end method
