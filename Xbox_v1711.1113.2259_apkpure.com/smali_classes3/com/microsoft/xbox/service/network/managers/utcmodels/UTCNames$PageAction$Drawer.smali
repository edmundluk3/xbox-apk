.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$Drawer;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Drawer"
.end annotation


# static fields
.field public static final Achievements:Ljava/lang/String; = "Achievements"

.field public static final ActivityFeed:Ljava/lang/String; = "ActivityFeed"

.field public static final Clubs:Ljava/lang/String; = "Clubs"

.field public static final ConsoleConnection:Ljava/lang/String; = "Console Connection"

.field public static final GameDVR:Ljava/lang/String; = "Game DVR"

.field public static final Home:Ljava/lang/String; = "Home"

.field public static final NetworkTest:Ljava/lang/String; = "Network Test"

.field public static final OneGuide:Ljava/lang/String; = "OneGuide"

.field public static final Pins:Ljava/lang/String; = "Pins"

.field public static final Profile:Ljava/lang/String; = "Profile"

.field public static final Search:Ljava/lang/String; = "Search"

.field public static final SendFeedback:Ljava/lang/String; = "Send Feedback"

.field public static final SmartGlassSettings:Ljava/lang/String; = "SmartGlass Settings"

.field public static final Store:Ljava/lang/String; = "Store"

.field public static final Trending:Ljava/lang/String; = "Trending"

.field public static final Tutorial:Ljava/lang/String; = "Tutorial"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 709
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
