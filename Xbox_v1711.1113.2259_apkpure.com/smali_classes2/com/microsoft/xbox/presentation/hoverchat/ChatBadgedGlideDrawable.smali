.class public Lcom/microsoft/xbox/presentation/hoverchat/ChatBadgedGlideDrawable;
.super Lcom/bumptech/glide/load/resource/bitmap/GlideBitmapDrawable;
.source "ChatBadgedGlideDrawable.java"


# instance fields
.field private final isRight:Z


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;Z)V
    .locals 0
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "isRight"    # Z

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/bumptech/glide/load/resource/bitmap/GlideBitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 22
    iput-boolean p3, p0, Lcom/microsoft/xbox/presentation/hoverchat/ChatBadgedGlideDrawable;->isRight:Z

    .line 23
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 27
    invoke-super {p0, p1}, Lcom/bumptech/glide/load/resource/bitmap/GlideBitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 28
    sget-object v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->INSTANCE:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/hoverchat/ChatBadgedGlideDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    iget-boolean v2, p0, Lcom/microsoft/xbox/presentation/hoverchat/ChatBadgedGlideDrawable;->isRight:Z

    invoke-virtual {v0, p1, v1, v2}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatBadger;->addBadgeToCanvas(Landroid/graphics/Canvas;Landroid/graphics/Rect;Z)V

    .line 29
    return-void
.end method
