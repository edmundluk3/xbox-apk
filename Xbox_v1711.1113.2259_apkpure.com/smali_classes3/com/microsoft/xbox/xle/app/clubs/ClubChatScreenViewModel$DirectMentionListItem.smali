.class public final Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;
.super Ljava/lang/Object;
.source "ClubChatScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DirectMentionListItem"
.end annotation


# instance fields
.field public final gamerPic:Ljava/lang/String;

.field public final gamerTag:Ljava/lang/String;

.field private volatile transient hashCode:I

.field public final realName:Ljava/lang/String;

.field public final xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 1
    .param p1, "data"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1172
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->xuid:Ljava/lang/String;

    .line 1173
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayPicRaw:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->gamerPic:Ljava/lang/String;

    .line 1174
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->gamertag:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->gamerTag:Ljava/lang/String;

    .line 1175
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->realName:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->realName:Ljava/lang/String;

    .line 1176
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;)V
    .locals 1
    .param p1, "data"    # Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1179
    iget-object v0, p1, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->id:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->xuid:Ljava/lang/String;

    .line 1180
    iget-object v0, p1, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->displayPicUri:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->gamerPic:Ljava/lang/String;

    .line 1181
    iget-object v0, p1, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->gamertag:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->gamerTag:Ljava/lang/String;

    .line 1182
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->realName:Ljava/lang/String;

    .line 1184
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 1196
    if-ne p1, p0, :cond_0

    .line 1197
    const/4 v1, 0x1

    .line 1202
    :goto_0
    return v1

    .line 1198
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;

    if-nez v1, :cond_1

    .line 1199
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 1201
    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;

    .line 1202
    .local v0, "other":Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->xuid:Ljava/lang/String;

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->xuid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 1208
    iget v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->hashCode:I

    if-nez v0, :cond_0

    .line 1209
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->hashCode:I

    .line 1210
    iget v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->xuid:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCodeLowercase(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->hashCode:I

    .line 1213
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->hashCode:I

    return v0
.end method

.method public matchesSearchTerm(Ljava/lang/String;)Z
    .locals 2
    .param p1, "searchTerm"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 1187
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1188
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 1190
    .local v0, "lowerTerm":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->gamerTag:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;->realName:Ljava/lang/String;

    .line 1191
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 1190
    :goto_0
    return v1

    .line 1191
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
