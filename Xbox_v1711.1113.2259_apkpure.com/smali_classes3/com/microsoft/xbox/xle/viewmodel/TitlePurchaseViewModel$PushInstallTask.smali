.class Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "TitlePurchaseViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PushInstallTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final productId:Ljava/lang/String;

.field private final skuId:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "productId"    # Ljava/lang/String;
    .param p3, "skuId"    # Ljava/lang/String;

    .prologue
    .line 297
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>.PushInstallTask;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 298
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;->productId:Ljava/lang/String;

    .line 299
    iput-object p3, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;->skuId:Ljava/lang/String;

    .line 300
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 304
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>.PushInstallTask;"
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 328
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>.PushInstallTask;"
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getStoreService()Lcom/microsoft/xbox/service/store/IStoreService;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;->productId:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;->skuId:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/service/store/IStoreService;->pushInstallProduct(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 335
    :goto_0
    return-object v1

    .line 328
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 331
    :catch_0
    move-exception v0

    .line 332
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->access$100()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Exception occurred while loading subscriptions from collection"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 335
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 293
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>.PushInstallTask;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 322
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>.PushInstallTask;"
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 293
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>.PushInstallTask;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 308
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>.PushInstallTask;"
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 317
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>.PushInstallTask;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 318
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 293
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>.PushInstallTask;"
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 312
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;, "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel<TT;>.PushInstallTask;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel$PushInstallTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;->updateAdapter()V

    .line 313
    return-void
.end method
