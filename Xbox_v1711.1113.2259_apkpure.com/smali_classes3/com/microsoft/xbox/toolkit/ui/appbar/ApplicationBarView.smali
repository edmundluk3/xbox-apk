.class public Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;
.super Landroid/widget/FrameLayout;
.source "ApplicationBarView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView$BlockTouchDelegate;
    }
.end annotation


# instance fields
.field protected appBarButtons:[Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field protected iconButtonContainer:Landroid/widget/LinearLayout;

.field protected layoutId:I

.field protected nowPlayingPrimaryButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field protected nowPlayingSecondaryButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field protected nowPlayingSecondaryTile:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field protected nowPlayingTile:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field protected recordThatButton:Landroid/widget/Button;

.field protected remoteControlButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field protected volumeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layoutId"    # I

    .prologue
    .line 57
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 58
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->setSoundEffectsEnabled(Z)V

    .line 59
    iput p2, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->layoutId:I

    .line 60
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->initialize()V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 65
    sget-object v1, Lcom/microsoft/xboxone/smartglass/R$styleable;->ApplicationBar:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 66
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->layoutId:I

    .line 67
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 68
    return-void
.end method


# virtual methods
.method public addIconButton(Lcom/microsoft/xbox/toolkit/ui/XLEButton;)V
    .locals 6
    .param p1, "button"    # Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .prologue
    .line 154
    const-string v1, "Menu items should only be added to the overflow app bar"

    instance-of v2, p1, Lcom/microsoft/xbox/toolkit/ui/appbar/AppBarMenuButton;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertFalse(Ljava/lang/String;Z)V

    .line 155
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 156
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f090046

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 157
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f090043

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 158
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f090044

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f09004f

    .line 159
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f090045

    .line 160
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f09004e

    .line 161
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 158
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 162
    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 164
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeViewFromParent(Landroid/view/View;)V

    .line 165
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->iconButtonContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 166
    return-void
.end method

.method public cleanup()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 169
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->iconButtonContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 170
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->iconButtonContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 172
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->volumeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v1, :cond_1

    .line 173
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->volumeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->remoteControlButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v1, :cond_2

    .line 176
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->remoteControlButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->iconButtonContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViewsInLayout()V

    .line 180
    return-void
.end method

.method public getAppBarButtons()[Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->appBarButtons:[Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object v0
.end method

.method public getIconButtonContainer()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->iconButtonContainer:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public getNowPlayingPrimaryText()Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->nowPlayingPrimaryButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object v0
.end method

.method public getNowPlayingSecondaryText()Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->nowPlayingSecondaryButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object v0
.end method

.method public getNowPlayingSecondaryTile()Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->nowPlayingSecondaryTile:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    return-object v0
.end method

.method public getNowPlayingTile()Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->nowPlayingTile:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    return-object v0
.end method

.method public getRecordThatButton()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->recordThatButton:Landroid/widget/Button;

    return-object v0
.end method

.method public getRemoteControlButton()Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->remoteControlButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object v0
.end method

.method public getVolumeButton()Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->volumeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object v0
.end method

.method protected initialize()V
    .locals 6

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getChildCount()I

    move-result v3

    new-array v3, v3, [Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->appBarButtons:[Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 108
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 109
    const-string v3, "All children of ApplicationBar must be of XLEButton type."

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    instance-of v4, v4, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 110
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 111
    .local v0, "button":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->appBarButtons:[Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    aput-object v0, v3, v1

    .line 108
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 113
    .end local v0    # "button":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->removeAllViews()V

    .line 115
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->layoutId:I

    if-lez v3, :cond_1

    .line 116
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 117
    .local v2, "vi":Landroid/view/LayoutInflater;
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->layoutId:I

    const/4 v4, 0x1

    invoke-virtual {v2, v3, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 119
    const v3, 0x7f0e045a

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->nowPlayingTile:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 120
    const v3, 0x7f0e0459

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->nowPlayingSecondaryTile:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 121
    const v3, 0x7f0e045b

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->nowPlayingPrimaryButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 122
    const v3, 0x7f0e045c

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->nowPlayingSecondaryButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 124
    const v3, 0x7f0e0457

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->volumeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 125
    const v3, 0x7f0e0456

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->remoteControlButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 127
    const v3, 0x7f0e045d

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->iconButtonContainer:Landroid/widget/LinearLayout;

    .line 128
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->iconButtonContainer:Landroid/widget/LinearLayout;

    new-instance v4, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView$BlockTouchDelegate;

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->iconButtonContainer:Landroid/widget/LinearLayout;

    invoke-direct {v4, p0, v5}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView$BlockTouchDelegate;-><init>(Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;Landroid/view/View;)V

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 132
    .end local v2    # "vi":Landroid/view/LayoutInflater;
    :goto_1
    return-void

    .line 130
    :cond_1
    const-string v3, "ApplicationBarView"

    const-string v4, "Did you forget to include xle:layout in your appbar view definition?"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public isLayoutRequested()Z
    .locals 3

    .prologue
    .line 143
    invoke-super {p0}, Landroid/widget/FrameLayout;->isLayoutRequested()Z

    move-result v0

    .line 145
    .local v0, "override":Z
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 146
    const-string v1, "ApplicationBarView"

    const-string v2, "Animating. Overriding isLayoutRequested to false"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const/4 v0, 0x0

    .line 150
    :cond_0
    return v0
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 136
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 138
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->initialize()V

    .line 139
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 210
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 207
    return-void
.end method

.method public setNowPlayingEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 195
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->nowPlayingTile:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->nowPlayingTile:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setEnabled(Z)V

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->nowPlayingPrimaryButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 199
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->nowPlayingPrimaryButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 201
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->nowPlayingSecondaryButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_2

    .line 202
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->nowPlayingSecondaryButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 204
    :cond_2
    return-void
.end method

.method public setNowPlayingVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 183
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->nowPlayingTile:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->nowPlayingTile:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->nowPlayingPrimaryButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->nowPlayingPrimaryButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 189
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->nowPlayingSecondaryButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_2

    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->nowPlayingSecondaryButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 192
    :cond_2
    return-void
.end method
