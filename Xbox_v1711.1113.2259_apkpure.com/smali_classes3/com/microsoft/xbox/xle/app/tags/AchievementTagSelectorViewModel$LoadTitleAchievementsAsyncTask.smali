.class Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "AchievementTagSelectorViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadTitleAchievementsAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private forceRefresh:Z

.field private isLoading:Z

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;Z)V
    .locals 0
    .param p2, "forceRefresh"    # Z

    .prologue
    .line 123
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 124
    iput-boolean p2, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->forceRefresh:Z

    .line 125
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->forceRefresh:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;

    .line 134
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->access$000(Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->access$100(Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;

    .line 135
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->access$000(Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->isXboxOneAchievement()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->access$100(Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleModel;->shouldRefreshXboxoneGameProgressAchievements()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 133
    :goto_0
    return v0

    .line 135
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->access$100(Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleModel;->shouldRefresh360GameProgressAchievements()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIsLoading()Z
    .locals 1

    .prologue
    .line 128
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->isLoading:Z

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->access$100(Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->access$000(Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 153
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->access$000(Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->isXboxOneAchievement()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->access$100(Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->forceRefresh:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameProgressXboxoneAchievements(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 160
    :goto_0
    return-object v0

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->access$100(Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->forceRefresh:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameProgress360Achievements(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    goto :goto_0

    .line 160
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->isLoading:Z

    .line 147
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 140
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->isLoading:Z

    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->access$200(Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 142
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 170
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->isLoading:Z

    .line 171
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;->access$200(Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 172
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 119
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/tags/AchievementTagSelectorViewModel$LoadTitleAchievementsAsyncTask;->isLoading:Z

    .line 166
    return-void
.end method
