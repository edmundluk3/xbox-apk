.class abstract Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;
.super Lcom/microsoft/xbox/domain/oobe/OOBESettings;
.source "$AutoValue_OOBESettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;
    }
.end annotation


# instance fields
.field private final autoDST:Z

.field private final autoUpdateApps:Z

.field private final autoUpdateSystem:Z

.field private final instantOn:Ljava/lang/Boolean;

.field private final timeZone:Ljava/lang/String;


# direct methods
.method constructor <init>(ZZLjava/lang/Boolean;ZLjava/lang/String;)V
    .locals 2
    .param p1, "autoUpdateApps"    # Z
    .param p2, "autoUpdateSystem"    # Z
    .param p3, "instantOn"    # Ljava/lang/Boolean;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "autoDST"    # Z
    .param p5, "timeZone"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;-><init>()V

    .line 23
    iput-boolean p1, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->autoUpdateApps:Z

    .line 24
    iput-boolean p2, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->autoUpdateSystem:Z

    .line 25
    iput-object p3, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->instantOn:Ljava/lang/Boolean;

    .line 26
    iput-boolean p4, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->autoDST:Z

    .line 27
    if-nez p5, :cond_0

    .line 28
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null timeZone"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_0
    iput-object p5, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->timeZone:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public autoDST()Z
    .locals 1
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "AutoDSTEnabled"
    .end annotation

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->autoDST:Z

    return v0
.end method

.method public autoUpdateApps()Z
    .locals 1
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "allowAutoUpdateApps"
    .end annotation

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->autoUpdateApps:Z

    return v0
.end method

.method public autoUpdateSystem()Z
    .locals 1
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "allowAutoUpdateSystem"
    .end annotation

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->autoUpdateSystem:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 77
    if-ne p1, p0, :cond_1

    .line 88
    :cond_0
    :goto_0
    return v1

    .line 80
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    if-eqz v3, :cond_4

    move-object v0, p1

    .line 81
    check-cast v0, Lcom/microsoft/xbox/domain/oobe/OOBESettings;

    .line 82
    .local v0, "that":Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    iget-boolean v3, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->autoUpdateApps:Z

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->autoUpdateApps()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->autoUpdateSystem:Z

    .line 83
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->autoUpdateSystem()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->instantOn:Ljava/lang/Boolean;

    if-nez v3, :cond_3

    .line 84
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->instantOn()Ljava/lang/Boolean;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-boolean v3, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->autoDST:Z

    .line 85
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->autoDST()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->timeZone:Ljava/lang/String;

    .line 86
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->timeZone()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 84
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->instantOn:Ljava/lang/Boolean;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBESettings;->instantOn()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .end local v0    # "that":Lcom/microsoft/xbox/domain/oobe/OOBESettings;
    :cond_4
    move v1, v2

    .line 88
    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const v4, 0xf4243

    .line 93
    const/4 v0, 0x1

    .line 94
    .local v0, "h":I
    mul-int/2addr v0, v4

    .line 95
    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->autoUpdateApps:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 96
    mul-int/2addr v0, v4

    .line 97
    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->autoUpdateSystem:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 98
    mul-int/2addr v0, v4

    .line 99
    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->instantOn:Ljava/lang/Boolean;

    if-nez v1, :cond_2

    const/4 v1, 0x0

    :goto_2
    xor-int/2addr v0, v1

    .line 100
    mul-int/2addr v0, v4

    .line 101
    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->autoDST:Z

    if-eqz v1, :cond_3

    :goto_3
    xor-int/2addr v0, v2

    .line 102
    mul-int/2addr v0, v4

    .line 103
    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->timeZone:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 104
    return v0

    :cond_0
    move v1, v3

    .line 95
    goto :goto_0

    :cond_1
    move v1, v3

    .line 97
    goto :goto_1

    .line 99
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->instantOn:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_3
    move v2, v3

    .line 101
    goto :goto_3
.end method

.method public instantOn()Ljava/lang/Boolean;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "AllowInstantOn"
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->instantOn:Ljava/lang/Boolean;

    return-object v0
.end method

.method public timeZone()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "currentTimeZone"
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->timeZone:Ljava/lang/String;

    return-object v0
.end method

.method public toBuilder()Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;
    .locals 1

    .prologue
    .line 109
    new-instance v0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings$Builder;-><init>(Lcom/microsoft/xbox/domain/oobe/OOBESettings;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OOBESettings{autoUpdateApps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->autoUpdateApps:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", autoUpdateSystem="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->autoUpdateSystem:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", instantOn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->instantOn:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", autoDST="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->autoDST:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", timeZone="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/$AutoValue_OOBESettings;->timeZone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
