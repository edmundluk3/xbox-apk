.class public Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;
.source "AlbumDetailsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;",
        ">;"
    }
.end annotation


# instance fields
.field private autoShowSongList:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;-><init>()V

    .line 18
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;->autoShowSongList:Z

    .line 22
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getAlbumDetailAdapter(Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 23
    return-void
.end method


# virtual methods
.method public getAlbumYearAndStudio()Ljava/lang/String;
    .locals 5

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;->getReleaseYear()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;->getLabelOwner()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070e6e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->concatenateStringsWithDelimiter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getArtist()Ljava/lang/String;
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getArtistName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getArtistName()Ljava/lang/String;

    move-result-object v0

    .line 37
    :goto_0
    return-object v0

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getTracks()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getTracks()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getTracks()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->getArtistName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 37
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getErrorStringResourceId()I
    .locals 1

    .prologue
    .line 83
    const v0, 0x7f070683

    return v0
.end method

.method public getIsExplicit()Z
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getIsExplicit()Z

    move-result v0

    return v0
.end method

.method public getLabelOwner()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getLabelOwner()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTracks()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getTracks()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public isAutoShowSongList()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;->autoShowSongList:Z

    return v0
.end method

.method public navigateToArtistDetail()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getPrimaryArtist()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getPrimaryArtist()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicArtistMediaItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 79
    :cond_0
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 27
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getAlbumDetailAdapter(Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 28
    return-void
.end method

.method public playAlbum()V
    .locals 8

    .prologue
    const-wide/32 v6, 0x18ffc9f4

    .line 71
    const-string v2, "ms-xbl-%08X://media-playback?ContentID=%s&ContentType=%s"

    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    const v4, 0x18ffc9f4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v4, 0x1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    const/4 v1, 0x2

    const-string v4, "Album"

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "uri":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Full:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    invoke-static {v6, v7, v1}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->getProviderLocation(JLcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    move-result-object v1

    invoke-static {v0, v1, v6, v7}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchUriThenRemoteIfTitleNotPlaying(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;J)V

    .line 73
    return-void
.end method

.method public playTrack(I)V
    .locals 8
    .param p1, "position"    # I

    .prologue
    const-wide/32 v6, 0x18ffc9f4

    .line 65
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumDetailModel;->getTracks()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    .line 66
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;
    const-string v2, "ms-xbl-%08X://media-playback?ContentID=%s&ContentType=%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const v5, 0x18ffc9f4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->ID:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "Track"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 67
    .local v1, "uri":Ljava/lang/String;
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Full:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    invoke-static {v6, v7, v2}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->getProviderLocation(JLcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    move-result-object v2

    invoke-static {v1, v2, v6, v7}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchUriThenRemoteIfTitleNotPlaying(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;J)V

    .line 68
    return-void
.end method

.method public setAutoShowSongList(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/AlbumDetailsScreenViewModel;->autoShowSongList:Z

    .line 62
    return-void
.end method
