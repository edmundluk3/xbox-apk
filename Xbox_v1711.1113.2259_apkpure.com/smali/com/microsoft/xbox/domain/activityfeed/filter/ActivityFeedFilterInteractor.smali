.class public final Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;
.super Ljava/lang/Object;
.source "ActivityFeedFilterInteractor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$ApplyAction;,
        Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$PrefsChangedAction;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final applyActionTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$ApplyAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult",
            "<",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;",
            ">;>;"
        }
    .end annotation
.end field

.field private final initialLoadTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult",
            "<",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;",
            ">;>;"
        }
    .end annotation
.end field

.field private final prefsChangedTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$PrefsChangedAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult",
            "<",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const-class v0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V
    .locals 1
    .param p1, "repository"    # Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;
    .param p2, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    invoke-static {p0, p1, p2}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;->initialLoadTransformer:Lio/reactivex/ObservableTransformer;

    .line 80
    invoke-static {p0, p2}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;->prefsChangedTransformer:Lio/reactivex/ObservableTransformer;

    .line 90
    invoke-static {p1, p2}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;->applyActionTransformer:Lio/reactivex/ObservableTransformer;

    .line 98
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    .locals 1

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;->toValidPrefs(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$lambda$1(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    .locals 1

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;->toValidPrefs(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;
    .param p1, "repository"    # Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;
    .param p2, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p3, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 71
    invoke-static {p0, p1, p2}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$13;->lambdaFactory$(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 72
    invoke-virtual {p3, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 71
    return-object v0
.end method

.method static synthetic lambda$new$5(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 80
    invoke-static {p0, p1}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 81
    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 80
    return-object v0
.end method

.method static synthetic lambda$new$9(Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 90
    invoke-static {p0, p1}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 91
    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 90
    return-object v0
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 77
    sget-object v0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;
    .param p1, "repository"    # Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;
    .param p2, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p3, "action"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 73
    invoke-interface {p1}, Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;->getPrefs()Lio/reactivex/Observable;

    move-result-object v0

    .line 74
    invoke-interface {p2}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$14;->lambdaFactory$(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 75
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$15;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 76
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p3}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$16;->lambdaFactory$(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 77
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 73
    return-object v0
.end method

.method static synthetic lambda$null$3(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$PrefsChangedAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$PrefsChangedAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 87
    sget-object v0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$4(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$PrefsChangedAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "action"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$PrefsChangedAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 82
    invoke-static {p2}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    .line 83
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$9;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 84
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 85
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$11;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 86
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p2}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$12;->lambdaFactory$(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$PrefsChangedAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 87
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 82
    return-object v0
.end method

.method static synthetic lambda$null$6(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$ApplyAction;Ljava/lang/Object;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$ApplyAction;
    .param p1, "ignore"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$ApplyAction;->prefs()Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;->with(Ljava/lang/Object;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$7(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$ApplyAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;)V
    .locals 2
    .param p0, "action"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$ApplyAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 96
    sget-object v0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;->TAG:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$8(Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$ApplyAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "action"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$ApplyAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 92
    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$ApplyAction;->prefs()Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/microsoft/xbox/data/repository/activityfeed/ActivityFeedFilterRepository;->setPrefs(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)Lio/reactivex/Completable;

    move-result-object v0

    .line 93
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Completable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object v0

    .line 94
    invoke-virtual {v0}, Lio/reactivex/Completable;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p2}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$ApplyAction;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 95
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p2}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$ApplyAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 96
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 92
    return-object v0
.end method

.method private toValidPrefs(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    .locals 5
    .param p1, "prefs"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    .prologue
    .line 117
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showFriends()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showFavorites()Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showFriends()Z

    move-result v0

    const/4 v1, 0x1

    .line 121
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showClubs()Z

    move-result v2

    .line 122
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showGames()Z

    move-result v3

    .line 123
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showPopular()Z

    move-result v4

    .line 118
    invoke-static {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->with(ZZZZZ)Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    move-result-object p1

    .line 125
    .end local p1    # "prefs":Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    :cond_0
    return-object p1
.end method


# virtual methods
.method public getApplyActionTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$ApplyAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult",
            "<",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;->applyActionTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public getInitialLoadTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult",
            "<",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;->initialLoadTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public getPrefsChangedTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$PrefsChangedAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult",
            "<",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;->prefsChangedTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method
