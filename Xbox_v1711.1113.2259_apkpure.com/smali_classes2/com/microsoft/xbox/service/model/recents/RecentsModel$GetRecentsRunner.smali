.class Lcom/microsoft/xbox/service/model/recents/RecentsModel$GetRecentsRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "RecentsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/recents/RecentsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetRecentsRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/microsoft/xbox/service/model/recents/Recent;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/recents/RecentsModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/recents/RecentsModel;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/recents/RecentsModel$GetRecentsRunner;->this$0:Lcom/microsoft/xbox/service/model/recents/RecentsModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/recents/RecentsModel;Lcom/microsoft/xbox/service/model/recents/RecentsModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/recents/RecentsModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/recents/RecentsModel$1;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/recents/RecentsModel$GetRecentsRunner;-><init>(Lcom/microsoft/xbox/service/model/recents/RecentsModel;)V

    return-void
.end method

.method private getTitleImagesFromId(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 169
    .local p1, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 171
    .local v1, "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 172
    .local v0, "legacyId":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->access$400()Lcom/microsoft/xbox/service/store/IStoreService;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/microsoft/xbox/service/store/IStoreService;->getProductFromLegacyProductId(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v2

    .line 173
    .local v2, "storeResponseList":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->getProducts()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 174
    new-instance v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->getProducts()Ljava/util/List;

    move-result-object v3

    const/4 v6, 0x0

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;

    invoke-direct {v5, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 178
    .end local v0    # "legacyId":Ljava/lang/String;
    .end local v2    # "storeResponseList":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    :cond_1
    return-object v1
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/recents/RecentsModel$GetRecentsRunner;->buildData()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/ArrayList;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/recents/Recent;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 88
    invoke-static {}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->access$100()Lcom/microsoft/xbox/service/network/managers/IESServiceManager;

    move-result-object v10

    invoke-interface {v10}, Lcom/microsoft/xbox/service/network/managers/IESServiceManager;->getRecents()Ljava/util/ArrayList;

    move-result-object v5

    .line 89
    .local v5, "recents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/recents/Recent;>;"
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v10

    if-nez v10, :cond_b

    .line 92
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 93
    .local v3, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 94
    .local v8, "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/recents/Recent;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/recents/Recent;

    .line 95
    .local v4, "recent":Lcom/microsoft/xbox/service/model/recents/Recent;
    iget-object v11, v4, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v11, v11, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ItemId:Ljava/lang/String;

    invoke-virtual {v3, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 98
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v11

    const/16 v12, 0xa

    if-lt v11, v12, :cond_0

    .line 99
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/service/model/recents/RecentsModel$GetRecentsRunner;->getTitleImagesFromId(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 100
    .local v1, "details":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 101
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_1
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/service/model/recents/Recent;

    .line 102
    .local v9, "trecent":Lcom/microsoft/xbox/service/model/recents/Recent;
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 103
    .local v7, "tdetail":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v13, v9, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v13, v13, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ItemId:Ljava/lang/String;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 104
    iget-object v12, v9, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getSquareIconUrl()Ljava/lang/String;

    move-result-object v13

    iput-object v13, v12, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ImageUrl:Ljava/lang/String;

    .line 105
    iget-object v12, v9, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBoxArtBackgroundColor()I

    move-result v13

    invoke-virtual {v12, v13}, Lcom/microsoft/xbox/service/model/recents/RecentItem;->setBoxArtBackgroundColor(I)V

    goto :goto_1

    .line 111
    .end local v7    # "tdetail":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .end local v9    # "trecent":Lcom/microsoft/xbox/service/model/recents/Recent;
    :cond_3
    new-instance v3, Ljava/util/ArrayList;

    .end local v3    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 112
    .restart local v3    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v8, Ljava/util/ArrayList;

    .end local v8    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/recents/Recent;>;"
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .restart local v8    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/recents/Recent;>;"
    goto :goto_0

    .line 117
    .end local v1    # "details":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    .end local v4    # "recent":Lcom/microsoft/xbox/service/model/recents/Recent;
    :cond_4
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lez v10, :cond_7

    .line 118
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/service/model/recents/RecentsModel$GetRecentsRunner;->getTitleImagesFromId(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 119
    .restart local v1    # "details":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v10

    if-nez v10, :cond_7

    .line 120
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_5
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/service/model/recents/Recent;

    .line 121
    .restart local v9    # "trecent":Lcom/microsoft/xbox/service/model/recents/Recent;
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_6
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_5

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 122
    .restart local v7    # "tdetail":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v12, v9, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v12, v12, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ItemId:Ljava/lang/String;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 123
    iget-object v11, v9, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getSquareIconUrl()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v11, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ImageUrl:Ljava/lang/String;

    goto :goto_2

    .line 131
    .end local v1    # "details":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    .end local v7    # "tdetail":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .end local v9    # "trecent":Lcom/microsoft/xbox/service/model/recents/Recent;
    :cond_7
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 133
    .local v0, "appRecents":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/recents/Recent;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_8
    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_9

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/recents/Recent;

    .line 134
    .restart local v4    # "recent":Lcom/microsoft/xbox/service/model/recents/Recent;
    const-string v11, "DApp"

    iget-object v12, v4, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-virtual {v12}, Lcom/microsoft/xbox/service/model/recents/RecentItem;->getContentType()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 135
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 139
    .end local v4    # "recent":Lcom/microsoft/xbox/service/model/recents/Recent;
    :cond_9
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v10

    if-nez v10, :cond_b

    .line 140
    new-instance v6, Ljava/util/concurrent/CountDownLatch;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v10

    invoke-direct {v6, v10}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 142
    .local v6, "storeSearchComplete":Ljava/util/concurrent/CountDownLatch;
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_a

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/recents/Recent;

    .line 143
    .restart local v4    # "recent":Lcom/microsoft/xbox/service/model/recents/Recent;
    invoke-static {}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->access$200()Ljava/lang/String;

    move-result-object v11

    const-string v12, "Launching Store Item from Recent"

    invoke-static {v11, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    sget-object v11, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    new-instance v12, Lcom/microsoft/xbox/service/model/recents/RecentsModel$SingleRecentRunner;

    iget-object v13, p0, Lcom/microsoft/xbox/service/model/recents/RecentsModel$GetRecentsRunner;->this$0:Lcom/microsoft/xbox/service/model/recents/RecentsModel;

    invoke-direct {v12, v13, v4, v6}, Lcom/microsoft/xbox/service/model/recents/RecentsModel$SingleRecentRunner;-><init>(Lcom/microsoft/xbox/service/model/recents/RecentsModel;Lcom/microsoft/xbox/service/model/recents/Recent;Ljava/util/concurrent/CountDownLatch;)V

    invoke-interface {v11, v12}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_4

    .line 148
    .end local v4    # "recent":Lcom/microsoft/xbox/service/model/recents/Recent;
    :cond_a
    const-wide/16 v10, 0x7530

    :try_start_0
    sget-object v12, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, v10, v11, v12}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 155
    .end local v0    # "appRecents":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/recents/Recent;>;"
    .end local v3    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v6    # "storeSearchComplete":Ljava/util/concurrent/CountDownLatch;
    .end local v8    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/recents/Recent;>;"
    :cond_b
    :goto_5
    return-object v5

    .line 149
    .restart local v0    # "appRecents":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/recents/Recent;>;"
    .restart local v3    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v6    # "storeSearchComplete":Ljava/util/concurrent/CountDownLatch;
    .restart local v8    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/recents/Recent;>;"
    :catch_0
    move-exception v2

    .line 150
    .local v2, "ex":Ljava/lang/InterruptedException;
    invoke-static {}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->access$200()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Interrupted app retrieval: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 165
    const-wide/16 v0, 0xfbd

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/recents/Recent;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 160
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/recents/Recent;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/recents/RecentsModel$GetRecentsRunner;->this$0:Lcom/microsoft/xbox/service/model/recents/RecentsModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->access$300(Lcom/microsoft/xbox/service/model/recents/RecentsModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 161
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 84
    return-void
.end method
