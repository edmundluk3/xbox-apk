.class final Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior$1;
.super Ljava/lang/Object;
.source "StackingBehavior.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->computeSortedDependencies(Landroid/support/design/widget/CoordinatorLayout;Landroid/view/View;)Ljava/util/Set;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$parent:Landroid/support/design/widget/CoordinatorLayout;


# direct methods
.method constructor <init>(Landroid/support/design/widget/CoordinatorLayout;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior$1;->val$parent:Landroid/support/design/widget/CoordinatorLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Landroid/view/View;Landroid/view/View;)I
    .locals 2
    .param p1, "lhs"    # Landroid/view/View;
    .param p2, "rhs"    # Landroid/view/View;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior$1;->val$parent:Landroid/support/design/widget/CoordinatorLayout;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/CoordinatorLayout;->indexOfChild(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior$1;->val$parent:Landroid/support/design/widget/CoordinatorLayout;

    invoke-virtual {v1, p2}, Landroid/support/design/widget/CoordinatorLayout;->indexOfChild(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 124
    check-cast p1, Landroid/view/View;

    check-cast p2, Landroid/view/View;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior$1;->compare(Landroid/view/View;Landroid/view/View;)I

    move-result v0

    return v0
.end method
