.class Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "CustomizeProfileScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChangeGamertagAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)V
    .locals 0

    .prologue
    .line 891
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$1;

    .prologue
    .line 891
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 895
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 896
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3

    .prologue
    .line 925
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCCustomizeProfile;->trackGamerTagChange()V

    .line 926
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->access$1900(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->changeGamertag(Ljava/lang/String;Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    .line 927
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 891
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 920
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 891
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 1

    .prologue
    .line 901
    const-string v0, "ChangeGamertagAsyncTask should always run if requested"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 902
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 914
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->access$1702(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Z)Z

    .line 915
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->access$1800(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 916
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 891
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 906
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 907
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->access$1702(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Z)Z

    .line 909
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$ChangeGamertagAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->updateGamertagPickerDialog()V

    .line 910
    return-void
.end method
