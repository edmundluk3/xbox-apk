.class public abstract Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "CarouselPagerAdapter.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# static fields
.field private static final ActualStartIndex:I = 0x1

.field private static final NumFakeViews:I = 0x2


# instance fields
.field private numOfItems:I

.field private paddedCount:I

.field protected viewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>(ILandroid/support/v4/view/ViewPager;)V
    .locals 1
    .param p1, "numOfItems"    # I
    .param p2, "viewPager"    # Landroid/support/v4/view/ViewPager;

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 37
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;->numOfItems:I

    .line 38
    add-int/lit8 v0, p1, 0x2

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;->paddedCount:I

    .line 39
    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 42
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 63
    check-cast p3, Landroid/view/View;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 64
    return-void
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 73
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;->numOfItems:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 74
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;->numOfItems:I

    .line 76
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;->paddedCount:I

    goto :goto_0
.end method

.method public getRealCount()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;->numOfItems:I

    return v0
.end method

.method public final instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 1
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p2, v0, :cond_0

    .line 51
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;->instantiateItemInternal(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    .line 57
    :goto_0
    return-object v0

    .line 52
    :cond_0
    if-nez p2, :cond_1

    .line 54
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;->numOfItems:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;->instantiateItemInternal(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 57
    :cond_1
    add-int/lit8 v0, p2, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;->instantiateItemInternal(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public abstract instantiateItemInternal(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 91
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 3

    .prologue
    .line 96
    invoke-super {p0}, Landroid/support/v4/view/PagerAdapter;->notifyDataSetChanged()V

    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;->viewPager:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 100
    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 5
    .param p1, "state"    # I

    .prologue
    const/4 v4, 0x0

    .line 112
    if-nez p1, :cond_0

    .line 113
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 114
    .local v0, "currentItemIndex":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;->getCount()I

    move-result v1

    .line 117
    .local v1, "totalPageCount":I
    if-nez v0, :cond_1

    .line 118
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;->viewPager:Landroid/support/v4/view/ViewPager;

    add-int/lit8 v3, v1, -0x2

    invoke-virtual {v2, v3, v4}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 123
    .end local v0    # "currentItemIndex":I
    .end local v1    # "totalPageCount":I
    :cond_0
    :goto_0
    return-void

    .line 119
    .restart local v0    # "currentItemIndex":I
    .restart local v1    # "totalPageCount":I
    :cond_1
    add-int/lit8 v2, v1, -0x1

    if-ne v0, v2, :cond_0

    .line 120
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;->viewPager:Landroid/support/v4/view/ViewPager;

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    goto :goto_0
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 104
    return-void
.end method

.method public onPageSelected(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 108
    return-void
.end method

.method protected setCount(I)V
    .locals 1
    .param p1, "newCount"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;->numOfItems:I

    .line 86
    add-int/lit8 v0, p1, 0x2

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/CarouselPagerAdapter;->paddedCount:I

    .line 87
    return-void
.end method
