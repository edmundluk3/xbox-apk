.class public Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;
.super Ljava/lang/Object;
.source "EpgConnectorFragmentMsg.java"


# instance fields
.field private datagramId:I

.field private datagramSize:I

.field private fragmentData:Ljava/lang/String;

.field private fragmentLength:I

.field private fragmentOffset:I


# direct methods
.method public constructor <init>(IIIILjava/lang/String;)V
    .locals 0
    .param p1, "datagramId"    # I
    .param p2, "datagramSize"    # I
    .param p3, "fragmentOffset"    # I
    .param p4, "fragmentLength"    # I
    .param p5, "fragmentData"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput p1, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;->datagramId:I

    .line 21
    iput p2, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;->datagramSize:I

    .line 22
    iput p3, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;->fragmentOffset:I

    .line 23
    iput p4, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;->fragmentLength:I

    .line 24
    iput-object p5, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;->fragmentData:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public static getFragmentMsgFromJson(Ljava/lang/String;)Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;
    .locals 9
    .param p0, "jsonMessage"    # Ljava/lang/String;

    .prologue
    .line 49
    :try_start_0
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 50
    .local v7, "jo":Lorg/json/JSONObject;
    const-string v0, "datagram_id"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 51
    .local v1, "datagramId":I
    const-string v0, "datagram_size"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 52
    .local v2, "datagramSize":I
    const-string v0, "fragment_offset"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 53
    .local v3, "fragmentOffset":I
    const-string v0, "fragment_length"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 54
    .local v4, "fragmentLength":I
    const-string v0, "fragment_data"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 55
    .local v5, "fragmentData":Ljava/lang/String;
    new-instance v0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;-><init>(IIIILjava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    .end local v1    # "datagramId":I
    .end local v2    # "datagramSize":I
    .end local v3    # "fragmentOffset":I
    .end local v4    # "fragmentLength":I
    .end local v5    # "fragmentData":Ljava/lang/String;
    .end local v7    # "jo":Lorg/json/JSONObject;
    :goto_0
    return-object v0

    .line 60
    :catch_0
    move-exception v6

    .line 61
    .local v6, "e":Lorg/json/JSONException;
    const-string v0, "EpgConnectorFragmentMsg"

    invoke-virtual {v6}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getDatagramId()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;->datagramId:I

    return v0
.end method

.method public getDatagramSize()I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;->datagramSize:I

    return v0
.end method

.method public getFragmentData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;->fragmentData:Ljava/lang/String;

    return-object v0
.end method

.method public getFragmentLength()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;->fragmentLength:I

    return v0
.end method

.method public getFragmentOffset()I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;->fragmentOffset:I

    return v0
.end method
