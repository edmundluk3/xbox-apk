.class final Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$4;
.super Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;
.source "HomeScreenPopupScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->createContentItemScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter",
        "<",
        "Lcom/microsoft/xbox/service/model/pins/LaunchableItem;",
        ">;"
    }
.end annotation


# instance fields
.field private currentData:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/LaunchableItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;)V
    .locals 1

    .prologue
    .line 144
    .local p1, "viewModel":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase<Lcom/microsoft/xbox/service/model/pins/LaunchableItem;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$1;)V

    return-void
.end method


# virtual methods
.method protected createAndBindHeader(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/LaunchableItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "data":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData<Lcom/microsoft/xbox/service/model/pins/LaunchableItem;>;"
    const/4 v10, 0x4

    .line 149
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;->getHeaderData()Ljava/lang/Object;

    move-result-object v8

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$4;->currentData:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;

    if-eq v8, p1, :cond_1

    .line 150
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$4;->currentData:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;

    .line 152
    const v8, 0x7f0e06d8

    invoke-virtual {p0, v8}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$4;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 154
    .local v5, "root":Landroid/view/View;
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;->getHeaderData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    .line 156
    .local v3, "li":Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
    const v8, 0x7f0e06de

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 157
    .local v4, "provider":Landroid/widget/TextView;
    invoke-interface {v3}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getContentType()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->access$200(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 158
    invoke-interface {v3}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getProviderName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 161
    :cond_0
    const v8, 0x7f0e06df

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 162
    .local v6, "title":Landroid/widget/TextView;
    invoke-interface {v3}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 164
    const v8, 0x7f0e06dc

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 165
    .local v7, "titleImage":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    const v8, 0x7f0e001d

    new-instance v9, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$4$1;

    invoke-direct {v9, p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$4$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$4;Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)V

    invoke-virtual {v7, v8, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setTag(ILjava/lang/Object;)V

    .line 173
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;->getPlaceholderRid()I

    move-result v1

    .line 174
    .local v1, "defaultRid":I
    invoke-interface {v3}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getImageUrl()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v1, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 175
    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->access$300(Landroid/widget/ImageView;)V

    .line 177
    const v8, 0x7f0e06e0

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 178
    .local v0, "companion":Landroid/widget/TextView;
    invoke-virtual {v5}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;->getHasState()Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->access$400(Landroid/content/res/Resources;Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;)Ljava/lang/String;

    move-result-object v2

    .line 179
    .local v2, "hasString":Ljava/lang/String;
    invoke-static {v0, v2, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 181
    .end local v0    # "companion":Landroid/widget/TextView;
    .end local v1    # "defaultRid":I
    .end local v2    # "hasString":Ljava/lang/String;
    .end local v3    # "li":Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
    .end local v4    # "provider":Landroid/widget/TextView;
    .end local v5    # "root":Landroid/view/View;
    .end local v6    # "title":Landroid/widget/TextView;
    .end local v7    # "titleImage":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    :cond_1
    return-void
.end method
