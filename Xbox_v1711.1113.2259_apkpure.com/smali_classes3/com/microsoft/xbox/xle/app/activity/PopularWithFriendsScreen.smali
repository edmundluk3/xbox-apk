.class public Lcom/microsoft/xbox/xle/app/activity/PopularWithFriendsScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "PopularWithFriendsScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/activity/PopularWithFriendsScreen$PopularWithFriendsAdapterProvider;
    }
.end annotation


# instance fields
.field protected adapterProvider:Lcom/microsoft/xbox/xle/app/activity/PopularWithFriendsScreen$PopularWithFriendsAdapterProvider;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/PopularWithFriendsScreen$PopularWithFriendsAdapterProvider;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/activity/PopularWithFriendsScreen$PopularWithFriendsAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/PopularWithFriendsScreen;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PopularWithFriendsScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/activity/PopularWithFriendsScreen$PopularWithFriendsAdapterProvider;

    .line 21
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    const-string v0, "Popular With Friends"

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 25
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 27
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/PopularWithFriendsScreen;->onCreateContentView()V

    .line 29
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/PopularWithFriendsScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/activity/PopularWithFriendsScreen$PopularWithFriendsAdapterProvider;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;-><init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 30
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->setAsPivotPane()V

    .line 31
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->onCreate()V

    .line 33
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/PopularWithFriendsScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 34
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 38
    const v0, 0x7f0301d7

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/PopularWithFriendsScreen;->setContentView(I)V

    .line 39
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    return v0
.end method
