.class Lcom/microsoft/xbox/smartglass/controls/InputV1;
.super Lcom/microsoft/xbox/smartglass/controls/Input;
.source "InputV1.java"


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V
    .locals 0
    .param p1, "canvasState"    # Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    .prologue
    .line 9
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/Input;-><init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    .line 10
    return-void
.end method


# virtual methods
.method protected send(ILjava/lang/String;)V
    .locals 3
    .param p1, "requestId"    # I
    .param p2, "json"    # Ljava/lang/String;

    .prologue
    .line 15
    :try_start_0
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->fromInt(I)Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lcom/microsoft/xbox/smartglass/controls/InputV1;->send(ILcom/microsoft/xbox/smartglass/controls/CanvasKey;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    .line 21
    :goto_0
    return-void

    .line 16
    :catch_0
    move-exception v0

    .line 17
    .local v0, "ex":Ljava/lang/NumberFormatException;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/InputV1;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v2, "Invalid input value."

    invoke-interface {v1, p1, v2}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0

    .line 18
    .end local v0    # "ex":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v0

    .line 19
    .local v0, "ex":Ljava/lang/IndexOutOfBoundsException;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/InputV1;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v2, "Invalid input value."

    invoke-interface {v1, p1, v2}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0
.end method
