.class public interface abstract Lcom/microsoft/xbox/data/service/oobe/OOBEService;
.super Ljava/lang/Object;
.source "OOBEService.java"


# virtual methods
.method public abstract createOrUpdateOOBESession(Ljava/lang/String;Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBERequestBody;)Lio/reactivex/Single;
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation

        .annotation runtime Lretrofit2/http/Path;
            value = "sessionId"
        .end annotation
    .end param
    .param p2    # Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBERequestBody;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBERequestBody;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/PUT;
        value = "/command/xtoken/v1/session/{sessionId}"
    .end annotation
.end method

.method public abstract getOOBESession(Ljava/lang/String;)Lio/reactivex/Single;
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation

        .annotation runtime Lretrofit2/http/Path;
            value = "sessionId"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/GET;
        value = "/command/xtoken/v1/session/{sessionId}"
    .end annotation
.end method
