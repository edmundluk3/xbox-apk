.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCNotifications$1;
.super Ljava/lang/Object;
.source "UTCNotifications.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCNotifications;->trackPushNotification(Lcom/microsoft/xbox/service/model/gcm/NotificationType;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$intent:Landroid/content/Intent;

.field final synthetic val$notifyType:Lcom/microsoft/xbox/service/model/gcm/NotificationType;


# direct methods
.method constructor <init>(Landroid/content/Intent;Lcom/microsoft/xbox/service/model/gcm/NotificationType;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCNotifications$1;->val$intent:Landroid/content/Intent;

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCNotifications$1;->val$notifyType:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    const/4 v7, 0x0

    const-wide/16 v12, 0x0

    .line 48
    iget-object v10, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCNotifications$1;->val$intent:Landroid/content/Intent;

    invoke-static {v10}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 49
    iget-object v10, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCNotifications$1;->val$intent:Landroid/content/Intent;

    invoke-virtual {v10}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 51
    .local v0, "extras":Landroid/os/Bundle;
    const-string v10, "com.microsoft.xbox.extra.IN_APP"

    invoke-virtual {v0, v10, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 52
    .local v1, "inApp":Z
    const-string v10, "com.microsoft.xbox.extra.TITLE_ID"

    invoke-virtual {v0, v10, v12, v13}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 53
    .local v8, "titleId":J
    const-string v10, "gcm.notification.body_loc_key"

    const-string v11, ""

    invoke-virtual {v0, v10, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 54
    .local v3, "messageStringId":Ljava/lang/String;
    const-string v10, "com.microsoft.xbox.extra.SESSION_ID"

    invoke-virtual {v0, v10, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 56
    .local v5, "multiplayerSessionId":Ljava/lang/String;
    new-instance v4, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v4}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 58
    .local v4, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const/4 v6, 0x0

    .line 59
    .local v6, "pageAction":Ljava/lang/String;
    sget-object v10, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCNotifications$2;->$SwitchMap$com$microsoft$xbox$service$model$gcm$NotificationType:[I

    iget-object v11, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCNotifications$1;->val$notifyType:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-virtual {v11}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    .line 179
    :cond_0
    :goto_0
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 180
    invoke-static {v6, v4}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 182
    :cond_1
    return-void

    .line 61
    :pswitch_0
    if-eqz v1, :cond_2

    const-string v6, "Push Online In-App"

    .line 62
    :goto_1
    goto :goto_0

    .line 61
    :cond_2
    const-string v6, "Push Online"

    goto :goto_1

    .line 64
    :pswitch_1
    if-eqz v1, :cond_3

    const-string v6, "Push Broadcast In-App"

    .line 65
    :goto_2
    goto :goto_0

    .line 64
    :cond_3
    const-string v6, "Push Broadcast"

    goto :goto_2

    .line 67
    :pswitch_2
    if-eqz v1, :cond_4

    const-string v6, "Push Message In-App"

    .line 68
    :goto_3
    goto :goto_0

    .line 67
    :cond_4
    const-string v6, "Push Message"

    goto :goto_3

    .line 70
    :pswitch_3
    const-string v10, "SessionRef"

    const-string v11, "com.microsoft.xbox.extra.HANDLE_ID"

    invoke-virtual {v0, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v10, v11}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 71
    const-string v10, "com.microsoft.xbox.extra.NOTIFICATION_SUBTYPE"

    invoke-virtual {v0, v10, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 72
    .local v2, "lfgSubtype":Ljava/lang/String;
    const/4 v10, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v11

    sparse-switch v11, :sswitch_data_0

    :cond_5
    move v7, v10

    :goto_4
    packed-switch v7, :pswitch_data_1

    goto :goto_0

    .line 74
    :pswitch_4
    const-string v6, "Push Notification - LFG Player Withdrawn"

    .line 75
    goto :goto_0

    .line 72
    :sswitch_0
    const-string v11, "hostHasConfirmedMemberWithdraw"

    invoke-virtual {v2, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    goto :goto_4

    :sswitch_1
    const-string v7, "hostHasNewApplicant"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    const/4 v7, 0x1

    goto :goto_4

    :sswitch_2
    const-string v7, "imminentLfgApplicantConfirmed"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    const/4 v7, 0x2

    goto :goto_4

    :sswitch_3
    const-string v7, "imminentLfgFilledWithoutApplicant"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    const/4 v7, 0x3

    goto :goto_4

    :sswitch_4
    const-string v7, "scheduledLfgApplicantConfirmed"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    const/4 v7, 0x4

    goto :goto_4

    :sswitch_5
    const-string v7, "scheduledLfgFilledWithoutApplicant"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    const/4 v7, 0x5

    goto :goto_4

    :sswitch_6
    const-string v7, "scheduledLfgCanceled"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    const/4 v7, 0x6

    goto :goto_4

    :sswitch_7
    const-string v7, "scheduledLfgExpired"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    const/4 v7, 0x7

    goto :goto_4

    .line 77
    :pswitch_5
    const-string v6, "Push Notification - LFG Player Interested"

    .line 78
    goto/16 :goto_0

    .line 80
    :pswitch_6
    const-string v6, "Push Notification - LFG Imminent Accepted"

    .line 81
    goto/16 :goto_0

    .line 83
    :pswitch_7
    const-string v6, "Push Notification - LFG Imminent Closed"

    .line 84
    goto/16 :goto_0

    .line 86
    :pswitch_8
    const-string v6, "Push Notification - LFG Scheduled Accepted"

    .line 87
    goto/16 :goto_0

    .line 89
    :pswitch_9
    const-string v6, "Push Notification - LFG Scheduled Full"

    .line 90
    goto/16 :goto_0

    .line 92
    :pswitch_a
    const-string v6, "Push Notification - LFG Scheduled Canceled"

    .line 93
    goto/16 :goto_0

    .line 95
    :pswitch_b
    const-string v6, "Push Notification - LFG Scheduled Expired"

    goto/16 :goto_0

    .line 100
    .end local v2    # "lfgSubtype":Ljava/lang/String;
    :pswitch_c
    const-string v6, "Push Notification - Clubs Joined Owned"

    .line 101
    const-string v7, "ClubId"

    const-string v10, "com.microsoft.xbox.extra.CLUB_ID"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v7, v10}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 104
    :pswitch_d
    const-string v6, "Push Notification - Clubs Promoted"

    .line 105
    const-string v7, "ClubId"

    const-string v10, "com.microsoft.xbox.extra.CLUB_ID"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v7, v10}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 108
    :pswitch_e
    const-string v6, "Push Notification - Clubs Moderator Left"

    .line 109
    const-string v7, "ClubId"

    const-string v10, "com.microsoft.xbox.extra.CLUB_ID"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v7, v10}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 112
    :pswitch_f
    const-string v6, "Push Notification - Clubs Invitation Request"

    .line 113
    const-string v7, "ClubId"

    const-string v10, "com.microsoft.xbox.extra.CLUB_ID"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v7, v10}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 116
    :pswitch_10
    const-string v6, "Push Notification - Clubs Reccommendations"

    .line 117
    const-string v7, "ClubId"

    const-string v10, "com.microsoft.xbox.extra.CLUB_ID"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v7, v10}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 120
    :pswitch_11
    const-string v6, "Push Notification - Clubs Invite"

    .line 121
    const-string v7, "ClubId"

    const-string v10, "com.microsoft.xbox.extra.CLUB_ID"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v7, v10}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 124
    :pswitch_12
    const-string v6, "Push Notification - Clubs Reports"

    .line 125
    const-string v7, "ClubId"

    const-string v10, "com.microsoft.xbox.extra.CLUB_ID"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v7, v10}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 128
    :pswitch_13
    const-string v6, "Push Notification - Clubs Transfer"

    .line 129
    const-string v7, "ClubId"

    const-string v10, "com.microsoft.xbox.extra.CLUB_ID"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v7, v10}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 132
    :pswitch_14
    const-string v6, "Push Notification - Clubs Joined"

    .line 133
    const-string v7, "ClubId"

    const-string v10, "com.microsoft.xbox.extra.CLUB_ID"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v7, v10}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 136
    :pswitch_15
    const-string v6, "Push Notification - Tournament Match Ready"

    .line 137
    cmp-long v7, v8, v12

    if-eqz v7, :cond_0

    .line 138
    const-string v7, "TitleId"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v4, v7, v10}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 142
    :pswitch_16
    iget-object v7, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCNotifications$1;->val$notifyType:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-static {v7, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCNotifications;->access$000(Lcom/microsoft/xbox/service/model/gcm/NotificationType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 143
    cmp-long v7, v8, v12

    if-eqz v7, :cond_0

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 144
    const-string v7, "TitleId"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v4, v7, v10}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 148
    :pswitch_17
    iget-object v7, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCNotifications$1;->val$notifyType:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-static {v7, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCNotifications;->access$000(Lcom/microsoft/xbox/service/model/gcm/NotificationType;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 149
    cmp-long v7, v8, v12

    if-eqz v7, :cond_0

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 150
    const-string v7, "TitleId"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v4, v7, v10}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 154
    :pswitch_18
    const-string v6, "Push Notification - Tournament Team Member Invite"

    .line 155
    cmp-long v7, v8, v12

    if-eqz v7, :cond_0

    .line 156
    const-string v7, "TitleId"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v4, v7, v10}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 160
    :pswitch_19
    const-string v6, "Push Notification - Tournament Team Member Join"

    .line 161
    cmp-long v7, v8, v12

    if-eqz v7, :cond_0

    .line 162
    const-string v7, "TitleId"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v4, v7, v10}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 166
    :pswitch_1a
    const-string v6, "Push Notification - Tournament Team Member Leave"

    .line 167
    cmp-long v7, v8, v12

    if-eqz v7, :cond_0

    .line 168
    const-string v7, "TitleId"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v4, v7, v10}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 172
    :pswitch_1b
    const-string v6, "PushNotification - Party invite"

    .line 173
    if-eqz v5, :cond_0

    .line 174
    const-string v7, "PartyId"

    invoke-virtual {v4, v7, v5}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 59
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
    .end packed-switch

    .line 72
    :sswitch_data_0
    .sparse-switch
        -0x698fe904 -> :sswitch_5
        -0x34dd573b -> :sswitch_7
        -0x2da769d4 -> :sswitch_3
        -0x262709cc -> :sswitch_1
        0x4e5580d -> :sswitch_2
        0x4fcb6f9 -> :sswitch_6
        0x4ffdc0dd -> :sswitch_4
        0x70ff44d1 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method
