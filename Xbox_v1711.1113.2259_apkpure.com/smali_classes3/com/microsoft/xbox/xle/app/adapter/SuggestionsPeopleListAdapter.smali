.class public Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "SuggestionsPeopleListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field private final inflater:Landroid/view/LayoutInflater;

.field private final primaryViewResourceId:I

.field private viewHolder:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;)V
    .locals 1
    .param p1, "cxt"    # Landroid/content/Context;
    .param p2, "rowViewResourceId"    # I
    .param p3, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->data:Ljava/util/List;

    .line 55
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 56
    iput p2, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->primaryViewResourceId:I

    .line 57
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    .line 58
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->data:Ljava/util/List;

    return-object v0
.end method

.method private getItemViewTypeInternal(I)Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 67
    if-eqz p1, :cond_0

    .line 68
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$1;->$SwitchMap$com$microsoft$xbox$toolkit$network$ListState:[I

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 79
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;->SPINNER_FILTER:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    :goto_0
    return-object v0

    .line 70
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;->LISTVIEW_ITEMS:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    goto :goto_0

    .line 72
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;->TEXT_ERROR:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    goto :goto_0

    .line 74
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;->TEXT_NO_CONTENT:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    goto :goto_0

    .line 76
    :pswitch_3
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;->TEXT_LOADING:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    goto :goto_0

    .line 68
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic lambda$onCreateViewHolder$0(Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;
    .param p1, "holder"    # Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;

    .prologue
    .line 95
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->getAdapterPosition()I

    move-result v1

    .line 96
    .local v1, "position":I
    if-eqz v1, :cond_0

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 98
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->data:Ljava/util/List;

    add-int/lit8 v3, v1, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;

    move-object v0, v2

    check-cast v0, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;

    .line 99
    .local v0, "item":Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;->getIsDummy()Z

    move-result v2

    if-nez v2, :cond_1

    .line 100
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->gotoGamerProfile(Lcom/microsoft/xbox/service/model/FollowersData;)V

    .line 109
    .end local v0    # "item":Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;
    :cond_0
    :goto_0
    return-void

    .line 101
    .restart local v0    # "item":Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;
    :cond_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;->getItemDummyType()Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_LINK_TO_FACEBOOK:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    if-ne v2, v3, :cond_2

    .line 102
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->login()V

    goto :goto_0

    .line 103
    :cond_2
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;->getItemDummyType()Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_INVITE_PHONE_CONTACTS:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    if-ne v2, v3, :cond_3

    .line 104
    new-instance v2, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->show()V

    goto :goto_0

    .line 105
    :cond_3
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;->getItemDummyType()Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_LINK_TO_PHONE_CONTACT:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    if-ne v2, v3, :cond_0

    .line 106
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;->linkUnlink()V

    goto :goto_0
.end method


# virtual methods
.method public addAll(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 137
    .local p1, "collection":Ljava/util/Collection;, "Ljava/util/Collection<+Lcom/microsoft/xbox/service/model/FollowersData;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->data:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 140
    :cond_0
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->data:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 134
    :cond_0
    return-void
.end method

.method public getItemCount()I
    .locals 2

    .prologue
    .line 144
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$1;->$SwitchMap$com$microsoft$xbox$toolkit$network$ListState:[I

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 155
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 149
    :pswitch_0
    const/4 v0, 0x2

    goto :goto_0

    .line 151
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->data:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 144
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->getItemViewTypeInternal(I)Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;->ordinal()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 37
    check-cast p1, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->onBindViewHolder(Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;I)V
    .locals 0
    .param p1, "holder"    # Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 121
    invoke-virtual {p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->updateUI(I)V

    .line 122
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;
    .locals 6
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    const/4 v5, 0x0

    .line 85
    const/4 v0, 0x0

    .line 86
    .local v0, "itemRootListener":Lcom/microsoft/xbox/toolkit/listeners/OnClickItemRootListener;, "Lcom/microsoft/xbox/toolkit/listeners/OnClickItemRootListener<Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;>;"
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;->values()[Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    move-result-object v3

    aget-object v1, v3, p2

    .line 88
    .local v1, "type":Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;
    sget-object v3, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$1;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$SuggestionsPeopleListAdapter$ViewType:[I

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 112
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->inflater:Landroid/view/LayoutInflater;

    const v4, 0x7f03021e

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 115
    .local v2, "v":Landroid/view/View;
    :goto_0
    new-instance v3, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;

    invoke-direct {v3, p0, v2, v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;Landroid/view/View;Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;Lcom/microsoft/xbox/toolkit/listeners/OnClickItemRootListener;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->viewHolder:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;

    .line 116
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->viewHolder:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;

    return-object v3

    .line 90
    .end local v2    # "v":Landroid/view/View;
    :pswitch_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->inflater:Landroid/view/LayoutInflater;

    const v4, 0x7f03021d

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 91
    .restart local v2    # "v":Landroid/view/View;
    goto :goto_0

    .line 93
    .end local v2    # "v":Landroid/view/View;
    :pswitch_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->inflater:Landroid/view/LayoutInflater;

    iget v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->primaryViewResourceId:I

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 94
    .restart local v2    # "v":Landroid/view/View;
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;)Lcom/microsoft/xbox/toolkit/listeners/OnClickItemRootListener;

    move-result-object v0

    .line 110
    goto :goto_0

    .line 88
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->viewHolder:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->viewHolder:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->removeSpinnerListener()V

    .line 128
    :cond_0
    return-void
.end method
