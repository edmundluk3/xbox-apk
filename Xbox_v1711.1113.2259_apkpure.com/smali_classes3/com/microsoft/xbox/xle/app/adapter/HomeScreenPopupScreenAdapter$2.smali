.class final Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$2;
.super Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;
.source "HomeScreenPopupScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->createPinScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter",
        "<",
        "Lcom/microsoft/xbox/service/model/pins/PinItem;",
        ">;"
    }
.end annotation


# instance fields
.field private currentPin:Lcom/microsoft/xbox/service/model/pins/PinItem;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;)V
    .locals 1

    .prologue
    .line 55
    .local p1, "viewModel":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase<Lcom/microsoft/xbox/service/model/pins/PinItem;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$1;)V

    return-void
.end method


# virtual methods
.method protected createAndBindHeader(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "data":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData<Lcom/microsoft/xbox/service/model/pins/PinItem;>;"
    const/4 v10, 0x4

    .line 60
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;->getHeaderData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/model/pins/PinItem;

    .line 61
    .local v3, "pin":Lcom/microsoft/xbox/service/model/pins/PinItem;
    if-eqz v3, :cond_1

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$2;->currentPin:Lcom/microsoft/xbox/service/model/pins/PinItem;

    if-eq v8, v3, :cond_1

    .line 62
    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$2;->currentPin:Lcom/microsoft/xbox/service/model/pins/PinItem;

    .line 64
    const v8, 0x7f0e06d8

    invoke-virtual {p0, v8}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$2;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 66
    .local v5, "root":Landroid/view/View;
    const v8, 0x7f0e06de

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 67
    .local v4, "provider":Landroid/widget/TextView;
    iget-object v8, v3, Lcom/microsoft/xbox/service/model/pins/PinItem;->ContentType:Ljava/lang/String;

    invoke-static {v8}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->access$200(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 68
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getProviderName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 71
    :cond_0
    const v8, 0x7f0e06df

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 72
    .local v6, "title":Landroid/widget/TextView;
    iget-object v8, v3, Lcom/microsoft/xbox/service/model/pins/PinItem;->Title:Ljava/lang/String;

    invoke-static {v6, v8, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 74
    const v8, 0x7f0e06dc

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 76
    .local v7, "titleImage":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getIsWebLink()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 77
    const v8, 0x7f0c00f6

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setBackgroundResource(I)V

    .line 78
    const v8, 0x7f070f64

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setText(I)V

    .line 94
    :goto_0
    const v8, 0x7f0e06e0

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 95
    .local v0, "companion":Landroid/widget/TextView;
    invoke-virtual {v5}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;->getHasState()Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->access$400(Landroid/content/res/Resources;Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;)Ljava/lang/String;

    move-result-object v2

    .line 96
    .local v2, "hasString":Ljava/lang/String;
    invoke-static {v0, v2, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 98
    .end local v0    # "companion":Landroid/widget/TextView;
    .end local v2    # "hasString":Ljava/lang/String;
    .end local v4    # "provider":Landroid/widget/TextView;
    .end local v5    # "root":Landroid/view/View;
    .end local v6    # "title":Landroid/widget/TextView;
    .end local v7    # "titleImage":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    :cond_1
    return-void

    .line 79
    .restart local v4    # "provider":Landroid/widget/TextView;
    .restart local v5    # "root":Landroid/view/View;
    .restart local v6    # "title":Landroid/widget/TextView;
    .restart local v7    # "titleImage":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    :cond_2
    iget-object v8, v3, Lcom/microsoft/xbox/service/model/pins/PinItem;->ImageUrl:Ljava/lang/String;

    if-eqz v8, :cond_3

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isMusicPlayList(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 80
    :cond_3
    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;->setUserBackground(Landroid/view/View;)V

    .line 81
    invoke-static {v3}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isMusicPlayList(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v8

    if-eqz v8, :cond_4

    const v8, 0x7f070fca

    :goto_1
    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setText(I)V

    goto :goto_0

    :cond_4
    const v8, 0x7f070fb5

    goto :goto_1

    .line 83
    :cond_5
    const v8, 0x7f0e001d

    new-instance v9, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$2$1;

    invoke-direct {v9, p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$2$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$2;Lcom/microsoft/xbox/service/model/pins/PinItem;)V

    invoke-virtual {v7, v8, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setTag(ILjava/lang/Object;)V

    .line 89
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;->getPlaceholderRid()I

    move-result v1

    .line 90
    .local v1, "defaultRid":I
    iget-object v8, v3, Lcom/microsoft/xbox/service/model/pins/PinItem;->ImageUrl:Ljava/lang/String;

    invoke-virtual {v7, v8, v1, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 91
    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->access$300(Landroid/widget/ImageView;)V

    goto :goto_0
.end method
