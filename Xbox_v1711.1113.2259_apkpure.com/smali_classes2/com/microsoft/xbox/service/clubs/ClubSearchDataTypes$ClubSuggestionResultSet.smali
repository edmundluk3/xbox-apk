.class public abstract Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;
.super Ljava/lang/Object;
.source "ClubSearchDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ClubSuggestionResultSet"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSuggestionResultSet$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSuggestionResultSet$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract results()Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestResult;",
            ">;"
        }
    .end annotation
.end method
