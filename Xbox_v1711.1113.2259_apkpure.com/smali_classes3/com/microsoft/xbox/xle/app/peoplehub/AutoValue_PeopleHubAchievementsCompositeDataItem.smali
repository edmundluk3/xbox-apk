.class final Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;
.super Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;
.source "AutoValue_PeopleHubAchievementsCompositeDataItem.java"


# instance fields
.field private final achievementItem:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            ">;"
        }
    .end annotation
.end field

.field private final isAchievementHeader:Z

.field private final isLeaderboardHeader:Z

.field private final leaderboardItem:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;


# direct methods
.method constructor <init>(ZZLcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;Landroid/util/Pair;)V
    .locals 0
    .param p1, "isLeaderboardHeader"    # Z
    .param p2, "isAchievementHeader"    # Z
    .param p3, "leaderboardItem"    # Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/util/Pair;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;",
            "Landroid/util/Pair",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p4, "achievementItem":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;-><init>()V

    .line 23
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;->isLeaderboardHeader:Z

    .line 24
    iput-boolean p2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;->isAchievementHeader:Z

    .line 25
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;->leaderboardItem:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;

    .line 26
    iput-object p4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;->achievementItem:Landroid/util/Pair;

    .line 27
    return-void
.end method


# virtual methods
.method public achievementItem()Landroid/util/Pair;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;->achievementItem:Landroid/util/Pair;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 63
    if-ne p1, p0, :cond_1

    .line 73
    :cond_0
    :goto_0
    return v1

    .line 66
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;

    if-eqz v3, :cond_5

    move-object v0, p1

    .line 67
    check-cast v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;

    .line 68
    .local v0, "that":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;
    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;->isLeaderboardHeader:Z

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;->isLeaderboardHeader()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;->isAchievementHeader:Z

    .line 69
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;->isAchievementHeader()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;->leaderboardItem:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;

    if-nez v3, :cond_3

    .line 70
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;->leaderboardItem()Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;->achievementItem:Landroid/util/Pair;

    if-nez v3, :cond_4

    .line 71
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;->achievementItem()Landroid/util/Pair;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 70
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;->leaderboardItem:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;->leaderboardItem()Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 71
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;->achievementItem:Landroid/util/Pair;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;->achievementItem()Landroid/util/Pair;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/util/Pair;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;
    :cond_5
    move v1, v2

    .line 73
    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v4, 0x0

    const v5, 0xf4243

    .line 78
    const/4 v0, 0x1

    .line 79
    .local v0, "h":I
    mul-int/2addr v0, v5

    .line 80
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;->isLeaderboardHeader:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 81
    mul-int/2addr v0, v5

    .line 82
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;->isAchievementHeader:Z

    if-eqz v1, :cond_1

    :goto_1
    xor-int/2addr v0, v2

    .line 83
    mul-int/2addr v0, v5

    .line 84
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;->leaderboardItem:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;

    if-nez v1, :cond_2

    move v1, v4

    :goto_2
    xor-int/2addr v0, v1

    .line 85
    mul-int/2addr v0, v5

    .line 86
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;->achievementItem:Landroid/util/Pair;

    if-nez v1, :cond_3

    :goto_3
    xor-int/2addr v0, v4

    .line 87
    return v0

    :cond_0
    move v1, v3

    .line 80
    goto :goto_0

    :cond_1
    move v2, v3

    .line 82
    goto :goto_1

    .line 84
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;->leaderboardItem:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;->hashCode()I

    move-result v1

    goto :goto_2

    .line 86
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;->achievementItem:Landroid/util/Pair;

    invoke-virtual {v1}, Landroid/util/Pair;->hashCode()I

    move-result v4

    goto :goto_3
.end method

.method public isAchievementHeader()Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;->isAchievementHeader:Z

    return v0
.end method

.method public isLeaderboardHeader()Z
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;->isLeaderboardHeader:Z

    return v0
.end method

.method public leaderboardItem()Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;->leaderboardItem:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PeopleHubAchievementsCompositeDataItem{isLeaderboardHeader="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;->isLeaderboardHeader:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isAchievementHeader="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;->isAchievementHeader:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", leaderboardItem="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;->leaderboardItem:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", achievementItem="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/AutoValue_PeopleHubAchievementsCompositeDataItem;->achievementItem:Landroid/util/Pair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
