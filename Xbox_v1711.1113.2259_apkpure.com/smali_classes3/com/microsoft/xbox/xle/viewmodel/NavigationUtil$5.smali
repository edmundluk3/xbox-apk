.class final Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$5;
.super Ljava/lang/Object;
.source "NavigationUtil.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->attachItemListeners(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

.field final synthetic val$bi:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

.field final synthetic val$item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

.field final synthetic val$nav:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;

.field final synthetic val$targetNavigator:Landroid/view/View$OnClickListener;

.field final synthetic val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;Landroid/view/View$OnClickListener;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;)V
    .locals 0

    .prologue
    .line 531
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$5;->val$nav:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$5;->val$targetNavigator:Landroid/view/View$OnClickListener;

    iput-object p3, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$5;->val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    iput-object p4, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$5;->val$item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iput-object p5, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$5;->val$authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    iput-object p6, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$5;->val$bi:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 534
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$13;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$NavigationUtil$NavigationGeneric:[I

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$5;->val$nav:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;->navigationGeneric:Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$NavigationGeneric;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 542
    :goto_0
    return-void

    .line 536
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$5;->val$targetNavigator:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 539
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$5;->val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$5;->val$item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$5;->val$authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$5;->val$bi:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->access$200(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;)V

    goto :goto_0

    .line 534
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
