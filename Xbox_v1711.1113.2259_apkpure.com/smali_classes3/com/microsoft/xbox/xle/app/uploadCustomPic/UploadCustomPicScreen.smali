.class public Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreen;
.super Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;
.source "UploadCustomPicScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;-><init>()V

    .line 13
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getContentScreenId()I
    .locals 1

    .prologue
    .line 33
    const v0, 0x7f03025b

    return v0
.end method

.method protected getErrorScreenId()I
    .locals 1

    .prologue
    .line 43
    const v0, 0x7f03025c

    return v0
.end method

.method protected getLoadingScreenId()I
    .locals 1

    .prologue
    .line 48
    const v0, 0x7f03025d

    return v0
.end method

.method protected getNoContentScreenId()I
    .locals 1

    .prologue
    .line 38
    const v0, 0x7f03025e

    return v0
.end method

.method protected getRootViewId()I
    .locals 1

    .prologue
    .line 28
    const v0, 0x7f03025f

    return v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 21
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/ui/Search/SwitchPanelScreen;->onCreate()V

    .line 22
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreen;->setShowUtilityBar(Z)V

    .line 23
    new-instance v0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/uploadCustomPic/UploadCustomPicScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 24
    return-void
.end method
