.class public Lcom/microsoft/xbox/smartglass/controls/GameDvr;
.super Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;
.source "GameDvr.java"


# static fields
.field public static final ComponentName:Ljava/lang/String; = "GameDvr"

.field private static final RecordPreviousMethod:Ljava/lang/String; = "SendMessage"


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V
    .locals 1
    .param p1, "canvasState"    # Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    .prologue
    .line 12
    const-string v0, "GameDvr"

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    .line 13
    const-string v0, "SendMessage"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/GameDvr;->registerMethod(Ljava/lang/String;)V

    .line 14
    return-void
.end method

.method private recordPrevious(ILjava/lang/String;)V
    .locals 5
    .param p1, "requestId"    # I
    .param p2, "json"    # Ljava/lang/String;

    .prologue
    .line 30
    :try_start_0
    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/JsonRecordPreviousMessage;

    invoke-direct {v1, p2}, Lcom/microsoft/xbox/smartglass/controls/JsonRecordPreviousMessage;-><init>(Ljava/lang/String;)V

    .line 31
    .local v1, "message":Lcom/microsoft/xbox/smartglass/controls/JsonRecordPreviousMessage;
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getGameDvr()Lcom/microsoft/xbox/smartglass/GameDvr;

    move-result-object v2

    iget v3, v1, Lcom/microsoft/xbox/smartglass/controls/JsonRecordPreviousMessage;->duration:I

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/smartglass/GameDvr;->recordPrevious(I)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 35
    .end local v1    # "message":Lcom/microsoft/xbox/smartglass/controls/JsonRecordPreviousMessage;
    :goto_0
    return-void

    .line 32
    :catch_0
    move-exception v0

    .line 33
    .local v0, "ex":Lorg/json/JSONException;
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/GameDvr;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid JSON. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, p1, v3}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic dispose()V
    .locals 0

    .prologue
    .line 7
    invoke-super {p0}, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->dispose()V

    return-void
.end method

.method public bridge synthetic getCurrentState()Lorg/json/JSONObject;
    .locals 1

    .prologue
    .line 7
    invoke-super {p0}, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->getCurrentState()Lorg/json/JSONObject;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic initialize(Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;)V
    .locals 0

    .prologue
    .line 7
    invoke-super {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->initialize(Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;)V

    return-void
.end method

.method public invoke(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "requestId"    # I
    .param p2, "methodName"    # Ljava/lang/String;
    .param p3, "json"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/smartglass/controls/GameDvr;->validateMethod(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 19
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/controls/GameDvr;->failRequestUnknownMethod(ILjava/lang/String;)V

    .line 26
    :cond_0
    :goto_0
    return-void

    .line 23
    :cond_1
    const-string v0, "SendMessage"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    invoke-direct {p0, p1, p3}, Lcom/microsoft/xbox/smartglass/controls/GameDvr;->recordPrevious(ILjava/lang/String;)V

    goto :goto_0
.end method
