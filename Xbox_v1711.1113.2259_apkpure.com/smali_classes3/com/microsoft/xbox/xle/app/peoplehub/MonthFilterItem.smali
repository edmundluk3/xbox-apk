.class public Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;
.super Ljava/lang/Object;
.source "MonthFilterItem.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;


# static fields
.field private static final DISPLAY_FORMATTER:Ljava/text/SimpleDateFormat;


# instance fields
.field private final month:Ljava/util/Calendar;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 14
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMMM yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;->DISPLAY_FORMATTER:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>(Ljava/util/Calendar;)V
    .locals 0
    .param p1, "month"    # Ljava/util/Calendar;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 21
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;->month:Ljava/util/Calendar;

    .line 22
    return-void
.end method


# virtual methods
.method public getDisplayName()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 32
    sget-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;->DISPLAY_FORMATTER:Ljava/text/SimpleDateFormat;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;->month:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMonth()Ljava/util/Calendar;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;->month:Ljava/util/Calendar;

    return-object v0
.end method

.method public getTelemetryName()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 38
    sget-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;->DISPLAY_FORMATTER:Ljava/text/SimpleDateFormat;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/MonthFilterItem;->month:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
