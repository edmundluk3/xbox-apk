.class public Lcom/microsoft/xbox/service/model/serialization/XBLMessageType;
.super Ljava/lang/Object;
.source "XBLMessageType.java"


# static fields
.field public static final CompetitionReminder:J = 0x5L

.field public static final CompetitionRequest:J = 0x6L

.field public static final FriendRequest:J = 0x2L

.field public static final GameInvite:J = 0x3L

.field public static final LiveMessage:J = 0x7L

.field public static final PartyInvite:J = 0xcL

.field public static final PersonalMessage:J = 0x8L

.field public static final QuickChatInvite:J = 0xaL

.field public static final TeamRecruit:J = 0x4L

.field public static final TitleCustom:J = 0x1L

.field public static final VideoChatInvite:J = 0xbL

.field public static final VideoMessage:J = 0x9L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
