.class Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DiscoverClubsViewHolder;
.super Ljava/lang/Object;
.source "PeopleScreenListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DiscoverClubsViewHolder"
.end annotation


# instance fields
.field hideButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08d9
    .end annotation
.end field

.field okButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08d8
    .end annotation
.end field

.field rootView:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08d7
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;Landroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 355
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DiscoverClubsViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 356
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 357
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 358
    return-void
.end method

.method static synthetic lambda$bindTo$0(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Landroid/view/View;)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 370
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->navigateToClubDiscovery()V

    return-void
.end method

.method static synthetic lambda$bindTo$1(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Landroid/view/View;)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 371
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->hideClubDiscoveryBlock()V

    return-void
.end method


# virtual methods
.method public bindTo(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)V
    .locals 5
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 361
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 363
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v2

    .line 364
    .local v2, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferredSecondaryColor()I

    move-result v0

    .line 365
    .local v0, "backgroundColor":I
    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v1

    .line 366
    .local v1, "buttonColor":I
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DiscoverClubsViewHolder;->rootView:Landroid/view/View;

    invoke-static {v3, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setBackgroundColorIfNotNull(Landroid/view/View;I)V

    .line 367
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DiscoverClubsViewHolder;->okButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v3, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setBackgroundColorIfNotNull(Landroid/view/View;I)V

    .line 368
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DiscoverClubsViewHolder;->hideButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {v3, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setBackgroundColorIfNotNull(Landroid/view/View;I)V

    .line 370
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DiscoverClubsViewHolder;->okButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DiscoverClubsViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 371
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DiscoverClubsViewHolder;->hideButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DiscoverClubsViewHolder$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 372
    return-void

    .line 364
    .end local v0    # "backgroundColor":I
    .end local v1    # "buttonColor":I
    :cond_0
    sget v0, Lcom/microsoft/xbox/service/model/ProfileModel;->DEFAULT_PROFILE_SECONDARY_COLOR:I

    goto :goto_0

    .line 365
    .restart local v0    # "backgroundColor":I
    :cond_1
    sget v1, Lcom/microsoft/xbox/service/model/ProfileModel;->DEFAULT_PROFILE_PRIMARY_COLOR:I

    goto :goto_1
.end method
