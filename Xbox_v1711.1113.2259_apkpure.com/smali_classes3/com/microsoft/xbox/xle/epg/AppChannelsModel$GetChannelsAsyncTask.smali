.class Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;
.super Lcom/microsoft/xbox/toolkit/XLEAsyncTask;
.source "AppChannelsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/epg/AppChannelsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetChannelsAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private items:[Lcom/microsoft/xbox/service/model/epg/EPListItem;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/AppChannelsModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/epg/AppChannelsModel;)V
    .locals 1

    .prologue
    .line 375
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/epg/AppChannelsModel;

    .line 376
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 373
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;->items:[Lcom/microsoft/xbox/service/model/epg/EPListItem;

    .line 377
    return-void
.end method


# virtual methods
.method protected doInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3

    .prologue
    .line 381
    const-string v1, "AppChannelsModel"

    const-string v2, "Start get channels"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->requestAppChannelLineups()Z

    move-result v0

    .line 383
    .local v0, "ret":Z
    if-nez v0, :cond_0

    .line 384
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 388
    :goto_0
    return-object v1

    .line 386
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPListsServiceManager;->getAppChannelFavoritesList()[Lcom/microsoft/xbox/service/model/epg/EPListItem;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;->items:[Lcom/microsoft/xbox/service/model/epg/EPListItem;

    .line 388
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 372
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;->doInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v3, 0x1

    .line 397
    const-string v0, "AppChannelsModel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Done get channels "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/epg/AppChannelsModel;

    invoke-static {v0, v3, v3}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->access$000(Lcom/microsoft/xbox/xle/epg/AppChannelsModel;ZZ)V

    .line 408
    :goto_0
    return-void

    .line 401
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;->items:[Lcom/microsoft/xbox/service/model/epg/EPListItem;

    if-nez v0, :cond_1

    .line 402
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/epg/AppChannelsModel;

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/microsoft/xbox/service/model/epg/EPListItem;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->access$102(Lcom/microsoft/xbox/xle/epg/AppChannelsModel;[Lcom/microsoft/xbox/service/model/epg/EPListItem;)[Lcom/microsoft/xbox/service/model/epg/EPListItem;

    .line 406
    :goto_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/epg/AppChannelsModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->access$200(Lcom/microsoft/xbox/xle/epg/AppChannelsModel;)V

    goto :goto_0

    .line 404
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/epg/AppChannelsModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;->items:[Lcom/microsoft/xbox/service/model/epg/EPListItem;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->access$102(Lcom/microsoft/xbox/xle/epg/AppChannelsModel;[Lcom/microsoft/xbox/service/model/epg/EPListItem;)[Lcom/microsoft/xbox/service/model/epg/EPListItem;

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 372
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 393
    return-void
.end method
