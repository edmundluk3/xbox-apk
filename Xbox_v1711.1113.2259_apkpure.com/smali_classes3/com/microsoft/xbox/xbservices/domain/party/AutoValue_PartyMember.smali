.class final Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMember;
.super Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;
.source "AutoValue_PartyMember.java"


# instance fields
.field private volatile currentState:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;


# direct methods
.method constructor <init>(Ljava/lang/String;ZZLcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "rosterKey$"    # Ljava/lang/String;
    .param p2, "isHost$"    # Z
    .param p3, "isTalking$"    # Z
    .param p4, "multiplayerMember$"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;
    .param p5, "privacyPermissions$"    # Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;
    .param p6, "isSelfMuted$"    # Ljava/lang/Boolean;

    .prologue
    .line 15
    invoke-direct/range {p0 .. p6}, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;-><init>(Ljava/lang/String;ZZLcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMember;Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionValues;Ljava/lang/Boolean;)V

    .line 16
    return-void
.end method


# virtual methods
.method public currentState()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;
    .locals 2

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMember;->currentState:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    if-nez v0, :cond_1

    .line 21
    monitor-enter p0

    .line 22
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMember;->currentState:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    if-nez v0, :cond_0

    .line 23
    invoke-super {p0}, Lcom/microsoft/xbox/xbservices/domain/party/$AutoValue_PartyMember;->currentState()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMember;->currentState:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMember;->currentState:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    if-nez v0, :cond_0

    .line 25
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "currentState() cannot return null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 28
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 30
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMember;->currentState:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember$MemberChatState;

    return-object v0
.end method
