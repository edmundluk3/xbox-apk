.class Lcom/microsoft/xbox/service/model/clubs/ClubModel$OwnerKeepClubTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ClubModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/clubs/ClubModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OwnerKeepClubTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountsService:Lcom/microsoft/xbox/service/clubs/IClubAccountsService;

.field private final resultAction:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1749
    .local p2, "resultAction":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/toolkit/AsyncActionStatus;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$OwnerKeepClubTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 1750
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$OwnerKeepClubTask;->resultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 1751
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getClubAccountsService()Lcom/microsoft/xbox/service/clubs/IClubAccountsService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$OwnerKeepClubTask;->accountsService:Lcom/microsoft/xbox/service/clubs/IClubAccountsService;

    .line 1752
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 1756
    const/4 v0, 0x0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 6

    .prologue
    .line 1770
    const/4 v1, 0x0

    .line 1773
    .local v1, "success":Z
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$OwnerKeepClubTask;->accountsService:Lcom/microsoft/xbox/service/clubs/IClubAccountsService;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$OwnerKeepClubTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v4

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;->Owner:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;

    invoke-interface {v2, v4, v5, v3}, Lcom/microsoft/xbox/service/clubs/IClubAccountsService;->keepClubForActor(JLcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;)Z
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1778
    :goto_0
    if-eqz v1, :cond_0

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    :goto_1
    return-object v2

    .line 1774
    :catch_0
    move-exception v0

    .line 1775
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->access$500()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Failed to keep club"

    invoke-static {v2, v3, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1778
    .end local v0    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_1
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1744
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$OwnerKeepClubTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 1765
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1744
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$OwnerKeepClubTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 1761
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 1787
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$OwnerKeepClubTask;->this$0:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->access$1200(Lcom/microsoft/xbox/service/model/clubs/ClubModel;)V

    .line 1788
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$OwnerKeepClubTask;->resultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 1789
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1744
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$OwnerKeepClubTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 1783
    return-void
.end method
