.class public Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "SuggestionsPeopleListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SuggestionsPersonViewHolder"
.end annotation


# instance fields
.field private addToFriendButtton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private facebookImageMD:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private filterSpinner:Landroid/widget/Spinner;

.field private gamerpicImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private gamertagTextView:Landroid/widget/TextView;

.field private linkToFacebookItem:Landroid/view/View;

.field private linkToPhoneContactSubTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private linkToPhoneContactTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private linkToPhoneContactsItem:Landroid/view/View;

.field private final onClickItemRootListener:Lcom/microsoft/xbox/toolkit/listeners/OnClickItemRootListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/listeners/OnClickItemRootListener",
            "<",
            "Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;",
            ">;"
        }
    .end annotation
.end field

.field private persionItemView:Landroid/view/View;

.field private presenceImageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

.field private realNameTextView:Landroid/widget/TextView;

.field private recommendationIconImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private recommendationIconText:Landroid/view/View;

.field private secondLineTextView:Landroid/widget/TextView;

.field private textView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;

.field private viewType:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;Landroid/view/View;Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;Lcom/microsoft/xbox/toolkit/listeners/OnClickItemRootListener;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;
    .param p2, "itemView"    # Landroid/view/View;
    .param p3, "viewType"    # Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;",
            "Lcom/microsoft/xbox/toolkit/listeners/OnClickItemRootListener",
            "<",
            "Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 180
    .local p4, "itemRootListener":Lcom/microsoft/xbox/toolkit/listeners/OnClickItemRootListener;, "Lcom/microsoft/xbox/toolkit/listeners/OnClickItemRootListener<Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;

    .line 181
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 182
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->viewType:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    .line 183
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->inflateListItemView()V

    .line 184
    iput-object p4, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->onClickItemRootListener:Lcom/microsoft/xbox/toolkit/listeners/OnClickItemRootListener;

    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    return-void
.end method

.method static synthetic lambda$inflateListItemView$0(Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;Landroid/view/View;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->getAdapterPosition()I

    move-result v1

    .line 212
    .local v1, "position":I
    if-eqz v1, :cond_0

    .line 213
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;)Ljava/util/List;

    move-result-object v2

    add-int/lit8 v3, v1, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;

    move-object v0, v2

    check-cast v0, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;

    .line 214
    .local v0, "item":Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;->getIsDummy()Z

    move-result v2

    if-nez v2, :cond_0

    .line 215
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->addToFriend(Lcom/microsoft/xbox/service/model/FollowersData;)V

    .line 218
    .end local v0    # "item":Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;
    :cond_0
    return-void
.end method

.method private updateListViewItemUI(Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;)V
    .locals 9
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;

    .prologue
    const v8, 0x7f020059

    const/4 v4, 0x1

    const/4 v6, 0x4

    const/4 v5, 0x0

    const/16 v7, 0x8

    .line 272
    if-eqz p1, :cond_2

    .line 273
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;->getIsDummy()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 275
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;->getItemDummyType()Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_LINK_TO_FACEBOOK:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    if-ne v3, v4, :cond_3

    .line 276
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->persionItemView:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 278
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;->FacebookFriend:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;->name()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/xle/app/FriendFinderSettings$IconImageSize;->MEDIUM:Lcom/microsoft/xbox/xle/app/FriendFinderSettings$IconImageSize;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/FriendFinderSettings;->getIconBySize(Ljava/lang/String;Lcom/microsoft/xbox/xle/app/FriendFinderSettings$IconImageSize;)Ljava/lang/String;

    move-result-object v0

    .line 279
    .local v0, "facebookImageMd":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->facebookImageMD:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    .line 280
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->facebookImageMD:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 282
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->linkToFacebookItem:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 283
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->linkToPhoneContactsItem:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 306
    .end local v0    # "facebookImageMd":Ljava/lang/String;
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->addToFriendButtton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 335
    :cond_2
    :goto_1
    return-void

    .line 284
    :cond_3
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;->getItemDummyType()Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_LINK_TO_PHONE_CONTACT:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    if-ne v3, v4, :cond_5

    .line 285
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->gamerpicImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    if-eqz v3, :cond_4

    .line 286
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->gamerpicImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v3, v8}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageResource(I)V

    .line 288
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->persionItemView:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 289
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->presenceImageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    invoke-static {v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 290
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->realNameTextView:Landroid/widget/TextView;

    invoke-static {v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 291
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->gamertagTextView:Landroid/widget/TextView;

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070568

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 292
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->secondLineTextView:Landroid/widget/TextView;

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070538

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 293
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->linkToPhoneContactsItem:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 294
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->linkToFacebookItem:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 295
    :cond_5
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;->getItemDummyType()Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_INVITE_PHONE_CONTACTS:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    if-ne v3, v4, :cond_1

    .line 296
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->gamerpicImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    if-eqz v3, :cond_6

    .line 297
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->gamerpicImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v3, v8}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageResource(I)V

    .line 299
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->persionItemView:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 300
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->presenceImageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setVisibility(I)V

    .line 301
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->linkToPhoneContactTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070e29

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 302
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->linkToPhoneContactSubTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070e28

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 303
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->linkToPhoneContactsItem:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 304
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->linkToFacebookItem:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 308
    :cond_7
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->persionItemView:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 309
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->linkToFacebookItem:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 310
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->linkToPhoneContactsItem:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 311
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->addToFriendButtton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 312
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->gamerpicImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    if-eqz v3, :cond_8

    .line 313
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;->getGamerPicUrl()Ljava/lang/String;

    move-result-object v1

    .line 314
    .local v1, "gamerPicUrl":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 315
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->gamerpicImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 319
    .end local v1    # "gamerPicUrl":Ljava/lang/String;
    :cond_8
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->gamertagTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;->getGamertag()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 320
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->realNameTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;->getGamerRealName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 322
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->presenceImageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    invoke-static {v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 325
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->secondLineTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;->getRecommendationFirstReason()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 326
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;->getRecommendationType()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;->name()Ljava/lang/String;

    move-result-object v3

    sget-object v6, Lcom/microsoft/xbox/xle/app/FriendFinderSettings$IconImageSize;->SMALL:Lcom/microsoft/xbox/xle/app/FriendFinderSettings$IconImageSize;

    invoke-static {v3, v6}, Lcom/microsoft/xbox/xle/app/FriendFinderSettings;->getIconBySize(Ljava/lang/String;Lcom/microsoft/xbox/xle/app/FriendFinderSettings$IconImageSize;)Ljava/lang/String;

    move-result-object v2

    .line 327
    .local v2, "icon":Ljava/lang/String;
    if-eqz v2, :cond_9

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->recommendationIconImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v3, :cond_9

    .line 328
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->recommendationIconImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 331
    :cond_9
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->recommendationIconImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-eqz v2, :cond_a

    move v3, v4

    :goto_2
    invoke-static {v6, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 332
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->recommendationIconText:Landroid/view/View;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;->getRecommendationType()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;->PhoneContact:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationType;

    if-ne v6, v7, :cond_b

    :goto_3
    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    goto/16 :goto_1

    :cond_a
    move v3, v5

    .line 331
    goto :goto_2

    :cond_b
    move v4, v5

    .line 332
    goto :goto_3
.end method


# virtual methods
.method public inflateListItemView()V
    .locals 2

    .prologue
    .line 190
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$1;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$SuggestionsPeopleListAdapter$ViewType:[I

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->viewType:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 223
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0a84

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->textView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 192
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0a83

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->filterSpinner:Landroid/widget/Spinner;

    goto :goto_0

    .line 195
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e08de

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->linkToFacebookItem:Landroid/view/View;

    .line 196
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e08e2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->linkToPhoneContactsItem:Landroid/view/View;

    .line 197
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e08e5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->linkToPhoneContactTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 198
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e08e6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->linkToPhoneContactSubTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 199
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e08df

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->facebookImageMD:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 200
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0a85

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->persionItemView:Landroid/view/View;

    .line 201
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0a87

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->gamerpicImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 202
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0a8c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->gamertagTextView:Landroid/widget/TextView;

    .line 203
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0a8d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->realNameTextView:Landroid/widget/TextView;

    .line 204
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0a8e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->recommendationIconImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 205
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0a8f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->recommendationIconText:Landroid/view/View;

    .line 206
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0a90

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->secondLineTextView:Landroid/widget/TextView;

    .line 207
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0a89

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->presenceImageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 208
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0a8a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->addToFriendButtton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 209
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->addToFriendButtton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->addToFriendButtton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 190
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 339
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->onClickItemRootListener:Lcom/microsoft/xbox/toolkit/listeners/OnClickItemRootListener;

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->onClickItemRootListener:Lcom/microsoft/xbox/toolkit/listeners/OnClickItemRootListener;

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/toolkit/listeners/OnClickItemRootListener;->onClick(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 342
    :cond_0
    return-void
.end method

.method public removeSpinnerListener()V
    .locals 2

    .prologue
    .line 345
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->filterSpinner:Landroid/widget/Spinner;

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->filterSpinner:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 348
    :cond_0
    return-void
.end method

.method public updateUI(I)V
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 230
    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$1;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$SuggestionsPeopleListAdapter$ViewType:[I

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->viewType:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 266
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->textView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v3, 0x7f07013a

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;I)V

    .line 269
    :cond_0
    :goto_0
    return-void

    .line 232
    :pswitch_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->filterSpinner:Landroid/widget/Spinner;

    if-eqz v2, :cond_0

    .line 233
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x1090008

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;

    .line 235
    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->getCurrentFilters()Ljava/util/List;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 236
    .local v1, "spinnerAdapter":Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;, "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter<Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;>;"
    const v2, 0x7f03020a

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->setDropDownViewResource(I)V

    .line 237
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->filterSpinner:Landroid/widget/Spinner;

    invoke-virtual {v2, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 238
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->filterSpinner:Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel;->getSuggestionsFilter()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 239
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->filterSpinner:Landroid/widget/Spinner;

    new-instance v3, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder$1;

    invoke-direct {v3, p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;)V

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto :goto_0

    .line 256
    .end local v1    # "spinnerAdapter":Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;, "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter<Lcom/microsoft/xbox/xle/viewmodel/SuggestionsPeopleScreenViewModel$SuggestionsPeopleFilter;>;"
    :pswitch_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;)Ljava/util/List;

    move-result-object v2

    add-int/lit8 v3, p1, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;

    move-object v0, v2

    check-cast v0, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;

    .line 257
    .local v0, "item":Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->updateListViewItemUI(Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;)V

    goto :goto_0

    .line 260
    .end local v0    # "item":Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;
    :pswitch_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->textView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v3, 0x7f070a50

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;I)V

    goto :goto_0

    .line 263
    :pswitch_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$SuggestionsPersonViewHolder;->textView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const v3, 0x7f0704c4

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;I)V

    goto :goto_0

    .line 230
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
