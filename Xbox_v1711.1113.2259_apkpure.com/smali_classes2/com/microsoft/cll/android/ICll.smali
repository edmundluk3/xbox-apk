.class public interface abstract Lcom/microsoft/cll/android/ICll;
.super Ljava/lang/Object;
.source "ICll.java"


# virtual methods
.method public abstract SubscribeCllEvents(Lcom/microsoft/cll/android/ICllEvents;)V
.end method

.method public abstract getAppUserId()Ljava/lang/String;
.end method

.method public varargs abstract log(Lcom/microsoft/cll/android/PreSerializedEvent;Ljava/util/List;[Lcom/microsoft/cll/android/EventSensitivity;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/cll/android/PreSerializedEvent;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;[",
            "Lcom/microsoft/cll/android/EventSensitivity;",
            ")V"
        }
    .end annotation
.end method

.method public varargs abstract log(Lcom/microsoft/telemetry/Base;Ljava/util/List;[Lcom/microsoft/cll/android/EventSensitivity;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/telemetry/Base;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;[",
            "Lcom/microsoft/cll/android/EventSensitivity;",
            ")V"
        }
    .end annotation
.end method

.method public abstract log(Lcom/microsoft/telemetry/Base;Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/telemetry/Base;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public varargs abstract log(Lcom/microsoft/telemetry/Base;[Lcom/microsoft/cll/android/EventSensitivity;)V
.end method

.method public abstract pause()V
.end method

.method public abstract resume()V
.end method

.method public abstract send()V
.end method

.method public abstract setAppUserId(Ljava/lang/String;)V
.end method

.method public abstract setDebugVerbosity(Lcom/microsoft/cll/android/Verbosity;)V
.end method

.method public abstract setEndpointUrl(Ljava/lang/String;)V
.end method

.method public abstract setExperimentId(Ljava/lang/String;)V
.end method

.method public abstract setXuidCallback(Lcom/microsoft/cll/android/ITicketCallback;)V
.end method

.method public abstract start()V
.end method

.method public abstract stop()V
.end method

.method public abstract synchronize()V
.end method

.method public abstract useLegacyCS(Z)V
.end method
