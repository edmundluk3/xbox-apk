.class Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$GetRecommendationsAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ClubDiscoveryScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetRecommendationsAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;)V
    .locals 0

    .prologue
    .line 259
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$GetRecommendationsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$1;

    .prologue
    .line 259
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$GetRecommendationsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 263
    const/4 v0, 0x1

    return v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 259
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$GetRecommendationsAsyncTask;->loadDataInBackground()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected loadDataInBackground()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation

    .prologue
    .line 277
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->access$200()Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->loadRecommendedClubs()Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 259
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$GetRecommendationsAsyncTask;->onError()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation

    .prologue
    .line 272
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 268
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 259
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$GetRecommendationsAsyncTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 287
    .local p1, "clubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$GetRecommendationsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->access$302(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;Z)Z

    .line 288
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$GetRecommendationsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->access$400(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;Ljava/util/List;)V

    .line 289
    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 282
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel$GetRecommendationsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;->access$302(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryScreenViewModel;Z)Z

    .line 283
    return-void
.end method
