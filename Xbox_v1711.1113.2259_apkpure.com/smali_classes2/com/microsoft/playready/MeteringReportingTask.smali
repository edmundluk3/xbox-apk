.class public Lcom/microsoft/playready/MeteringReportingTask;
.super Ljava/util/concurrent/FutureTask;
.source "MeteringReportingTask.java"

# interfaces
.implements Lcom/microsoft/playready/IMeteringReportingTask;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/FutureTask",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Lcom/microsoft/playready/IMeteringReportingTask;"
    }
.end annotation


# instance fields
.field private mListenersNotified:Z

.field private mMeteringreporterListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/playready/IMeteringReporterListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/playready/MeteringReportingWorker;)V
    .locals 1
    .param p1, "worker"    # Lcom/microsoft/playready/MeteringReportingWorker;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/playready/MeteringReportingTask;->mMeteringreporterListeners:Ljava/util/ArrayList;

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/playready/MeteringReportingTask;->mListenersNotified:Z

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/MeteringReportingTask;->mMeteringreporterListeners:Ljava/util/ArrayList;

    .line 25
    return-void
.end method


# virtual methods
.method public addMeteringReporterListener(Lcom/microsoft/playready/IMeteringReporterListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/playready/IMeteringReporterListener;

    .prologue
    .line 50
    monitor-enter p0

    .line 52
    :try_start_0
    iget-boolean v0, p0, Lcom/microsoft/playready/MeteringReportingTask;->mListenersNotified:Z

    if-nez v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/microsoft/playready/MeteringReportingTask;->mMeteringreporterListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 50
    :goto_0
    monitor-exit p0

    .line 61
    return-void

    .line 58
    :cond_0
    invoke-interface {p1, p0}, Lcom/microsoft/playready/IMeteringReporterListener;->onReportMeteringCompleted(Lcom/microsoft/playready/IMeteringReportingTask;)V

    goto :goto_0

    .line 50
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected done()V
    .locals 3

    .prologue
    .line 30
    monitor-enter p0

    .line 32
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/playready/MeteringReportingTask;->mMeteringreporterListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 37
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/playready/MeteringReportingTask;->mListenersNotified:Z

    .line 30
    monitor-exit p0

    .line 39
    return-void

    .line 32
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/IMeteringReporterListener;

    .line 34
    .local v0, "listener":Lcom/microsoft/playready/IMeteringReporterListener;
    invoke-interface {v0, p0}, Lcom/microsoft/playready/IMeteringReporterListener;->onReportMeteringCompleted(Lcom/microsoft/playready/IMeteringReportingTask;)V

    goto :goto_0

    .line 30
    .end local v0    # "listener":Lcom/microsoft/playready/IMeteringReporterListener;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getResponseCustomData()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/microsoft/playready/MeteringReportingTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public removeMeteringReporterListener(Lcom/microsoft/playready/IMeteringReporterListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/playready/IMeteringReporterListener;

    .prologue
    .line 66
    monitor-enter p0

    .line 68
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/MeteringReportingTask;->mMeteringreporterListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 66
    monitor-exit p0

    .line 70
    return-void

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
