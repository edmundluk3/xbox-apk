.class Lcom/microsoft/xbox/xle/epg/TvStreamer$2;
.super Ljava/lang/Object;
.source "TvStreamer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/epg/TvStreamer;-><init>(Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/epg/TvStreamer;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/epg/TvStreamer;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$2;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 174
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-ne v0, v1, :cond_0

    .line 175
    const-string v0, "TvStreamer"

    const-string v1, "Stream resume requested"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/TvStreamer$2;->this$0:Lcom/microsoft/xbox/xle/epg/TvStreamer;

    sget-object v1, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->IDLE:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->setStreamerState(Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;)V

    .line 177
    invoke-static {v2, v2}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->streamChannel(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    :goto_0
    return-void

    .line 179
    :cond_0
    const-string v0, "TvStreamer"

    const-string v1, "Stream resume requested, but no connection; launching connection dialog"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showConnectDialog()V

    goto :goto_0
.end method
