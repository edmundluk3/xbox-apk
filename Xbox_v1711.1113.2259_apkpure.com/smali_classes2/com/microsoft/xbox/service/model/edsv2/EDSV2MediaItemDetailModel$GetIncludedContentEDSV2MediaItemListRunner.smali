.class Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetIncludedContentEDSV2MediaItemListRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "EDSV2MediaItemDetailModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetIncludedContentEDSV2MediaItemListRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/ArrayList",
        "<TU;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;)V
    .locals 0

    .prologue
    .line 885
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetIncludedContentEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetIncludedContentEDSV2MediaItemListRunner;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetIncludedContentEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$1;

    .prologue
    .line 885
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetIncludedContentEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetIncludedContentEDSV2MediaItemListRunner;"
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetIncludedContentEDSV2MediaItemListRunner;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 885
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetIncludedContentEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetIncludedContentEDSV2MediaItemListRunner;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetIncludedContentEDSV2MediaItemListRunner;->buildData()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<TU;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 892
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetIncludedContentEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetIncludedContentEDSV2MediaItemListRunner;"
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetIncludedContentEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    iget-boolean v1, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->IsBundle:Z

    if-eqz v1, :cond_0

    .line 893
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetIncludedContentEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetIncludedContentEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBundledProductsIds()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/service/store/IStoreService;->getProductsReducedInfoFromIds(Ljava/util/List;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v0

    .line 894
    .local v0, "list":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetIncludedContentEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getListFromBigCatProduct(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;)Ljava/util/ArrayList;

    move-result-object v1

    return-object v1

    .line 897
    .end local v0    # "list":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    :cond_0
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0x2198

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v1
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 907
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetIncludedContentEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetIncludedContentEDSV2MediaItemListRunner;"
    const-wide/16 v0, 0x2198

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<TU;>;>;)V"
        }
    .end annotation

    .prologue
    .line 902
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetIncludedContentEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetIncludedContentEDSV2MediaItemListRunner;"
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<TU;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetIncludedContentEDSV2MediaItemListRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->access$900(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 903
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 888
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetIncludedContentEDSV2MediaItemListRunner;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>.GetIncludedContentEDSV2MediaItemListRunner;"
    return-void
.end method
