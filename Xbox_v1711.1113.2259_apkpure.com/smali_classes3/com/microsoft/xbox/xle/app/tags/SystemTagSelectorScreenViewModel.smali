.class public Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;
.super Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;
.source "SystemTagSelectorScreenViewModel.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;


# instance fields
.field private final tagGroups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;->TAG:Ljava/lang/String;

    .line 27
    sget-object v0, Lcom/microsoft/xbox/service/model/SocialTagModel;->INSTANCE:Lcom/microsoft/xbox/service/model/SocialTagModel;

    sput-object v0, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;->socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;->tagGroups:Ljava/util/List;

    .line 32
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 33
    return-void
.end method

.method private onSocialTagsChanged(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 81
    sget-object v0, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 94
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;->updateAdapter()V

    .line 95
    return-void

    .line 85
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;->updateTags()V

    goto :goto_0

    .line 89
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Social tags changed to invalid state. Using old data"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 81
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private updateTags()V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;->tags:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;->tags:Ljava/util/List;

    sget-object v1, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;->socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/SocialTagModel;->getSystemTags()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;->tagGroups:Ljava/util/List;

    sget-object v1, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;->socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/SocialTagModel;->getSystemTagGroups()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;->tagGroups:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 102
    return-void

    .line 101
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method


# virtual methods
.method public getErrorString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 57
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0706fe

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNoContentString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 52
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0706ff

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSystemTagListWithHeaders()Ljava/util/List;
    .locals 7
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;->getTagGroups()Ljava/util/List;

    move-result-object v3

    .line 116
    .local v3, "tagGroups":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 118
    .local v0, "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;

    .line 119
    .local v2, "tagGroup":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;->groupName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;->tags()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;

    .line 121
    .local v1, "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 125
    .end local v1    # "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
    .end local v2    # "tagGroup":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;
    :cond_1
    return-object v0
.end method

.method public getTagGroups()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;->tagGroups:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;->socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SocialTagModel;->getIsLoading()Z

    move-result v0

    return v0
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 62
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 63
    sget-object v0, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;->socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/SocialTagModel;->loadSystemTagsAsync(Z)V

    .line 64
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;->socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SocialTagModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 38
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;->socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SocialTagModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 43
    return-void
.end method

.method public setTagSelectedListener(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;)V
    .locals 1
    .param p1, "tagSelectedListener"    # Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v0, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenAdapter;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenAdapter;->setSelectedTagListener(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;)V

    .line 107
    return-void
.end method

.method public updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 68
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    move-object v0, v2

    .line 69
    .local v0, "result":Lcom/microsoft/xbox/service/model/UpdateData;
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 70
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v1

    .line 72
    .local v1, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v2, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 78
    .end local v1    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :cond_0
    :goto_1
    return-void

    .line 68
    .end local v0    # "result":Lcom/microsoft/xbox/service/model/UpdateData;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 74
    .restart local v0    # "result":Lcom/microsoft/xbox/service/model/UpdateData;
    .restart local v1    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :pswitch_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/tags/SystemTagSelectorScreenViewModel;->onSocialTagsChanged(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    goto :goto_1

    .line 72
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
