.class public final enum Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;
.super Ljava/lang/Enum;
.source "GcmModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/gcm/GcmModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ChatNotificationSetting"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

.field public static final enum AllMessages:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

.field public static final enum DirectMention:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

.field public static final enum None:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 780
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    const-string v1, "AllMessages"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->AllMessages:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    .line 781
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    const-string v1, "DirectMention"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->DirectMention:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    .line 782
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    const-string v1, "None"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->None:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    .line 779
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->AllMessages:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->DirectMention:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->None:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->$VALUES:[Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 779
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 779
    const-class v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;
    .locals 1

    .prologue
    .line 779
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->$VALUES:[Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    return-object v0
.end method
