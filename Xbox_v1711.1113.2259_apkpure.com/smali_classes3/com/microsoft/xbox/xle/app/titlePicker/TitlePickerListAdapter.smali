.class public Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "TitlePickerListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;,
        Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$SelectionListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final selectionListener:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$SelectionListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$SelectionListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "selectionListener"    # Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$SelectionListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 30
    const v0, 0x7f030233

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 31
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 33
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;->selectionListener:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$SelectionListener;

    .line 34
    return-void
.end method

.method static synthetic lambda$getView$0(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 55
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->toggleSelection()V

    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;->selectionListener:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$SelectionListener;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$SelectionListener;->onSelectedChanged(Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;->notifyDataSetChanged()V

    .line 62
    :goto_0
    return-void

    .line 60
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->toggleSelection()V

    goto :goto_0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    .line 40
    if-nez p2, :cond_0

    .line 41
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030233

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 42
    new-instance v1, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;

    const/4 v2, 0x0

    invoke-direct {v1, p2, v2}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;-><init>(Landroid/view/View;Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$1;)V

    .line 43
    .local v1, "viewHolder":Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 48
    :goto_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;

    .line 50
    .local v0, "item":Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    if-eqz v0, :cond_1

    .line 51
    invoke-virtual {p2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 52
    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;->access$100(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;)V

    .line 54
    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter;Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    :goto_1
    return-object p2

    .line 45
    .end local v0    # "item":Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    .end local v1    # "viewHolder":Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;

    .restart local v1    # "viewHolder":Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerListAdapter$ViewHolder;
    goto :goto_0

    .line 64
    .restart local v0    # "item":Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    :cond_1
    const/16 v2, 0x8

    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method
