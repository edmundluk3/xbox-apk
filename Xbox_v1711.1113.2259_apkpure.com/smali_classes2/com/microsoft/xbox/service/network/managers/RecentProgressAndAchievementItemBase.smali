.class public Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
.super Ljava/lang/Object;
.source "RecentProgressAndAchievementItemBase.java"


# static fields
.field protected static transient dateFormat:Ljava/text/DateFormat;


# instance fields
.field private currentGamerscore:I

.field private devices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private transient displayImage:Ljava/lang/String;

.field private earnedAchievements:I

.field private transient heroStats:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;",
            ">;"
        }
    .end annotation
.end field

.field private lastUnlock:Ljava/util/Date;

.field private maxGamerscore:I

.field private mediaItemType:Ljava/lang/String;

.field private transient minutesPlayed:Ljava/lang/String;

.field private modernTitleId:J

.field public transient responseType:Lcom/microsoft/xbox/service/network/managers/SLSResponseType;

.field private transient titleName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/text/SimpleDateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->dateFormat:Ljava/text/DateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/SLSResponseType;->XboxOne:Lcom/microsoft/xbox/service/network/managers/SLSResponseType;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->responseType:Lcom/microsoft/xbox/service/network/managers/SLSResponseType;

    return-void
.end method


# virtual methods
.method public getCurrentGamerscore()I
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->currentGamerscore:I

    return v0
.end method

.method public getDisplayImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->displayImage:Ljava/lang/String;

    return-object v0
.end method

.method public getEarnedAchievements()I
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->earnedAchievements:I

    return v0
.end method

.method public getHeroStats()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->heroStats:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getHeroStatsByUser(ILjava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    .locals 3
    .param p1, "index"    # I
    .param p2, "xuid"    # Ljava/lang/String;

    .prologue
    .line 84
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->heroStats:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->heroStats:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 85
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->heroStats:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;

    .line 86
    .local v0, "statsCollection":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->arrangebyfieldid:Ljava/lang/String;

    invoke-virtual {p2, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 87
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge p1, v2, :cond_0

    .line 88
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .line 94
    .end local v0    # "statsCollection":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLastUnlock()Ljava/util/Date;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->lastUnlock:Ljava/util/Date;

    return-object v0
.end method

.method public getMaxGamerscore()I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->maxGamerscore:I

    return v0
.end method

.method public getMediaItemType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->mediaItemType:Ljava/lang/String;

    return-object v0
.end method

.method public getMinutesPlayed()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->minutesPlayed:Ljava/lang/String;

    return-object v0
.end method

.method public getSingleHeroStat(I)Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 73
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->heroStats:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->heroStats:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 74
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->heroStats:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;

    .line 75
    .local v0, "data":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 76
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .line 80
    .end local v0    # "data":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTitleId()J
    .locals 2

    .prologue
    .line 49
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase$1;->$SwitchMap$com$microsoft$xbox$service$network$managers$SLSResponseType:[I

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->responseType:Lcom/microsoft/xbox/service/network/managers/SLSResponseType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/SLSResponseType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 55
    const-string v0, "Unknown title type"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 56
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    .line 51
    :pswitch_0
    check-cast p0, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;

    .end local p0    # "this":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->titleId:I

    int-to-long v0, v0

    goto :goto_0

    .line 53
    .restart local p0    # "this":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    :pswitch_1
    check-cast p0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    .end local p0    # "this":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    iget-wide v0, p0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->titleId:J

    goto :goto_0

    .line 49
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getTitleName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->titleName:Ljava/lang/String;

    return-object v0
.end method

.method public isSameTitle(Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;)Z
    .locals 6
    .param p1, "other"    # Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    .prologue
    const/4 v0, 0x0

    .line 187
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->responseType:Lcom/microsoft/xbox/service/network/managers/SLSResponseType;

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->responseType:Lcom/microsoft/xbox/service/network/managers/SLSResponseType;

    if-eq v1, v2, :cond_1

    .line 191
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->getTitleId()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->getTitleId()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setCurrentGamerscore(I)V
    .locals 0
    .param p1, "score"    # I

    .prologue
    .line 139
    iput p1, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->currentGamerscore:I

    .line 140
    return-void
.end method

.method public setDevices(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p1, "devices":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->devices:Ljava/util/List;

    .line 46
    return-void
.end method

.method public setDisplayImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "displayImage"    # Ljava/lang/String;

    .prologue
    .line 163
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->displayImage:Ljava/lang/String;

    .line 164
    return-void
.end method

.method public setEarnedAchievements(I)V
    .locals 0
    .param p1, "earnedAchievements"    # I

    .prologue
    .line 171
    iput p1, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->earnedAchievements:I

    .line 172
    return-void
.end method

.method public setHeroStats(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 65
    .local p1, "statistics":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->heroStats:Ljava/util/ArrayList;

    .line 66
    return-void
.end method

.method public setLastUnlock(Ljava/util/Date;)V
    .locals 0
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->lastUnlock:Ljava/util/Date;

    .line 180
    return-void
.end method

.method public setMaxGamerscore(I)V
    .locals 0
    .param p1, "maxScore"    # I

    .prologue
    .line 147
    iput p1, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->maxGamerscore:I

    .line 148
    return-void
.end method

.method public setMediaItemType(Ljava/lang/String;)V
    .locals 0
    .param p1, "mediaItemType"    # Ljava/lang/String;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->mediaItemType:Ljava/lang/String;

    .line 103
    return-void
.end method

.method public setMinutesPlayed(Ljava/lang/String;)V
    .locals 0
    .param p1, "minutesPlayed"    # Ljava/lang/String;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->minutesPlayed:Ljava/lang/String;

    .line 113
    return-void
.end method

.method public setMinutesPlayedForYouProfile(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p2, "youXuid"    # Ljava/lang/String;
    .param p3, "titleId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "statisticsCollection":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 116
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v3

    :goto_0
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 117
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    :goto_1
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 119
    if-eqz p1, :cond_4

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_4

    .line 120
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;

    .line 121
    .local v1, "statsCollection":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    iget-object v3, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->arrangebyfieldid:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 122
    iget-object v3, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 123
    iget-object v3, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;->stats:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .line 124
    .local v0, "item":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    sget-object v5, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->MinutesPlayed:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->titleid:Ljava/lang/String;

    invoke-virtual {v4, p3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 125
    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->convertFromMinuteToHours(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->minutesPlayed:Ljava/lang/String;

    goto :goto_2

    .end local v0    # "item":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    .end local v1    # "statsCollection":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsData;
    :cond_2
    move v2, v4

    .line 116
    goto :goto_0

    :cond_3
    move v3, v4

    .line 117
    goto :goto_1

    .line 132
    :cond_4
    return-void
.end method

.method public setMinutesPlayedFromStats(Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V
    .locals 1
    .param p1, "statistics"    # Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .prologue
    .line 106
    if-eqz p1, :cond_0

    .line 107
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->convertFromMinuteToHours(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->minutesPlayed:Ljava/lang/String;

    .line 109
    :cond_0
    return-void
.end method

.method public setTitleName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 155
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->titleName:Ljava/lang/String;

    .line 156
    return-void
.end method

.method public shouldDisplayAchievements()Z
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->devices:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->devices:Ljava/util/List;

    sget-object v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->Win32:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$DeviceType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
