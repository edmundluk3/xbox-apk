.class public final enum Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;
.super Ljava/lang/Enum;
.source "EDSV2SearchFilterType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

.field public static final enum SEARCHFILTERTYPE_ALL:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

.field public static final enum SEARCHFILTERTYPE_APP:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

.field public static final enum SEARCHFILTERTYPE_COMPANION:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

.field public static final enum SEARCHFILTERTYPE_MOVIE:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

.field public static final enum SEARCHFILTERTYPE_MUSIC:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

.field public static final enum SEARCHFILTERTYPE_MUSICALBUM:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

.field public static final enum SEARCHFILTERTYPE_MUSICALL:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

.field public static final enum SEARCHFILTERTYPE_MUSICARTIST:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

.field public static final enum SEARCHFILTERTYPE_MUSICTRACK:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

.field public static final enum SEARCHFILTERTYPE_TV:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

.field public static final enum SEARCHFILTERTYPE_WEBVIDEO:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

.field public static final enum SEARCHFILTERTYPE_XBOXGAME:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

.field private static hash:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;",
            ">;"
        }
    .end annotation
.end field

.field private static nameHash:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 10
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    const-string v3, "SEARCHFILTERTYPE_ALL"

    invoke-direct {v2, v3, v1, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_ALL:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .line 11
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    const-string v3, "SEARCHFILTERTYPE_APP"

    invoke-direct {v2, v3, v6, v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_APP:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .line 12
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    const-string v3, "SEARCHFILTERTYPE_XBOXGAME"

    invoke-direct {v2, v3, v7, v7}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_XBOXGAME:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .line 13
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    const-string v3, "SEARCHFILTERTYPE_MUSIC"

    invoke-direct {v2, v3, v8, v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_MUSIC:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .line 14
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    const-string v3, "SEARCHFILTERTYPE_TV"

    invoke-direct {v2, v3, v9, v9}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_TV:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .line 15
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    const-string v3, "SEARCHFILTERTYPE_MOVIE"

    const/4 v4, 0x5

    const/4 v5, 0x5

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_MOVIE:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .line 16
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    const-string v3, "SEARCHFILTERTYPE_MUSICARTIST"

    const/4 v4, 0x6

    const/4 v5, 0x6

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_MUSICARTIST:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .line 17
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    const-string v3, "SEARCHFILTERTYPE_WEBVIDEO"

    const/4 v4, 0x7

    const/4 v5, 0x7

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_WEBVIDEO:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .line 18
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    const-string v3, "SEARCHFILTERTYPE_MUSICALBUM"

    const/16 v4, 0x8

    const/16 v5, 0x8

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_MUSICALBUM:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .line 19
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    const-string v3, "SEARCHFILTERTYPE_MUSICTRACK"

    const/16 v4, 0x9

    const/16 v5, 0x9

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_MUSICTRACK:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .line 20
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    const-string v3, "SEARCHFILTERTYPE_MUSICALL"

    const/16 v4, 0xa

    const/16 v5, 0xa

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_MUSICALL:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .line 21
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    const-string v3, "SEARCHFILTERTYPE_COMPANION"

    const/16 v4, 0xb

    const/16 v5, 0xb

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_COMPANION:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .line 8
    const/16 v2, 0xc

    new-array v2, v2, [Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_ALL:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_APP:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    aput-object v3, v2, v6

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_XBOXGAME:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    aput-object v3, v2, v7

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_MUSIC:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    aput-object v3, v2, v8

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_TV:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    aput-object v3, v2, v9

    const/4 v3, 0x5

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_MOVIE:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    aput-object v4, v2, v3

    const/4 v3, 0x6

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_MUSICARTIST:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    aput-object v4, v2, v3

    const/4 v3, 0x7

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_WEBVIDEO:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    aput-object v4, v2, v3

    const/16 v3, 0x8

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_MUSICALBUM:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    aput-object v4, v2, v3

    const/16 v3, 0x9

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_MUSICTRACK:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    aput-object v4, v2, v3

    const/16 v3, 0xa

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_MUSICALL:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    aput-object v4, v2, v3

    const/16 v3, 0xb

    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_COMPANION:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    aput-object v4, v2, v3

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->$VALUES:[Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .line 27
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->hash:Ljava/util/HashMap;

    .line 28
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->nameHash:Ljava/util/HashMap;

    .line 31
    invoke-static {}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->values()[Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    move-result-object v2

    array-length v3, v2

    .local v0, "type":Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 32
    sget-object v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->hash:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->getValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 35
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->nameHash:Ljava/util/HashMap;

    const-string v2, "AppType"

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_APP:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->nameHash:Ljava/util/HashMap;

    const-string v2, "GameType"

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_XBOXGAME:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->nameHash:Ljava/util/HashMap;

    const-string v2, "MusicType"

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_MUSICALL:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->nameHash:Ljava/util/HashMap;

    const-string v2, "TVType"

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_TV:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->nameHash:Ljava/util/HashMap;

    const-string v2, "MovieType"

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_MOVIE:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->nameHash:Ljava/util/HashMap;

    const-string v2, "MusicArtistType"

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_MUSICARTIST:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->nameHash:Ljava/util/HashMap;

    const-string v2, "WebVideoType"

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_WEBVIDEO:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->nameHash:Ljava/util/HashMap;

    const-string v2, "EnhancedContentType"

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_COMPANION:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "v"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 46
    iput p3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->value:I

    .line 47
    return-void
.end method

.method public static forValue(I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 58
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->hash:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->hash:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .line 62
    :goto_0
    return-object v0

    .line 61
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 62
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->SEARCHFILTERTYPE_ALL:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    goto :goto_0
.end method

.method public static getTypeFromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 66
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->nameHash:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->nameHash:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .line 71
    :goto_0
    return-object v0

    .line 70
    :cond_0
    const-string v0, "EDSV2SearchFilter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private putName(Ljava/lang/String;Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .prologue
    .line 50
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->nameHash:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 8
    const-class v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->$VALUES:[Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->value:I

    return v0
.end method
