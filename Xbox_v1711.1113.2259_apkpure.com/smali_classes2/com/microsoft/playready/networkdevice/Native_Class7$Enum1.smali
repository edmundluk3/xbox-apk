.class final enum Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;
.super Ljava/lang/Enum;
.source "Native_Class7.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/networkdevice/Native_Class7;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Enum1"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

.field public static final enum Browse:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

.field public static final enum ConnectionIDs:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

.field public static final enum ConnectionInfo:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

.field public static final enum ProtocolInfo:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

.field public static final enum SearchCaps:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

.field public static final enum SortCaps:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

.field public static final enum SystemUpdateID:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

.field public static final enum Unknown:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 104
    new-instance v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    const-string v1, "Unknown"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->Unknown:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    .line 105
    new-instance v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    const-string v1, "ConnectionIDs"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->ConnectionIDs:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    .line 106
    new-instance v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    const-string v1, "ConnectionInfo"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->ConnectionInfo:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    .line 107
    new-instance v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    const-string v1, "Browse"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->Browse:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    .line 108
    new-instance v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    const-string v1, "ProtocolInfo"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->ProtocolInfo:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    .line 109
    new-instance v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    const-string v1, "SearchCaps"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->SearchCaps:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    .line 110
    new-instance v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    const-string v1, "SortCaps"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->SortCaps:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    .line 111
    new-instance v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    const-string v1, "SystemUpdateID"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->SystemUpdateID:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    .line 102
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    sget-object v1, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->Unknown:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->ConnectionIDs:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->ConnectionInfo:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->Browse:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->ProtocolInfo:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->SearchCaps:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->SortCaps:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->SystemUpdateID:Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->$VALUES:[Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 102
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 102
    const-class v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->$VALUES:[Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    invoke-virtual {v0}, [Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/playready/networkdevice/Native_Class7$Enum1;

    return-object v0
.end method
