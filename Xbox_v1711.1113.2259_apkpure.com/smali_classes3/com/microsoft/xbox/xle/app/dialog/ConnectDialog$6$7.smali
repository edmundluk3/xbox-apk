.class Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$7;
.super Ljava/lang/Object;
.source "ConnectDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->bindConsoleRow(Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;Lcom/microsoft/xbox/xle/model/ConsoleData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;

.field final synthetic val$autoConnectCheckbox:Landroid/widget/CheckBox;

.field final synthetic val$autoConnectLabel:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field final synthetic val$console:Lcom/microsoft/xbox/xle/model/ConsoleData;

.field final synthetic val$powerOnOffConnectMessage:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field final synthetic val$powerOnOffConnectView:Landroid/widget/LinearLayout;

.field final synthetic val$turnOffButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;Landroid/widget/CheckBox;Lcom/microsoft/xbox/toolkit/ui/XLEButton;Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;Landroid/widget/LinearLayout;Lcom/microsoft/xbox/xle/model/ConsoleData;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;

    .prologue
    .line 540
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$7;->this$1:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$7;->val$autoConnectCheckbox:Landroid/widget/CheckBox;

    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$7;->val$autoConnectLabel:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object p4, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$7;->val$turnOffButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    iput-object p5, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$7;->val$powerOnOffConnectMessage:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object p6, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$7;->val$powerOnOffConnectView:Landroid/widget/LinearLayout;

    iput-object p7, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$7;->val$console:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x4

    const/4 v3, 0x0

    .line 544
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$7;->this$1:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$1000(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)V

    .line 545
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$7;->val$autoConnectCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 546
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$7;->val$autoConnectLabel:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 547
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$7;->val$turnOffButton:Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setEnabled(Z)V

    .line 548
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$7;->val$powerOnOffConnectMessage:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f070364

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 549
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$7;->val$powerOnOffConnectView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 550
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Power Turn Off"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$7;->this$1:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$700(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$7;->val$console:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->powerOff(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    .line 552
    return-void
.end method
