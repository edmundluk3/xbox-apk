.class public Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;
.super Ljava/lang/Object;
.source "LfgInterestedMember.java"


# instance fields
.field private final heroStats:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;",
            ">;"
        }
    .end annotation
.end field

.field public final member:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

.field public final memberIndex:Ljava/lang/String;

.field public final memberSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

.field private timePlayed:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Ljava/lang/Integer;)V
    .locals 1
    .param p1, "memberIndex"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "member"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "memberSummary"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4, "timePlayed"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 28
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 29
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 31
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->memberIndex:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->member:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    .line 33
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->memberSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 34
    iput-object p4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->timePlayed:Ljava/lang/Integer;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->heroStats:Ljava/util/List;

    .line 36
    return-void
.end method


# virtual methods
.method public addHeroStats(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p1, "statistics":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;>;"
    if-eqz p1, :cond_0

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->heroStats:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 51
    :cond_0
    return-void
.end method

.method public getHeroStats()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->heroStats:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTimePlayed()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->timePlayed:Ljava/lang/Integer;

    return-object v0
.end method

.method public setTimePlayed(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "timePlayed"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 44
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->timePlayed:Ljava/lang/Integer;

    .line 45
    return-void
.end method
