.class public interface abstract Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$Callback;
.super Ljava/lang/Object;
.source "XLEWebSocket.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callback"
.end annotation


# virtual methods
.method public abstract onCloseError(Ljava/lang/Exception;)V
.end method

.method public abstract onClosed()V
.end method

.method public abstract onConnectError(Ljava/lang/Exception;)V
.end method

.method public abstract onConnected()V
.end method

.method public abstract onDataReceived(Ljava/lang/String;)V
.end method

.method public abstract onDataReceived(Ljava/nio/ByteBuffer;)V
.end method

.method public abstract onPong(Ljava/lang/String;)V
.end method
