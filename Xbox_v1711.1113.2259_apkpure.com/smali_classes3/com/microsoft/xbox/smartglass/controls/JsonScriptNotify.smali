.class Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;
.super Ljava/lang/Object;
.source "JsonScriptNotify.java"


# instance fields
.field public final args:Ljava/lang/String;

.field public final className:Ljava/lang/String;

.field public final id:I

.field public final key:Ljava/util/UUID;

.field public final methodName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "json"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 22
    .local v0, "jsonObject":Lorg/json/JSONObject;
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->id:I

    .line 23
    const-string v1, "className"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->className:Ljava/lang/String;

    .line 24
    const-string v1, "methodName"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->methodName:Ljava/lang/String;

    .line 25
    const-string v1, "key"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->key:Ljava/util/UUID;

    .line 29
    const-string v1, "args"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 30
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->args:Ljava/lang/String;

    .line 34
    :goto_0
    return-void

    .line 32
    :cond_0
    const-string v1, "args"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->args:Ljava/lang/String;

    goto :goto_0
.end method
