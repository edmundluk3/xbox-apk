.class public final enum Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;
.super Ljava/lang/Enum;
.source "EventInitializationFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/idp/ui/EventInitializationFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;

.field public static final enum ERROR:Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;

.field public static final enum PROVIDER_ERROR:Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;

.field public static final enum SUCCESS:Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 187
    new-instance v0, Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;->SUCCESS:Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;

    new-instance v0, Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;->ERROR:Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;

    new-instance v0, Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;

    const-string v1, "PROVIDER_ERROR"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;->PROVIDER_ERROR:Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;

    .line 186
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;

    sget-object v1, Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;->SUCCESS:Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;->ERROR:Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;->PROVIDER_ERROR:Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;->$VALUES:[Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 186
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 186
    const-class v0, Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;
    .locals 1

    .prologue
    .line 186
    sget-object v0, Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;->$VALUES:[Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/idp/ui/EventInitializationFragment$Status;

    return-object v0
.end method
