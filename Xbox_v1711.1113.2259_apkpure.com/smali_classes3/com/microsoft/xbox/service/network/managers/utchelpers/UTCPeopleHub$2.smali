.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$2;
.super Ljava/lang/Object;
.source "UTCPeopleHub.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/functions/GenericFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/toolkit/functions/GenericFunction",
        "<",
        "Ljava/lang/Void;",
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic eval(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 141
    check-cast p1, Ljava/util/HashMap;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$2;->eval(Ljava/util/HashMap;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public eval(Ljava/util/HashMap;)Ljava/lang/Void;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    .prologue
    .line 145
    .local p1, "args":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v4, "pagename"

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "pageaction"

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "pagetransitionorder"

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 146
    const-string v4, "pagetransitionorder"

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 147
    .local v3, "pagetransitionorder":Ljava/lang/String;
    const-string v4, "pagename"

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 148
    .local v2, "pageName":Ljava/lang/String;
    const-string v4, "pageaction"

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 150
    .local v1, "pageAction":Ljava/lang/String;
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 151
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->access$000()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 152
    const-string v4, "TargetXuid"

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->access$000()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 153
    const-string v4, "ProfileType"

    const-string v5, "YouProfile"

    invoke-virtual {v0, v4, v5}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 158
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->access$200()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->access$102(Ljava/lang/String;)Ljava/lang/String;

    .line 159
    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->access$202(Ljava/lang/String;)Ljava/lang/String;

    .line 160
    const-string v4, "02"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 161
    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 164
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->access$200()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->access$100()Ljava/lang/String;

    move-result-object v5

    if-eq v4, v5, :cond_1

    .line 165
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->access$200()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->access$100()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 170
    .end local v0    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    .end local v1    # "pageAction":Ljava/lang/String;
    .end local v2    # "pageName":Ljava/lang/String;
    .end local v3    # "pagetransitionorder":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    return-object v4

    .line 155
    .restart local v0    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    .restart local v1    # "pageAction":Ljava/lang/String;
    .restart local v2    # "pageName":Ljava/lang/String;
    .restart local v3    # "pagetransitionorder":Ljava/lang/String;
    :cond_2
    const-string v4, "ProfileType"

    const-string v5, "MeProfile"

    invoke-virtual {v0, v4, v5}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method
