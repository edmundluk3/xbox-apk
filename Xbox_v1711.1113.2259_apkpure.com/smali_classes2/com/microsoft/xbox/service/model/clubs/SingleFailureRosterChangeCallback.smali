.class public final Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;
.super Ljava/lang/Object;
.source "SingleFailureRosterChangeCallback.java"

# interfaces
.implements Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;


# instance fields
.field private final failureString:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field private final onFailure:Ljava/lang/Runnable;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "onFailure"    # Ljava/lang/Runnable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 25
    const v0, 0x7f070b6d

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;-><init>(Ljava/lang/Runnable;I)V

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/lang/Runnable;I)V
    .locals 0
    .param p1, "onFailure"    # Ljava/lang/Runnable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "failureString"    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 30
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;->onFailure:Ljava/lang/Runnable;

    .line 31
    iput p2, p0, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;->failureString:I

    .line 32
    return-void
.end method


# virtual methods
.method public onPartialRosterChangeSuccess(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p1, "successfulMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    .local p2, "failedMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    const-string v0, "Should not be possible due to all requests being singular"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method public onRosterChangeFailure(Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 40
    .local p1, "failedMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;->failureString:I

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(I)V

    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;->onFailure:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 42
    return-void
.end method

.method public onRosterChangeSuccess(Ljava/util/List;)V
    .locals 0
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p1, "successfulMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    return-void
.end method
