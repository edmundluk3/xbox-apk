.class public final enum Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;
.super Ljava/lang/Enum;
.source "RecommendationListResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "StoreListType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

.field public static final enum AddonsNew:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

.field public static final enum AddonsTopFree:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

.field public static final enum AddonsTopPaid:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

.field public static final enum AppsListsPopularOnXbox:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

.field public static final enum AppsNew:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

.field public static final enum GamepassRTMList1:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

.field public static final enum GamepassRTMList2:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

.field public static final enum GamepassRTMList3:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

.field public static final enum GamepassRTMList4:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

.field public static final enum GamepassRTMList5:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

.field public static final enum GamesComingSoon:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

.field public static final enum GamesMostPlayed:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

.field public static final enum GamesRecent:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

.field public static final enum GoldDeals:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

.field public static final enum GoldGame:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

.field public static final enum Unknown:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;


# instance fields
.field private final typeString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 27
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    const-string v1, "Unknown"

    const-string v2, ""

    invoke-direct {v0, v1, v4, v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->Unknown:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    .line 28
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    const-string v1, "GamesRecent"

    const-string v2, "New"

    invoke-direct {v0, v1, v5, v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GamesRecent:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    .line 29
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    const-string v1, "GamesMostPlayed"

    const-string v2, "MostPlayed"

    invoke-direct {v0, v1, v6, v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GamesMostPlayed:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    .line 30
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    const-string v1, "GamesComingSoon"

    const-string v2, "ComingSoon"

    invoke-direct {v0, v1, v7, v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GamesComingSoon:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    .line 31
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    const-string v1, "AddonsNew"

    const-string v2, "New"

    invoke-direct {v0, v1, v8, v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->AddonsNew:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    .line 32
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    const-string v1, "AddonsTopPaid"

    const/4 v2, 0x5

    const-string v3, "TopPaid"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->AddonsTopPaid:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    .line 33
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    const-string v1, "AddonsTopFree"

    const/4 v2, 0x6

    const-string v3, "TopFree"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->AddonsTopFree:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    .line 34
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    const-string v1, "AppsNew"

    const/4 v2, 0x7

    const-string v3, "AppsNew"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->AppsNew:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    .line 35
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    const-string v1, "AppsListsPopularOnXbox"

    const/16 v2, 0x8

    const-string v3, "AppsListsPopularOnXbox"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->AppsListsPopularOnXbox:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    .line 36
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    const-string v1, "GoldGame"

    const/16 v2, 0x9

    const-string v3, "GamesWithGold"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GoldGame:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    .line 37
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    const-string v1, "GoldDeals"

    const/16 v2, 0xa

    const-string v3, "DealsWithGold"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GoldDeals:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    .line 38
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    const-string v1, "GamepassRTMList1"

    const/16 v2, 0xb

    const-string v3, "SubsXGPFeatured"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GamepassRTMList1:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    .line 39
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    const-string v1, "GamepassRTMList2"

    const/16 v2, 0xc

    const-string v3, "SubsXGPChannel3"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GamepassRTMList2:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    .line 40
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    const-string v1, "GamepassRTMList3"

    const/16 v2, 0xd

    const-string v3, "SubsXGPChannel5"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GamepassRTMList3:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    .line 41
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    const-string v1, "GamepassRTMList4"

    const/16 v2, 0xe

    const-string v3, "SubsXGPChannel7"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GamepassRTMList4:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    .line 42
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    const-string v1, "GamepassRTMList5"

    const/16 v2, 0xf

    const-string v3, "SubsXGPChannel9"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GamepassRTMList5:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    .line 26
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    sget-object v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->Unknown:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GamesRecent:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GamesMostPlayed:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GamesComingSoon:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->AddonsNew:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->AddonsTopPaid:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->AddonsTopFree:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->AppsNew:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->AppsListsPopularOnXbox:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GoldGame:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GoldDeals:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GamepassRTMList1:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GamepassRTMList2:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GamepassRTMList3:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GamepassRTMList4:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GamepassRTMList5:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->$VALUES:[Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "typeString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 47
    iput-object p3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->typeString:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    const-class v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->$VALUES:[Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->typeString:Ljava/lang/String;

    return-object v0
.end method
