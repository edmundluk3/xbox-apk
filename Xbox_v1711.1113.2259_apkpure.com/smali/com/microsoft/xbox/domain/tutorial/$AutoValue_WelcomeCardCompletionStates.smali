.class abstract Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;
.super Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;
.source "$AutoValue_WelcomeCardCompletionStates.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;
    }
.end annotation


# instance fields
.field private final clubWelcomeCardCompleted:Z

.field private final facebookWelcomeCardCompleted:Z

.field private final redeemWelcomeCardCompleted:Z

.field private final setupWelcomeCardCompleted:Z

.field private final shopWelcomeCardCompleted:Z

.field private final suggestedFriendsWelcomeCardCompleted:Z


# direct methods
.method constructor <init>(ZZZZZZ)V
    .locals 0
    .param p1, "setupWelcomeCardCompleted"    # Z
    .param p2, "redeemWelcomeCardCompleted"    # Z
    .param p3, "shopWelcomeCardCompleted"    # Z
    .param p4, "facebookWelcomeCardCompleted"    # Z
    .param p5, "suggestedFriendsWelcomeCardCompleted"    # Z
    .param p6, "clubWelcomeCardCompleted"    # Z

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;-><init>()V

    .line 24
    iput-boolean p1, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->setupWelcomeCardCompleted:Z

    .line 25
    iput-boolean p2, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->redeemWelcomeCardCompleted:Z

    .line 26
    iput-boolean p3, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->shopWelcomeCardCompleted:Z

    .line 27
    iput-boolean p4, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->facebookWelcomeCardCompleted:Z

    .line 28
    iput-boolean p5, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->suggestedFriendsWelcomeCardCompleted:Z

    .line 29
    iput-boolean p6, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->clubWelcomeCardCompleted:Z

    .line 30
    return-void
.end method


# virtual methods
.method public clubWelcomeCardCompleted()Z
    .locals 1
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "clubWelcomeCardCompleted"
    .end annotation

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->clubWelcomeCardCompleted:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 82
    if-ne p1, p0, :cond_1

    .line 94
    :cond_0
    :goto_0
    return v1

    .line 85
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 86
    check-cast v0, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    .line 87
    .local v0, "that":Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;
    iget-boolean v3, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->setupWelcomeCardCompleted:Z

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->setupWelcomeCardCompleted()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->redeemWelcomeCardCompleted:Z

    .line 88
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->redeemWelcomeCardCompleted()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->shopWelcomeCardCompleted:Z

    .line 89
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->shopWelcomeCardCompleted()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->facebookWelcomeCardCompleted:Z

    .line 90
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->facebookWelcomeCardCompleted()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->suggestedFriendsWelcomeCardCompleted:Z

    .line 91
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->suggestedFriendsWelcomeCardCompleted()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->clubWelcomeCardCompleted:Z

    .line 92
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->clubWelcomeCardCompleted()Z

    move-result v4

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;
    :cond_3
    move v1, v2

    .line 94
    goto :goto_0
.end method

.method public facebookWelcomeCardCompleted()Z
    .locals 1
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "facebookWelcomeCardCompleted"
    .end annotation

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->facebookWelcomeCardCompleted:Z

    return v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const v4, 0xf4243

    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    .line 99
    const/4 v0, 0x1

    .line 100
    .local v0, "h":I
    mul-int/2addr v0, v4

    .line 101
    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->setupWelcomeCardCompleted:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 102
    mul-int/2addr v0, v4

    .line 103
    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->redeemWelcomeCardCompleted:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 104
    mul-int/2addr v0, v4

    .line 105
    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->shopWelcomeCardCompleted:Z

    if-eqz v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 106
    mul-int/2addr v0, v4

    .line 107
    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->facebookWelcomeCardCompleted:Z

    if-eqz v1, :cond_3

    move v1, v2

    :goto_3
    xor-int/2addr v0, v1

    .line 108
    mul-int/2addr v0, v4

    .line 109
    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->suggestedFriendsWelcomeCardCompleted:Z

    if-eqz v1, :cond_4

    move v1, v2

    :goto_4
    xor-int/2addr v0, v1

    .line 110
    mul-int/2addr v0, v4

    .line 111
    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->clubWelcomeCardCompleted:Z

    if-eqz v1, :cond_5

    :goto_5
    xor-int/2addr v0, v2

    .line 112
    return v0

    :cond_0
    move v1, v3

    .line 101
    goto :goto_0

    :cond_1
    move v1, v3

    .line 103
    goto :goto_1

    :cond_2
    move v1, v3

    .line 105
    goto :goto_2

    :cond_3
    move v1, v3

    .line 107
    goto :goto_3

    :cond_4
    move v1, v3

    .line 109
    goto :goto_4

    :cond_5
    move v2, v3

    .line 111
    goto :goto_5
.end method

.method public redeemWelcomeCardCompleted()Z
    .locals 1
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "redeemWelcomeCardCompleted"
    .end annotation

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->redeemWelcomeCardCompleted:Z

    return v0
.end method

.method public setupWelcomeCardCompleted()Z
    .locals 1
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "setupWelcomeCardCompleted"
    .end annotation

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->setupWelcomeCardCompleted:Z

    return v0
.end method

.method public shopWelcomeCardCompleted()Z
    .locals 1
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "shopWelcomeCardCompleted"
    .end annotation

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->shopWelcomeCardCompleted:Z

    return v0
.end method

.method public suggestedFriendsWelcomeCardCompleted()Z
    .locals 1
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "suggestedFriendsWelcomeCardCompleted"
    .end annotation

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->suggestedFriendsWelcomeCardCompleted:Z

    return v0
.end method

.method public toBuilder()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;
    .locals 1

    .prologue
    .line 117
    new-instance v0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;-><init>(Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WelcomeCardCompletionStates{setupWelcomeCardCompleted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->setupWelcomeCardCompleted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", redeemWelcomeCardCompleted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->redeemWelcomeCardCompleted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", shopWelcomeCardCompleted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->shopWelcomeCardCompleted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", facebookWelcomeCardCompleted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->facebookWelcomeCardCompleted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", suggestedFriendsWelcomeCardCompleted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->suggestedFriendsWelcomeCardCompleted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clubWelcomeCardCompleted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates;->clubWelcomeCardCompleted:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
