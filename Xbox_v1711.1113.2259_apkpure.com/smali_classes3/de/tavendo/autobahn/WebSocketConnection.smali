.class public Lde/tavendo/autobahn/WebSocketConnection;
.super Ljava/lang/Object;
.source "WebSocketConnection.java"

# interfaces
.implements Lde/tavendo/autobahn/WebSocket;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lde/tavendo/autobahn/WebSocketConnection$SocketThread;,
        Lde/tavendo/autobahn/WebSocketConnection$ThreadHandler;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final WSS_URI_SCHEME:Ljava/lang/String; = "wss"

.field private static final WS_READER:Ljava/lang/String; = "WebSocketReader"

.field private static final WS_URI_SCHEME:Ljava/lang/String; = "ws"

.field private static final WS_WRITER:Ljava/lang/String; = "WebSocketWriter"


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private mPreviousConnection:Z

.field private mSocket:Ljava/net/Socket;

.field private mSocketThread:Lde/tavendo/autobahn/WebSocketConnection$SocketThread;

.field private mWebSocketConnectionObserver:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver;",
            ">;"
        }
    .end annotation
.end field

.field private mWebSocketOptions:Lde/tavendo/autobahn/WebSocketOptions;

.field private mWebSocketReader:Lde/tavendo/autobahn/WebSocketReader;

.field private mWebSocketSubprotocols:[Ljava/lang/String;

.field private mWebSocketURI:Ljava/net/URI;

.field private mWebSocketWriter:Lde/tavendo/autobahn/WebSocketWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lde/tavendo/autobahn/WebSocketConnection;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lde/tavendo/autobahn/WebSocketConnection;->TAG:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lde/tavendo/autobahn/WebSocketConnection;->mPreviousConnection:Z

    .line 62
    sget-object v0, Lde/tavendo/autobahn/WebSocketConnection;->TAG:Ljava/lang/String;

    const-string v1, "WebSocket connection created."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    new-instance v0, Lde/tavendo/autobahn/WebSocketConnection$ThreadHandler;

    invoke-direct {v0, p0}, Lde/tavendo/autobahn/WebSocketConnection$ThreadHandler;-><init>(Lde/tavendo/autobahn/WebSocketConnection;)V

    iput-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection;->mHandler:Landroid/os/Handler;

    .line 65
    return-void
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lde/tavendo/autobahn/WebSocketConnection;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lde/tavendo/autobahn/WebSocketConnection;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 344
    invoke-direct {p0, p1}, Lde/tavendo/autobahn/WebSocketConnection;->handleMessage(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$2(Lde/tavendo/autobahn/WebSocketConnection;)Lde/tavendo/autobahn/WebSocketConnection$SocketThread;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection;->mSocketThread:Lde/tavendo/autobahn/WebSocketConnection$SocketThread;

    return-object v0
.end method

.method private connect()V
    .locals 5

    .prologue
    .line 199
    new-instance v2, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;

    iget-object v3, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketURI:Ljava/net/URI;

    iget-object v4, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketOptions:Lde/tavendo/autobahn/WebSocketOptions;

    invoke-direct {v2, v3, v4}, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;-><init>(Ljava/net/URI;Lde/tavendo/autobahn/WebSocketOptions;)V

    iput-object v2, p0, Lde/tavendo/autobahn/WebSocketConnection;->mSocketThread:Lde/tavendo/autobahn/WebSocketConnection$SocketThread;

    .line 201
    iget-object v2, p0, Lde/tavendo/autobahn/WebSocketConnection;->mSocketThread:Lde/tavendo/autobahn/WebSocketConnection$SocketThread;

    invoke-virtual {v2}, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;->start()V

    .line 202
    iget-object v3, p0, Lde/tavendo/autobahn/WebSocketConnection;->mSocketThread:Lde/tavendo/autobahn/WebSocketConnection$SocketThread;

    monitor-enter v3

    .line 204
    :try_start_0
    iget-object v2, p0, Lde/tavendo/autobahn/WebSocketConnection;->mSocketThread:Lde/tavendo/autobahn/WebSocketConnection$SocketThread;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    :goto_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 208
    iget-object v2, p0, Lde/tavendo/autobahn/WebSocketConnection;->mSocketThread:Lde/tavendo/autobahn/WebSocketConnection$SocketThread;

    invoke-virtual {v2}, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;->getHandler()Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lde/tavendo/autobahn/WebSocketConnection$3;

    invoke-direct {v3, p0}, Lde/tavendo/autobahn/WebSocketConnection$3;-><init>(Lde/tavendo/autobahn/WebSocketConnection;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 216
    iget-object v3, p0, Lde/tavendo/autobahn/WebSocketConnection;->mSocketThread:Lde/tavendo/autobahn/WebSocketConnection$SocketThread;

    monitor-enter v3

    .line 218
    :try_start_2
    iget-object v2, p0, Lde/tavendo/autobahn/WebSocketConnection;->mSocketThread:Lde/tavendo/autobahn/WebSocketConnection$SocketThread;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 216
    :goto_1
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 223
    iget-object v2, p0, Lde/tavendo/autobahn/WebSocketConnection;->mSocketThread:Lde/tavendo/autobahn/WebSocketConnection$SocketThread;

    invoke-virtual {v2}, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;->getSocket()Ljava/net/Socket;

    move-result-object v2

    iput-object v2, p0, Lde/tavendo/autobahn/WebSocketConnection;->mSocket:Ljava/net/Socket;

    .line 225
    iget-object v2, p0, Lde/tavendo/autobahn/WebSocketConnection;->mSocket:Ljava/net/Socket;

    if-nez v2, :cond_0

    .line 226
    sget-object v2, Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;->CANNOT_CONNECT:Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;

    iget-object v3, p0, Lde/tavendo/autobahn/WebSocketConnection;->mSocketThread:Lde/tavendo/autobahn/WebSocketConnection$SocketThread;

    invoke-virtual {v3}, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;->getFailureMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lde/tavendo/autobahn/WebSocketConnection;->onClose(Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;Ljava/lang/String;)V

    .line 240
    :goto_2
    return-void

    .line 202
    :catchall_0
    move-exception v2

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v2

    .line 216
    :catchall_1
    move-exception v2

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v2

    .line 227
    :cond_0
    iget-object v2, p0, Lde/tavendo/autobahn/WebSocketConnection;->mSocket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 229
    :try_start_6
    invoke-virtual {p0}, Lde/tavendo/autobahn/WebSocketConnection;->createReader()V

    .line 230
    invoke-virtual {p0}, Lde/tavendo/autobahn/WebSocketConnection;->createWriter()V

    .line 232
    new-instance v0, Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;

    iget-object v2, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketURI:Ljava/net/URI;

    const/4 v3, 0x0

    iget-object v4, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketSubprotocols:[Ljava/lang/String;

    invoke-direct {v0, v2, v3, v4}, Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;-><init>(Ljava/net/URI;Ljava/net/URI;[Ljava/lang/String;)V

    .line 233
    .local v0, "clientHandshake":Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;
    iget-object v2, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketWriter:Lde/tavendo/autobahn/WebSocketWriter;

    invoke-virtual {v2, v0}, Lde/tavendo/autobahn/WebSocketWriter;->forward(Ljava/lang/Object;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_2

    .line 234
    .end local v0    # "clientHandshake":Lde/tavendo/autobahn/WebSocketMessage$ClientHandshake;
    :catch_0
    move-exception v1

    .line 235
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;->INTERNAL_ERROR:Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;

    invoke-virtual {v1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lde/tavendo/autobahn/WebSocketConnection;->onClose(Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;Ljava/lang/String;)V

    goto :goto_2

    .line 238
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    sget-object v2, Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;->CANNOT_CONNECT:Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;

    const-string v3, "could not connect to WebSockets server"

    invoke-direct {p0, v2, v3}, Lde/tavendo/autobahn/WebSocketConnection;->onClose(Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;Ljava/lang/String;)V

    goto :goto_2

    .line 219
    :catch_1
    move-exception v2

    goto :goto_1

    .line 205
    :catch_2
    move-exception v2

    goto :goto_0
.end method

.method private failConnection(Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;Ljava/lang/String;)V
    .locals 4
    .param p1, "code"    # Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;
    .param p2, "reason"    # Ljava/lang/String;

    .prologue
    .line 94
    sget-object v1, Lde/tavendo/autobahn/WebSocketConnection;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "fail connection [code = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", reason = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketReader:Lde/tavendo/autobahn/WebSocketReader;

    if-eqz v1, :cond_0

    .line 97
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketReader:Lde/tavendo/autobahn/WebSocketReader;

    invoke-virtual {v1}, Lde/tavendo/autobahn/WebSocketReader;->quit()V

    .line 100
    :try_start_0
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketReader:Lde/tavendo/autobahn/WebSocketReader;

    invoke-virtual {v1}, Lde/tavendo/autobahn/WebSocketReader;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :goto_0
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketWriter:Lde/tavendo/autobahn/WebSocketWriter;

    if-eqz v1, :cond_1

    .line 109
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketWriter:Lde/tavendo/autobahn/WebSocketWriter;

    new-instance v2, Lde/tavendo/autobahn/WebSocketMessage$Quit;

    invoke-direct {v2}, Lde/tavendo/autobahn/WebSocketMessage$Quit;-><init>()V

    invoke-virtual {v1, v2}, Lde/tavendo/autobahn/WebSocketWriter;->forward(Ljava/lang/Object;)V

    .line 112
    :try_start_1
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketWriter:Lde/tavendo/autobahn/WebSocketWriter;

    invoke-virtual {v1}, Lde/tavendo/autobahn/WebSocketWriter;->join()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 120
    :goto_1
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketConnection;->mSocket:Ljava/net/Socket;

    if-eqz v1, :cond_2

    .line 121
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketConnection;->mSocketThread:Lde/tavendo/autobahn/WebSocketConnection$SocketThread;

    invoke-virtual {v1}, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lde/tavendo/autobahn/WebSocketConnection$1;

    invoke-direct {v2, p0}, Lde/tavendo/autobahn/WebSocketConnection$1;-><init>(Lde/tavendo/autobahn/WebSocketConnection;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 132
    :goto_2
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketConnection;->mSocketThread:Lde/tavendo/autobahn/WebSocketConnection$SocketThread;

    invoke-virtual {v1}, Lde/tavendo/autobahn/WebSocketConnection$SocketThread;->getHandler()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lde/tavendo/autobahn/WebSocketConnection$2;

    invoke-direct {v2, p0}, Lde/tavendo/autobahn/WebSocketConnection$2;-><init>(Lde/tavendo/autobahn/WebSocketConnection;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 140
    invoke-direct {p0, p1, p2}, Lde/tavendo/autobahn/WebSocketConnection;->onClose(Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;Ljava/lang/String;)V

    .line 142
    sget-object v1, Lde/tavendo/autobahn/WebSocketConnection;->TAG:Ljava/lang/String;

    const-string v2, "worker threads stopped"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    return-void

    .line 101
    :catch_0
    move-exception v0

    .line 102
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 105
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    sget-object v1, Lde/tavendo/autobahn/WebSocketConnection;->TAG:Ljava/lang/String;

    const-string v2, "mReader already NULL"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 113
    :catch_1
    move-exception v0

    .line 114
    .restart local v0    # "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 117
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    sget-object v1, Lde/tavendo/autobahn/WebSocketConnection;->TAG:Ljava/lang/String;

    const-string v2, "mWriter already NULL"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 129
    :cond_2
    sget-object v1, Lde/tavendo/autobahn/WebSocketConnection;->TAG:Ljava/lang/String;

    const-string v2, "mTransportChannel already NULL"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    .line 345
    iget-object v9, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketConnectionObserver:Ljava/lang/ref/WeakReference;

    invoke-virtual {v9}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver;

    .line 347
    .local v8, "webSocketObserver":Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver;
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v9, v9, Lde/tavendo/autobahn/WebSocketMessage$TextMessage;

    if-eqz v9, :cond_2

    .line 348
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v7, Lde/tavendo/autobahn/WebSocketMessage$TextMessage;

    .line 350
    .local v7, "textMessage":Lde/tavendo/autobahn/WebSocketMessage$TextMessage;
    if-eqz v8, :cond_1

    .line 351
    iget-object v9, v7, Lde/tavendo/autobahn/WebSocketMessage$TextMessage;->mPayload:Ljava/lang/String;

    invoke-interface {v8, v9}, Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver;->onTextMessage(Ljava/lang/String;)V

    .line 428
    .end local v7    # "textMessage":Lde/tavendo/autobahn/WebSocketMessage$TextMessage;
    :cond_0
    :goto_0
    return-void

    .line 353
    .restart local v7    # "textMessage":Lde/tavendo/autobahn/WebSocketMessage$TextMessage;
    :cond_1
    sget-object v9, Lde/tavendo/autobahn/WebSocketConnection;->TAG:Ljava/lang/String;

    const-string v10, "could not call onTextMessage() .. handler already NULL"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 356
    .end local v7    # "textMessage":Lde/tavendo/autobahn/WebSocketMessage$TextMessage;
    :cond_2
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v9, v9, Lde/tavendo/autobahn/WebSocketMessage$RawTextMessage;

    if-eqz v9, :cond_4

    .line 357
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Lde/tavendo/autobahn/WebSocketMessage$RawTextMessage;

    .line 359
    .local v5, "rawTextMessage":Lde/tavendo/autobahn/WebSocketMessage$RawTextMessage;
    if-eqz v8, :cond_3

    .line 360
    iget-object v9, v5, Lde/tavendo/autobahn/WebSocketMessage$RawTextMessage;->mPayload:[B

    invoke-interface {v8, v9}, Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver;->onRawTextMessage([B)V

    goto :goto_0

    .line 362
    :cond_3
    sget-object v9, Lde/tavendo/autobahn/WebSocketConnection;->TAG:Ljava/lang/String;

    const-string v10, "could not call onRawTextMessage() .. handler already NULL"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 365
    .end local v5    # "rawTextMessage":Lde/tavendo/autobahn/WebSocketMessage$RawTextMessage;
    :cond_4
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v9, v9, Lde/tavendo/autobahn/WebSocketMessage$BinaryMessage;

    if-eqz v9, :cond_6

    .line 366
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lde/tavendo/autobahn/WebSocketMessage$BinaryMessage;

    .line 368
    .local v0, "binaryMessage":Lde/tavendo/autobahn/WebSocketMessage$BinaryMessage;
    if-eqz v8, :cond_5

    .line 369
    iget-object v9, v0, Lde/tavendo/autobahn/WebSocketMessage$BinaryMessage;->mPayload:[B

    invoke-interface {v8, v9}, Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver;->onBinaryMessage([B)V

    goto :goto_0

    .line 371
    :cond_5
    sget-object v9, Lde/tavendo/autobahn/WebSocketConnection;->TAG:Ljava/lang/String;

    const-string v10, "could not call onBinaryMessage() .. handler already NULL"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 374
    .end local v0    # "binaryMessage":Lde/tavendo/autobahn/WebSocketMessage$BinaryMessage;
    :cond_6
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v9, v9, Lde/tavendo/autobahn/WebSocketMessage$Ping;

    if-eqz v9, :cond_7

    .line 375
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Lde/tavendo/autobahn/WebSocketMessage$Ping;

    .line 376
    .local v3, "ping":Lde/tavendo/autobahn/WebSocketMessage$Ping;
    sget-object v9, Lde/tavendo/autobahn/WebSocketConnection;->TAG:Ljava/lang/String;

    const-string v10, "WebSockets Ping received"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    new-instance v4, Lde/tavendo/autobahn/WebSocketMessage$Pong;

    invoke-direct {v4}, Lde/tavendo/autobahn/WebSocketMessage$Pong;-><init>()V

    .line 379
    .local v4, "pong":Lde/tavendo/autobahn/WebSocketMessage$Pong;
    iget-object v9, v3, Lde/tavendo/autobahn/WebSocketMessage$Ping;->mPayload:[B

    iput-object v9, v4, Lde/tavendo/autobahn/WebSocketMessage$Pong;->mPayload:[B

    .line 380
    iget-object v9, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketWriter:Lde/tavendo/autobahn/WebSocketWriter;

    invoke-virtual {v9, v4}, Lde/tavendo/autobahn/WebSocketWriter;->forward(Ljava/lang/Object;)V

    goto :goto_0

    .line 382
    .end local v3    # "ping":Lde/tavendo/autobahn/WebSocketMessage$Ping;
    .end local v4    # "pong":Lde/tavendo/autobahn/WebSocketMessage$Pong;
    :cond_7
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v9, v9, Lde/tavendo/autobahn/WebSocketMessage$Pong;

    if-eqz v9, :cond_8

    .line 383
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v4, Lde/tavendo/autobahn/WebSocketMessage$Pong;

    .line 385
    .restart local v4    # "pong":Lde/tavendo/autobahn/WebSocketMessage$Pong;
    sget-object v9, Lde/tavendo/autobahn/WebSocketConnection;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "WebSockets Pong received"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, v4, Lde/tavendo/autobahn/WebSocketMessage$Pong;->mPayload:[B

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 387
    .end local v4    # "pong":Lde/tavendo/autobahn/WebSocketMessage$Pong;
    :cond_8
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v9, v9, Lde/tavendo/autobahn/WebSocketMessage$Close;

    if-eqz v9, :cond_9

    .line 388
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lde/tavendo/autobahn/WebSocketMessage$Close;

    .line 390
    .local v1, "close":Lde/tavendo/autobahn/WebSocketMessage$Close;
    sget-object v9, Lde/tavendo/autobahn/WebSocketConnection;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "WebSockets Close received ("

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lde/tavendo/autobahn/WebSocketMessage$Close;->getCode()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Lde/tavendo/autobahn/WebSocketMessage$Close;->getReason()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ")"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    iget-object v9, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketWriter:Lde/tavendo/autobahn/WebSocketWriter;

    new-instance v10, Lde/tavendo/autobahn/WebSocketMessage$Close;

    const/16 v11, 0x3e8

    invoke-direct {v10, v11}, Lde/tavendo/autobahn/WebSocketMessage$Close;-><init>(I)V

    invoke-virtual {v9, v10}, Lde/tavendo/autobahn/WebSocketWriter;->forward(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 394
    .end local v1    # "close":Lde/tavendo/autobahn/WebSocketMessage$Close;
    :cond_9
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v9, v9, Lde/tavendo/autobahn/WebSocketMessage$ServerHandshake;

    if-eqz v9, :cond_b

    .line 395
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Lde/tavendo/autobahn/WebSocketMessage$ServerHandshake;

    .line 397
    .local v6, "serverHandshake":Lde/tavendo/autobahn/WebSocketMessage$ServerHandshake;
    sget-object v9, Lde/tavendo/autobahn/WebSocketConnection;->TAG:Ljava/lang/String;

    const-string v10, "opening handshake received"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    iget-boolean v9, v6, Lde/tavendo/autobahn/WebSocketMessage$ServerHandshake;->mSuccess:Z

    if-eqz v9, :cond_0

    .line 400
    if-eqz v8, :cond_a

    .line 401
    invoke-interface {v8}, Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver;->onOpen()V

    .line 405
    :goto_1
    const/4 v9, 0x1

    iput-boolean v9, p0, Lde/tavendo/autobahn/WebSocketConnection;->mPreviousConnection:Z

    goto/16 :goto_0

    .line 403
    :cond_a
    sget-object v9, Lde/tavendo/autobahn/WebSocketConnection;->TAG:Ljava/lang/String;

    const-string v10, "could not call onOpen() .. handler already NULL"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 408
    .end local v6    # "serverHandshake":Lde/tavendo/autobahn/WebSocketMessage$ServerHandshake;
    :cond_b
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v9, v9, Lde/tavendo/autobahn/WebSocketMessage$ConnectionLost;

    if-eqz v9, :cond_c

    .line 410
    sget-object v9, Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;->CONNECTION_LOST:Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;

    const-string v10, "WebSockets connection lost"

    invoke-direct {p0, v9, v10}, Lde/tavendo/autobahn/WebSocketConnection;->failConnection(Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 412
    :cond_c
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v9, v9, Lde/tavendo/autobahn/WebSocketMessage$ProtocolViolation;

    if-eqz v9, :cond_d

    .line 414
    sget-object v9, Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;->PROTOCOL_ERROR:Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;

    const-string v10, "WebSockets protocol violation"

    invoke-direct {p0, v9, v10}, Lde/tavendo/autobahn/WebSocketConnection;->failConnection(Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 416
    :cond_d
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v9, v9, Lde/tavendo/autobahn/WebSocketMessage$Error;

    if-eqz v9, :cond_e

    .line 417
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lde/tavendo/autobahn/WebSocketMessage$Error;

    .line 418
    .local v2, "error":Lde/tavendo/autobahn/WebSocketMessage$Error;
    sget-object v9, Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;->INTERNAL_ERROR:Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "WebSockets internal error ("

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, v2, Lde/tavendo/autobahn/WebSocketMessage$Error;->mException:Ljava/lang/Exception;

    invoke-virtual {v11}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ")"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v9, v10}, Lde/tavendo/autobahn/WebSocketConnection;->failConnection(Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 420
    .end local v2    # "error":Lde/tavendo/autobahn/WebSocketMessage$Error;
    :cond_e
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v9, v9, Lde/tavendo/autobahn/WebSocketMessage$ServerError;

    if-eqz v9, :cond_f

    .line 421
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lde/tavendo/autobahn/WebSocketMessage$ServerError;

    .line 422
    .local v2, "error":Lde/tavendo/autobahn/WebSocketMessage$ServerError;
    sget-object v9, Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;->SERVER_ERROR:Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Server error "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v11, v2, Lde/tavendo/autobahn/WebSocketMessage$ServerError;->mStatusCode:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " ("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v2, Lde/tavendo/autobahn/WebSocketMessage$ServerError;->mStatusMessage:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ")"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v9, v10}, Lde/tavendo/autobahn/WebSocketConnection;->failConnection(Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 425
    .end local v2    # "error":Lde/tavendo/autobahn/WebSocketMessage$ServerError;
    :cond_f
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {p0, v9}, Lde/tavendo/autobahn/WebSocketConnection;->processAppMessage(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method private onClose(Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;Ljava/lang/String;)V
    .locals 5
    .param p1, "code"    # Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;
    .param p2, "reason"    # Ljava/lang/String;

    .prologue
    .line 279
    const/4 v1, 0x0

    .line 281
    .local v1, "reconnecting":Z
    sget-object v3, Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;->CANNOT_CONNECT:Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;

    if-eq p1, v3, :cond_0

    sget-object v3, Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;->CONNECTION_LOST:Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;

    if-ne p1, v3, :cond_1

    .line 282
    :cond_0
    invoke-virtual {p0}, Lde/tavendo/autobahn/WebSocketConnection;->scheduleReconnect()Z

    move-result v1

    .line 285
    :cond_1
    iget-object v3, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketConnectionObserver:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver;

    .line 286
    .local v2, "webSocketObserver":Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver;
    if-eqz v2, :cond_3

    .line 288
    if-eqz v1, :cond_2

    .line 289
    :try_start_0
    sget-object v3, Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;->RECONNECT:Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;

    invoke-interface {v2, v3, p2}, Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver;->onClose(Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;Ljava/lang/String;)V

    .line 299
    :goto_0
    return-void

    .line 291
    :cond_2
    invoke-interface {v2, p1, p2}, Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver;->onClose(Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver$WebSocketCloseNotification;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 293
    :catch_0
    move-exception v0

    .line 294
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 297
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    sget-object v3, Lde/tavendo/autobahn/WebSocketConnection;->TAG:Ljava/lang/String;

    const-string v4, "WebSocketObserver null"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public connect(Ljava/net/URI;Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver;)V
    .locals 1
    .param p1, "webSocketURI"    # Ljava/net/URI;
    .param p2, "connectionObserver"    # Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lde/tavendo/autobahn/WebSocketException;
        }
    .end annotation

    .prologue
    .line 148
    new-instance v0, Lde/tavendo/autobahn/WebSocketOptions;

    invoke-direct {v0}, Lde/tavendo/autobahn/WebSocketOptions;-><init>()V

    invoke-virtual {p0, p1, p2, v0}, Lde/tavendo/autobahn/WebSocketConnection;->connect(Ljava/net/URI;Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver;Lde/tavendo/autobahn/WebSocketOptions;)V

    .line 149
    return-void
.end method

.method public connect(Ljava/net/URI;Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver;Lde/tavendo/autobahn/WebSocketOptions;)V
    .locals 1
    .param p1, "webSocketURI"    # Ljava/net/URI;
    .param p2, "connectionObserver"    # Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver;
    .param p3, "options"    # Lde/tavendo/autobahn/WebSocketOptions;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lde/tavendo/autobahn/WebSocketException;
        }
    .end annotation

    .prologue
    .line 152
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2, p3}, Lde/tavendo/autobahn/WebSocketConnection;->connect(Ljava/net/URI;[Ljava/lang/String;Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver;Lde/tavendo/autobahn/WebSocketOptions;)V

    .line 153
    return-void
.end method

.method public connect(Ljava/net/URI;[Ljava/lang/String;Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver;Lde/tavendo/autobahn/WebSocketOptions;)V
    .locals 2
    .param p1, "webSocketURI"    # Ljava/net/URI;
    .param p2, "subprotocols"    # [Ljava/lang/String;
    .param p3, "connectionObserver"    # Lde/tavendo/autobahn/WebSocket$WebSocketConnectionObserver;
    .param p4, "options"    # Lde/tavendo/autobahn/WebSocketOptions;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lde/tavendo/autobahn/WebSocketException;
        }
    .end annotation

    .prologue
    .line 156
    invoke-virtual {p0}, Lde/tavendo/autobahn/WebSocketConnection;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    new-instance v0, Lde/tavendo/autobahn/WebSocketException;

    const-string v1, "already connected"

    invoke-direct {v0, v1}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 160
    :cond_0
    if-nez p1, :cond_1

    .line 161
    new-instance v0, Lde/tavendo/autobahn/WebSocketException;

    const-string v1, "WebSockets URI null."

    invoke-direct {v0, v1}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163
    :cond_1
    iput-object p1, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketURI:Ljava/net/URI;

    .line 164
    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketURI:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ws"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketURI:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "wss"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 165
    new-instance v0, Lde/tavendo/autobahn/WebSocketException;

    const-string v1, "unsupported scheme for WebSockets URI"

    invoke-direct {v0, v1}, Lde/tavendo/autobahn/WebSocketException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 168
    :cond_2
    iput-object p2, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketSubprotocols:[Ljava/lang/String;

    .line 169
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketConnectionObserver:Ljava/lang/ref/WeakReference;

    .line 170
    new-instance v0, Lde/tavendo/autobahn/WebSocketOptions;

    invoke-direct {v0, p4}, Lde/tavendo/autobahn/WebSocketOptions;-><init>(Lde/tavendo/autobahn/WebSocketOptions;)V

    iput-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketOptions:Lde/tavendo/autobahn/WebSocketOptions;

    .line 172
    invoke-direct {p0}, Lde/tavendo/autobahn/WebSocketConnection;->connect()V

    .line 174
    return-void
.end method

.method protected createReader()V
    .locals 5

    .prologue
    .line 331
    new-instance v0, Lde/tavendo/autobahn/WebSocketReader;

    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketConnection;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lde/tavendo/autobahn/WebSocketConnection;->mSocket:Ljava/net/Socket;

    iget-object v3, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketOptions:Lde/tavendo/autobahn/WebSocketOptions;

    const-string v4, "WebSocketReader"

    invoke-direct {v0, v1, v2, v3, v4}, Lde/tavendo/autobahn/WebSocketReader;-><init>(Landroid/os/Handler;Ljava/net/Socket;Lde/tavendo/autobahn/WebSocketOptions;Ljava/lang/String;)V

    iput-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketReader:Lde/tavendo/autobahn/WebSocketReader;

    .line 332
    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketReader:Lde/tavendo/autobahn/WebSocketReader;

    invoke-virtual {v0}, Lde/tavendo/autobahn/WebSocketReader;->start()V

    .line 334
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketReader:Lde/tavendo/autobahn/WebSocketReader;

    monitor-enter v1

    .line 336
    :try_start_0
    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketReader:Lde/tavendo/autobahn/WebSocketReader;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 334
    :goto_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 341
    sget-object v0, Lde/tavendo/autobahn/WebSocketConnection;->TAG:Ljava/lang/String;

    const-string v1, "WebSocket reader created and started."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    return-void

    .line 334
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 337
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected createWriter()V
    .locals 5

    .prologue
    .line 312
    new-instance v0, Lde/tavendo/autobahn/WebSocketWriter;

    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketConnection;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lde/tavendo/autobahn/WebSocketConnection;->mSocket:Ljava/net/Socket;

    iget-object v3, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketOptions:Lde/tavendo/autobahn/WebSocketOptions;

    const-string v4, "WebSocketWriter"

    invoke-direct {v0, v1, v2, v3, v4}, Lde/tavendo/autobahn/WebSocketWriter;-><init>(Landroid/os/Handler;Ljava/net/Socket;Lde/tavendo/autobahn/WebSocketOptions;Ljava/lang/String;)V

    iput-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketWriter:Lde/tavendo/autobahn/WebSocketWriter;

    .line 313
    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketWriter:Lde/tavendo/autobahn/WebSocketWriter;

    invoke-virtual {v0}, Lde/tavendo/autobahn/WebSocketWriter;->start()V

    .line 315
    iget-object v1, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketWriter:Lde/tavendo/autobahn/WebSocketWriter;

    monitor-enter v1

    .line 317
    :try_start_0
    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketWriter:Lde/tavendo/autobahn/WebSocketWriter;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 315
    :goto_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 322
    sget-object v0, Lde/tavendo/autobahn/WebSocketConnection;->TAG:Ljava/lang/String;

    const-string v1, "WebSocket writer created and started."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    return-void

    .line 315
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 318
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public disconnect()V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketWriter:Lde/tavendo/autobahn/WebSocketWriter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketWriter:Lde/tavendo/autobahn/WebSocketWriter;

    invoke-virtual {v0}, Lde/tavendo/autobahn/WebSocketWriter;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketWriter:Lde/tavendo/autobahn/WebSocketWriter;

    new-instance v1, Lde/tavendo/autobahn/WebSocketMessage$Close;

    invoke-direct {v1}, Lde/tavendo/autobahn/WebSocketMessage$Close;-><init>()V

    invoke-virtual {v0, v1}, Lde/tavendo/autobahn/WebSocketWriter;->forward(Ljava/lang/Object;)V

    .line 183
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lde/tavendo/autobahn/WebSocketConnection;->mPreviousConnection:Z

    .line 184
    return-void

    .line 180
    :cond_0
    sget-object v0, Lde/tavendo/autobahn/WebSocketConnection;->TAG:Ljava/lang/String;

    const-string v1, "Could not send WebSocket Close .. writer already null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection;->mSocket:Ljava/net/Socket;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection;->mSocket:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection;->mSocket:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected processAppMessage(Ljava/lang/Object;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/Object;

    .prologue
    .line 305
    return-void
.end method

.method public reconnect()Z
    .locals 1

    .prologue
    .line 191
    invoke-virtual {p0}, Lde/tavendo/autobahn/WebSocketConnection;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketURI:Ljava/net/URI;

    if-eqz v0, :cond_0

    .line 192
    invoke-direct {p0}, Lde/tavendo/autobahn/WebSocketConnection;->connect()V

    .line 193
    const/4 v0, 0x1

    .line 195
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected scheduleReconnect()Z
    .locals 6

    .prologue
    .line 254
    iget-object v2, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketOptions:Lde/tavendo/autobahn/WebSocketOptions;

    invoke-virtual {v2}, Lde/tavendo/autobahn/WebSocketOptions;->getReconnectInterval()I

    move-result v0

    .line 255
    .local v0, "interval":I
    iget-object v2, p0, Lde/tavendo/autobahn/WebSocketConnection;->mSocket:Ljava/net/Socket;

    if-eqz v2, :cond_1

    .line 256
    iget-object v2, p0, Lde/tavendo/autobahn/WebSocketConnection;->mSocket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 257
    iget-boolean v2, p0, Lde/tavendo/autobahn/WebSocketConnection;->mPreviousConnection:Z

    if-eqz v2, :cond_1

    .line 255
    if-lez v0, :cond_1

    const/4 v1, 0x1

    .line 259
    .local v1, "shouldReconnect":Z
    :goto_0
    if-eqz v1, :cond_0

    .line 260
    sget-object v2, Lde/tavendo/autobahn/WebSocketConnection;->TAG:Ljava/lang/String;

    const-string v3, "WebSocket reconnection scheduled"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    iget-object v2, p0, Lde/tavendo/autobahn/WebSocketConnection;->mHandler:Landroid/os/Handler;

    new-instance v3, Lde/tavendo/autobahn/WebSocketConnection$4;

    invoke-direct {v3, p0}, Lde/tavendo/autobahn/WebSocketConnection$4;-><init>(Lde/tavendo/autobahn/WebSocketConnection;)V

    .line 267
    int-to-long v4, v0

    .line 261
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 269
    :cond_0
    return v1

    .line 255
    .end local v1    # "shouldReconnect":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public sendBinaryMessage([B)V
    .locals 2
    .param p1, "payload"    # [B

    .prologue
    .line 82
    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketWriter:Lde/tavendo/autobahn/WebSocketWriter;

    new-instance v1, Lde/tavendo/autobahn/WebSocketMessage$BinaryMessage;

    invoke-direct {v1, p1}, Lde/tavendo/autobahn/WebSocketMessage$BinaryMessage;-><init>([B)V

    invoke-virtual {v0, v1}, Lde/tavendo/autobahn/WebSocketWriter;->forward(Ljava/lang/Object;)V

    .line 83
    return-void
.end method

.method public sendRawTextMessage([B)V
    .locals 2
    .param p1, "payload"    # [B

    .prologue
    .line 77
    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketWriter:Lde/tavendo/autobahn/WebSocketWriter;

    new-instance v1, Lde/tavendo/autobahn/WebSocketMessage$RawTextMessage;

    invoke-direct {v1, p1}, Lde/tavendo/autobahn/WebSocketMessage$RawTextMessage;-><init>([B)V

    invoke-virtual {v0, v1}, Lde/tavendo/autobahn/WebSocketWriter;->forward(Ljava/lang/Object;)V

    .line 78
    return-void
.end method

.method public sendTextMessage(Ljava/lang/String;)V
    .locals 2
    .param p1, "payload"    # Ljava/lang/String;

    .prologue
    .line 72
    iget-object v0, p0, Lde/tavendo/autobahn/WebSocketConnection;->mWebSocketWriter:Lde/tavendo/autobahn/WebSocketWriter;

    new-instance v1, Lde/tavendo/autobahn/WebSocketMessage$TextMessage;

    invoke-direct {v1, p1}, Lde/tavendo/autobahn/WebSocketMessage$TextMessage;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lde/tavendo/autobahn/WebSocketWriter;->forward(Ljava/lang/Object;)V

    .line 73
    return-void
.end method
