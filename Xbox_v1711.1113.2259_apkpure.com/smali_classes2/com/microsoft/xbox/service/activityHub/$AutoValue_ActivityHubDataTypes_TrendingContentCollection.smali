.class abstract Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingContentCollection;
.super Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;
.source "$AutoValue_ActivityHubDataTypes_TrendingContentCollection.java"


# instance fields
.field private final description:Ljava/lang/String;

.field private final items:Lcom/google/gson/JsonArray;

.field private final title:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/gson/JsonArray;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "description"    # Ljava/lang/String;
    .param p3, "items"    # Lcom/google/gson/JsonArray;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;-><init>()V

    .line 18
    if-nez p1, :cond_0

    .line 19
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null title"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 21
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingContentCollection;->title:Ljava/lang/String;

    .line 22
    if-nez p2, :cond_1

    .line 23
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null description"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 25
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingContentCollection;->description:Ljava/lang/String;

    .line 26
    if-nez p3, :cond_2

    .line 27
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null items"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingContentCollection;->items:Lcom/google/gson/JsonArray;

    .line 30
    return-void
.end method


# virtual methods
.method public description()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingContentCollection;->description:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58
    if-ne p1, p0, :cond_1

    .line 67
    :cond_0
    :goto_0
    return v1

    .line 61
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 62
    check-cast v0, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;

    .line 63
    .local v0, "that":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingContentCollection;->title:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;->title()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingContentCollection;->description:Ljava/lang/String;

    .line 64
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;->description()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingContentCollection;->items:Lcom/google/gson/JsonArray;

    .line 65
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;->items()Lcom/google/gson/JsonArray;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/gson/JsonArray;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;
    :cond_3
    move v1, v2

    .line 67
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 72
    const/4 v0, 0x1

    .line 73
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 74
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingContentCollection;->title:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 75
    mul-int/2addr v0, v2

    .line 76
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingContentCollection;->description:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 77
    mul-int/2addr v0, v2

    .line 78
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingContentCollection;->items:Lcom/google/gson/JsonArray;

    invoke-virtual {v1}, Lcom/google/gson/JsonArray;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 79
    return v0
.end method

.method public items()Lcom/google/gson/JsonArray;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingContentCollection;->items:Lcom/google/gson/JsonArray;

    return-object v0
.end method

.method public title()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingContentCollection;->title:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TrendingContentCollection{title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingContentCollection;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingContentCollection;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityHub/$AutoValue_ActivityHubDataTypes_TrendingContentCollection;->items:Lcom/google/gson/JsonArray;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
