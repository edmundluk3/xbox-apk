.class public Lcom/microsoft/playready/networkdevice/TransmitterAuthenticationData;
.super Ljava/lang/Object;
.source "TransmitterAuthenticationData.java"


# instance fields
.field public final CustomResponseData:Lcom/microsoft/playready/networkdevice/CustomData;

.field public final TransmitterCertificate:Lcom/microsoft/playready/networkdevice/TransmitterCertificate;


# direct methods
.method public constructor <init>(Lcom/microsoft/playready/networkdevice/TransmitterCertificate;Lcom/microsoft/playready/networkdevice/CustomData;)V
    .locals 0
    .param p1, "transmitterCertificate"    # Lcom/microsoft/playready/networkdevice/TransmitterCertificate;
    .param p2, "customResponseData"    # Lcom/microsoft/playready/networkdevice/CustomData;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/TransmitterAuthenticationData;->TransmitterCertificate:Lcom/microsoft/playready/networkdevice/TransmitterCertificate;

    .line 33
    iput-object p2, p0, Lcom/microsoft/playready/networkdevice/TransmitterAuthenticationData;->CustomResponseData:Lcom/microsoft/playready/networkdevice/CustomData;

    .line 34
    return-void
.end method
