.class public Lcom/microsoft/xbox/smartglass/internal/Random;
.super Ljava/lang/Object;
.source "Random.java"


# instance fields
.field private _rand:Ljava/security/SecureRandom;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/internal/Random;->_rand:Ljava/security/SecureRandom;

    .line 10
    return-void
.end method


# virtual methods
.method generateBytes(I)[B
    .locals 2
    .param p1, "count"    # I

    .prologue
    .line 13
    new-array v0, p1, [B

    .line 14
    .local v0, "data":[B
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/internal/Random;->_rand:Ljava/security/SecureRandom;

    invoke-virtual {v1, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 15
    return-object v0
.end method
