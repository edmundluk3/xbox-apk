.class public interface abstract Lcom/microsoft/xbox/xle/viewmodel/IAddFollowingUserManager;
.super Ljava/lang/Object;
.source "IAddFollowingUserManager.java"


# virtual methods
.method public abstract onAddFollowingUserNoAction(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract postAddFollowingUser(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract preAddFollowingUser()V
.end method
