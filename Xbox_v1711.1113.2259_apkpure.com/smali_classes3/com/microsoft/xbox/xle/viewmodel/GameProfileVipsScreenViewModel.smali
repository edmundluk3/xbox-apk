.class public Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;
.source "GameProfileVipsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;
    }
.end annotation


# instance fields
.field private loadGameProfileVipsTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 0
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePeopleViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 22
    return-void
.end method


# virtual methods
.method public isBusy()Z
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;->loadGameProfileVipsTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;->loadGameProfileVipsTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;->getIsBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;->loadGameProfileVipsTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;->loadGameProfileVipsTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;->cancel()V

    .line 69
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;->loadGameProfileVipsTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;->loadGameProfileVipsTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;->load(Z)V

    .line 71
    return-void
.end method

.method protected onLoadGameProfileVipsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 6
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 41
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 61
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;->updateAdapter()V

    .line 62
    return-void

    .line 45
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGameProfileVipsModel()Lcom/microsoft/xbox/service/model/GameProfileVipsModel;

    move-result-object v2

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;->gameTitleId:J

    invoke-virtual {v2, v4, v5}, Lcom/microsoft/xbox/service/model/GameProfileVipsModel;->getResult(J)Ljava/util/ArrayList;

    move-result-object v1

    .line 46
    .local v1, "vips":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/GameProfileVipData;>;"
    if-eqz v1, :cond_0

    .line 47
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;->peopleList:Ljava/util/ArrayList;

    .line 48
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/GameProfileVipData;

    .line 49
    .local v0, "vip":Lcom/microsoft/xbox/service/model/GameProfileVipData;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;->peopleList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 52
    .end local v0    # "vip":Lcom/microsoft/xbox/service/model/GameProfileVipData;
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;->updateViewModelState()V

    goto :goto_0

    .line 56
    .end local v1    # "vips":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/GameProfileVipData;>;"
    :pswitch_1
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;->peopleList:Ljava/util/ArrayList;

    .line 57
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 41
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 31
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;->loadGameProfileVipsTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel;->loadGameProfileVipsTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileVipsScreenViewModel$LoadGameProfileVipsAsyncTask;->cancel()V

    .line 38
    :cond_0
    return-void
.end method
