.class public interface abstract Lcom/microsoft/xbox/xle/ui/SearchBarView$OnSearchBarListener;
.super Ljava/lang/Object;
.source "SearchBarView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/SearchBarView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnSearchBarListener"
.end annotation


# virtual methods
.method public abstract onClear()V
.end method

.method public abstract onQueryTextChange(Ljava/lang/CharSequence;)V
.end method

.method public abstract onQueryTextSubmit(Ljava/lang/CharSequence;)V
.end method
