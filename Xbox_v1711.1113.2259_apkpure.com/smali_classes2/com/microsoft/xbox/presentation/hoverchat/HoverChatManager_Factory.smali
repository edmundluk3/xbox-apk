.class public final Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;
.super Ljava/lang/Object;
.source "HoverChatManager_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final chatHeadManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/flipkart/chatheads/ui/ChatHeadManager",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;>;"
        }
    .end annotation
.end field

.field private final chatHeadViewAdapterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;>;"
        }
    .end annotation
.end field

.field private final contentDescriptionDownloaderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadContentDescriptionDownloaderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final dialogManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;",
            ">;"
        }
    .end annotation
.end field

.field private final imageDownloaderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloaderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final navigationManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/NavigationManager;",
            ">;"
        }
    .end annotation
.end field

.field private final repositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final schedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/flipkart/chatheads/ui/ChatHeadManager",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;>;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;>;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloaderFactory;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadContentDescriptionDownloaderFactory;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/NavigationManager;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 50
    .local p1, "chatHeadManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/flipkart/chatheads/ui/ChatHeadManager<Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;>;>;"
    .local p2, "chatHeadViewAdapterProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter<Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;>;>;"
    .local p3, "repositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;>;"
    .local p4, "imageDownloaderFactoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloaderFactory;>;"
    .local p5, "contentDescriptionDownloaderFactoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadContentDescriptionDownloaderFactory;>;"
    .local p6, "navigationManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/ui/NavigationManager;>;"
    .local p7, "dialogManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;>;"
    .local p8, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    sget-boolean v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 52
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->chatHeadManagerProvider:Ljavax/inject/Provider;

    .line 53
    sget-boolean v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 54
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->chatHeadViewAdapterProvider:Ljavax/inject/Provider;

    .line 55
    sget-boolean v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 56
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->repositoryProvider:Ljavax/inject/Provider;

    .line 57
    sget-boolean v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 58
    :cond_3
    iput-object p4, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->imageDownloaderFactoryProvider:Ljavax/inject/Provider;

    .line 59
    sget-boolean v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 60
    :cond_4
    iput-object p5, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->contentDescriptionDownloaderFactoryProvider:Ljavax/inject/Provider;

    .line 61
    sget-boolean v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 62
    :cond_5
    iput-object p6, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->navigationManagerProvider:Ljavax/inject/Provider;

    .line 63
    sget-boolean v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_6

    if-nez p7, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 64
    :cond_6
    iput-object p7, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->dialogManagerProvider:Ljavax/inject/Provider;

    .line 65
    sget-boolean v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_7

    if-nez p8, :cond_7

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 66
    :cond_7
    iput-object p8, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->schedulerProvider:Ljavax/inject/Provider;

    .line 67
    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/flipkart/chatheads/ui/ChatHeadManager",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;>;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            ">;>;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloaderFactory;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadContentDescriptionDownloaderFactory;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/NavigationManager;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    .local p0, "chatHeadManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/flipkart/chatheads/ui/ChatHeadManager<Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;>;>;"
    .local p1, "chatHeadViewAdapterProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter<Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;>;>;"
    .local p2, "repositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;>;"
    .local p3, "imageDownloaderFactoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloaderFactory;>;"
    .local p4, "contentDescriptionDownloaderFactoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadContentDescriptionDownloaderFactory;>;"
    .local p5, "navigationManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/ui/NavigationManager;>;"
    .local p6, "dialogManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;>;"
    .local p7, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    new-instance v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;
    .locals 9

    .prologue
    .line 71
    new-instance v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->chatHeadManagerProvider:Ljavax/inject/Provider;

    .line 72
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/flipkart/chatheads/ui/ChatHeadManager;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->chatHeadViewAdapterProvider:Ljavax/inject/Provider;

    .line 73
    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->repositoryProvider:Ljavax/inject/Provider;

    .line 74
    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    iget-object v4, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->imageDownloaderFactoryProvider:Ljavax/inject/Provider;

    .line 75
    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloaderFactory;

    iget-object v5, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->contentDescriptionDownloaderFactoryProvider:Ljavax/inject/Provider;

    .line 76
    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadContentDescriptionDownloaderFactory;

    iget-object v6, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->navigationManagerProvider:Ljavax/inject/Provider;

    .line 77
    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    iget-object v7, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->dialogManagerProvider:Ljavax/inject/Provider;

    .line 78
    invoke-interface {v7}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    iget-object v8, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->schedulerProvider:Ljavax/inject/Provider;

    .line 79
    invoke-interface {v8}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    invoke-direct/range {v0 .. v8}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;-><init>(Lcom/flipkart/chatheads/ui/ChatHeadManager;Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadImageDownload$HoverChatHeadImageDownloaderFactory;Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadParticipantsDownload$HoverChatHeadContentDescriptionDownloaderFactory;Lcom/microsoft/xbox/toolkit/ui/NavigationManager;Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V

    .line 71
    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager_Factory;->get()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;

    move-result-object v0

    return-object v0
.end method
