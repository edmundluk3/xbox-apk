.class final Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;
.super Lcom/microsoft/xbox/presentation/lce/LceViewState;
.source "AutoValue_LceViewState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/microsoft/xbox/presentation/lce/LceViewState",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final content:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final error:Ljava/lang/Throwable;

.field private final isLoading:Z

.field private final isLoadingMore:Z


# direct methods
.method constructor <init>(Ljava/lang/Object;Ljava/lang/Throwable;ZZ)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "error"    # Ljava/lang/Throwable;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "isLoading"    # Z
    .param p4, "isLoadingMore"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/lang/Throwable;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p0, "this":Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;, "Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState<TT;>;"
    .local p1, "content":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/lce/LceViewState;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;->content:Ljava/lang/Object;

    .line 21
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;->error:Ljava/lang/Throwable;

    .line 22
    iput-boolean p3, p0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;->isLoading:Z

    .line 23
    iput-boolean p4, p0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;->isLoadingMore:Z

    .line 24
    return-void
.end method


# virtual methods
.method public content()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 29
    .local p0, "this":Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;, "Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;->content:Ljava/lang/Object;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;, "Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState<TT;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 60
    if-ne p1, p0, :cond_1

    .line 70
    :cond_0
    :goto_0
    return v1

    .line 63
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/presentation/lce/LceViewState;

    if-eqz v3, :cond_5

    move-object v0, p1

    .line 64
    check-cast v0, Lcom/microsoft/xbox/presentation/lce/LceViewState;

    .line 65
    .local v0, "that":Lcom/microsoft/xbox/presentation/lce/LceViewState;, "Lcom/microsoft/xbox/presentation/lce/LceViewState<*>;"
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;->content:Ljava/lang/Object;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->content()Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;->error:Ljava/lang/Throwable;

    if-nez v3, :cond_4

    .line 66
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->error()Ljava/lang/Throwable;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-boolean v3, p0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;->isLoading:Z

    .line 67
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->isLoading()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;->isLoadingMore:Z

    .line 68
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->isLoadingMore()Z

    move-result v4

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 65
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;->content:Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->content()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 66
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;->error:Ljava/lang/Throwable;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->error()Ljava/lang/Throwable;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .end local v0    # "that":Lcom/microsoft/xbox/presentation/lce/LceViewState;, "Lcom/microsoft/xbox/presentation/lce/LceViewState<*>;"
    :cond_5
    move v1, v2

    .line 70
    goto :goto_0
.end method

.method public error()Ljava/lang/Throwable;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 35
    .local p0, "this":Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;, "Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;->error:Ljava/lang/Throwable;

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;, "Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState<TT;>;"
    const/16 v4, 0x4d5

    const/16 v3, 0x4cf

    const/4 v2, 0x0

    const v5, 0xf4243

    .line 75
    const/4 v0, 0x1

    .line 76
    .local v0, "h":I
    mul-int/2addr v0, v5

    .line 77
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;->content:Ljava/lang/Object;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 78
    mul-int/2addr v0, v5

    .line 79
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;->error:Ljava/lang/Throwable;

    if-nez v1, :cond_1

    :goto_1
    xor-int/2addr v0, v2

    .line 80
    mul-int/2addr v0, v5

    .line 81
    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;->isLoading:Z

    if-eqz v1, :cond_2

    move v1, v3

    :goto_2
    xor-int/2addr v0, v1

    .line 82
    mul-int/2addr v0, v5

    .line 83
    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;->isLoadingMore:Z

    if-eqz v1, :cond_3

    :goto_3
    xor-int/2addr v0, v3

    .line 84
    return v0

    .line 77
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;->content:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    .line 79
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;->error:Ljava/lang/Throwable;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_2
    move v1, v4

    .line 81
    goto :goto_2

    :cond_3
    move v3, v4

    .line 83
    goto :goto_3
.end method

.method public isLoading()Z
    .locals 1

    .prologue
    .line 40
    .local p0, "this":Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;, "Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState<TT;>;"
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;->isLoading:Z

    return v0
.end method

.method public isLoadingMore()Z
    .locals 1

    .prologue
    .line 45
    .local p0, "this":Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;, "Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState<TT;>;"
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;->isLoadingMore:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 50
    .local p0, "this":Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;, "Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState<TT;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LceViewState{content="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;->content:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;->error:Ljava/lang/Throwable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isLoading="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;->isLoading:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isLoadingMore="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/lce/AutoValue_LceViewState;->isLoadingMore:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
