.class public Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;
.super Ljava/lang/Object;
.source "UserSummaryFromPeopleHubPersonDataMapper.java"

# interfaces
.implements Lcom/microsoft/xbox/data/repository/DataMapper;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/data/repository/DataMapper",
        "<",
        "Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;",
        "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method


# virtual methods
.method public apply(Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;)Lio/reactivex/Observable;
    .locals 14
    .param p1, "data"    # Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->xuid()J

    move-result-wide v0

    .line 28
    .local v0, "xuid":J
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->gamertag()Ljava/lang/String;

    move-result-object v2

    .line 29
    .local v2, "gamertag":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->realName()Ljava/lang/String;

    move-result-object v11

    const-string v12, ""

    invoke-static {v11, v12}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 30
    .local v3, "realName":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->displayPicRaw()Ljava/lang/String;

    move-result-object v11

    const-string v12, ""

    invoke-static {v11, v12}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 31
    .local v4, "displayPicUri":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->gamerScore()J

    move-result-wide v6

    .line 32
    .local v6, "gamerScore":J
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->presenceText()Ljava/lang/String;

    move-result-object v11

    const-string v12, ""

    invoke-static {v11, v12}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 33
    .local v5, "presenceText":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->preferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v11

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->defaultProfileColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .line 34
    .local v8, "colors":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->multiplayerSummary()Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;

    move-result-object v10

    .line 35
    .local v10, "summary":Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;
    if-eqz v10, :cond_0

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;->inParty()I

    move-result v11

    if-lez v11, :cond_0

    const/4 v9, 0x1

    .line 37
    .local v9, "isInParty":Z
    :goto_0
    const-wide/16 v12, 0x0

    cmp-long v11, v0, v12

    if-lez v11, :cond_1

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 38
    invoke-static/range {v0 .. v9}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->with(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;Z)Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    move-result-object v11

    invoke-static {v11}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v11

    .line 41
    :goto_1
    return-object v11

    .line 35
    .end local v9    # "isInParty":Z
    :cond_0
    const/4 v9, 0x0

    goto :goto_0

    .line 40
    .restart local v9    # "isInParty":Z
    :cond_1
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Failed to map user summary: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 41
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v11

    goto :goto_1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Lio/reactivex/Observable;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 18
    check-cast p1, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;->apply(Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 18
    check-cast p1, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;->apply(Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
