.class public final enum Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;
.super Ljava/lang/Enum;
.source "SubscriptionType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

.field public static final enum EAAccess:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

.field public static final enum Gamepass:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

.field public static final enum Unknown:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

.field public static final enum XboxGold:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

.field private static final stringToSubscriptionTypeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private productId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 12
    new-instance v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    const-string v3, "Unknown"

    const/4 v4, 0x0

    invoke-direct {v2, v3, v1, v4}, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->Unknown:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    new-instance v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    const-string v3, "XboxGold"

    const-string v4, "CFQ7TTC0K5DJ"

    invoke-direct {v2, v3, v5, v4}, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->XboxGold:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    new-instance v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    const-string v3, "EAAccess"

    const-string v4, "CFQ7TTC0K5DH"

    invoke-direct {v2, v3, v6, v4}, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->EAAccess:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    new-instance v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    const-string v3, "Gamepass"

    const-string v4, "CFQ7TTC0K6L8"

    invoke-direct {v2, v3, v7, v4}, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->Gamepass:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    .line 11
    const/4 v2, 0x4

    new-array v2, v2, [Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    sget-object v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->Unknown:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->XboxGold:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    aput-object v3, v2, v5

    sget-object v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->EAAccess:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    aput-object v3, v2, v6

    sget-object v3, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->Gamepass:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    aput-object v3, v2, v7

    sput-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->$VALUES:[Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    .line 24
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->stringToSubscriptionTypeMap:Ljava/util/Map;

    .line 25
    invoke-static {}, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->values()[Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    move-result-object v2

    array-length v3, v2

    .local v0, "type":Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 26
    sget-object v4, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->stringToSubscriptionTypeMap:Ljava/util/Map;

    sget-object v5, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->XboxGold:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    iget-object v5, v5, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->productId:Ljava/lang/String;

    sget-object v6, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->XboxGold:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    sget-object v4, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->stringToSubscriptionTypeMap:Ljava/util/Map;

    sget-object v5, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->EAAccess:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    iget-object v5, v5, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->productId:Ljava/lang/String;

    sget-object v6, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->EAAccess:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    sget-object v4, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->stringToSubscriptionTypeMap:Ljava/util/Map;

    sget-object v5, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->Gamepass:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    iget-object v5, v5, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->productId:Ljava/lang/String;

    sget-object v6, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->Gamepass:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 30
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "productId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 20
    iput-object p3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->productId:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public static getSubscriptionFromAffirmationProductId(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;
    .locals 1
    .param p0, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 34
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 36
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->stringToSubscriptionTypeMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->stringToSubscriptionTypeMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->Unknown:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    const-class v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->$VALUES:[Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    return-object v0
.end method


# virtual methods
.method public getProductId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->productId:Ljava/lang/String;

    return-object v0
.end method
