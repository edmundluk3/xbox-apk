.class Lcom/microsoft/xbox/smartglass/controls/Accelerometer$Listener;
.super Lcom/microsoft/xbox/smartglass/AccelerometerListener;
.source "Accelerometer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/smartglass/controls/Accelerometer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Listener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/smartglass/controls/Accelerometer;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/smartglass/controls/Accelerometer;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/smartglass/controls/Accelerometer;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/Accelerometer$Listener;->this$0:Lcom/microsoft/xbox/smartglass/controls/Accelerometer;

    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/AccelerometerListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onReadingChanged(Lcom/microsoft/xbox/smartglass/Accelerometer;Lcom/microsoft/xbox/smartglass/AccelerometerReading;)V
    .locals 8
    .param p1, "accelerometer"    # Lcom/microsoft/xbox/smartglass/Accelerometer;
    .param p2, "reading"    # Lcom/microsoft/xbox/smartglass/AccelerometerReading;

    .prologue
    .line 56
    :try_start_0
    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/JsonAccelerometer;

    iget-wide v2, p2, Lcom/microsoft/xbox/smartglass/AccelerometerReading;->x:D

    iget-wide v4, p2, Lcom/microsoft/xbox/smartglass/AccelerometerReading;->y:D

    iget-wide v6, p2, Lcom/microsoft/xbox/smartglass/AccelerometerReading;->z:D

    invoke-direct/range {v1 .. v7}, Lcom/microsoft/xbox/smartglass/controls/JsonAccelerometer;-><init>(DDD)V

    .line 57
    .local v1, "json":Lcom/microsoft/xbox/smartglass/controls/JsonAccelerometer;
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Accelerometer$Listener;->this$0:Lcom/microsoft/xbox/smartglass/controls/Accelerometer;

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/controls/Accelerometer;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v2, "accelerometer"

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/controls/JsonAccelerometer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->raiseEvent(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    .end local v1    # "json":Lcom/microsoft/xbox/smartglass/controls/JsonAccelerometer;
    :goto_0
    return-void

    .line 58
    :catch_0
    move-exception v0

    goto :goto_0
.end method
