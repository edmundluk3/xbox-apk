.class public interface abstract Lcom/microsoft/playready/networkdevice/IReceiverSession;
.super Ljava/lang/Object;
.source "IReceiverSession.java"


# static fields
.field public static final MaximumExpirationTimeInSeconds:I = 0x2a300


# virtual methods
.method public abstract addSessionExpirationListener(Lcom/microsoft/playready/networkdevice/ISessionExpirationListener;)V
.end method

.method public abstract addSessionExpirationListener(Lcom/microsoft/playready/networkdevice/ISessionExpirationListener;JLjava/util/concurrent/TimeUnit;)V
.end method

.method public abstract close()V
.end method

.method public abstract fetchLicense(Lcom/microsoft/playready/networkdevice/ContentIDType;[B)Z
.end method

.method public abstract getID()[B
.end method

.method public abstract getReceiverPlugin()Lcom/microsoft/playready/networkdevice/IReceiverPlugin;
.end method

.method public abstract getSessionStartTime()Ljava/util/Date;
.end method

.method public abstract getTransmitter()Lcom/microsoft/playready/networkdevice/TransmitterInfo;
.end method

.method public abstract removeSessionExpirationListener(Lcom/microsoft/playready/networkdevice/ISessionExpirationListener;)V
.end method

.method public abstract restartSession()Lcom/microsoft/playready/networkdevice/IReceiverSessionTask;
.end method

.method public abstract toString()Ljava/lang/String;
.end method
