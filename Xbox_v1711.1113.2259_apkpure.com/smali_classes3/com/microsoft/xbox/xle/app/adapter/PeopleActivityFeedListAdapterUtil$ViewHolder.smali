.class public Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "PeopleActivityFeedListAdapterUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ViewHolder"
.end annotation


# instance fields
.field public achievementIconView:Landroid/widget/TextView;

.field public achievementItemText:Landroid/widget/TextView;

.field public achievementRareIconView:Landroid/widget/TextView;

.field public achievementScoreView:Landroid/widget/TextView;

.field public actionView:Landroid/widget/TextView;

.field public addFriendButton:Landroid/widget/RelativeLayout;

.field public commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field public contentTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field public dateView:Landroid/widget/TextView;

.field public dismissButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field public gameprofileOrLinkButton:Landroid/widget/RelativeLayout;

.field public gamertagTextView:Landroid/widget/TextView;

.field public itemText:Landroid/widget/TextView;

.field public leaderboardContainer:Landroid/view/View;

.field public leaderboardSubtitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field public leaderboardTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field public legacyAchievementItemImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field public lfgContainer:Landroid/view/View;

.field public lfgDescription:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field public lfgPostedTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field public lfgTagAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

.field public lfgTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field public lfgTitleImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field public likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

.field public metadataLayout:Landroid/view/ViewGroup;

.field public moreActionButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field public parent:Landroid/view/View;

.field public pinIcon:Landroid/widget/TextView;

.field public playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field public presenceIcon:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

.field public primaryGamerContainer:Landroid/view/View;

.field public primaryGamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field public primaryGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field public primaryGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field public realnameView:Landroid/widget/TextView;

.field public recommendationGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field public recommendationIconImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field public recommendationIconText:Landroid/view/View;

.field public recommendationPersonView:Landroid/view/View;

.field public recommendationProfileImg:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field public recommendationProfileView:Landroid/view/View;

.field public recommendationRealname:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field public recommendationReason:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field public recommendationRemoveButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field public screenshotIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field public screenshotIconClickable:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field public screenshotLayout:Landroid/view/ViewGroup;

.field public screenshotView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field public secondaryGamerContainer:Landroid/view/View;

.field public secondaryGamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field public secondaryGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field public secondaryGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field public seeAllRecommendations:Landroid/widget/RelativeLayout;

.field public seeAllTrending:Landroid/widget/RelativeLayout;

.field public self:Landroid/view/View;

.field public shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field public sharedHolder:Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;

.field public socialBar:Landroid/widget/LinearLayout;

.field public socialBarValueComments:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

.field public socialBarValueLikes:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

.field public socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

.field public socialBarValuesContainer:Landroid/view/View;

.field public tagBackgroundColor:I

.field public targetGeneric:Landroid/view/View;

.field public targetUserImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field public titleImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field public trendingDescription:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field public ugcCaption:Landroid/widget/TextView;

.field public undoButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field public userImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field public webLinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "itemView"    # Landroid/view/View;

    .prologue
    .line 1293
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 1286
    const v0, 0x7f0c014e

    iput v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->tagBackgroundColor:I

    .line 1294
    return-void
.end method


# virtual methods
.method public init()V
    .locals 5

    .prologue
    .line 1297
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->self:Landroid/view/View;

    .line 1298
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e08b1

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->userImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 1299
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e08b2

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->titleImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 1300
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0889

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->targetGeneric:Landroid/view/View;

    .line 1301
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e088a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->targetUserImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 1302
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e088e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotLayout:Landroid/view/ViewGroup;

    .line 1303
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e08b3

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->metadataLayout:Landroid/view/ViewGroup;

    .line 1304
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e088f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 1305
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0891

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 1306
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0890

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIconClickable:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 1307
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0894

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementScoreView:Landroid/widget/TextView;

    .line 1308
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0893

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    .line 1309
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0892

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementRareIconView:Landroid/widget/TextView;

    .line 1310
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e08b5

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->gamertagTextView:Landroid/widget/TextView;

    .line 1311
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e08b9

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->actionView:Landroid/widget/TextView;

    .line 1312
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e08b7

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->dateView:Landroid/widget/TextView;

    .line 1313
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e08b8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->pinIcon:Landroid/widget/TextView;

    .line 1314
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e08b6

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->realnameView:Landroid/widget/TextView;

    .line 1315
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0899

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBar:Landroid/widget/LinearLayout;

    .line 1316
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0192

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/LikeControl;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    .line 1317
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0896

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementItemText:Landroid/widget/TextView;

    .line 1318
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0888

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->ugcCaption:Landroid/widget/TextView;

    .line 1319
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e088b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    .line 1320
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0194

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 1321
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0196

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 1322
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0198

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->moreActionButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 1323
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0231

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->presenceIcon:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 1326
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0193

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueLikes:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    .line 1327
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0195

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueComments:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    .line 1328
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0197

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    .line 1329
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e019b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValuesContainer:Landroid/view/View;

    .line 1331
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0895

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->gameprofileOrLinkButton:Landroid/widget/RelativeLayout;

    .line 1334
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e06d2

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->trendingDescription:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1335
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e06d3

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->seeAllTrending:Landroid/widget/RelativeLayout;

    .line 1338
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0897

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->webLinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1341
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0898

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->legacyAchievementItemImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 1344
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0872

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardContainer:Landroid/view/View;

    .line 1345
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0874

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->primaryGamerContainer:Landroid/view/View;

    .line 1346
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0875

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->primaryGamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 1347
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0876

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->primaryGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1348
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0877

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->primaryGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1349
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0878

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->secondaryGamerContainer:Landroid/view/View;

    .line 1350
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0879

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->secondaryGamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 1351
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e087a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->secondaryGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1352
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e087b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->secondaryGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1353
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e087c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1354
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e087d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardSubtitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1356
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e087e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgContainer:Landroid/view/View;

    .line 1357
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0880

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgTitleImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 1358
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0881

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1359
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0882

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgPostedTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1360
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0884

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgDescription:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1362
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    iget v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->tagBackgroundColor:I

    .line 1364
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgTagAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    .line 1366
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0883

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;

    .line 1368
    .local v0, "tagsList":Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;
    if-eqz v0, :cond_0

    .line 1369
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgTagAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->setAdapter(Landroid/widget/Adapter;)V

    .line 1370
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->setMaxRows(I)V

    .line 1376
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e08af

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->undoButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 1377
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e08b0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->dismissButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 1378
    return-void

    .line 1372
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->TAG:Ljava/lang/String;

    const-string v2, "Could not find tag list"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public initAsAttachement()V
    .locals 5

    .prologue
    .line 1381
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->self:Landroid/view/View;

    .line 1382
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0889

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->targetGeneric:Landroid/view/View;

    .line 1383
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e08a0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->targetUserImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 1384
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e08a2

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotLayout:Landroid/view/ViewGroup;

    .line 1385
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e089b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->metadataLayout:Landroid/view/ViewGroup;

    .line 1386
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e08a3

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 1387
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e08a5

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 1388
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e08a7

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementScoreView:Landroid/widget/TextView;

    .line 1389
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e08a6

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    .line 1390
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0892

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementRareIconView:Landroid/widget/TextView;

    .line 1391
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e089c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->gamertagTextView:Landroid/widget/TextView;

    .line 1392
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e089e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->actionView:Landroid/widget/TextView;

    .line 1393
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e089d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->realnameView:Landroid/widget/TextView;

    .line 1394
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e08a9

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementItemText:Landroid/widget/TextView;

    .line 1395
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e08a1

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    .line 1396
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0231

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->presenceIcon:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 1397
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e08a4

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1398
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e08aa

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->webLinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1399
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e08a8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->legacyAchievementItemImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 1401
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0872

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardContainer:Landroid/view/View;

    .line 1402
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0874

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->primaryGamerContainer:Landroid/view/View;

    .line 1403
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0875

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->primaryGamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 1404
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0876

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->primaryGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1405
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0877

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->primaryGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1406
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0878

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->secondaryGamerContainer:Landroid/view/View;

    .line 1407
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0879

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->secondaryGamerpic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 1408
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e087a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->secondaryGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1409
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e087b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->secondaryGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1410
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e087c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1411
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e087d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->leaderboardSubtitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1414
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e087e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgContainer:Landroid/view/View;

    .line 1415
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0880

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgTitleImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 1416
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0881

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1417
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0882

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgPostedTime:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1418
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0884

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgDescription:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1420
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    iget v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->tagBackgroundColor:I

    .line 1422
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgTagAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    .line 1424
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v2, 0x7f0e0883

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;

    .line 1426
    .local v0, "tagsList":Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;
    if-eqz v0, :cond_0

    .line 1427
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->lfgTagAdapter:Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->setAdapter(Landroid/widget/Adapter;)V

    .line 1428
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TagFlowLayout;->setMaxRows(I)V

    .line 1432
    :goto_0
    return-void

    .line 1430
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->TAG:Ljava/lang/String;

    const-string v2, "Could not find tag list"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public initAsSocialRecommendation()V
    .locals 2

    .prologue
    .line 1435
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->self:Landroid/view/View;

    .line 1438
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e06c5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->recommendationPersonView:Landroid/view/View;

    .line 1439
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e06c8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->seeAllRecommendations:Landroid/widget/RelativeLayout;

    .line 1440
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e03d4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->recommendationProfileImg:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 1441
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e06c9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->recommendationProfileView:Landroid/view/View;

    .line 1442
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e06ca

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->recommendationRemoveButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 1443
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e06cb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->recommendationGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1444
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e06cc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->recommendationRealname:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1445
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e06cd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->recommendationIconImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 1446
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e06ce

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->recommendationIconText:Landroid/view/View;

    .line 1447
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e06cf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->recommendationReason:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1448
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e06d0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->addFriendButton:Landroid/widget/RelativeLayout;

    .line 1449
    return-void
.end method

.method public initAsTrendingContent()V
    .locals 2

    .prologue
    .line 1452
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->self:Landroid/view/View;

    .line 1453
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0889

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->targetGeneric:Landroid/view/View;

    .line 1454
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e08bd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->targetUserImageView:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 1455
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e08bf

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotLayout:Landroid/view/ViewGroup;

    .line 1456
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e08c8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->metadataLayout:Landroid/view/ViewGroup;

    .line 1457
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e08c0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 1458
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e08c2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->playButtonView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 1459
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e08c4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementScoreView:Landroid/widget/TextView;

    .line 1460
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e08c3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementIconView:Landroid/widget/TextView;

    .line 1461
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0892

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementRareIconView:Landroid/widget/TextView;

    .line 1462
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e08c9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->gamertagTextView:Landroid/widget/TextView;

    .line 1463
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e08ca

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->realnameView:Landroid/widget/TextView;

    .line 1464
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e08c6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->achievementItemText:Landroid/widget/TextView;

    .line 1465
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e08be

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    .line 1466
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0231

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->presenceIcon:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 1467
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e08cb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->ugcCaption:Landroid/widget/TextView;

    .line 1468
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e08cc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->contentTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1469
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemText:Landroid/widget/TextView;

    const v1, 0x7f0e08c1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->screenshotIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1470
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e08c7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->webLinkText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 1471
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0194

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->commentButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 1472
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0196

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->shareButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 1473
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0198

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->moreActionButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 1476
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e08cd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBar:Landroid/widget/LinearLayout;

    .line 1477
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0192

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/LikeControl;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->likeControl:Lcom/microsoft/xbox/xle/ui/LikeControl;

    .line 1478
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0193

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueLikes:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    .line 1479
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0195

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueComments:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    .line 1480
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0197

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValueShares:Lcom/microsoft/xbox/xle/ui/SocialBarValue;

    .line 1481
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e019b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->socialBarValuesContainer:Landroid/view/View;

    .line 1483
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e08c5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;->legacyAchievementItemImage:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 1484
    return-void
.end method
