.class Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$2;
.super Ljava/lang/Object;
.source "CompanionSession.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->notifySessionStateChanged(Lcom/microsoft/xbox/smartglass/ConnectionState;Lcom/microsoft/xbox/smartglass/SGResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

.field final synthetic val$result:Lcom/microsoft/xbox/smartglass/SGResult;

.field final synthetic val$state:Lcom/microsoft/xbox/smartglass/ConnectionState;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;Lcom/microsoft/xbox/smartglass/ConnectionState;Lcom/microsoft/xbox/smartglass/SGResult;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    .prologue
    .line 594
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$2;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$2;->val$state:Lcom/microsoft/xbox/smartglass/ConnectionState;

    iput-object p3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$2;->val$result:Lcom/microsoft/xbox/smartglass/SGResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    .line 596
    const/4 v7, 0x0

    .line 607
    .local v7, "timeConsumed":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Notify session state changed to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$2;->val$state:Lcom/microsoft/xbox/smartglass/ConnectionState;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$2;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    iget-object v8, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->datalock:Ljava/lang/Object;

    monitor-enter v8

    .line 609
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$2;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$2;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$2;->val$state:Lcom/microsoft/xbox/smartglass/ConnectionState;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->convertConnectionStateToSessionState(Lcom/microsoft/xbox/smartglass/ConnectionState;)I

    move-result v3

    iput v3, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->sessionState:I

    .line 610
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "companion session state is now "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$2;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    iget v4, v4, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->sessionState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    const/4 v1, 0x0

    .line 613
    .local v1, "error":Lcom/microsoft/xbox/toolkit/XLEException;
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$2;->val$state:Lcom/microsoft/xbox/smartglass/ConnectionState;

    sget-object v3, Lcom/microsoft/xbox/smartglass/ConnectionState;->Error:Lcom/microsoft/xbox/smartglass/ConnectionState;

    if-ne v2, v3, :cond_1

    .line 614
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$2;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$2;->val$result:Lcom/microsoft/xbox/smartglass/SGResult;

    iget-object v3, v3, Lcom/microsoft/xbox/smartglass/SGResult;->error:Lcom/microsoft/xbox/smartglass/SGError;

    invoke-virtual {v3}, Lcom/microsoft/xbox/smartglass/SGError;->getValue()I

    move-result v3

    int-to-long v4, v3

    iput-wide v4, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->lastErrorCode:J

    .line 615
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v1    # "error":Lcom/microsoft/xbox/toolkit/XLEException;
    const-wide/16 v2, 0x7d1

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$2;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    iget-wide v10, v6, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->lastErrorCode:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;)V

    .line 616
    .restart local v1    # "error":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Connection to console failed with error code: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$2;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    iget-wide v4, v4, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->lastErrorCode:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    :goto_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 629
    if-eqz v7, :cond_0

    .line 630
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->access$000()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "Notify session state changed done. Time used=%dms"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->currentTime()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->currentTime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 632
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "spent too much time in onConnectionStateChanged"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    :cond_0
    return-void

    .line 618
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$2;->val$state:Lcom/microsoft/xbox/smartglass/ConnectionState;

    sget-object v3, Lcom/microsoft/xbox/smartglass/ConnectionState;->Disconnected:Lcom/microsoft/xbox/smartglass/ConnectionState;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$2;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    iget-wide v2, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->lastErrorCode:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 619
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v1    # "error":Lcom/microsoft/xbox/toolkit/XLEException;
    const-wide/16 v2, 0x7d1

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$2;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    iget-wide v10, v6, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->lastErrorCode:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/String;Ljava/lang/Throwable;Ljava/lang/Object;)V

    .line 622
    .restart local v1    # "error":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$2;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    iget-object v3, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->sessionStateListenerList:Ljava/util/LinkedList;

    monitor-enter v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 623
    :try_start_2
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$2;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->sessionStateListenerList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionStateListener;

    .line 624
    .local v0, "listener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionStateListener;
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$2;->this$0:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    iget v4, v4, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->sessionState:I

    invoke-interface {v0, v4, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionStateListener;->onSessionStateChanged(ILcom/microsoft/xbox/toolkit/XLEException;)V

    goto :goto_1

    .line 626
    .end local v0    # "listener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionStateListener;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v2

    .line 628
    .end local v1    # "error":Lcom/microsoft/xbox/toolkit/XLEException;
    :catchall_1
    move-exception v2

    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2

    .line 626
    .restart local v1    # "error":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_3
    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method
