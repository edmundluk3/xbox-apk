.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2Genre;
.super Ljava/lang/Object;
.source "EDSV2Genre.java"


# instance fields
.field public final Name:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 14
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Genre;->Name:Ljava/lang/String;

    .line 15
    return-void
.end method
