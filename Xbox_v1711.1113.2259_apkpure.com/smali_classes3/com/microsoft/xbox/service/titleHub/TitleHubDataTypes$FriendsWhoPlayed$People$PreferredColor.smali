.class public final Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;
.super Ljava/lang/Object;
.source "TitleHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PreferredColor"
.end annotation


# instance fields
.field private volatile transient hashCode:I

.field public final primaryColor:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final secondaryColor:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final tertiaryColor:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "primaryColor"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "secondaryColor"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "tertiaryColor"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 580
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 581
    iput-object p1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;->primaryColor:Ljava/lang/String;

    .line 582
    iput-object p2, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;->secondaryColor:Ljava/lang/String;

    .line 583
    iput-object p3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;->tertiaryColor:Ljava/lang/String;

    .line 584
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 588
    if-ne p1, p0, :cond_1

    .line 594
    :cond_0
    :goto_0
    return v1

    .line 590
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;

    if-nez v3, :cond_2

    move v1, v2

    .line 591
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 593
    check-cast v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;

    .line 594
    .local v0, "other":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;
    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;->primaryColor:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;->primaryColor:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;->secondaryColor:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;->secondaryColor:Ljava/lang/String;

    .line 595
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;->tertiaryColor:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;->tertiaryColor:Ljava/lang/String;

    .line 596
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 602
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;->hashCode:I

    if-nez v0, :cond_0

    .line 603
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;->hashCode:I

    .line 604
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;->primaryColor:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;->hashCode:I

    .line 605
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;->secondaryColor:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;->hashCode:I

    .line 606
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;->tertiaryColor:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;->hashCode:I

    .line 609
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 614
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
