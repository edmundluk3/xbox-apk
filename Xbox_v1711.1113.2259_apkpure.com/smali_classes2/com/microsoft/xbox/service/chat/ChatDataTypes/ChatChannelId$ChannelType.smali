.class public final enum Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;
.super Ljava/lang/Enum;
.source "ChatChannelId.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ChannelType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

.field public static final enum Club:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

.field public static final enum Custom:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

.field public static final enum Title:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

.field public static final enum Unknown:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

.field public static final enum User:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;


# instance fields
.field value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 19
    new-instance v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    const-string v1, "Unknown"

    invoke-direct {v0, v1, v3, v3}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->Unknown:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    .line 20
    new-instance v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    const-string v1, "Club"

    const v2, 0x434c5542

    invoke-direct {v0, v1, v4, v2}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->Club:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    .line 21
    new-instance v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    const-string v1, "Custom"

    const v2, 0x43555354

    invoke-direct {v0, v1, v5, v2}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->Custom:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    .line 22
    new-instance v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    const-string v1, "User"

    const v2, 0x58554944

    invoke-direct {v0, v1, v6, v2}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->User:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    .line 23
    new-instance v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    const-string v1, "Title"

    const v2, 0x5449544c

    invoke-direct {v0, v1, v7, v2}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->Title:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    .line 18
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    sget-object v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->Unknown:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->Club:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->Custom:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->User:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->Title:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->$VALUES:[Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 28
    iput p3, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->value:I

    .line 29
    return-void
.end method

.method public static fromValue(I)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;
    .locals 5
    .param p0, "value"    # I

    .prologue
    .line 36
    invoke-static {}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->values()[Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 37
    .local v0, "type":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->getValue()I

    move-result v4

    if-ne v4, p0, :cond_0

    .line 42
    .end local v0    # "type":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;
    :goto_1
    return-object v0

    .line 36
    .restart local v0    # "type":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 42
    .end local v0    # "type":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->Unknown:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 18
    const-class v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->$VALUES:[Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->value:I

    return v0
.end method
