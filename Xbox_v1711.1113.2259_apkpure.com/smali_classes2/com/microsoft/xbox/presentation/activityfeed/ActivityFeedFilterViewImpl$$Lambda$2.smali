.class final synthetic Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl$$Lambda$2;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function5;


# static fields
.field private static final instance:Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl$$Lambda$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl$$Lambda$2;

    invoke-direct {v0}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl$$Lambda$2;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl$$Lambda$2;->instance:Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl$$Lambda$2;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static lambdaFactory$()Lio/reactivex/functions/Function5;
    .locals 1

    sget-object v0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl$$Lambda$2;->instance:Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewImpl$$Lambda$2;

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    check-cast p3, Ljava/lang/Boolean;

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    check-cast p4, Ljava/lang/Boolean;

    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    check-cast p5, Ljava/lang/Boolean;

    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->with(ZZZZZ)Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    move-result-object v0

    return-object v0
.end method
