.class public Lcom/microsoft/xbox/xle/ui/ComparisonBar;
.super Landroid/widget/LinearLayout;
.source "ComparisonBar.java"


# instance fields
.field private isSpace:Z

.field private leftBar:Landroid/view/View;

.field private leftEdge:Landroid/view/View;

.field private rightBar:Landroid/view/View;

.field private rightEdge:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->initViews(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->initViews(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method private initViews(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/high16 v8, 0x41400000    # 12.0f

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const v6, 0x7f0c0142

    const/4 v5, -0x1

    .line 37
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setOrientation(I)V

    .line 38
    sget-object v2, Lcom/microsoft/xboxone/smartglass/R$styleable;->ComparisonBar:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 39
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->isSpace:Z

    .line 41
    new-instance v2, Landroid/view/View;

    invoke-direct {v2, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->leftBar:Landroid/view/View;

    .line 42
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->leftBar:Landroid/view/View;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x2

    invoke-direct {v3, v4, v5, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 43
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->leftBar:Landroid/view/View;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 46
    new-instance v2, Landroid/view/View;

    invoke-direct {v2, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightBar:Landroid/view/View;

    .line 47
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightBar:Landroid/view/View;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x2

    invoke-direct {v3, v4, v5, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 48
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightBar:Landroid/view/View;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 51
    new-instance v2, Landroid/view/View;

    invoke-direct {v2, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->leftEdge:Landroid/view/View;

    .line 52
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->leftEdge:Landroid/view/View;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->DIPtoPixels(F)I

    move-result v4

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 53
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->leftEdge:Landroid/view/View;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 56
    new-instance v2, Landroid/view/View;

    invoke-direct {v2, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightEdge:Landroid/view/View;

    .line 57
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightEdge:Landroid/view/View;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->DIPtoPixels(F)I

    move-result v4

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 58
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightEdge:Landroid/view/View;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 61
    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 62
    .local v1, "spacer":Landroid/view/View;
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v3, 0x40c00000    # 6.0f

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->DIPtoPixels(F)I

    move-result v3

    invoke-direct {v2, v3, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 63
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0c0145

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 65
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->leftEdge:Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->addView(Landroid/view/View;)V

    .line 66
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->leftBar:Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->addView(Landroid/view/View;)V

    .line 67
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->isSpace:Z

    if-eqz v2, :cond_0

    .line 68
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->addView(Landroid/view/View;)V

    .line 70
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightBar:Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->addView(Landroid/view/View;)V

    .line 71
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightEdge:Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->addView(Landroid/view/View;)V

    .line 72
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 73
    return-void
.end method


# virtual methods
.method public setExactValues(JJ)V
    .locals 9
    .param p1, "leftValue"    # J
    .param p3, "rightValue"    # J

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, -0x1

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->leftBar:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightBar:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 117
    cmp-long v0, p1, v6

    if-gtz v0, :cond_2

    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->leftBar:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->leftEdge:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->leftEdge:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 130
    :cond_0
    :goto_0
    cmp-long v0, p3, v6

    if-gtz v0, :cond_4

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightBar:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightEdge:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 133
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightEdge:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 143
    :cond_1
    :goto_1
    return-void

    .line 123
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->leftBar:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->leftEdge:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->leftEdge:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 127
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->leftBar:Landroid/view/View;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    long-to-float v2, p1

    invoke-direct {v1, v3, v5, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 136
    :cond_4
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightBar:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightEdge:Landroid/view/View;

    if-eqz v0, :cond_5

    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightEdge:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 140
    :cond_5
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightBar:Landroid/view/View;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    long-to-float v2, p3

    invoke-direct {v1, v3, v5, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1
.end method

.method public setLeftBarColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->leftBar:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->leftBar:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->leftEdge:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->leftEdge:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 82
    :cond_1
    return-void
.end method

.method public setRightBarAlpha(F)V
    .locals 1
    .param p1, "val"    # F

    .prologue
    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightBar:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightBar:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightEdge:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightEdge:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 100
    :cond_1
    return-void
.end method

.method public setRightBarColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightBar:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightBar:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightEdge:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightEdge:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 91
    :cond_1
    return-void
.end method

.method public setValues(JJ)V
    .locals 9
    .param p1, "leftValue"    # J
    .param p3, "rightValue"    # J

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->leftBar:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightBar:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 104
    cmp-long v0, p1, v6

    if-nez v0, :cond_0

    cmp-long v0, p3, v6

    if-nez v0, :cond_0

    .line 106
    const-wide/16 p1, 0x1

    .line 107
    const-wide/16 p3, 0x1

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->leftBar:Landroid/view/View;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    long-to-float v2, p1

    invoke-direct {v1, v4, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->rightBar:Landroid/view/View;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    long-to-float v2, p3

    invoke-direct {v1, v4, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 113
    :cond_1
    return-void
.end method
