.class Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$6;
.super Ljava/lang/Object;
.source "MultiplayerTelemetryService.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;->handleEvents(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

.field final synthetic val$event:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    .prologue
    .line 188
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$6;->this$0:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    iput-object p2, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$6;->val$event:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 4

    .prologue
    .line 192
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$6;->val$event:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 193
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$6;->val$event:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    instance-of v1, v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/DiagnosticEvent;

    if-eqz v1, :cond_1

    .line 194
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$6;->val$event:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$6;->this$0:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    iget-object v3, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$6;->val$event:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;->access$000(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCDiagnostic;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 195
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$6;->val$event:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    instance-of v1, v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    if-eqz v1, :cond_0

    .line 196
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService$6;->val$event:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;

    check-cast v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;

    .line 197
    .local v0, "error":Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;->getError()Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->getDescription()Ljava/lang/String;

    move-result-object v1

    .line 198
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;->getError()Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->getCode()Ljava/lang/String;

    move-result-object v2

    .line 199
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/ErrorEvent;->getException()Ljava/lang/Exception;

    move-result-object v3

    .line 197
    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->trackPartyChatError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method
