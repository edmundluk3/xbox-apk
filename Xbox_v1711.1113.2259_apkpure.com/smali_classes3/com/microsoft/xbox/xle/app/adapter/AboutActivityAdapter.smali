.class public Lcom/microsoft/xbox/xle/app/adapter/AboutActivityAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "AboutActivityAdapter.java"


# instance fields
.field private aboutText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/AboutActivityViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/AboutActivityViewModel;)V
    .locals 1
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/AboutActivityViewModel;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/AboutActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AboutActivityViewModel;

    .line 27
    const v0, 0x7f0e010e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AboutActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AboutActivityAdapter;->aboutText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 28
    const v0, 0x7f0e010d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/AboutActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AboutActivityAdapter;->screenBody:Landroid/view/View;

    .line 29
    return-void
.end method


# virtual methods
.method public updateViewOverride()V
    .locals 3

    .prologue
    .line 33
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070df4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "aboutText":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AboutActivityAdapter;->aboutText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 35
    return-void
.end method
