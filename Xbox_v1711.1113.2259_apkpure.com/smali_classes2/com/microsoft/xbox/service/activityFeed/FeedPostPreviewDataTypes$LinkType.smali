.class public final enum Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;
.super Ljava/lang/Enum;
.source "FeedPostPreviewDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LinkType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;

.field public static final enum Default:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;

.field public static final enum YouTube:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;


# instance fields
.field private linkType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22
    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;

    const-string v1, "Default"

    const-string v2, "default"

    invoke-direct {v0, v1, v3, v2}, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;->Default:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;

    .line 23
    new-instance v0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;

    const-string v1, "YouTube"

    const-string/jumbo v2, "youTube"

    invoke-direct {v0, v1, v4, v2}, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;->YouTube:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;

    .line 20
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;

    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;->Default:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;->YouTube:Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;->$VALUES:[Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "linkType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 28
    iput-object p3, p0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;->linkType:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;->$VALUES:[Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/FeedPostPreviewDataTypes$LinkType;->linkType:Ljava/lang/String;

    return-object v0
.end method
