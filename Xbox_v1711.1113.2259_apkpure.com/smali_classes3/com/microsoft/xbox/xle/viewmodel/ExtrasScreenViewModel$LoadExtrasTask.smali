.class public Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ExtrasScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "LoadExtrasTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    .prologue
    .line 163
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 167
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    move-result-object v1

    if-nez v1, :cond_1

    .line 170
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->shouldRefresh()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->shouldRefresh()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 12

    .prologue
    const/16 v11, 0x2329

    const/4 v10, 0x1

    .line 197
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-static {v8}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v8

    iget-boolean v9, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->forceLoad:Z

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v7

    .line 199
    .local v7, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 200
    sget-object v8, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 239
    :goto_0
    return-object v8

    .line 203
    :cond_0
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-static {v8}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getIsBundle()Z

    move-result v8

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-static {v8}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaType()I

    move-result v8

    if-eq v8, v11, :cond_1

    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-static {v8}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaType()I

    move-result v8

    const/16 v9, 0x2328

    if-ne v8, v9, :cond_2

    .line 204
    :cond_1
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-static {v8}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBundlePrimaryItemId()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 206
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-virtual {v8, v10}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->setShouldHideScreen(Z)V

    .line 220
    :cond_2
    :goto_1
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->getShouldHideScreen()Z

    move-result v8

    if-nez v8, :cond_6

    .line 221
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-static {v8}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaType()I

    move-result v3

    .line 222
    .local v3, "mediaType":I
    if-eq v3, v11, :cond_3

    if-ne v3, v10, :cond_5

    .line 223
    :cond_3
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-static {v8}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    .line 224
    .local v0, "gameModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;
    if-eqz v0, :cond_5

    .line 226
    iget-boolean v8, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->forceLoad:Z

    invoke-virtual {v0, v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->loadAddOns(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v8

    goto :goto_0

    .line 208
    .end local v0    # "gameModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;
    .end local v3    # "mediaType":I
    :cond_4
    new-instance v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-direct {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 209
    .local v5, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-static {v8}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBundlePrimaryItemId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setBigCatProductId(Ljava/lang/String;)V

    .line 210
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-static {v8}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->access$400(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v8

    iget-object v8, v8, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    iput-object v8, v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    .line 211
    invoke-static {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 212
    .local v6, "primaryItemModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    iget-boolean v8, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->forceLoad:Z

    invoke-virtual {v6, v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v7

    .line 213
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 215
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-static {v8, v6}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->access$102(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    goto :goto_1

    .line 230
    .end local v5    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .end local v6    # "primaryItemModel":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .restart local v3    # "mediaType":I
    :cond_5
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-static {v8}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v2

    .line 231
    .local v2, "mediaId":Ljava/lang/String;
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-static {v8}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v8

    iget-object v4, v8, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    .line 232
    .local v4, "mediaTypeString":Ljava/lang/String;
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-static {v8}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaGroup()I

    move-result v1

    .line 233
    .local v1, "mediaGroup":I
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-static {v2, v4, v1}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getModel(Ljava/lang/String;Ljava/lang/String;I)Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->access$002(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;)Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    .line 234
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-static {v8}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    move-result-object v8

    if-eqz v8, :cond_6

    .line 235
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-static {v8}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;)Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    move-result-object v8

    iget-boolean v9, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->forceLoad:Z

    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->loadSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v8

    goto/16 :goto_0

    .line 239
    .end local v1    # "mediaGroup":I
    .end local v2    # "mediaId":Ljava/lang/String;
    .end local v3    # "mediaType":I
    .end local v4    # "mediaTypeString":Ljava/lang/String;
    :cond_6
    sget-object v8, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto/16 :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 192
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 177
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 188
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 163
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->access$302(Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;Z)Z

    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel$LoadExtrasTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ExtrasScreenViewModel;->updateAdapter()V

    .line 183
    return-void
.end method
