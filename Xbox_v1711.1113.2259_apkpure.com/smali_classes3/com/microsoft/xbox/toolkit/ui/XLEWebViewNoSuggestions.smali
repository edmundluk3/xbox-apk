.class public Lcom/microsoft/xbox/toolkit/ui/XLEWebViewNoSuggestions;
.super Lcom/microsoft/xbox/toolkit/ui/XLEWebView;
.source "XLEWebViewNoSuggestions.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/XLEWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    return-void
.end method


# virtual methods
.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 2
    .param p1, "outAttrs"    # Landroid/view/inputmethod/EditorInfo;

    .prologue
    .line 19
    invoke-super {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEWebView;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    .line 21
    .local v0, "ic":Landroid/view/inputmethod/InputConnection;
    iget v1, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    and-int/lit16 v1, v1, -0xff1

    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    .line 22
    iget v1, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    or-int/lit16 v1, v1, 0xe0

    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    .line 24
    return-object v0
.end method
