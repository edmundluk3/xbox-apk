.class public Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;
.super Ljava/lang/Object;
.source "TelemetryProvider.java"


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private final events:Lio/reactivex/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/PublishSubject",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {}, Lio/reactivex/subjects/PublishSubject;->create()Lio/reactivex/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->events:Lio/reactivex/subjects/PublishSubject;

    .line 26
    return-void
.end method


# virtual methods
.method public getEvents()Lio/reactivex/subjects/PublishSubject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/subjects/PublishSubject",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->events:Lio/reactivex/subjects/PublishSubject;

    return-object v0
.end method

.method public send(Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;)V
    .locals 1
    .param p1, "event"    # Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 42
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 43
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 44
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/events/TelemetryEventBase;->getPayload()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;->events:Lio/reactivex/subjects/PublishSubject;

    invoke-virtual {v0, p1}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    .line 47
    return-void
.end method
