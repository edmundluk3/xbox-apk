.class public final Lcom/microsoft/xbox/xle/viewmodel/BiEvents;
.super Ljava/lang/Object;
.source "BiEvents.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;,
        Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;,
        Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;,
        Lcom/microsoft/xbox/xle/viewmodel/BiEvents$StaticFeedItemBiMap;,
        Lcom/microsoft/xbox/xle/viewmodel/BiEvents$Event;,
        Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;,
        Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;
    }
.end annotation


# static fields
.field public static final ACTIONS_DETAIL_FEED:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

.field public static final ACTIVITY_FEED:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

.field public static final CONVERSATION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 16
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$StaticFeedItemBiMap;

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents;->getConversationEvents()Ljava/util/Map;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$StaticFeedItemBiMap;

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents;->getConversationEvents()Ljava/util/Map;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$StaticFeedItemBiMap;-><init>(Ljava/util/Map;)V

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$StaticFeedItemBiMap;-><init>(Ljava/util/Map;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents;->CONVERSATION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

    .line 17
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$StaticFeedItemBiMap;

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents;->getActivityFeedEvents()Ljava/util/Map;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$StaticFeedItemBiMap;

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents;->getActivityFeedSharedEvents()Ljava/util/Map;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$StaticFeedItemBiMap;-><init>(Ljava/util/Map;)V

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$StaticFeedItemBiMap;-><init>(Ljava/util/Map;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents;->ACTIVITY_FEED:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

    .line 18
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$StaticFeedItemBiMap;

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents;->getActionsDetailEvents()Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$StaticFeedItemBiMap;-><init>(Ljava/util/Map;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents;->ACTIONS_DETAIL_FEED:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    return-void
.end method

.method private static getActionsDetailEvents()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;",
            "Lcom/microsoft/xbox/xle/viewmodel/BiEvents$Event;",
            ">;"
        }
    .end annotation

    .prologue
    .line 151
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private static getActivityFeedEvents()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;",
            "Lcom/microsoft/xbox/xle/viewmodel/BiEvents$Event;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 119
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$Event;>;"
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->PROFILE:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_PROFILE_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->TARGET_PROFILE:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SHARE_PROFILE_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_ACHIEVEMENT:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_ACHIEVEMENT_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_BROADCAST:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_BROADCAST_PLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_GAME_CLIP:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_GAME_DVR_PLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_FRIEND_ADD:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_ADD_FRIEND_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_STATUS_POST:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_STATUS_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_GENERIC:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_PROFILE_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_SCREENSHOT:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SCREENSHOT_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_SCREENSHOT_GLOBAL:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->GLOBAL_SCREENSHOT_DISPLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    return-object v0
.end method

.method private static getActivityFeedSharedEvents()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;",
            "Lcom/microsoft/xbox/xle/viewmodel/BiEvents$Event;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 135
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$Event;>;"
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->PROFILE:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SHARE_PROFILE_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->TARGET_PROFILE:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SHARE_PROFILE_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_ACHIEVEMENT:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SHARE_ACHIEVEMENT_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_BROADCAST:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SHARE_BROADCAST_PLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_GAME_CLIP:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SHARE_GAME_DVR_PLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_SCREENSHOT:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SHARE_SCREENSHOT_DISPLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_FRIEND_ADD:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SHARE_ADD_FRIEND_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_STATUS_POST:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SHARE_STATUS_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_GENERIC:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SHARE_PROFILE_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_SCREENSHOT_GLOBAL:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->GLOBAL_SCREENSHOT_DISPLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    return-object v0
.end method

.method private static getConversationEvents()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;",
            "Lcom/microsoft/xbox/xle/viewmodel/BiEvents$Event;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 104
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$Event;>;"
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->PROFILE:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_PROFILE_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->TARGET_PROFILE:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->MESSAGE_ATTACHMENT_SHARED_ITEM_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_ACHIEVEMENT:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->MESSAGE_ATTACHMENT_ACHIEVEMENT_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_BROADCAST:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->MESSAGE_ATTACHMENT_BROADCAST_PLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_GAME_CLIP:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->MESSAGE_ATTACHMENT_GAME_DVR_PLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_SCREENSHOT:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->MESSAGE_ATTACHMENT_SCREENSHOT_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_GENERIC:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->MESSAGE_ATTACHMENT_SHARED_ITEM_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_SCREENSHOT_GLOBAL:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;

    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->GLOBAL_SCREENSHOT_DISPLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageActionEvent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    return-object v0
.end method
