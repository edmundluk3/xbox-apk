.class Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter$1;
.super Ljava/lang/Object;
.source "ActivityAlertScreenAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 47
    .local v0, "item":Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 48
    instance-of v1, v0, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    if-eqz v1, :cond_1

    .line 49
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;

    move-result-object v1

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;

    .end local v0    # "item":Ljava/lang/Object;
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->navigateToActivityFeedScreen(Lcom/microsoft/xbox/service/network/managers/CommentAlertResultContainer$Action;)V

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 51
    .restart local v0    # "item":Ljava/lang/Object;
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/ActivityAlertScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;

    move-result-object v1

    check-cast v0, Lcom/microsoft/xbox/service/model/FollowersData;

    .end local v0    # "item":Ljava/lang/Object;
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityAlertViewModel;->navigateToProfile(Lcom/microsoft/xbox/service/model/FollowersData;)V

    goto :goto_0
.end method
