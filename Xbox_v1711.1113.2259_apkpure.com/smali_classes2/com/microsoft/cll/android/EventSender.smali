.class public Lcom/microsoft/cll/android/EventSender;
.super Ljava/lang/Object;
.source "EventSender.java"


# instance fields
.field private final NO_HTTPS_CONN:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field private final clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

.field private final endpoint:Ljava/net/URL;

.field private final logger:Lcom/microsoft/cll/android/ILogger;


# direct methods
.method public constructor <init>(Ljava/net/URL;Lcom/microsoft/cll/android/ClientTelemetry;Lcom/microsoft/cll/android/ILogger;)V
    .locals 1
    .param p1, "endpoint"    # Ljava/net/URL;
    .param p2, "clientTelemetry"    # Lcom/microsoft/cll/android/ClientTelemetry;
    .param p3, "logger"    # Lcom/microsoft/cll/android/ILogger;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-string v0, "URL didn\'t return HttpsUrlConnection instance."

    iput-object v0, p0, Lcom/microsoft/cll/android/EventSender;->NO_HTTPS_CONN:Ljava/lang/String;

    .line 26
    const-string v0, "EventSender"

    iput-object v0, p0, Lcom/microsoft/cll/android/EventSender;->TAG:Ljava/lang/String;

    .line 33
    iput-object p1, p0, Lcom/microsoft/cll/android/EventSender;->endpoint:Ljava/net/URL;

    .line 34
    iput-object p2, p0, Lcom/microsoft/cll/android/EventSender;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    .line 35
    iput-object p3, p0, Lcom/microsoft/cll/android/EventSender;->logger:Lcom/microsoft/cll/android/ILogger;

    .line 36
    return-void
.end method

.method private getTime()J
    .locals 2

    .prologue
    .line 198
    const-string v0, "UTC"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v0, v1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method protected getResponseBody(Ljava/io/BufferedReader;)Ljava/lang/String;
    .locals 8
    .param p1, "reader"    # Ljava/io/BufferedReader;

    .prologue
    .line 167
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 170
    .local v4, "responseBuilder":Ljava/lang/StringBuilder;
    :goto_0
    :try_start_0
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .local v2, "line":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 171
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 173
    .end local v2    # "line":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 174
    .local v0, "e":Ljava/io/IOException;
    iget-object v5, p0, Lcom/microsoft/cll/android/EventSender;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v6, "EventSender"

    const-string v7, "Couldn\'t read response body"

    invoke-interface {v5, v6, v7}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    :try_start_1
    new-instance v1, Lorg/json/JSONObject;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 180
    .local v1, "jsonObject":Lorg/json/JSONObject;
    const-string v5, "rej"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 181
    .local v3, "rejectCount":I
    iget-object v5, p0, Lcom/microsoft/cll/android/EventSender;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    invoke-virtual {v5, v3}, Lcom/microsoft/cll/android/ClientTelemetry;->IncremenetRejectDropCount(I)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_2

    .line 188
    .end local v1    # "jsonObject":Lorg/json/JSONObject;
    .end local v3    # "rejectCount":I
    :goto_1
    iget-object v5, p0, Lcom/microsoft/cll/android/EventSender;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v6, "EventSender"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 182
    :catch_1
    move-exception v0

    .line 183
    .local v0, "e":Lorg/json/JSONException;
    iget-object v5, p0, Lcom/microsoft/cll/android/EventSender;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v6, "EventSender"

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 184
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_2
    move-exception v0

    .line 185
    .local v0, "e":Ljava/lang/RuntimeException;
    iget-object v5, p0, Lcom/microsoft/cll/android/EventSender;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v6, "EventSender"

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected openConnection(IZLcom/microsoft/cll/android/TicketHeaders;)Ljava/net/HttpURLConnection;
    .locals 9
    .param p1, "length"    # I
    .param p2, "compressed"    # Z
    .param p3, "ticketHeaders"    # Lcom/microsoft/cll/android/TicketHeaders;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 106
    iget-object v7, p0, Lcom/microsoft/cll/android/EventSender;->endpoint:Ljava/net/URL;

    invoke-virtual {v7}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    .line 107
    .local v0, "connection":Ljava/net/URLConnection;
    instance-of v7, v0, Ljava/net/HttpURLConnection;

    if-eqz v7, :cond_4

    move-object v4, v0

    .line 109
    check-cast v4, Ljava/net/HttpURLConnection;

    .line 110
    .local v4, "httpsConnection":Ljava/net/HttpURLConnection;
    invoke-virtual {v4, v8}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 111
    const-string v7, "POST"

    invoke-virtual {v4, v7}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 112
    invoke-virtual {v4, v8}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 113
    const-string v7, "Content-Type"

    const-string v8, "application/x-json-stream; charset=utf-8"

    invoke-virtual {v4, v7, v8}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v7, "yyyy-MM-dd\'T\'HH:mm:ss.SSSSSSS\'Z\'"

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v7, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 115
    .local v1, "dateFormat":Ljava/text/SimpleDateFormat;
    const-string v7, "UTC"

    invoke-static {v7}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 116
    const-string v7, "X-UploadTime"

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v7, "Content-Length"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    sget-object v7, Lcom/microsoft/cll/android/SettingsStore$Settings;->HTTPTIMEOUTINTERVAL:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static {v7}, Lcom/microsoft/cll/android/SettingsStore;->getCllSettingsAsInt(Lcom/microsoft/cll/android/SettingsStore$Settings;)I

    move-result v7

    invoke-virtual {v4, v7}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 119
    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 121
    if-eqz p2, :cond_0

    .line 122
    const-string v7, "Accept"

    const-string v8, "application/json"

    invoke-virtual {v4, v7, v8}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v7, "Accept-Encoding"

    const-string v8, "gzip, deflate"

    invoke-virtual {v4, v7, v8}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const-string v7, "Content-Encoding"

    const-string v8, "deflate"

    invoke-virtual {v4, v7, v8}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    :cond_0
    if-eqz p3, :cond_3

    iget-object v7, p3, Lcom/microsoft/cll/android/TicketHeaders;->xtokens:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    .line 128
    const-string v6, ""

    .line 129
    .local v6, "ticketString":Ljava/lang/String;
    const/4 v3, 0x1

    .line 130
    .local v3, "first":Z
    iget-object v7, p3, Lcom/microsoft/cll/android/TicketHeaders;->xtokens:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 133
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v3, :cond_1

    .line 134
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ";"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 137
    :cond_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\"=\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 138
    const/4 v3, 0x0

    .line 139
    goto :goto_0

    .line 141
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_2
    const-string v7, "X-Tickets"

    invoke-virtual {v4, v7, v6}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    const-string v7, "X-AuthXToken"

    iget-object v8, p3, Lcom/microsoft/cll/android/TicketHeaders;->authXToken:Ljava/lang/String;

    invoke-virtual {v4, v7, v8}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget-object v7, p3, Lcom/microsoft/cll/android/TicketHeaders;->msaDeviceTicket:Ljava/lang/String;

    if-eqz v7, :cond_3

    .line 147
    const-string v7, "X-AuthMsaDeviceTicket"

    iget-object v8, p3, Lcom/microsoft/cll/android/TicketHeaders;->msaDeviceTicket:Ljava/lang/String;

    invoke-virtual {v4, v7, v8}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    .end local v3    # "first":Z
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "ticketString":Ljava/lang/String;
    :cond_3
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->connect()V

    .line 152
    return-object v4

    .line 156
    .end local v1    # "dateFormat":Ljava/text/SimpleDateFormat;
    .end local v4    # "httpsConnection":Ljava/net/HttpURLConnection;
    :cond_4
    iget-object v7, p0, Lcom/microsoft/cll/android/EventSender;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    const/4 v8, -0x1

    invoke-virtual {v7, v8}, Lcom/microsoft/cll/android/ClientTelemetry;->IncrementVortexHttpFailures(I)V

    .line 157
    new-instance v7, Ljava/io/IOException;

    const-string v8, "URL didn\'t return HttpsUrlConnection instance."

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7
.end method

.method public sendEvent(Ljava/lang/String;Lcom/microsoft/cll/android/TicketHeaders;)I
    .locals 2
    .param p1, "body"    # Ljava/lang/String;
    .param p2, "ticketHeaders"    # Lcom/microsoft/cll/android/TicketHeaders;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    const-string v1, "UTF-8"

    invoke-static {v1}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 45
    .local v0, "bodyBytes":[B
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p2}, Lcom/microsoft/cll/android/EventSender;->sendEvent([BZLcom/microsoft/cll/android/TicketHeaders;)I

    move-result v1

    return v1
.end method

.method public sendEvent([BZLcom/microsoft/cll/android/TicketHeaders;)I
    .locals 12
    .param p1, "body"    # [B
    .param p2, "compressed"    # Z
    .param p3, "ticketHeaders"    # Lcom/microsoft/cll/android/TicketHeaders;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    iget-object v9, p0, Lcom/microsoft/cll/android/EventSender;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    invoke-virtual {v9}, Lcom/microsoft/cll/android/ClientTelemetry;->IncrementVortexHttpAttempts()V

    .line 62
    array-length v9, p1

    invoke-virtual {p0, v9, p2, p3}, Lcom/microsoft/cll/android/EventSender;->openConnection(IZLcom/microsoft/cll/android/TicketHeaders;)Ljava/net/HttpURLConnection;

    move-result-object v0

    .line 65
    .local v0, "connection":Ljava/net/HttpURLConnection;
    :try_start_0
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v8

    .line 66
    .local v8, "stream":Ljava/io/OutputStream;
    invoke-virtual {v8, p1}, Ljava/io/OutputStream;->write([B)V

    .line 67
    invoke-virtual {v8}, Ljava/io/OutputStream;->flush()V

    .line 68
    invoke-virtual {v8}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    .end local v8    # "stream":Ljava/io/OutputStream;
    :goto_0
    invoke-direct {p0}, Lcom/microsoft/cll/android/EventSender;->getTime()J

    move-result-wide v6

    .line 74
    .local v6, "start":J
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v5

    .line 75
    .local v5, "responseCode":I
    invoke-direct {p0}, Lcom/microsoft/cll/android/EventSender;->getTime()J

    move-result-wide v10

    sub-long v2, v10, v6

    .line 77
    .local v2, "diff":J
    const/16 v9, 0xc8

    if-ne v5, v9, :cond_1

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 78
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v9, Ljava/io/InputStreamReader;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v4, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 79
    .local v4, "reader":Ljava/io/BufferedReader;
    invoke-virtual {p0, v4}, Lcom/microsoft/cll/android/EventSender;->getResponseBody(Ljava/io/BufferedReader;)Ljava/lang/String;

    .line 80
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    .line 90
    .end local v4    # "reader":Ljava/io/BufferedReader;
    :cond_0
    :goto_1
    iget-object v9, p0, Lcom/microsoft/cll/android/EventSender;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    long-to-int v10, v2

    invoke-virtual {v9, v10}, Lcom/microsoft/cll/android/ClientTelemetry;->SetAvgVortexLatencyMs(I)V

    .line 91
    iget-object v9, p0, Lcom/microsoft/cll/android/EventSender;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    long-to-int v10, v2

    invoke-virtual {v9, v10}, Lcom/microsoft/cll/android/ClientTelemetry;->SetMaxVortexLatencyMs(I)V

    .line 93
    return v5

    .line 69
    .end local v2    # "diff":J
    .end local v5    # "responseCode":I
    .end local v6    # "start":J
    :catch_0
    move-exception v1

    .line 70
    .local v1, "e":Ljava/lang/Exception;
    iget-object v9, p0, Lcom/microsoft/cll/android/EventSender;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v10, "EventSender"

    const-string v11, "Error writing data"

    invoke-interface {v9, v10, v11}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 81
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v2    # "diff":J
    .restart local v5    # "responseCode":I
    .restart local v6    # "start":J
    :cond_1
    const/16 v9, 0x1f4

    if-lt v5, v9, :cond_2

    const/16 v9, 0x258

    if-ge v5, v9, :cond_2

    .line 82
    iget-object v9, p0, Lcom/microsoft/cll/android/EventSender;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v10, "EventSender"

    const-string v11, "Bad Response Code"

    invoke-interface {v9, v10, v11}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    iget-object v9, p0, Lcom/microsoft/cll/android/EventSender;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v10

    invoke-virtual {v9, v10}, Lcom/microsoft/cll/android/ClientTelemetry;->IncrementVortexHttpFailures(I)V

    goto :goto_1

    .line 84
    :cond_2
    const/16 v9, 0x190

    if-ne v5, v9, :cond_0

    .line 85
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v9, Ljava/io/InputStreamReader;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v4, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 86
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    invoke-virtual {p0, v4}, Lcom/microsoft/cll/android/EventSender;->getResponseBody(Ljava/io/BufferedReader;)Ljava/lang/String;

    .line 87
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    goto :goto_1
.end method
