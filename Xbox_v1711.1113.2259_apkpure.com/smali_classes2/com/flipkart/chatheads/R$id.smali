.class public final Lcom/flipkart/chatheads/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flipkart/chatheads/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action0:I = 0x7f0e07e2

.field public static final action_bar:I = 0x7f0e00fd

.field public static final action_bar_activity_content:I = 0x7f0e0000

.field public static final action_bar_container:I = 0x7f0e00fc

.field public static final action_bar_root:I = 0x7f0e00f8

.field public static final action_bar_spinner:I = 0x7f0e0001

.field public static final action_bar_subtitle:I = 0x7f0e00db

.field public static final action_bar_title:I = 0x7f0e00da

.field public static final action_context_bar:I = 0x7f0e00fe

.field public static final action_divider:I = 0x7f0e07e6

.field public static final action_menu_divider:I = 0x7f0e0002

.field public static final action_menu_presenter:I = 0x7f0e0003

.field public static final action_mode_bar:I = 0x7f0e00fa

.field public static final action_mode_bar_stub:I = 0x7f0e00f9

.field public static final action_mode_close_button:I = 0x7f0e00dc

.field public static final action_settings:I = 0x7f0e0c22

.field public static final activity_chooser_view_content:I = 0x7f0e00dd

.field public static final alertTitle:I = 0x7f0e00f1

.field public static final always:I = 0x7f0e00b9

.field public static final beginning:I = 0x7f0e00b5

.field public static final buttonPanel:I = 0x7f0e00e4

.field public static final cancel_action:I = 0x7f0e07e3

.field public static final checkbox:I = 0x7f0e00f4

.field public static final chronometer:I = 0x7f0e07eb

.field public static final collapseActionView:I = 0x7f0e00ba

.field public static final contentPanel:I = 0x7f0e00e7

.field public static final custom:I = 0x7f0e00ee

.field public static final customPanel:I = 0x7f0e00ed

.field public static final decor_content_parent:I = 0x7f0e00fb

.field public static final default_activity_button:I = 0x7f0e00e0

.field public static final disableHome:I = 0x7f0e0081

.field public static final edit_query:I = 0x7f0e00ff

.field public static final end:I = 0x7f0e009b

.field public static final end_padder:I = 0x7f0e07f5

.field public static final expand_activities_button:I = 0x7f0e00de

.field public static final expanded_menu:I = 0x7f0e00f3

.field public static final home:I = 0x7f0e0019

.field public static final homeAsUp:I = 0x7f0e0082

.field public static final icon:I = 0x7f0e00e2

.field public static final ifRoom:I = 0x7f0e00bb

.field public static final image:I = 0x7f0e00df

.field public static final info:I = 0x7f0e07ec

.field public static final line1:I = 0x7f0e07f1

.field public static final line3:I = 0x7f0e07f3

.field public static final listMode:I = 0x7f0e007e

.field public static final list_item:I = 0x7f0e00e1

.field public static final media_actions:I = 0x7f0e07e5

.field public static final middle:I = 0x7f0e00b6

.field public static final multiply:I = 0x7f0e008d

.field public static final never:I = 0x7f0e00bc

.field public static final none:I = 0x7f0e0077

.field public static final normal:I = 0x7f0e007f

.field public static final parentPanel:I = 0x7f0e00e6

.field public static final progress_circular:I = 0x7f0e005c

.field public static final progress_horizontal:I = 0x7f0e005d

.field public static final radio:I = 0x7f0e00f6

.field public static final screen:I = 0x7f0e008e

.field public static final scrollView:I = 0x7f0e00e9

.field public static final search_badge:I = 0x7f0e0101

.field public static final search_bar:I = 0x7f0e0100

.field public static final search_button:I = 0x7f0e0102

.field public static final search_close_btn:I = 0x7f0e0107

.field public static final search_edit_frame:I = 0x7f0e0103

.field public static final search_go_btn:I = 0x7f0e0109

.field public static final search_mag_icon:I = 0x7f0e0104

.field public static final search_plate:I = 0x7f0e0105

.field public static final search_src_text:I = 0x7f0e0106

.field public static final search_voice_btn:I = 0x7f0e010a

.field public static final select_dialog_listview:I = 0x7f0e010b

.field public static final shortcut:I = 0x7f0e00f5

.field public static final showCustom:I = 0x7f0e0083

.field public static final showHome:I = 0x7f0e0084

.field public static final showTitle:I = 0x7f0e0085

.field public static final split_action_bar:I = 0x7f0e0061

.field public static final src_atop:I = 0x7f0e008f

.field public static final src_in:I = 0x7f0e0090

.field public static final src_over:I = 0x7f0e0091

.field public static final status_bar_latest_event_content:I = 0x7f0e07e4

.field public static final submit_area:I = 0x7f0e0108

.field public static final tabMode:I = 0x7f0e0080

.field public static final text:I = 0x7f0e07f4

.field public static final text2:I = 0x7f0e07f2

.field public static final textSpacerNoButtons:I = 0x7f0e00eb

.field public static final time:I = 0x7f0e07ea

.field public static final title:I = 0x7f0e00e3

.field public static final title_template:I = 0x7f0e00f0

.field public static final topPanel:I = 0x7f0e00ef

.field public static final up:I = 0x7f0e0068

.field public static final useLogo:I = 0x7f0e0086

.field public static final withText:I = 0x7f0e00bd

.field public static final wrap_content:I = 0x7f0e0092


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 430
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
