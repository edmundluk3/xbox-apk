.class public final Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystem;",
        ">;"
    }
.end annotation


# instance fields
.field private final activeAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final connectionAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultActive:Ljava/lang/Boolean;

.field private defaultConnection:Ljava/lang/String;

.field private defaultDescription:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;

.field private defaultSubscription:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystemSubscription;

.field private final descriptionAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;",
            ">;"
        }
    .end annotation
.end field

.field private final subscriptionAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystemSubscription;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->defaultActive:Ljava/lang/Boolean;

    .line 27
    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->defaultConnection:Ljava/lang/String;

    .line 28
    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->defaultSubscription:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystemSubscription;

    .line 29
    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->defaultDescription:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;

    .line 31
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->activeAdapter:Lcom/google/gson/TypeAdapter;

    .line 32
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->connectionAdapter:Lcom/google/gson/TypeAdapter;

    .line 33
    const-class v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystemSubscription;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->subscriptionAdapter:Lcom/google/gson/TypeAdapter;

    .line 34
    const-class v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->descriptionAdapter:Lcom/google/gson/TypeAdapter;

    .line 35
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystem;
    .locals 7
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v5

    sget-object v6, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v5, v6, :cond_0

    .line 74
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 75
    const/4 v5, 0x0

    .line 111
    :goto_0
    return-object v5

    .line 77
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 78
    iget-object v1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->defaultActive:Ljava/lang/Boolean;

    .line 79
    .local v1, "active":Ljava/lang/Boolean;
    iget-object v2, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->defaultConnection:Ljava/lang/String;

    .line 80
    .local v2, "connection":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->defaultSubscription:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystemSubscription;

    .line 81
    .local v4, "subscription":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystemSubscription;
    iget-object v3, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->defaultDescription:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;

    .line 82
    .local v3, "description":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 83
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 84
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v5

    sget-object v6, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v5, v6, :cond_1

    .line 85
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 88
    :cond_1
    const/4 v5, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v5, :pswitch_data_0

    .line 106
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 88
    :sswitch_0
    const-string v6, "active"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x0

    goto :goto_2

    :sswitch_1
    const-string v6, "connection"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x1

    goto :goto_2

    :sswitch_2
    const-string v6, "subscription"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x2

    goto :goto_2

    :sswitch_3
    const-string v6, "description"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x3

    goto :goto_2

    .line 90
    :pswitch_0
    iget-object v5, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->activeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "active":Ljava/lang/Boolean;
    check-cast v1, Ljava/lang/Boolean;

    .line 91
    .restart local v1    # "active":Ljava/lang/Boolean;
    goto :goto_1

    .line 94
    :pswitch_1
    iget-object v5, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->connectionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "connection":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 95
    .restart local v2    # "connection":Ljava/lang/String;
    goto :goto_1

    .line 98
    :pswitch_2
    iget-object v5, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->subscriptionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "subscription":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystemSubscription;
    check-cast v4, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystemSubscription;

    .line 99
    .restart local v4    # "subscription":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystemSubscription;
    goto :goto_1

    .line 102
    :pswitch_3
    iget-object v5, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->descriptionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "description":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;
    check-cast v3, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;

    .line 103
    .restart local v3    # "description":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;
    goto :goto_1

    .line 110
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 111
    new-instance v5, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem;

    invoke-direct {v5, v1, v2, v4, v3}, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystemSubscription;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;)V

    goto/16 :goto_0

    .line 88
    nop

    :sswitch_data_0
    .sparse-switch
        -0x66ca7c04 -> :sswitch_3
        -0x54d080fa -> :sswitch_0
        -0x2e3b8122 -> :sswitch_1
        0x1456591d -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystem;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultActive(Ljava/lang/Boolean;)Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultActive"    # Ljava/lang/Boolean;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->defaultActive:Ljava/lang/Boolean;

    .line 38
    return-object p0
.end method

.method public setDefaultConnection(Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultConnection"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->defaultConnection:Ljava/lang/String;

    .line 42
    return-object p0
.end method

.method public setDefaultDescription(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;)Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultDescription"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->defaultDescription:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;

    .line 50
    return-object p0
.end method

.method public setDefaultSubscription(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystemSubscription;)Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultSubscription"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystemSubscription;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->defaultSubscription:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystemSubscription;

    .line 46
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystem;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    if-nez p2, :cond_0

    .line 56
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 69
    :goto_0
    return-void

    .line 59
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 60
    const-string v0, "active"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->activeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystem;->active()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 62
    const-string v0, "connection"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->connectionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystem;->connection()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 64
    const-string v0, "subscription"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->subscriptionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystem;->subscription()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystemSubscription;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 66
    const-string v0, "description"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->descriptionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystem;->description()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 68
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    check-cast p2, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystem;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/multiplayer/AutoValue_MultiplayerSessionDataTypes_MultiplayerMemberPropertiesSystem$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystem;)V

    return-void
.end method
