.class public final Lcom/microsoft/xbox/service/mediaHub/MediaHubServiceFactory;
.super Lcom/microsoft/xbox/service/retrofit/BaseRetrofitFactory;
.source "MediaHubServiceFactory.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/retrofit/BaseRetrofitFactory",
        "<",
        "Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;",
        ">;"
    }
.end annotation


# static fields
.field private static final BASE_URL:Ljava/lang/String; = "https://mediahub.xboxlive.com"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/microsoft/xbox/service/retrofit/BaseRetrofitFactory;-><init>()V

    return-void
.end method

.method private getClient()Lokhttp3/OkHttpClient;
    .locals 2

    .prologue
    .line 44
    invoke-static {}, Lcom/microsoft/xbox/service/retrofit/OkHttpFactory;->getXTokenOkHttpBuilder()Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 45
    invoke-static {}, Lcom/microsoft/xbox/service/retrofit/OkHttpFactory;->getDefaultLoggingInterceptor()Lokhttp3/Interceptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;-><init>()V

    .line 46
    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/service/retrofit/ContentRestrictionsHeaderInterceptor;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/retrofit/ContentRestrictionsHeaderInterceptor;-><init>()V

    .line 47
    invoke-virtual {v0, v1}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    .line 44
    return-object v0
.end method


# virtual methods
.method public getRetrofit()Lretrofit2/Retrofit;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 26
    const-string v0, "https://mediahub.xboxlive.com"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubServiceFactory;->getDefaultRetrofitBuilder(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 27
    invoke-static {}, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;->create()Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit$Builder;->addCallAdapterFactory(Lretrofit2/CallAdapter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 28
    invoke-direct {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubServiceFactory;->getClient()Lokhttp3/OkHttpClient;

    move-result-object v1

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit$Builder;->client(Lokhttp3/OkHttpClient;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 29
    invoke-virtual {v0}, Lretrofit2/Retrofit$Builder;->build()Lretrofit2/Retrofit;

    move-result-object v0

    .line 26
    return-object v0
.end method

.method public getStubInstance(Lretrofit2/mock/BehaviorDelegate;)Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/mock/BehaviorDelegate",
            "<",
            "Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;",
            ">;)",
            "Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;"
        }
    .end annotation

    .prologue
    .line 35
    .local p1, "delegate":Lretrofit2/mock/BehaviorDelegate;, "Lretrofit2/mock/BehaviorDelegate<Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;>;"
    invoke-static {}, Lretrofit2/mock/NetworkBehavior;->create()Lretrofit2/mock/NetworkBehavior;

    move-result-object v0

    .line 36
    .local v0, "behavior":Lretrofit2/mock/NetworkBehavior;
    new-instance v2, Lretrofit2/mock/MockRetrofit$Builder;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubServiceFactory;->getRetrofit()Lretrofit2/Retrofit;

    move-result-object v3

    invoke-direct {v2, v3}, Lretrofit2/mock/MockRetrofit$Builder;-><init>(Lretrofit2/Retrofit;)V

    .line 37
    invoke-virtual {v2, v0}, Lretrofit2/mock/MockRetrofit$Builder;->networkBehavior(Lretrofit2/mock/NetworkBehavior;)Lretrofit2/mock/MockRetrofit$Builder;

    move-result-object v2

    .line 38
    invoke-virtual {v2}, Lretrofit2/mock/MockRetrofit$Builder;->build()Lretrofit2/mock/MockRetrofit;

    move-result-object v1

    .line 40
    .local v1, "mockRetrofit":Lretrofit2/mock/MockRetrofit;
    new-instance v2, Lcom/microsoft/xbox/service/mediaHub/MediaHubServiceStub;

    invoke-direct {v2, v1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubServiceStub;-><init>(Lretrofit2/mock/MockRetrofit;)V

    return-object v2
.end method

.method public bridge synthetic getStubInstance(Lretrofit2/mock/BehaviorDelegate;)Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/mediaHub/MediaHubServiceFactory;->getStubInstance(Lretrofit2/mock/BehaviorDelegate;)Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    move-result-object v0

    return-object v0
.end method
