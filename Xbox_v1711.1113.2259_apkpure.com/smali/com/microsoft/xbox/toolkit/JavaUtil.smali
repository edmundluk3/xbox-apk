.class public Lcom/microsoft/xbox/toolkit/JavaUtil;
.super Ljava/lang/Object;
.source "JavaUtil.java"


# static fields
.field public static final DAY_IN_MILLISECONDS:J = 0x5265c00L

.field public static final EMPTY_GUID:Ljava/lang/String; = "00000000-0000-0000-0000-000000000000"

.field public static final HEX_PREFIX:Ljava/lang/String; = "0x"

.field public static final HEX_PREFIX_UPPER:Ljava/lang/String; = "0X"

.field public static final HOUR_IN_MILLISECONDS:J = 0x36ee80L

.field public static final HTTPS_PREFIX:Ljava/lang/String; = "https://"

.field public static final HTTP_PREFIX:Ljava/lang/String; = "http://"

.field public static final LINE_SEPARATOR:Ljava/lang/String;

.field public static final MAX_MONTH_DAYS_IN_MILLISECONDS:J = 0x9fa52400L

.field public static final MIN_DATE:Ljava/util/Date;

.field public static final MIN_IN_MILLISECONDS:J = 0xea60L

.field public static final MONTH_IN_MILLISECONDS:J = 0x9a7ec800L

.field public static final NON_BREAKING_SPACE:C = '\u00a0'

.field public static final NO_OP:Ljava/lang/Runnable;

.field private static final TAG:Ljava/lang/String;

.field public static final WEEK_IN_MILLISECONDS:J = 0x240c8400L


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 34
    const-class v0, Lcom/microsoft/xbox/toolkit/JavaUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/JavaUtil;->TAG:Ljava/lang/String;

    .line 42
    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/JavaUtil;->LINE_SEPARATOR:Ljava/lang/String;

    .line 45
    new-instance v0, Ljava/util/Date;

    const/16 v1, 0x64

    invoke-direct {v0, v1, v2, v2}, Ljava/util/Date;-><init>(III)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/JavaUtil;->MIN_DATE:Ljava/util/Date;

    .line 55
    invoke-static {}, Lcom/microsoft/xbox/toolkit/JavaUtil$$Lambda$1;->lambdaFactory$()Ljava/lang/Runnable;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static varargs concatenateArrays([[I)[I
    .locals 8
    .param p0, "arrays"    # [[I

    .prologue
    const/4 v5, 0x0

    .line 225
    if-nez p0, :cond_1

    .line 226
    new-array v1, v5, [I

    .line 245
    :cond_0
    return-object v1

    .line 229
    :cond_1
    const/4 v3, 0x0

    .line 230
    .local v3, "finalSize":I
    array-length v6, p0

    move v4, v5

    :goto_0
    if-ge v4, v6, :cond_3

    aget-object v0, p0, v4

    .line 231
    .local v0, "a":[I
    if-eqz v0, :cond_2

    .line 232
    array-length v7, v0

    add-int/2addr v3, v7

    .line 230
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 236
    .end local v0    # "a":[I
    :cond_3
    new-array v1, v3, [I

    .line 237
    .local v1, "destArray":[I
    const/4 v2, 0x0

    .line 238
    .local v2, "destPos":I
    array-length v6, p0

    move v4, v5

    :goto_1
    if-ge v4, v6, :cond_0

    aget-object v0, p0, v4

    .line 239
    .restart local v0    # "a":[I
    if-eqz v0, :cond_4

    .line 240
    array-length v7, v0

    invoke-static {v0, v5, v1, v2, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 241
    array-length v7, v0

    add-int/2addr v2, v7

    .line 238
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method public static concatenateStringsWithDelimiter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 2
    .param p0, "str1"    # Ljava/lang/String;
    .param p1, "str2"    # Ljava/lang/String;
    .param p2, "str3"    # Ljava/lang/String;
    .param p3, "delimiter"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4, "addSpaceBeforeDelimiter"    # Z

    .prologue
    .line 199
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    invoke-static {p3, p4, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->concatenateStringsWithDelimiter(Ljava/lang/String;Z[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static varargs concatenateStringsWithDelimiter(Ljava/lang/String;Z[Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "delimiter"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "addSpaceBeforeDelimiter"    # Z
    .param p2, "strs"    # [Ljava/lang/String;

    .prologue
    .line 203
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 205
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p1, :cond_0

    const-string v2, " "

    :goto_0
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 206
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 207
    .local v0, "sb":Ljava/lang/StringBuilder;
    array-length v2, p2

    if-nez v2, :cond_1

    .line 208
    const-string v2, ""

    .line 221
    :goto_1
    return-object v2

    .line 205
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    const-string v2, ""

    goto :goto_0

    .line 211
    .restart local v0    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    array-length v3, p2

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v3, :cond_4

    aget-object v1, p2, v2

    .line 212
    .local v1, "str":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 213
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_2

    .line 214
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    :cond_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 221
    .end local v1    # "str":Ljava/lang/String;
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public static containsFlag(II)Z
    .locals 1
    .param p0, "value"    # I
    .param p1, "flagToCheck"    # I

    .prologue
    .line 157
    and-int v0, p0, p1

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static convertToUTC(Ljava/util/Date;)Ljava/util/Date;
    .locals 6
    .param p0, "local"    # Ljava/util/Date;

    .prologue
    .line 274
    const/4 v2, 0x0

    .line 275
    .local v2, "utc":Ljava/util/Date;
    if-eqz p0, :cond_0

    .line 276
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    .line 277
    .local v1, "tz":Ljava/util/TimeZone;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 278
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 279
    const/16 v3, 0xe

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v4

    neg-int v4, v4

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->add(II)V

    .line 280
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    .line 282
    .end local v0    # "cal":Ljava/util/Calendar;
    .end local v1    # "tz":Ljava/util/TimeZone;
    :cond_0
    return-object v2
.end method

.method public static dateToUrlFormat(Ljava/util/Date;)Ljava/lang/String;
    .locals 3
    .param p0, "date"    # Ljava/util/Date;

    .prologue
    .line 355
    if-nez p0, :cond_0

    .line 356
    const/4 v1, 0x0

    .line 360
    :goto_0
    return-object v1

    .line 358
    :cond_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd\'T\'HH:mm:ss"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 359
    .local v0, "formatter":Ljava/text/SimpleDateFormat;
    const-string v1, "GMT"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 360
    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;TT;)TT;"
        }
    .end annotation

    .prologue
    .line 152
    .local p0, "val":Ljava/lang/Object;, "TT;"
    .local p1, "defaultVal":Ljava/lang/Object;, "TT;"
    if-nez p0, :cond_0

    .end local p1    # "defaultVal":Ljava/lang/Object;, "TT;"
    :goto_0
    return-object p1

    .restart local p1    # "defaultVal":Ljava/lang/Object;, "TT;"
    :cond_0
    move-object p1, p0

    goto :goto_0
.end method

.method public static ensureHttpsPrefix(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "uriString"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 476
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 478
    move-object v0, p0

    .line 479
    .local v0, "uriStringWithHttps":Ljava/lang/String;
    const-string v1, "https://"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "http://"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 480
    const-string v1, "//"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 481
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "//"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 487
    :cond_0
    :goto_0
    return-object v0

    .line 483
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static ensureUrlEncoding(Ljava/lang/String;Ljava/nio/charset/Charset;)Ljava/lang/String;
    .locals 6
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "encoding"    # Ljava/nio/charset/Charset;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 250
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 252
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 253
    const-string v2, ""

    .line 264
    :goto_0
    return-object v2

    .line 256
    :cond_0
    move-object v2, p0

    .line 258
    .local v2, "encoded":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 259
    .local v0, "decoded":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/nio/charset/Charset;->name()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 260
    .end local v0    # "decoded":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 261
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    const-string v3, "JavaUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to decode/encode URL: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getBigDecimalFromFloat(FI)Ljava/math/BigDecimal;
    .locals 4
    .param p0, "num"    # F
    .param p1, "scale"    # I
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 409
    new-instance v0, Ljava/math/BigDecimal;

    float-to-double v2, p0

    invoke-direct {v0, v2, v3}, Ljava/math/BigDecimal;-><init>(D)V

    const/4 v1, 0x4

    invoke-virtual {v0, p1, v1}, Ljava/math/BigDecimal;->setScale(II)Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public static getPriceText(Ljava/math/BigDecimal;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "price"    # Ljava/math/BigDecimal;
    .param p1, "currencyCode"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 399
    if-eqz p0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 400
    :cond_0
    const-string v1, ""

    .line 404
    :goto_0
    return-object v1

    .line 402
    :cond_1
    invoke-static {}, Ljava/text/NumberFormat;->getCurrencyInstance()Ljava/text/NumberFormat;

    move-result-object v0

    .line 403
    .local v0, "currencyFormat":Ljava/text/NumberFormat;
    invoke-static {p1}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setCurrency(Ljava/util/Currency;)V

    .line 404
    invoke-virtual {v0, p0}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getShortClassName(Ljava/lang/Class;)Ljava/lang/String;
    .locals 3
    .param p0, "cls"    # Ljava/lang/Class;

    .prologue
    .line 59
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 60
    .local v0, "name":Ljava/lang/String;
    const-string v2, "\\."

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 61
    .local v1, "tokens":[Ljava/lang/String;
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v1, v2

    return-object v2
.end method

.method public static isDateWithinLastMonth(Ljava/util/Date;)Z
    .locals 8
    .param p0, "date"    # Ljava/util/Date;

    .prologue
    const/4 v2, 0x0

    .line 305
    if-nez p0, :cond_1

    .line 317
    :cond_0
    :goto_0
    return v2

    .line 309
    :cond_1
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    sub-long v0, v4, v6

    .line 311
    .local v0, "timeDiff":J
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-ltz v3, :cond_0

    .line 313
    const-wide v4, 0x9fa52400L

    cmp-long v3, v0, v4

    if-gez v3, :cond_0

    .line 314
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static isDateWithinLastWeek(Ljava/util/Date;)Z
    .locals 8
    .param p0, "date"    # Ljava/util/Date;

    .prologue
    const/4 v2, 0x0

    .line 321
    if-nez p0, :cond_1

    .line 333
    :cond_0
    :goto_0
    return v2

    .line 325
    :cond_1
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    sub-long v0, v4, v6

    .line 327
    .local v0, "timeDiff":J
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-ltz v3, :cond_0

    .line 329
    const-wide/32 v4, 0x240c8400

    cmp-long v3, v0, v4

    if-gez v3, :cond_0

    .line 330
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static isDateWithinYesterday(Ljava/util/Date;)Z
    .locals 10
    .param p0, "date"    # Ljava/util/Date;

    .prologue
    const/4 v7, 0x0

    const/4 v9, 0x6

    const/4 v6, 0x1

    .line 337
    if-nez p0, :cond_0

    .line 351
    :goto_0
    return v7

    .line 340
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 341
    .local v3, "yesterday":Ljava/util/Calendar;
    const/4 v8, -0x1

    invoke-virtual {v3, v9, v8}, Ljava/util/Calendar;->add(II)V

    .line 343
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 344
    .local v0, "item":Ljava/util/Calendar;
    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 346
    invoke-virtual {v3, v6}, Ljava/util/Calendar;->get(I)I

    move-result v5

    .line 347
    .local v5, "yesterdayYear":I
    invoke-virtual {v3, v9}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 348
    .local v4, "yesterdayDayOfYear":I
    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 349
    .local v2, "itemYear":I
    invoke-virtual {v0, v9}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 351
    .local v1, "itemDayOfYear":I
    if-ne v5, v2, :cond_1

    if-ne v4, v1, :cond_1

    :goto_1
    move v7, v6

    goto :goto_0

    :cond_1
    move v6, v7

    goto :goto_1
.end method

.method public static isNullOrEmpty(Ljava/lang/Iterable;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Iterable",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 370
    .local p0, "collection":Ljava/lang/Iterable;, "Ljava/lang/Iterable<TT;>;"
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isNullOrEmpty([Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)Z"
        }
    .end annotation

    .prologue
    .line 380
    .local p0, "array":[Ljava/lang/Object;, "[TT;"
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$static$0()V
    .locals 0

    .prologue
    .line 56
    return-void
.end method

.method public static objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p0, "a"    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "b"    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 65
    if-nez p0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    if-eq p0, p1, :cond_1

    if-eqz p0, :cond_2

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseHexLong(Ljava/lang/String;)J
    .locals 6
    .param p0, "hexLong"    # Ljava/lang/String;

    .prologue
    .line 128
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 129
    const-wide/16 v2, 0x0

    .line 143
    :goto_0
    return-wide v2

    .line 132
    :cond_0
    const-string v1, "0x"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "0X"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 133
    :cond_1
    const-string v1, "0x"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 136
    :cond_2
    const-wide/16 v2, 0x0

    .line 138
    .local v2, "value":J
    const/16 v1, 0x10

    :try_start_0
    invoke-static {p0, v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    goto :goto_0

    .line 139
    :catch_0
    move-exception v0

    .line 140
    .local v0, "ex":Ljava/lang/Exception;
    sget-object v1, Lcom/microsoft/xbox/toolkit/JavaUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parseHexLong: failed to parse "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static replaceSpacesWithNonBreakSpaces(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "str"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 287
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 288
    const/16 v0, 0x20

    const/16 v1, 0xa0

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static safeCopy(Ljava/util/Collection;)Ljava/util/Collection;
    .locals 1
    .param p0    # Ljava/util/Collection;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<TT;>;)",
            "Ljava/util/Collection",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 421
    .local p0, "collection":Ljava/util/Collection;, "Ljava/util/Collection<TT;>;"
    if-nez p0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public static safeCopy(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 433
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    if-nez p0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public static safeCopy(Ljava/util/Map;)Ljava/util/Map;
    .locals 1
    .param p0    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Map",
            "<TK;TV;>;)",
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 447
    .local p0, "map":Ljava/util/Map;, "Ljava/util/Map<TK;TV;>;"
    if-nez p0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    goto :goto_0
.end method

.method public static safeCopy(Ljava/util/Set;)Ljava/util/Set;
    .locals 1
    .param p0    # Ljava/util/Set;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Set",
            "<TV;>;)",
            "Ljava/util/Set",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 471
    .local p0, "set":Ljava/util/Set;, "Ljava/util/Set<TV;>;"
    if-nez p0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public static safeCopyWritable(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 459
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    if-nez p0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public static safeExtract(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p0, "inputString"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "delimiterRegex"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "index"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 170
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeExtract(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static safeExtract(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "inputString"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "delimiterRegex"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "index"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p3, "defaultReturn"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 184
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 185
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 186
    const-wide/16 v2, 0x0

    int-to-long v4, p2

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 190
    add-int/lit8 v1, p2, 0x2

    invoke-virtual {p0, p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    .line 191
    .local v0, "parts":[Ljava/lang/String;
    array-length v1, v0

    if-le v1, p2, :cond_0

    .line 192
    aget-object p3, v0, p2

    .line 195
    .end local p3    # "defaultReturn":Ljava/lang/String;
    :cond_0
    return-object p3
.end method

.method public static selectUniqueRandomN(Ljava/util/List;I)Ljava/util/List;
    .locals 6
    .param p1, "n"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;I)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 500
    .local p0, "source":Ljava/util/List;, "Ljava/util/List<TT;>;"
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    .line 501
    .local v1, "random":Ljava/util/Random;
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 502
    .local v2, "randomSelected":Ljava/util/Set;, "Ljava/util/Set<TT;>;"
    new-instance v4, Ljava/util/ArrayList;

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5, p0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 503
    .local v4, "sourceWithoutDups":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 505
    .local v3, "sourceSize":I
    if-le v3, p1, :cond_0

    .line 506
    :goto_0
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v5

    if-ge v5, p1, :cond_1

    .line 507
    invoke-virtual {v1, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    invoke-interface {v4, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    .line 508
    .local v0, "item":Ljava/lang/Object;, "TT;"
    add-int/lit8 v3, v3, -0x1

    .line 510
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 513
    .end local v0    # "item":Ljava/lang/Object;, "TT;"
    :cond_0
    invoke-interface {v2, p0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 516
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v5
.end method

.method public static shouldRefresh(Ljava/util/Date;J)Z
    .locals 9
    .param p0, "lastRefreshTime"    # Ljava/util/Date;
    .param p1, "lifetime"    # J

    .prologue
    const/4 v1, 0x1

    .line 384
    if-nez p0, :cond_1

    .line 392
    :cond_0
    :goto_0
    return v1

    .line 387
    :cond_1
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 388
    .local v0, "currentTime":Ljava/util/Date;
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    sub-long v2, v4, v6

    .line 389
    .local v2, "diff":J
    cmp-long v4, v2, p1

    if-gtz v4, :cond_0

    .line 392
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static stringCompareUserLocale(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p0, "s1"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "s2"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 292
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 293
    const/4 v1, 0x0

    .line 300
    :goto_0
    return v1

    .line 294
    :cond_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 295
    const/4 v1, 0x1

    goto :goto_0

    .line 296
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 297
    const/4 v1, -0x1

    goto :goto_0

    .line 299
    :cond_2
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    .line 300
    .local v0, "collator":Ljava/text/Collator;
    invoke-virtual {v0, p0, p1}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method

.method public static stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "lhs"    # Ljava/lang/String;
    .param p1, "rhs"    # Ljava/lang/String;

    .prologue
    .line 70
    if-eq p0, p1, :cond_0

    invoke-static {p0, p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualNonNull(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "lhs"    # Ljava/lang/String;
    .param p1, "rhs"    # Ljava/lang/String;

    .prologue
    .line 79
    if-eq p0, p1, :cond_0

    invoke-static {p0, p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualNonNullCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static stringsEqualNonNull(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "lhs"    # Ljava/lang/String;
    .param p1, "rhs"    # Ljava/lang/String;

    .prologue
    .line 74
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static stringsEqualNonNullCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "lhs"    # Ljava/lang/String;
    .param p1, "rhs"    # Ljava/lang/String;

    .prologue
    .line 83
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static tryParseBoolean(Ljava/lang/String;Z)Z
    .locals 1
    .param p0, "booleanString"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Z

    .prologue
    .line 88
    :try_start_0
    invoke-static {p0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .line 90
    .end local p1    # "defaultValue":Z
    :goto_0
    return p1

    .line 89
    .restart local p1    # "defaultValue":Z
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public static tryParseDouble(Ljava/lang/String;D)D
    .locals 1
    .param p0, "doubleString"    # Ljava/lang/String;
    .param p1, "defaultValue"    # D

    .prologue
    .line 112
    :try_start_0
    invoke-static {p0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p1

    .line 114
    .end local p1    # "defaultValue":D
    :goto_0
    return-wide p1

    .line 113
    .restart local p1    # "defaultValue":D
    :catch_0
    move-exception v0

    .line 114
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public static tryParseInteger(Ljava/lang/String;I)I
    .locals 1
    .param p0, "integerString"    # Ljava/lang/String;
    .param p1, "defaultValue"    # I

    .prologue
    .line 96
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .line 98
    .end local p1    # "defaultValue":I
    :goto_0
    return p1

    .line 97
    .restart local p1    # "defaultValue":I
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public static tryParseLong(Ljava/lang/String;J)J
    .locals 1
    .param p0, "longString"    # Ljava/lang/String;
    .param p1, "defaultValue"    # J

    .prologue
    .line 104
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p1

    .line 106
    .end local p1    # "defaultValue":J
    :goto_0
    return-wide p1

    .line 105
    .restart local p1    # "defaultValue":J
    :catch_0
    move-exception v0

    .line 106
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public static tryParseUUID(Ljava/lang/String;Ljava/util/UUID;)Ljava/util/UUID;
    .locals 1
    .param p0, "uuidString"    # Ljava/lang/String;
    .param p1, "defaultValue"    # Ljava/util/UUID;

    .prologue
    .line 120
    :try_start_0
    invoke-static {p0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 122
    .end local p1    # "defaultValue":Ljava/util/UUID;
    :goto_0
    return-object p1

    .line 121
    .restart local p1    # "defaultValue":Ljava/util/UUID;
    :catch_0
    move-exception v0

    .line 122
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    goto :goto_0
.end method
