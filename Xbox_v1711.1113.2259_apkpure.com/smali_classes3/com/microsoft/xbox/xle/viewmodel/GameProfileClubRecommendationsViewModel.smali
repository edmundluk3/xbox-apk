.class public Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "GameProfileClubRecommendationsViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;,
        Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GetRecommendationsAsyncTask;
    }
.end annotation


# static fields
.field private static final MAX_TITLE_RECOMMENDATIONS:I = 0xf

.field private static final TAG:Ljava/lang/String;

.field private static final clubModelManager:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;


# instance fields
.field private final gameTitleId:J

.field private getRecommendationsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GetRecommendationsAsyncTask;

.field private recommendationCategory:Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->TAG:Ljava/lang/String;

    .line 36
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->clubModelManager:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 4
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 41
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 48
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->getParent()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 49
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->getParent()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v2

    iget-object v2, v2, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 52
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 53
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    .line 54
    .local v1, "selectedItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v2

    :goto_0
    iput-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->gameTitleId:J

    .line 55
    return-void

    .line 54
    :cond_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getTitleId()J

    move-result-wide v2

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;

    .prologue
    .line 33
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->gameTitleId:J

    return-wide v0
.end method

.method static synthetic access$200()Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->clubModelManager:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->onGetRecommendedClubsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method private cancelTask()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->getRecommendationsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GetRecommendationsAsyncTask;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->getRecommendationsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GetRecommendationsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GetRecommendationsAsyncTask;->cancel()V

    .line 92
    :cond_0
    return-void
.end method

.method private onGetRecommendedClubsCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 12
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 105
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 106
    sget-object v8, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onGetRecommendedClubsCompleted "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    sget-object v8, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 160
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->updateAdapter()V

    .line 161
    return-void

    .line 112
    :pswitch_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/List;

    .line 113
    .local v7, "recommendedClubList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 114
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->recommendationCategory:Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

    .line 115
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    invoke-direct {v1, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 116
    .local v1, "clubCards":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;>;"
    const/4 v5, 0x0

    .line 117
    .local v5, "reasonText":Ljava/lang/String;
    const/4 v3, 0x0

    .line 118
    .local v3, "criteria":Ljava/lang/String;
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 119
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 121
    sget-object v8, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->getClubModel(J)Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    move-result-object v2

    .line 122
    .local v2, "clubModel":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 123
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getSocialTags()Ljava/util/List;

    move-result-object v8

    invoke-static {v0, v8}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->with(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Ljava/util/List;)Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;

    move-result-object v8

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->recommendation()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    move-result-object v6

    .line 127
    .local v6, "recommendation":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;
    if-eqz v6, :cond_1

    .line 128
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;->reasons()Ljava/util/List;

    move-result-object v8

    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 129
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;->reasons()Ljava/util/List;

    move-result-object v8

    const/4 v10, 0x0

    invoke-interface {v8, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendationReason;

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendationReason;->localizedText()Ljava/lang/String;

    move-result-object v5

    .line 132
    :cond_2
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 133
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;->criteria()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 137
    .end local v6    # "recommendation":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;
    :cond_3
    sget-object v10, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Club model for club "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v11, " was not loaded correctly!"

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v10, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 142
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .end local v2    # "clubModel":Lcom/microsoft/xbox/service/model/clubs/ClubModel;
    :cond_4
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_6

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_6

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 143
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 144
    .local v4, "gameTitles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    iget-wide v8, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->gameTitleId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    new-instance v8, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

    invoke-direct {v8, v5, v3, v1, v4}, Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    iput-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->recommendationCategory:Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

    .line 151
    .end local v1    # "clubCards":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;>;"
    .end local v3    # "criteria":Ljava/lang/String;
    .end local v4    # "gameTitles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    .end local v5    # "reasonText":Ljava/lang/String;
    :cond_5
    :goto_2
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->recommendationCategory:Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

    if-nez v8, :cond_7

    sget-object v8, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_3
    iput-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto/16 :goto_0

    .line 147
    .restart local v1    # "clubCards":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;>;"
    .restart local v3    # "criteria":Ljava/lang/String;
    .restart local v5    # "reasonText":Ljava/lang/String;
    :cond_6
    sget-object v8, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unable to create club recommendation list for title: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->gameTitleId:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 151
    .end local v1    # "clubCards":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;>;"
    .end local v3    # "criteria":Ljava/lang/String;
    .end local v5    # "reasonText":Ljava/lang/String;
    :cond_7
    sget-object v8, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_3

    .line 155
    .end local v7    # "recommendedClubList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    :pswitch_1
    iget-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->recommendationCategory:Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

    if-eqz v8, :cond_0

    .line 156
    sget-object v8, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v8, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto/16 :goto_0

    .line 108
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getRecommendationCategory()Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->recommendationCategory:Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;

    .line 81
    .local v0, "categoryItem":Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;
    if-eqz v0, :cond_0

    .line 82
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;)V

    .line 85
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isBusy()Z
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 96
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 98
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->cancelTask()V

    .line 100
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GetRecommendationsAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GetRecommendationsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->getRecommendationsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GetRecommendationsAsyncTask;

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->getRecommendationsAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GetRecommendationsAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GetRecommendationsAsyncTask;->load(Z)V

    .line 102
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->getParent()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->getParent()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 66
    :cond_0
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel;->cancelTask()V

    .line 71
    return-void
.end method
