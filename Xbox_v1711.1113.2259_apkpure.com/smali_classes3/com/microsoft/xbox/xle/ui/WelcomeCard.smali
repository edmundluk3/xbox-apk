.class public Lcom/microsoft/xbox/xle/ui/WelcomeCard;
.super Landroid/widget/RelativeLayout;
.source "WelcomeCard.java"


# static fields
.field private static final WHITE_COLOR:I


# instance fields
.field button:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0bd8
    .end annotation
.end field

.field private buttonTaps:Lio/reactivex/Observable;

.field completionIndicator:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0bd4
    .end annotation
.end field

.field description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0bd7
    .end annotation
.end field

.field icon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0bd5
    .end annotation
.end field

.field title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0bd6
    .end annotation
.end field

.field private welcomeCardContent:Lcom/microsoft/xbox/xle/ui/WelcomeCardContent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-string v0, "#FFFFFF"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->WHITE_COLOR:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 48
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    const-string v2, "layout_inflater"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 51
    .local v1, "vi":Landroid/view/LayoutInflater;
    const v2, 0x7f030276

    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 53
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/view/View;)Lbutterknife/Unbinder;

    .line 56
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xboxone/smartglass/R$styleable;->WelcomeCard:[I

    invoke-virtual {v2, p2, v3, v4, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 62
    .local v0, "styledAttributes":Landroid/content/res/TypedArray;
    const/4 v2, 0x0

    .line 63
    :try_start_0
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    .line 64
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    .line 65
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x3

    .line 66
    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 62
    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/xle/ui/WelcomeCardContent;->withContent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/xle/ui/WelcomeCardContent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->setWelcomeCardContent(Lcom/microsoft/xbox/xle/ui/WelcomeCardContent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 72
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->initViews(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 73
    return-void

    .line 69
    :catchall_0
    move-exception v2

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v2
.end method

.method private initViews(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 102
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->welcomeCardContent:Lcom/microsoft/xbox/xle/ui/WelcomeCardContent;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 104
    invoke-static {p0}, Lcom/jakewharton/rxbinding2/view/RxView;->clicks(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->buttonTaps:Lio/reactivex/Observable;

    .line 106
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    const-string v3, "fonts/segoeui.ttf"

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 107
    .local v0, "segoeUIRegular":Landroid/graphics/Typeface;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    const-string v3, "fonts/seguisb.ttf"

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    .line 110
    .local v1, "segoeUISemibold":Landroid/graphics/Typeface;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getMePreferredColor()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->setBackgroundColor(I)V

    .line 113
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->icon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget v3, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->WHITE_COLOR:I

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setTextColor(I)V

    .line 116
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 117
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget v3, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->WHITE_COLOR:I

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setTextColor(I)V

    .line 120
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 121
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v3}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c0151

    invoke-static {v3, v4}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setTextColor(I)V

    .line 124
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->button:Landroid/widget/Button;

    sget v3, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->WHITE_COLOR:I

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setTextColor(I)V

    .line 125
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->button:Landroid/widget/Button;

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v3}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c014e

    invoke-static {v3, v4}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setBackgroundColor(I)V

    .line 126
    return-void
.end method


# virtual methods
.method public getButtonTaps()Lio/reactivex/Observable;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->buttonTaps:Lio/reactivex/Observable;

    return-object v0
.end method

.method public setWelcomeCardCompleted(Z)V
    .locals 2
    .param p1, "completed"    # Z

    .prologue
    .line 76
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->completionIndicator:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 77
    return-void

    .line 76
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setWelcomeCardContent(Lcom/microsoft/xbox/xle/ui/WelcomeCardContent;)V
    .locals 2
    .param p1, "welcomeCardContent"    # Lcom/microsoft/xbox/xle/ui/WelcomeCardContent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 80
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 81
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->welcomeCardContent:Lcom/microsoft/xbox/xle/ui/WelcomeCardContent;

    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->icon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->welcomeCardContent:Lcom/microsoft/xbox/xle/ui/WelcomeCardContent;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/WelcomeCardContent;->icon()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->welcomeCardContent:Lcom/microsoft/xbox/xle/ui/WelcomeCardContent;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/WelcomeCardContent;->title()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->welcomeCardContent:Lcom/microsoft/xbox/xle/ui/WelcomeCardContent;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/WelcomeCardContent;->description()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->button:Landroid/widget/Button;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->welcomeCardContent:Lcom/microsoft/xbox/xle/ui/WelcomeCardContent;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/WelcomeCardContent;->buttonText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->welcomeCardContent:Lcom/microsoft/xbox/xle/ui/WelcomeCardContent;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/WelcomeCardContent;->title()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->welcomeCardContent:Lcom/microsoft/xbox/xle/ui/WelcomeCardContent;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/WelcomeCardContent;->description()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->button:Landroid/widget/Button;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/WelcomeCard;->welcomeCardContent:Lcom/microsoft/xbox/xle/ui/WelcomeCardContent;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/WelcomeCardContent;->buttonText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 93
    return-void
.end method
