.class Lcom/microsoft/xbox/xle/remote/RemoteControl$6;
.super Ljava/lang/Object;
.source "RemoteControl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/remote/RemoteControl;->showUniversalRemote()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/remote/RemoteControl;

.field final synthetic val$scrollView:Landroid/widget/ScrollView;

.field final synthetic val$urcViewControl:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/remote/RemoteControl;Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;Landroid/widget/ScrollView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/remote/RemoteControl;

    .prologue
    .line 288
    iput-object p1, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/RemoteControl;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl$6;->val$urcViewControl:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;

    iput-object p3, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl$6;->val$scrollView:Landroid/widget/ScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 293
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl$6;->this$0:Lcom/microsoft/xbox/xle/remote/RemoteControl;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->access$600(Lcom/microsoft/xbox/xle/remote/RemoteControl;)Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 294
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl$6;->val$urcViewControl:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl$6;->val$scrollView:Landroid/widget/ScrollView;

    new-instance v2, Lcom/microsoft/xbox/toolkit/anim/ExponentialInterpolator;

    const/high16 v3, 0x40e00000    # 7.0f

    sget-object v4, Lcom/microsoft/xbox/toolkit/anim/EasingMode;->EaseOut:Lcom/microsoft/xbox/toolkit/anim/EasingMode;

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/toolkit/anim/ExponentialInterpolator;-><init>(FLcom/microsoft/xbox/toolkit/anim/EasingMode;)V

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->performFirstRunAnimation(Landroid/widget/ScrollView;Landroid/animation/TimeInterpolator;J)V

    .line 295
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl$6;->val$urcViewControl:Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->setVisibility(I)V

    .line 297
    return-void
.end method
