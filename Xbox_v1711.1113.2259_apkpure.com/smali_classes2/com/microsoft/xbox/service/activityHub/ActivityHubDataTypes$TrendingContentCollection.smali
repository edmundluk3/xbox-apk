.class public abstract Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;
.super Ljava/lang/Object;
.source "ActivityHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "TrendingContentCollection"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingContentCollection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    new-instance v0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingContentCollection$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingContentCollection$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract description()Ljava/lang/String;
.end method

.method public abstract items()Lcom/google/gson/JsonArray;
.end method

.method public abstract title()Ljava/lang/String;
.end method
