.class public Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;
.super Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;
.source "TvListingsScreenAdapter.java"


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;)V
    .locals 9
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;

    .prologue
    const/4 v8, 0x1

    .line 24
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;)V

    .line 26
    const v6, 0x7f0e0b1e

    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->screenBody:Landroid/view/View;

    .line 27
    const v6, 0x7f0e0b1f

    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 28
    const-string v6, "TvListingsScreenAdapter"

    iput-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->diagTag:Ljava/lang/String;

    .line 30
    const v6, 0x7f0e0b20

    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/xle/ui/EPGView;

    sget-object v7, Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;->TV_LISTINGS:Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;

    invoke-virtual {p0, v6, v7}, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->setEPGView(Lcom/microsoft/xbox/xle/ui/EPGView;Lcom/microsoft/xbox/xle/epg/LiveTvUtils$Screen;)V

    .line 33
    const v6, 0x7f0e0b04

    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 34
    .local v0, "header":Landroid/view/ViewGroup;
    sget-object v6, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->TopHeading:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedDrawable(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 36
    const v6, 0x7f0e0b06

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->providerBar:Landroid/widget/LinearLayout;

    .line 38
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/ui/EPGView;->createExtraRow()Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    move-result-object v3

    .line 39
    .local v3, "row":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->getScroller()Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    move-result-object v6

    iput-boolean v8, v6, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mRequestDisallowInterceptTouchEventonTouchDown:Z

    .line 40
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->providerBar:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 41
    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->setAlpha(F)V

    .line 42
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v6, v3}, Lcom/microsoft/xbox/xle/ui/EPGView;->addExtraRow(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;)V

    .line 43
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v6, v3}, Lcom/microsoft/xbox/xle/ui/EPGView;->createDayView(Landroid/view/ViewGroup;)V

    .line 45
    const v6, 0x7f0e0b08

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->providerName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 46
    const v6, 0x7f0e0b09

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->providerChevron:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 47
    sget-object v6, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->TopHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedTextColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->providerName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->addTextView(Landroid/widget/TextView;)V

    .line 48
    sget-object v6, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->TopHeadingText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedTextColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->providerChevron:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->addTextView(Landroid/widget/TextView;)V

    .line 50
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->screenBody:Landroid/view/View;

    const v7, 0x7f0e0b22

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->errorStateProviderBar:Landroid/widget/LinearLayout;

    .line 51
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->errorStateProviderBar:Landroid/widget/LinearLayout;

    const v7, 0x7f0e0b23

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->errorStateProviderName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 53
    const v6, 0x7f0e0b07

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    iput-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->providerLogo:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 56
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->updateProviderName()V

    .line 57
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->updateProviderBarState()V

    .line 60
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/ui/EPGView;->createExtraRow()Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;

    move-result-object v4

    .line 61
    .local v4, "timeLine":Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 65
    .local v2, "resources":Landroid/content/res/Resources;
    const v6, 0x7f090584

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v5, v6

    .line 66
    .local v5, "timeLineHeight":I
    const v6, 0x7f0902c8

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v1, v6

    .line 67
    .local v1, "horizontalMargin":I
    new-instance v6, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v7, -0x1

    invoke-direct {v6, v7, v5}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    invoke-virtual {v4, v6}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 69
    invoke-virtual {v0, v4, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 73
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->getX()F

    move-result v6

    int-to-float v7, v1

    sub-float/2addr v6, v7

    invoke-virtual {v4, v6}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->setX(F)V

    .line 75
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v6, v4}, Lcom/microsoft/xbox/xle/ui/EPGView;->addExtraRow(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;)V

    .line 78
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/TvListingsScreenAdapter;->epgView:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/ui/EPGView;->addVerticalScrollAccelerator()V

    .line 81
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGridRow;->getScroller()Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    move-result-object v6

    iput-boolean v8, v6, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->mRequestDisallowInterceptTouchEventonTouchDown:Z

    .line 82
    return-void
.end method
