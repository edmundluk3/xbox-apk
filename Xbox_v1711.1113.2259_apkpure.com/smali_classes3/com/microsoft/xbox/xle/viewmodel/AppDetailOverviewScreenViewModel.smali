.class public Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;
.source "AppDetailOverviewScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/TitlePurchaseViewModel;-><init>()V

    .line 18
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getAppDetailOverviewAdapter(Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 19
    return-void
.end method


# virtual methods
.method public getAverageUserRating()F
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;->getAverageUserRating()F

    move-result v0

    return v0
.end method

.method public getBackgroundImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getContentType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 77
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;

    .line 78
    .local v0, "midd":Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->MediaItemType:Ljava/lang/String;

    goto :goto_0
.end method

.method protected getErrorStringResourceId()I
    .locals 1

    .prologue
    .line 28
    const v0, 0x7f070683

    return v0
.end method

.method public getLanguages()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;->getLanguages()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPublisher()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;->getPublisher()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;->getParentalRating()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public launchApp()V
    .locals 6

    .prologue
    .line 54
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;->getProviders()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;->getProviders()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 55
    :cond_0
    const-string v1, "the provider is empty"

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 67
    :goto_0
    return-void

    .line 59
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;->getProviders()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    .line 60
    .local v0, "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 62
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 63
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaType()I

    move-result v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 64
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaGroup()I

    move-result v3

    sget-object v4, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Full:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    .line 65
    invoke-static {v0, v4}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->getProviderLocation(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v5, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;

    .line 66
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v5

    .line 60
    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchProvider(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;Ljava/lang/String;IILcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto :goto_0
.end method

.method public launchHelp()V
    .locals 5

    .prologue
    .line 70
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getTitleId()J

    move-result-wide v2

    .line 71
    .local v2, "titleId":J
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getProductId()Ljava/lang/String;

    move-result-object v1

    .line 72
    .local v1, "productId":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, "name":Ljava/lang/String;
    invoke-static {v2, v3, v1, v0}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchHelp(JLjava/lang/String;Ljava/lang/String;)V

    .line 74
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 23
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getAppDetailOverviewAdapter(Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 24
    return-void
.end method
