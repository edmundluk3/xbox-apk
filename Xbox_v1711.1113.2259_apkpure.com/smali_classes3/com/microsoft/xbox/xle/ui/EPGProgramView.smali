.class public Lcom/microsoft/xbox/xle/ui/EPGProgramView;
.super Landroid/widget/LinearLayout;
.source "EPGProgramView.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IViewPortAwareness;


# static fields
.field private static mCache:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/microsoft/xbox/xle/ui/EPGProgramView;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mChannel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

.field private mHiddenStartIndicator:Landroid/widget/TextView;

.field private mLastLeft:I

.field private mLastRight:I

.field private mPaddingLeft:I

.field private mPaddingRight:I

.field private mProgram:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

.field private mStartIsHidden:Z

.field private mTitleTextView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 196
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mCache:Ljava/util/LinkedList;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, -0x1

    .line 199
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mLastLeft:I

    .line 56
    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mLastRight:I

    .line 200
    return-void
.end method

.method public static getInstance(Landroid/view/ViewGroup;)Lcom/microsoft/xbox/xle/ui/EPGProgramView;
    .locals 2
    .param p0, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 211
    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mCache:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 213
    invoke-static {p0}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->newInstance(Landroid/view/ViewGroup;)Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    move-result-object v0

    .line 221
    .local v0, "view":Lcom/microsoft/xbox/xle/ui/EPGProgramView;
    :goto_0
    return-object v0

    .line 215
    .end local v0    # "view":Lcom/microsoft/xbox/xle/ui/EPGProgramView;
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mCache:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    .line 218
    .restart local v0    # "view":Lcom/microsoft/xbox/xle/ui/EPGProgramView;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->setPressed(Z)V

    goto :goto_0
.end method

.method public static newInstance(Landroid/view/ViewGroup;)Lcom/microsoft/xbox/xle/ui/EPGProgramView;
    .locals 3
    .param p0, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 204
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 205
    .local v0, "inflator":Landroid/view/LayoutInflater;
    const v1, 0x7f0300f4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    return-object v1
.end method

.method private static reclaimInstance(Lcom/microsoft/xbox/xle/ui/EPGProgramView;)V
    .locals 1
    .param p0, "view"    # Lcom/microsoft/xbox/xle/ui/EPGProgramView;

    .prologue
    .line 225
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mCache:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 226
    return-void
.end method

.method private updateHiddenStartIndicator()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 189
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mStartIsHidden:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x1

    .line 190
    .local v0, "show":Z
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mHiddenStartIndicator:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 191
    return-void

    .end local v0    # "show":Z
    :cond_0
    move v0, v1

    .line 189
    goto :goto_0

    .line 190
    .restart local v0    # "show":Z
    :cond_1
    const/4 v1, 0x4

    goto :goto_1
.end method


# virtual methods
.method public disableStateColors()V
    .locals 3

    .prologue
    .line 145
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->CellHighlightText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedTextColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->removeTextView(Landroid/widget/TextView;)V

    .line 146
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->CellHighlightText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedTextColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mHiddenStartIndicator:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->removeTextView(Landroid/widget/TextView;)V

    .line 148
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->setBackgroundColor(I)V

    .line 149
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00ac

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 150
    return-void
.end method

.method protected displayText(Z)V
    .locals 4
    .param p1, "withFade"    # Z

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 92
    const/4 p1, 0x0

    .line 93
    if-eqz p1, :cond_0

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mTitleTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-wide v2, Lcom/microsoft/xbox/xle/ui/EPGViewConfig;->FADE_IN_DURATION:J

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 100
    :goto_0
    return-void

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setAlpha(F)V

    goto :goto_0
.end method

.method public displayTitle(Z)V
    .locals 2
    .param p1, "animate"    # Z

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->displayText(Z)V

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mTitleTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 87
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->updateHiddenStartIndicator()V

    .line 88
    return-void
.end method

.method public getProgram()Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mProgram:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 60
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 62
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->getPaddingLeft()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mPaddingLeft:I

    .line 63
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->getPaddingRight()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mPaddingRight:I

    .line 64
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mStartIsHidden:Z

    .line 66
    const v1, 0x7f0e0576

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mTitleTextView:Landroid/widget/TextView;

    .line 67
    const v1, 0x7f0e0581

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mHiddenStartIndicator:Landroid/widget/TextView;

    .line 69
    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->CellHighlightText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedTextColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->addTextView(Landroid/widget/TextView;)V

    .line 70
    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->CellHighlightText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedTextColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mHiddenStartIndicator:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->addTextView(Landroid/widget/TextView;)V

    .line 72
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->updateHiddenStartIndicator()V

    .line 75
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00aa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 76
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->CellHighlight:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedDrawable(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->CellHighlight:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    .line 77
    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedDrawable(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 76
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->defaultPressedSelectedDrawable(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 78
    return-void
.end method

.method public reclaim()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 153
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->setSelected(Z)V

    .line 154
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->setPressed(Z)V

    .line 156
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->CellHighlightText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedTextColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->removeTextView(Landroid/widget/TextView;)V

    .line 157
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;->CellHighlightText:Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory;->getSharedTextColor(Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$ColorPurpose;)Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mHiddenStartIndicator:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGDrawableFactory$SharedTextColor;->removeTextView(Landroid/widget/TextView;)V

    .line 159
    invoke-static {p0}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->reclaimInstance(Lcom/microsoft/xbox/xle/ui/EPGProgramView;)V

    .line 160
    return-void
.end method

.method public setProgram(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;ZZ)V
    .locals 4
    .param p1, "program"    # Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;
    .param p2, "channel"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;
    .param p3, "showTitle"    # Z
    .param p4, "animate"    # Z

    .prologue
    const/4 v3, 0x0

    .line 105
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mProgram:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    invoke-static {v1, p1}, Lcom/microsoft/xbox/xle/ui/EPGView;->isTheSameProgram(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mChannel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    invoke-static {v1, p2}, Lcom/microsoft/xbox/xle/ui/EPGView;->isTheSameChannel(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 109
    :cond_1
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mProgram:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    .line 110
    iput-object p2, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mChannel:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    .line 113
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mTitleTextView:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 114
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 117
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->updateHiddenStartIndicator()V

    .line 120
    const-string v0, ""

    .line 121
    .local v0, "title":Ljava/lang/String;
    invoke-static {p2, p1}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->isContentAppropriate(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 122
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mProgram:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowParentSeriesTitle()Ljava/lang/String;

    move-result-object v0

    .line 123
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 124
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mProgram:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowTitle()Ljava/lang/String;

    move-result-object v0

    .line 128
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    invoke-virtual {p0, v3, v3}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->viewPortUpdated(II)V

    .line 134
    if-eqz p3, :cond_0

    .line 135
    invoke-virtual {p0, p4}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->displayTitle(Z)V

    goto :goto_0

    .line 126
    :cond_3
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070481

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    return-void
.end method

.method public viewPortUpdated(II)V
    .locals 6
    .param p1, "left"    # I
    .param p2, "right"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 166
    iget v4, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mLastLeft:I

    if-ne p1, v4, :cond_2

    iget v4, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mLastRight:I

    if-ne p2, v4, :cond_2

    iget-boolean v5, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mStartIsHidden:Z

    if-lez p1, :cond_1

    move v4, v3

    :goto_0
    if-ne v5, v4, :cond_2

    .line 185
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v4, v2

    .line 166
    goto :goto_0

    .line 170
    :cond_2
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mLastLeft:I

    .line 171
    iput p2, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mLastRight:I

    .line 173
    if-lez p1, :cond_3

    move v2, v3

    :cond_3
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mStartIsHidden:Z

    .line 174
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->updateHiddenStartIndicator()V

    .line 176
    iget v2, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mPaddingLeft:I

    add-int v0, p1, v2

    .line 177
    .local v0, "desiredPaddingLeft":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->getPaddingLeft()I

    move-result v1

    .line 179
    .local v1, "originalPaddingleft":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->getPaddingTop()I

    move-result v2

    iget v4, p0, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->mPaddingRight:I

    .line 180
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->getPaddingBottom()I

    move-result v5

    .line 179
    invoke-virtual {p0, v0, v2, v4, v5}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->setPadding(IIII)V

    .line 182
    if-eq v0, v1, :cond_0

    .line 183
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/EPGProgramView;->displayText(Z)V

    goto :goto_1
.end method
