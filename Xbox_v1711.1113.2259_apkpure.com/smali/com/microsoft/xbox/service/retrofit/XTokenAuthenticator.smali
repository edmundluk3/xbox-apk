.class public final Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;
.super Ljava/lang/Object;
.source "XTokenAuthenticator.java"

# interfaces
.implements Lokhttp3/Authenticator;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method


# virtual methods
.method public authenticate(Lokhttp3/Route;Lokhttp3/Response;)Lokhttp3/Request;
    .locals 6
    .param p1, "route"    # Lokhttp3/Route;
    .param p2, "response"    # Lokhttp3/Response;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    invoke-static {}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->getInstance()Lcom/microsoft/xbox/idp/interop/XsapiUser;

    move-result-object v2

    .line 29
    invoke-virtual {p2}, Lokhttp3/Response;->request()Lokhttp3/Request;

    move-result-object v3

    invoke-virtual {v3}, Lokhttp3/Request;->method()Ljava/lang/String;

    move-result-object v3

    .line 30
    invoke-virtual {p2}, Lokhttp3/Response;->request()Lokhttp3/Request;

    move-result-object v4

    invoke-virtual {v4}, Lokhttp3/Request;->url()Lokhttp3/HttpUrl;

    move-result-object v4

    invoke-virtual {v4}, Lokhttp3/HttpUrl;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    .line 28
    invoke-virtual {v2, v3, v4, v5}, Lcom/microsoft/xbox/idp/interop/XsapiUser;->getNewTokenAndSignatureSync(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;

    move-result-object v2

    .line 32
    invoke-virtual {v2}, Lcom/microsoft/xbox/idp/interop/TokenAndSignatureResult;->getToken()Lcom/microsoft/xbox/idp/interop/TokenAndSignature;

    move-result-object v1

    .line 34
    .local v1, "tokenAndSignature":Lcom/microsoft/xbox/idp/interop/TokenAndSignature;
    if-eqz v1, :cond_0

    .line 35
    invoke-virtual {v1}, Lcom/microsoft/xbox/idp/interop/TokenAndSignature;->getToken()Ljava/lang/String;

    move-result-object v0

    .line 36
    .local v0, "newXToken":Ljava/lang/String;
    invoke-virtual {p2}, Lokhttp3/Response;->request()Lokhttp3/Request;

    move-result-object v2

    invoke-virtual {v2}, Lokhttp3/Request;->newBuilder()Lokhttp3/Request$Builder;

    move-result-object v2

    const-string v3, "Authorization"

    invoke-virtual {v2, v3, v0}, Lokhttp3/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v2

    .line 39
    .end local v0    # "newXToken":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 38
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;->TAG:Ljava/lang/String;

    const-string v3, "Failed to refresh XToken"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    const/4 v2, 0x0

    goto :goto_0
.end method
