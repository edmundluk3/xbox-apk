.class public abstract Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
.super Ljava/lang/Object;
.source "ClubAccountsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ClubAccountsResponse"
.end annotation


# static fields
.field public static final MSA_CHECK_FAILED:I = 0x40d


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$hasOwnerSuspension$0(Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;)Z
    .locals 2
    .param p0, "s"    # Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;->actor()Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;->Owner:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newErrorResponse(ILjava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    .locals 13
    .param p0, "code"    # I
    .param p1, "description"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 208
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 209
    new-instance v1, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;

    const-wide/16 v6, -0x1

    const/4 v10, 0x0

    move v2, p0

    move-object v3, p1

    move-object v5, v4

    move-object v8, v4

    move-object v9, v4

    move-object v11, v4

    move-object v12, v4

    invoke-direct/range {v1 .. v12}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;Ljava/lang/String;ZLjava/util/Date;Lcom/google/common/collect/ImmutableList;)V

    return-object v1
.end method

.method public static newSuccessResponse(Ljava/lang/String;Ljava/lang/String;JLcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;Ljava/lang/String;ZLjava/util/Date;Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    .locals 14
    .param p0, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "owner"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "id"    # J
    .param p4, "clubType"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "created"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "canDeleteImmediately"    # Z
    .param p7, "suspensionRequiredAfter"    # Ljava/util/Date;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Date;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;"
        }
    .end annotation

    .prologue
    .line 200
    .local p8, "suspensions":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 201
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 203
    if-eqz p8, :cond_0

    invoke-static/range {p8 .. p8}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v12

    .line 204
    .local v12, "suspensionsCopy":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;>;"
    :goto_0
    new-instance v1, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v4, p0

    move-object v5, p1

    move-wide/from16 v6, p2

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move/from16 v10, p6

    move-object/from16 v11, p7

    invoke-direct/range {v1 .. v12}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;Ljava/lang/String;ZLjava/util/Date;Lcom/google/common/collect/ImmutableList;)V

    return-object v1

    .line 203
    .end local v12    # "suspensionsCopy":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;>;"
    :cond_0
    const/4 v12, 0x0

    goto :goto_0
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 231
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract canDeleteImmediately()Z
.end method

.method public abstract code()I
.end method

.method public abstract created()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract description()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public getSuspensionRequiredAfter()Ljava/util/Date;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->suspensionRequiredAfter()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/Date;

    .line 226
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->suspensionRequiredAfter()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 225
    :goto_0
    return-object v0

    .line 226
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasOwnerSuspension()Z
    .locals 2

    .prologue
    .line 217
    .line 218
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->suspensions()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse$$Lambda$1;->lambdaFactory$()Lcom/microsoft/xbox/toolkit/java8/Predicate;

    move-result-object v1

    .line 217
    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/ListUtil;->filter(Ljava/util/List;Lcom/microsoft/xbox/toolkit/java8/Predicate;)Ljava/util/List;

    move-result-object v0

    .line 220
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 217
    :goto_0
    return v0

    .line 220
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract id()J
.end method

.method public isSuccess()Z
    .locals 1

    .prologue
    .line 213
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->code()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract name()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract owner()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract suspensionRequiredAfter()Ljava/util/Date;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract suspensions()Lcom/google/common/collect/ImmutableList;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;",
            ">;"
        }
    .end annotation
.end method

.method public abstract type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
