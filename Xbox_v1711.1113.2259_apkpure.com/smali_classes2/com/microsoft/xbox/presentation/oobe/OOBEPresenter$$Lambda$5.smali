.class final synthetic Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$5;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;

.field private final arg$2:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;

.field private final arg$3:Lcom/microsoft/xbox/toolkit/MyProfileProvider;

.field private final arg$4:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

.field private final arg$5:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

.field private final arg$6:Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;Lcom/microsoft/xbox/toolkit/MyProfileProvider;Lcom/microsoft/xbox/domain/auth/AuthStateManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$5;->arg$1:Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;

    iput-object p2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$5;->arg$2:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;

    iput-object p3, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$5;->arg$3:Lcom/microsoft/xbox/toolkit/MyProfileProvider;

    iput-object p4, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$5;->arg$4:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    iput-object p5, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$5;->arg$5:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    iput-object p6, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$5;->arg$6:Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;Lcom/microsoft/xbox/toolkit/MyProfileProvider;Lcom/microsoft/xbox/domain/auth/AuthStateManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;)Lio/reactivex/functions/Function;
    .locals 7

    new-instance v0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$5;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$5;-><init>(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;Lcom/microsoft/xbox/toolkit/MyProfileProvider;Lcom/microsoft/xbox/domain/auth/AuthStateManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$5;->arg$1:Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$5;->arg$2:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$5;->arg$3:Lcom/microsoft/xbox/toolkit/MyProfileProvider;

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$5;->arg$4:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    iget-object v4, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$5;->arg$5:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    iget-object v5, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter$$Lambda$5;->arg$6:Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;

    move-object v6, p1

    check-cast v6, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    invoke-static/range {v0 .. v6}, Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;->lambda$null$24(Lcom/microsoft/xbox/presentation/oobe/OOBEPresenter;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;Lcom/microsoft/xbox/toolkit/MyProfileProvider;Lcom/microsoft/xbox/domain/auth/AuthStateManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/presentation/oobe/OOBENavigator;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)Lio/reactivex/ObservableSource;

    move-result-object v0

    return-object v0
.end method
