.class public abstract Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;
.super Ljava/lang/Object;
.source "EditorialDataTypes.java"

# interfaces
.implements Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "SystemTag"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 98
    new-instance v0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTag$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTag$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public getDisplayText()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;->id()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSymbol()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;->icon()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract icon()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "systemTag.tagEmoji"
    .end annotation
.end method

.method public abstract id()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "systemTag.tagID"
    .end annotation
.end method

.method public abstract name()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "systemTag.tagName"
    .end annotation
.end method
