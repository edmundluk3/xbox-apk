.class public Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl_ViewBinding;
.super Ljava/lang/Object;
.source "OOBEViewImpl_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;

.field private view2131625998:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;)V
    .locals 0
    .param p1, "target"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl_ViewBinding;-><init>(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;Landroid/view/View;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;Landroid/view/View;)V
    .locals 5
    .param p1, "target"    # Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    const v3, 0x7f0e080e

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl_ViewBinding;->target:Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;

    .line 37
    const-string v1, "field \'closeButton\' and method \'onClose\'"

    invoke-static {p2, v3, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 38
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'closeButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {v0, v3, v1, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->closeButton:Landroid/widget/Button;

    .line 39
    iput-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl_ViewBinding;->view2131625998:Landroid/view/View;

    .line 40
    new-instance v1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl_ViewBinding$1;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl_ViewBinding$1;-><init>(Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl_ViewBinding;Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    const v1, 0x7f0e0815

    const-string v2, "field \'switchPanel\'"

    const-class v3, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 47
    const v1, 0x7f0e0826

    const-string v2, "field \'backButton\'"

    const-class v3, Landroid/widget/Button;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->backButton:Landroid/widget/Button;

    .line 48
    const v1, 0x7f0e0827

    const-string v2, "field \'nextButton\'"

    const-class v3, Landroid/widget/Button;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->nextButton:Landroid/widget/Button;

    .line 49
    const v1, 0x7f0e0816

    const-string v2, "field \'enterCodeHeader\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->enterCodeHeader:Landroid/widget/TextView;

    .line 50
    const v1, 0x7f0e0817

    const-string v2, "field \'enterCodeDetails\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->enterCodeDetails:Landroid/widget/TextView;

    .line 51
    const v1, 0x7f0e0818

    const-string v2, "field \'enterCodeInput\'"

    const-class v3, Landroid/widget/EditText;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->enterCodeInput:Landroid/widget/EditText;

    .line 52
    const v1, 0x7f0e0819

    const-string v2, "field \'enterCodeError\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->enterCodeError:Landroid/widget/TextView;

    .line 53
    const v1, 0x7f0e081a

    const-string v2, "field \'timeZoneHeader\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->timeZoneHeader:Landroid/widget/TextView;

    .line 54
    const v1, 0x7f0e081b

    const-string v2, "field \'timeZoneSpinner\'"

    const-class v3, Landroid/widget/Spinner;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->timeZoneSpinner:Landroid/widget/Spinner;

    .line 55
    const v1, 0x7f0e081c

    const-string v2, "field \'timeZoneAutoAdjustCheckbox\'"

    const-class v3, Landroid/widget/CheckBox;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->timeZoneAutoAdjustCheckbox:Landroid/widget/CheckBox;

    .line 56
    const v1, 0x7f0e081d

    const-string v2, "field \'powerSettingsHeader\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->powerSettingsHeader:Landroid/widget/TextView;

    .line 57
    const v1, 0x7f0e081e

    const-string v2, "field \'powerSettingsGroup\'"

    const-class v3, Landroid/widget/RadioGroup;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioGroup;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->powerSettingsGroup:Landroid/widget/RadioGroup;

    .line 58
    const v1, 0x7f0e081f

    const-string v2, "field \'powerEnergySaving\'"

    const-class v3, Landroid/widget/RadioButton;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->powerEnergySaving:Landroid/widget/RadioButton;

    .line 59
    const v1, 0x7f0e0820

    const-string v2, "field \'powerInstantOn\'"

    const-class v3, Landroid/widget/RadioButton;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->powerInstantOn:Landroid/widget/RadioButton;

    .line 60
    const v1, 0x7f0e0821

    const-string v2, "field \'updateSettingsHeader\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->updateSettingsHeader:Landroid/widget/TextView;

    .line 61
    const v1, 0x7f0e0822

    const-string v2, "field \'updateConsoleAutomaticallyCheckbox\'"

    const-class v3, Landroid/widget/CheckBox;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->updateConsoleAutomaticallyCheckbox:Landroid/widget/CheckBox;

    .line 62
    const v1, 0x7f0e0823

    const-string v2, "field \'updateGamesAutomaticallyCheckbox\'"

    const-class v3, Landroid/widget/CheckBox;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->updateGamesAutomaticallyCheckbox:Landroid/widget/CheckBox;

    .line 63
    const v1, 0x7f0e0824

    const-string v2, "field \'completionHeader\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->completionHeader:Landroid/widget/TextView;

    .line 64
    const v1, 0x7f0e0825

    const-string v2, "field \'completionDescription\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->completionDescription:Landroid/widget/TextView;

    .line 65
    const/4 v1, 0x5

    new-array v1, v1, [Landroid/view/View;

    const/4 v2, 0x0

    const v3, 0x7f0e0810

    const-string v4, "field \'progressViews\'"

    .line 66
    invoke-static {p2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const v3, 0x7f0e0811

    const-string v4, "field \'progressViews\'"

    .line 67
    invoke-static {p2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const v3, 0x7f0e0812

    const-string v4, "field \'progressViews\'"

    .line 68
    invoke-static {p2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const v3, 0x7f0e0813

    const-string v4, "field \'progressViews\'"

    .line 69
    invoke-static {p2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const v3, 0x7f0e0814

    const-string v4, "field \'progressViews\'"

    .line 70
    invoke-static {p2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v3

    aput-object v3, v1, v2

    .line 65
    invoke-static {v1}, Lbutterknife/internal/Utils;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p1, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->progressViews:Ljava/util/List;

    .line 71
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl_ViewBinding;->target:Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;

    .line 77
    .local v0, "target":Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 78
    :cond_0
    iput-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl_ViewBinding;->target:Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;

    .line 80
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->closeButton:Landroid/widget/Button;

    .line 81
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 82
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->backButton:Landroid/widget/Button;

    .line 83
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->nextButton:Landroid/widget/Button;

    .line 84
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->enterCodeHeader:Landroid/widget/TextView;

    .line 85
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->enterCodeDetails:Landroid/widget/TextView;

    .line 86
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->enterCodeInput:Landroid/widget/EditText;

    .line 87
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->enterCodeError:Landroid/widget/TextView;

    .line 88
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->timeZoneHeader:Landroid/widget/TextView;

    .line 89
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->timeZoneSpinner:Landroid/widget/Spinner;

    .line 90
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->timeZoneAutoAdjustCheckbox:Landroid/widget/CheckBox;

    .line 91
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->powerSettingsHeader:Landroid/widget/TextView;

    .line 92
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->powerSettingsGroup:Landroid/widget/RadioGroup;

    .line 93
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->powerEnergySaving:Landroid/widget/RadioButton;

    .line 94
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->powerInstantOn:Landroid/widget/RadioButton;

    .line 95
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->updateSettingsHeader:Landroid/widget/TextView;

    .line 96
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->updateConsoleAutomaticallyCheckbox:Landroid/widget/CheckBox;

    .line 97
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->updateGamesAutomaticallyCheckbox:Landroid/widget/CheckBox;

    .line 98
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->completionHeader:Landroid/widget/TextView;

    .line 99
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->completionDescription:Landroid/widget/TextView;

    .line 100
    iput-object v2, v0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl;->progressViews:Ljava/util/List;

    .line 102
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl_ViewBinding;->view2131625998:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    iput-object v2, p0, Lcom/microsoft/xbox/presentation/oobe/OOBEViewImpl_ViewBinding;->view2131625998:Landroid/view/View;

    .line 104
    return-void
.end method
