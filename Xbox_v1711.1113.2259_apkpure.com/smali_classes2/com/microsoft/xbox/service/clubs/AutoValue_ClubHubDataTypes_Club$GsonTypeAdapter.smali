.class public final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_ClubHubDataTypes_Club.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
        ">;"
    }
.end annotation


# instance fields
.field private final bannerImageUrlAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final clubPresenceAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;",
            ">;>;"
        }
    .end annotation
.end field

.field private final clubPresenceCountAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final clubPresenceInGameCountAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final clubPresenceTodayCountAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final clubTypeAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final creationDateUtcAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultBannerImageUrl:Ljava/lang/String;

.field private defaultClubPresence:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;",
            ">;"
        }
    .end annotation
.end field

.field private defaultClubPresenceCount:J

.field private defaultClubPresenceInGameCount:J

.field private defaultClubPresenceTodayCount:J

.field private defaultClubType:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

.field private defaultCreationDateUtc:Ljava/lang/String;

.field private defaultFollowersCount:J

.field private defaultFounderXuid:J

.field private defaultGlyphImageUrl:Ljava/lang/String;

.field private defaultId:J

.field private defaultMaxMembersInGame:J

.field private defaultMaxMembersPerClub:J

.field private defaultMembersCount:J

.field private defaultModeratorsCount:J

.field private defaultOwnerXuid:Ljava/lang/String;

.field private defaultProfile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

.field private defaultRecommendation:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

.field private defaultRecommendedCount:J

.field private defaultReportCount:J

.field private defaultReportedItemsCount:J

.field private defaultRequestedToJoinCount:J

.field private defaultRoster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

.field private defaultSettings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

.field private defaultShortName:Ljava/lang/String;

.field private defaultState:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

.field private defaultSuspendedUntilUtc:Ljava/util/Date;

.field private defaultTargetRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

.field private defaultTitleDeeplinks:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

.field private final followersCountAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final founderXuidAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final glyphImageUrlAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final idAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final maxMembersInGameAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final maxMembersPerClubAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final membersCountAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final moderatorsCountAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final ownerXuidAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final profileAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;",
            ">;"
        }
    .end annotation
.end field

.field private final recommendationAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;",
            ">;"
        }
    .end annotation
.end field

.field private final recommendedCountAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final reportCountAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final reportedItemsCountAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final requestedToJoinCountAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final rosterAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final shortNameAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final stateAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;",
            ">;"
        }
    .end annotation
.end field

.field private final suspendedUntilUtcAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private final targetRolesAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;",
            ">;"
        }
    .end annotation
.end field

.field private final titleDeeplinksAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 4
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 91
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 62
    iput-wide v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultId:J

    .line 63
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultProfile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    .line 64
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultClubType:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    .line 65
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultShortName:Ljava/lang/String;

    .line 66
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultOwnerXuid:Ljava/lang/String;

    .line 67
    iput-wide v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultFounderXuid:J

    .line 68
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultCreationDateUtc:Ljava/lang/String;

    .line 69
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultGlyphImageUrl:Ljava/lang/String;

    .line 70
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultBannerImageUrl:Ljava/lang/String;

    .line 71
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultSettings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    .line 72
    iput-wide v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultFollowersCount:J

    .line 73
    iput-wide v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultMembersCount:J

    .line 74
    iput-wide v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultModeratorsCount:J

    .line 75
    iput-wide v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultRecommendedCount:J

    .line 76
    iput-wide v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultRequestedToJoinCount:J

    .line 77
    iput-wide v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultClubPresenceCount:J

    .line 78
    iput-wide v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultClubPresenceTodayCount:J

    .line 79
    iput-wide v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultClubPresenceInGameCount:J

    .line 80
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultRoster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    .line 81
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultTargetRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    .line 82
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultRecommendation:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    .line 83
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultClubPresence:Lcom/google/common/collect/ImmutableList;

    .line 84
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultTitleDeeplinks:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    .line 85
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultState:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    .line 86
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultSuspendedUntilUtc:Ljava/util/Date;

    .line 87
    iput-wide v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultReportCount:J

    .line 88
    iput-wide v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultReportedItemsCount:J

    .line 89
    iput-wide v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultMaxMembersPerClub:J

    .line 90
    iput-wide v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultMaxMembersInGame:J

    .line 92
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->idAdapter:Lcom/google/gson/TypeAdapter;

    .line 93
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->profileAdapter:Lcom/google/gson/TypeAdapter;

    .line 94
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->clubTypeAdapter:Lcom/google/gson/TypeAdapter;

    .line 95
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->shortNameAdapter:Lcom/google/gson/TypeAdapter;

    .line 96
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->ownerXuidAdapter:Lcom/google/gson/TypeAdapter;

    .line 97
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->founderXuidAdapter:Lcom/google/gson/TypeAdapter;

    .line 98
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->creationDateUtcAdapter:Lcom/google/gson/TypeAdapter;

    .line 99
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->glyphImageUrlAdapter:Lcom/google/gson/TypeAdapter;

    .line 100
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->bannerImageUrlAdapter:Lcom/google/gson/TypeAdapter;

    .line 101
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->settingsAdapter:Lcom/google/gson/TypeAdapter;

    .line 102
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->followersCountAdapter:Lcom/google/gson/TypeAdapter;

    .line 103
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->membersCountAdapter:Lcom/google/gson/TypeAdapter;

    .line 104
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->moderatorsCountAdapter:Lcom/google/gson/TypeAdapter;

    .line 105
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->recommendedCountAdapter:Lcom/google/gson/TypeAdapter;

    .line 106
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->requestedToJoinCountAdapter:Lcom/google/gson/TypeAdapter;

    .line 107
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->clubPresenceCountAdapter:Lcom/google/gson/TypeAdapter;

    .line 108
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->clubPresenceTodayCountAdapter:Lcom/google/gson/TypeAdapter;

    .line 109
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->clubPresenceInGameCountAdapter:Lcom/google/gson/TypeAdapter;

    .line 110
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->rosterAdapter:Lcom/google/gson/TypeAdapter;

    .line 111
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->targetRolesAdapter:Lcom/google/gson/TypeAdapter;

    .line 112
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->recommendationAdapter:Lcom/google/gson/TypeAdapter;

    .line 113
    const-class v0, Lcom/google/common/collect/ImmutableList;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/reflect/Type;

    const/4 v2, 0x0

    const-class v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->clubPresenceAdapter:Lcom/google/gson/TypeAdapter;

    .line 114
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->titleDeeplinksAdapter:Lcom/google/gson/TypeAdapter;

    .line 115
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->stateAdapter:Lcom/google/gson/TypeAdapter;

    .line 116
    const-class v0, Ljava/util/Date;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->suspendedUntilUtcAdapter:Lcom/google/gson/TypeAdapter;

    .line 117
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->reportCountAdapter:Lcom/google/gson/TypeAdapter;

    .line 118
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->reportedItemsCountAdapter:Lcom/google/gson/TypeAdapter;

    .line 119
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->maxMembersPerClubAdapter:Lcom/google/gson/TypeAdapter;

    .line 120
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->maxMembersInGameAdapter:Lcom/google/gson/TypeAdapter;

    .line 121
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .locals 48
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 307
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v3

    sget-object v47, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    move-object/from16 v0, v47

    if-ne v3, v0, :cond_0

    .line 308
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 309
    const/4 v3, 0x0

    .line 470
    :goto_0
    return-object v3

    .line 311
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 312
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultId:J

    .line 313
    .local v4, "id":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultProfile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    .line 314
    .local v6, "profile":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultClubType:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    .line 315
    .local v7, "clubType":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultShortName:Ljava/lang/String;

    .line 316
    .local v8, "shortName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultOwnerXuid:Ljava/lang/String;

    .line 317
    .local v9, "ownerXuid":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultFounderXuid:J

    .line 318
    .local v10, "founderXuid":J
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultCreationDateUtc:Ljava/lang/String;

    .line 319
    .local v12, "creationDateUtc":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultGlyphImageUrl:Ljava/lang/String;

    .line 320
    .local v13, "glyphImageUrl":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultBannerImageUrl:Ljava/lang/String;

    .line 321
    .local v14, "bannerImageUrl":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultSettings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    .line 322
    .local v15, "settings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultFollowersCount:J

    move-wide/from16 v16, v0

    .line 323
    .local v16, "followersCount":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultMembersCount:J

    move-wide/from16 v18, v0

    .line 324
    .local v18, "membersCount":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultModeratorsCount:J

    move-wide/from16 v20, v0

    .line 325
    .local v20, "moderatorsCount":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultRecommendedCount:J

    move-wide/from16 v22, v0

    .line 326
    .local v22, "recommendedCount":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultRequestedToJoinCount:J

    move-wide/from16 v24, v0

    .line 327
    .local v24, "requestedToJoinCount":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultClubPresenceCount:J

    move-wide/from16 v26, v0

    .line 328
    .local v26, "clubPresenceCount":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultClubPresenceTodayCount:J

    move-wide/from16 v28, v0

    .line 329
    .local v28, "clubPresenceTodayCount":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultClubPresenceInGameCount:J

    move-wide/from16 v30, v0

    .line 330
    .local v30, "clubPresenceInGameCount":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultRoster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-object/from16 v32, v0

    .line 331
    .local v32, "roster":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultTargetRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    move-object/from16 v33, v0

    .line 332
    .local v33, "targetRoles":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultRecommendation:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    move-object/from16 v34, v0

    .line 333
    .local v34, "recommendation":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultClubPresence:Lcom/google/common/collect/ImmutableList;

    move-object/from16 v35, v0

    .line 334
    .local v35, "clubPresence":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultTitleDeeplinks:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    move-object/from16 v36, v0

    .line 335
    .local v36, "titleDeeplinks":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultState:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    move-object/from16 v37, v0

    .line 336
    .local v37, "state":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultSuspendedUntilUtc:Ljava/util/Date;

    move-object/from16 v38, v0

    .line 337
    .local v38, "suspendedUntilUtc":Ljava/util/Date;
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultReportCount:J

    move-wide/from16 v39, v0

    .line 338
    .local v39, "reportCount":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultReportedItemsCount:J

    move-wide/from16 v41, v0

    .line 339
    .local v41, "reportedItemsCount":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultMaxMembersPerClub:J

    move-wide/from16 v43, v0

    .line 340
    .local v43, "maxMembersPerClub":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultMaxMembersInGame:J

    move-wide/from16 v45, v0

    .line 341
    .local v45, "maxMembersInGame":J
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 342
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v2

    .line 343
    .local v2, "_name":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v3

    sget-object v47, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    move-object/from16 v0, v47

    if-ne v3, v0, :cond_1

    .line 344
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 347
    :cond_1
    const/4 v3, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v47

    sparse-switch v47, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v3, :pswitch_data_0

    .line 465
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 347
    :sswitch_0
    const-string v47, "id"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/4 v3, 0x0

    goto :goto_2

    :sswitch_1
    const-string v47, "profile"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/4 v3, 0x1

    goto :goto_2

    :sswitch_2
    const-string v47, "clubType"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/4 v3, 0x2

    goto :goto_2

    :sswitch_3
    const-string v47, "shortName"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/4 v3, 0x3

    goto :goto_2

    :sswitch_4
    const-string v47, "ownerXuid"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/4 v3, 0x4

    goto :goto_2

    :sswitch_5
    const-string v47, "founderXuid"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/4 v3, 0x5

    goto :goto_2

    :sswitch_6
    const-string v47, "creationDateUtc"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/4 v3, 0x6

    goto :goto_2

    :sswitch_7
    const-string v47, "glyphImageUrl"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/4 v3, 0x7

    goto :goto_2

    :sswitch_8
    const-string v47, "bannerImageUrl"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/16 v3, 0x8

    goto :goto_2

    :sswitch_9
    const-string v47, "settings"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/16 v3, 0x9

    goto :goto_2

    :sswitch_a
    const-string v47, "followersCount"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/16 v3, 0xa

    goto/16 :goto_2

    :sswitch_b
    const-string v47, "membersCount"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/16 v3, 0xb

    goto/16 :goto_2

    :sswitch_c
    const-string v47, "moderatorsCount"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/16 v3, 0xc

    goto/16 :goto_2

    :sswitch_d
    const-string v47, "recommendedCount"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/16 v3, 0xd

    goto/16 :goto_2

    :sswitch_e
    const-string v47, "requestedToJoinCount"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/16 v3, 0xe

    goto/16 :goto_2

    :sswitch_f
    const-string v47, "clubPresenceCount"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/16 v3, 0xf

    goto/16 :goto_2

    :sswitch_10
    const-string v47, "clubPresenceTodayCount"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/16 v3, 0x10

    goto/16 :goto_2

    :sswitch_11
    const-string v47, "clubPresenceInGameCount"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/16 v3, 0x11

    goto/16 :goto_2

    :sswitch_12
    const-string v47, "roster"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/16 v3, 0x12

    goto/16 :goto_2

    :sswitch_13
    const-string v47, "targetRoles"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/16 v3, 0x13

    goto/16 :goto_2

    :sswitch_14
    const-string v47, "recommendation"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/16 v3, 0x14

    goto/16 :goto_2

    :sswitch_15
    const-string v47, "clubPresence"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/16 v3, 0x15

    goto/16 :goto_2

    :sswitch_16
    const-string v47, "titleDeeplinks"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/16 v3, 0x16

    goto/16 :goto_2

    :sswitch_17
    const-string v47, "state"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/16 v3, 0x17

    goto/16 :goto_2

    :sswitch_18
    const-string v47, "suspendedUntilUtc"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/16 v3, 0x18

    goto/16 :goto_2

    :sswitch_19
    const-string v47, "reportCount"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/16 v3, 0x19

    goto/16 :goto_2

    :sswitch_1a
    const-string v47, "reportedItemsCount"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/16 v3, 0x1a

    goto/16 :goto_2

    :sswitch_1b
    const-string v47, "maxMembersPerClub"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/16 v3, 0x1b

    goto/16 :goto_2

    :sswitch_1c
    const-string v47, "maxMembersInGame"

    move-object/from16 v0, v47

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_2

    const/16 v3, 0x1c

    goto/16 :goto_2

    .line 349
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->idAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 350
    goto/16 :goto_1

    .line 353
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->profileAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "profile":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;
    check-cast v6, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    .line 354
    .restart local v6    # "profile":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;
    goto/16 :goto_1

    .line 357
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->clubTypeAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "clubType":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;
    check-cast v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    .line 358
    .restart local v7    # "clubType":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;
    goto/16 :goto_1

    .line 361
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->shortNameAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "shortName":Ljava/lang/String;
    check-cast v8, Ljava/lang/String;

    .line 362
    .restart local v8    # "shortName":Ljava/lang/String;
    goto/16 :goto_1

    .line 365
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->ownerXuidAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "ownerXuid":Ljava/lang/String;
    check-cast v9, Ljava/lang/String;

    .line 366
    .restart local v9    # "ownerXuid":Ljava/lang/String;
    goto/16 :goto_1

    .line 369
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->founderXuidAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 370
    goto/16 :goto_1

    .line 373
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->creationDateUtcAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v12

    .end local v12    # "creationDateUtc":Ljava/lang/String;
    check-cast v12, Ljava/lang/String;

    .line 374
    .restart local v12    # "creationDateUtc":Ljava/lang/String;
    goto/16 :goto_1

    .line 377
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->glyphImageUrlAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v13

    .end local v13    # "glyphImageUrl":Ljava/lang/String;
    check-cast v13, Ljava/lang/String;

    .line 378
    .restart local v13    # "glyphImageUrl":Ljava/lang/String;
    goto/16 :goto_1

    .line 381
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->bannerImageUrlAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v14

    .end local v14    # "bannerImageUrl":Ljava/lang/String;
    check-cast v14, Ljava/lang/String;

    .line 382
    .restart local v14    # "bannerImageUrl":Ljava/lang/String;
    goto/16 :goto_1

    .line 385
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->settingsAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v15

    .end local v15    # "settings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    check-cast v15, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    .line 386
    .restart local v15    # "settings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    goto/16 :goto_1

    .line 389
    :pswitch_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->followersCountAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 390
    goto/16 :goto_1

    .line 393
    :pswitch_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->membersCountAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    .line 394
    goto/16 :goto_1

    .line 397
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->moderatorsCountAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    .line 398
    goto/16 :goto_1

    .line 401
    :pswitch_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->recommendedCountAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    .line 402
    goto/16 :goto_1

    .line 405
    :pswitch_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->requestedToJoinCountAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v24

    .line 406
    goto/16 :goto_1

    .line 409
    :pswitch_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->clubPresenceCountAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v26

    .line 410
    goto/16 :goto_1

    .line 413
    :pswitch_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->clubPresenceTodayCountAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v28

    .line 414
    goto/16 :goto_1

    .line 417
    :pswitch_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->clubPresenceInGameCountAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v30

    .line 418
    goto/16 :goto_1

    .line 421
    :pswitch_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->rosterAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v32

    .end local v32    # "roster":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    check-cast v32, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    .line 422
    .restart local v32    # "roster":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    goto/16 :goto_1

    .line 425
    :pswitch_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->targetRolesAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v33

    .end local v33    # "targetRoles":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;
    check-cast v33, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    .line 426
    .restart local v33    # "targetRoles":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;
    goto/16 :goto_1

    .line 429
    :pswitch_14
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->recommendationAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v34

    .end local v34    # "recommendation":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;
    check-cast v34, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    .line 430
    .restart local v34    # "recommendation":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;
    goto/16 :goto_1

    .line 433
    :pswitch_15
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->clubPresenceAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v35

    .end local v35    # "clubPresence":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;>;"
    check-cast v35, Lcom/google/common/collect/ImmutableList;

    .line 434
    .restart local v35    # "clubPresence":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;>;"
    goto/16 :goto_1

    .line 437
    :pswitch_16
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->titleDeeplinksAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v36

    .end local v36    # "titleDeeplinks":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;
    check-cast v36, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    .line 438
    .restart local v36    # "titleDeeplinks":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;
    goto/16 :goto_1

    .line 441
    :pswitch_17
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->stateAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v37

    .end local v37    # "state":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;
    check-cast v37, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    .line 442
    .restart local v37    # "state":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;
    goto/16 :goto_1

    .line 445
    :pswitch_18
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->suspendedUntilUtcAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v38

    .end local v38    # "suspendedUntilUtc":Ljava/util/Date;
    check-cast v38, Ljava/util/Date;

    .line 446
    .restart local v38    # "suspendedUntilUtc":Ljava/util/Date;
    goto/16 :goto_1

    .line 449
    :pswitch_19
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->reportCountAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v39

    .line 450
    goto/16 :goto_1

    .line 453
    :pswitch_1a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->reportedItemsCountAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v41

    .line 454
    goto/16 :goto_1

    .line 457
    :pswitch_1b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->maxMembersPerClubAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v43

    .line 458
    goto/16 :goto_1

    .line 461
    :pswitch_1c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->maxMembersInGameAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v45

    .line 462
    goto/16 :goto_1

    .line 469
    .end local v2    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 470
    new-instance v3, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club;

    invoke-direct/range {v3 .. v46}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club;-><init>(JLcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;JJJJJJJJLcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;Lcom/google/common/collect/ImmutableList;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;Ljava/util/Date;JJJJ)V

    goto/16 :goto_0

    .line 347
    nop

    :sswitch_data_0
    .sparse-switch
        -0x7db1c4a0 -> :sswitch_7
        -0x7cf98054 -> :sswitch_13
        -0x78e42ad9 -> :sswitch_3
        -0x74a9c962 -> :sswitch_1b
        -0x722cbc8c -> :sswitch_d
        -0x6448f442 -> :sswitch_f
        -0x5d3e3e06 -> :sswitch_a
        -0x57f368c9 -> :sswitch_6
        -0x515a21d4 -> :sswitch_1c
        -0x45d37bf9 -> :sswitch_11
        -0x3d4fc047 -> :sswitch_14
        -0x37255175 -> :sswitch_12
        -0x2c2c425f -> :sswitch_c
        -0x28274301 -> :sswitch_10
        -0x24a6e684 -> :sswitch_e
        -0x23b3592b -> :sswitch_16
        -0x164c702f -> :sswitch_15
        -0x12717657 -> :sswitch_1
        -0x103ed3c5 -> :sswitch_19
        -0x18e999e -> :sswitch_1a
        0xd1b -> :sswitch_0
        0x10f262b -> :sswitch_4
        0x1b82127 -> :sswitch_5
        0x68ac491 -> :sswitch_17
        0x1cd45f76 -> :sswitch_b
        0x4a695d10 -> :sswitch_2
        0x5582bc23 -> :sswitch_9
        0x57375960 -> :sswitch_8
        0x64411061 -> :sswitch_18
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultBannerImageUrl(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultBannerImageUrl"    # Ljava/lang/String;

    .prologue
    .line 155
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultBannerImageUrl:Ljava/lang/String;

    .line 156
    return-object p0
.end method

.method public setDefaultClubPresence(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 207
    .local p1, "defaultClubPresence":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultClubPresence:Lcom/google/common/collect/ImmutableList;

    .line 208
    return-object p0
.end method

.method public setDefaultClubPresenceCount(J)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultClubPresenceCount"    # J

    .prologue
    .line 183
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultClubPresenceCount:J

    .line 184
    return-object p0
.end method

.method public setDefaultClubPresenceInGameCount(J)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultClubPresenceInGameCount"    # J

    .prologue
    .line 191
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultClubPresenceInGameCount:J

    .line 192
    return-object p0
.end method

.method public setDefaultClubPresenceTodayCount(J)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultClubPresenceTodayCount"    # J

    .prologue
    .line 187
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultClubPresenceTodayCount:J

    .line 188
    return-object p0
.end method

.method public setDefaultClubType(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultClubType"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultClubType:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    .line 132
    return-object p0
.end method

.method public setDefaultCreationDateUtc(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultCreationDateUtc"    # Ljava/lang/String;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultCreationDateUtc:Ljava/lang/String;

    .line 148
    return-object p0
.end method

.method public setDefaultFollowersCount(J)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultFollowersCount"    # J

    .prologue
    .line 163
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultFollowersCount:J

    .line 164
    return-object p0
.end method

.method public setDefaultFounderXuid(J)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultFounderXuid"    # J

    .prologue
    .line 143
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultFounderXuid:J

    .line 144
    return-object p0
.end method

.method public setDefaultGlyphImageUrl(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultGlyphImageUrl"    # Ljava/lang/String;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultGlyphImageUrl:Ljava/lang/String;

    .line 152
    return-object p0
.end method

.method public setDefaultId(J)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultId"    # J

    .prologue
    .line 123
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultId:J

    .line 124
    return-object p0
.end method

.method public setDefaultMaxMembersInGame(J)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultMaxMembersInGame"    # J

    .prologue
    .line 235
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultMaxMembersInGame:J

    .line 236
    return-object p0
.end method

.method public setDefaultMaxMembersPerClub(J)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultMaxMembersPerClub"    # J

    .prologue
    .line 231
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultMaxMembersPerClub:J

    .line 232
    return-object p0
.end method

.method public setDefaultMembersCount(J)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultMembersCount"    # J

    .prologue
    .line 167
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultMembersCount:J

    .line 168
    return-object p0
.end method

.method public setDefaultModeratorsCount(J)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultModeratorsCount"    # J

    .prologue
    .line 171
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultModeratorsCount:J

    .line 172
    return-object p0
.end method

.method public setDefaultOwnerXuid(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultOwnerXuid"    # Ljava/lang/String;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultOwnerXuid:Ljava/lang/String;

    .line 140
    return-object p0
.end method

.method public setDefaultProfile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultProfile"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultProfile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    .line 128
    return-object p0
.end method

.method public setDefaultRecommendation(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultRecommendation"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    .prologue
    .line 203
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultRecommendation:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    .line 204
    return-object p0
.end method

.method public setDefaultRecommendedCount(J)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultRecommendedCount"    # J

    .prologue
    .line 175
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultRecommendedCount:J

    .line 176
    return-object p0
.end method

.method public setDefaultReportCount(J)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultReportCount"    # J

    .prologue
    .line 223
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultReportCount:J

    .line 224
    return-object p0
.end method

.method public setDefaultReportedItemsCount(J)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultReportedItemsCount"    # J

    .prologue
    .line 227
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultReportedItemsCount:J

    .line 228
    return-object p0
.end method

.method public setDefaultRequestedToJoinCount(J)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultRequestedToJoinCount"    # J

    .prologue
    .line 179
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultRequestedToJoinCount:J

    .line 180
    return-object p0
.end method

.method public setDefaultRoster(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultRoster"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    .prologue
    .line 195
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultRoster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    .line 196
    return-object p0
.end method

.method public setDefaultSettings(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultSettings"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    .prologue
    .line 159
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultSettings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    .line 160
    return-object p0
.end method

.method public setDefaultShortName(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultShortName"    # Ljava/lang/String;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultShortName:Ljava/lang/String;

    .line 136
    return-object p0
.end method

.method public setDefaultState(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultState"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    .prologue
    .line 215
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultState:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    .line 216
    return-object p0
.end method

.method public setDefaultSuspendedUntilUtc(Ljava/util/Date;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultSuspendedUntilUtc"    # Ljava/util/Date;

    .prologue
    .line 219
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultSuspendedUntilUtc:Ljava/util/Date;

    .line 220
    return-object p0
.end method

.method public setDefaultTargetRoles(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTargetRoles"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    .prologue
    .line 199
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultTargetRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    .line 200
    return-object p0
.end method

.method public setDefaultTitleDeeplinks(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTitleDeeplinks"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->defaultTitleDeeplinks:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    .line 212
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V
    .locals 4
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 240
    if-nez p2, :cond_0

    .line 241
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 304
    :goto_0
    return-void

    .line 244
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 245
    const-string v0, "id"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 246
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->idAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 247
    const-string v0, "profile"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 248
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->profileAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 249
    const-string v0, "clubType"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 250
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->clubTypeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubType()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 251
    const-string v0, "shortName"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 252
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->shortNameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->shortName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 253
    const-string v0, "ownerXuid"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 254
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->ownerXuidAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->ownerXuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 255
    const-string v0, "founderXuid"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 256
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->founderXuidAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->founderXuid()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 257
    const-string v0, "creationDateUtc"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 258
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->creationDateUtcAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->creationDateUtc()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 259
    const-string v0, "glyphImageUrl"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 260
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->glyphImageUrlAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->glyphImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 261
    const-string v0, "bannerImageUrl"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 262
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->bannerImageUrlAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->bannerImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 263
    const-string v0, "settings"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 264
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->settingsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 265
    const-string v0, "followersCount"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 266
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->followersCountAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->followersCount()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 267
    const-string v0, "membersCount"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 268
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->membersCountAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->membersCount()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 269
    const-string v0, "moderatorsCount"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 270
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->moderatorsCountAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->moderatorsCount()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 271
    const-string v0, "recommendedCount"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 272
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->recommendedCountAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->recommendedCount()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 273
    const-string v0, "requestedToJoinCount"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 274
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->requestedToJoinCountAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->requestedToJoinCount()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 275
    const-string v0, "clubPresenceCount"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 276
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->clubPresenceCountAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceCount()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 277
    const-string v0, "clubPresenceTodayCount"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 278
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->clubPresenceTodayCountAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceTodayCount()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 279
    const-string v0, "clubPresenceInGameCount"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 280
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->clubPresenceInGameCountAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceInGameCount()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 281
    const-string v0, "roster"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 282
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->rosterAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 283
    const-string v0, "targetRoles"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 284
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->targetRolesAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->targetRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 285
    const-string v0, "recommendation"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 286
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->recommendationAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->recommendation()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 287
    const-string v0, "clubPresence"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 288
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->clubPresenceAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresence()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 289
    const-string v0, "titleDeeplinks"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 290
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->titleDeeplinksAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->titleDeeplinks()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 291
    const-string v0, "state"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 292
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->stateAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->state()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 293
    const-string v0, "suspendedUntilUtc"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 294
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->suspendedUntilUtcAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->suspendedUntilUtc()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 295
    const-string v0, "reportCount"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 296
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->reportCountAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->reportCount()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 297
    const-string v0, "reportedItemsCount"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 298
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->reportedItemsCountAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->reportedItemsCount()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 299
    const-string v0, "maxMembersPerClub"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 300
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->maxMembersPerClubAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->maxMembersPerClub()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 301
    const-string v0, "maxMembersInGame"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 302
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->maxMembersInGameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->maxMembersInGame()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 303
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto/16 :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32
    check-cast p2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Club$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    return-void
.end method
