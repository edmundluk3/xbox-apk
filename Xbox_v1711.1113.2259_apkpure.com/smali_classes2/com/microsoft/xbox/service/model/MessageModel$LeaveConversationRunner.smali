.class Lcom/microsoft/xbox/service/model/MessageModel$LeaveConversationRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "MessageModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/MessageModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LeaveConversationRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final caller:Lcom/microsoft/xbox/service/model/MessageModel;

.field private final conversationId:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/MessageModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;)V
    .locals 0
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .param p3, "conversationId"    # Ljava/lang/String;

    .prologue
    .line 2227
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/MessageModel$LeaveConversationRunner;->this$0:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 2228
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/MessageModel$LeaveConversationRunner;->caller:Lcom/microsoft/xbox/service/model/MessageModel;

    .line 2229
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/MessageModel$LeaveConversationRunner;->conversationId:Ljava/lang/String;

    .line 2230
    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2221
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MessageModel$LeaveConversationRunner;->buildData()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/lang/Void;
    .locals 7
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2235
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    .line 2236
    .local v0, "meXuid":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2237
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->access$500()Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/MessageModel$LeaveConversationRunner;->conversationId:Ljava/lang/String;

    const-string v5, "8:xbox:%s"

    new-array v6, v1, [Ljava/lang/Object;

    aput-object v0, v6, v2

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;->leaveGroupConversation(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2238
    .local v1, "result":Z
    :goto_0
    if-nez v1, :cond_1

    .line 2239
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->access$900()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Failed to leave conversation"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2240
    new-instance v2, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v4, 0x2649

    invoke-direct {v2, v4, v5}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v2

    .end local v1    # "result":Z
    :cond_0
    move v1, v2

    .line 2237
    goto :goto_0

    .line 2243
    .restart local v1    # "result":Z
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->access$500()Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MessageModel$LeaveConversationRunner;->conversationId:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;->deleteGroup(Ljava/lang/String;)Z

    move-result v1

    .line 2244
    if-nez v1, :cond_2

    .line 2245
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->access$900()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Failed to delete conversation"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 2246
    new-instance v2, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v4, 0x264c

    invoke-direct {v2, v4, v5}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v2

    .line 2249
    :cond_2
    const/4 v2, 0x0

    return-object v2
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 2263
    const-wide/16 v0, 0x2649

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2258
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Void;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel$LeaveConversationRunner;->caller:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel$LeaveConversationRunner;->conversationId:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->access$2000(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V

    .line 2259
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 2254
    return-void
.end method
