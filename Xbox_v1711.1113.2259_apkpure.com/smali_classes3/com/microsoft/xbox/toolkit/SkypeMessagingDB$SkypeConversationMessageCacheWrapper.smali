.class Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeConversationMessageCacheWrapper;
.super Ljava/lang/Object;
.source "SkypeMessagingDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SkypeConversationMessageCacheWrapper"
.end annotation


# instance fields
.field private final skypeConversationMessagesCache:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;Ljava/util/concurrent/ConcurrentHashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 301
    .local p2, "skypeConversationMessagesCache":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeConversationMessageCacheWrapper;->this$0:Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 302
    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeConversationMessageCacheWrapper;->skypeConversationMessagesCache:Ljava/util/concurrent/ConcurrentHashMap;

    .line 303
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;Ljava/util/concurrent/ConcurrentHashMap;Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;
    .param p2, "x1"    # Ljava/util/concurrent/ConcurrentHashMap;
    .param p3, "x2"    # Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$1;

    .prologue
    .line 298
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeConversationMessageCacheWrapper;-><init>(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;Ljava/util/concurrent/ConcurrentHashMap;)V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeConversationMessageCacheWrapper;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeConversationMessageCacheWrapper;

    .prologue
    .line 298
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeConversationMessageCacheWrapper;->skypeConversationMessagesCache:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method
