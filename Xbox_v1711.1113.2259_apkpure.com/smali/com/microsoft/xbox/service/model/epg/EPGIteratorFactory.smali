.class public Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;
.super Ljava/lang/Object;
.source "EPGIteratorFactory.java"

# interfaces
.implements Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;,
        Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;,
        Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;,
        Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;
    }
.end annotation


# static fields
.field private static final ChannelBlock:I = 0x32

.field private static final ListServiceFetchCacheTimeInMilliseconds:I = 0x493e0

.field private static final TAG:Ljava/lang/String; = "EPGIteratorFactory"

.field private static final TimeBlock:I = 0x7080


# instance fields
.field private channels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

.field private favChannels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

.field private final fetchCache:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;

.field private fetchLast:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

.field private fetchPendingRequest:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

.field private fetchQueued:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

.field private hadAuthError:Z

.field private isError:Z

.field private isFetching:Z

.field private isFetchingFavorites:Z

.field private lastFavoritesLoad:Ljava/util/Date;

.field private final listeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;",
            ">;"
        }
    .end annotation
.end field

.field private loadDataTask:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;

.field private final mHeadend:Ljava/lang/String;

.field private final mLogPrefix:Ljava/lang/String;

.field private final mProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)V
    .locals 3
    .param p1, "provider"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->listeners:Ljava/util/ArrayList;

    .line 48
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->isFetching:Z

    .line 49
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->isFetchingFavorites:Z

    .line 50
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->isError:Z

    .line 51
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->hadAuthError:Z

    .line 52
    iput-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->loadDataTask:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;

    .line 54
    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchCache:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;

    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchCache:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->createInitialState()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchLast:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    .line 56
    iput-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchPendingRequest:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    .line 57
    iput-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchQueued:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    .line 59
    iput-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->channels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    .line 60
    iput-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->favChannels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    .line 61
    iput-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->lastFavoritesLoad:Ljava/util/Date;

    .line 64
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mHeadend:Ljava/lang/String;

    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getProviderName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mLogPrefix:Ljava/lang/String;

    .line 67
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->onFetchCompleteUIThread(Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;)V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->onSetFavoriteErrorUIThread(Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;)V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->notifyListenersState()V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;)Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->getFetchPending()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;)Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->loadDataTask:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mLogPrefix:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    return-object v0
.end method

.method private cancelFetchTask()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 242
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->loadDataTask:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->loadDataTask:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->cancel()V

    .line 244
    iput-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->loadDataTask:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;

    .line 246
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->setFetchPending(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;Z)Z

    .line 247
    return-void
.end method

.method public static final formatTime(I)Ljava/lang/String;
    .locals 6
    .param p0, "timeInSeconds"    # I

    .prologue
    .line 420
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 421
    .local v0, "t":Landroid/text/format/Time;
    int-to-long v2, p0

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 422
    invoke-virtual {v0}, Landroid/text/format/Time;->format2445()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private final declared-synchronized getFetchPending()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;
    .locals 1

    .prologue
    .line 258
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchPendingRequest:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private notifyListenersData()V
    .locals 6

    .prologue
    .line 404
    const/4 v2, 0x0

    .line 405
    .local v2, "startChannel":I
    const v0, 0x7fffffff

    .line 408
    .local v0, "endChannel":I
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchPendingRequest:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    if-eqz v3, :cond_0

    .line 409
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchPendingRequest:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    iget v2, v3, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;->channel:I

    .line 410
    add-int/lit8 v0, v2, 0x32

    .line 413
    :cond_0
    const-string v3, "EPGIteratorFactory"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mLogPrefix:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "notifyListenersData ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;

    .line 415
    .local v1, "l":Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mHeadend:Ljava/lang/String;

    invoke-interface {v1, v4, v2, v0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;->onDataChanged(Ljava/lang/String;II)V

    goto :goto_0

    .line 417
    .end local v1    # "l":Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;
    :cond_1
    return-void
.end method

.method private notifyListenersState()V
    .locals 4

    .prologue
    .line 397
    const-string v1, "EPGIteratorFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mLogPrefix:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "notifyListenersState"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;

    .line 399
    .local v0, "l":Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mHeadend:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;->onFetchingStatusChanged(Ljava/lang/String;)V

    goto :goto_0

    .line 401
    .end local v0    # "l":Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;
    :cond_0
    return-void
.end method

.method private onFetchCompleteUIThread(Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;)V
    .locals 6
    .param p1, "result"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 297
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 299
    const-string v0, "EPGIteratorFactory"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mLogPrefix:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Fetch completed in UI "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    iput-boolean v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->isFetching:Z

    .line 303
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->resultNoError:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    if-eq p1, v0, :cond_3

    .line 304
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->cancelFetchTask()V

    .line 305
    iput-boolean v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->isError:Z

    .line 308
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorNetwork:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    if-ne p1, v0, :cond_1

    .line 309
    iput-object v4, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchQueued:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    .line 340
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchQueued:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchQueued:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    invoke-direct {p0, v0, v5}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->setFetchPending(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 341
    iput-object v4, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchQueued:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    .line 342
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->startFetchTask(Z)V

    .line 348
    :goto_1
    return-void

    .line 310
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorAuthentication:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    if-ne p1, v0, :cond_0

    .line 311
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->clearAllTokens()V

    .line 313
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->hadAuthError:Z

    if-eqz v0, :cond_2

    .line 316
    iput-object v4, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchQueued:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    .line 317
    iput-boolean v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->hadAuthError:Z

    goto :goto_0

    .line 322
    :cond_2
    iput-boolean v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->hadAuthError:Z

    goto :goto_0

    .line 326
    :cond_3
    iput-boolean v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->hadAuthError:Z

    .line 327
    iput-boolean v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->isError:Z

    .line 329
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->notifyListenersData()V

    .line 331
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->getFetchPending()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 332
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->getFetchPending()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchLast:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    .line 333
    invoke-direct {p0, v4, v3}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->setFetchPending(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;Z)Z

    .line 335
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchCache:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchLast:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    iget v1, v1, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;->time:I

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchLast:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    iget v2, v2, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;->channel:I

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->add(II)V

    goto :goto_0

    .line 346
    :cond_4
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->notifyListenersState()V

    goto :goto_1
.end method

.method private onSetFavoriteErrorUIThread(Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;)V
    .locals 3
    .param p1, "result"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    .prologue
    .line 391
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;

    .line 392
    .local v0, "l":Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mHeadend:Ljava/lang/String;

    invoke-interface {v0, v2, p1}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;->onFavoritesError(Ljava/lang/String;Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;)V

    goto :goto_0

    .line 394
    .end local v0    # "l":Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;
    :cond_0
    return-void
.end method

.method private final declared-synchronized setFetchPending(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;Z)Z
    .locals 1
    .param p1, "state"    # Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;
    .param p2, "onlyWhenNull"    # Z

    .prologue
    .line 250
    monitor-enter p0

    if-eqz p2, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchPendingRequest:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 251
    const/4 v0, 0x0

    .line 254
    :goto_0
    monitor-exit p0

    return v0

    .line 253
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchPendingRequest:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 254
    const/4 v0, 0x1

    goto :goto_0

    .line 250
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private startFetchTask(Z)V
    .locals 3
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 232
    const-string v0, "EPGIteratorFactory"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mLogPrefix:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Start fetch task: f:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->isFetching:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ft:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->loadDataTask:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->isFetching:Z

    .line 235
    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;ZZ)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->loadDataTask:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;

    .line 236
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->loadDataTask:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->execute()V

    .line 238
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->notifyListenersState()V

    .line 239
    return-void
.end method

.method private updateChannelList()V
    .locals 7

    .prologue
    .line 168
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->getChannels()[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    move-result-object v1

    .line 169
    .local v1, "currentChannels":[Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    const/4 v0, 0x0

    .line 170
    .local v0, "channelsFromModel":[Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    sget-object v5, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$4;->$SwitchMap$com$microsoft$xbox$xle$urc$net$HeadendInfo$FilterPreference:[I

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getFilter()Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 182
    :goto_0
    if-nez v1, :cond_1

    .line 183
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->setChannels([Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V

    .line 205
    :cond_0
    :goto_1
    return-void

    .line 172
    :pswitch_0
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHdOnlyChannelList()[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    move-result-object v0

    .line 173
    goto :goto_0

    .line 175
    :pswitch_1
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHdAndUnmatchedSdChannelList()[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    move-result-object v0

    .line 176
    goto :goto_0

    .line 178
    :pswitch_2
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getChannelList()[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    move-result-object v0

    goto :goto_0

    .line 185
    :cond_1
    if-eqz v0, :cond_0

    .line 186
    array-length v5, v0

    array-length v6, v1

    if-eq v5, v6, :cond_2

    .line 187
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->setChannels([Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V

    goto :goto_1

    .line 189
    :cond_2
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    array-length v5, v0

    if-ge v2, v5, :cond_0

    .line 190
    aget-object v3, v0, v2

    .line 191
    .local v3, "newChannel":Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    aget-object v4, v1, v2

    .line 193
    .local v4, "oldChannel":Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    if-eqz v4, :cond_3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelOrdinal()I

    move-result v5

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelOrdinal()I

    move-result v6

    if-eq v5, v6, :cond_4

    .line 194
    :cond_3
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->setChannels([Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V

    goto :goto_1

    .line 198
    :cond_4
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->isFavorite()Z

    move-result v5

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->isFavorite()Z

    move-result v6

    if-eq v5, v6, :cond_5

    .line 199
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->isFavorite()Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->copyFavorite(Z)V

    .line 189
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 170
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private updateFavoritesList()V
    .locals 6

    .prologue
    .line 208
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->getFavoriteChannels()[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    move-result-object v1

    .line 209
    .local v1, "currentFavorites":[Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getFavoriteChannelList()[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    move-result-object v2

    .line 210
    .local v2, "favoritesFromModel":[Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    if-nez v1, :cond_1

    .line 211
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->setFavoriteChannels([Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 213
    :cond_1
    const/4 v0, 0x0

    .line 214
    .local v0, "channel":Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    if-eqz v2, :cond_0

    .line 215
    array-length v4, v2

    array-length v5, v1

    if-eq v4, v5, :cond_2

    .line 216
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->setFavoriteChannels([Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V

    goto :goto_0

    .line 218
    :cond_2
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    array-length v4, v2

    if-ge v3, v4, :cond_0

    .line 219
    aget-object v0, v1, v3

    .line 221
    if-eqz v0, :cond_3

    aget-object v4, v2, v3

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelOrdinal()I

    move-result v4

    aget-object v5, v1, v3

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelOrdinal()I

    move-result v5

    if-eq v4, v5, :cond_4

    .line 222
    :cond_3
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->setFavoriteChannels([Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V

    goto :goto_0

    .line 218
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method


# virtual methods
.method public declared-synchronized ResetFavoritesState()V
    .locals 1

    .prologue
    .line 88
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->lastFavoritesLoad:Ljava/util/Date;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    monitor-exit p0

    return-void

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized getChannels()[Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    .locals 1

    .prologue
    .line 75
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->channels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getEPGIterator(IIZ)Lcom/microsoft/xbox/service/model/epg/EPGIterator;
    .locals 7
    .param p1, "channelOrdinal"    # I
    .param p2, "startTime"    # I
    .param p3, "forward"    # Z

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 131
    const-string v3, "EPGIteratorFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mLogPrefix:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "Creating iterator (Ch Ord "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", Time "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz p3, :cond_1

    const-string v2, ", Fwd"

    :goto_0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mHeadend:Ljava/lang/String;

    invoke-static {v2, p1, p2, p3}, Lcom/microsoft/xbox/service/model/epg/EPGIterator;->createIteratorAt(Ljava/lang/String;IIZ)Lcom/microsoft/xbox/service/model/epg/EPGIterator;

    move-result-object v0

    .line 135
    .local v0, "iterator":Lcom/microsoft/xbox/service/model/epg/EPGIterator;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchLast:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    if-nez v2, :cond_2

    .line 136
    const-string v2, "EPGIteratorFactory"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mLogPrefix:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "EPG state does not exists - request"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchCache:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;

    invoke-virtual {v2, p2, p1}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->createFetchState(II)Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    move-result-object v2

    invoke-direct {p0, v2, v6}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->setFetchPending(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 138
    invoke-direct {p0, v5}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->startFetchTask(Z)V

    .line 156
    :cond_0
    :goto_1
    return-object v0

    .line 131
    .end local v0    # "iterator":Lcom/microsoft/xbox/service/model/epg/EPGIterator;
    :cond_1
    const-string v2, ", Back"

    goto :goto_0

    .line 141
    .restart local v0    # "iterator":Lcom/microsoft/xbox/service/model/epg/EPGIterator;
    :cond_2
    const/4 v1, 0x0

    .line 142
    .local v1, "newRequest":Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchCache:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;

    invoke-virtual {v2, p2, p1}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->check(II)Z

    move-result v2

    if-nez v2, :cond_4

    .line 143
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchCache:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;

    invoke-virtual {v2, p2, p1}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->createFetchState(II)Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    move-result-object v1

    .line 148
    :cond_3
    :goto_2
    if-eqz v1, :cond_0

    .line 149
    invoke-direct {p0, v1, v6}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->setFetchPending(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;Z)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 150
    invoke-direct {p0, v5}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->startFetchTask(Z)V

    goto :goto_1

    .line 144
    :cond_4
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchCache:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;

    add-int/lit16 v3, p2, 0x2a30

    invoke-virtual {v2, v3, p1}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->check(II)Z

    move-result v2

    if-nez v2, :cond_3

    .line 145
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchCache:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;

    add-int/lit16 v3, p2, 0x2a30

    invoke-virtual {v2, v3, p1}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->createFetchState(II)Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    move-result-object v1

    goto :goto_2

    .line 151
    :cond_5
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchQueued:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchQueued:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 152
    :cond_6
    iput-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchQueued:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    goto :goto_1
.end method

.method public final declared-synchronized getFavoriteChannels()[Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    .locals 1

    .prologue
    .line 84
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->favChannels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isError()Z
    .locals 1

    .prologue
    .line 164
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->isError:Z

    return v0
.end method

.method public isFetching()Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->isFetching:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->isFetchingFavorites:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onFetchComplete(Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;)V
    .locals 7
    .param p1, "result"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .prologue
    const/4 v6, 0x1

    .line 264
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->resultNoError:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    if-ne p1, v0, :cond_1

    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->isFetchingFavorites:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->lastFavoritesLoad:Ljava/util/Date;

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->lastFavoritesLoad:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const-wide/32 v4, 0x493e0

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 265
    :cond_0
    const-string v0, "EPGIteratorFactory"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mLogPrefix:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Fetching favorites"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    iput-boolean v6, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->isFetchingFavorites:Z

    .line 267
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v0, v1, :cond_2

    .line 268
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->fetchFromListServiceIfNeededAsync()Z

    .line 277
    :cond_1
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v0, v1, :cond_3

    .line 278
    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$1;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$1;-><init>(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 287
    :goto_1
    return-void

    .line 271
    :cond_2
    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v6, v1}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;ZZ)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->loadDataTask:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;

    .line 272
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->loadDataTask:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->execute()V

    goto :goto_0

    .line 285
    :cond_3
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->onFetchCompleteUIThread(Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;)V

    goto :goto_1
.end method

.method public onFetchListServiceComplete(Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    .prologue
    .line 371
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->isFetchingFavorites:Z

    .line 372
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->resultNoError:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    if-ne p1, v0, :cond_0

    .line 373
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->lastFavoritesLoad:Ljava/util/Date;

    .line 374
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->updateChannelList()V

    .line 375
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->updateFavoritesList()V

    .line 378
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v0, v1, :cond_1

    .line 379
    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$3;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$3;-><init>(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 388
    :goto_0
    return-void

    .line 386
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->notifyListenersState()V

    goto :goto_0
.end method

.method public onFetchProgress(I)V
    .locals 0
    .param p1, "percent"    # I

    .prologue
    .line 352
    return-void
.end method

.method public onSendComplete(Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;Z)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;
    .param p2, "attemptedFavoriteValue"    # Z

    .prologue
    .line 291
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorNetwork:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    if-ne p1, v0, :cond_0

    .line 292
    if-eqz p2, :cond_1

    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;->errorCannotSet:Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->onSetFavoriteError(Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;)V

    .line 294
    :cond_0
    return-void

    .line 292
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;->errorCannotRemove:Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    goto :goto_0
.end method

.method public onSetFavoriteError(Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    .prologue
    .line 357
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v0, v1, :cond_0

    .line 358
    new-instance v0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$2;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$2;-><init>(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 367
    :goto_0
    return-void

    .line 365
    :cond_0
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->onSetFavoriteErrorUIThread(Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;)V

    goto :goto_0
.end method

.method public refresh()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 108
    const-string v0, "EPGIteratorFactory"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mLogPrefix:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Refresh: e:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->isError:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", f:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->isFetching:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ft:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->loadDataTask:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->cancelFetchTask()V

    .line 111
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->isError:Z

    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchCache:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->refresh()V

    .line 117
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchLast:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchLast:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    iget v0, v0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;->time:I

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchCache:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->access$000(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;)I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchCache:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$EPGFetchCache;->createInitialState()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchLast:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    .line 121
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->updateChannelList()V

    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->fetchLast:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;->clone()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    move-result-object v0

    invoke-direct {p0, v0, v3}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->setFetchPending(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 124
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->startFetchTask(Z)V

    .line 128
    :goto_0
    return-void

    .line 126
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->resultNoError:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->onFetchComplete(Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;)V

    goto :goto_0
.end method

.method public final declared-synchronized setChannels([Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V
    .locals 3
    .param p1, "_channels"    # [Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    .prologue
    .line 70
    monitor-enter p0

    :try_start_0
    const-string v1, "EPGIteratorFactory"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mLogPrefix:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "Got "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p1, :cond_0

    const-string v0, "null"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " channels"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->channels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    monitor-exit p0

    return-void

    .line 70
    :cond_0
    :try_start_1
    array-length v0, p1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized setFavoriteChannels([Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V
    .locals 3
    .param p1, "_channels"    # [Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    .prologue
    .line 79
    monitor-enter p0

    :try_start_0
    const-string v1, "EPGIteratorFactory"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mLogPrefix:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "Got "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p1, :cond_0

    const-string v0, "null"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " favorite channels"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->favChannels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    monitor-exit p0

    return-void

    .line 79
    :cond_0
    :try_start_1
    array-length v0, p1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public start(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->addListener(Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;)V

    .line 94
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->refresh()V

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    return-void
.end method

.method public stop(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$IEPGFactoryListener;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->mProvider:Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->removeListener(Lcom/microsoft/xbox/service/model/epg/EPGProvider$IFetchListener;)V

    .line 103
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->cancelFetchTask()V

    .line 105
    :cond_0
    return-void
.end method
