.class public Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;
.super Ljava/lang/Object;
.source "RatingStringsHelper.java"


# static fields
.field private static final UnratedRating:Ljava/lang/String; = "Unrated"

.field private static final UnratedRatingResId:I = 0x7f070a10

.field private static final liveRatingDescriptor:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final liveRatingLevel:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const v7, 0x7f07096d

    const v6, 0x7f070a11

    const v5, 0x7f0709de

    const v4, 0x7f0709dd

    const v3, 0x7f0709db

    .line 18
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    .line 21
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "0"

    const v2, 0x7f07095c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "10"

    const v2, 0x7f070959

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "20"

    const v2, 0x7f07095a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "25"

    const v2, 0x7f07095b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "30"

    const v2, 0x7f07095f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "40"

    const v2, 0x7f07095d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "50"

    const v2, 0x7f070958

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "60"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "100"

    const v2, 0x7f0708c6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "110"

    const v2, 0x7f0708c7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "120"

    const v2, 0x7f0708c8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "130"

    const v2, 0x7f0708c9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "140"

    const v2, 0x7f0708ca

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "150"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "4001"

    const v2, 0x7f0709df

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "4002"

    const v2, 0x7f0709e2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "4003"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "4004"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "4005"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "5001"

    const v2, 0x7f0709e0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "5002"

    const v2, 0x7f0709e1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "5003"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "5004"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "5005"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "6001"

    const v2, 0x7f0709d6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "6002"

    const v2, 0x7f0709d9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "6003"

    const v2, 0x7f0709d7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "6004"

    const v2, 0x7f0709d8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "6005"

    const v2, 0x7f0709d2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "6006"

    const v2, 0x7f0709d3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "6007"

    const v2, 0x7f0709d4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "6008"

    const v2, 0x7f0709d5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "7001"

    const v2, 0x7f070a07

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "7002"

    const v2, 0x7f070a06

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "7003"

    const v2, 0x7f070a04

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "7004"

    const v2, 0x7f070a05

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "7005"

    const v2, 0x7f070a08

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "7006"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "8001"

    const v2, 0x7f0709c1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "8002"

    const v2, 0x7f0709c2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "8003"

    const v2, 0x7f0709c4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "8004"

    const v2, 0x7f0709c5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "8005"

    const v2, 0x7f0709c8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "8006"

    const v2, 0x7f0709c0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "8007"

    const v2, 0x7f0709c1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "8008"

    const v2, 0x7f0709c6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "8009"

    const v2, 0x7f0709c3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "8010"

    const v2, 0x7f0709c5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "8011"

    const v2, 0x7f0709c7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "8012"

    const v2, 0x7f0709c9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "9001"

    const v2, 0x7f0709ca

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "9002"

    const v2, 0x7f0709cc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "9003"

    const v2, 0x7f0709cb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "9004"

    const v2, 0x7f0709cf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "9005"

    const v2, 0x7f0709cd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "9006"

    const v2, 0x7f0709ce

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "9007"

    const v2, 0x7f0709d0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "9008"

    const v2, 0x7f0709d1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "11001"

    const v2, 0x7f070999

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "11002"

    const v2, 0x7f070996

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "11003"

    const v2, 0x7f070997

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "11004"

    const v2, 0x7f070998

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "13001"

    const v2, 0x7f07096c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "13002"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "13003"

    const v2, 0x7f070968

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "13004"

    const v2, 0x7f070969

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "13005"

    const v2, 0x7f07096a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "13006"

    const v2, 0x7f07096b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "13007"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "12006"

    const v2, 0x7f0708b7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "12001"

    const v2, 0x7f0708b8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "12007"

    const v2, 0x7f0708b2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "12002"

    const v2, 0x7f0708b3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "12003"

    const v2, 0x7f0708b4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "12004"

    const v2, 0x7f0708b5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "12005"

    const v2, 0x7f0708b6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "14001"

    const v2, 0x7f0709df

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "14002"

    const v2, 0x7f0709e2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "14003"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "14004"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "14005"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "15001"

    const v2, 0x7f070981

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "15002"

    const v2, 0x7f07097e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "15003"

    const v2, 0x7f07097f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "15004"

    const v2, 0x7f070980

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "17001"

    const v2, 0x7f0709f8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "17002"

    const v2, 0x7f0709fa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "17003"

    const v2, 0x7f0709f9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "17004"

    const v2, 0x7f0709fb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "18001"

    const v2, 0x7f0709e4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "18002"

    const v2, 0x7f0709e3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    const-string v1, "18003"

    const v2, 0x7f0709e5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    .line 118
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "0"

    const v2, 0x7f0708cf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "1"

    const v2, 0x7f0708d0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "2"

    const v2, 0x7f0708f7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "3"

    const v2, 0x7f070903

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "4"

    const v2, 0x7f070914

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "5"

    const v2, 0x7f070927

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "6"

    const v2, 0x7f070939

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "7"

    const v2, 0x7f07094b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "8"

    const v2, 0x7f070956

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "9"

    const v2, 0x7f070957

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "10"

    const v2, 0x7f0708d1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "11"

    const v2, 0x7f0708e6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "12"

    const v2, 0x7f0708e8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "13"

    const v2, 0x7f0708e9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "14"

    const v2, 0x7f0708ea

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "15"

    const v2, 0x7f0708f2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "16"

    const v2, 0x7f0708f3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "17"

    const v2, 0x7f0708f4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "18"

    const v2, 0x7f0708f5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "19"

    const v2, 0x7f0708f6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "20"

    const v2, 0x7f0708f8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "21"

    const v2, 0x7f0708f9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "22"

    const v2, 0x7f0708fa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "23"

    const v2, 0x7f0708fc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "24"

    const v2, 0x7f0708fd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "25"

    const v2, 0x7f0708fe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "26"

    const v2, 0x7f0708ff

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "27"

    const v2, 0x7f070900

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "28"

    const v2, 0x7f070901

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "29"

    const v2, 0x7f070902

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "30"

    const v2, 0x7f070904

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "31"

    const v2, 0x7f070905

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "32"

    const v2, 0x7f070906

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "33"

    const v2, 0x7f07090d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "34"

    const v2, 0x7f07090e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "35"

    const v2, 0x7f07090f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "36"

    const v2, 0x7f070910

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "37"

    const v2, 0x7f070911

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "38"

    const v2, 0x7f070912

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "39"

    const v2, 0x7f070913

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "40"

    const v2, 0x7f070915

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "41"

    const v2, 0x7f07091d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "42"

    const v2, 0x7f07091e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "43"

    const v2, 0x7f07091f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "44"

    const v2, 0x7f070920

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "45"

    const v2, 0x7f070921

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "46"

    const v2, 0x7f070922

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "47"

    const v2, 0x7f070923

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "48"

    const v2, 0x7f070924

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "49"

    const v2, 0x7f070925

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "50"

    const v2, 0x7f070928

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "51"

    const v2, 0x7f070930

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "52"

    const v2, 0x7f070931

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "53"

    const v2, 0x7f070932

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "54"

    const v2, 0x7f070933

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "55"

    const v2, 0x7f070934

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "56"

    const v2, 0x7f070935

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "57"

    const v2, 0x7f070936

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "58"

    const v2, 0x7f070937

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "59"

    const v2, 0x7f070938

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "60"

    const v2, 0x7f07093a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "61"

    const v2, 0x7f070942

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "62"

    const v2, 0x7f070943

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "63"

    const v2, 0x7f070944

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "64"

    const v2, 0x7f070945

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "65"

    const v2, 0x7f070946

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "66"

    const v2, 0x7f070947

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "67"

    const v2, 0x7f070948

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "68"

    const v2, 0x7f070949

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "69"

    const v2, 0x7f07094a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "70"

    const v2, 0x7f07094c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "71"

    const v2, 0x7f07094d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "72"

    const v2, 0x7f07094e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "73"

    const v2, 0x7f07094f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "74"

    const v2, 0x7f070950

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "75"

    const v2, 0x7f070951

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "76"

    const v2, 0x7f070952

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "77"

    const v2, 0x7f070953

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "78"

    const v2, 0x7f070954

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "79"

    const v2, 0x7f070955

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "100"

    const v2, 0x7f0708d2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "101"

    const v2, 0x7f0708dd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "102"

    const v2, 0x7f0708de

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "103"

    const v2, 0x7f0708df

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "104"

    const v2, 0x7f0708e0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "105"

    const v2, 0x7f0708e1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "106"

    const v2, 0x7f0708e2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "107"

    const v2, 0x7f0708e3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "108"

    const v2, 0x7f0708e4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "109"

    const v2, 0x7f0708e5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "110"

    const v2, 0x7f0708e7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "2283"

    const v2, 0x7f0708fb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "4000"

    const v2, 0x7f070916

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "4001"

    const v2, 0x7f070917

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "4002"

    const v2, 0x7f070918

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "4003"

    const v2, 0x7f070919

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "4004"

    const v2, 0x7f07091a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "4005"

    const v2, 0x7f07091b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "4006"

    const v2, 0x7f07091c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "4999"

    const v2, 0x7f070926

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "5000"

    const v2, 0x7f070929

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "5001"

    const v2, 0x7f07092a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "5002"

    const v2, 0x7f07092b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "5003"

    const v2, 0x7f07092c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "5004"

    const v2, 0x7f07092d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "5005"

    const v2, 0x7f07092e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "5006"

    const v2, 0x7f07092f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "6000"

    const v2, 0x7f07093b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "6001"

    const v2, 0x7f07093c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "6002"

    const v2, 0x7f07093d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "6003"

    const v2, 0x7f07093e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "6004"

    const v2, 0x7f07093f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "6005"

    const v2, 0x7f070940

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "6006"

    const v2, 0x7f070941

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "10001"

    const v2, 0x7f0708d3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "10002"

    const v2, 0x7f0708d4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "10003"

    const v2, 0x7f0708d5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "10004"

    const v2, 0x7f0708d6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "10005"

    const v2, 0x7f0708d7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "10006"

    const v2, 0x7f0708d8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "10007"

    const v2, 0x7f0708d9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "10008"

    const v2, 0x7f0708da

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "10009"

    const v2, 0x7f0708db

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "10010"

    const v2, 0x7f0708dc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "14000"

    const v2, 0x7f0708eb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "14001"

    const v2, 0x7f0708ec

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "14002"

    const v2, 0x7f0708ed

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "14003"

    const v2, 0x7f0708ee

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "14004"

    const v2, 0x7f0708ef

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "14005"

    const v2, 0x7f0708f0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "14006"

    const v2, 0x7f0708f1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "320010"

    const v2, 0x7f070907

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "320020"

    const v2, 0x7f070908

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "320030"

    const v2, 0x7f070909

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "320040"

    const v2, 0x7f07090a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "320050"

    const v2, 0x7f07090b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    const-string v1, "320060"

    const v2, 0x7f07090c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getGameRatingDescriptor(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "ratingDescriptorId"    # Ljava/lang/String;
    .param p1, "nonLocalizedDescriptor"    # Ljava/lang/String;

    .prologue
    .line 284
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 286
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingDescriptor:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getStringForLocale(ILjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 289
    .end local p1    # "nonLocalizedDescriptor":Ljava/lang/String;
    :cond_0
    return-object p1
.end method

.method public static getGameRatingString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "ratingLevel"    # Ljava/lang/String;
    .param p1, "defaultRatingString"    # Ljava/lang/String;

    .prologue
    .line 267
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 268
    const-string v0, "Unrated"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 269
    const v0, 0x7f070a10

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getStringForLocale(ILjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 273
    .end local p1    # "defaultRatingString":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 270
    .restart local p1    # "defaultRatingString":Ljava/lang/String;
    :cond_1
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RatingStringsHelper;->liveRatingLevel:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getStringForLocale(ILjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method
