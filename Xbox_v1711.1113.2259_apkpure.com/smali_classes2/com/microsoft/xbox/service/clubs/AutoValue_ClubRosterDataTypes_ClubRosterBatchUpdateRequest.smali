.class final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateRequest;
.super Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateRequest;
.source "AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateRequest.java"


# instance fields
.field private final method:Ljava/lang/String;

.field private final userIds:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/common/collect/ImmutableList;)V
    .locals 2
    .param p1, "method"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 15
    .local p2, "userIds":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateRequest;-><init>()V

    .line 16
    if-nez p1, :cond_0

    .line 17
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null method"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateRequest;->method:Ljava/lang/String;

    .line 20
    if-nez p2, :cond_1

    .line 21
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null userIds"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 23
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateRequest;->userIds:Lcom/google/common/collect/ImmutableList;

    .line 24
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 46
    if-ne p1, p0, :cond_1

    .line 54
    :cond_0
    :goto_0
    return v1

    .line 49
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateRequest;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 50
    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateRequest;

    .line 51
    .local v0, "that":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateRequest;
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateRequest;->method:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateRequest;->method()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateRequest;->userIds:Lcom/google/common/collect/ImmutableList;

    .line 52
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateRequest;->userIds()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateRequest;
    :cond_3
    move v1, v2

    .line 54
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 59
    const/4 v0, 0x1

    .line 60
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 61
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateRequest;->method:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 62
    mul-int/2addr v0, v2

    .line 63
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateRequest;->userIds:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 64
    return v0
.end method

.method public method()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateRequest;->method:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClubRosterBatchUpdateRequest{method="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateRequest;->method:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", userIds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateRequest;->userIds:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public userIds()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubRosterBatchUpdateRequest;->userIds:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method
