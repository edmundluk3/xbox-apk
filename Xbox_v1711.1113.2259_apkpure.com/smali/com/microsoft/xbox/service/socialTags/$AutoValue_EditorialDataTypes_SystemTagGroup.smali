.class abstract Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTagGroup;
.super Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;
.source "$AutoValue_EditorialDataTypes_SystemTagGroup.java"


# instance fields
.field private final groupName:Ljava/lang/String;

.field private final tags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1, "groupName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 17
    .local p2, "tags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTagGroup;->groupName:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTagGroup;->tags:Ljava/util/List;

    .line 20
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 46
    if-ne p1, p0, :cond_1

    .line 54
    :cond_0
    :goto_0
    return v1

    .line 49
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;

    if-eqz v3, :cond_5

    move-object v0, p1

    .line 50
    check-cast v0, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;

    .line 51
    .local v0, "that":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;
    iget-object v3, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTagGroup;->groupName:Ljava/lang/String;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;->groupName()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTagGroup;->tags:Ljava/util/List;

    if-nez v3, :cond_4

    .line 52
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;->tags()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 51
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTagGroup;->groupName:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;->groupName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 52
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTagGroup;->tags:Ljava/util/List;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;->tags()Ljava/util/List;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;
    :cond_5
    move v1, v2

    .line 54
    goto :goto_0
.end method

.method public groupName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "systemTagGroup.name"
    .end annotation

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTagGroup;->groupName:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const v3, 0xf4243

    const/4 v2, 0x0

    .line 59
    const/4 v0, 0x1

    .line 60
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 61
    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTagGroup;->groupName:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 62
    mul-int/2addr v0, v3

    .line 63
    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTagGroup;->tags:Ljava/util/List;

    if-nez v1, :cond_1

    :goto_1
    xor-int/2addr v0, v2

    .line 64
    return v0

    .line 61
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTagGroup;->groupName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 63
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTagGroup;->tags:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public tags()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "systemTagGroup.Tags"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTagGroup;->tags:Ljava/util/List;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SystemTagGroup{groupName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTagGroup;->groupName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_SystemTagGroup;->tags:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
