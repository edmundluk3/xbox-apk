.class Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$1;
.super Ljava/lang/Object;
.source "ClubAdminRequestsScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->sendAllInvites()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPartialRosterChangeSuccess(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 120
    .local p1, "successfulMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    .local p2, "failedMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    const v1, 0x7f070209

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;I)V

    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;)V

    .line 122
    return-void
.end method

.method public onRosterChangeFailure(Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112
    .local p1, "failedMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    const v1, 0x7f070b6d

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;I)V

    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;)V

    .line 114
    return-void
.end method

.method public onRosterChangeSuccess(Ljava/util/List;)V
    .locals 0
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 108
    .local p1, "successfulMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    return-void
.end method
