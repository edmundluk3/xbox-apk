.class final synthetic Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter$$Lambda$7;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;

.field private final arg$2:Lcom/microsoft/xbox/presentation/beam/BeamNavigator;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;Lcom/microsoft/xbox/presentation/beam/BeamNavigator;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter$$Lambda$7;->arg$1:Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;

    iput-object p2, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter$$Lambda$7;->arg$2:Lcom/microsoft/xbox/presentation/beam/BeamNavigator;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;Lcom/microsoft/xbox/presentation/beam/BeamNavigator;)Lio/reactivex/functions/Function;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter$$Lambda$7;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter$$Lambda$7;-><init>(Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;Lcom/microsoft/xbox/presentation/beam/BeamNavigator;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter$$Lambda$7;->arg$1:Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter$$Lambda$7;->arg$2:Lcom/microsoft/xbox/presentation/beam/BeamNavigator;

    check-cast p1, Lio/reactivex/Observable;

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/presentation/beam/TrendingBeamPresenter;->lambda$null$4(Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;Lcom/microsoft/xbox/presentation/beam/BeamNavigator;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;

    move-result-object v0

    return-object v0
.end method
