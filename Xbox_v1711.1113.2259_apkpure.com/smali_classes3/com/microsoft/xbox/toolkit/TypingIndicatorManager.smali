.class public Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;
.super Ljava/lang/Object;
.source "TypingIndicatorManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/TypingIndicatorManager$TypingIndicatorCallback;
    }
.end annotation


# static fields
.field private static final EXPIRATION_TIMER_MS:J = 0x7d0L

.field private static final TAG:Ljava/lang/String;

.field private static final TYPING_EXPIRATION_TIME_MS:J = 0x1388L


# instance fields
.field private final callback:Lcom/microsoft/xbox/toolkit/TypingIndicatorManager$TypingIndicatorCallback;

.field private final typingData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private typingExpirationFuture:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/TypingIndicatorManager$TypingIndicatorCallback;)V
    .locals 1
    .param p1, "callback"    # Lcom/microsoft/xbox/toolkit/TypingIndicatorManager$TypingIndicatorCallback;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 31
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->typingData:Ljava/util/Map;

    .line 32
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->callback:Lcom/microsoft/xbox/toolkit/TypingIndicatorManager$TypingIndicatorCallback;

    .line 33
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->expireTypingIndicator()V

    return-void
.end method

.method private expireTypingIndicator()V
    .locals 12

    .prologue
    .line 63
    const/4 v3, 0x0

    .line 64
    .local v3, "isUpdated":Z
    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->typingData:Ljava/util/Map;

    monitor-enter v6

    .line 65
    :try_start_0
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 66
    .local v0, "currentTime":J
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->typingData:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 67
    .local v4, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/Date;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 68
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 69
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/Date;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Date;

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    sub-long v8, v0, v8

    const-wide/16 v10, 0x1388

    cmp-long v5, v8, v10

    if-ltz v5, :cond_0

    .line 70
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    .line 71
    const/4 v3, 0x1

    goto :goto_0

    .line 74
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/Date;>;"
    :cond_1
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    if-eqz v3, :cond_2

    .line 77
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->callback:Lcom/microsoft/xbox/toolkit/TypingIndicatorManager$TypingIndicatorCallback;

    invoke-interface {v5}, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager$TypingIndicatorCallback;->onTypingIndicatorUpdate()V

    .line 79
    :cond_2
    return-void

    .line 74
    .end local v0    # "currentTime":J
    .end local v4    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/Date;>;>;"
    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 105
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->typingData:Ljava/util/Map;

    monitor-enter v1

    .line 106
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->typingData:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 107
    monitor-exit v1

    .line 108
    return-void

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clearTypingIndicator(Ljava/lang/String;)V
    .locals 3
    .param p1, "gamerTag"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 50
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 53
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->typingData:Ljava/util/Map;

    monitor-enter v2

    .line 54
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->typingData:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 55
    .local v0, "isUpdated":Z
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    if-eqz v0, :cond_0

    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->callback:Lcom/microsoft/xbox/toolkit/TypingIndicatorManager$TypingIndicatorCallback;

    invoke-interface {v1}, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager$TypingIndicatorCallback;->onTypingIndicatorUpdate()V

    .line 60
    :cond_0
    return-void

    .line 54
    .end local v0    # "isUpdated":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 55
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public getTypingIndicatorList()Ljava/util/List;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->typingData:Ljava/util/Map;

    monitor-enter v1

    .line 100
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->typingData:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit v1

    return-object v0

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public startTypingExpirationTask()V
    .locals 7

    .prologue
    const-wide/16 v2, 0x7d0

    .line 82
    sget-object v0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->TAG:Ljava/lang/String;

    const-string v1, "startTypingExpirationTask"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;)Ljava/lang/Runnable;

    move-result-object v1

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide v4, v2

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->typingExpirationFuture:Ljava/util/concurrent/ScheduledFuture;

    .line 86
    return-void
.end method

.method public stopTypingExpirationTask()V
    .locals 2

    .prologue
    .line 89
    sget-object v0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->TAG:Ljava/lang/String;

    const-string v1, "stopTypingExpirationTask"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->typingExpirationFuture:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->typingExpirationFuture:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->typingExpirationFuture:Ljava/util/concurrent/ScheduledFuture;

    .line 95
    :cond_0
    return-void
.end method

.method public updateTypingIndicator(Ljava/lang/String;Ljava/util/Date;)V
    .locals 3
    .param p1, "gamerTag"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "timeStamp"    # Ljava/util/Date;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 36
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 37
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 40
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->typingData:Ljava/util/Map;

    monitor-enter v2

    .line 41
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->typingData:Ljava/util/Map;

    invoke-interface {v1, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    .line 42
    .local v0, "isUpdated":Z
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    if-eqz v0, :cond_0

    .line 45
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager;->callback:Lcom/microsoft/xbox/toolkit/TypingIndicatorManager$TypingIndicatorCallback;

    invoke-interface {v1}, Lcom/microsoft/xbox/toolkit/TypingIndicatorManager$TypingIndicatorCallback;->onTypingIndicatorUpdate()V

    .line 47
    :cond_0
    return-void

    .line 41
    .end local v0    # "isUpdated":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 42
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
