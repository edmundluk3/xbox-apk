.class public Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl_ViewBinding;
.super Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen_ViewBinding;
.source "ClubWatchViewImpl_ViewBinding.java"


# instance fields
.field private target:Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;)V
    .locals 0
    .param p1, "target"    # Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p1}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl_ViewBinding;-><init>(Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;Landroid/view/View;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen_ViewBinding;-><init>(Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;Landroid/view/View;)V

    .line 26
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl_ViewBinding;->target:Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;

    .line 28
    const v0, 0x7f0e03d0

    const-string v1, "field \'contentList\'"

    const-class v2, Landroid/support/v7/widget/RecyclerView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->contentList:Landroid/support/v7/widget/RecyclerView;

    .line 29
    const v0, 0x7f0e03cf

    const-string v1, "field \'swipeRefreshLayoutContentScreen\'"

    const-class v2, Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->swipeRefreshLayoutContentScreen:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 30
    const v0, 0x7f0e03d1

    const-string v1, "field \'swipeRefreshLayoutNoContentScreen\'"

    const-class v2, Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v0, p1, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->swipeRefreshLayoutNoContentScreen:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 31
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl_ViewBinding;->target:Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;

    .line 36
    .local v0, "target":Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 37
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl_ViewBinding;->target:Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;

    .line 39
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->contentList:Landroid/support/v7/widget/RecyclerView;

    .line 40
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->swipeRefreshLayoutContentScreen:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 41
    iput-object v1, v0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchViewImpl;->swipeRefreshLayoutNoContentScreen:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 43
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen_ViewBinding;->unbind()V

    .line 44
    return-void
.end method
