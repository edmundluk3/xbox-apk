.class public abstract Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
.super Landroid/widget/FrameLayout;
.source "ScreenModuleLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    return-void
.end method


# virtual methods
.method public abstract getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.end method

.method public invalidateView()V
    .locals 0

    .prologue
    .line 47
    return-void
.end method

.method public onApplicationPause()V
    .locals 0

    .prologue
    .line 27
    return-void
.end method

.method public onApplicationResume()V
    .locals 0

    .prologue
    .line 30
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 36
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 24
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 33
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 39
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 42
    return-void
.end method

.method protected setContentView(I)V
    .locals 3
    .param p1, "screenLayoutId"    # I

    .prologue
    .line 19
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 20
    .local v0, "vi":Landroid/view/LayoutInflater;
    const/4 v1, 0x1

    invoke-virtual {v0, p1, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 21
    return-void
.end method

.method public abstract setViewModel(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
.end method

.method public abstract updateView()V
.end method
