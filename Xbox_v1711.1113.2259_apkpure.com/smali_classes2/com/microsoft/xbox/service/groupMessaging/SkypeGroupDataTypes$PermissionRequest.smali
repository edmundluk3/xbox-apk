.class public final Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionRequest;
.super Ljava/lang/Object;
.source "SkypeGroupDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PermissionRequest"
.end annotation


# instance fields
.field private final permissions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final users:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "permissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "users":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$User;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 198
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 200
    iput-object p1, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionRequest;->permissions:Ljava/util/List;

    .line 201
    iput-object p2, p0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionRequest;->users:Ljava/util/List;

    .line 202
    return-void

    :cond_0
    move v0, v2

    .line 197
    goto :goto_0

    :cond_1
    move v1, v2

    .line 198
    goto :goto_1
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
