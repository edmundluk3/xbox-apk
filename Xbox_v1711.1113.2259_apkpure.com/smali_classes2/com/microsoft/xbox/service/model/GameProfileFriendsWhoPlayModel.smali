.class public Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;
.super Ljava/lang/Object;
.source "GameProfileFriendsWhoPlayModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;
    }
.end annotation


# static fields
.field private static final MAX_MODEL_COUNT:I = 0xff


# instance fields
.field private models:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/Long;",
            "Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;",
            ">;"
        }
    .end annotation
.end field

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "xuid"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0xff

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;->models:Landroid/util/LruCache;

    .line 28
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;->xuid:Ljava/lang/String;

    .line 29
    return-void
.end method

.method private getDataModel(J)Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;
    .locals 5
    .param p1, "gameTitleId"    # J

    .prologue
    .line 32
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;->models:Landroid/util/LruCache;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;

    .line 33
    .local v0, "model":Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;
    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;

    .end local v0    # "model":Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;->xuid:Ljava/lang/String;

    invoke-direct {v0, v2, p1, p2}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;-><init>(Ljava/lang/String;J)V

    .line 35
    .restart local v0    # "model":Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;->models:Landroid/util/LruCache;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 39
    .end local v0    # "model":Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;
    .local v1, "model":Ljava/lang/Object;
    :goto_0
    return-object v1

    .end local v1    # "model":Ljava/lang/Object;
    .restart local v0    # "model":Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;
    :cond_0
    move-object v1, v0

    .restart local v1    # "model":Ljava/lang/Object;
    goto :goto_0
.end method


# virtual methods
.method public getBroadcaster(J)Lcom/microsoft/xbox/service/model/FollowersData;
    .locals 1
    .param p1, "gameTitleId"    # J
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;->getDataModel(J)Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;->getBroadcaster()Lcom/microsoft/xbox/service/model/FollowersData;

    move-result-object v0

    return-object v0
.end method

.method public getResult(J)Ljava/util/ArrayList;
    .locals 1
    .param p1, "gameTitleId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;->getDataModel(J)Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;->getResult()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public load(ZJ)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 2
    .param p1, "forceRefresh"    # Z
    .param p2, "gameTitleId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZJ)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p2, p3}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;->getDataModel(J)Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;->load(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public shouldRefresh(J)Z
    .locals 1
    .param p1, "gameTitleId"    # J

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;->getDataModel(J)Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel$GameProfileFriendsWhoPlayDataModel;->shouldRefresh()Z

    move-result v0

    return v0
.end method
