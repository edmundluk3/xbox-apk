.class public Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes;
.super Ljava/lang/Object;
.source "ClubRosterDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;,
        Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;,
        Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateResponse;,
        Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterBatchUpdateRequest;,
        Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMembersRoleRequest;,
        Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubRosterRequestRole;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "This type shouldn\'t be instantiated."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
