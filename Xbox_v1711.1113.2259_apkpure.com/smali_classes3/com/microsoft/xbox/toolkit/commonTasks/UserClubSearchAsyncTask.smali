.class public Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "UserClubSearchAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final callbackRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final clubId:Ljava/lang/String;

.field private final query:Ljava/lang/String;

.field private final userSearchService:Lcom/microsoft/xbox/service/userSearch/IUserSearchService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 1
    .param p1, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "clubId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p3, "callback":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 31
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 32
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 34
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getUserSearchService()Lcom/microsoft/xbox/service/userSearch/IUserSearchService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;->userSearchService:Lcom/microsoft/xbox/service/userSearch/IUserSearchService;

    .line 35
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;->query:Ljava/lang/String;

    .line 36
    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;->clubId:Ljava/lang/String;

    .line 37
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;->callbackRef:Ljava/lang/ref/WeakReference;

    .line 38
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;
    .locals 4

    .prologue
    .line 57
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;->userSearchService:Lcom/microsoft/xbox/service/userSearch/IUserSearchService;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;->query:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;->clubId:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/microsoft/xbox/service/userSearch/IUserSearchService;->clubSearch(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 62
    :goto_0
    return-object v1

    .line 58
    :catch_0
    move-exception v0

    .line 59
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed to search for query:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;->query:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " clubId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;->clubId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;->onError()Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 47
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;)V
    .locals 2
    .param p1, "userSearchResponse"    # Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;

    .prologue
    .line 71
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;->callbackRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 73
    .local v0, "callback":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;>;"
    if-eqz v0, :cond_0

    .line 74
    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 76
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/commonTasks/UserClubSearchAsyncTask;->onPostExecute(Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 67
    return-void
.end method
