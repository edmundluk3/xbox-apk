.class public final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_ClubAccountsDataTypes_ClubAccountsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final canDeleteImmediatelyAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final codeAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final createdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultCanDeleteImmediately:Z

.field private defaultCode:I

.field private defaultCreated:Ljava/lang/String;

.field private defaultDescription:Ljava/lang/String;

.field private defaultId:J

.field private defaultName:Ljava/lang/String;

.field private defaultOwner:Ljava/lang/String;

.field private defaultSuspensionRequiredAfter:Ljava/util/Date;

.field private defaultSuspensions:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;",
            ">;"
        }
    .end annotation
.end field

.field private defaultType:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

.field private final descriptionAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final idAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final nameAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final ownerAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final suspensionRequiredAfterAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private final suspensionsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;",
            ">;>;"
        }
    .end annotation
.end field

.field private final typeAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 4
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 47
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 37
    iput v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultCode:I

    .line 38
    iput-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultDescription:Ljava/lang/String;

    .line 39
    iput-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultName:Ljava/lang/String;

    .line 40
    iput-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultOwner:Ljava/lang/String;

    .line 41
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultId:J

    .line 42
    iput-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultType:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 43
    iput-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultCreated:Ljava/lang/String;

    .line 44
    iput-boolean v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultCanDeleteImmediately:Z

    .line 45
    iput-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultSuspensionRequiredAfter:Ljava/util/Date;

    .line 46
    iput-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultSuspensions:Lcom/google/common/collect/ImmutableList;

    .line 48
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->codeAdapter:Lcom/google/gson/TypeAdapter;

    .line 49
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->descriptionAdapter:Lcom/google/gson/TypeAdapter;

    .line 50
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->nameAdapter:Lcom/google/gson/TypeAdapter;

    .line 51
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->ownerAdapter:Lcom/google/gson/TypeAdapter;

    .line 52
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->idAdapter:Lcom/google/gson/TypeAdapter;

    .line 53
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->typeAdapter:Lcom/google/gson/TypeAdapter;

    .line 54
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->createdAdapter:Lcom/google/gson/TypeAdapter;

    .line 55
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->canDeleteImmediatelyAdapter:Lcom/google/gson/TypeAdapter;

    .line 56
    const-class v0, Ljava/util/Date;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->suspensionRequiredAfterAdapter:Lcom/google/gson/TypeAdapter;

    .line 57
    const-class v0, Lcom/google/common/collect/ImmutableList;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/reflect/Type;

    const-class v2, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->suspensionsAdapter:Lcom/google/gson/TypeAdapter;

    .line 58
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    .locals 14
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 132
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v1

    sget-object v13, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v1, v13, :cond_0

    .line 133
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 134
    const/4 v1, 0x0

    .line 200
    :goto_0
    return-object v1

    .line 136
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 137
    iget v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultCode:I

    .line 138
    .local v2, "code":I
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultDescription:Ljava/lang/String;

    .line 139
    .local v3, "description":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultName:Ljava/lang/String;

    .line 140
    .local v4, "name":Ljava/lang/String;
    iget-object v5, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultOwner:Ljava/lang/String;

    .line 141
    .local v5, "owner":Ljava/lang/String;
    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultId:J

    .line 142
    .local v6, "id":J
    iget-object v8, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultType:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 143
    .local v8, "type":Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    iget-object v9, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultCreated:Ljava/lang/String;

    .line 144
    .local v9, "created":Ljava/lang/String;
    iget-boolean v10, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultCanDeleteImmediately:Z

    .line 145
    .local v10, "canDeleteImmediately":Z
    iget-object v11, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultSuspensionRequiredAfter:Ljava/util/Date;

    .line 146
    .local v11, "suspensionRequiredAfter":Ljava/util/Date;
    iget-object v12, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultSuspensions:Lcom/google/common/collect/ImmutableList;

    .line 147
    .local v12, "suspensions":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;>;"
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 148
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 149
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v1

    sget-object v13, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v1, v13, :cond_1

    .line 150
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 153
    :cond_1
    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v13

    sparse-switch v13, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v1, :pswitch_data_0

    .line 195
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 153
    :sswitch_0
    const-string v13, "code"

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v1, 0x0

    goto :goto_2

    :sswitch_1
    const-string v13, "description"

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :sswitch_2
    const-string v13, "name"

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v1, 0x2

    goto :goto_2

    :sswitch_3
    const-string v13, "owner"

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v1, 0x3

    goto :goto_2

    :sswitch_4
    const-string v13, "id"

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v1, 0x4

    goto :goto_2

    :sswitch_5
    const-string v13, "type"

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v1, 0x5

    goto :goto_2

    :sswitch_6
    const-string v13, "created"

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v1, 0x6

    goto :goto_2

    :sswitch_7
    const-string v13, "canDeleteImmediately"

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v1, 0x7

    goto :goto_2

    :sswitch_8
    const-string v13, "suspensionRequiredAfter"

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/16 v1, 0x8

    goto :goto_2

    :sswitch_9
    const-string v13, "suspensions"

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/16 v1, 0x9

    goto :goto_2

    .line 155
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->codeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 156
    goto/16 :goto_1

    .line 159
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->descriptionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "description":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 160
    .restart local v3    # "description":Ljava/lang/String;
    goto/16 :goto_1

    .line 163
    :pswitch_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->nameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "name":Ljava/lang/String;
    check-cast v4, Ljava/lang/String;

    .line 164
    .restart local v4    # "name":Ljava/lang/String;
    goto/16 :goto_1

    .line 167
    :pswitch_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->ownerAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "owner":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 168
    .restart local v5    # "owner":Ljava/lang/String;
    goto/16 :goto_1

    .line 171
    :pswitch_4
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->idAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 172
    goto/16 :goto_1

    .line 175
    :pswitch_5
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->typeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "type":Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    check-cast v8, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 176
    .restart local v8    # "type":Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    goto/16 :goto_1

    .line 179
    :pswitch_6
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->createdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "created":Ljava/lang/String;
    check-cast v9, Ljava/lang/String;

    .line 180
    .restart local v9    # "created":Ljava/lang/String;
    goto/16 :goto_1

    .line 183
    :pswitch_7
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->canDeleteImmediatelyAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    .line 184
    goto/16 :goto_1

    .line 187
    :pswitch_8
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->suspensionRequiredAfterAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "suspensionRequiredAfter":Ljava/util/Date;
    check-cast v11, Ljava/util/Date;

    .line 188
    .restart local v11    # "suspensionRequiredAfter":Ljava/util/Date;
    goto/16 :goto_1

    .line 191
    :pswitch_9
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->suspensionsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v12

    .end local v12    # "suspensions":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;>;"
    check-cast v12, Lcom/google/common/collect/ImmutableList;

    .line 192
    .restart local v12    # "suspensions":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;>;"
    goto/16 :goto_1

    .line 199
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 200
    new-instance v1, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;

    invoke-direct/range {v1 .. v12}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;Ljava/lang/String;ZLjava/util/Date;Lcom/google/common/collect/ImmutableList;)V

    goto/16 :goto_0

    .line 153
    nop

    :sswitch_data_0
    .sparse-switch
        -0x66ca7c04 -> :sswitch_1
        -0x64741b60 -> :sswitch_8
        0xd1b -> :sswitch_4
        0x2eaded -> :sswitch_0
        0x337a8b -> :sswitch_2
        0x368f3a -> :sswitch_5
        0x653f2b3 -> :sswitch_3
        0x369eec36 -> :sswitch_9
        0x3d4e7ee8 -> :sswitch_6
        0x7d257063 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultCanDeleteImmediately(Z)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultCanDeleteImmediately"    # Z

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultCanDeleteImmediately:Z

    .line 89
    return-object p0
.end method

.method public setDefaultCode(I)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultCode"    # I

    .prologue
    .line 60
    iput p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultCode:I

    .line 61
    return-object p0
.end method

.method public setDefaultCreated(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultCreated"    # Ljava/lang/String;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultCreated:Ljava/lang/String;

    .line 85
    return-object p0
.end method

.method public setDefaultDescription(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultDescription"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultDescription:Ljava/lang/String;

    .line 65
    return-object p0
.end method

.method public setDefaultId(J)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultId"    # J

    .prologue
    .line 76
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultId:J

    .line 77
    return-object p0
.end method

.method public setDefaultName(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultName"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultName:Ljava/lang/String;

    .line 69
    return-object p0
.end method

.method public setDefaultOwner(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultOwner"    # Ljava/lang/String;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultOwner:Ljava/lang/String;

    .line 73
    return-object p0
.end method

.method public setDefaultSuspensionRequiredAfter(Ljava/util/Date;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultSuspensionRequiredAfter"    # Ljava/util/Date;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultSuspensionRequiredAfter:Ljava/util/Date;

    .line 93
    return-object p0
.end method

.method public setDefaultSuspensions(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 96
    .local p1, "defaultSuspensions":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultSuspensions:Lcom/google/common/collect/ImmutableList;

    .line 97
    return-object p0
.end method

.method public setDefaultType(Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultType"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->defaultType:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 81
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;)V
    .locals 4
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    if-nez p2, :cond_0

    .line 103
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 128
    :goto_0
    return-void

    .line 106
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 107
    const-string v0, "code"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->codeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->code()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 109
    const-string v0, "description"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->descriptionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->description()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 111
    const-string v0, "name"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->nameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 113
    const-string v0, "owner"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->ownerAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->owner()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 115
    const-string v0, "id"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->idAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->id()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 117
    const-string v0, "type"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->typeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 119
    const-string v0, "created"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->createdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->created()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 121
    const-string v0, "canDeleteImmediately"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->canDeleteImmediatelyAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->canDeleteImmediately()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 123
    const-string v0, "suspensionRequiredAfter"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->suspensionRequiredAfterAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->suspensionRequiredAfter()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 125
    const-string v0, "suspensions"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->suspensionsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;->suspensions()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 127
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto/16 :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26
    check-cast p2, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;)V

    return-void
.end method
