.class Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "LfgVettingScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MultiplayerSessionAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final handleId:Ljava/lang/String;

.field private session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "handleId"    # Ljava/lang/String;

    .prologue
    .line 363
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 364
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 366
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;->handleId:Ljava/lang/String;

    .line 367
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 371
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3

    .prologue
    .line 386
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->multiplayerService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;->handleId:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerService;->getMultiplayerSession(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 392
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    :goto_0
    return-object v1

    .line 387
    :catch_0
    move-exception v0

    .line 388
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to load multiplayer session."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0

    .line 392
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 359
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 380
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 359
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 376
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 401
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;->session:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSession;)V

    .line 402
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 359
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgVettingScreenViewModel$MultiplayerSessionAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 397
    return-void
.end method
