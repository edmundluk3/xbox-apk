.class public Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "RecentsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel$LoadRecentsTask;
    }
.end annotation


# instance fields
.field private isLoadingRecents:Z

.field private loadTask:Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel$LoadRecentsTask;

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 15
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->isLoadingRecents:Z

    .line 16
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->onRecentsLoaded(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 13
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->isLoadingRecents:Z

    return p1
.end method

.method private hasRecents()Z
    .locals 1

    .prologue
    .line 58
    invoke-static {}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->getInstance()Lcom/microsoft/xbox/service/model/recents/RecentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->getRecents()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onRecentsLoaded(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->isLoadingRecents:Z

    .line 68
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 82
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->updateAdapter()V

    .line 83
    return-void

    .line 72
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->hasRecents()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 76
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    .line 77
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 68
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected createAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/RecentsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;)V

    return-object v0
.end method

.method public getRecents()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/recents/Recent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    invoke-static {}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->getInstance()Lcom/microsoft/xbox/service/model/recents/RecentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->getRecents()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->isLoadingRecents:Z

    return v0
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel$LoadRecentsTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel$LoadRecentsTask;->load(Z)V

    .line 51
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->createAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 32
    return-void
.end method

.method protected onStartOverride()V
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    if-nez v0, :cond_0

    .line 22
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->createAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 23
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel$LoadRecentsTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel$LoadRecentsTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel$LoadRecentsTask;

    .line 25
    :cond_0
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel;->loadTask:Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel$LoadRecentsTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/RecentsScreenViewModel$LoadRecentsTask;->cancel()V

    .line 41
    return-void
.end method
