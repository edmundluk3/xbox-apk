.class public Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "XboxAuthActivityViewModel.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private authStateDisposable:Lio/reactivex/disposables/Disposable;

.field authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private relaunchIntent:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 42
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;)V

    .line 43
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/XboxAuthActivityAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 44
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;->gotoNextPage()V

    return-void
.end method

.method private checkVersion()Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v7, 0x0

    .line 106
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getMustUpdate(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 108
    const-string v0, "VersionCheck"

    const-string v1, "must update"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XLEUtil;->forceUpdate()V

    move v0, v7

    .line 145
    :goto_0
    return v0

    .line 113
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getHasUpdate(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 115
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getLatestVersion()I

    move-result v6

    .line 116
    .local v6, "lastestVersion":I
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getLastCheckedVersion()I

    move-result v1

    if-ge v1, v6, :cond_1

    .line 117
    const-string v0, "VersionCheck"

    const-string v1, "available optional update"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070ca1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070ca5

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel$1;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;)V

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070607

    .line 124
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel$2;

    invoke-direct {v5, p0, v6}, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel$2;-><init>(Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;I)V

    move-object v0, p0

    .line 119
    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    move v0, v7

    .line 134
    goto :goto_0

    .line 136
    :cond_1
    const-string v1, "VersionCheck"

    const-string v2, "already checked, let it pass"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 141
    .end local v6    # "lastestVersion":I
    :cond_2
    const-string v1, "VersionCheck"

    const-string/jumbo v2, "up to date"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setLastCheckedVersion(I)V

    goto :goto_0
.end method

.method private gotoNextPage()V
    .locals 5

    .prologue
    .line 149
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;->relaunchIntent:Landroid/content/Intent;

    if-eqz v3, :cond_0

    .line 151
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 152
    .local v0, "a":Landroid/app/Activity;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;->relaunchIntent:Landroid/content/Intent;

    invoke-virtual {v0, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 157
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    .end local v0    # "a":Landroid/app/Activity;
    :goto_0
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;->relaunchIntent:Landroid/content/Intent;

    .line 166
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigateToHomeScreen()V
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_1

    .line 170
    :goto_1
    return-void

    .line 158
    :catch_0
    move-exception v1

    .line 159
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;->TAG:Ljava/lang/String;

    const-string v4, "Failed to relaunch brokered app after signin"

    invoke-static {v3, v4, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 167
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    :catch_1
    move-exception v2

    .line 168
    .local v2, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;->TAG:Ljava/lang/String;

    const-string v4, "Failed to navigate to home screen after auth"

    invoke-static {v3, v4, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method static synthetic lambda$beginLogin$0(Lcom/microsoft/xbox/domain/auth/AuthState;)Z
    .locals 1
    .param p0, "state"    # Lcom/microsoft/xbox/domain/auth/AuthState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 74
    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthState;->SignInSuccess:Lcom/microsoft/xbox/domain/auth/AuthState;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$beginLogin$1(Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;Lcom/microsoft/xbox/domain/auth/AuthState;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;
    .param p1, "ignore"    # Lcom/microsoft/xbox/domain/auth/AuthState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 76
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 78
    .local v0, "activity":Lcom/microsoft/xbox/xle/app/MainActivity;
    if-eqz v0, :cond_0

    .line 79
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->onSignInSuccess()V

    .line 82
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;->checkVersion()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 83
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getInstance()Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->ensureRegIdOnLogin()V

    .line 84
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;->gotoNextPage()V

    .line 86
    :cond_1
    return-void
.end method


# virtual methods
.method public beginLogin()V
    .locals 3

    .prologue
    .line 65
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 67
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getForceSignin()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 68
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getAppRelaunchIntent()Landroid/content/Intent;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;->relaunchIntent:Landroid/content/Intent;

    .line 71
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    invoke-interface {v1}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->signInWithUi()V

    .line 73
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    invoke-interface {v1}, Lcom/microsoft/xbox/domain/auth/AuthStateManager;->getAuthStates()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel$$Lambda$1;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v2

    .line 74
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 75
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;->authStateDisposable:Lio/reactivex/disposables/Disposable;

    .line 87
    return-void
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    return v0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 53
    return-void
.end method

.method public onBackButtonPressed()V
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->killApp()V

    .line 103
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 49
    return-void
.end method

.method protected onStartOverride()V
    .locals 2

    .prologue
    .line 91
    const-string v0, "XboxAuth"

    const-string v1, "onStart called"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->resetGlobalParameters()V

    .line 93
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 57
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onStop()V

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;->authStateDisposable:Lio/reactivex/disposables/Disposable;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;->authStateDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 62
    :cond_0
    return-void
.end method

.method protected onStopOverride()V
    .locals 2

    .prologue
    .line 97
    const-string v0, "XboxAuth"

    const-string v1, "onStop called"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    return-void
.end method
