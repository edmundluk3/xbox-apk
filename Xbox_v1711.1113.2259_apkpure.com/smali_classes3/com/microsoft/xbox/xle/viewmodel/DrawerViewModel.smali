.class public Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "DrawerViewModel.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 39
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getDrawerAdapter(Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 40
    return-void
.end method

.method static synthetic lambda$updateOverride$0(Ljava/lang/Long;)Z
    .locals 1
    .param p0, "ignore"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 91
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->isAnimating()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$updateOverride$1(Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;Ljava/lang/Long;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;
    .param p1, "ignore"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->updateAdapter()V

    return-void
.end method


# virtual methods
.method public getGamerScore()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerScore()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getGamerTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerTag()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getProfilePicUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerPicImageUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getRealName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getRealName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    return v0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 75
    return-void
.end method

.method public navigateTo(Ljava/lang/Class;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/app/activity/ActivityBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 115
    .local p1, "destScreen":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/app/activity/ActivityBase;>;"
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 116
    .local v0, "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v3, p1, :cond_2

    :cond_0
    const/4 v2, 0x1

    .line 117
    .local v2, "requiresTransition":Z
    :goto_0
    if-eqz v2, :cond_1

    .line 118
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedXuid(Ljava/lang/String;)V

    .line 119
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->putScreenOnHomeScreen(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 122
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->toggleDrawer()V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    .end local v0    # "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .end local v2    # "requiresTransition":Z
    :goto_1
    return-void

    .line 116
    .restart local v0    # "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 123
    .end local v0    # "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :catch_0
    move-exception v1

    .line 124
    .local v1, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to navigate to screen \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public navigateToHome()V
    .locals 3

    .prologue
    .line 130
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->navigateToHomeScreen()V

    .line 131
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->toggleDrawer()V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    :goto_0
    return-void

    .line 132
    :catch_0
    move-exception v0

    .line 133
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Failed to navigate to home from drawer"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public navigateToPivot(Ljava/lang/Class;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/app/activity/PivotActivity;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/app/activity/ActivityBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 147
    .local p1, "destPivot":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/app/activity/PivotActivity;>;"
    .local p2, "pane":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/app/activity/ActivityBase;>;"
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 148
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->navigateToPivot(Ljava/lang/Class;Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 149
    return-void
.end method

.method public navigateToPivot(Ljava/lang/Class;Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    .locals 7
    .param p3, "params"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/app/activity/PivotActivity;",
            ">;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/app/activity/ActivityBase;",
            ">;",
            "Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;",
            ")V"
        }
    .end annotation

    .prologue
    .line 157
    .local p1, "destPivot":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/app/activity/PivotActivity;>;"
    .local p2, "pane":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/app/activity/ActivityBase;>;"
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v1

    .line 158
    .local v1, "currentParameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 159
    .local v0, "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-ne v4, p1, :cond_0

    if-eqz v1, :cond_1

    .line 160
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedProfile()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedProfile()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    const/4 v3, 0x1

    .line 162
    .local v3, "requiresTransition":Z
    :goto_0
    if-eqz v3, :cond_2

    .line 163
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setActivePivotPane(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 164
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v4

    invoke-virtual {v4, p1, p3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->putScreenOnHomeScreen(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 169
    .end local v0    # "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :goto_1
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->toggleDrawer()V

    .line 173
    .end local v1    # "currentParameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .end local v3    # "requiresTransition":Z
    :goto_2
    return-void

    .line 160
    .restart local v0    # "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .restart local v1    # "currentParameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 166
    .restart local v3    # "requiresTransition":Z
    :cond_2
    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;

    .end local v0    # "currentActivity":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->animateToActivePivotPane(Ljava/lang/Class;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 170
    .end local v1    # "currentParameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .end local v3    # "requiresTransition":Z
    :catch_0
    move-exception v2

    .line 171
    .local v2, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to navigate to pivot \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' / pane \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public navigateToProfile()V
    .locals 3

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->refreshProfile()V

    .line 139
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v1, :cond_0

    .line 140
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 141
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedProfile(Ljava/lang/String;)V

    .line 142
    const-class v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    const-class v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreen;

    invoke-virtual {p0, v1, v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->navigateToPivot(Ljava/lang/Class;Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 144
    .end local v0    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_0
    return-void
.end method

.method public navigateToSearchScreen()V
    .locals 1

    .prologue
    .line 152
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/SearchScreen;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->NavigateTo(Ljava/lang/Class;)V

    .line 153
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 46
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getDrawerAdapter(Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 47
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 57
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/MessageModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 58
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 59
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->refreshProfile()V

    .line 60
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/ProfileModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 69
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/MessageModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 70
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 71
    return-void
.end method

.method public refreshProfile()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-nez v0, :cond_0

    .line 79
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/ProfileModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 85
    :cond_0
    return-void
.end method

.method public updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 90
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    const-wide/16 v0, 0x64

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->interval(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel$$Lambda$1;->lambdaFactory$()Lio/reactivex/functions/Predicate;

    move-result-object v1

    .line 91
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v2, 0x1

    .line 92
    invoke-virtual {v0, v2, v3}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 93
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/DrawerViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 94
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 95
    return-void
.end method
