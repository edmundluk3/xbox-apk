.class public Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "ClubCreationViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubCreatedListener;
.implements Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubNameReservedListener;
.implements Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubInformationListener;


# static fields
.field private static final FIX_ACCOUNT_URL:Ljava/lang/String; = "http://xbox.com/fix-clubs"


# instance fields
.field private final clubCreationManager:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

.field private currentPage:I

.field private newClubId:J

.field private final pages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationStep;",
            ">;"
        }
    .end annotation
.end field

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 3
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 45
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationStep;

    const/4 v1, 0x0

    new-instance v2, Lcom/microsoft/xbox/xle/app/clubs/create/ClubTypeSelectionStep;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubTypeSelectionStep;-><init>(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubNameSelectionStep;-><init>(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreateConfirmationStep;-><init>(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;)V

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->pages:Ljava/util/List;

    .line 46
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubCreationScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 47
    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-direct {v0, p0, p0, p0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubInformationListener;Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubNameReservedListener;Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager$ClubCreatedListener;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->clubCreationManager:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    .line 48
    return-void
.end method

.method static synthetic lambda$onClubCreationError$0()V
    .locals 4

    .prologue
    .line 188
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    const-string v3, "http://xbox.com/fix-clubs"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public createClub()V
    .locals 1

    .prologue
    .line 158
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->clubCreationManager:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->createClub()V

    .line 160
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->updateAdapter()V

    .line 161
    return-void
.end method

.method public exit()V
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->currentPage:I

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackCreateCancel(I)V

    .line 165
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->goBack()V

    .line 166
    return-void
.end method

.method public finishClubCreation()V
    .locals 6

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->exit()V

    .line 218
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreen;

    const/4 v2, 0x1

    new-instance v3, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->newClubId:J

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;-><init>(J)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 219
    return-void
.end method

.method public getClubName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->clubCreationManager:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->getSelectedClubName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getClubType()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->clubCreationManager:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->getSelectedClubType()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v0

    return-object v0
.end method

.method public getCreationPage(I)Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationStep;
    .locals 1
    .param p1, "pos"    # I
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->pages:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationStep;

    return-object v0
.end method

.method public getCurrentPage()I
    .locals 1

    .prologue
    .line 139
    iget v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->currentPage:I

    return v0
.end method

.method public getMaximumHiddenClubs()I
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->clubCreationManager:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->getMaximumHiddenClubs()I

    move-result v0

    return v0
.end method

.method public getMaximumOpenAndClosedClubs()I
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->clubCreationManager:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->getMaximumOpenAndClosedClubs()I

    move-result v0

    return v0
.end method

.method public getNameErrorReason()I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->clubCreationManager:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->getNameReservationErrorReason()I

    move-result v0

    return v0
.end method

.method public getPageCount()I
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->pages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getRemainingOpenAndClosedClubs()I
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->clubCreationManager:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->getRemainingOpenAndClosedClubs()I

    move-result v0

    return v0
.end method

.method public getRemainingPrivateClubs()I
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->clubCreationManager:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->getRemainingHiddenClubs()I

    move-result v0

    return v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    return v0
.end method

.method public isClubNameValid()Z
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->clubCreationManager:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->isSelectedClubNameValid()Z

    move-result v0

    return v0
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 70
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->clubCreationManager:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->loadClubsInformation(Z)V

    .line 72
    return-void
.end method

.method public nextPage()V
    .locals 2

    .prologue
    .line 143
    iget v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->currentPage:I

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->getPageCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 144
    iget v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->currentPage:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->currentPage:I

    .line 145
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->updateAdapter()V

    .line 147
    :cond_0
    return-void
.end method

.method public onBackButtonPressed()V
    .locals 2

    .prologue
    .line 76
    iget v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->currentPage:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->currentPage:I

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 77
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->previousPage()V

    .line 81
    :goto_0
    return-void

    .line 79
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onBackButtonPressed()V

    goto :goto_0
.end method

.method public onClubCreated(J)V
    .locals 1
    .param p1, "clubId"    # J

    .prologue
    .line 174
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 175
    iput-wide p1, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->newClubId:J

    .line 176
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->finishClubCreation()V

    .line 177
    return-void
.end method

.method public onClubCreationError(I)V
    .locals 8
    .param p1, "errorCode"    # I

    .prologue
    .line 181
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 183
    const/16 v0, 0x40d

    if-ne p1, v0, :cond_0

    .line 184
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    .line 185
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f070756

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 186
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f070754

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "http://xbox.com/fix-clubs"

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 187
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    const v4, 0x7f070757

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel$$Lambda$1;->lambdaFactory$()Ljava/lang/Runnable;

    move-result-object v4

    .line 189
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v5

    const v6, 0x7f07060d

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    .line 184
    invoke-interface/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 191
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->updateAdapter()V

    .line 203
    :goto_0
    return-void

    .line 193
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->clubCreationManager:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->getClubCreationNameErrorReason()I

    move-result v7

    .line 194
    .local v7, "nameErrorReason":I
    if-eqz v7, :cond_1

    .line 195
    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->showError(I)V

    .line 196
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->clubCreationManager:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->resetClubName()V

    .line 197
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->previousPage()V

    goto :goto_0

    .line 199
    :cond_1
    const v0, 0x7f070189

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->showError(I)V

    .line 200
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->updateAdapter()V

    goto :goto_0
.end method

.method public onClubInformationListener(Z)V
    .locals 1
    .param p1, "success"    # Z

    .prologue
    .line 207
    if-eqz p1, :cond_0

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 208
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->updateAdapter()V

    .line 209
    return-void

    .line 207
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method

.method public onClubNameReserved()V
    .locals 0

    .prologue
    .line 213
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->updateAdapter()V

    .line 214
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 56
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubCreationScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 57
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 52
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public previousPage()V
    .locals 1

    .prologue
    .line 150
    iget v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->currentPage:I

    if-lez v0, :cond_0

    .line 151
    iget v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->currentPage:I

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackCreateBack(I)V

    .line 152
    iget v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->currentPage:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->currentPage:I

    .line 153
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->updateAdapter()V

    .line 155
    :cond_0
    return-void
.end method

.method public setClubName(Ljava/lang/String;)V
    .locals 1
    .param p1, "clubName"    # Ljava/lang/String;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->clubCreationManager:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->setClubName(Ljava/lang/String;)V

    .line 107
    return-void
.end method

.method public setClubType(Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)V
    .locals 2
    .param p1, "clubType"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->clubCreationManager:Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubCreationManager;->setSelectedClubType(Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/create/ClubCreationViewModel;->nextPage()V

    .line 99
    :goto_0
    return-void

    .line 97
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const-string v1, "Cannot create club"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(Ljava/lang/String;)V

    goto :goto_0
.end method
