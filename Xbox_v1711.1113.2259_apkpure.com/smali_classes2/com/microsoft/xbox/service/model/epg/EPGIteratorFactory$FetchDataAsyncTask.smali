.class Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;
.super Lcom/microsoft/xbox/toolkit/XLEAsyncTask;
.source "EPGIteratorFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FetchDataAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private fetchFromListService:Z

.field private forceRefresh:Z

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;ZZ)V
    .locals 2
    .param p2, "_fetchFromListService"    # Z
    .param p3, "_forceRefresh"    # Z

    .prologue
    const/4 v1, 0x0

    .line 463
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    .line 464
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 460
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->fetchFromListService:Z

    .line 461
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->forceRefresh:Z

    .line 465
    iput-boolean p2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->fetchFromListService:Z

    .line 466
    iput-boolean p3, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->forceRefresh:Z

    .line 467
    return-void
.end method


# virtual methods
.method protected doInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 471
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->access$400(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;)Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;

    move-result-object v7

    .line 472
    .local v7, "fetchPending":Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;
    if-nez v7, :cond_0

    .line 473
    const-string v0, "EPGIteratorFactory"

    const-string v1, "Pending state should not be null"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 497
    :goto_0
    return-object v0

    .line 479
    :cond_0
    const-wide/16 v0, 0x32

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 483
    :goto_1
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->cancelled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->access$500(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;)Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;

    move-result-object v0

    if-eq p0, v0, :cond_2

    .line 484
    :cond_1
    const-string v0, "EPGIteratorFactory"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->access$600(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Task already cancelled "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->cancelled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " vs "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->access$500(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;)Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0

    .line 488
    :cond_2
    const-string v0, "EPGIteratorFactory"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->access$600(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "Start Fetch "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, v7, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;->channel:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " at "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, v7, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;->time:I

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->formatTime(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->fetchFromListService:Z

    if-eqz v0, :cond_3

    .line 491
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->access$700(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->fetchFromListServiceIfNeededAsync()Z

    .line 497
    :goto_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto/16 :goto_0

    .line 494
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->access$700(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v0

    iget v1, v7, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;->time:I

    iget v3, v7, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchState;->channel:I

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->access$700(Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getFilter()Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;->fromFilterPreference(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo$FilterPreference;)Lcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;

    move-result-object v5

    iget-boolean v6, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->forceRefresh:Z

    move v4, v2

    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->fetchIfNeededForTimeAsync(IZIZLcom/microsoft/xbox/service/model/epg/EPGProvider$EPGScheduleType;Z)V

    goto :goto_2

    .line 480
    :catch_0
    move-exception v0

    goto/16 :goto_1
.end method

.method protected bridge synthetic doInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 458
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->doInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 506
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 507
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->fetchFromListService:Z

    if-eqz v0, :cond_0

    .line 508
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorUnknown:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->onFetchListServiceComplete(Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;)V

    .line 519
    :goto_0
    return-void

    .line 510
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->errorUnknown:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->onFetchComplete(Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;)V

    goto :goto_0

    .line 513
    :cond_1
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->fetchFromListService:Z

    if-eqz v0, :cond_2

    .line 514
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->resultNoError:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->onFetchListServiceComplete(Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;)V

    goto :goto_0

    .line 516
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    sget-object v1, Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;->resultNoError:Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->onFetchComplete(Lcom/microsoft/xbox/service/model/epg/EPGProvider$FetchResult;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 458
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory$FetchDataAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 502
    return-void
.end method
