.class public Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;
.super Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;
.source "HomeScreenPinsAdapterPhone.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewType;,
        Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;
    }
.end annotation


# static fields
.field private static final MARGIN_MULTIPLIER:I = 0x2

.field private static final MAX_CELLS:I = 0x10

.field private static final MIN_PIN_CELLS:I = 0x0

.field private static final WIDTH_IN_CELLS:I = 0x3


# instance fields
.field private final adp:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;

.field private final adpView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

.field private final cellListener:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;

.field private final moreListener:Landroid/view/View$OnClickListener;

.field private pinsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            ">;"
        }
    .end annotation
.end field

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .prologue
    .line 33
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->getContainerResourceId()I

    move-result v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->getPosition()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;-><init>(II)V

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->pinsList:Ljava/util/List;

    .line 141
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->moreListener:Landroid/view/View$OnClickListener;

    .line 34
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->section:Landroid/view/View;

    const v1, 0x7f0e06fd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->adpView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->adpView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    iget v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->cellMargin:I

    mul-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->setItemMargin(I)V

    .line 39
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->container:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;-><init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->adp:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;

    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->adpView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->adp:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 41
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->cellListener:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;)Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->adpView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->adp:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->getPinCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->pinsList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->cellListener:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->moreListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    return-object v0
.end method

.method private getPinCount()I
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->pinsList:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->pinsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method protected onLayoutChanged()V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->section:Landroid/view/View;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 74
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 46
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->onStart()V

    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->adpView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->getAdpViewScrollState()Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->applyScrollState(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Lcom/microsoft/xbox/toolkit/ui/ScrollState;)V

    .line 48
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->adpView:Lcom/microsoft/xbox/toolkit/ui/LoadMoreHorizontalListView;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->createScrollState(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->setAdpVewScrollState(Lcom/microsoft/xbox/toolkit/ui/ScrollState;)V

    .line 53
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->onStop()V

    .line 54
    return-void
.end method

.method protected updateViewOverride()V
    .locals 2

    .prologue
    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->getPins()Ljava/util/List;

    move-result-object v0

    .line 59
    .local v0, "modelPins":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/pins/PinItem;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->pinsList:Ljava/util/List;

    if-eq v0, v1, :cond_0

    .line 60
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->pinsList:Ljava/util/List;

    .line 61
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;->adp:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone$ViewAdapter;->notifyDataSetChanged()V

    .line 63
    :cond_0
    return-void
.end method
