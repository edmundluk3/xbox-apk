.class final enum Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;
.super Ljava/lang/Enum;
.source "BiEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/BiEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "PageAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

.field public static final enum ACTIVITY_FEED_ACHIEVEMENT_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

.field public static final enum ACTIVITY_FEED_ADD_FRIEND_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

.field public static final enum ACTIVITY_FEED_BROADCAST_PLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

.field public static final enum ACTIVITY_FEED_GAME_DVR_PLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

.field public static final enum ACTIVITY_FEED_PROFILE_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

.field public static final enum ACTIVITY_FEED_SCREENSHOT_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

.field public static final enum ACTIVITY_FEED_SHARE_ACHIEVEMENT_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

.field public static final enum ACTIVITY_FEED_SHARE_ADD_FRIEND_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

.field public static final enum ACTIVITY_FEED_SHARE_BROADCAST_PLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

.field public static final enum ACTIVITY_FEED_SHARE_GAME_DVR_PLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

.field public static final enum ACTIVITY_FEED_SHARE_PROFILE_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

.field public static final enum ACTIVITY_FEED_SHARE_SCREENSHOT_DISPLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

.field public static final enum ACTIVITY_FEED_SHARE_STATUS_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

.field public static final enum ACTIVITY_FEED_STATUS_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

.field public static final enum GLOBAL_SCREENSHOT_DISPLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

.field public static final enum MESSAGE_ATTACHMENT_ACHIEVEMENT_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

.field public static final enum MESSAGE_ATTACHMENT_BROADCAST_PLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

.field public static final enum MESSAGE_ATTACHMENT_GAME_DVR_PLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

.field public static final enum MESSAGE_ATTACHMENT_SCREENSHOT_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

.field public static final enum MESSAGE_ATTACHMENT_SHARED_ITEM_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

.field public static final enum WATCH_BROADCAST:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 156
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    const-string v1, "WATCH_BROADCAST"

    const-string v2, "Watch Broadcast"

    invoke-direct {v0, v1, v4, v2}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->WATCH_BROADCAST:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    .line 157
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    const-string v1, "MESSAGE_ATTACHMENT_ACHIEVEMENT_SELECTION"

    const-string v2, "Messaging - Show Achievement"

    invoke-direct {v0, v1, v5, v2}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->MESSAGE_ATTACHMENT_ACHIEVEMENT_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    .line 158
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    const-string v1, "MESSAGE_ATTACHMENT_BROADCAST_PLAY"

    const-string v2, "Messaging - Play Broadcast"

    invoke-direct {v0, v1, v6, v2}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->MESSAGE_ATTACHMENT_BROADCAST_PLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    .line 159
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    const-string v1, "MESSAGE_ATTACHMENT_GAME_DVR_PLAY"

    const-string v2, "Messaging - Play Clip"

    invoke-direct {v0, v1, v7, v2}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->MESSAGE_ATTACHMENT_GAME_DVR_PLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    .line 160
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    const-string v1, "MESSAGE_ATTACHMENT_SHARED_ITEM_SELECTION"

    const-string v2, "Messaging - Show Shared Item"

    invoke-direct {v0, v1, v8, v2}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->MESSAGE_ATTACHMENT_SHARED_ITEM_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    .line 161
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    const-string v1, "MESSAGE_ATTACHMENT_SCREENSHOT_SELECTION"

    const/4 v2, 0x5

    const-string v3, "Messaging - Show Screenshot"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->MESSAGE_ATTACHMENT_SCREENSHOT_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    .line 162
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    const-string v1, "ACTIVITY_FEED_ACHIEVEMENT_SELECTION"

    const/4 v2, 0x6

    const-string v3, "Activity Feed - Achievement Selection"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_ACHIEVEMENT_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    .line 163
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    const-string v1, "ACTIVITY_FEED_BROADCAST_PLAY"

    const/4 v2, 0x7

    const-string v3, "Activity Feed - Broadcast Play"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_BROADCAST_PLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    .line 164
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    const-string v1, "ACTIVITY_FEED_GAME_DVR_PLAY"

    const/16 v2, 0x8

    const-string v3, "Activity Feed - Game DVR Play"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_GAME_DVR_PLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    .line 165
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    const-string v1, "ACTIVITY_FEED_SCREENSHOT_SELECTION"

    const/16 v2, 0x9

    const-string v3, "Activity Feed - Screenshot Display"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SCREENSHOT_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    .line 166
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    const-string v1, "ACTIVITY_FEED_STATUS_SELECTION"

    const/16 v2, 0xa

    const-string v3, "Activity Feed - Status Selection"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_STATUS_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    .line 167
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    const-string v1, "ACTIVITY_FEED_ADD_FRIEND_SELECTION"

    const/16 v2, 0xb

    const-string v3, "Activity Feed - Add Friend Selection"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_ADD_FRIEND_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    .line 168
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    const-string v1, "ACTIVITY_FEED_PROFILE_SELECTION"

    const/16 v2, 0xc

    const-string v3, "Activity Feed - Profile Selection"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_PROFILE_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    .line 169
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    const-string v1, "ACTIVITY_FEED_SHARE_ACHIEVEMENT_SELECTION"

    const/16 v2, 0xd

    const-string v3, "Activity Feed Share - Achievement Selection"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SHARE_ACHIEVEMENT_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    .line 170
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    const-string v1, "ACTIVITY_FEED_SHARE_BROADCAST_PLAY"

    const/16 v2, 0xe

    const-string v3, "Activity Feed Share - Broadcast Play"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SHARE_BROADCAST_PLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    .line 171
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    const-string v1, "ACTIVITY_FEED_SHARE_GAME_DVR_PLAY"

    const/16 v2, 0xf

    const-string v3, "Activity Feed Share - Game DVR Play"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SHARE_GAME_DVR_PLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    .line 172
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    const-string v1, "ACTIVITY_FEED_SHARE_SCREENSHOT_DISPLAY"

    const/16 v2, 0x10

    const-string v3, "Activity Feed Share - Screenshot Display"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SHARE_SCREENSHOT_DISPLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    .line 173
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    const-string v1, "ACTIVITY_FEED_SHARE_STATUS_SELECTION"

    const/16 v2, 0x11

    const-string v3, "Activity Feed Share - Status Selection"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SHARE_STATUS_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    .line 174
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    const-string v1, "ACTIVITY_FEED_SHARE_ADD_FRIEND_SELECTION"

    const/16 v2, 0x12

    const-string v3, "Activity Feed Share - Add Friend Selection"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SHARE_ADD_FRIEND_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    .line 175
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    const-string v1, "GLOBAL_SCREENSHOT_DISPLAY"

    const/16 v2, 0x13

    const-string v3, "Screenshot View Count"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->GLOBAL_SCREENSHOT_DISPLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    .line 176
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    const-string v1, "ACTIVITY_FEED_SHARE_PROFILE_SELECTION"

    const/16 v2, 0x14

    const-string v3, "Activity Feed Share - Profile Selection"

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SHARE_PROFILE_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    .line 154
    const/16 v0, 0x15

    new-array v0, v0, [Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->WATCH_BROADCAST:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->MESSAGE_ATTACHMENT_ACHIEVEMENT_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->MESSAGE_ATTACHMENT_BROADCAST_PLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->MESSAGE_ATTACHMENT_GAME_DVR_PLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    aput-object v1, v0, v7

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->MESSAGE_ATTACHMENT_SHARED_ITEM_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->MESSAGE_ATTACHMENT_SCREENSHOT_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_ACHIEVEMENT_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_BROADCAST_PLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_GAME_DVR_PLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SCREENSHOT_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_STATUS_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_ADD_FRIEND_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_PROFILE_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SHARE_ACHIEVEMENT_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SHARE_BROADCAST_PLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SHARE_GAME_DVR_PLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SHARE_SCREENSHOT_DISPLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SHARE_STATUS_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SHARE_ADD_FRIEND_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->GLOBAL_SCREENSHOT_DISPLAY:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->ACTIVITY_FEED_SHARE_PROFILE_SELECTION:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 180
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 181
    iput-object p3, p0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->name:Ljava/lang/String;

    .line 182
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 154
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;
    .locals 1

    .prologue
    .line 154
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$PageAction;->name:Ljava/lang/String;

    return-object v0
.end method
