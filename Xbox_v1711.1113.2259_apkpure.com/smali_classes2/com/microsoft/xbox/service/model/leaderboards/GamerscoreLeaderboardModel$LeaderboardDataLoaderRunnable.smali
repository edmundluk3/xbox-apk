.class Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "GamerscoreLeaderboardModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LeaderboardDataLoaderRunnable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final maxItems:J

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

.field private final xuid:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;J)V
    .locals 2
    .param p2, "maxItems"    # J

    .prologue
    .line 260
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;->this$0:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 261
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;->xuid:Ljava/lang/String;

    .line 262
    iput-wide p2, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;->maxItems:J

    .line 263
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;JLcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;
    .param p2, "x1"    # J
    .param p4, "x2"    # Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$1;

    .prologue
    .line 255
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;-><init>(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;J)V

    return-void
.end method

.method private constructor <init>(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;Ljava/lang/String;J)V
    .locals 1
    .param p2, "xuid"    # Ljava/lang/String;
    .param p3, "maxItems"    # J

    .prologue
    .line 265
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;->this$0:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 266
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;->xuid:Ljava/lang/String;

    .line 267
    iput-wide p3, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;->maxItems:J

    .line 268
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;Ljava/lang/String;JLcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$1;)V
    .locals 1
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # J
    .param p5, "x3"    # Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$1;

    .prologue
    .line 255
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;-><init>(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;Ljava/lang/String;J)V

    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 279
    iget-object v11, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;->this$0:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    monitor-enter v11

    .line 280
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v9

    .line 281
    .local v9, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    const/4 v1, 0x2

    if-ge v8, v1, :cond_3

    .line 283
    if-eqz v9, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;->this$0:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$200(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;->this$0:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$300(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 284
    :cond_0
    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v1, v10

    .line 303
    :goto_1
    return-object v1

    .line 289
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;->xuid:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 290
    invoke-static {}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$500()Lcom/microsoft/xbox/service/leaderboards/ILeaderboardsService;

    move-result-object v1

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;->this$0:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$400(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;)Ljava/util/Calendar;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;->this$0:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$200(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;->xuid:Ljava/lang/String;

    iget-wide v6, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;->maxItems:J

    invoke-interface/range {v1 .. v7}, Lcom/microsoft/xbox/service/leaderboards/ILeaderboardsService;->getGamerscoreLeaderboard(Ljava/lang/String;Ljava/util/Calendar;Ljava/lang/String;Ljava/lang/String;J)Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    :try_start_2
    monitor-exit v11

    goto :goto_1

    .line 304
    .end local v8    # "i":I
    .end local v9    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :catchall_0
    move-exception v1

    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 292
    .restart local v8    # "i":I
    .restart local v9    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_2
    :try_start_3
    invoke-static {}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$500()Lcom/microsoft/xbox/service/leaderboards/ILeaderboardsService;

    move-result-object v1

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;->this$0:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$400(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;)Ljava/util/Calendar;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;->this$0:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$200(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;->this$0:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-static {v5}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$600(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;)Ljava/lang/Long;

    move-result-object v5

    iget-wide v6, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;->maxItems:J

    invoke-interface/range {v1 .. v7}, Lcom/microsoft/xbox/service/leaderboards/ILeaderboardsService;->getGamerscoreLeaderboard(Ljava/lang/String;Ljava/util/Calendar;Ljava/lang/String;Ljava/lang/Long;J)Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;
    :try_end_3
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    :try_start_4
    monitor-exit v11

    goto :goto_1

    .line 294
    :catch_0
    move-exception v0

    .line 295
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$700()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to get leaderboard data"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 298
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;->this$0:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->refreshServerInstance()V

    .line 281
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 302
    .end local v0    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->access$700()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to get a valid response from the service"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    monitor-exit v11
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v1, v10

    goto :goto_1
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;->buildData()Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 314
    const-wide/16 v0, 0x2134

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 309
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/leaderboards/LeaderboardsDataTypes$LeaderboardResponse;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;->this$0:Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel$LeaderboardDataLoaderRunnable;->xuid:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, p1, v0}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->onLeaderboardDataLoaded(Lcom/microsoft/xbox/toolkit/AsyncResult;Z)V

    .line 310
    return-void

    .line 309
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 272
    return-void
.end method
