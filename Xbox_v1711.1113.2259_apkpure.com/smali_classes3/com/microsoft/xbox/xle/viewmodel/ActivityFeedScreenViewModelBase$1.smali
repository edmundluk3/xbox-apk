.class Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$1;
.super Ljava/lang/Object;
.source "ActivityFeedScreenViewModelBase.java"

# interfaces
.implements Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->hideActivitiesFromClub(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

.field final synthetic val$clubId:J


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;J)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    .prologue
    .line 707
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$1;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    iput-wide p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$1;->val$clubId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPartialRosterChangeSuccess(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 729
    .local p1, "successfulMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    .local p2, "failedMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    return-void
.end method

.method public onRosterChangeFailure(Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 721
    .local p1, "failedMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 722
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$1;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    const v1, 0x7f0700d6

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->showError(I)V

    .line 723
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$1;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->access$202(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Z)Z

    .line 724
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$1;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateBlockingIndicator()V

    .line 725
    return-void
.end method

.method public onRosterChangeSuccess(Ljava/util/List;)V
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 710
    .local p1, "successfulMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 711
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$1;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$1;->val$clubId:J

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->hideClubActivityFeed(J)V

    .line 712
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$1;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$1;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->getPeopleActivityFeedData()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->access$102(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/util/List;)Ljava/util/List;

    .line 713
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$1;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->access$202(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Z)Z

    .line 714
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$1;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->setShouldUpdateHiddenState(Z)V

    .line 715
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$1;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateAdapter()V

    .line 716
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$1;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateBlockingIndicator()V

    .line 717
    return-void
.end method
