.class public Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/PopupScreenAdapterBase;
.source "ProviderPickerDialogAdapter.java"


# instance fields
.field private final airingClickListener:Landroid/view/View$OnClickListener;

.field private final clickListener:Landroid/view/View$OnClickListener;

.field private final displayAirings:Z

.field private final imageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private mediaProviders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;",
            ">;"
        }
    .end annotation
.end field

.field private final msgText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final providerList:Landroid/widget/LinearLayout;

.field private final titleName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;Z)V
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;
    .param p2, "displayAirings"    # Z

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PopupScreenAdapterBase;-><init>()V

    .line 41
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->mediaProviders:Ljava/util/ArrayList;

    .line 148
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->clickListener:Landroid/view/View$OnClickListener;

    .line 158
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter$3;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->airingClickListener:Landroid/view/View$OnClickListener;

    .line 50
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;

    .line 51
    iput-boolean p2, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->displayAirings:Z

    .line 52
    const v1, 0x7f0e06df

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->titleName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 53
    const v1, 0x7f0e06e0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->msgText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 54
    const v1, 0x7f0e06dc

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 55
    const v1, 0x7f0e06e2

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->providerList:Landroid/widget/LinearLayout;

    .line 58
    const v1, 0x7f0e06d9

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 59
    .local v0, "closeButton":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 60
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;

    return-object v0
.end method

.method private addAiring(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;)V
    .locals 4
    .param p1, "airingData"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;

    .prologue
    .line 134
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v2}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 135
    .local v1, "vi":Landroid/view/LayoutInflater;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->providerList:Landroid/widget/LinearLayout;

    invoke-static {v1, v2, p1}, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->createAndBindAiringRecord(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;)Landroid/view/View;

    move-result-object v0

    .line 137
    .local v0, "airingButton":Landroid/view/View;
    if-nez v0, :cond_0

    .line 138
    const-string v2, "ProviderPickerDialog"

    const-string v3, "received null airing button, ignoring"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :goto_0
    return-void

    .line 142
    :cond_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 143
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->airingClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->providerList:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private addProvider(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)V
    .locals 4
    .param p1, "providerData"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    .prologue
    .line 120
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v2}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 121
    .local v1, "vi":Landroid/view/LayoutInflater;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->providerList:Landroid/widget/LinearLayout;

    invoke-static {v1, v2, p1}, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->createAndBindProviderRecord(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)Landroid/view/View;

    move-result-object v0

    .line 122
    .local v0, "providerButton":Landroid/view/View;
    if-nez v0, :cond_0

    .line 123
    const-string v2, "ProviderPickerDialog"

    const-string v3, "get null, just ignore for provider"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    :goto_0
    return-void

    .line 127
    :cond_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 128
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->providerList:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public static createAndBindAiringRecord(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;)Landroid/view/View;
    .locals 12
    .param p0, "inflater"    # Landroid/view/LayoutInflater;
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "airingData"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;

    .prologue
    const v9, 0x7f020064

    const/16 v11, 0x8

    const/4 v10, 0x0

    .line 208
    const v7, 0x7f030036

    invoke-virtual {p0, v7, p1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 210
    .local v0, "airingButton":Landroid/view/View;
    const v7, 0x7f0e019d

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 211
    .local v5, "tileView":Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    const v7, 0x7f0e019e

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 212
    .local v2, "channelTextView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    const v7, 0x7f0e019f

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 213
    .local v1, "channelNumberView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    const v7, 0x7f0e01a1

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 214
    .local v6, "titleTextView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    const v7, 0x7f0e01a2

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 217
    .local v4, "providerTextView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    iget-object v7, p2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->ChannelName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    iget-object v7, p2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->ChannelNumber:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 221
    invoke-virtual {v1, v10}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 222
    iget-object v7, p2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->ChannelNumber:Ljava/lang/String;

    invoke-virtual {v1, v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    :goto_0
    iget-object v7, p2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->HeadendId:Ljava/lang/String;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProvider(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v3

    .line 229
    .local v3, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProviders()Ljava/util/HashMap;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v7

    const/4 v8, 0x1

    if-le v7, v8, :cond_1

    if-eqz v3, :cond_1

    .line 230
    invoke-virtual {v4, v10}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 231
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getProviderName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 237
    :goto_1
    iget-object v7, p2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->Images:Ljava/util/ArrayList;

    if-eqz v7, :cond_2

    iget-object v7, p2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->Images:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_2

    .line 238
    iget-object v7, p2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->Images:Ljava/util/ArrayList;

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->getUrl()Ljava/lang/String;

    move-result-object v7

    sget v8, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    sget v9, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    invoke-virtual {v5, v7, v8, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageURI2(Ljava/lang/String;II)V

    .line 239
    invoke-virtual {v2, v11}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 240
    invoke-virtual {v5, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setVisibility(I)V

    .line 247
    :goto_2
    return-object v0

    .line 224
    .end local v3    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    :cond_0
    invoke-virtual {v1, v11}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_0

    .line 233
    .restart local v3    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    :cond_1
    invoke-virtual {v4, v11}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_1

    .line 242
    :cond_2
    const/4 v7, 0x0

    invoke-virtual {v5, v7, v9, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageURI2(Ljava/lang/String;II)V

    .line 243
    invoke-virtual {v5, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setVisibility(I)V

    .line 244
    invoke-virtual {v2, v10}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_2
.end method

.method public static createAndBindProviderRecord(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)Landroid/view/View;
    .locals 9
    .param p0, "inflater"    # Landroid/view/LayoutInflater;
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "providerData"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    .prologue
    const/4 v3, 0x0

    const v8, 0x7f020064

    .line 179
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-gtz v4, :cond_1

    .line 180
    const-string v4, "ProviderPickerDialog"

    const-string v5, "pass in invalid provide"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v3

    .line 204
    :cond_0
    :goto_0
    return-object v0

    .line 184
    :cond_1
    const v4, 0x7f0301db

    const/4 v5, 0x0

    invoke-virtual {p0, v4, p1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 186
    .local v0, "providerButton":Landroid/view/View;
    const v4, 0x7f0e0989

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 187
    .local v2, "tileView":Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;
    const v4, 0x7f0e098b

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 189
    .local v1, "textView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    if-eqz v2, :cond_0

    .line 191
    if-eqz v1, :cond_2

    .line 192
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    :cond_2
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getImageUrl()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 196
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getImageUrl()Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    sget v5, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    invoke-virtual {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageURI2(Ljava/lang/String;II)V

    goto :goto_0

    .line 197
    :cond_3
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getIsXboxMusic()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 198
    const v3, 0x7f020216

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageResource(I)V

    goto :goto_0

    .line 200
    :cond_4
    invoke-virtual {v2, v3, v8, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageURI2(Ljava/lang/String;II)V

    goto :goto_0
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 12

    .prologue
    .line 71
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->titleName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->getTitle()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 72
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;

    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->getTitleImagePlaceholderRid()I

    move-result v8

    .line 73
    .local v8, "rid":I
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->getTitleImageUrl()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10, v8, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 75
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;

    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->getProviders()Ljava/util/ArrayList;

    move-result-object v7

    .line 76
    .local v7, "providers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;>;"
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->mediaProviders:Ljava/util/ArrayList;

    if-eq v9, v7, :cond_1

    .line 77
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->providerList:Landroid/widget/LinearLayout;

    if-eqz v9, :cond_0

    .line 78
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->providerList:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 81
    :cond_0
    iput-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->mediaProviders:Ljava/util/ArrayList;

    .line 83
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->mediaProviders:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    .line 84
    .local v6, "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    invoke-direct {p0, v6}, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->addProvider(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)V

    goto :goto_0

    .line 88
    .end local v6    # "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    :cond_1
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->mediaProviders:Ljava/util/ArrayList;

    invoke-static {v9}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    .line 90
    .local v4, "displayErrorMessage":Z
    iget-boolean v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->displayAirings:Z

    if-eqz v9, :cond_5

    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getPremiumLiveTVEnabled()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 91
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;

    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->getAirings()Ljava/util/ArrayList;

    move-result-object v1

    .line 92
    .local v1, "airings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 94
    .local v2, "currentMediaAirings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;>;"
    if-eqz v1, :cond_4

    .line 95
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    .line 97
    .local v3, "currentTime":Ljava/util/Date;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v5, v9, :cond_3

    .line 98
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;

    iget-object v9, v9, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->StartTime:Ljava/util/Date;

    invoke-virtual {v9, v3}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;

    iget-object v9, v9, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->EndTime:Ljava/util/Date;

    invoke-virtual {v9, v3}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 99
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 103
    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-lez v9, :cond_4

    .line 104
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;

    .line 105
    .local v0, "airing":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->addAiring(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;)V

    goto :goto_2

    .line 109
    .end local v0    # "airing":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;
    .end local v3    # "currentTime":Ljava/util/Date;
    .end local v5    # "i":I
    :cond_4
    if-eqz v4, :cond_6

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v9

    if-eqz v9, :cond_6

    const/4 v4, 0x1

    .line 112
    .end local v1    # "airings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;>;"
    .end local v2    # "currentMediaAirings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;>;"
    :cond_5
    :goto_3
    if-eqz v4, :cond_7

    .line 113
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->msgText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->msgText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;

    invoke-virtual {v11}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->getErrorRid()I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 117
    :goto_4
    return-void

    .line 109
    .restart local v1    # "airings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;>;"
    .restart local v2    # "currentMediaAirings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;>;"
    :cond_6
    const/4 v4, 0x0

    goto :goto_3

    .line 115
    .end local v1    # "airings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;>;"
    .end local v2    # "currentMediaAirings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;>;"
    :cond_7
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->msgText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->msgText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/adapter/ProviderPickerDialogAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;

    invoke-virtual {v11}, Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel;->getMessageRid()I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto :goto_4
.end method
