.class public Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;
.super Ljava/lang/Object;
.source "HoverChatHeadPresenter.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final chatHead:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;

.field private final chatHeadKey:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;

.field private final interactor:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor;

.field private final stateRenderDisposable:Lio/reactivex/disposables/Disposable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V
    .locals 18
    .param p1, "chatHead"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;
    .param p2, "repository"    # Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;
    .param p3, "interactor"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor;
    .param p4, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .prologue
    .line 45
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 47
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;->chatHead:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;

    .line 48
    invoke-interface/range {p1 .. p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;->getKey()Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;->chatHeadKey:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;

    .line 49
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;->interactor:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor;

    .line 53
    invoke-interface/range {p2 .. p2}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->getChatHeadManagerEvents()Lio/reactivex/Observable;

    move-result-object v13

    const-class v14, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;

    .line 54
    invoke-virtual {v13, v14}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v13

    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;)Lio/reactivex/functions/Function;

    move-result-object v14

    .line 55
    invoke-virtual {v13, v14}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v7

    .line 63
    .local v7, "maximizedChangedObservable":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$MaximizedChangedResult;>;"
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;->chatHead:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;

    invoke-interface {v13}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;->getXChangedEvents()Lio/reactivex/Observable;

    move-result-object v13

    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;)Lio/reactivex/functions/Function;

    move-result-object v14

    .line 64
    invoke-virtual {v13, v14}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v13

    .line 65
    invoke-virtual {v13}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v13

    invoke-static {}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v14

    .line 66
    invoke-virtual {v13, v14}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v10

    .line 70
    .local v10, "positionChangedObservable":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$PositionChangedResult;>;"
    invoke-interface/range {p2 .. p2}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->getClubChatKeys()Lio/reactivex/Observable;

    move-result-object v13

    .line 71
    invoke-interface/range {p2 .. p2}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->getMessageKeys()Lio/reactivex/Observable;

    move-result-object v14

    .line 69
    invoke-static {v13, v14}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v13

    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;)Lio/reactivex/functions/Predicate;

    move-result-object v14

    .line 72
    invoke-virtual {v13, v14}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v13

    const/4 v14, 0x1

    .line 73
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-static {}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$5;->lambdaFactory$()Lio/reactivex/functions/BiFunction;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Lio/reactivex/Observable;->scan(Ljava/lang/Object;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v13

    const-wide/16 v14, 0x1

    .line 74
    invoke-virtual {v13, v14, v15}, Lio/reactivex/Observable;->skip(J)Lio/reactivex/Observable;

    move-result-object v8

    .line 77
    .local v8, "newMessageEvents":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Ljava/lang/Integer;>;"
    invoke-interface/range {p2 .. p2}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->getChatHeadLifecycleEvents()Lio/reactivex/Observable;

    move-result-object v13

    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;)Lio/reactivex/functions/Predicate;

    move-result-object v14

    .line 78
    invoke-virtual {v13, v14}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v13

    .line 79
    invoke-virtual {v13}, Lio/reactivex/Observable;->share()Lio/reactivex/Observable;

    move-result-object v6

    .line 83
    .local v6, "lifecycleEvents":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadLifecycleEvent<Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;>;>;"
    const-class v13, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadDetachedEvent;

    .line 84
    invoke-virtual {v6, v13}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v13

    invoke-static {}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$7;->lambdaFactory$()Lio/reactivex/functions/BiFunction;

    move-result-object v14

    .line 83
    invoke-static {v13, v8, v14}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v13

    .line 87
    invoke-virtual {v13}, Lio/reactivex/Observable;->distinct()Lio/reactivex/Observable;

    move-result-object v13

    invoke-static {}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$8;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v14

    .line 88
    invoke-virtual {v13, v14}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v13

    sget-object v14, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ShowBadgeResult;->INSTANCE:Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ShowBadgeResult;

    .line 89
    invoke-virtual {v13, v14}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v12

    .line 93
    .local v12, "showBadgeObservable":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ShowBadgeResult;>;"
    const-class v13, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadAttachedEvent;

    .line 94
    invoke-virtual {v6, v13}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v13

    invoke-static {}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$9;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v14

    .line 95
    invoke-virtual {v13, v14}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 98
    .local v2, "hideBadgeObservable":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$HideBadgeResult;>;"
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;->interactor:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor;

    invoke-virtual {v13}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor;->loadImageUrls()Lio/reactivex/Single;

    move-result-object v13

    .line 99
    invoke-virtual {v13}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v13

    .line 100
    invoke-interface/range {p4 .. p4}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->io()Lio/reactivex/Scheduler;

    move-result-object v14

    invoke-virtual {v13, v14}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v3

    .line 102
    .local v3, "imagesDownloadedResultObservable":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ImagesDownloadedResult;>;"
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;->interactor:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor;

    invoke-virtual {v13}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadInteractor;->getParticipants()Lio/reactivex/Single;

    move-result-object v13

    .line 103
    invoke-virtual {v13}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v13

    .line 104
    invoke-interface/range {p4 .. p4}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->io()Lio/reactivex/Scheduler;

    move-result-object v14

    invoke-virtual {v13, v14}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v9

    .line 107
    .local v9, "participantsObservable":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ParticipantsResult;>;"
    const/4 v13, 0x6

    new-array v13, v13, [Lio/reactivex/ObservableSource;

    const/4 v14, 0x0

    aput-object v12, v13, v14

    const/4 v14, 0x1

    aput-object v2, v13, v14

    const/4 v14, 0x2

    aput-object v10, v13, v14

    const/4 v14, 0x3

    aput-object v7, v13, v14

    const/4 v14, 0x4

    aput-object v3, v13, v14

    const/4 v14, 0x5

    aput-object v9, v13, v14

    invoke-static {v13}, Lio/reactivex/Observable;->mergeArray([Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v11

    .line 115
    .local v11, "results":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;>;"
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;->getChatHeadBadgePosition()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    move-result-object v4

    .line 117
    .local v4, "initialPosition":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;
    const/4 v13, 0x0

    .line 118
    invoke-static {v13, v4}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->with(ZLcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;

    move-result-object v13

    const/4 v14, 0x0

    .line 120
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v15

    .line 121
    invoke-interface/range {p2 .. p2}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->hoverChatIsOpen()Z

    move-result v16

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;->getInitialShape(Z)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;

    move-result-object v16

    .line 122
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v17

    .line 117
    invoke-static/range {v13 .. v17}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->with(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;ZLjava/util/List;Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;Ljava/util/List;)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;

    move-result-object v5

    .line 124
    .local v5, "initialState":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;
    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;)Lio/reactivex/functions/BiFunction;

    move-result-object v13

    invoke-virtual {v11, v5, v13}, Lio/reactivex/Observable;->scan(Ljava/lang/Object;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v13

    invoke-static {}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$11;->lambdaFactory$()Lio/reactivex/functions/BiPredicate;

    move-result-object v14

    .line 125
    invoke-virtual {v13, v14}, Lio/reactivex/Observable;->distinctUntilChanged(Lio/reactivex/functions/BiPredicate;)Lio/reactivex/Observable;

    move-result-object v13

    .line 126
    invoke-interface/range {p4 .. p4}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v14

    invoke-virtual {v13, v14}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;->chatHead:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;

    invoke-virtual {v14}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v14}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$12;->lambdaFactory$(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;)Lio/reactivex/functions/Consumer;

    move-result-object v14

    .line 127
    invoke-virtual {v13, v14}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;->stateRenderDisposable:Lio/reactivex/disposables/Disposable;

    .line 129
    const-class v13, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadRemovedEvent;

    invoke-virtual {v6, v13}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v13

    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$13;->lambdaFactory$(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;)Lio/reactivex/functions/Consumer;

    move-result-object v14

    .line 130
    invoke-virtual {v13, v14}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 131
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;->stateReducer(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;

    move-result-object v0

    return-object v0
.end method

.method private getChatHeadBadgePosition()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;
    .locals 6

    .prologue
    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;->chatHead:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;

    invoke-interface {v0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;->getX()D

    move-result-wide v0

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;->chatHead:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;

    invoke-interface {v2}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadView;->getParentX()D

    move-result-wide v2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;->Left:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;->Right:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    goto :goto_0
.end method

.method private getInitialShape(Z)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;
    .locals 1
    .param p1, "isOpen"    # Z

    .prologue
    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;->chatHeadKey:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;

    instance-of v0, v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 141
    sget-object v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;->Square:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;

    .line 143
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;->Circle:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;

    goto :goto_0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;)Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$MaximizedChangedResult;
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;
    .param p1, "event"    # Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 56
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadArrangementChangedEvent;->newArrangement()Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    move-result-object v0

    .line 57
    .local v0, "newArrangement":Lcom/flipkart/chatheads/ui/ChatHeadArrangement;
    if-eqz v0, :cond_0

    .line 58
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/flipkart/chatheads/ui/MaximizedArrangement;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 59
    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;->getChatHeadBadgePosition()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    move-result-object v2

    .line 57
    invoke-static {v1, v2}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$MaximizedChangedResult;->with(ZLcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;)Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$MaximizedChangedResult;

    move-result-object v1

    return-object v1

    .line 58
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;Ljava/lang/Object;)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;
    .param p1, "ignore"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;->getChatHeadBadgePosition()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)Z
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;
    .param p1, "k"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;->chatHeadKey:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic lambda$new$3(Ljava/lang/Integer;Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)Ljava/lang/Integer;
    .locals 1
    .param p0, "count"    # Ljava/lang/Integer;
    .param p1, "baseKey"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 73
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$new$4(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadLifecycleEvent;)Z
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;
    .param p1, "event"    # Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadLifecycleEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p1, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadLifecycleEvent;->key:Ljava/io/Serializable;

    check-cast v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;->chatHeadKey:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic lambda$new$5(Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadDetachedEvent;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0
    .param p0, "e"    # Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadDetachedEvent;
    .param p1, "count"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 86
    return-object p1
.end method

.method static synthetic lambda$new$6(Ljava/lang/Integer;)Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ShowBadgeResult;
    .locals 1
    .param p0, "p"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 88
    sget-object v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ShowBadgeResult;->INSTANCE:Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ShowBadgeResult;

    return-object v0
.end method

.method static synthetic lambda$new$7(Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadAttachedEvent;)Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$HideBadgeResult;
    .locals 1
    .param p0, "ignore"    # Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadAttachedEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 95
    sget-object v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$HideBadgeResult;->INSTANCE:Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$HideBadgeResult;

    return-object v0
.end method

.method static synthetic lambda$new$8(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadRemovedEvent;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;
    .param p1, "ignore"    # Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadRemovedEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;->stateRenderDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    return-void
.end method

.method private stateReducer(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;
    .locals 12
    .param p1, "previousState"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;
    .param p2, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;

    .prologue
    .line 148
    sget-object v8, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "stateReducer with result: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {p2}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;->toLogString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->badgeViewState()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;

    move-result-object v7

    .line 152
    .local v7, "previousBadgeViewState":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;
    instance-of v8, p2, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ShowBadgeResult;

    if-eqz v8, :cond_1

    invoke-virtual {v7}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->isHidden()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 153
    const/4 v8, 0x0

    invoke-virtual {v7}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->position()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->with(ZLcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;

    move-result-object v3

    .line 154
    .local v3, "newBadgeViewState":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->isMaximized()Z

    move-result v8

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->imageUrls()Lcom/google/common/collect/ImmutableList;

    move-result-object v9

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->shape()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;

    move-result-object v10

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->participants()Lcom/google/common/collect/ImmutableList;

    move-result-object v11

    invoke-static {v3, v8, v9, v10, v11}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->with(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;ZLjava/util/List;Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;Ljava/util/List;)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;

    move-result-object p1

    .line 185
    .end local v3    # "newBadgeViewState":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;
    .end local p1    # "previousState":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;
    :cond_0
    :goto_0
    return-object p1

    .line 156
    .restart local p1    # "previousState":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;
    :cond_1
    instance-of v8, p2, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$HideBadgeResult;

    if-eqz v8, :cond_2

    invoke-virtual {v7}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->isHidden()Z

    move-result v8

    if-nez v8, :cond_2

    .line 157
    const/4 v8, 0x1

    invoke-virtual {v7}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->position()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->with(ZLcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;

    move-result-object v3

    .line 158
    .restart local v3    # "newBadgeViewState":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->isMaximized()Z

    move-result v8

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->imageUrls()Lcom/google/common/collect/ImmutableList;

    move-result-object v9

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->shape()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;

    move-result-object v10

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->participants()Lcom/google/common/collect/ImmutableList;

    move-result-object v11

    invoke-static {v3, v8, v9, v10, v11}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->with(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;ZLjava/util/List;Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;Ljava/util/List;)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;

    move-result-object p1

    goto :goto_0

    .line 160
    .end local v3    # "newBadgeViewState":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;
    :cond_2
    instance-of v8, p2, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$PositionChangedResult;

    if-eqz v8, :cond_4

    move-object v6, p2

    .line 161
    check-cast v6, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$PositionChangedResult;

    .line 162
    .local v6, "positionChangedResult":Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$PositionChangedResult;
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->isMaximized()Z

    move-result v8

    if-eqz v8, :cond_3

    sget-object v4, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;->Left:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    .line 163
    .local v4, "newPosition":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;
    :goto_1
    invoke-virtual {v7}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->isHidden()Z

    move-result v8

    invoke-static {v8, v4}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->with(ZLcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;

    move-result-object v3

    .line 164
    .restart local v3    # "newBadgeViewState":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->isMaximized()Z

    move-result v8

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->imageUrls()Lcom/google/common/collect/ImmutableList;

    move-result-object v9

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->shape()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;

    move-result-object v10

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->participants()Lcom/google/common/collect/ImmutableList;

    move-result-object v11

    invoke-static {v3, v8, v9, v10, v11}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->with(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;ZLjava/util/List;Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;Ljava/util/List;)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;

    move-result-object p1

    goto :goto_0

    .line 162
    .end local v3    # "newBadgeViewState":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;
    .end local v4    # "newPosition":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;
    :cond_3
    invoke-virtual {v6}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$PositionChangedResult;->newPosition()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    move-result-object v4

    goto :goto_1

    .line 166
    .end local v6    # "positionChangedResult":Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$PositionChangedResult;
    :cond_4
    instance-of v8, p2, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$MaximizedChangedResult;

    if-eqz v8, :cond_8

    move-object v2, p2

    .line 167
    check-cast v2, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$MaximizedChangedResult;

    .line 169
    .local v2, "maximizedChangedResult":Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$MaximizedChangedResult;
    invoke-virtual {v2}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$MaximizedChangedResult;->isMaximized()Z

    move-result v8

    if-eqz v8, :cond_5

    sget-object v4, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;->Left:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    .line 170
    .restart local v4    # "newPosition":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;
    :goto_2
    invoke-virtual {v2}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$MaximizedChangedResult;->isMaximized()Z

    move-result v8

    if-eqz v8, :cond_7

    iget-object v8, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;->chatHeadKey:Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;

    instance-of v8, v8, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    if-eqz v8, :cond_6

    sget-object v5, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;->Square:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;

    .line 174
    .local v5, "newShape":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;
    :goto_3
    invoke-virtual {v7}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->isHidden()Z

    move-result v8

    invoke-static {v8, v4}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->with(ZLcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;

    move-result-object v3

    .line 175
    .restart local v3    # "newBadgeViewState":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;
    invoke-virtual {v2}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$MaximizedChangedResult;->isMaximized()Z

    move-result v8

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->imageUrls()Lcom/google/common/collect/ImmutableList;

    move-result-object v9

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->participants()Lcom/google/common/collect/ImmutableList;

    move-result-object v10

    invoke-static {v3, v8, v9, v5, v10}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->with(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;ZLjava/util/List;Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;Ljava/util/List;)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;

    move-result-object p1

    goto/16 :goto_0

    .line 169
    .end local v3    # "newBadgeViewState":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;
    .end local v4    # "newPosition":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;
    .end local v5    # "newShape":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;
    :cond_5
    invoke-virtual {v2}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$MaximizedChangedResult;->position()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;

    move-result-object v4

    goto :goto_2

    .line 170
    .restart local v4    # "newPosition":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;
    :cond_6
    sget-object v5, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;->Circle:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;

    goto :goto_3

    :cond_7
    sget-object v5, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;->Circle:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;

    goto :goto_3

    .line 177
    .end local v2    # "maximizedChangedResult":Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$MaximizedChangedResult;
    .end local v4    # "newPosition":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;
    :cond_8
    instance-of v8, p2, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ImagesDownloadedResult;

    if-eqz v8, :cond_9

    move-object v1, p2

    .line 178
    check-cast v1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ImagesDownloadedResult;

    .line 179
    .local v1, "imagesDownloadedResult":Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ImagesDownloadedResult;
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->isMaximized()Z

    move-result v8

    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ImagesDownloadedResult;->imageUrls()Lcom/google/common/collect/ImmutableList;

    move-result-object v9

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->shape()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;

    move-result-object v10

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->participants()Lcom/google/common/collect/ImmutableList;

    move-result-object v11

    invoke-static {v7, v8, v9, v10, v11}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->with(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;ZLjava/util/List;Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;Ljava/util/List;)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;

    move-result-object p1

    goto/16 :goto_0

    .line 180
    .end local v1    # "imagesDownloadedResult":Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ImagesDownloadedResult;
    :cond_9
    instance-of v8, p2, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ParticipantsResult;

    if-eqz v8, :cond_0

    move-object v0, p2

    .line 181
    check-cast v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ParticipantsResult;

    .line 182
    .local v0, "ParticipantsResultResult":Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ParticipantsResult;
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->isMaximized()Z

    move-result v8

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->imageUrls()Lcom/google/common/collect/ImmutableList;

    move-result-object v9

    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->shape()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;

    move-result-object v10

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$ParticipantsResult;->participants()Lcom/google/common/collect/ImmutableList;

    move-result-object v11

    invoke-static {v7, v8, v9, v10, v11}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->with(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;ZLjava/util/List;Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;Ljava/util/List;)Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;

    move-result-object p1

    goto/16 :goto_0
.end method
