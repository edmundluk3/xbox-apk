.class public Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;
.super Ljava/lang/Object;
.source "ApplicationMetrics.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;
    }
.end annotation


# static fields
.field private static appMetrics:Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;


# instance fields
.field public final TAG:Ljava/lang/String;

.field private final appLifetime:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/toolkit/TimeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final companionConnectedLifetime:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/toolkit/TimeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final companionLifetime:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/toolkit/TimeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final connectedLifetime:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/toolkit/TimeMonitor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appMetrics:Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const-string v0, "ApplicationMetrics"

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->TAG:Ljava/lang/String;

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    .line 26
    return-void
.end method

.method private varargs Log(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 29
    const-string v0, "ApplicationMetrics"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v1, p1, p2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appMetrics:Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appMetrics:Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;

    .line 36
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appMetrics:Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;

    return-object v0
.end method

.method private getIsEnded(Ljava/util/ArrayList;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/toolkit/TimeMonitor;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 366
    .local p1, "monitors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/toolkit/TimeMonitor;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->getIsEnded()Z

    move-result v0

    return v0
.end method

.method private getIsStarted(Ljava/util/ArrayList;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/toolkit/TimeMonitor;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 362
    .local p1, "monitors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/toolkit/TimeMonitor;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->getIsStarted()Z

    move-result v0

    return v0
.end method

.method private getTotalTimeMs(Ljava/util/ArrayList;)J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/toolkit/TimeMonitor;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 348
    .local p1, "monitors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/toolkit/TimeMonitor;>;"
    const-wide/16 v2, 0x0

    .line 349
    .local v2, "result":J
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    .line 350
    .local v0, "monitor":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->getElapsedMs()J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 351
    goto :goto_0

    .line 353
    .end local v0    # "monitor":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    :cond_0
    return-wide v2
.end method

.method private start(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/toolkit/TimeMonitor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 358
    .local p1, "monitors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/toolkit/TimeMonitor;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->start()V

    .line 359
    return-void
.end method


# virtual methods
.method public getElapsedMs(Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;)J
    .locals 2
    .param p1, "metric"    # Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;

    .prologue
    .line 150
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getElapsedMs(Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;Z)J

    move-result-wide v0

    return-wide v0
.end method

.method public getElapsedMs(Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;Z)J
    .locals 5
    .param p1, "metric"    # Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;
    .param p2, "getMostRecent"    # Z

    .prologue
    const-wide/16 v0, 0x0

    .line 277
    const-string v2, "%s get elapsed"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-direct {p0, v2, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->Log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 278
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$1;->$SwitchMap$com$microsoft$xbox$service$network$managers$xblshared$ApplicationMetrics$Metric:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 317
    :goto_0
    return-wide v0

    .line 280
    :pswitch_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    monitor-enter v2

    .line 281
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 282
    monitor-exit v2

    goto :goto_0

    .line 285
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 284
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getTotalTimeMs(Ljava/util/ArrayList;)J

    move-result-wide v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 287
    :pswitch_1
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    monitor-enter v2

    .line 288
    :try_start_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 289
    monitor-exit v2

    goto :goto_0

    .line 295
    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 291
    :cond_1
    if-eqz p2, :cond_2

    .line 292
    :try_start_3
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->getElapsedMs()J

    move-result-wide v0

    monitor-exit v2

    goto :goto_0

    .line 294
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getTotalTimeMs(Ljava/util/ArrayList;)J

    move-result-wide v0

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 297
    :pswitch_2
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    monitor-enter v2

    .line 298
    :try_start_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 299
    monitor-exit v2

    goto :goto_0

    .line 305
    :catchall_2
    move-exception v0

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v0

    .line 301
    :cond_3
    if-eqz p2, :cond_4

    .line 302
    :try_start_5
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->getElapsedMs()J

    move-result-wide v0

    monitor-exit v2

    goto :goto_0

    .line 304
    :cond_4
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getTotalTimeMs(Ljava/util/ArrayList;)J

    move-result-wide v0

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_0

    .line 307
    :pswitch_3
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    monitor-enter v2

    .line 308
    :try_start_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 309
    monitor-exit v2

    goto/16 :goto_0

    .line 315
    :catchall_3
    move-exception v0

    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    throw v0

    .line 311
    :cond_5
    if-eqz p2, :cond_6

    .line 312
    :try_start_7
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->getElapsedMs()J

    move-result-wide v0

    monitor-exit v2

    goto/16 :goto_0

    .line 314
    :cond_6
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getTotalTimeMs(Ljava/util/ArrayList;)J

    move-result-wide v0

    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    goto/16 :goto_0

    .line 278
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getIsMetricEnded(Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;)Z
    .locals 3
    .param p1, "metric"    # Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;

    .prologue
    const/4 v0, 0x0

    .line 64
    const-string v1, "%s get ended"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v0

    invoke-direct {p0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->Log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 65
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$1;->$SwitchMap$com$microsoft$xbox$service$network$managers$xblshared$ApplicationMetrics$Metric:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 83
    :goto_0
    return v0

    .line 67
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    monitor-enter v1

    .line 68
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getIsEnded(Ljava/util/ArrayList;)Z

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 71
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    monitor-enter v1

    .line 72
    :try_start_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getIsEnded(Ljava/util/ArrayList;)Z

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 73
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    .line 75
    :pswitch_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    monitor-enter v1

    .line 76
    :try_start_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getIsEnded(Ljava/util/ArrayList;)Z

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 77
    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    throw v0

    .line 79
    :pswitch_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    monitor-enter v1

    .line 80
    :try_start_3
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getIsEnded(Ljava/util/ArrayList;)Z

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 81
    :catchall_3
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    throw v0

    .line 65
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getIsMetricStarted(Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;)Z
    .locals 3
    .param p1, "metric"    # Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;

    .prologue
    const/4 v0, 0x0

    .line 40
    const-string v1, "%s get started"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v0

    invoke-direct {p0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->Log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 41
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$1;->$SwitchMap$com$microsoft$xbox$service$network$managers$xblshared$ApplicationMetrics$Metric:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 60
    :goto_0
    return v0

    .line 43
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    monitor-enter v1

    .line 44
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getIsStarted(Ljava/util/ArrayList;)Z

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 47
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    monitor-enter v1

    .line 48
    :try_start_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getIsStarted(Ljava/util/ArrayList;)Z

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 49
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    .line 51
    :pswitch_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    monitor-enter v1

    .line 52
    :try_start_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getIsStarted(Ljava/util/ArrayList;)Z

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 53
    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    throw v0

    .line 56
    :pswitch_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    monitor-enter v1

    .line 57
    :try_start_3
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getIsStarted(Ljava/util/ArrayList;)Z

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 58
    :catchall_3
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    throw v0

    .line 41
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getIsStartedAndRunning(Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;)Z
    .locals 6
    .param p1, "metric"    # Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 236
    const-string v3, "%s isStartedAndRunning"

    new-array v4, v2, [Ljava/lang/Object;

    aput-object p1, v4, v1

    invoke-direct {p0, v3, v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->Log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 237
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$1;->$SwitchMap$com$microsoft$xbox$service$network$managers$xblshared$ApplicationMetrics$Metric:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 271
    :goto_0
    return v1

    .line 239
    :pswitch_0
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    monitor-enter v3

    .line 240
    :try_start_0
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 241
    monitor-exit v3

    goto :goto_0

    .line 245
    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 243
    :cond_0
    :try_start_1
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    .line 244
    .local v0, "monitor":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->getIsStarted()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->getIsEnded()Z

    move-result v4

    if-nez v4, :cond_1

    move v1, v2

    :cond_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 247
    .end local v0    # "monitor":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    :pswitch_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    monitor-enter v3

    .line 248
    :try_start_2
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 249
    monitor-exit v3

    goto :goto_0

    .line 253
    :catchall_1
    move-exception v1

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1

    .line 251
    :cond_2
    :try_start_3
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    .line 252
    .restart local v0    # "monitor":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->getIsStarted()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->getIsEnded()Z

    move-result v4

    if-nez v4, :cond_3

    move v1, v2

    :cond_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 255
    .end local v0    # "monitor":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    :pswitch_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    monitor-enter v3

    .line 256
    :try_start_4
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 257
    monitor-exit v3

    goto :goto_0

    .line 261
    :catchall_2
    move-exception v1

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v1

    .line 259
    :cond_4
    :try_start_5
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    .line 260
    .restart local v0    # "monitor":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->getIsStarted()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->getIsEnded()Z

    move-result v4

    if-nez v4, :cond_5

    move v1, v2

    :cond_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto/16 :goto_0

    .line 263
    .end local v0    # "monitor":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    :pswitch_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    monitor-enter v3

    .line 264
    :try_start_6
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 265
    monitor-exit v3

    goto/16 :goto_0

    .line 269
    :catchall_3
    move-exception v1

    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    throw v1

    .line 267
    :cond_6
    :try_start_7
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    .line 268
    .restart local v0    # "monitor":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->getIsStarted()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->getIsEnded()Z

    move-result v4

    if-nez v4, :cond_7

    move v1, v2

    :cond_7
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    goto/16 :goto_0

    .line 237
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public resetMetric(Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;)V
    .locals 3
    .param p1, "metric"    # Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;

    .prologue
    .line 321
    const-string v0, "%s reset"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->Log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 322
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$1;->$SwitchMap$com$microsoft$xbox$service$network$managers$xblshared$ApplicationMetrics$Metric:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 345
    :goto_0
    return-void

    .line 324
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    monitor-enter v1

    .line 325
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 326
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 329
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    monitor-enter v1

    .line 330
    :try_start_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 331
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    .line 334
    :pswitch_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    monitor-enter v1

    .line 335
    :try_start_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 336
    monitor-exit v1

    goto :goto_0

    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    throw v0

    .line 339
    :pswitch_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    monitor-enter v1

    .line 340
    :try_start_3
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 341
    monitor-exit v1

    goto :goto_0

    :catchall_3
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    throw v0

    .line 322
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public start(Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;)V
    .locals 4
    .param p1, "metric"    # Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;

    .prologue
    .line 87
    const-string v0, "%s started"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->Log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$1;->$SwitchMap$com$microsoft$xbox$service$network$managers$xblshared$ApplicationMetrics$Metric:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 147
    :goto_0
    return-void

    .line 90
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    monitor-enter v1

    .line 91
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    new-instance v2, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-direct {v2}, Lcom/microsoft/xbox/toolkit/TimeMonitor;-><init>()V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->start(Ljava/util/ArrayList;)V

    .line 95
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 98
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    monitor-enter v1

    .line 99
    :try_start_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    new-instance v2, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-direct {v2}, Lcom/microsoft/xbox/toolkit/TimeMonitor;-><init>()V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 105
    :goto_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->start(Ljava/util/ArrayList;)V

    .line 106
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    .line 102
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->stop()V

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    new-instance v2, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-direct {v2}, Lcom/microsoft/xbox/toolkit/TimeMonitor;-><init>()V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    .line 109
    :pswitch_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    monitor-enter v1

    .line 110
    :try_start_3
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    new-instance v2, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-direct {v2}, Lcom/microsoft/xbox/toolkit/TimeMonitor;-><init>()V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    :goto_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->start(Ljava/util/ArrayList;)V

    .line 117
    monitor-exit v1

    goto :goto_0

    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v0

    .line 113
    :cond_2
    :try_start_4
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->stop()V

    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    new-instance v2, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-direct {v2}, Lcom/microsoft/xbox/toolkit/TimeMonitor;-><init>()V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_2

    .line 120
    :pswitch_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    monitor-enter v1

    .line 121
    :try_start_5
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    monitor-enter v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 122
    :try_start_6
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 124
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    :try_start_7
    monitor-exit v1

    goto/16 :goto_0

    .line 144
    :catchall_3
    move-exception v0

    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v0

    .line 125
    :cond_3
    :try_start_8
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->getIsStarted()Z

    move-result v0

    if-nez v0, :cond_4

    .line 126
    monitor-exit v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    :try_start_9
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    goto/16 :goto_0

    .line 127
    :cond_4
    :try_start_a
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->getIsEnded()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 128
    monitor-exit v2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    :try_start_b
    monitor-exit v1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    goto/16 :goto_0

    .line 130
    :cond_5
    :try_start_c
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    new-instance v3, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-direct {v3}, Lcom/microsoft/xbox/toolkit/TimeMonitor;-><init>()V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    :goto_3
    monitor-exit v2
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 143
    :try_start_d
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->start(Ljava/util/ArrayList;)V

    .line 144
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    goto/16 :goto_0

    .line 132
    :cond_6
    :try_start_e
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 133
    monitor-exit v2
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    :try_start_f
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    goto/16 :goto_0

    .line 134
    :cond_7
    :try_start_10
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->getIsStarted()Z

    move-result v0

    if-nez v0, :cond_8

    .line 135
    monitor-exit v2
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    :try_start_11
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    goto/16 :goto_0

    .line 136
    :cond_8
    :try_start_12
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->getIsEnded()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 137
    monitor-exit v2
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_4

    :try_start_13
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_3

    goto/16 :goto_0

    .line 139
    :cond_9
    :try_start_14
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->stop()V

    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    new-instance v3, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-direct {v3}, Lcom/microsoft/xbox/toolkit/TimeMonitor;-><init>()V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 142
    :catchall_4
    move-exception v0

    monitor-exit v2
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_4

    :try_start_15
    throw v0
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_3

    .line 88
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public stopMetric(Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;)V
    .locals 3
    .param p1, "metric"    # Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;

    .prologue
    .line 198
    const-string v0, "%s stopped"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->Log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 199
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$1;->$SwitchMap$com$microsoft$xbox$service$network$managers$xblshared$ApplicationMetrics$Metric:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 233
    :goto_0
    return-void

    .line 201
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    monitor-enter v1

    .line 202
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    monitor-exit v1

    goto :goto_0

    .line 206
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 205
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->stop()V

    .line 206
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 209
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    monitor-enter v1

    .line 210
    :try_start_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 211
    monitor-exit v1

    goto :goto_0

    .line 214
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 213
    :cond_1
    :try_start_3
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->stop()V

    .line 214
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 217
    :pswitch_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    monitor-enter v1

    .line 218
    :try_start_4
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 219
    monitor-exit v1

    goto :goto_0

    .line 222
    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v0

    .line 221
    :cond_2
    :try_start_5
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->stop()V

    .line 222
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_0

    .line 225
    :pswitch_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    monitor-enter v1

    .line 226
    :try_start_6
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 227
    monitor-exit v1

    goto :goto_0

    .line 230
    :catchall_3
    move-exception v0

    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    throw v0

    .line 229
    :cond_3
    :try_start_7
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->stop()V

    .line 230
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    goto/16 :goto_0

    .line 199
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public stopMetricAndGetElapsedMs(Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;)J
    .locals 6
    .param p1, "metric"    # Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;

    .prologue
    const-wide/16 v2, 0x0

    .line 154
    const-string v1, "%s stopMetricAndGetElapsedMs"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-direct {p0, v1, v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->Log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 155
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$1;->$SwitchMap$com$microsoft$xbox$service$network$managers$xblshared$ApplicationMetrics$Metric:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;->ordinal()I

    move-result v4

    aget v1, v1, v4

    packed-switch v1, :pswitch_data_0

    .line 193
    :goto_0
    return-wide v2

    .line 157
    :pswitch_0
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    monitor-enter v4

    .line 158
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 159
    monitor-exit v4

    goto :goto_0

    .line 164
    :catchall_0
    move-exception v1

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 161
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->appLifetime:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    .line 162
    .local v0, "monitor":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->stop()V

    .line 163
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->getElapsedMs()J

    move-result-wide v2

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 166
    .end local v0    # "monitor":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    :pswitch_1
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    monitor-enter v4

    .line 167
    :try_start_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 168
    monitor-exit v4

    goto :goto_0

    .line 173
    :catchall_1
    move-exception v1

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1

    .line 170
    :cond_1
    :try_start_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->connectedLifetime:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    .line 171
    .restart local v0    # "monitor":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->stop()V

    .line 172
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->getElapsedMs()J

    move-result-wide v2

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 175
    .end local v0    # "monitor":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    :pswitch_2
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    monitor-enter v4

    .line 176
    :try_start_4
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 177
    monitor-exit v4

    goto :goto_0

    .line 182
    :catchall_2
    move-exception v1

    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v1

    .line 179
    :cond_2
    :try_start_5
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionLifetime:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    .line 180
    .restart local v0    # "monitor":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->stop()V

    .line 181
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->getElapsedMs()J

    move-result-wide v2

    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_0

    .line 184
    .end local v0    # "monitor":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    :pswitch_3
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    monitor-enter v4

    .line 185
    :try_start_6
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 186
    monitor-exit v4

    goto/16 :goto_0

    .line 191
    :catchall_3
    move-exception v1

    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    throw v1

    .line 188
    :cond_3
    :try_start_7
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->companionConnectedLifetime:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    .line 189
    .restart local v0    # "monitor":Lcom/microsoft/xbox/toolkit/TimeMonitor;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->stop()V

    .line 190
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->getElapsedMs()J

    move-result-wide v2

    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    goto/16 :goto_0

    .line 155
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
