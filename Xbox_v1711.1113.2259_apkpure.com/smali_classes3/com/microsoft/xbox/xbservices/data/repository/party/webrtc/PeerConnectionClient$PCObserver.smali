.class Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;
.super Ljava/lang/Object;
.source "PeerConnectionClient.java"

# interfaces
.implements Lorg/webrtc/PeerConnection$Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PCObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)V
    .locals 0

    .prologue
    .line 820
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;
    .param p2, "x1"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$1;

    .prologue
    .line 820
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)V

    return-void
.end method


# virtual methods
.method public onAddStream(Lorg/webrtc/MediaStream;)V
    .locals 3
    .param p1, "stream"    # Lorg/webrtc/MediaStream;

    .prologue
    .line 869
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$800(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v0

    const-string v1, "PCRTCClient"

    const-string v2, "onAddStream"

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 870
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$1300(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$4;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$4;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;Lorg/webrtc/MediaStream;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 886
    return-void
.end method

.method public onAddTrack(Lorg/webrtc/RtpReceiver;[Lorg/webrtc/MediaStream;)V
    .locals 0
    .param p1, "receiver"    # Lorg/webrtc/RtpReceiver;
    .param p2, "mediaStreams"    # [Lorg/webrtc/MediaStream;

    .prologue
    .line 925
    return-void
.end method

.method public onDataChannel(Lorg/webrtc/DataChannel;)V
    .locals 4
    .param p1, "dc"    # Lorg/webrtc/DataChannel;

    .prologue
    .line 894
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$800(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v0

    const-string v1, "PCRTCClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "New Data channel "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lorg/webrtc/DataChannel;->label()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 895
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$2600(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 918
    :goto_0
    return-void

    .line 897
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$5;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$5;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;Lorg/webrtc/DataChannel;)V

    invoke-virtual {p1, v0}, Lorg/webrtc/DataChannel;->registerObserver(Lorg/webrtc/DataChannel$Observer;)V

    goto :goto_0
.end method

.method public onIceCandidate(Lorg/webrtc/IceCandidate;)V
    .locals 2
    .param p1, "candidate"    # Lorg/webrtc/IceCandidate;

    .prologue
    .line 823
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$1300(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$1;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$1;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;Lorg/webrtc/IceCandidate;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 829
    return-void
.end method

.method public onIceCandidatesRemoved([Lorg/webrtc/IceCandidate;)V
    .locals 2
    .param p1, "candidates"    # [Lorg/webrtc/IceCandidate;

    .prologue
    .line 832
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$1300(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$2;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$2;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;[Lorg/webrtc/IceCandidate;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 838
    return-void
.end method

.method public onIceConnectionChange(Lorg/webrtc/PeerConnection$IceConnectionState;)V
    .locals 2
    .param p1, "newState"    # Lorg/webrtc/PeerConnection$IceConnectionState;

    .prologue
    .line 845
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$1300(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$3;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver$3;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;Lorg/webrtc/PeerConnection$IceConnectionState;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 858
    return-void
.end method

.method public onIceConnectionReceivingChange(Z)V
    .locals 4
    .param p1, "receiving"    # Z

    .prologue
    .line 865
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$800(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v0

    const-string v1, "PCRTCClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IceConnectionReceiving changed to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    return-void
.end method

.method public onIceGatheringChange(Lorg/webrtc/PeerConnection$IceGatheringState;)V
    .locals 4
    .param p1, "newState"    # Lorg/webrtc/PeerConnection$IceGatheringState;

    .prologue
    .line 861
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$800(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v0

    const-string v1, "PCRTCClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IceGatheringState: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 862
    return-void
.end method

.method public onRemoveStream(Lorg/webrtc/MediaStream;)V
    .locals 0
    .param p1, "stream"    # Lorg/webrtc/MediaStream;

    .prologue
    .line 890
    return-void
.end method

.method public onRenegotiationNeeded()V
    .locals 0

    .prologue
    .line 923
    return-void
.end method

.method public onSignalingChange(Lorg/webrtc/PeerConnection$SignalingState;)V
    .locals 4
    .param p1, "newState"    # Lorg/webrtc/PeerConnection$SignalingState;

    .prologue
    .line 841
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$PCObserver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;

    invoke-static {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;->access$800(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v0

    const-string v1, "PCRTCClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SignalingState: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 842
    return-void
.end method
