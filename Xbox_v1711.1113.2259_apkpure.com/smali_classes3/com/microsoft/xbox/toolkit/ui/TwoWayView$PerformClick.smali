.class Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;
.super Lcom/microsoft/xbox/toolkit/ui/TwoWayView$WindowRunnnable;
.source "TwoWayView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PerformClick"
.end annotation


# instance fields
.field mClickMotionPosition:I

.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V
    .locals 1

    .prologue
    .line 6024
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$WindowRunnnable;-><init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p2, "x1"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;

    .prologue
    .line 6024
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;-><init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 6029
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$500(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 6043
    :cond_0
    :goto_0
    return-void

    .line 6033
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$1400(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)Landroid/widget/ListAdapter;

    move-result-object v0

    .line 6034
    .local v0, "adapter":Landroid/widget/ListAdapter;
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;->mClickMotionPosition:I

    .line 6036
    .local v2, "motionPosition":I
    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$1800(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I

    move-result v3

    if-lez v3, :cond_0

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;->sameWindow()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 6038
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$3300(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I

    move-result v4

    sub-int v4, v2, v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 6039
    .local v1, "child":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 6040
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$PerformClick;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-interface {v0, v2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-virtual {v3, v1, v2, v4, v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->performItemClick(Landroid/view/View;IJ)Z

    goto :goto_0
.end method
