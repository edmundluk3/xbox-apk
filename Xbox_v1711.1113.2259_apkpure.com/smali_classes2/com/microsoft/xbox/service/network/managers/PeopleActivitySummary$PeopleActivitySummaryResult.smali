.class public Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$PeopleActivitySummaryResult;
.super Ljava/lang/Object;
.source "PeopleActivitySummary.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PeopleActivitySummaryResult"
.end annotation


# instance fields
.field private summaries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$Summary;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deserialize(Ljava/io/InputStream;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 10
    .param p0, "stream"    # Ljava/io/InputStream;
    .param p1, "meXuid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/PopularGame;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    const/4 v2, 0x0

    .line 49
    .local v2, "games":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/PopularGame;>;"
    const-class v5, Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$PeopleActivitySummaryResult;

    const-class v6, Ljava/util/Date;

    new-instance v7, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterJSONDeserializer;

    invoke-direct {v7}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterJSONDeserializer;-><init>()V

    invoke-static {p0, v5, v6, v7}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$PeopleActivitySummaryResult;

    .line 51
    .local v3, "result":Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$PeopleActivitySummaryResult;
    if-eqz v3, :cond_2

    .line 53
    sget-object v5, Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$SummaryType;->Title:Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$SummaryType;

    invoke-direct {v3, v5}, Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$PeopleActivitySummaryResult;->findSummary(Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$SummaryType;)Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$Summary;

    move-result-object v4

    .line 54
    .local v4, "titleSummary":Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$Summary;
    new-instance v2, Ljava/util/ArrayList;

    .end local v2    # "games":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/PopularGame;>;"
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 55
    .restart local v2    # "games":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/PopularGame;>;"
    if-eqz v4, :cond_2

    iget-object v5, v4, Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$Summary;->billboard:Ljava/util/ArrayList;

    if-eqz v5, :cond_2

    .line 56
    iget-object v5, v4, Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$Summary;->billboard:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$Billboard;

    .line 57
    .local v0, "b":Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$Billboard;
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/PopularGame;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/network/managers/PopularGame;-><init>()V

    .line 58
    .local v1, "g":Lcom/microsoft/xbox/service/network/managers/PopularGame;
    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$Billboard;->contentImageUri:Ljava/lang/String;

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/service/network/managers/PopularGame;->setImageUri(Ljava/lang/String;)V

    .line 59
    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$Billboard;->contentTitle:Ljava/lang/String;

    iput-object v5, v1, Lcom/microsoft/xbox/service/network/managers/PopularGame;->name:Ljava/lang/String;

    .line 60
    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$Billboard;->bingId:Ljava/lang/String;

    iput-object v5, v1, Lcom/microsoft/xbox/service/network/managers/PopularGame;->bingId:Ljava/lang/String;

    .line 61
    iget-wide v8, v0, Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$Billboard;->titleId:J

    iput-wide v8, v1, Lcom/microsoft/xbox/service/network/managers/PopularGame;->titleId:J

    .line 62
    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$Billboard;->date:Ljava/util/Date;

    iput-object v5, v1, Lcom/microsoft/xbox/service/network/managers/PopularGame;->date:Ljava/util/Date;

    .line 64
    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$Billboard;->xuids:Ljava/util/ArrayList;

    if-eqz v5, :cond_1

    .line 65
    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$Billboard;->xuids:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    iput-object v5, v1, Lcom/microsoft/xbox/service/network/managers/PopularGame;->xuids:Ljava/util/ArrayList;

    .line 67
    :cond_1
    iget-object v5, v1, Lcom/microsoft/xbox/service/network/managers/PopularGame;->xuids:Ljava/util/ArrayList;

    if-eqz v5, :cond_0

    iget-object v5, v1, Lcom/microsoft/xbox/service/network/managers/PopularGame;->xuids:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_0

    .line 68
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 73
    .end local v0    # "b":Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$Billboard;
    .end local v1    # "g":Lcom/microsoft/xbox/service/network/managers/PopularGame;
    .end local v4    # "titleSummary":Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$Summary;
    :cond_2
    return-object v2
.end method

.method private findSummary(Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$SummaryType;)Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$Summary;
    .locals 5
    .param p1, "type"    # Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$SummaryType;

    .prologue
    .line 77
    const/4 v0, 0x0

    .line 78
    .local v0, "result":Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$Summary;
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$PeopleActivitySummaryResult;->summaries:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 79
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$PeopleActivitySummaryResult;->summaries:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$Summary;

    .line 80
    .local v1, "s":Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$Summary;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$SummaryType;->name()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v1, Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$Summary;->summaryType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 81
    move-object v0, v1

    .line 86
    .end local v1    # "s":Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$Summary;
    :cond_1
    return-object v0
.end method


# virtual methods
.method public getSummaries()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$Summary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/PeopleActivitySummary$PeopleActivitySummaryResult;->summaries:Ljava/util/ArrayList;

    return-object v0
.end method
