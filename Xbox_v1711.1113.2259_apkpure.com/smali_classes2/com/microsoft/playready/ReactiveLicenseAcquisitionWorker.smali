.class Lcom/microsoft/playready/ReactiveLicenseAcquisitionWorker;
.super Ljava/lang/Object;
.source "ReactiveLicenseAcquisitionWorker.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private mContext:Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionContext;

.field private mLAPlugin:Lcom/microsoft/playready/ILicenseAcquisitionPlugin;


# direct methods
.method public constructor <init>(Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionContext;Lcom/microsoft/playready/ILicenseAcquisitionPlugin;)V
    .locals 1
    .param p1, "context"    # Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionContext;
    .param p2, "plugin"    # Lcom/microsoft/playready/ILicenseAcquisitionPlugin;

    .prologue
    const/4 v0, 0x0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object v0, p0, Lcom/microsoft/playready/ReactiveLicenseAcquisitionWorker;->mLAPlugin:Lcom/microsoft/playready/ILicenseAcquisitionPlugin;

    .line 11
    iput-object v0, p0, Lcom/microsoft/playready/ReactiveLicenseAcquisitionWorker;->mContext:Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionContext;

    .line 17
    iput-object p1, p0, Lcom/microsoft/playready/ReactiveLicenseAcquisitionWorker;->mContext:Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionContext;

    .line 18
    iput-object p2, p0, Lcom/microsoft/playready/ReactiveLicenseAcquisitionWorker;->mLAPlugin:Lcom/microsoft/playready/ILicenseAcquisitionPlugin;

    .line 19
    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/microsoft/playready/ReactiveLicenseAcquisitionWorker;->call()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 24
    const/4 v0, 0x0

    .line 25
    .local v0, "licenseChallenge":Lcom/microsoft/playready/DrmProxy$ChallengeData;
    const/4 v1, 0x0

    .line 27
    .local v1, "licenseResponse":[B
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 29
    new-instance v2, Ljava/lang/InterruptedException;

    invoke-direct {v2}, Ljava/lang/InterruptedException;-><init>()V

    throw v2

    .line 32
    :cond_0
    iget-object v2, p0, Lcom/microsoft/playready/ReactiveLicenseAcquisitionWorker;->mContext:Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionContext;

    .line 33
    iget-object v3, p0, Lcom/microsoft/playready/ReactiveLicenseAcquisitionWorker;->mLAPlugin:Lcom/microsoft/playready/ILicenseAcquisitionPlugin;

    invoke-interface {v3}, Lcom/microsoft/playready/ILicenseAcquisitionPlugin;->getChallengeCustomData()Ljava/lang/String;

    move-result-object v3

    .line 34
    iget-object v4, p0, Lcom/microsoft/playready/ReactiveLicenseAcquisitionWorker;->mLAPlugin:Lcom/microsoft/playready/ILicenseAcquisitionPlugin;

    invoke-interface {v4}, Lcom/microsoft/playready/ILicenseAcquisitionPlugin;->getCurrentDomainInfo()Lcom/microsoft/playready/DomainInfo;

    move-result-object v4

    .line 32
    invoke-interface {v2, v3, v4}, Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionContext;->generateLicenseChallenge(Ljava/lang/String;Lcom/microsoft/playready/DomainInfo;)Lcom/microsoft/playready/DrmProxy$ChallengeData;

    move-result-object v0

    .line 36
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 38
    new-instance v2, Ljava/lang/InterruptedException;

    invoke-direct {v2}, Ljava/lang/InterruptedException;-><init>()V

    throw v2

    .line 41
    :cond_1
    iget-object v2, p0, Lcom/microsoft/playready/ReactiveLicenseAcquisitionWorker;->mLAPlugin:Lcom/microsoft/playready/ILicenseAcquisitionPlugin;

    .line 42
    invoke-virtual {v0}, Lcom/microsoft/playready/DrmProxy$ChallengeData;->getChallengeBytes()[B

    move-result-object v3

    .line 43
    invoke-virtual {v0}, Lcom/microsoft/playready/DrmProxy$ChallengeData;->getEmbeddedServerUri()Ljava/lang/String;

    move-result-object v4

    .line 41
    invoke-interface {v2, v3, v4}, Lcom/microsoft/playready/ILicenseAcquisitionPlugin;->doLicenseRequest([BLjava/lang/String;)[B

    move-result-object v1

    .line 45
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 47
    new-instance v2, Ljava/lang/InterruptedException;

    invoke-direct {v2}, Ljava/lang/InterruptedException;-><init>()V

    throw v2

    .line 50
    :cond_2
    iget-object v2, p0, Lcom/microsoft/playready/ReactiveLicenseAcquisitionWorker;->mContext:Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionContext;

    invoke-interface {v2, v1}, Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionContext;->processLicenseReponse([B)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
