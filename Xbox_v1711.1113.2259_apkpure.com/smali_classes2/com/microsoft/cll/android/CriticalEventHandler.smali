.class public Lcom/microsoft/cll/android/CriticalEventHandler;
.super Lcom/microsoft/cll/android/AbstractHandler;
.source "CriticalEventHandler.java"


# instance fields
.field private final TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/cll/android/ILogger;Ljava/lang/String;Lcom/microsoft/cll/android/ClientTelemetry;)V
    .locals 2
    .param p1, "logger"    # Lcom/microsoft/cll/android/ILogger;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "clientTelemetry"    # Lcom/microsoft/cll/android/ClientTelemetry;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/cll/android/AbstractHandler;-><init>(Lcom/microsoft/cll/android/ILogger;Ljava/lang/String;Lcom/microsoft/cll/android/ClientTelemetry;)V

    .line 12
    const-string v0, "CriticalEventHandler"

    iput-object v0, p0, Lcom/microsoft/cll/android/CriticalEventHandler;->TAG:Ljava/lang/String;

    .line 22
    new-instance v0, Lcom/microsoft/cll/android/FileStorage;

    const-string v1, ".crit.cllevent"

    invoke-direct {v0, v1, p1, p2, p0}, Lcom/microsoft/cll/android/FileStorage;-><init>(Ljava/lang/String;Lcom/microsoft/cll/android/ILogger;Ljava/lang/String;Lcom/microsoft/cll/android/AbstractHandler;)V

    iput-object v0, p0, Lcom/microsoft/cll/android/CriticalEventHandler;->fileStorage:Lcom/microsoft/cll/android/FileStorage;

    .line 23
    return-void
.end method

.method private dropNormalFile()V
    .locals 14

    .prologue
    const/4 v10, 0x0

    .line 108
    const-string v8, ".norm.cllevent"

    invoke-virtual {p0, v8}, Lcom/microsoft/cll/android/CriticalEventHandler;->findExistingFiles(Ljava/lang/String;)[Ljava/io/File;

    move-result-object v2

    .line 109
    .local v2, "files":[Ljava/io/File;
    array-length v8, v2

    const/4 v9, 0x2

    if-ge v8, v9, :cond_0

    .line 113
    iget-object v8, p0, Lcom/microsoft/cll/android/CriticalEventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v9, "CriticalEventHandler"

    const-string v10, "There are no normal files to delete"

    invoke-interface {v8, v9, v10}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :goto_0
    return-void

    .line 117
    :cond_0
    aget-object v8, v2, v10

    invoke-virtual {v8}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    .line 118
    .local v4, "lastModified":J
    aget-object v6, v2, v10

    .line 119
    .local v6, "lastModifiedFile":Ljava/io/File;
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v7, v0

    .local v7, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v7, :cond_2

    aget-object v1, v0, v3

    .line 121
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v8

    cmp-long v8, v8, v4

    if-gez v8, :cond_1

    .line 122
    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    .line 123
    move-object v6, v1

    .line 119
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 127
    .end local v1    # "file":Ljava/io/File;
    :cond_2
    sget-object v8, Lcom/microsoft/cll/android/CriticalEventHandler;->totalStorageUsed:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v10, -0x1

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v12

    mul-long/2addr v10, v12

    invoke-virtual {v8, v10, v11}, Ljava/util/concurrent/atomic/AtomicLong;->getAndAdd(J)J

    .line 128
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized add(Ljava/lang/String;Ljava/util/List;)V
    .locals 6
    .param p1, "event"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/microsoft/cll/android/FileStorage$FileFullException;
        }
    .end annotation

    .prologue
    .line 33
    .local p2, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    monitor-enter p0

    :try_start_0
    new-instance v1, Lcom/microsoft/cll/android/Tuple;

    invoke-direct {v1, p1, p2}, Lcom/microsoft/cll/android/Tuple;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 34
    .local v1, "tuple":Lcom/microsoft/cll/android/Tuple;, "Lcom/microsoft/cll/android/Tuple<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    const/4 v0, 0x0

    .line 35
    .local v0, "attempts":I
    :goto_0
    invoke-virtual {p0, v1}, Lcom/microsoft/cll/android/CriticalEventHandler;->canAdd(Lcom/microsoft/cll/android/Tuple;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 38
    sget-object v2, Lcom/microsoft/cll/android/SettingsStore$Settings;->MAXCRITICALCANADDATTEMPTS:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static {v2}, Lcom/microsoft/cll/android/SettingsStore;->getCllSettingsAsInt(Lcom/microsoft/cll/android/SettingsStore$Settings;)I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 40
    iget-object v2, p0, Lcom/microsoft/cll/android/CriticalEventHandler;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    invoke-virtual {v2}, Lcom/microsoft/cll/android/ClientTelemetry;->IncrementEventsDroppedDueToQuota()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    :goto_1
    monitor-exit p0

    return-void

    .line 44
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/microsoft/cll/android/CriticalEventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v3, "CriticalEventHandler"

    const-string v4, "Out of storage space. Attempting to drop normal file"

    invoke-interface {v2, v3, v4}, Lcom/microsoft/cll/android/ILogger;->warn(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    invoke-direct {p0}, Lcom/microsoft/cll/android/CriticalEventHandler;->dropNormalFile()V

    .line 46
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 50
    :cond_1
    iget-object v2, p0, Lcom/microsoft/cll/android/CriticalEventHandler;->fileStorage:Lcom/microsoft/cll/android/FileStorage;

    invoke-virtual {v2, v1}, Lcom/microsoft/cll/android/FileStorage;->canAdd(Lcom/microsoft/cll/android/Tuple;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 51
    iget-object v2, p0, Lcom/microsoft/cll/android/CriticalEventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v3, "CriticalEventHandler"

    const-string v4, "Closing full file and opening a new one"

    invoke-interface {v2, v3, v4}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    iget-object v2, p0, Lcom/microsoft/cll/android/CriticalEventHandler;->fileStorage:Lcom/microsoft/cll/android/FileStorage;

    invoke-virtual {v2}, Lcom/microsoft/cll/android/FileStorage;->close()V

    .line 53
    new-instance v2, Lcom/microsoft/cll/android/FileStorage;

    const-string v3, ".crit.cllevent"

    iget-object v4, p0, Lcom/microsoft/cll/android/CriticalEventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    iget-object v5, p0, Lcom/microsoft/cll/android/CriticalEventHandler;->filePath:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v5, p0}, Lcom/microsoft/cll/android/FileStorage;-><init>(Ljava/lang/String;Lcom/microsoft/cll/android/ILogger;Ljava/lang/String;Lcom/microsoft/cll/android/AbstractHandler;)V

    iput-object v2, p0, Lcom/microsoft/cll/android/CriticalEventHandler;->fileStorage:Lcom/microsoft/cll/android/FileStorage;

    .line 56
    :cond_2
    iget-object v2, p0, Lcom/microsoft/cll/android/CriticalEventHandler;->fileStorage:Lcom/microsoft/cll/android/FileStorage;

    invoke-virtual {v2, v1}, Lcom/microsoft/cll/android/FileStorage;->add(Lcom/microsoft/cll/android/Tuple;)V

    .line 57
    sget-object v2, Lcom/microsoft/cll/android/CriticalEventHandler;->totalStorageUsed:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->getAndAdd(J)J

    .line 58
    iget-object v2, p0, Lcom/microsoft/cll/android/CriticalEventHandler;->fileStorage:Lcom/microsoft/cll/android/FileStorage;

    invoke-virtual {v2}, Lcom/microsoft/cll/android/FileStorage;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 33
    .end local v0    # "attempts":I
    .end local v1    # "tuple":Lcom/microsoft/cll/android/Tuple;, "Lcom/microsoft/cll/android/Tuple<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public close()V
    .locals 3

    .prologue
    .line 90
    iget-object v0, p0, Lcom/microsoft/cll/android/CriticalEventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v1, "CriticalEventHandler"

    const-string v2, "Closing critical file"

    invoke-interface {v0, v1, v2}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/microsoft/cll/android/CriticalEventHandler;->fileStorage:Lcom/microsoft/cll/android/FileStorage;

    invoke-virtual {v0}, Lcom/microsoft/cll/android/FileStorage;->close()V

    .line 92
    return-void
.end method

.method public dispose(Lcom/microsoft/cll/android/IStorage;)V
    .locals 6
    .param p1, "storage"    # Lcom/microsoft/cll/android/IStorage;

    .prologue
    .line 100
    sget-object v0, Lcom/microsoft/cll/android/CriticalEventHandler;->totalStorageUsed:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, -0x1

    invoke-interface {p1}, Lcom/microsoft/cll/android/IStorage;->size()J

    move-result-wide v4

    mul-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->getAndAdd(J)J

    .line 101
    return-void
.end method

.method public declared-synchronized getFilesForDraining()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/cll/android/IStorage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/microsoft/cll/android/CriticalEventHandler;->fileStorage:Lcom/microsoft/cll/android/FileStorage;

    invoke-virtual {v1}, Lcom/microsoft/cll/android/FileStorage;->size()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 74
    iget-object v1, p0, Lcom/microsoft/cll/android/CriticalEventHandler;->fileStorage:Lcom/microsoft/cll/android/FileStorage;

    invoke-virtual {v1}, Lcom/microsoft/cll/android/FileStorage;->close()V

    .line 75
    const-string v1, ".crit.cllevent"

    invoke-virtual {p0, v1}, Lcom/microsoft/cll/android/CriticalEventHandler;->getFilesByExtensionForDraining(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 76
    .local v0, "storageList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/cll/android/IStorage;>;"
    new-instance v1, Lcom/microsoft/cll/android/FileStorage;

    const-string v2, ".crit.cllevent"

    iget-object v3, p0, Lcom/microsoft/cll/android/CriticalEventHandler;->logger:Lcom/microsoft/cll/android/ILogger;

    iget-object v4, p0, Lcom/microsoft/cll/android/CriticalEventHandler;->filePath:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4, p0}, Lcom/microsoft/cll/android/FileStorage;-><init>(Ljava/lang/String;Lcom/microsoft/cll/android/ILogger;Ljava/lang/String;Lcom/microsoft/cll/android/AbstractHandler;)V

    iput-object v1, p0, Lcom/microsoft/cll/android/CriticalEventHandler;->fileStorage:Lcom/microsoft/cll/android/FileStorage;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    :goto_0
    monitor-exit p0

    return-object v0

    .line 78
    .end local v0    # "storageList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/cll/android/IStorage;>;"
    :cond_0
    :try_start_1
    const-string v1, ".crit.cllevent"

    invoke-virtual {p0, v1}, Lcom/microsoft/cll/android/CriticalEventHandler;->getFilesByExtensionForDraining(Ljava/lang/String;)Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .restart local v0    # "storageList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/cll/android/IStorage;>;"
    goto :goto_0

    .line 73
    .end local v0    # "storageList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/cll/android/IStorage;>;"
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
