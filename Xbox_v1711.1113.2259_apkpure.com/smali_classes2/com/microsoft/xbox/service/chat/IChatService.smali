.class public interface abstract Lcom/microsoft/xbox/service/chat/IChatService;
.super Ljava/lang/Object;
.source "IChatService.java"


# virtual methods
.method public abstract deleteMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;Ljava/lang/String;J)Z
    .param p1    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method

.method public abstract getBackwardHistoryMessages(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;Ljava/lang/String;JI)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;
    .param p1    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p5    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method

.method public abstract getForwardHistoryMessages(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;Ljava/lang/String;JI)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;
    .param p1    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p5    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method

.method public abstract getMessageOfTheDay(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;Ljava/lang/String;)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatMessageOfTheDay;
    .param p1    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method

.method public abstract getMessages(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;
    .param p1    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method

.method public abstract registerForNotifications(JLjava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$IChatNotificationRegistrationResult;
    .param p1    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method

.method public abstract unregisterForNotifications(JLjava/lang/String;)Z
    .param p1    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method
