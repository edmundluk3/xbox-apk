.class Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppRecentHolder;
.super Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;
.source "MRUScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FullAppRecentHolder"
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/View;ILcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V
    .locals 0
    .param p1, "itemView"    # Landroid/view/View;
    .param p2, "parentWidth"    # I
    .param p3, "nowPlayingViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;
    .param p4, "pinsViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .prologue
    .line 318
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppHolder;-><init>(Landroid/view/View;ILcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V

    .line 319
    return-void
.end method


# virtual methods
.method public bind(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V
    .locals 6
    .param p1, "npi"    # Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    .prologue
    .line 323
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppRecentHolder;->bindNPI(Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;)V

    .line 325
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getRecent()Lcom/microsoft/xbox/service/model/recents/Recent;

    move-result-object v2

    .line 327
    .local v2, "recent":Lcom/microsoft/xbox/service/model/recents/Recent;
    iget-object v4, v2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->toMediaItem(Lcom/microsoft/xbox/service/model/recents/RecentItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    .line 328
    .local v1, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v4, v2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v3, v4, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ImageUrl:Ljava/lang/String;

    .line 329
    .local v3, "uri":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v4

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v0

    .line 330
    .local v0, "defaultRid":I
    :goto_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppRecentHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    const v5, 0x7f0e001d

    invoke-virtual {v4, v5, p0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setTag(ILjava/lang/Object;)V

    .line 331
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppRecentHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v4, v3, v0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 332
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppRecentHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppRecentHolder;->updateImageBackground(Landroid/widget/ImageView;)V

    .line 335
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppRecentHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    if-nez v3, :cond_1

    const/4 v4, 0x4

    :goto_1
    invoke-virtual {v5, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 337
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppRecentHolder;->mediaTitle:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v5, v2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/recents/RecentItem;->Title:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 338
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppRecentHolder;->heroStat:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppRecentHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getHeroStat()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 339
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppRecentHolder;->achievements:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppRecentHolder;->nowPlayingViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getAchievementsEarned()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 341
    iget-object v4, v2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppRecentHolder;->smartglassIcon:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppRecentHolder;->bindLaunchableItem(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Landroid/widget/TextView;)V

    .line 342
    return-void

    .line 329
    .end local v0    # "defaultRid":I
    :cond_0
    sget v0, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    goto :goto_0

    .line 335
    .restart local v0    # "defaultRid":I
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 346
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$FullAppRecentHolder;->onRecentClick(I)V

    .line 347
    return-void
.end method
