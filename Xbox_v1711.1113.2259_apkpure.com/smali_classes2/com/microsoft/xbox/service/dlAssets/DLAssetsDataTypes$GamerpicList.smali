.class public abstract Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;
.super Ljava/lang/Object;
.source "DLAssetsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "GamerpicList"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$GamerpicList;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    new-instance v0, Lcom/microsoft/xbox/service/dlAssets/AutoValue_DLAssetsDataTypes_GamerpicList$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/dlAssets/AutoValue_DLAssetsDataTypes_GamerpicList$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract gamerpics()Lcom/google/common/collect/ImmutableList;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/dlAssets/DLAssetsDataTypes$PicItem;",
            ">;"
        }
    .end annotation
.end method

.method public abstract version()I
.end method
