.class Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;
.super Lcom/microsoft/xbox/toolkit/XLEAsyncTask;
.source "ClubChatScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FormatMessageFromServiceAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEAsyncTask",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final clubId:Ljava/lang/String;

.field private containsLinks:Z

.field private directMentionSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private formattedMessage:Ljava/lang/String;

.field private final message:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

.field private final userSearchService:Lcom/microsoft/xbox/service/userSearch/IUserSearchService;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "message"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "clubId"    # Ljava/lang/String;

    .prologue
    .line 1017
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    .line 1018
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/XLEAsyncTask;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 1011
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->containsLinks:Z

    .line 1019
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1020
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1022
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->message:Ljava/lang/String;

    .line 1023
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->clubId:Ljava/lang/String;

    .line 1025
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getUserSearchService()Lcom/microsoft/xbox/service/userSearch/IUserSearchService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->userSearchService:Lcom/microsoft/xbox/service/userSearch/IUserSearchService;

    .line 1027
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->directMentionSet:Ljava/util/Set;

    .line 1028
    return-void
.end method

.method private findQueryTerm(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "potentialDirectMention"    # Ljava/lang/String;

    .prologue
    .line 1031
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1032
    const-string v1, ""

    .line 1041
    :goto_0
    return-object v1

    .line 1035
    :cond_0
    const/4 v0, 0x0

    .line 1036
    .local v0, "index":I
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1037
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1038
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1041
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method static synthetic lambda$doInBackground$0(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;Ljava/lang/String;)Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;
    .locals 10
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;
    .param p1, "potentialDirectMention"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1060
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->findQueryTerm(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1061
    .local v3, "query":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1091
    :cond_0
    :goto_0
    return-object v6

    .line 1065
    :cond_1
    const/4 v4, 0x0

    .line 1067
    .local v4, "response":Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;
    :try_start_0
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->userSearchService:Lcom/microsoft/xbox/service/userSearch/IUserSearchService;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->clubId:Ljava/lang/String;

    invoke-interface {v7, v3, v8}, Lcom/microsoft/xbox/service/userSearch/IUserSearchService;->clubSearch(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 1072
    :goto_1
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;->getResults()Ljava/util/List;

    move-result-object v7

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1073
    const/4 v2, 0x0

    .line 1074
    .local v2, "longestMatchedItem":Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResponse;->getResults()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;

    .line 1075
    .local v5, "userSearchResult":Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;
    if-eqz v5, :cond_2

    .line 1076
    iget-object v8, v5, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;->result:Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;

    iget-object v1, v8, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->gamertag:Ljava/lang/String;

    .line 1078
    .local v1, "gamerTag":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 1079
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    if-eqz v2, :cond_3

    iget-object v8, v2, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;->result:Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;

    iget-object v8, v8, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->gamertag:Ljava/lang/String;

    .line 1080
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    if-ge v8, v9, :cond_2

    .line 1081
    :cond_3
    move-object v2, v5

    goto :goto_2

    .line 1068
    .end local v1    # "gamerTag":Ljava/lang/String;
    .end local v2    # "longestMatchedItem":Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;
    .end local v5    # "userSearchResult":Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;
    :catch_0
    move-exception v0

    .line 1069
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->access$300()Ljava/lang/String;

    move-result-object v7

    const-string v8, "FormatMessageFromServiceAsyncTask - usersearch error"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1086
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    .restart local v2    # "longestMatchedItem":Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;
    :cond_4
    if-eqz v2, :cond_0

    .line 1087
    iget-object v6, v2, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResult;->result:Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;

    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic doInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1007
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->doInBackground()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected doInBackground()Ljava/lang/Void;
    .locals 19

    .prologue
    .line 1050
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->message:Ljava/lang/String;

    invoke-static {v14}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatUtil;->xmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->formattedMessage:Ljava/lang/String;

    .line 1052
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1053
    .local v8, "potentialDirectMentionIndexList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1054
    .local v9, "resolveTasks":Ljava/util/List;, "Ljava/util/List<Ljava/util/concurrent/Callable<Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;>;>;"
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->access$000()Ljava/util/regex/Pattern;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->formattedMessage:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v10

    .line 1055
    .local v10, "resolvingMatcher":Ljava/util/regex/Matcher;
    :goto_0
    invoke-virtual {v10}, Ljava/util/regex/Matcher;->find()Z

    move-result v14

    if-eqz v14, :cond_0

    .line 1056
    invoke-virtual {v10}, Ljava/util/regex/Matcher;->start()I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-interface {v8, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1058
    const/4 v14, 0x1

    invoke-virtual {v10, v14}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    .line 1059
    .local v7, "potentialDirectMention":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-static {v0, v7}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v14

    invoke-interface {v9, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1095
    .end local v7    # "potentialDirectMention":Ljava/lang/String;
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1097
    .local v4, "futureList":Ljava/util/List;, "Ljava/util/List<Ljava/util/concurrent/Future<Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;>;>;"
    :try_start_0
    sget-object v14, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v14, v9}, Ljava/util/concurrent/ExecutorService;->invokeAll(Ljava/util/Collection;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 1102
    :goto_1
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 1103
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->access$300()Ljava/lang/String;

    move-result-object v14

    const-string v15, "FormatMessageFromServiceAsyncTask - futureList is null or empty"

    invoke-static {v14, v15}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 1151
    :cond_1
    :goto_2
    const/4 v14, 0x0

    return-object v14

    .line 1098
    :catch_0
    move-exception v2

    .line 1099
    .local v2, "ex":Ljava/lang/InterruptedException;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->access$300()Ljava/lang/String;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "FormatMessageFromServiceAsyncTask - invokeAll error: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1105
    .end local v2    # "ex":Ljava/lang/InterruptedException;
    :cond_2
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v14

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v15

    if-eq v14, v15, :cond_3

    .line 1106
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->access$300()Ljava/lang/String;

    move-result-object v14

    const-string v15, "FormatMessageFromServiceAsyncTask - returned result doesn\'t match with request list"

    invoke-static {v14, v15}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1109
    :cond_3
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 1110
    .local v12, "stringBuilder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .line 1111
    .local v1, "end":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_3
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v14

    if-ge v5, v14, :cond_5

    .line 1112
    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 1113
    .local v6, "index":I
    const/4 v11, 0x0

    .line 1115
    .local v11, "result":Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;
    :try_start_1
    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/concurrent/Future;

    invoke-interface {v14}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v14

    move-object v0, v14

    check-cast v0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;

    move-object v11, v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1120
    :goto_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->formattedMessage:Ljava/lang/String;

    invoke-virtual {v14, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1121
    if-eqz v11, :cond_4

    .line 1122
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->directMentionSet:Ljava/util/Set;

    iget-object v15, v11, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->id:Ljava/lang/String;

    invoke-interface {v14, v15}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1124
    sget-object v14, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v15, "<at id=\"%1$s\">@%2$s</at>"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    iget-object v0, v11, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->id:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v16, v17

    const/16 v17, 0x1

    iget-object v0, v11, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->gamertag:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v16, v17

    invoke-static/range {v14 .. v16}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1126
    .local v3, "formattedDirectMention":Ljava/lang/String;
    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1131
    iget-object v14, v11, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->gamertag:Ljava/lang/String;

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int/2addr v14, v6

    add-int/lit8 v1, v14, 0x1

    .line 1111
    .end local v3    # "formattedDirectMention":Ljava/lang/String;
    :goto_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 1116
    :catch_1
    move-exception v2

    .line 1117
    .local v2, "ex":Ljava/lang/Exception;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->access$300()Ljava/lang/String;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "FormatMessageFromServiceAsyncTask - Future get error: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 1133
    .end local v2    # "ex":Ljava/lang/Exception;
    :cond_4
    move v1, v6

    goto :goto_5

    .line 1137
    .end local v6    # "index":I
    .end local v11    # "result":Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;
    :cond_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->formattedMessage:Ljava/lang/String;

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    if-eq v1, v14, :cond_6

    .line 1138
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->formattedMessage:Ljava/lang/String;

    invoke-virtual {v14, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1141
    :cond_6
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->formattedMessage:Ljava/lang/String;

    .line 1143
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->formattedMessage:Ljava/lang/String;

    invoke-static {v14}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatUtil;->reformatUrlsInMessage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1144
    .local v13, "urlFormattedMessage":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->formattedMessage:Ljava/lang/String;

    invoke-virtual {v14, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_1

    .line 1145
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->formattedMessage:Ljava/lang/String;

    .line 1146
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->containsLinks:Z

    goto/16 :goto_2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1007
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 4
    .param p1, "aVoid"    # Ljava/lang/Void;

    .prologue
    .line 1156
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->directMentionSet:Ljava/util/Set;

    .line 1157
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->containsLinks:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->message:Ljava/lang/String;

    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->directMentionSet:Ljava/util/Set;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->containsLinks:Z

    .line 1156
    invoke-static {v1, v0, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;Ljava/lang/String;Ljava/util/List;Z)V

    .line 1160
    return-void

    .line 1157
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$FormatMessageFromServiceAsyncTask;->formattedMessage:Ljava/lang/String;

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 1046
    return-void
.end method
