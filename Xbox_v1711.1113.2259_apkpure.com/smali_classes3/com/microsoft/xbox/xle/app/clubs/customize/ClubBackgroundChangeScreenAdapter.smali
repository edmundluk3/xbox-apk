.class public Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
.source "ClubBackgroundChangeScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$AchievementBackgroundItem;,
        Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$ScreenshotBackgroundItem;
    }
.end annotation


# static fields
.field private static final SCREENSHOT_POSITION:I


# instance fields
.field private final addCustomButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private final backgroundImageAdapter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;

.field private final browseAllAchievementsBtn:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final gameFilterSpinner:Landroid/widget/Spinner;

.field private final gamesFilterAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final removeBackground:Landroid/widget/Button;

.field private final screenshotAchievementSpinner:Landroid/widget/Spinner;

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;)V
    .locals 7
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v6, 0x1090009

    const v5, 0x1090008

    .line 54
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;-><init>()V

    .line 55
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 56
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;

    .line 58
    const v3, 0x7f0e0324

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    const v3, 0x7f0e0327

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 60
    const v3, 0x7f0e032c

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->browseAllAchievementsBtn:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 61
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->browseAllAchievementsBtn:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;-><init>(Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener$Callback;)V

    .line 65
    .local v1, "itemSelectedListener":Lcom/microsoft/xbox/toolkit/ui/ItemSelectedListener;
    const v3, 0x7f0e032b

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->gameFilterSpinner:Landroid/widget/Spinner;

    .line 66
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->gameFilterSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 67
    new-instance v3, Landroid/widget/ArrayAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-direct {v3, v4, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->gamesFilterAdapter:Landroid/widget/ArrayAdapter;

    .line 68
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->gamesFilterAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v3, v6}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 69
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->gameFilterSpinner:Landroid/widget/Spinner;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->gamesFilterAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 71
    const v3, 0x7f0e032a

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->screenshotAchievementSpinner:Landroid/widget/Spinner;

    .line 73
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    const/high16 v4, 0x7f0d0000

    invoke-static {v3, v4, v5}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v0

    .line 75
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    invoke-virtual {v0, v6}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 76
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->screenshotAchievementSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 77
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->screenshotAchievementSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 79
    const v3, 0x7f0e0328

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/RecyclerView;

    .line 80
    .local v2, "recyclerView":Landroid/support/v7/widget/RecyclerView;
    new-instance v3, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct {v3, v4, v5, v6}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 81
    new-instance v3, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;-><init>(Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->backgroundImageAdapter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;

    .line 82
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->backgroundImageAdapter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 84
    const v3, 0x7f0e032d

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->removeBackground:Landroid/widget/Button;

    .line 85
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->removeBackground:Landroid/widget/Button;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    const v3, 0x7f0e0329

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->addCustomButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 88
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->addCustomButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->removeLabelLeftMargin()V

    .line 89
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->addCustomButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->onImageSelected(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;)V

    return-void
.end method

.method private buildAchievementList(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    .local p1, "achievements":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 126
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;>;"
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$$Lambda$7;->lambdaFactory$()Lcom/microsoft/xbox/toolkit/java8/Predicate;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/microsoft/xbox/toolkit/ListUtil;->filter(Ljava/util/List;Lcom/microsoft/xbox/toolkit/java8/Predicate;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    .line 127
    .local v0, "item":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;
    new-instance v3, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$AchievementBackgroundItem;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v0, v4}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$AchievementBackgroundItem;-><init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$1;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 129
    .end local v0    # "item":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;
    :cond_0
    return-object v1
.end method

.method private buildScreenshotList(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    .local p1, "screenshotList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 142
    .local v0, "result":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;>;"
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$$Lambda$8;->lambdaFactory$()Lcom/microsoft/xbox/toolkit/java8/Predicate;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/microsoft/xbox/toolkit/ListUtil;->filter(Ljava/util/List;Lcom/microsoft/xbox/toolkit/java8/Predicate;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;

    .line 143
    .local v1, "screenshot":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;
    new-instance v3, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$ScreenshotBackgroundItem;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v1, v4}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$ScreenshotBackgroundItem;-><init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$1;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 145
    .end local v1    # "screenshot":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;
    :cond_0
    return-object v0
.end method

.method private buildScreenshotList(Ljava/util/Map;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;",
            ">;>;)",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    .local p1, "screenshotsByTitle":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;>;>;"
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 134
    .local v0, "result":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;>;"
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 135
    .local v1, "title":Ljava/lang/String;
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->buildScreenshotList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 137
    .end local v1    # "title":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method private displayAchievements(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 162
    .local p1, "achievements":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->backgroundImageAdapter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;->clear()V

    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->backgroundImageAdapter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->buildAchievementList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;->addAll(Ljava/util/Collection;)V

    .line 164
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->backgroundImageAdapter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;->notifyDataSetChanged()V

    .line 165
    return-void
.end method

.method private displayScreenshots(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 153
    .local p1, "screenshotsByTitle":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->backgroundImageAdapter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;->clear()V

    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->backgroundImageAdapter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->buildScreenshotList(Ljava/util/Map;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;->addAll(Ljava/util/Collection;)V

    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->backgroundImageAdapter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;->notifyDataSetChanged()V

    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->gamesFilterAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->gamesFilterAdapter:Landroid/widget/ArrayAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0701a9

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 158
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->gamesFilterAdapter:Landroid/widget/ArrayAdapter;

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 159
    return-void
.end method

.method static synthetic lambda$buildAchievementList$4(Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;)Z
    .locals 1
    .param p0, "a"    # Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->getAchievementIconUri()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$buildScreenshotList$5(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;)Z
    .locals 1
    .param p0, "s"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;

    .prologue
    .line 142
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->contentLocators()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;Landroid/view/View;)V
    .locals 0
    .param p0, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->close()V

    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->browseAllAchievements()V

    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->removeBackground()V

    return-void
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->navigateToUploadPicScreen()V

    return-void
.end method

.method private onImageSelected(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;)V
    .locals 2
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;

    invoke-interface {p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;->getImageURI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->onImageSelected(Ljava/lang/String;)V

    .line 150
    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 168
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->screenshotAchievementSpinner:Landroid/widget/Spinner;

    if-ne p1, v2, :cond_3

    .line 170
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->gameFilterSpinner:Landroid/widget/Spinner;

    if-nez p3, :cond_0

    move v2, v3

    :goto_0
    invoke-static {v5, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 171
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->browseAllAchievementsBtn:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz p3, :cond_1

    :goto_1
    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 173
    if-nez p3, :cond_2

    .line 174
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->getScreenshotsByTitle()Ljava/util/Map;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->displayScreenshots(Ljava/util/Map;)V

    .line 192
    :goto_2
    return-void

    :cond_0
    move v2, v4

    .line 170
    goto :goto_0

    :cond_1
    move v3, v4

    .line 171
    goto :goto_1

    .line 176
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->getAchievements()Ljava/util/List;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->displayAchievements(Ljava/util/List;)V

    goto :goto_2

    .line 181
    :cond_3
    if-eqz p3, :cond_4

    .line 182
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->gamesFilterAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2, p3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 183
    .local v1, "selected":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->getScreenshotsByTitle()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->buildScreenshotList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 188
    .end local v1    # "selected":Ljava/lang/String;
    .local v0, "gamesList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;>;"
    :goto_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->backgroundImageAdapter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;->clear()V

    .line 189
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->backgroundImageAdapter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;->addAll(Ljava/util/Collection;)V

    .line 190
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->backgroundImageAdapter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter;->notifyDataSetChanged()V

    goto :goto_2

    .line 185
    .end local v0    # "gamesList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;>;"
    :cond_4
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->getScreenshotsByTitle()Ljava/util/Map;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->buildScreenshotList(Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    .restart local v0    # "gamesList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;>;"
    goto :goto_3
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 94
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->onStart()V

    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->addCustomButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->customPicUploadEnabled()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 97
    return-void
.end method

.method protected updateViewOverride()V
    .locals 4

    .prologue
    .line 101
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 103
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->removeBackground:Landroid/widget/Button;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->removeBackgroundEnabled()Z

    move-result v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 105
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v2, v3, :cond_0

    .line 106
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->screenshotAchievementSpinner:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v2

    if-nez v2, :cond_2

    .line 107
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->getScreenshotsByTitle()Ljava/util/Map;

    move-result-object v1

    .line 108
    .local v1, "screenshotsByTitle":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;>;>;"
    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 109
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 122
    .end local v1    # "screenshotsByTitle":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;>;>;"
    :cond_0
    :goto_0
    return-void

    .line 111
    .restart local v1    # "screenshotsByTitle":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;>;>;"
    :cond_1
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->displayScreenshots(Ljava/util/Map;)V

    goto :goto_0

    .line 114
    .end local v1    # "screenshotsByTitle":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;>;>;"
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;->getAchievements()Ljava/util/List;

    move-result-object v0

    .line 115
    .local v0, "achievements":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 116
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    goto :goto_0

    .line 118
    :cond_3
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;->displayAchievements(Ljava/util/List;)V

    goto :goto_0
.end method
