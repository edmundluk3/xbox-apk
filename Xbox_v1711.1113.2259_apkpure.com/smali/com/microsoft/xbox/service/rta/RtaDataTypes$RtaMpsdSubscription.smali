.class public abstract Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaMpsdSubscription;
.super Ljava/lang/Object;
.source "RtaDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/rta/RtaDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "RtaMpsdSubscription"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaMpsdSubscription;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    new-instance v0, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaMpsdSubscription$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/rta/AutoValue_RtaDataTypes_RtaMpsdSubscription$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract connectionId()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ConnectionId"
    .end annotation
.end method
