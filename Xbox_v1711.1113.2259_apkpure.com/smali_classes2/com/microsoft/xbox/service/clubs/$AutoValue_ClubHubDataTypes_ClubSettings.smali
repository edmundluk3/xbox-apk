.class abstract Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;
.super Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
.source "$AutoValue_ClubHubDataTypes_ClubSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings$Builder;
    }
.end annotation


# instance fields
.field private final chat:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

.field private final feed:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

.field private final lfg:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

.field private final profile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

.field private final roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

.field private final viewerRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;)V
    .locals 0
    .param p1, "feed"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "chat"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "lfg"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "roster"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "profile"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "viewerRoles"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->feed:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    .line 25
    iput-object p2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->chat:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    .line 26
    iput-object p3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->lfg:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    .line 27
    iput-object p4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    .line 28
    iput-object p5, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->profile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    .line 29
    iput-object p6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->viewerRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    .line 30
    return-void
.end method


# virtual methods
.method public chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->chat:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 82
    if-ne p1, p0, :cond_1

    .line 94
    :cond_0
    :goto_0
    return v1

    .line 85
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    if-eqz v3, :cond_9

    move-object v0, p1

    .line 86
    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    .line 87
    .local v0, "that":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->feed:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->chat:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    if-nez v3, :cond_4

    .line 88
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->lfg:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    if-nez v3, :cond_5

    .line 89
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    if-nez v3, :cond_6

    .line 90
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->profile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    if-nez v3, :cond_7

    .line 91
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->viewerRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    if-nez v3, :cond_8

    .line 92
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 87
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->feed:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 88
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->chat:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 89
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->lfg:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_3

    .line 90
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_4

    .line 91
    :cond_7
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->profile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_5

    .line 92
    :cond_8
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->viewerRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    :cond_9
    move v1, v2

    .line 94
    goto/16 :goto_0
.end method

.method public feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->feed:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const v3, 0xf4243

    const/4 v2, 0x0

    .line 99
    const/4 v0, 0x1

    .line 100
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 101
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->feed:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 102
    mul-int/2addr v0, v3

    .line 103
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->chat:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 104
    mul-int/2addr v0, v3

    .line 105
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->lfg:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 106
    mul-int/2addr v0, v3

    .line 107
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    xor-int/2addr v0, v1

    .line 108
    mul-int/2addr v0, v3

    .line 109
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->profile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    xor-int/2addr v0, v1

    .line 110
    mul-int/2addr v0, v3

    .line 111
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->viewerRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    if-nez v1, :cond_5

    :goto_5
    xor-int/2addr v0, v2

    .line 112
    return v0

    .line 101
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->feed:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    .line 103
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->chat:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    .line 105
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->lfg:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    .line 107
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_3

    .line 109
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->profile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_4

    .line 111
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->viewerRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5
.end method

.method public lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->lfg:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    return-object v0
.end method

.method public profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->profile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    return-object v0
.end method

.method public roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    return-object v0
.end method

.method public toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;
    .locals 1

    .prologue
    .line 117
    new-instance v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings$Builder;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings$Builder;-><init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClubSettings{feed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->feed:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", chat="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->chat:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lfg="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->lfg:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", roster="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", profile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->profile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", viewerRoles="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->viewerRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;->viewerRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    return-object v0
.end method
