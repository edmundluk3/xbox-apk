.class final enum Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;
.super Ljava/lang/Enum;
.source "MRUScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ViewType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

.field public static final enum FULL_APP:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

.field public static final enum FULL_APP_RECENT:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

.field public static final enum FULL_GAME:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

.field public static final enum FULL_GAME_RECENT:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

.field public static final enum FULL_NO_STATS:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

.field public static final enum FULL_NO_STATS_RECENT:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

.field public static final enum FULL_PROMO:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

.field public static final enum RECENT:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

.field public static final enum SNAP:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 169
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    const-string v1, "FULL_APP"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->FULL_APP:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    .line 170
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    const-string v1, "FULL_APP_RECENT"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->FULL_APP_RECENT:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    .line 171
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    const-string v1, "FULL_GAME"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->FULL_GAME:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    .line 172
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    const-string v1, "FULL_GAME_RECENT"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->FULL_GAME_RECENT:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    .line 173
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    const-string v1, "FULL_NO_STATS"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->FULL_NO_STATS:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    .line 174
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    const-string v1, "FULL_NO_STATS_RECENT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->FULL_NO_STATS_RECENT:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    .line 175
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    const-string v1, "FULL_PROMO"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->FULL_PROMO:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    .line 176
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    const-string v1, "SNAP"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->SNAP:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    const-string v1, "RECENT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->RECENT:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    .line 168
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->FULL_APP:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->FULL_APP_RECENT:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->FULL_GAME:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->FULL_GAME_RECENT:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->FULL_NO_STATS:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->FULL_NO_STATS_RECENT:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->FULL_PROMO:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->SNAP:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->RECENT:Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->$VALUES:[Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 168
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 168
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;
    .locals 1

    .prologue
    .line 168
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->$VALUES:[Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/adapter/MRUScreenAdapter$ViewType;

    return-object v0
.end method
