.class public Lcom/microsoft/xbox/service/model/SessionModel;
.super Lcom/microsoft/xbox/toolkit/XLEObservable;
.source "SessionModel.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionStateListener;
.implements Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IMediaStateListener;
.implements Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangedListener;
.implements Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionRequestCompleteListener;
.implements Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleMessageListener;
.implements Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IChannelEstablishedListener;
.implements Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IPrimaryDeviceStateChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/SessionModel$JoinSessionRunner;,
        Lcom/microsoft/xbox/service/model/SessionModel$SessionModelHolder;,
        Lcom/microsoft/xbox/service/model/SessionModel$OnConnectedLocaleChangedHandler;,
        Lcom/microsoft/xbox/service/model/SessionModel$OnSessionDroppedHandler;,
        Lcom/microsoft/xbox/service/model/SessionModel$OnExplicitConnectionRequiredHandler;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEObservable",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;",
        "Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionStateListener;",
        "Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IMediaStateListener;",
        "Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangedListener;",
        "Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionRequestCompleteListener;",
        "Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleMessageListener;",
        "Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IChannelEstablishedListener;",
        "Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IPrimaryDeviceStateChangedListener;"
    }
.end annotation


# static fields
.field private static BUNDLE_KEY_ACTIVE_TITLE_LOCATOIN:Ljava/lang/String; = null

.field private static final DateFormat:Ljava/lang/String; = "yyyy-MM-dd\'T\'HH:mm:ss.SSS\'z\'"

.field private static final POST_ACTION_DELAY_MS:I = 0xc8


# instance fields
.field private companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

.field private connectTimer:Lcom/microsoft/xbox/toolkit/TimeMonitor;

.field private consoleLocale:Ljava/lang/String;

.field private currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

.field private deviceId:Ljava/lang/String;

.field private falseStart:Z

.field private fireVortexConnectEventLater:Z

.field private ieChannelReady:Lcom/microsoft/xbox/toolkit/Ready;

.field private isConnecting:Z

.field private isIeUrlChanging:Z

.field private isIpAddressEnteredManually:Z

.field private isManual:Z

.field private isRetryConnecting:Z

.field private lastKnownIeUrl:Ljava/lang/String;

.field private leftSession:Z

.field private localeChangedHandler:Lcom/microsoft/xbox/service/model/SessionModel$OnConnectedLocaleChangedHandler;

.field private manualConnectHandler:Lcom/microsoft/xbox/service/model/SessionModel$OnExplicitConnectionRequiredHandler;

.field private queuedAction:Ljava/lang/Runnable;

.field private sessionDroppedHandler:Lcom/microsoft/xbox/service/model/SessionModel$OnSessionDroppedHandler;

.field private startTime:Ljava/util/Date;

.field private wasConnected:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 87
    const-string v0, "ActiveTitleLocation"

    sput-object v0, Lcom/microsoft/xbox/service/model/SessionModel;->BUNDLE_KEY_ACTIVE_TITLE_LOCATOIN:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 118
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/XLEObservable;-><init>()V

    .line 92
    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->isConnecting:Z

    .line 96
    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->falseStart:Z

    .line 98
    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->isIeUrlChanging:Z

    .line 110
    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->isRetryConnecting:Z

    .line 111
    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->wasConnected:Z

    .line 112
    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->fireVortexConnectEventLater:Z

    .line 119
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->addCompanionSessionStateListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionStateListener;)V

    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->addTitleChangedListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangedListener;)V

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->addMediaStateListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IMediaStateListener;)V

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->addTitleMessageListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleMessageListener;)V

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->addChannelEstablishedListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IChannelEstablishedListener;)V

    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->addCompanionSessionRequestCompleteListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionRequestCompleteListener;)V

    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->addPrimaryDeviceStateChangedListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IPrimaryDeviceStateChangedListener;)V

    .line 129
    new-instance v0, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/TimeMonitor;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->connectTimer:Lcom/microsoft/xbox/toolkit/TimeMonitor;

    .line 130
    new-instance v0, Lcom/microsoft/xbox/toolkit/Ready;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/Ready;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->ieChannelReady:Lcom/microsoft/xbox/toolkit/Ready;

    .line 131
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/SessionModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/SessionModel$1;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/SessionModel;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/model/SessionModel;)Lcom/microsoft/xbox/toolkit/Ready;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/SessionModel;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->ieChannelReady:Lcom/microsoft/xbox/toolkit/Ready;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/service/model/SessionModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/SessionModel;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/SessionModel;->stopIeChannel()V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/service/model/SessionModel;)Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/SessionModel;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/service/model/SessionModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/SessionModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/SessionModel;->onJoinSessionAsync(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method private clearQueuedActions()V
    .locals 1

    .prologue
    .line 462
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->queuedAction:Ljava/lang/Runnable;

    .line 463
    return-void
.end method

.method private completeQueuedActions()V
    .locals 3

    .prologue
    .line 453
    const-string v1, "SessionModel"

    const-string v2, "complete the queued actions"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/SessionModel;->dequeueAction()Ljava/lang/Runnable;

    move-result-object v0

    .line 455
    .local v0, "action":Ljava/lang/Runnable;
    if-eqz v0, :cond_0

    .line 457
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/SessionModel;->runAction(Ljava/lang/Runnable;)V

    .line 459
    :cond_0
    return-void
.end method

.method private dequeueAction()Ljava/lang/Runnable;
    .locals 2

    .prologue
    .line 408
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->queuedAction:Ljava/lang/Runnable;

    .line 409
    .local v0, "action":Ljava/lang/Runnable;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/SessionModel;->queuedAction:Ljava/lang/Runnable;

    .line 410
    return-object v0
.end method

.method private fireVortexConnectEvent(ZLjava/lang/String;)V
    .locals 12
    .param p1, "success"    # Z
    .param p2, "consoleLocale"    # Ljava/lang/String;

    .prologue
    .line 666
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/SessionModel;->connectTimer:Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->getIsStarted()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/SessionModel;->connectTimer:Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->currentTime()J

    move-result-wide v0

    .line 667
    .local v0, "duration":J
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/SessionModel;->connectTimer:Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->reset()V

    .line 668
    new-instance v11, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyy-MM-dd\'T\'HH:mm:ss.SSS\'z\'"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v11, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 669
    .local v11, "sdf":Ljava/text/SimpleDateFormat;
    const-string v2, "UTC"

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 670
    if-eqz p1, :cond_0

    .line 674
    iget-boolean v2, p0, Lcom/microsoft/xbox/service/model/SessionModel;->isManual:Z

    iget-boolean v3, p0, Lcom/microsoft/xbox/service/model/SessionModel;->isIpAddressEnteredManually:Z

    iget-boolean v4, p0, Lcom/microsoft/xbox/service/model/SessionModel;->isRetryConnecting:Z

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/SessionModel;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    .line 678
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getLastErrorCode()J

    move-result-wide v6

    long-to-int v5, v6

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/SessionModel;->startTime:Ljava/util/Date;

    .line 679
    invoke-virtual {v11, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/service/model/SessionModel;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    .line 680
    invoke-virtual {v7}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getSessionId()Ljava/util/UUID;

    move-result-object v7

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v8, p2

    .line 674
    invoke-static/range {v0 .. v10}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection;->trackConnectSuccess(JZZZILjava/lang/String;Ljava/util/UUID;Ljava/lang/String;Ljava/lang/String;I)V

    .line 685
    :cond_0
    return-void

    .line 666
    .end local v0    # "duration":J
    .end local v11    # "sdf":Ljava/text/SimpleDateFormat;
    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public static getInstance()Lcom/microsoft/xbox/service/model/SessionModel;
    .locals 1

    .prologue
    .line 149
    sget-object v0, Lcom/microsoft/xbox/service/model/SessionModel$SessionModelHolder;->instance:Lcom/microsoft/xbox/service/model/SessionModel;

    return-object v0
.end method

.method public static getNowPlayingLocation(Landroid/os/Bundle;)Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .locals 1
    .param p0, "b"    # Landroid/os/Bundle;

    .prologue
    .line 662
    sget-object v0, Lcom/microsoft/xbox/service/model/SessionModel;->BUNDLE_KEY_ACTIVE_TITLE_LOCATOIN:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->fromInt(I)Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v0

    return-object v0
.end method

.method private hasQueuedActions()Z
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->queuedAction:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private makeNowPlayingBundle(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Landroid/os/Bundle;
    .locals 3
    .param p1, "location"    # Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .prologue
    .line 656
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 657
    .local v0, "b":Landroid/os/Bundle;
    sget-object v1, Lcom/microsoft/xbox/service/model/SessionModel;->BUNDLE_KEY_ACTIVE_TITLE_LOCATOIN:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->getValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 658
    return-object v0
.end method

.method private onJoinSessionAsync(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Void;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 309
    sget-object v0, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 312
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/SessionModel;->isConnecting:Z

    .line 313
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 315
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v2, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v3, Lcom/microsoft/xbox/service/model/UpdateType;->SessionState:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v2, v3, v1}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v1

    invoke-direct {v0, v2, p0, v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/SessionModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 317
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 309
    goto :goto_0
.end method

.method public static reset()V
    .locals 2

    .prologue
    .line 153
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 154
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/SessionModel;->clearObservers()V

    .line 155
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->getPlatformReady()Lcom/microsoft/xbox/toolkit/Ready;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/Ready;->getIsReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;->SIGNOUT:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/SessionModel;->leaveSession(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;)V

    .line 158
    :cond_0
    return-void

    .line 153
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private runAction(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "action"    # Ljava/lang/Runnable;

    .prologue
    .line 448
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 449
    return-void
.end method

.method private runActionSafe(Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "action"    # Ljava/lang/Runnable;

    .prologue
    const/4 v2, 0x0

    .line 414
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getCurrentSessionState()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    .line 415
    const-string v0, "SessionModel"

    const-string v1, "not connected, queue action"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/SessionModel;->queueAction(Ljava/lang/Runnable;)V

    .line 417
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    if-eqz v0, :cond_1

    .line 420
    const-string v0, "SessionModel"

    const-string v1, "console exists, try to connect"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    invoke-virtual {p0, v2, v2}, Lcom/microsoft/xbox/service/model/SessionModel;->connectToConsole(ZZ)V

    .line 432
    :cond_0
    :goto_0
    return-void

    .line 423
    :cond_1
    const-string v0, "SessionModel"

    const-string v1, "no previously connected console, bring up console picker"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->manualConnectHandler:Lcom/microsoft/xbox/service/model/SessionModel$OnExplicitConnectionRequiredHandler;

    if-eqz v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->manualConnectHandler:Lcom/microsoft/xbox/service/model/SessionModel$OnExplicitConnectionRequiredHandler;

    invoke-interface {v0}, Lcom/microsoft/xbox/service/model/SessionModel$OnExplicitConnectionRequiredHandler;->onExplicitConnectionRequired()V

    goto :goto_0

    .line 430
    :cond_2
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/SessionModel;->runAction(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private runIfConnected(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "action"    # Ljava/lang/Runnable;

    .prologue
    .line 320
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getCurrentSessionState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 321
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 325
    :goto_0
    return-void

    .line 323
    :cond_0
    const-string v0, "SessionModel"

    const-string v1, "session not connected, ignore action, you should check session state before calling"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setConsoleLocale(Ljava/lang/String;)V
    .locals 2
    .param p1, "locale"    # Ljava/lang/String;

    .prologue
    .line 642
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->localeChangedHandler:Lcom/microsoft/xbox/service/model/SessionModel$OnConnectedLocaleChangedHandler;

    if-eqz v0, :cond_0

    .line 643
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->localeChangedHandler:Lcom/microsoft/xbox/service/model/SessionModel$OnConnectedLocaleChangedHandler;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/SessionModel;->consoleLocale:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lcom/microsoft/xbox/service/model/SessionModel$OnConnectedLocaleChangedHandler;->onLocaleChanged(Ljava/lang/String;Ljava/lang/String;)V

    .line 646
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/SessionModel;->consoleLocale:Ljava/lang/String;

    .line 647
    return-void
.end method

.method private startIeChannel()V
    .locals 3

    .prologue
    .line 636
    const-string v0, "SessionModel"

    const-string v1, "Starting an IE channel"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 637
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->ieChannelReady:Lcom/microsoft/xbox/toolkit/Ready;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/Ready;->reset()V

    .line 638
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    new-instance v1, Lcom/microsoft/xbox/smartglass/MessageTarget;

    const v2, 0x3d8b930f

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/smartglass/MessageTarget;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->startChannel(Lcom/microsoft/xbox/smartglass/MessageTarget;I)V

    .line 639
    return-void
.end method

.method private stopIeChannel()V
    .locals 3

    .prologue
    .line 650
    const-string v0, "SessionModel"

    const-string v1, "Stopping an IE channel"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    new-instance v1, Lcom/microsoft/xbox/smartglass/MessageTarget;

    const v2, 0x3d8b930f

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/smartglass/MessageTarget;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->stopChannel(Lcom/microsoft/xbox/smartglass/MessageTarget;)V

    .line 652
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->ieChannelReady:Lcom/microsoft/xbox/toolkit/Ready;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/Ready;->reset()V

    .line 653
    return-void
.end method


# virtual methods
.method public TEST_clearObservers()V
    .locals 0

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/SessionModel;->clearObservers()V

    .line 145
    return-void
.end method

.method public connectToConsole(ZZ)V
    .locals 9
    .param p1, "isManual"    # Z
    .param p2, "isIpAddressEnteredManually"    # Z

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 279
    sget-object v2, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    if-ne v2, v5, :cond_0

    move v2, v3

    :goto_0
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 280
    const-string v2, "SessionModel"

    const-string v5, "connect to console is called"

    invoke-static {v2, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    iput-boolean v4, p0, Lcom/microsoft/xbox/service/model/SessionModel;->leftSession:Z

    .line 286
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/SessionModel;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getCurrentSessionState()I

    move-result v2

    if-nez v2, :cond_1

    .line 287
    iput-boolean v4, p0, Lcom/microsoft/xbox/service/model/SessionModel;->falseStart:Z

    .line 288
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/model/SessionModel;->isManual:Z

    .line 289
    iput-boolean p2, p0, Lcom/microsoft/xbox/service/model/SessionModel;->isIpAddressEnteredManually:Z

    .line 291
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/SessionModel;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 292
    iput-boolean v3, p0, Lcom/microsoft/xbox/service/model/SessionModel;->isConnecting:Z

    .line 293
    iput-boolean v4, p0, Lcom/microsoft/xbox/service/model/SessionModel;->wasConnected:Z

    .line 294
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/service/model/SessionModel;->startTime:Ljava/util/Date;

    .line 295
    new-instance v0, Lcom/microsoft/xbox/service/model/SessionModel$JoinSessionRunner;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/SessionModel;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/ConsoleData;->getIpAddress()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/microsoft/xbox/service/model/SessionModel$JoinSessionRunner;-><init>(Lcom/microsoft/xbox/service/model/SessionModel;Ljava/lang/String;)V

    .line 296
    .local v0, "runner":Lcom/microsoft/xbox/service/model/SessionModel$JoinSessionRunner;
    new-instance v1, Lcom/microsoft/xbox/toolkit/DataLoaderTask;

    invoke-direct {v1, v0}, Lcom/microsoft/xbox/toolkit/DataLoaderTask;-><init>(Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    .line 297
    .local v1, "task":Lcom/microsoft/xbox/toolkit/DataLoaderTask;, "Lcom/microsoft/xbox/toolkit/DataLoaderTask<Ljava/lang/Void;>;"
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/DataLoaderTask;->execute()V

    .line 299
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/SessionModel;->connectTimer:Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->start()V

    .line 300
    new-instance v2, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v3, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v5, Lcom/microsoft/xbox/service/model/UpdateType;->SessionState:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v3, v5, v4}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-direct {v2, v3, p0, v8}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/model/SessionModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 306
    .end local v0    # "runner":Lcom/microsoft/xbox/service/model/SessionModel$JoinSessionRunner;
    .end local v1    # "task":Lcom/microsoft/xbox/toolkit/DataLoaderTask;, "Lcom/microsoft/xbox/toolkit/DataLoaderTask<Ljava/lang/Void;>;"
    :goto_1
    return-void

    :cond_0
    move v2, v4

    .line 279
    goto :goto_0

    .line 302
    :cond_1
    new-instance v2, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v5, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v6, Lcom/microsoft/xbox/service/model/UpdateType;->SessionState:Lcom/microsoft/xbox/service/model/UpdateType;

    iget-boolean v7, p0, Lcom/microsoft/xbox/service/model/SessionModel;->isConnecting:Z

    if-nez v7, :cond_2

    :goto_2
    invoke-direct {v5, v6, v3}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-direct {v2, v5, p0, v8}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/model/SessionModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 304
    const-string v2, "SessionModel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ignore connect request because "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/SessionModel;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getCurrentSessionState()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    move v3, v4

    .line 302
    goto :goto_2
.end method

.method public falseStart(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 266
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/model/SessionModel;->falseStart:Z

    .line 267
    return-void
.end method

.method public getActiveTitleStates()Ljava/util/Hashtable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Hashtable",
            "<",
            "Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;",
            "Lcom/microsoft/xbox/smartglass/ActiveTitleState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 200
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getActiveTitleStates()Ljava/util/Hashtable;

    move-result-object v0

    return-object v0
.end method

.method public getConsoleLocale()Ljava/lang/String;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->consoleLocale:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 271
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->deviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayedSessionState()I
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getCurrentSessionState()I

    move-result v0

    return v0
.end method

.method public getIeChannelReady()Lcom/microsoft/xbox/toolkit/Ready;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->ieChannelReady:Lcom/microsoft/xbox/toolkit/Ready;

    return-object v0
.end method

.method public getIsConnecting()Z
    .locals 1

    .prologue
    .line 173
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->isConnecting:Z

    return v0
.end method

.method public getIsIeUrlChanging()Z
    .locals 1

    .prologue
    .line 217
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->isIeUrlChanging:Z

    return v0
.end method

.method public getIsRetryConnecting()Z
    .locals 1

    .prologue
    .line 177
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->isRetryConnecting:Z

    return v0
.end method

.method public getLastErrorCode()J
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getLastErrorCode()J

    move-result-wide v0

    return-wide v0
.end method

.method public getLastKnownIeUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->lastKnownIeUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getMediaState(I)Lcom/microsoft/xbox/smartglass/MediaState;
    .locals 1
    .param p1, "titleId"    # I

    .prologue
    .line 204
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getMediaState(I)Lcom/microsoft/xbox/smartglass/MediaState;

    move-result-object v0

    return-object v0
.end method

.method public getSessionState()I
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getCurrentSessionState()I

    move-result v0

    return v0
.end method

.method public getTitleId(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)I
    .locals 2
    .param p1, "location"    # Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .prologue
    .line 208
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/SessionModel;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getActiveTitleStates()Ljava/util/Hashtable;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    .line 209
    .local v0, "titleState":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    if-nez v0, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    iget v1, v0, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    goto :goto_0
.end method

.method public launchUriOnXboxOne(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;Ljava/lang/Runnable;)V
    .locals 4
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "location"    # Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;
    .param p3, "postAction"    # Ljava/lang/Runnable;

    .prologue
    .line 383
    const-string v1, "SessionModel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "launch uri for location "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Snapped:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    if-ne p2, v1, :cond_1

    .line 385
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    const-string v2, "Play in Snap"

    invoke-virtual {v1, v2, p1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    :goto_0
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    .line 390
    :cond_0
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 405
    :goto_1
    return-void

    .line 387
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    const-string v2, "Play on Xbox"

    invoke-virtual {v1, v2, p1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 392
    :cond_2
    new-instance v0, Lcom/microsoft/xbox/service/model/SessionModel$8;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/service/model/SessionModel$8;-><init>(Lcom/microsoft/xbox/service/model/SessionModel;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;Ljava/lang/Runnable;)V

    .line 403
    .local v0, "action":Ljava/lang/Runnable;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/SessionModel;->runActionSafe(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method public leaveSession(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;)V
    .locals 5
    .param p1, "mode"    # Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 239
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    sget-object v4, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v1, v4, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 240
    const-string v1, "SessionModel"

    const-string v4, "leave the session"

    invoke-static {v1, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/SessionModel;->leftSession:Z

    .line 242
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/SessionModel;->clearQueuedActions()V

    .line 247
    new-instance v0, Lcom/microsoft/xbox/service/model/SessionModel$2;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/microsoft/xbox/service/model/SessionModel$1;

    invoke-direct {v2, p0, p1}, Lcom/microsoft/xbox/service/model/SessionModel$1;-><init>(Lcom/microsoft/xbox/service/model/SessionModel;Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;)V

    invoke-direct {v0, p0, v1, v2}, Lcom/microsoft/xbox/service/model/SessionModel$2;-><init>(Lcom/microsoft/xbox/service/model/SessionModel;Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 258
    .local v0, "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->execute()V

    .line 259
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/SessionModel;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    .line 260
    iput-boolean v3, p0, Lcom/microsoft/xbox/service/model/SessionModel;->wasConnected:Z

    .line 261
    return-void

    .end local v0    # "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    :cond_0
    move v1, v3

    .line 239
    goto :goto_0
.end method

.method public load(Z)V
    .locals 4
    .param p1, "refreshSessionData"    # Z

    .prologue
    .line 235
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->SessionState:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/SessionModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 236
    return-void
.end method

.method public onChannelEstablished(Lcom/microsoft/xbox/smartglass/MessageTarget;Lcom/microsoft/xbox/smartglass/SGResult;)V
    .locals 2
    .param p1, "target"    # Lcom/microsoft/xbox/smartglass/MessageTarget;
    .param p2, "result"    # Lcom/microsoft/xbox/smartglass/SGResult;

    .prologue
    .line 606
    invoke-virtual {p2}, Lcom/microsoft/xbox/smartglass/SGResult;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/smartglass/MessageTarget;->isTitleId()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    const v1, 0x3d8b930f

    if-ne v0, v1, :cond_0

    .line 607
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->ieChannelReady:Lcom/microsoft/xbox/toolkit/Ready;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/Ready;->setReady()V

    .line 608
    const-string v0, "SessionModel"

    const-string v1, "IE channel established"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    :cond_0
    return-void
.end method

.method public onConnectCancelled()V
    .locals 2

    .prologue
    .line 435
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 436
    const-string v0, "SessionModel"

    const-string v1, "user canceled connection, clear the queued actions"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/SessionModel;->clearQueuedActions()V

    .line 438
    return-void
.end method

.method public onMediaStateUpdated(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;Lcom/microsoft/xbox/smartglass/MediaState;)V
    .locals 5
    .param p1, "titleLocation"    # Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .param p2, "mediaState"    # Lcom/microsoft/xbox/smartglass/MediaState;

    .prologue
    const/4 v1, 0x1

    .line 580
    sget-object v0, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 581
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v2, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v3, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingState:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/SessionModel;->makeNowPlayingBundle(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Landroid/os/Bundle;

    move-result-object v4

    invoke-direct {v2, v3, v1, v4}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;ZLandroid/os/Bundle;)V

    const/4 v1, 0x0

    invoke-direct {v0, v2, p0, v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/SessionModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 582
    return-void

    .line 580
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPrimaryDeviceStateChanged(Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;)V
    .locals 2
    .param p1, "state"    # Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;

    .prologue
    .line 614
    if-eqz p1, :cond_1

    .line 615
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->fireVortexConnectEventLater:Z

    if-eqz v0, :cond_0

    .line 617
    const/4 v0, 0x1

    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;->locale:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/service/model/SessionModel;->fireVortexConnectEvent(ZLjava/lang/String;)V

    .line 618
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->fireVortexConnectEventLater:Z

    .line 620
    :cond_0
    iget-object v0, p1, Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;->locale:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/SessionModel;->setConsoleLocale(Ljava/lang/String;)V

    .line 622
    :cond_1
    return-void
.end method

.method public onSessionRequestCompleted(IIJ)V
    .locals 5
    .param p1, "operation"    # I
    .param p2, "state"    # I
    .param p3, "error"    # J

    .prologue
    const/4 v3, 0x1

    .line 626
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 627
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-nez v0, :cond_1

    .line 628
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->SessionLaunchRequestComplete:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/SessionModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 633
    :cond_0
    :goto_0
    return-void

    .line 630
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->SessionLaunchRequestComplete:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    new-instance v2, Lcom/microsoft/xbox/toolkit/XLEException;

    invoke-direct {v2, p3, p4}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    invoke-direct {v0, v1, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/SessionModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    goto :goto_0
.end method

.method public onSessionStateChanged(ILcom/microsoft/xbox/toolkit/XLEException;)V
    .locals 9
    .param p1, "newSessionState"    # I
    .param p2, "exception"    # Lcom/microsoft/xbox/toolkit/XLEException;

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 472
    sget-object v3, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    if-ne v3, v6, :cond_3

    move v3, v4

    :goto_0
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 474
    const/4 v0, 0x0

    .line 475
    .local v0, "shouldReport":Z
    const/4 v1, 0x0

    .line 477
    .local v1, "success":Z
    const-string v3, "SessionModel"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Session state changed to: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    packed-switch p1, :pswitch_data_0

    .line 534
    :goto_1
    if-eqz v0, :cond_1

    .line 535
    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/SessionModel;->consoleLocale:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 537
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/SessionModel;->consoleLocale:Ljava/lang/String;

    invoke-direct {p0, v1, v3}, Lcom/microsoft/xbox/service/model/SessionModel;->fireVortexConnectEvent(ZLjava/lang/String;)V

    .line 545
    :cond_1
    :goto_2
    new-instance v3, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v5, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v6, Lcom/microsoft/xbox/service/model/UpdateType;->SessionState:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v5, v6, v4}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-direct {v3, v5, p0, p2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/service/model/SessionModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 546
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/SessionModel;->hasQueuedActions()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 547
    const/4 v3, 0x2

    if-ne p1, v3, :cond_7

    .line 548
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/SessionModel;->completeQueuedActions()V

    .line 555
    :cond_2
    :goto_3
    return-void

    .end local v0    # "shouldReport":Z
    .end local v1    # "success":Z
    :cond_3
    move v3, v5

    .line 472
    goto :goto_0

    .line 481
    .restart local v0    # "shouldReport":Z
    .restart local v1    # "success":Z
    :pswitch_0
    iput-boolean v4, p0, Lcom/microsoft/xbox/service/model/SessionModel;->isConnecting:Z

    goto :goto_1

    .line 484
    :pswitch_1
    const/4 v0, 0x1

    .line 485
    const/4 v1, 0x1

    .line 486
    iput-boolean v4, p0, Lcom/microsoft/xbox/service/model/SessionModel;->wasConnected:Z

    .line 487
    iput-boolean v5, p0, Lcom/microsoft/xbox/service/model/SessionModel;->isConnecting:Z

    goto :goto_1

    .line 497
    :pswitch_2
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->isConnecting:Z

    .line 498
    iput-boolean v5, p0, Lcom/microsoft/xbox/service/model/SessionModel;->isConnecting:Z

    .line 499
    iget-boolean v3, p0, Lcom/microsoft/xbox/service/model/SessionModel;->wasConnected:Z

    if-eqz v3, :cond_5

    iget-boolean v3, p0, Lcom/microsoft/xbox/service/model/SessionModel;->leftSession:Z

    if-nez v3, :cond_5

    iget-boolean v3, p0, Lcom/microsoft/xbox/service/model/SessionModel;->isRetryConnecting:Z

    if-nez v3, :cond_5

    .line 500
    const-string v3, "SessionModel"

    const-string v6, "Session dropped and not trigged by leave session, and not retrying, retry connect"

    invoke-static {v3, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/SessionModel;->companionSession:Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    sget-object v6, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;->DROPPED:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->reportSessionDropped(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;)V

    .line 503
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/SessionModel;->sessionDroppedHandler:Lcom/microsoft/xbox/service/model/SessionModel$OnSessionDroppedHandler;

    if-eqz v3, :cond_4

    .line 504
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/SessionModel;->sessionDroppedHandler:Lcom/microsoft/xbox/service/model/SessionModel$OnSessionDroppedHandler;

    .line 505
    .local v2, "tempHandler":Lcom/microsoft/xbox/service/model/SessionModel$OnSessionDroppedHandler;
    const-string v3, "SessionModel"

    const-string v6, "notify session dropped"

    invoke-static {v3, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    new-instance v3, Lcom/microsoft/xbox/service/model/SessionModel$9;

    invoke-direct {v3, p0, v2}, Lcom/microsoft/xbox/service/model/SessionModel$9;-><init>(Lcom/microsoft/xbox/service/model/SessionModel;Lcom/microsoft/xbox/service/model/SessionModel$OnSessionDroppedHandler;)V

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 521
    .end local v2    # "tempHandler":Lcom/microsoft/xbox/service/model/SessionModel$OnSessionDroppedHandler;
    :cond_4
    :goto_4
    iput-boolean v5, p0, Lcom/microsoft/xbox/service/model/SessionModel;->wasConnected:Z

    .line 522
    iput-object v8, p0, Lcom/microsoft/xbox/service/model/SessionModel;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    .line 523
    invoke-direct {p0, v8}, Lcom/microsoft/xbox/service/model/SessionModel;->setConsoleLocale(Ljava/lang/String;)V

    goto :goto_1

    .line 516
    :cond_5
    const-string v3, "SessionModel"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "should not retry, leftsession: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/microsoft/xbox/service/model/SessionModel;->leftSession:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    const-string v3, "SessionModel"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "should not retry, retrying : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/microsoft/xbox/service/model/SessionModel;->isRetryConnecting:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    const-string v3, "SessionModel"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "should not retry, retrying : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/microsoft/xbox/service/model/SessionModel;->wasConnected:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 526
    :pswitch_3
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->isConnecting:Z

    .line 527
    iput-boolean v5, p0, Lcom/microsoft/xbox/service/model/SessionModel;->isConnecting:Z

    .line 528
    iput-object v8, p0, Lcom/microsoft/xbox/service/model/SessionModel;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    goto/16 :goto_1

    .line 541
    :cond_6
    iput-boolean v4, p0, Lcom/microsoft/xbox/service/model/SessionModel;->fireVortexConnectEventLater:Z

    goto/16 :goto_2

    .line 550
    :cond_7
    const/4 v3, 0x3

    if-ne p1, v3, :cond_2

    .line 551
    new-instance v3, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v5, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v6, Lcom/microsoft/xbox/service/model/UpdateType;->SessionRequestFailure:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v5, v6, v4}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-direct {v3, v5, p0, v8}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/service/model/SessionModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 552
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/SessionModel;->clearQueuedActions()V

    goto/16 :goto_3

    .line 479
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public onTitleChanged(Lcom/microsoft/xbox/smartglass/ActiveTitleState;Lcom/microsoft/xbox/smartglass/ActiveTitleState;)V
    .locals 7
    .param p1, "oldTitleState"    # Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    .param p2, "newTitleState"    # Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 559
    sget-object v0, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 561
    const-string v3, "SessionModel"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "OnTitleChanged %s. old id=%s, new id=%d"

    const/4 v0, 0x3

    new-array v6, v0, [Ljava/lang/Object;

    iget-object v0, p2, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    aput-object v0, v6, v2

    if-nez p1, :cond_1

    const-string v0, "none"

    .line 562
    :goto_1
    aput-object v0, v6, v1

    const/4 v0, 0x2

    iget v2, p2, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v0

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 561
    invoke-static {v3, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 575
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v2, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v3, Lcom/microsoft/xbox/service/model/UpdateType;->NowPlayingState:Lcom/microsoft/xbox/service/model/UpdateType;

    iget-object v4, p2, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/service/model/SessionModel;->makeNowPlayingBundle(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Landroid/os/Bundle;

    move-result-object v4

    invoke-direct {v2, v3, v1, v4}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;ZLandroid/os/Bundle;)V

    const/4 v1, 0x0

    invoke-direct {v0, v2, p0, v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/SessionModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 576
    return-void

    :cond_0
    move v0, v2

    .line 559
    goto :goto_0

    .line 561
    :cond_1
    iget v0, p1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    .line 562
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public onTitleMessage(Lcom/microsoft/xbox/smartglass/Message;)V
    .locals 6
    .param p1, "message"    # Lcom/microsoft/xbox/smartglass/Message;

    .prologue
    const/4 v3, 0x1

    .line 586
    iget-object v2, p1, Lcom/microsoft/xbox/smartglass/Message;->type:Lcom/microsoft/xbox/smartglass/MessageType;

    sget-object v4, Lcom/microsoft/xbox/smartglass/MessageType;->Json:Lcom/microsoft/xbox/smartglass/MessageType;

    if-ne v2, v4, :cond_0

    iget-object v2, p1, Lcom/microsoft/xbox/smartglass/Message;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    iget v2, v2, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    const v4, 0x3d8b930f

    if-ne v2, v4, :cond_0

    move-object v0, p1

    .line 589
    check-cast v0, Lcom/microsoft/xbox/smartglass/JsonMessage;

    .line 591
    .local v0, "jm":Lcom/microsoft/xbox/smartglass/JsonMessage;
    iget-object v2, v0, Lcom/microsoft/xbox/smartglass/JsonMessage;->text:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/TitleMessage;->fromJson(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/TitleMessage;

    move-result-object v1

    .line 592
    .local v1, "titleMessage":Lcom/microsoft/xbox/service/model/TitleMessage;
    if-eqz v1, :cond_0

    .line 593
    iget-object v2, v1, Lcom/microsoft/xbox/service/model/TitleMessage;->notification:Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    sget-object v4, Lcom/microsoft/xbox/service/model/TitleMessage$Type;->titleinfo:Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    if-ne v2, v4, :cond_1

    iget-object v2, v1, Lcom/microsoft/xbox/service/model/TitleMessage;->id:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/microsoft/xbox/service/model/TitleMessage;->id:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v4, 0x3

    if-lt v2, v4, :cond_1

    .line 594
    iget-object v2, v1, Lcom/microsoft/xbox/service/model/TitleMessage;->id:Ljava/lang/String;

    iget-object v4, v1, Lcom/microsoft/xbox/service/model/TitleMessage;->id:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/service/model/SessionModel;->deviceId:Ljava/lang/String;

    .line 602
    .end local v0    # "jm":Lcom/microsoft/xbox/smartglass/JsonMessage;
    .end local v1    # "titleMessage":Lcom/microsoft/xbox/service/model/TitleMessage;
    :cond_0
    :goto_0
    return-void

    .line 595
    .restart local v0    # "jm":Lcom/microsoft/xbox/smartglass/JsonMessage;
    .restart local v1    # "titleMessage":Lcom/microsoft/xbox/service/model/TitleMessage;
    :cond_1
    iget-object v2, v1, Lcom/microsoft/xbox/service/model/TitleMessage;->notification:Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    sget-object v4, Lcom/microsoft/xbox/service/model/TitleMessage$Type;->url_changed:Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    if-eq v2, v4, :cond_2

    iget-object v2, v1, Lcom/microsoft/xbox/service/model/TitleMessage;->notification:Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    sget-object v4, Lcom/microsoft/xbox/service/model/TitleMessage$Type;->url_changing:Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    if-ne v2, v4, :cond_0

    :cond_2
    iget-object v2, v1, Lcom/microsoft/xbox/service/model/TitleMessage;->data:Lcom/microsoft/xbox/service/model/TitleMessage$Data;

    if-eqz v2, :cond_0

    .line 596
    iget-object v2, v1, Lcom/microsoft/xbox/service/model/TitleMessage;->data:Lcom/microsoft/xbox/service/model/TitleMessage$Data;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/TitleMessage$Data;->url:Lcom/microsoft/xbox/service/model/TitleMessage$DataChunk;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/TitleMessage$DataChunk;->data:Ljava/lang/String;

    iput-object v2, p0, Lcom/microsoft/xbox/service/model/SessionModel;->lastKnownIeUrl:Ljava/lang/String;

    .line 597
    iget-object v2, v1, Lcom/microsoft/xbox/service/model/TitleMessage;->notification:Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    sget-object v4, Lcom/microsoft/xbox/service/model/TitleMessage$Type;->url_changing:Lcom/microsoft/xbox/service/model/TitleMessage$Type;

    if-ne v2, v4, :cond_3

    move v2, v3

    :goto_1
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/SessionModel;->isIeUrlChanging:Z

    .line 598
    new-instance v2, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v4, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v5, Lcom/microsoft/xbox/service/model/UpdateType;->InternetExplorerData:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {v4, v5, v3}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    const/4 v3, 0x0

    invoke-direct {v2, v4, p0, v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/model/SessionModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    goto :goto_0

    .line 597
    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public queueAction(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "action"    # Ljava/lang/Runnable;

    .prologue
    .line 441
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SessionModel;->queuedAction:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 442
    const-string v0, "SessionModel"

    const-string v1, "we are overwritten queued action, only one is allowed. "

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/SessionModel;->queuedAction:Ljava/lang/Runnable;

    .line 445
    return-void
.end method

.method public sendKey(Lcom/microsoft/xbox/smartglass/GamePadButtons;)V
    .locals 1
    .param p1, "key"    # Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .prologue
    .line 350
    new-instance v0, Lcom/microsoft/xbox/service/model/SessionModel$5;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/model/SessionModel$5;-><init>(Lcom/microsoft/xbox/service/model/SessionModel;Lcom/microsoft/xbox/smartglass/GamePadButtons;)V

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/SessionModel;->runIfConnected(Ljava/lang/Runnable;)V

    .line 358
    return-void
.end method

.method public sendKey(Lcom/microsoft/xbox/smartglass/GamePadButtons;Z)V
    .locals 1
    .param p1, "key"    # Lcom/microsoft/xbox/smartglass/GamePadButtons;
    .param p2, "simulateButtonPress"    # Z

    .prologue
    .line 361
    new-instance v0, Lcom/microsoft/xbox/service/model/SessionModel$6;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/model/SessionModel$6;-><init>(Lcom/microsoft/xbox/service/model/SessionModel;Lcom/microsoft/xbox/smartglass/GamePadButtons;Z)V

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/SessionModel;->runIfConnected(Ljava/lang/Runnable;)V

    .line 368
    return-void
.end method

.method public sendTitleMessage(Lcom/microsoft/xbox/smartglass/MessageTarget;Ljava/lang/String;)V
    .locals 1
    .param p1, "target"    # Lcom/microsoft/xbox/smartglass/MessageTarget;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 371
    new-instance v0, Lcom/microsoft/xbox/service/model/SessionModel$7;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/model/SessionModel$7;-><init>(Lcom/microsoft/xbox/service/model/SessionModel;Lcom/microsoft/xbox/smartglass/MessageTarget;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/SessionModel;->runIfConnected(Ljava/lang/Runnable;)V

    .line 380
    return-void
.end method

.method public setCompanionSession(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;)V
    .locals 0
    .param p1, "newSession"    # Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    .prologue
    .line 141
    return-void
.end method

.method public setCurrentConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)V
    .locals 0
    .param p1, "console"    # Lcom/microsoft/xbox/xle/model/ConsoleData;

    .prologue
    .line 275
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/SessionModel;->currentConsole:Lcom/microsoft/xbox/xle/model/ConsoleData;

    .line 276
    return-void
.end method

.method public setExplicitConnectListner(Lcom/microsoft/xbox/service/model/SessionModel$OnExplicitConnectionRequiredHandler;)V
    .locals 0
    .param p1, "handler"    # Lcom/microsoft/xbox/service/model/SessionModel$OnExplicitConnectionRequiredHandler;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/SessionModel;->manualConnectHandler:Lcom/microsoft/xbox/service/model/SessionModel$OnExplicitConnectionRequiredHandler;

    .line 162
    return-void
.end method

.method public setOnLocaleChangedHandler(Lcom/microsoft/xbox/service/model/SessionModel$OnConnectedLocaleChangedHandler;)V
    .locals 0
    .param p1, "handler"    # Lcom/microsoft/xbox/service/model/SessionModel$OnConnectedLocaleChangedHandler;

    .prologue
    .line 169
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/SessionModel;->localeChangedHandler:Lcom/microsoft/xbox/service/model/SessionModel$OnConnectedLocaleChangedHandler;

    .line 170
    return-void
.end method

.method public setOnSessionDroppedHandler(Lcom/microsoft/xbox/service/model/SessionModel$OnSessionDroppedHandler;)V
    .locals 0
    .param p1, "handler"    # Lcom/microsoft/xbox/service/model/SessionModel$OnSessionDroppedHandler;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/SessionModel;->sessionDroppedHandler:Lcom/microsoft/xbox/service/model/SessionModel$OnSessionDroppedHandler;

    .line 166
    return-void
.end method

.method public setRetryConnectingStatus(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 181
    sget-object v0, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 182
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/model/SessionModel;->isRetryConnecting:Z

    .line 183
    return-void

    .line 181
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startChannel(Lcom/microsoft/xbox/smartglass/MessageTarget;I)V
    .locals 1
    .param p1, "target"    # Lcom/microsoft/xbox/smartglass/MessageTarget;
    .param p2, "activityId"    # I

    .prologue
    .line 328
    new-instance v0, Lcom/microsoft/xbox/service/model/SessionModel$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/model/SessionModel$3;-><init>(Lcom/microsoft/xbox/service/model/SessionModel;Lcom/microsoft/xbox/smartglass/MessageTarget;I)V

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/SessionModel;->runIfConnected(Ljava/lang/Runnable;)V

    .line 336
    return-void
.end method

.method public stopChannel(Lcom/microsoft/xbox/smartglass/MessageTarget;)V
    .locals 1
    .param p1, "target"    # Lcom/microsoft/xbox/smartglass/MessageTarget;

    .prologue
    .line 339
    new-instance v0, Lcom/microsoft/xbox/service/model/SessionModel$4;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/model/SessionModel$4;-><init>(Lcom/microsoft/xbox/service/model/SessionModel;Lcom/microsoft/xbox/smartglass/MessageTarget;)V

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/SessionModel;->runIfConnected(Ljava/lang/Runnable;)V

    .line 347
    return-void
.end method
