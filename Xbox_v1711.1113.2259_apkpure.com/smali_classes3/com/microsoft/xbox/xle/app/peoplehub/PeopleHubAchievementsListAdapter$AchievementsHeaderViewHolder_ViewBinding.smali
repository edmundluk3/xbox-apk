.class public Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder_ViewBinding;
.super Ljava/lang/Object;
.source "PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;

    .line 24
    const v0, 0x7f0e08fb

    const-string v1, "field \'compareAchievementButton\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->compareAchievementButton:Landroid/view/View;

    .line 25
    const v0, 0x7f0e08fc

    const-string v1, "field \'comparisonHeader\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->comparisonHeader:Landroid/view/View;

    .line 26
    const v0, 0x7f0e08fa

    const-string v1, "field \'shareAchievementButton\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->shareAchievementButton:Landroid/view/View;

    .line 27
    const v0, 0x7f0e0401

    const-string v1, "field \'otherGamertag\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->otherGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 28
    const v0, 0x7f0e03f7

    const-string v1, "field \'meProfileImage\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->meProfileImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 29
    const v0, 0x7f0e0402

    const-string v1, "field \'otherProfileImage\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->otherProfileImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 30
    const v0, 0x7f0e03f9

    const-string v1, "field \'meGamerscore\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->meGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 31
    const v0, 0x7f0e0404

    const-string v1, "field \'otherGamerscore\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->otherGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 32
    const v0, 0x7f0e03fa

    const-string v1, "field \'meGamerscoreChange\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->meGamerscoreChange:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 33
    const v0, 0x7f0e0405

    const-string v1, "field \'otherGamerscoreChange\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->otherGamerscoreChange:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 34
    const v0, 0x7f0e03fb

    const-string v1, "field \'meGamerscoreMonth\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->meGamerscoreMonth:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 35
    const v0, 0x7f0e0406

    const-string v1, "field \'otherGamerscoreMonth\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->otherGamerscoreMonth:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 36
    const v0, 0x7f0e03fc

    const-string v1, "field \'achievementMeLayout\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->achievementMeLayout:Landroid/view/View;

    .line 37
    const v0, 0x7f0e0407

    const-string v1, "field \'achievementYouLayout\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->achievementYouLayout:Landroid/view/View;

    .line 38
    const v0, 0x7f0e03fe

    const-string v1, "field \'rareAchievementMeLayout\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->rareAchievementMeLayout:Landroid/view/View;

    .line 39
    const v0, 0x7f0e0409

    const-string v1, "field \'rareAchievementYouLayout\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->rareAchievementYouLayout:Landroid/view/View;

    .line 40
    const v0, 0x7f0e08fd

    const-string v1, "field \'filterSpinner\'"

    const-class v2, Landroid/widget/Spinner;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->filterSpinner:Landroid/widget/Spinner;

    .line 41
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;

    .line 47
    .local v0, "target":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 48
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;

    .line 50
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->compareAchievementButton:Landroid/view/View;

    .line 51
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->comparisonHeader:Landroid/view/View;

    .line 52
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->shareAchievementButton:Landroid/view/View;

    .line 53
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->otherGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 54
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->meProfileImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 55
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->otherProfileImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 56
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->meGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 57
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->otherGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 58
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->meGamerscoreChange:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 59
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->otherGamerscoreChange:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 60
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->meGamerscoreMonth:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 61
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->otherGamerscoreMonth:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 62
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->achievementMeLayout:Landroid/view/View;

    .line 63
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->achievementYouLayout:Landroid/view/View;

    .line 64
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->rareAchievementMeLayout:Landroid/view/View;

    .line 65
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->rareAchievementYouLayout:Landroid/view/View;

    .line 66
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->filterSpinner:Landroid/widget/Spinner;

    .line 67
    return-void
.end method
