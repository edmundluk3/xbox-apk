.class public Lcom/flipkart/chatheads/ui/MaximizedArrangement;
.super Lcom/flipkart/chatheads/ui/ChatHeadArrangement;
.source "MaximizedArrangement.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljava/io/Serializable;",
        ">",
        "Lcom/flipkart/chatheads/ui/ChatHeadArrangement;"
    }
.end annotation


# static fields
.field public static final BUNDLE_HERO_INDEX_KEY:Ljava/lang/String; = "hero_index"

.field private static MAX_DISTANCE_FROM_ORIGINAL:D

.field private static MIN_VELOCITY_TO_POSITION_BACK:I


# instance fields
.field private arrowLayout:Lcom/flipkart/chatheads/ui/UpArrowLayout;

.field private currentChatHead:Lcom/flipkart/chatheads/ui/ChatHead;

.field private extras:Landroid/os/Bundle;

.field private isActive:Z

.field private isChatHeadCountChanging:Z

.field private isTransitioning:Z

.field private manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/flipkart/chatheads/ui/ChatHeadManager",
            "<TT;>;"
        }
    .end annotation
.end field

.field private maxDistanceFromOriginal:I

.field private maxHeight:I

.field private maxWidth:I

.field private final positions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/flipkart/chatheads/ui/ChatHead;",
            "Landroid/graphics/Point;",
            ">;"
        }
    .end annotation
.end field

.field private topPadding:I


# direct methods
.method public constructor <init>(Lcom/flipkart/chatheads/ui/ChatHeadManager;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHeadManager",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    .local p1, "manager":Lcom/flipkart/chatheads/ui/ChatHeadManager;, "Lcom/flipkart/chatheads/ui/ChatHeadManager<TT;>;"
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/ChatHeadArrangement;-><init>()V

    .line 24
    new-instance v0, Landroid/support/v4/util/ArrayMap;

    invoke-direct {v0}, Landroid/support/v4/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->positions:Ljava/util/Map;

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->currentChatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    .line 32
    iput-boolean v1, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->isActive:Z

    .line 33
    iput-boolean v1, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->isTransitioning:Z

    .line 34
    iput-boolean v1, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->isChatHeadCountChanging:Z

    .line 38
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    .line 39
    return-void
.end method

.method static synthetic access$000(Lcom/flipkart/chatheads/ui/MaximizedArrangement;)V
    .locals 0
    .param p0, "x0"    # Lcom/flipkart/chatheads/ui/MaximizedArrangement;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->deactivate()V

    return-void
.end method

.method static synthetic access$100(Lcom/flipkart/chatheads/ui/MaximizedArrangement;)Z
    .locals 1
    .param p0, "x0"    # Lcom/flipkart/chatheads/ui/MaximizedArrangement;

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->isTransitioning:Z

    return v0
.end method

.method static synthetic access$102(Lcom/flipkart/chatheads/ui/MaximizedArrangement;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/flipkart/chatheads/ui/MaximizedArrangement;
    .param p1, "x1"    # Z

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->isTransitioning:Z

    return p1
.end method

.method static synthetic access$200(Lcom/flipkart/chatheads/ui/MaximizedArrangement;)Lcom/flipkart/chatheads/ui/ChatHead;
    .locals 1
    .param p0, "x0"    # Lcom/flipkart/chatheads/ui/MaximizedArrangement;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->currentChatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    return-object v0
.end method

.method static synthetic access$302(Lcom/flipkart/chatheads/ui/MaximizedArrangement;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/flipkart/chatheads/ui/MaximizedArrangement;
    .param p1, "x1"    # Z

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->isChatHeadCountChanging:Z

    return p1
.end method

.method private deactivate()V
    .locals 3

    .prologue
    .line 427
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    const-class v1, Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->getBundleWithHero()Landroid/os/Bundle;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->setArrangement(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 428
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->hideView()V

    .line 429
    return-void
.end method

.method private detach(Lcom/flipkart/chatheads/ui/ChatHead;)V
    .locals 2
    .param p1, "chatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;

    .prologue
    .line 192
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->getArrowLayout()Lcom/flipkart/chatheads/ui/UpArrowLayout;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->detachView(Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)V

    .line 193
    return-void
.end method

.method private getArrowLayout()Lcom/flipkart/chatheads/ui/UpArrowLayout;
    .locals 1

    .prologue
    .line 303
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->arrowLayout:Lcom/flipkart/chatheads/ui/UpArrowLayout;

    if-nez v0, :cond_0

    .line 304
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v0}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getArrowLayout()Lcom/flipkart/chatheads/ui/UpArrowLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->arrowLayout:Lcom/flipkart/chatheads/ui/UpArrowLayout;

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->arrowLayout:Lcom/flipkart/chatheads/ui/UpArrowLayout;

    return-object v0
.end method

.method private getBundleWithHero()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 418
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->extras:Landroid/os/Bundle;

    .line 419
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v0, :cond_0

    .line 420
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "bundle":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 422
    .restart local v0    # "bundle":Landroid/os/Bundle;
    :cond_0
    const-string v1, "hero_index"

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->getHeroIndex()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 423
    return-object v0
.end method

.method private getNextBestChatHead()Lcom/flipkart/chatheads/ui/ChatHead;
    .locals 5

    .prologue
    .line 406
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    const/4 v1, 0x0

    .line 407
    .local v1, "nextBestChatHead":Lcom/flipkart/chatheads/ui/ChatHead;
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v2}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getChatHeads()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flipkart/chatheads/ui/ChatHead;

    .line 408
    .local v0, "head":Lcom/flipkart/chatheads/ui/ChatHead;
    if-nez v1, :cond_1

    .line 409
    move-object v1, v0

    goto :goto_0

    .line 410
    :cond_1
    invoke-virtual {v0}, Lcom/flipkart/chatheads/ui/ChatHead;->getUnreadCount()I

    move-result v3

    invoke-virtual {v1}, Lcom/flipkart/chatheads/ui/ChatHead;->getUnreadCount()I

    move-result v4

    if-lt v3, v4, :cond_0

    .line 411
    move-object v1, v0

    goto :goto_0

    .line 414
    .end local v0    # "head":Lcom/flipkart/chatheads/ui/ChatHead;
    :cond_2
    return-object v1
.end method

.method private hideView()V
    .locals 2

    .prologue
    .line 318
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    iget-boolean v1, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->isChatHeadCountChanging:Z

    if-nez v1, :cond_0

    .line 319
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->getArrowLayout()Lcom/flipkart/chatheads/ui/UpArrowLayout;

    move-result-object v0

    .line 320
    .local v0, "arrowLayout":Lcom/flipkart/chatheads/ui/UpArrowLayout;
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->setVisibility(I)V

    .line 322
    .end local v0    # "arrowLayout":Lcom/flipkart/chatheads/ui/UpArrowLayout;
    :cond_0
    return-void
.end method

.method private isViewHidden()Z
    .locals 4

    .prologue
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    const/4 v1, 0x1

    .line 310
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->getArrowLayout()Lcom/flipkart/chatheads/ui/UpArrowLayout;

    move-result-object v0

    .line 311
    .local v0, "arrowLayout":Lcom/flipkart/chatheads/ui/UpArrowLayout;
    if-eqz v0, :cond_0

    .line 312
    invoke-virtual {v0}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_1

    .line 314
    :cond_0
    :goto_0
    return v1

    .line 312
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private pointTo(Lcom/flipkart/chatheads/ui/ChatHead;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 344
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    .local p1, "activeChatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->getArrowLayout()Lcom/flipkart/chatheads/ui/UpArrowLayout;

    move-result-object v0

    .line 345
    .local v0, "arrowLayout":Lcom/flipkart/chatheads/ui/UpArrowLayout;
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->getArrowLayout()Lcom/flipkart/chatheads/ui/UpArrowLayout;

    move-result-object v3

    invoke-virtual {v3}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->removeAllViews()V

    .line 346
    iget-object v3, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v3, p1, v0}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->attachView(Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)Landroid/view/View;

    .line 347
    iget-object v3, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v3}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getOverlayView()Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;

    move-result-object v3

    invoke-static {v3}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->sendViewToBack(Landroid/view/View;)V

    .line 348
    iget-object v3, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->positions:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Point;

    .line 349
    .local v2, "point":Landroid/graphics/Point;
    if-eqz v2, :cond_0

    .line 350
    iget-object v3, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v3}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getConfig()Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    move-result-object v3

    iget v4, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->maxWidth:I

    iget v5, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->maxHeight:I

    invoke-virtual {v3, v4, v5}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->getHeadVerticalSpacing(II)I

    move-result v1

    .line 351
    .local v1, "padding":I
    iget v3, v2, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v4}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getConfig()Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    move-result-object v4

    invoke-virtual {v4}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->getHeadWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    iget v4, v2, Landroid/graphics/Point;->y:I

    iget-object v5, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v5}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getConfig()Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    move-result-object v5

    invoke-virtual {v5}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->getHeadHeight()I

    move-result v5

    add-int/2addr v4, v5

    add-int/2addr v4, v1

    invoke-virtual {v0, v3, v4}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->pointTo(II)V

    .line 353
    .end local v1    # "padding":I
    :cond_0
    return-void
.end method

.method private positionToOriginal(Lcom/flipkart/chatheads/ui/ChatHead;Lcom/facebook/rebound/Spring;Lcom/facebook/rebound/Spring;)V
    .locals 12
    .param p1, "activeChatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;
    .param p2, "activeHorizontalSpring"    # Lcom/facebook/rebound/Spring;
    .param p3, "activeVerticalSpring"    # Lcom/facebook/rebound/Spring;

    .prologue
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    const-wide/16 v10, 0x0

    .line 196
    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->isSticky()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 197
    iget-object v3, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->positions:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Point;

    .line 198
    .local v2, "point":Landroid/graphics/Point;
    if-eqz v2, :cond_1

    .line 199
    iget v3, v2, Landroid/graphics/Point;->x:I

    int-to-double v4, v3

    invoke-virtual {p2}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v6

    sub-double/2addr v4, v6

    iget v3, v2, Landroid/graphics/Point;->y:I

    int-to-double v6, v3

    invoke-virtual {p3}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v8

    sub-double/2addr v6, v8

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    .line 200
    .local v0, "distanceFromOriginal":D
    sget-wide v4, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->MAX_DISTANCE_FROM_ORIGINAL:D

    cmpl-double v3, v0, v4

    if-lez v3, :cond_1

    .line 201
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->deactivate()V

    .line 220
    .end local v0    # "distanceFromOriginal":D
    .end local v2    # "point":Landroid/graphics/Point;
    :cond_0
    :goto_0
    return-void

    .line 208
    :cond_1
    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getState()Lcom/flipkart/chatheads/ui/ChatHead$State;

    move-result-object v3

    sget-object v4, Lcom/flipkart/chatheads/ui/ChatHead$State;->FREE:Lcom/flipkart/chatheads/ui/ChatHead$State;

    if-ne v3, v4, :cond_0

    .line 209
    iget-object v3, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->positions:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Point;

    .line 210
    .restart local v2    # "point":Landroid/graphics/Point;
    if-eqz v2, :cond_0

    .line 211
    sget-object v3, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    invoke-virtual {p2, v3}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 212
    invoke-virtual {p2, v10, v11}, Lcom/facebook/rebound/Spring;->setVelocity(D)Lcom/facebook/rebound/Spring;

    .line 213
    iget v3, v2, Landroid/graphics/Point;->x:I

    int-to-double v4, v3

    invoke-virtual {p2, v4, v5}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    .line 214
    sget-object v3, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    invoke-virtual {p3, v3}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 215
    invoke-virtual {p3, v10, v11}, Lcom/facebook/rebound/Spring;->setVelocity(D)Lcom/facebook/rebound/Spring;

    .line 216
    iget v3, v2, Landroid/graphics/Point;->y:I

    int-to-double v4, v3

    invoke-virtual {p3, v4, v5}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    goto :goto_0
.end method

.method private selectTab(Lcom/flipkart/chatheads/ui/ChatHead;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 183
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    .local p1, "activeChatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->currentChatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    if-eq v0, p1, :cond_0

    .line 184
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->currentChatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-direct {p0, v0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->detach(Lcom/flipkart/chatheads/ui/ChatHead;)V

    .line 185
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->currentChatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    .line 187
    :cond_0
    invoke-direct {p0, p1}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->pointTo(Lcom/flipkart/chatheads/ui/ChatHead;)V

    .line 188
    invoke-direct {p0, p1}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->showOrHideView(Lcom/flipkart/chatheads/ui/ChatHead;)V

    .line 189
    return-void
.end method

.method public static sendViewToBack(Landroid/view/View;)V
    .locals 2
    .param p0, "child"    # Landroid/view/View;

    .prologue
    .line 336
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 337
    .local v0, "parent":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v1

    if-eqz v1, :cond_0

    .line 338
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 339
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 341
    :cond_0
    return-void
.end method

.method private showOrHideView(Lcom/flipkart/chatheads/ui/ChatHead;)V
    .locals 12
    .param p1, "activeChatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;

    .prologue
    .line 288
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->positions:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/Point;

    .line 289
    .local v8, "point":Landroid/graphics/Point;
    if-eqz v8, :cond_0

    .line 290
    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getHorizontalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v0

    iget v9, v8, Landroid/graphics/Point;->x:I

    int-to-double v10, v9

    sub-double v2, v0, v10

    .line 291
    .local v2, "dx":D
    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getVerticalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v0

    iget v9, v8, Landroid/graphics/Point;->y:I

    int-to-double v10, v9

    sub-double v4, v0, v10

    .line 292
    .local v4, "dy":D
    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v6

    .line 293
    .local v6, "distanceFromOriginal":D
    iget v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->maxDistanceFromOriginal:I

    int-to-double v0, v0

    cmpg-double v0, v6, v0

    if-gez v0, :cond_1

    move-object v0, p0

    move-object v1, p1

    .line 294
    invoke-direct/range {v0 .. v7}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->showView(Lcom/flipkart/chatheads/ui/ChatHead;DDD)V

    .line 300
    .end local v2    # "dx":D
    .end local v4    # "dy":D
    .end local v6    # "distanceFromOriginal":D
    :cond_0
    :goto_0
    return-void

    .line 296
    .restart local v2    # "dx":D
    .restart local v4    # "dy":D
    .restart local v6    # "distanceFromOriginal":D
    :cond_1
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->hideView()V

    goto :goto_0
.end method

.method private showView(Lcom/flipkart/chatheads/ui/ChatHead;DDD)V
    .locals 4
    .param p1, "activeChatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;
    .param p2, "dx"    # D
    .param p4, "dy"    # D
    .param p6, "distanceFromOriginal"    # D

    .prologue
    .line 325
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->getArrowLayout()Lcom/flipkart/chatheads/ui/UpArrowLayout;

    move-result-object v0

    .line 326
    .local v0, "arrowLayout":Lcom/flipkart/chatheads/ui/UpArrowLayout;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->setVisibility(I)V

    .line 328
    iget-boolean v1, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->isChatHeadCountChanging:Z

    if-nez v1, :cond_0

    .line 329
    double-to-float v1, p2

    invoke-virtual {v0, v1}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->setTranslationX(F)V

    .line 330
    double-to-float v1, p4

    invoke-virtual {v0, v1}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->setTranslationY(F)V

    .line 331
    const/high16 v1, 0x3f800000    # 1.0f

    double-to-float v2, p6

    iget v3, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->maxDistanceFromOriginal:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/flipkart/chatheads/ui/UpArrowLayout;->setAlpha(F)V

    .line 333
    :cond_0
    return-void
.end method


# virtual methods
.method public bringToFront(Lcom/flipkart/chatheads/ui/ChatHead;)V
    .locals 0
    .param p1, "chatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;

    .prologue
    .line 481
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    invoke-virtual {p0, p1}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->selectChatHead(Lcom/flipkart/chatheads/ui/ChatHead;)V

    .line 482
    return-void
.end method

.method public canDrag(Lcom/flipkart/chatheads/ui/ChatHead;)Z
    .locals 1
    .param p1, "chatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;

    .prologue
    .line 460
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->isSticky()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 461
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getHeroIndex()Ljava/lang/Integer;
    .locals 6

    .prologue
    .line 436
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    const/4 v2, 0x0

    .line 437
    .local v2, "heroIndex":I
    iget-object v4, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v4}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getChatHeads()Ljava/util/List;

    move-result-object v1

    .line 438
    .local v1, "chatHeads":Ljava/util/List;, "Ljava/util/List<Lcom/flipkart/chatheads/ui/ChatHead<TT;>;>;"
    const/4 v3, 0x0

    .line 439
    .local v3, "i":I
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flipkart/chatheads/ui/ChatHead;

    .line 440
    .local v0, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    iget-object v5, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->currentChatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    if-ne v5, v0, :cond_0

    .line 441
    move v2, v3

    .line 443
    :cond_0
    add-int/lit8 v3, v3, 0x1

    .line 444
    goto :goto_0

    .line 445
    .end local v0    # "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    :cond_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    return-object v4
.end method

.method public getRetainBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 455
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->getBundleWithHero()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public handleTouchUp(Lcom/flipkart/chatheads/ui/ChatHead;IILcom/facebook/rebound/Spring;Lcom/facebook/rebound/Spring;Z)Z
    .locals 4
    .param p1, "activeChatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;
    .param p2, "xVelocity"    # I
    .param p3, "yVelocity"    # I
    .param p4, "activeHorizontalSpring"    # Lcom/facebook/rebound/Spring;
    .param p5, "activeVerticalSpring"    # Lcom/facebook/rebound/Spring;
    .param p6, "wasDragging"    # Z

    .prologue
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    const/4 v1, 0x1

    .line 153
    if-nez p2, :cond_0

    if-nez p3, :cond_0

    .line 156
    const/4 p2, 0x1

    .line 157
    const/4 p3, 0x1

    .line 160
    :cond_0
    int-to-double v2, p2

    invoke-virtual {p4, v2, v3}, Lcom/facebook/rebound/Spring;->setVelocity(D)Lcom/facebook/rebound/Spring;

    .line 161
    int-to-double v2, p3

    invoke-virtual {p5, v2, v3}, Lcom/facebook/rebound/Spring;->setVelocity(D)Lcom/facebook/rebound/Spring;

    .line 164
    if-eqz p6, :cond_2

    move v0, v1

    .line 178
    :cond_1
    :goto_0
    return v0

    .line 167
    :cond_2
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->currentChatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    if-eq p1, v2, :cond_3

    .line 168
    iget-object v2, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v2, p1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->onItemSelected(Lcom/flipkart/chatheads/ui/ChatHead;)Z

    move-result v0

    .line 169
    .local v0, "handled":Z
    if-nez v0, :cond_3

    .line 170
    invoke-direct {p0, p1}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->selectTab(Lcom/flipkart/chatheads/ui/ChatHead;)V

    move v0, v1

    .line 171
    goto :goto_0

    .line 174
    .end local v0    # "handled":Z
    :cond_3
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v1, p1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->onItemSelected(Lcom/flipkart/chatheads/ui/ChatHead;)Z

    move-result v0

    .line 175
    .restart local v0    # "handled":Z
    if-nez v0, :cond_1

    .line 176
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->deactivate()V

    goto :goto_0
.end method

.method public onActivate(Lcom/flipkart/chatheads/ui/ChatHeadManager;Landroid/os/Bundle;IIZ)V
    .locals 15
    .param p1, "container"    # Lcom/flipkart/chatheads/ui/ChatHeadManager;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "maxWidth"    # I
    .param p4, "maxHeight"    # I
    .param p5, "animated"    # Z

    .prologue
    .line 49
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    const/4 v12, 0x1

    iput-boolean v12, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->isTransitioning:Z

    .line 50
    move-object/from16 v0, p1

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    .line 51
    move/from16 v0, p3

    iput v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->maxWidth:I

    .line 52
    move/from16 v0, p4

    iput v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->maxHeight:I

    .line 53
    invoke-interface/range {p1 .. p1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v12

    const/16 v13, 0x32

    invoke-static {v12, v13}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/util/DisplayMetrics;I)I

    move-result v12

    sput v12, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->MIN_VELOCITY_TO_POSITION_BACK:I

    .line 54
    invoke-interface/range {p1 .. p1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getContext()Landroid/content/Context;

    move-result-object v12

    const/16 v13, 0xa

    invoke-static {v12, v13}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v12

    int-to-double v12, v12

    sput-wide v12, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->MAX_DISTANCE_FROM_ORIGINAL:D

    .line 55
    const/4 v12, 0x1

    iput-boolean v12, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->isActive:Z

    .line 56
    invoke-interface/range {p1 .. p1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getChatHeads()Ljava/util/List;

    move-result-object v3

    .line 57
    .local v3, "chatHeads":Ljava/util/List;, "Ljava/util/List<Lcom/flipkart/chatheads/ui/ChatHead;>;"
    const/4 v4, 0x0

    .line 58
    .local v4, "heroIndex":I
    move-object/from16 v0, p2

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->extras:Landroid/os/Bundle;

    .line 59
    if-eqz p2, :cond_0

    .line 60
    const-string v12, "hero_index"

    const/4 v13, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 61
    :cond_0
    if-gez v4, :cond_1

    iget-object v12, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->currentChatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    if-eqz v12, :cond_1

    .line 62
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->getHeroIndex()Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 64
    :cond_1
    if-ltz v4, :cond_2

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    if-le v4, v12, :cond_3

    .line 65
    :cond_2
    const/4 v4, 0x0

    .line 67
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v12

    if-lez v12, :cond_7

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v12

    if-ge v4, v12, :cond_7

    .line 68
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/flipkart/chatheads/ui/ChatHead;

    iput-object v12, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->currentChatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    .line 69
    sget-wide v12, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->MAX_DISTANCE_FROM_ORIGINAL:D

    double-to-int v12, v12

    iput v12, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->maxDistanceFromOriginal:I

    .line 71
    invoke-interface/range {p1 .. p1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getConfig()Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    move-result-object v12

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {v12, v0, v1}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->getHeadHorizontalSpacing(II)I

    move-result v8

    .line 72
    .local v8, "spacing":I
    invoke-interface/range {p1 .. p1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getConfig()Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    move-result-object v12

    invoke-virtual {v12}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->getHeadWidth()I

    move-result v10

    .line 73
    .local v10, "widthPerHead":I
    invoke-interface/range {p1 .. p1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getContext()Landroid/content/Context;

    move-result-object v12

    const/4 v13, 0x5

    invoke-static {v12, v13}, Lcom/flipkart/chatheads/ChatHeadUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v12

    iput v12, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->topPadding:I

    .line 74
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v12

    add-int v13, v10, v8

    mul-int/2addr v12, v13

    sub-int v7, p3, v12

    .line 75
    .local v7, "leftIndent":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v12

    if-ge v6, v12, :cond_6

    .line 76
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/flipkart/chatheads/ui/ChatHead;

    .line 77
    .local v2, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;
    invoke-virtual {v2}, Lcom/flipkart/chatheads/ui/ChatHead;->getHorizontalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v5

    .line 78
    .local v5, "horizontalSpring":Lcom/facebook/rebound/Spring;
    add-int v12, v10, v8

    mul-int/2addr v12, v6

    add-int v11, v7, v12

    .line 79
    .local v11, "xPos":I
    iget-object v12, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->positions:Ljava/util/Map;

    new-instance v13, Landroid/graphics/Point;

    iget v14, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->topPadding:I

    invoke-direct {v13, v11, v14}, Landroid/graphics/Point;-><init>(II)V

    invoke-interface {v12, v2, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    invoke-virtual {v5}, Lcom/facebook/rebound/Spring;->setAtRest()Lcom/facebook/rebound/Spring;

    .line 81
    sget-object v12, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    invoke-virtual {v5, v12}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 82
    int-to-double v12, v11

    invoke-virtual {v5, v12, v13}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    .line 83
    if-nez p5, :cond_4

    .line 84
    int-to-double v12, v11

    invoke-virtual {v5, v12, v13}, Lcom/facebook/rebound/Spring;->setCurrentValue(D)Lcom/facebook/rebound/Spring;

    .line 86
    :cond_4
    invoke-virtual {v2}, Lcom/flipkart/chatheads/ui/ChatHead;->getVerticalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v9

    .line 87
    .local v9, "verticalSpring":Lcom/facebook/rebound/Spring;
    invoke-virtual {v9}, Lcom/facebook/rebound/Spring;->setAtRest()Lcom/facebook/rebound/Spring;

    .line 88
    sget-object v12, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    invoke-virtual {v9, v12}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 89
    iget v12, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->topPadding:I

    int-to-double v12, v12

    invoke-virtual {v9, v12, v13}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    .line 90
    if-nez p5, :cond_5

    .line 91
    iget v12, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->topPadding:I

    int-to-double v12, v12

    invoke-virtual {v9, v12, v13}, Lcom/facebook/rebound/Spring;->setCurrentValue(D)Lcom/facebook/rebound/Spring;

    .line 75
    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 95
    .end local v2    # "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;
    .end local v5    # "horizontalSpring":Lcom/facebook/rebound/Spring;
    .end local v9    # "verticalSpring":Lcom/facebook/rebound/Spring;
    .end local v11    # "xPos":I
    :cond_6
    invoke-interface/range {p1 .. p1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getCloseButton()Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    move-result-object v12

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->setEnabled(Z)V

    .line 96
    invoke-interface/range {p1 .. p1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getOverlayView()Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;

    move-result-object v12

    new-instance v13, Lcom/flipkart/chatheads/ui/MaximizedArrangement$1;

    invoke-direct {v13, p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement$1;-><init>(Lcom/flipkart/chatheads/ui/MaximizedArrangement;)V

    invoke-virtual {v12, v13}, Lcom/flipkart/chatheads/ui/ChatHeadOverlayView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    move-object/from16 v0, p1

    move/from16 v1, p5

    invoke-interface {v0, v1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->showOverlayView(Z)V

    .line 103
    iget-object v12, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->currentChatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {p0, v12}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->selectChatHead(Lcom/flipkart/chatheads/ui/ChatHead;)V

    .line 104
    iget-object v12, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->currentChatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v12}, Lcom/flipkart/chatheads/ui/ChatHead;->getVerticalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v12

    new-instance v13, Lcom/flipkart/chatheads/ui/MaximizedArrangement$2;

    invoke-direct {v13, p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement$2;-><init>(Lcom/flipkart/chatheads/ui/MaximizedArrangement;)V

    invoke-virtual {v12, v13}, Lcom/facebook/rebound/Spring;->addListener(Lcom/facebook/rebound/SpringListener;)Lcom/facebook/rebound/Spring;

    .line 119
    iget-object v12, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->currentChatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v12}, Lcom/flipkart/chatheads/ui/ChatHead;->getHorizontalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v12

    new-instance v13, Lcom/flipkart/chatheads/ui/MaximizedArrangement$3;

    invoke-direct {v13, p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement$3;-><init>(Lcom/flipkart/chatheads/ui/MaximizedArrangement;)V

    invoke-virtual {v12, v13}, Lcom/facebook/rebound/Spring;->addListener(Lcom/facebook/rebound/SpringListener;)Lcom/facebook/rebound/Spring;

    .line 134
    .end local v6    # "i":I
    .end local v7    # "leftIndent":I
    .end local v8    # "spacing":I
    .end local v10    # "widthPerHead":I
    :cond_7
    return-void
.end method

.method public onCapture(Lcom/flipkart/chatheads/ui/ChatHeadManager;Lcom/flipkart/chatheads/ui/ChatHead;)V
    .locals 2
    .param p1, "container"    # Lcom/flipkart/chatheads/ui/ChatHeadManager;
    .param p2, "activeChatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;

    .prologue
    .line 394
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    invoke-virtual {p2}, Lcom/flipkart/chatheads/ui/ChatHead;->isSticky()Z

    move-result v0

    if-nez v0, :cond_0

    .line 395
    invoke-virtual {p2}, Lcom/flipkart/chatheads/ui/ChatHead;->getKey()Ljava/io/Serializable;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->removeChatHead(Ljava/io/Serializable;Z)Z

    .line 397
    :cond_0
    return-void
.end method

.method public onChatHeadAdded(Lcom/flipkart/chatheads/ui/ChatHead;Z)V
    .locals 7
    .param p1, "chatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;
    .param p2, "animated"    # Z

    .prologue
    .line 358
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getHorizontalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v6

    .line 359
    .local v6, "spring":Lcom/facebook/rebound/Spring;
    iget v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->maxWidth:I

    int-to-double v0, v0

    invoke-virtual {v6, v0, v1}, Lcom/facebook/rebound/Spring;->setCurrentValue(D)Lcom/facebook/rebound/Spring;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/rebound/Spring;->setAtRest()Lcom/facebook/rebound/Spring;

    .line 360
    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getVerticalSpring()Lcom/facebook/rebound/Spring;

    move-result-object v6

    .line 361
    iget v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->topPadding:I

    int-to-double v0, v0

    invoke-virtual {v6, v0, v1}, Lcom/facebook/rebound/Spring;->setCurrentValue(D)Lcom/facebook/rebound/Spring;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/rebound/Spring;->setAtRest()Lcom/facebook/rebound/Spring;

    .line 363
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->isChatHeadCountChanging:Z

    .line 365
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->getBundleWithHero()Landroid/os/Bundle;

    move-result-object v2

    iget v3, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->maxWidth:I

    iget v4, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->maxHeight:I

    move-object v0, p0

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->onActivate(Lcom/flipkart/chatheads/ui/ChatHeadManager;Landroid/os/Bundle;IIZ)V

    .line 366
    return-void
.end method

.method public onChatHeadRemoved(Lcom/flipkart/chatheads/ui/ChatHead;)V
    .locals 8
    .param p1, "removed"    # Lcom/flipkart/chatheads/ui/ChatHead;

    .prologue
    .line 370
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->getArrowLayout()Lcom/flipkart/chatheads/ui/UpArrowLayout;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->detachView(Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)V

    .line 371
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->getArrowLayout()Lcom/flipkart/chatheads/ui/UpArrowLayout;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->removeView(Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)V

    .line 372
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->positions:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 373
    const/4 v6, 0x0

    .line 374
    .local v6, "isEmpty":Z
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->currentChatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    if-ne v0, p1, :cond_0

    .line 375
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->getNextBestChatHead()Lcom/flipkart/chatheads/ui/ChatHead;

    move-result-object v7

    .line 376
    .local v7, "nextBestChatHead":Lcom/flipkart/chatheads/ui/ChatHead;
    if-eqz v7, :cond_1

    .line 377
    const/4 v6, 0x0

    .line 378
    invoke-direct {p0, v7}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->selectTab(Lcom/flipkart/chatheads/ui/ChatHead;)V

    .line 383
    .end local v7    # "nextBestChatHead":Lcom/flipkart/chatheads/ui/ChatHead;
    :cond_0
    :goto_0
    if-nez v6, :cond_2

    .line 384
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->getBundleWithHero()Landroid/os/Bundle;

    move-result-object v2

    iget v3, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->maxWidth:I

    iget v4, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->maxHeight:I

    const/4 v5, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->onActivate(Lcom/flipkart/chatheads/ui/ChatHeadManager;Landroid/os/Bundle;IIZ)V

    .line 389
    :goto_1
    return-void

    .line 380
    .restart local v7    # "nextBestChatHead":Lcom/flipkart/chatheads/ui/ChatHead;
    :cond_1
    const/4 v6, 0x1

    goto :goto_0

    .line 386
    .end local v7    # "nextBestChatHead":Lcom/flipkart/chatheads/ui/ChatHead;
    :cond_2
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->deactivate()V

    goto :goto_1
.end method

.method public onConfigChanged(Lcom/flipkart/chatheads/ui/ChatHeadConfig;)V
    .locals 0
    .param p1, "newConfig"    # Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    .prologue
    .line 451
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    return-void
.end method

.method public onDeactivate(II)V
    .locals 3
    .param p1, "maxWidth"    # I
    .param p2, "maxHeight"    # I

    .prologue
    .line 139
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->currentChatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    iget-object v1, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->currentChatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->getArrowLayout()Lcom/flipkart/chatheads/ui/UpArrowLayout;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->detachView(Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)V

    .line 142
    :cond_0
    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->hideView()V

    .line 143
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->hideOverlayView(Z)V

    .line 144
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->positions:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 145
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->isActive:Z

    .line 146
    return-void
.end method

.method public onReloadFragment(Lcom/flipkart/chatheads/ui/ChatHead;)V
    .locals 2
    .param p1, "chatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;

    .prologue
    .line 486
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->currentChatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->currentChatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    if-ne p1, v0, :cond_0

    .line 487
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-direct {p0}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->getArrowLayout()Lcom/flipkart/chatheads/ui/UpArrowLayout;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->attachView(Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)Landroid/view/View;

    .line 489
    :cond_0
    return-void
.end method

.method public onSpringUpdate(Lcom/flipkart/chatheads/ui/ChatHead;ZIILcom/facebook/rebound/Spring;Lcom/facebook/rebound/Spring;Lcom/facebook/rebound/Spring;I)V
    .locals 14
    .param p1, "activeChatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;
    .param p2, "isDragging"    # Z
    .param p3, "maxWidth"    # I
    .param p4, "maxHeight"    # I
    .param p5, "spring"    # Lcom/facebook/rebound/Spring;
    .param p6, "activeHorizontalSpring"    # Lcom/facebook/rebound/Spring;
    .param p7, "activeVerticalSpring"    # Lcom/facebook/rebound/Spring;
    .param p8, "totalVelocity"    # I

    .prologue
    .line 225
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    move-object/from16 v0, p5

    move-object/from16 v1, p6

    if-ne v0, v1, :cond_8

    if-nez p2, :cond_8

    .line 226
    invoke-virtual/range {p6 .. p6}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v6

    .line 227
    .local v6, "xPosition":D
    iget-object v3, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v3}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getConfig()Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->getHeadWidth()I

    move-result v3

    int-to-double v10, v3

    add-double/2addr v10, v6

    move/from16 v0, p3

    int-to-double v12, v0

    cmpl-double v3, v10, v12

    if-lez v3, :cond_0

    invoke-virtual/range {p6 .. p6}, Lcom/facebook/rebound/Spring;->getSpringConfig()Lcom/facebook/rebound/SpringConfig;

    move-result-object v3

    sget-object v10, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    if-eq v3, v10, :cond_0

    invoke-virtual/range {p6 .. p6}, Lcom/facebook/rebound/Spring;->isOvershooting()Z

    move-result v3

    if-nez v3, :cond_0

    .line 228
    move-object/from16 v0, p6

    move-object/from16 v1, p7

    invoke-direct {p0, p1, v0, v1}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->positionToOriginal(Lcom/flipkart/chatheads/ui/ChatHead;Lcom/facebook/rebound/Spring;Lcom/facebook/rebound/Spring;)V

    .line 230
    :cond_0
    const-wide/16 v10, 0x0

    cmpg-double v3, v6, v10

    if-gez v3, :cond_1

    invoke-virtual/range {p6 .. p6}, Lcom/facebook/rebound/Spring;->getSpringConfig()Lcom/facebook/rebound/SpringConfig;

    move-result-object v3

    sget-object v10, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    if-eq v3, v10, :cond_1

    invoke-virtual/range {p6 .. p6}, Lcom/facebook/rebound/Spring;->isOvershooting()Z

    move-result v3

    if-nez v3, :cond_1

    .line 231
    move-object/from16 v0, p6

    move-object/from16 v1, p7

    invoke-direct {p0, p1, v0, v1}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->positionToOriginal(Lcom/flipkart/chatheads/ui/ChatHead;Lcom/facebook/rebound/Spring;Lcom/facebook/rebound/Spring;)V

    .line 246
    .end local v6    # "xPosition":D
    :cond_1
    :goto_0
    if-nez p2, :cond_2

    sget v3, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->MIN_VELOCITY_TO_POSITION_BACK:I

    move/from16 v0, p8

    if-ge v0, v3, :cond_2

    invoke-virtual/range {p6 .. p6}, Lcom/facebook/rebound/Spring;->getSpringConfig()Lcom/facebook/rebound/SpringConfig;

    move-result-object v3

    sget-object v10, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->DRAGGING:Lcom/facebook/rebound/SpringConfig;

    if-ne v3, v10, :cond_2

    .line 247
    move-object/from16 v0, p6

    move-object/from16 v1, p7

    invoke-direct {p0, p1, v0, v1}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->positionToOriginal(Lcom/flipkart/chatheads/ui/ChatHead;Lcom/facebook/rebound/Spring;Lcom/facebook/rebound/Spring;)V

    .line 251
    :cond_2
    iget-object v3, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->currentChatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    if-ne p1, v3, :cond_3

    .line 253
    invoke-direct {p0, p1}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->showOrHideView(Lcom/flipkart/chatheads/ui/ChatHead;)V

    .line 255
    :cond_3
    if-nez p2, :cond_7

    .line 257
    iget-object v3, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v3, p1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getChatHeadCoordsForCloseButton(Lcom/flipkart/chatheads/ui/ChatHead;)[I

    move-result-object v2

    .line 258
    .local v2, "coords":[I
    iget-object v3, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-virtual/range {p6 .. p6}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v10

    double-to-float v10, v10

    iget-object v11, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v11}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getConfig()Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    move-result-object v11

    invoke-virtual {v11}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->getHeadWidth()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    int-to-float v11, v11

    add-float/2addr v10, v11

    invoke-virtual/range {p7 .. p7}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v12

    double-to-float v11, v12

    iget-object v12, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v12}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getConfig()Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    move-result-object v12

    invoke-virtual {v12}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->getHeadHeight()I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    int-to-float v12, v12

    add-float/2addr v11, v12

    invoke-interface {v3, v10, v11}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getDistanceCloseButtonFromHead(FF)D

    move-result-wide v4

    .line 260
    .local v4, "distanceCloseButtonFromHead":D
    iget v3, p1, Lcom/flipkart/chatheads/ui/ChatHead;->CLOSE_ATTRACTION_THRESHOLD:I

    int-to-double v10, v3

    cmpg-double v3, v4, v10

    if-gez v3, :cond_4

    invoke-virtual/range {p6 .. p6}, Lcom/facebook/rebound/Spring;->getSpringConfig()Lcom/facebook/rebound/SpringConfig;

    move-result-object v3

    sget-object v10, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->DRAGGING:Lcom/facebook/rebound/SpringConfig;

    if-ne v3, v10, :cond_4

    invoke-virtual/range {p7 .. p7}, Lcom/facebook/rebound/Spring;->getSpringConfig()Lcom/facebook/rebound/SpringConfig;

    move-result-object v3

    sget-object v10, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->DRAGGING:Lcom/facebook/rebound/SpringConfig;

    if-ne v3, v10, :cond_4

    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->isSticky()Z

    move-result v3

    if-nez v3, :cond_4

    .line 262
    sget-object v3, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    move-object/from16 v0, p6

    invoke-virtual {v0, v3}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 263
    sget-object v3, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    move-object/from16 v0, p7

    invoke-virtual {v0, v3}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 264
    sget-object v3, Lcom/flipkart/chatheads/ui/ChatHead$State;->CAPTURED:Lcom/flipkart/chatheads/ui/ChatHead$State;

    invoke-virtual {p1, v3}, Lcom/flipkart/chatheads/ui/ChatHead;->setState(Lcom/flipkart/chatheads/ui/ChatHead$State;)V

    .line 266
    :cond_4
    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getState()Lcom/flipkart/chatheads/ui/ChatHead$State;

    move-result-object v3

    sget-object v10, Lcom/flipkart/chatheads/ui/ChatHead$State;->CAPTURED:Lcom/flipkart/chatheads/ui/ChatHead$State;

    if-ne v3, v10, :cond_5

    invoke-virtual/range {p6 .. p6}, Lcom/facebook/rebound/Spring;->getSpringConfig()Lcom/facebook/rebound/SpringConfig;

    move-result-object v3

    sget-object v10, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->CAPTURING:Lcom/facebook/rebound/SpringConfig;

    if-eq v3, v10, :cond_5

    .line 267
    invoke-virtual/range {p6 .. p6}, Lcom/facebook/rebound/Spring;->setAtRest()Lcom/facebook/rebound/Spring;

    .line 268
    invoke-virtual/range {p7 .. p7}, Lcom/facebook/rebound/Spring;->setAtRest()Lcom/facebook/rebound/Spring;

    .line 269
    sget-object v3, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->CAPTURING:Lcom/facebook/rebound/SpringConfig;

    move-object/from16 v0, p6

    invoke-virtual {v0, v3}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 270
    sget-object v3, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->CAPTURING:Lcom/facebook/rebound/SpringConfig;

    move-object/from16 v0, p7

    invoke-virtual {v0, v3}, Lcom/facebook/rebound/Spring;->setSpringConfig(Lcom/facebook/rebound/SpringConfig;)Lcom/facebook/rebound/Spring;

    .line 271
    const/4 v3, 0x0

    aget v3, v2, v3

    int-to-double v10, v3

    move-object/from16 v0, p6

    invoke-virtual {v0, v10, v11}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    .line 272
    const/4 v3, 0x1

    aget v3, v2, v3

    int-to-double v10, v3

    move-object/from16 v0, p7

    invoke-virtual {v0, v10, v11}, Lcom/facebook/rebound/Spring;->setEndValue(D)Lcom/facebook/rebound/Spring;

    .line 275
    :cond_5
    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->getState()Lcom/flipkart/chatheads/ui/ChatHead$State;

    move-result-object v3

    sget-object v10, Lcom/flipkart/chatheads/ui/ChatHead$State;->CAPTURED:Lcom/flipkart/chatheads/ui/ChatHead$State;

    if-ne v3, v10, :cond_6

    invoke-virtual/range {p7 .. p7}, Lcom/facebook/rebound/Spring;->isAtRest()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 276
    iget-object v3, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v3}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getCloseButton()Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    move-result-object v3

    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-virtual {v3, v10, v11}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->disappear(ZZ)V

    .line 277
    iget-object v3, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v3, p1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->captureChatHeads(Lcom/flipkart/chatheads/ui/ChatHead;)V

    .line 279
    :cond_6
    invoke-virtual/range {p7 .. p7}, Lcom/facebook/rebound/Spring;->isAtRest()Z

    move-result v3

    if-nez v3, :cond_a

    iget-boolean v3, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->isTransitioning:Z

    if-nez v3, :cond_a

    .line 280
    iget-object v3, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v3}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getCloseButton()Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    move-result-object v3

    invoke-virtual {v3}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->appear()V

    .line 285
    .end local v2    # "coords":[I
    .end local v4    # "distanceCloseButtonFromHead":D
    :cond_7
    :goto_1
    return-void

    .line 233
    :cond_8
    move-object/from16 v0, p5

    move-object/from16 v1, p7

    if-ne v0, v1, :cond_1

    if-nez p2, :cond_1

    .line 234
    invoke-virtual/range {p7 .. p7}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v8

    .line 236
    .local v8, "yPosition":D
    iget-object v3, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v3}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getConfig()Lcom/flipkart/chatheads/ui/ChatHeadConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/flipkart/chatheads/ui/ChatHeadConfig;->getHeadHeight()I

    move-result v3

    int-to-double v10, v3

    add-double/2addr v10, v8

    move/from16 v0, p4

    int-to-double v12, v0

    cmpl-double v3, v10, v12

    if-lez v3, :cond_9

    invoke-virtual/range {p6 .. p6}, Lcom/facebook/rebound/Spring;->getSpringConfig()Lcom/facebook/rebound/SpringConfig;

    move-result-object v3

    sget-object v10, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    if-eq v3, v10, :cond_9

    invoke-virtual/range {p6 .. p6}, Lcom/facebook/rebound/Spring;->isOvershooting()Z

    move-result v3

    if-nez v3, :cond_9

    .line 237
    move-object/from16 v0, p6

    move-object/from16 v1, p7

    invoke-direct {p0, p1, v0, v1}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->positionToOriginal(Lcom/flipkart/chatheads/ui/ChatHead;Lcom/facebook/rebound/Spring;Lcom/facebook/rebound/Spring;)V

    .line 239
    :cond_9
    const-wide/16 v10, 0x0

    cmpg-double v3, v8, v10

    if-gez v3, :cond_1

    invoke-virtual/range {p6 .. p6}, Lcom/facebook/rebound/Spring;->getSpringConfig()Lcom/facebook/rebound/SpringConfig;

    move-result-object v3

    sget-object v10, Lcom/flipkart/chatheads/ui/SpringConfigsHolder;->NOT_DRAGGING:Lcom/facebook/rebound/SpringConfig;

    if-eq v3, v10, :cond_1

    invoke-virtual/range {p6 .. p6}, Lcom/facebook/rebound/Spring;->isOvershooting()Z

    move-result v3

    if-nez v3, :cond_1

    .line 240
    move-object/from16 v0, p6

    move-object/from16 v1, p7

    invoke-direct {p0, p1, v0, v1}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->positionToOriginal(Lcom/flipkart/chatheads/ui/ChatHead;Lcom/facebook/rebound/Spring;Lcom/facebook/rebound/Spring;)V

    goto/16 :goto_0

    .line 282
    .end local v8    # "yPosition":D
    .restart local v2    # "coords":[I
    .restart local v4    # "distanceCloseButtonFromHead":D
    :cond_a
    iget-object v3, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v3}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getCloseButton()Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;

    move-result-object v3

    const/4 v10, 0x1

    const/4 v11, 0x1

    invoke-virtual {v3, v10, v11}, Lcom/flipkart/chatheads/ui/ChatHeadCloseButton;->disappear(ZZ)V

    goto :goto_1
.end method

.method public removeOldestChatHead()V
    .locals 4

    .prologue
    .line 466
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->isChatHeadCountChanging:Z

    .line 468
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-interface {v1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getChatHeads()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flipkart/chatheads/ui/ChatHead;

    .line 470
    .local v0, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    invoke-virtual {v0}, Lcom/flipkart/chatheads/ui/ChatHead;->isSticky()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->currentChatHead:Lcom/flipkart/chatheads/ui/ChatHead;

    if-eq v0, v2, :cond_0

    .line 471
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    invoke-virtual {v0}, Lcom/flipkart/chatheads/ui/ChatHead;->getKey()Ljava/io/Serializable;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->removeChatHead(Ljava/io/Serializable;Z)Z

    .line 475
    .end local v0    # "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    :cond_1
    return-void
.end method

.method public selectChatHead(Lcom/flipkart/chatheads/ui/ChatHead;)V
    .locals 0
    .param p1, "chatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;

    .prologue
    .line 401
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    invoke-direct {p0, p1}, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->selectTab(Lcom/flipkart/chatheads/ui/ChatHead;)V

    .line 402
    return-void
.end method

.method public setContainer(Lcom/flipkart/chatheads/ui/ChatHeadManager;)V
    .locals 0
    .param p1, "container"    # Lcom/flipkart/chatheads/ui/ChatHeadManager;

    .prologue
    .line 44
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/MaximizedArrangement;->manager:Lcom/flipkart/chatheads/ui/ChatHeadManager;

    .line 45
    return-void
.end method

.method public shouldShowCloseButton(Lcom/flipkart/chatheads/ui/ChatHead;)Z
    .locals 1
    .param p1, "chatHead"    # Lcom/flipkart/chatheads/ui/ChatHead;

    .prologue
    .line 493
    .local p0, "this":Lcom/flipkart/chatheads/ui/MaximizedArrangement;, "Lcom/flipkart/chatheads/ui/MaximizedArrangement<TT;>;"
    invoke-virtual {p1}, Lcom/flipkart/chatheads/ui/ChatHead;->isSticky()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 494
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
