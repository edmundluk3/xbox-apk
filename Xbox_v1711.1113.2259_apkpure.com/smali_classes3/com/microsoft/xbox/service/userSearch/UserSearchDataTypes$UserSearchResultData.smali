.class public final Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;
.super Ljava/lang/Object;
.source "UserSearchDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UserSearchResultData"
.end annotation


# instance fields
.field public final displayPicUri:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final gamertag:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private volatile transient hashCode:I

.field public final id:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "gamertag"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "displayPicUri"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 108
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 110
    iput-object p1, p0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->id:Ljava/lang/String;

    .line 111
    iput-object p2, p0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->gamertag:Ljava/lang/String;

    .line 112
    iput-object p3, p0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->displayPicUri:Ljava/lang/String;

    .line 113
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 117
    if-ne p1, p0, :cond_0

    .line 118
    const/4 v1, 0x1

    .line 123
    :goto_0
    return v1

    .line 119
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;

    if-nez v1, :cond_1

    .line 120
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 122
    check-cast v0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;

    .line 123
    .local v0, "other":Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;
    iget-object v1, p0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->id:Ljava/lang/String;

    iget-object v2, v0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 129
    iget v0, p0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->hashCode:I

    if-nez v0, :cond_0

    .line 130
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->hashCode:I

    .line 131
    iget v0, p0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->id:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->hashCode:I

    .line 134
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
