.class abstract Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;
.super Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;
.source "$AutoValue_ClubDataTypes_ClubNotificationDetails.java"


# instance fields
.field private final actorGamertag:Ljava/lang/String;

.field private final actorId:Ljava/lang/String;

.field private final clubId:Ljava/lang/String;

.field private final clubName:Ljava/lang/String;

.field private final notificationType:Ljava/lang/String;

.field private final targetGamertag:Ljava/lang/String;

.field private final targetId:Ljava/lang/String;

.field private final version:I


# direct methods
.method constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "version"    # I
    .param p2, "notificationType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "clubId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "clubName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "actorId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "actorGamertag"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p7, "targetId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p8, "targetGamertag"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;-><init>()V

    .line 28
    iput p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->version:I

    .line 29
    iput-object p2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->notificationType:Ljava/lang/String;

    .line 30
    iput-object p3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->clubId:Ljava/lang/String;

    .line 31
    iput-object p4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->clubName:Ljava/lang/String;

    .line 32
    iput-object p5, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->actorId:Ljava/lang/String;

    .line 33
    iput-object p6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->actorGamertag:Ljava/lang/String;

    .line 34
    iput-object p7, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->targetId:Ljava/lang/String;

    .line 35
    iput-object p8, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->targetGamertag:Ljava/lang/String;

    .line 36
    return-void
.end method


# virtual methods
.method public actorGamertag()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->actorGamertag:Ljava/lang/String;

    return-object v0
.end method

.method public actorId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->actorId:Ljava/lang/String;

    return-object v0
.end method

.method public clubId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->clubId:Ljava/lang/String;

    return-object v0
.end method

.method public clubName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->clubName:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 101
    if-ne p1, p0, :cond_1

    .line 115
    :cond_0
    :goto_0
    return v1

    .line 104
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;

    if-eqz v3, :cond_a

    move-object v0, p1

    .line 105
    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;

    .line 106
    .local v0, "that":Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;
    iget v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->version:I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->version()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->notificationType:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 107
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->notificationType()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->clubId:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 108
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->clubId()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->clubName:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 109
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->clubName()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->actorId:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 110
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->actorId()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->actorGamertag:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 111
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->actorGamertag()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->targetId:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 112
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->targetId()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->targetGamertag:Ljava/lang/String;

    if-nez v3, :cond_9

    .line 113
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->targetGamertag()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 107
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->notificationType:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->notificationType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 108
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->clubId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->clubId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 109
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->clubName:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->clubName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_3

    .line 110
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->actorId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->actorId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_4

    .line 111
    :cond_7
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->actorGamertag:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->actorGamertag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_5

    .line 112
    :cond_8
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->targetId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->targetId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_6

    .line 113
    :cond_9
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->targetGamertag:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;->targetGamertag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;
    :cond_a
    move v1, v2

    .line 115
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    const v3, 0xf4243

    .line 120
    const/4 v0, 0x1

    .line 121
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 122
    iget v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->version:I

    xor-int/2addr v0, v1

    .line 123
    mul-int/2addr v0, v3

    .line 124
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->notificationType:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 125
    mul-int/2addr v0, v3

    .line 126
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->clubId:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 127
    mul-int/2addr v0, v3

    .line 128
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->clubName:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 129
    mul-int/2addr v0, v3

    .line 130
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->actorId:Ljava/lang/String;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    xor-int/2addr v0, v1

    .line 131
    mul-int/2addr v0, v3

    .line 132
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->actorGamertag:Ljava/lang/String;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    xor-int/2addr v0, v1

    .line 133
    mul-int/2addr v0, v3

    .line 134
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->targetId:Ljava/lang/String;

    if-nez v1, :cond_5

    move v1, v2

    :goto_5
    xor-int/2addr v0, v1

    .line 135
    mul-int/2addr v0, v3

    .line 136
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->targetGamertag:Ljava/lang/String;

    if-nez v1, :cond_6

    :goto_6
    xor-int/2addr v0, v2

    .line 137
    return v0

    .line 124
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->notificationType:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 126
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->clubId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 128
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->clubName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 130
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->actorId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    .line 132
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->actorGamertag:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    .line 134
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->targetId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    .line 136
    :cond_6
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->targetGamertag:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_6
.end method

.method public notificationType()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->notificationType:Ljava/lang/String;

    return-object v0
.end method

.method public targetGamertag()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->targetGamertag:Ljava/lang/String;

    return-object v0
.end method

.method public targetId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->targetId:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClubNotificationDetails{version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->version:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", notificationType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->notificationType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clubId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->clubId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clubName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->clubName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", actorId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->actorId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", actorGamertag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->actorGamertag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", targetId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->targetId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", targetGamertag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->targetGamertag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public version()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubDataTypes_ClubNotificationDetails;->version:I

    return v0
.end method
