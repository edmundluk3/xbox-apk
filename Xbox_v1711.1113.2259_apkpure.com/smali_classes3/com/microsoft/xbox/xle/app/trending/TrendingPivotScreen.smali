.class public Lcom/microsoft/xbox/xle/app/trending/TrendingPivotScreen;
.super Lcom/microsoft/xbox/xle/app/activity/PivotActivity;
.source "TrendingPivotScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;-><init>()V

    .line 15
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 3

    .prologue
    .line 23
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->onCreate()V

    .line 25
    const v1, 0x7f030238

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/trending/TrendingPivotScreen;->setContentView(I)V

    .line 29
    const v1, 0x7f0e0aeb

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/trending/TrendingPivotScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    .local v0, "pivotWithTabs":Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingPivotScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    .line 31
    if-eqz v0, :cond_0

    .line 32
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0c0149

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setHeaderBackgroundColor(I)V

    .line 33
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setSelectedTabIndicatorColorToProfileColor()V

    .line 36
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->showBeamTrending()Z

    move-result v1

    if-nez v1, :cond_1

    .line 38
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingPivotScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->removePane(I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 41
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingPivotScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onCreate()V

    .line 42
    return-void
.end method
