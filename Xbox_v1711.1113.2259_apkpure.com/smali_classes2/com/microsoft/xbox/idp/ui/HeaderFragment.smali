.class public Lcom/microsoft/xbox/idp/ui/HeaderFragment;
.super Lcom/microsoft/xbox/idp/compat/BaseFragment;
.source "HeaderFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/idp/ui/HeaderFragment$Callbacks;
    }
.end annotation


# static fields
.field private static final NO_OP_CALLBACKS:Lcom/microsoft/xbox/idp/ui/HeaderFragment$Callbacks;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private callbacks:Lcom/microsoft/xbox/idp/ui/HeaderFragment$Callbacks;

.field private final imageCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Lcom/microsoft/xbox/idp/toolkit/BitmapLoader$Result;",
            ">;"
        }
    .end annotation
.end field

.field private userAccount:Lcom/microsoft/xbox/idp/model/UserAccount;

.field userAccountCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Lcom/microsoft/xbox/idp/toolkit/ObjectLoader$Result",
            "<",
            "Lcom/microsoft/xbox/idp/model/UserAccount;",
            ">;>;"
        }
    .end annotation
.end field

.field private userEmail:Landroid/widget/TextView;

.field private userImageView:Landroid/widget/ImageView;

.field private userName:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/microsoft/xbox/idp/ui/HeaderFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/idp/ui/HeaderFragment;->TAG:Ljava/lang/String;

    .line 166
    new-instance v0, Lcom/microsoft/xbox/idp/ui/HeaderFragment$3;

    invoke-direct {v0}, Lcom/microsoft/xbox/idp/ui/HeaderFragment$3;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/idp/ui/HeaderFragment;->NO_OP_CALLBACKS:Lcom/microsoft/xbox/idp/ui/HeaderFragment$Callbacks;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/microsoft/xbox/idp/compat/BaseFragment;-><init>()V

    .line 31
    sget-object v0, Lcom/microsoft/xbox/idp/ui/HeaderFragment;->NO_OP_CALLBACKS:Lcom/microsoft/xbox/idp/ui/HeaderFragment$Callbacks;

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/HeaderFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/HeaderFragment$Callbacks;

    .line 93
    new-instance v0, Lcom/microsoft/xbox/idp/ui/HeaderFragment$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/idp/ui/HeaderFragment$1;-><init>(Lcom/microsoft/xbox/idp/ui/HeaderFragment;)V

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/HeaderFragment;->userAccountCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    .line 130
    new-instance v0, Lcom/microsoft/xbox/idp/ui/HeaderFragment$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/idp/ui/HeaderFragment$2;-><init>(Lcom/microsoft/xbox/idp/ui/HeaderFragment;)V

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/HeaderFragment;->imageCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    .line 39
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/microsoft/xbox/idp/ui/HeaderFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/idp/ui/HeaderFragment;)Lcom/microsoft/xbox/idp/model/UserAccount;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/HeaderFragment;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/HeaderFragment;->userAccount:Lcom/microsoft/xbox/idp/model/UserAccount;

    return-object v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/idp/ui/HeaderFragment;Lcom/microsoft/xbox/idp/model/UserAccount;)Lcom/microsoft/xbox/idp/model/UserAccount;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/HeaderFragment;
    .param p1, "x1"    # Lcom/microsoft/xbox/idp/model/UserAccount;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/microsoft/xbox/idp/ui/HeaderFragment;->userAccount:Lcom/microsoft/xbox/idp/model/UserAccount;

    return-object p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/idp/ui/HeaderFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/HeaderFragment;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/HeaderFragment;->userEmail:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/idp/ui/HeaderFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/HeaderFragment;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/HeaderFragment;->userName:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/idp/ui/HeaderFragment;)Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/HeaderFragment;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/HeaderFragment;->imageCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/idp/ui/HeaderFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/HeaderFragment;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/HeaderFragment;->userImageView:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onAttach(Landroid/content/Context;)V

    .line 44
    check-cast p1, Lcom/microsoft/xbox/idp/ui/HeaderFragment$Callbacks;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/microsoft/xbox/idp/ui/HeaderFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/HeaderFragment$Callbacks;

    .line 45
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 87
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 88
    .local v0, "id":I
    sget v1, Lcom/microsoft/xbox/idp/R$id;->xbid_close:I

    if-ne v0, v1, :cond_0

    .line 89
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/HeaderFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/HeaderFragment$Callbacks;

    invoke-interface {v1}, Lcom/microsoft/xbox/idp/ui/HeaderFragment$Callbacks;->onClickCloseHeader()V

    .line 91
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 55
    invoke-super {p0, p1}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 56
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 62
    sget v0, Lcom/microsoft/xbox/idp/R$layout;->xbid_fragment_header:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 49
    invoke-super {p0}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onDetach()V

    .line 50
    sget-object v0, Lcom/microsoft/xbox/idp/ui/HeaderFragment;->NO_OP_CALLBACKS:Lcom/microsoft/xbox/idp/ui/HeaderFragment$Callbacks;

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/HeaderFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/HeaderFragment$Callbacks;

    .line 51
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 76
    invoke-super {p0}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onResume()V

    .line 77
    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/ui/HeaderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 78
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 79
    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/ui/HeaderFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    sget v2, Lcom/microsoft/xbox/idp/R$id;->loader_header_get_profile:I

    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/HeaderFragment;->userAccountCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 83
    :goto_0
    return-void

    .line 81
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/idp/ui/HeaderFragment;->TAG:Ljava/lang/String;

    const-string v2, "No arguments provided"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 67
    invoke-super {p0, p1, p2}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 68
    sget v0, Lcom/microsoft/xbox/idp/R$id;->xbid_close:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    sget v0, Lcom/microsoft/xbox/idp/R$id;->xbid_user_image:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/HeaderFragment;->userImageView:Landroid/widget/ImageView;

    .line 70
    sget v0, Lcom/microsoft/xbox/idp/R$id;->xbid_user_name:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/HeaderFragment;->userName:Landroid/widget/TextView;

    .line 71
    sget v0, Lcom/microsoft/xbox/idp/R$id;->xbid_user_email:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/HeaderFragment;->userEmail:Landroid/widget/TextView;

    .line 72
    return-void
.end method
