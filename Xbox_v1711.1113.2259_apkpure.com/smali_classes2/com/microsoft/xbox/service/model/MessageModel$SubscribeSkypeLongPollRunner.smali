.class Lcom/microsoft/xbox/service/model/MessageModel$SubscribeSkypeLongPollRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "MessageModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/MessageModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SubscribeSkypeLongPollRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final caller:Lcom/microsoft/xbox/service/model/MessageModel;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private final edfRegId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private final serviceManager:Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/MessageModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/microsoft/xbox/service/model/MessageModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/MessageModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "edfRegId"    # Ljava/lang/String;

    .prologue
    .line 1534
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/MessageModel$SubscribeSkypeLongPollRunner;->this$0:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 1527
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSkypeGroupMessageService()Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel$SubscribeSkypeLongPollRunner;->serviceManager:Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;

    .line 1535
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1536
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1538
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/MessageModel$SubscribeSkypeLongPollRunner;->caller:Lcom/microsoft/xbox/service/model/MessageModel;

    .line 1539
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/MessageModel$SubscribeSkypeLongPollRunner;->edfRegId:Ljava/lang/String;

    .line 1540
    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1526
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MessageModel$SubscribeSkypeLongPollRunner;->buildData()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/lang/String;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1549
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel$SubscribeSkypeLongPollRunner;->serviceManager:Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel$SubscribeSkypeLongPollRunner;->edfRegId:Ljava/lang/String;

    new-instance v2, Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointLongPollBody;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointLongPollBody;-><init>()V

    .line 1550
    invoke-static {v2}, Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointLongPollBody;->getSkypeEndpointLongPollBody(Lcom/microsoft/xbox/service/model/serialization/SkypeEndpointLongPollBody;)Ljava/lang/String;

    move-result-object v2

    .line 1549
    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;->subscribeWithSkypeChatServiceForLongPoll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 1560
    const-wide/16 v0, 0xbe8

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1555
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel$SubscribeSkypeLongPollRunner;->caller:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/MessageModel;->access$1400(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 1556
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 1544
    return-void
.end method
