.class Lcom/microsoft/xbox/service/model/ProgrammingModel$DiscoverModelHolder;
.super Ljava/lang/Object;
.source "ProgrammingModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProgrammingModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DiscoverModelHolder"
.end annotation


# static fields
.field private static instance:Lcom/microsoft/xbox/service/model/ProgrammingModel;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Lcom/microsoft/xbox/service/model/ProgrammingModel;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/model/ProgrammingModel;-><init>(Lcom/microsoft/xbox/service/model/ProgrammingModel$1;)V

    sput-object v0, Lcom/microsoft/xbox/service/model/ProgrammingModel$DiscoverModelHolder;->instance:Lcom/microsoft/xbox/service/model/ProgrammingModel;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$200()Lcom/microsoft/xbox/service/model/ProgrammingModel;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/microsoft/xbox/service/model/ProgrammingModel$DiscoverModelHolder;->instance:Lcom/microsoft/xbox/service/model/ProgrammingModel;

    return-object v0
.end method

.method static synthetic access$300()V
    .locals 0

    .prologue
    .line 49
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProgrammingModel$DiscoverModelHolder;->reset()V

    return-void
.end method

.method private static reset()V
    .locals 2

    .prologue
    .line 53
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 54
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->getInstance()Lcom/microsoft/xbox/service/model/ProgrammingModel;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/ProgrammingModel;->access$100(Lcom/microsoft/xbox/service/model/ProgrammingModel;)V

    .line 55
    new-instance v0, Lcom/microsoft/xbox/service/model/ProgrammingModel;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/model/ProgrammingModel;-><init>(Lcom/microsoft/xbox/service/model/ProgrammingModel$1;)V

    sput-object v0, Lcom/microsoft/xbox/service/model/ProgrammingModel$DiscoverModelHolder;->instance:Lcom/microsoft/xbox/service/model/ProgrammingModel;

    .line 56
    return-void

    .line 53
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
