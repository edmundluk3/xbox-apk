.class public Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;
.super Ljava/lang/Object;
.source "ActivitiesServiceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CompanionList"
.end annotation


# instance fields
.field public companions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;",
            ">;"
        }
    .end annotation
.end field

.field public defaultCompanionIndex:I

.field final synthetic this$0:Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;

    .prologue
    .line 417
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;->this$0:Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 418
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;->defaultCompanionIndex:I

    .line 419
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;->companions:Ljava/util/ArrayList;

    .line 420
    return-void
.end method


# virtual methods
.method public getDefault()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .locals 2

    .prologue
    .line 423
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;->companions:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;->companions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;->defaultCompanionIndex:I

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;->companions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;->defaultCompanionIndex:I

    if-le v0, v1, :cond_0

    .line 424
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;->companions:Ljava/util/ArrayList;

    iget v1, p0, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;->defaultCompanionIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .line 426
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
