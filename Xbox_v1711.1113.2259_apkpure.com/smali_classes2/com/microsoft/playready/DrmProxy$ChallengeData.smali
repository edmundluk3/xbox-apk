.class Lcom/microsoft/playready/DrmProxy$ChallengeData;
.super Ljava/lang/Object;
.source "DrmProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/DrmProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ChallengeData"
.end annotation


# instance fields
.field private final mChallengeBytes:[B

.field private final mEmbeddedServerUri:Ljava/lang/String;


# direct methods
.method constructor <init>([BLjava/lang/String;)V
    .locals 0
    .param p1, "challengeBytes"    # [B
    .param p2, "embeddedServerUri"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/microsoft/playready/DrmProxy$ChallengeData;->mChallengeBytes:[B

    .line 35
    iput-object p2, p0, Lcom/microsoft/playready/DrmProxy$ChallengeData;->mEmbeddedServerUri:Ljava/lang/String;

    .line 36
    return-void
.end method


# virtual methods
.method public getChallengeBytes()[B
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/playready/DrmProxy$ChallengeData;->mChallengeBytes:[B

    return-object v0
.end method

.method public getEmbeddedServerUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/playready/DrmProxy$ChallengeData;->mEmbeddedServerUri:Ljava/lang/String;

    return-object v0
.end method
