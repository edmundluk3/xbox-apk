.class public final enum Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;
.super Ljava/lang/Enum;
.source "ServiceCommon.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/ServiceCommon;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HttpOperation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;

.field public static final enum DELETE:Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;

.field public static final enum GET:Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;

.field public static final enum POST:Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 67
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;

    const-string v1, "GET"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;->GET:Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;

    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;

    const-string v1, "POST"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;->POST:Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;

    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;

    const-string v1, "DELETE"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;->DELETE:Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;

    .line 66
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;->GET:Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;->POST:Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;->DELETE:Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 66
    const-class v0, Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/network/managers/ServiceCommon$HttpOperation;

    return-object v0
.end method
