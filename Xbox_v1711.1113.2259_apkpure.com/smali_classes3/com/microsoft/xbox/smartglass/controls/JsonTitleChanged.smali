.class Lcom/microsoft/xbox/smartglass/controls/JsonTitleChanged;
.super Lorg/json/JSONObject;
.source "JsonTitleChanged.java"


# direct methods
.method public constructor <init>(ILjava/util/Collection;)V
    .locals 4
    .param p1, "newTitleId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Lorg/json/JSONObject;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 14
    .local p2, "activeTitles":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/json/JSONObject;>;"
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 15
    const-string v0, "newTitleId"

    invoke-static {p1}, Lcom/microsoft/xbox/smartglass/controls/Json;->unsignedInt(I)J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/JsonTitleChanged;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 16
    const-string v0, "activeTitles"

    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p2}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/JsonTitleChanged;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 17
    return-void
.end method
