.class public Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService;
.super Ljava/lang/Object;
.source "ActivityFeedTelemetryService.java"


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method

.method private getDifferences(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 4
    .param p1    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/ArrayList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    .local p1, "pref1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p2, "pref2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 94
    .local v1, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 95
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 98
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 99
    .local v0, "pref":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 100
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 104
    .end local v0    # "pref":Ljava/lang/String;
    :cond_1
    return-object v1
.end method

.method private getFilterValues(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)Ljava/util/ArrayList;
    .locals 2
    .param p1, "prefs"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 76
    .local v0, "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 77
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showClubs()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "ClubsOn"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showFavorites()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "FavoritesOn"

    :goto_1
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showFriends()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "FriendsOn"

    :goto_2
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showGames()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "GamesOn"

    :goto_3
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->showPopular()Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "PopularNowOn"

    :goto_4
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    return-object v0

    .line 77
    :cond_0
    const-string v1, "ClubsOff"

    goto :goto_0

    .line 78
    :cond_1
    const-string v1, "FavoritesOff"

    goto :goto_1

    .line 79
    :cond_2
    const-string v1, "FriendsOff"

    goto :goto_2

    .line 80
    :cond_3
    const-string v1, "GamesOff"

    goto :goto_3

    .line 81
    :cond_4
    const-string v1, "PopularNowOff"

    goto :goto_4
.end method

.method static synthetic lambda$trackActivityFeedFilterPageView$1()V
    .locals 1

    .prologue
    .line 36
    const-string v0, "Activity Feed - Filter Preferences View"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$trackApplyFeedFilter$2(Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)V
    .locals 7
    .param p0, "this"    # Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "prefs"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "originalPrefs"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    .prologue
    .line 46
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 47
    invoke-static {}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->defaultPrefs()Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    move-result-object v5

    invoke-static {p2, v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    .line 49
    .local v3, "orig":Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService;->getFilterValues(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)Ljava/util/ArrayList;

    move-result-object v4

    .line 50
    .local v4, "originalValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService;->getFilterValues(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)Ljava/util/ArrayList;

    move-result-object v1

    .line 51
    .local v1, "finalValues":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0, v4, v1}, Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService;->getDifferences(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 53
    .local v0, "differences":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v2, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 54
    .local v2, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v5, "ActivityFeedFilterFull"

    invoke-virtual {v4}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 55
    const-string v5, "ActivityFeedFilterChanges"

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 57
    const-string v5, "Activity Feed - Apply Filter Preferences"

    invoke-static {v5, v2}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 58
    return-void
.end method

.method static synthetic lambda$trackCancelFeedFilter$3()V
    .locals 1

    .prologue
    .line 65
    const-string v0, "Activity Feed - Close Filter Preferences"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$trackShowFilterScreen$0()V
    .locals 1

    .prologue
    .line 29
    const-string v0, "Activity Feed - Filter Activity Feed"

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public trackActivityFeedFilterPageView()V
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService$$Lambda$2;->lambdaFactory$()Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 37
    return-void
.end method

.method public trackApplyFeedFilter(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)V
    .locals 1
    .param p1, "originalPrefs"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "prefs"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 45
    invoke-static {p0, p2, p1}, Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 59
    return-void
.end method

.method public trackCancelFeedFilter()V
    .locals 1

    .prologue
    .line 65
    invoke-static {}, Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService$$Lambda$4;->lambdaFactory$()Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 66
    return-void
.end method

.method public trackShowFilterScreen()V
    .locals 1

    .prologue
    .line 29
    invoke-static {}, Lcom/microsoft/xbox/data/service/activityfeed/ActivityFeedTelemetryService$$Lambda$1;->lambdaFactory$()Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 30
    return-void
.end method
