.class public final Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes;
.super Ljava/lang/Object;
.source "PartyWebRtcDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcDataChannelConnected;,
        Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcDisconnected;,
        Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcConnected;,
        Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcSdpUpdate;,
        Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcTextMessage;,
        Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcMemberStatusChange;,
        Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$PartyWebRtcEvent;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No instances"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
