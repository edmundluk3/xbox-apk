.class abstract Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;
.super Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
.source "$AutoValue_ClubHubDataTypes_Club.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;
    }
.end annotation


# instance fields
.field private final bannerImageUrl:Ljava/lang/String;

.field private final clubPresence:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;",
            ">;"
        }
    .end annotation
.end field

.field private final clubPresenceCount:J

.field private final clubPresenceInGameCount:J

.field private final clubPresenceTodayCount:J

.field private final clubType:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

.field private final creationDateUtc:Ljava/lang/String;

.field private final followersCount:J

.field private final founderXuid:J

.field private final glyphImageUrl:Ljava/lang/String;

.field private final id:J

.field private final maxMembersInGame:J

.field private final maxMembersPerClub:J

.field private final membersCount:J

.field private final moderatorsCount:J

.field private final ownerXuid:Ljava/lang/String;

.field private final profile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

.field private final recommendation:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

.field private final recommendedCount:J

.field private final reportCount:J

.field private final reportedItemsCount:J

.field private final requestedToJoinCount:J

.field private final roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

.field private final settings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

.field private final shortName:Ljava/lang/String;

.field private final state:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

.field private final suspendedUntilUtc:Ljava/util/Date;

.field private final targetRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

.field private final titleDeeplinks:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;


# direct methods
.method constructor <init>(JLcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;JJJJJJJJLcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;Lcom/google/common/collect/ImmutableList;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;Ljava/util/Date;JJJJ)V
    .locals 5
    .param p1, "id"    # J
    .param p3, "profile"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "clubType"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "shortName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "ownerXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p7, "founderXuid"    # J
    .param p9, "creationDateUtc"    # Ljava/lang/String;
    .param p10, "glyphImageUrl"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p11, "bannerImageUrl"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p12, "settings"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p13, "followersCount"    # J
    .param p15, "membersCount"    # J
    .param p17, "moderatorsCount"    # J
    .param p19, "recommendedCount"    # J
    .param p21, "requestedToJoinCount"    # J
    .param p23, "clubPresenceCount"    # J
    .param p25, "clubPresenceTodayCount"    # J
    .param p27, "clubPresenceInGameCount"    # J
    .param p29, "roster"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p30, "targetRoles"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p31, "recommendation"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p32    # Lcom/google/common/collect/ImmutableList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p33, "titleDeeplinks"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p34, "state"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;
    .param p35, "suspendedUntilUtc"    # Ljava/util/Date;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p36, "reportCount"    # J
    .param p38, "reportedItemsCount"    # J
    .param p40, "maxMembersPerClub"    # J
    .param p42, "maxMembersInGame"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;",
            "JJJJJJJJ",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;",
            ">;",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;",
            "Ljava/util/Date;",
            "JJJJ)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p32, "clubPresence":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;-><init>()V

    .line 74
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->id:J

    .line 75
    iput-object p3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->profile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    .line 76
    iput-object p4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubType:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    .line 77
    iput-object p5, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->shortName:Ljava/lang/String;

    .line 78
    iput-object p6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->ownerXuid:Ljava/lang/String;

    .line 79
    iput-wide p7, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->founderXuid:J

    .line 80
    if-nez p9, :cond_0

    .line 81
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Null creationDateUtc"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 83
    :cond_0
    iput-object p9, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->creationDateUtc:Ljava/lang/String;

    .line 84
    iput-object p10, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->glyphImageUrl:Ljava/lang/String;

    .line 85
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->bannerImageUrl:Ljava/lang/String;

    .line 86
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->settings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    .line 87
    move-wide/from16 v0, p13

    iput-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->followersCount:J

    .line 88
    move-wide/from16 v0, p15

    iput-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->membersCount:J

    .line 89
    move-wide/from16 v0, p17

    iput-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->moderatorsCount:J

    .line 90
    move-wide/from16 v0, p19

    iput-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->recommendedCount:J

    .line 91
    move-wide/from16 v0, p21

    iput-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->requestedToJoinCount:J

    .line 92
    move-wide/from16 v0, p23

    iput-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresenceCount:J

    .line 93
    move-wide/from16 v0, p25

    iput-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresenceTodayCount:J

    .line 94
    move-wide/from16 v0, p27

    iput-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresenceInGameCount:J

    .line 95
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    .line 96
    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->targetRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    .line 97
    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->recommendation:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    .line 98
    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresence:Lcom/google/common/collect/ImmutableList;

    .line 99
    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->titleDeeplinks:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    .line 100
    if-nez p34, :cond_1

    .line 101
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Null state"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 103
    :cond_1
    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->state:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    .line 104
    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->suspendedUntilUtc:Ljava/util/Date;

    .line 105
    move-wide/from16 v0, p36

    iput-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->reportCount:J

    .line 106
    move-wide/from16 v0, p38

    iput-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->reportedItemsCount:J

    .line 107
    move-wide/from16 v0, p40

    iput-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->maxMembersPerClub:J

    .line 108
    move-wide/from16 v0, p42

    iput-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->maxMembersInGame:J

    .line 109
    return-void
.end method


# virtual methods
.method public bannerImageUrl()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 160
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->bannerImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public clubPresence()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;",
            ">;"
        }
    .end annotation

    .prologue
    .line 230
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresence:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public clubPresenceCount()J
    .locals 2

    .prologue
    .line 196
    iget-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresenceCount:J

    return-wide v0
.end method

.method public clubPresenceInGameCount()J
    .locals 2

    .prologue
    .line 206
    iget-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresenceInGameCount:J

    return-wide v0
.end method

.method public clubPresenceTodayCount()J
    .locals 2

    .prologue
    .line 201
    iget-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresenceTodayCount:J

    return-wide v0
.end method

.method public clubType()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubType:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    return-object v0
.end method

.method public creationDateUtc()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 148
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->creationDateUtc:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 308
    if-ne p1, p0, :cond_1

    .line 343
    :cond_0
    :goto_0
    return v1

    .line 311
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    if-eqz v3, :cond_10

    move-object v0, p1

    .line 312
    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 313
    .local v0, "that":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->id:J

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->profile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    if-nez v3, :cond_3

    .line 314
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubType:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    if-nez v3, :cond_4

    .line 315
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubType()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->shortName:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 316
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->shortName()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->ownerXuid:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 317
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->ownerXuid()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_4
    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->founderXuid:J

    .line 318
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->founderXuid()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->creationDateUtc:Ljava/lang/String;

    .line 319
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->creationDateUtc()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->glyphImageUrl:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 320
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->glyphImageUrl()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->bannerImageUrl:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 321
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->bannerImageUrl()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->settings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    if-nez v3, :cond_9

    .line 322
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_7
    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->followersCount:J

    .line 323
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->followersCount()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->membersCount:J

    .line 324
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->membersCount()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->moderatorsCount:J

    .line 325
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->moderatorsCount()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->recommendedCount:J

    .line 326
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->recommendedCount()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->requestedToJoinCount:J

    .line 327
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->requestedToJoinCount()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresenceCount:J

    .line 328
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceCount()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresenceTodayCount:J

    .line 329
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceTodayCount()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresenceInGameCount:J

    .line 330
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceInGameCount()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    if-nez v3, :cond_a

    .line 331
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_8
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->targetRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    if-nez v3, :cond_b

    .line 332
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->targetRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_9
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->recommendation:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    if-nez v3, :cond_c

    .line 333
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->recommendation()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_a
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresence:Lcom/google/common/collect/ImmutableList;

    if-nez v3, :cond_d

    .line 334
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresence()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_b
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->titleDeeplinks:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    if-nez v3, :cond_e

    .line 335
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->titleDeeplinks()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_c
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->state:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    .line 336
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->state()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->suspendedUntilUtc:Ljava/util/Date;

    if-nez v3, :cond_f

    .line 337
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->suspendedUntilUtc()Ljava/util/Date;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_d
    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->reportCount:J

    .line 338
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->reportCount()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->reportedItemsCount:J

    .line 339
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->reportedItemsCount()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->maxMembersPerClub:J

    .line 340
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->maxMembersPerClub()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->maxMembersInGame:J

    .line 341
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->maxMembersInGame()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto/16 :goto_0

    .line 314
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->profile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_1

    .line 315
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubType:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubType()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_2

    .line 316
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->shortName:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->shortName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_3

    .line 317
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->ownerXuid:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->ownerXuid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_4

    .line 320
    :cond_7
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->glyphImageUrl:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->glyphImageUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_5

    .line 321
    :cond_8
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->bannerImageUrl:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->bannerImageUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_6

    .line 322
    :cond_9
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->settings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_7

    .line 331
    :cond_a
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_8

    .line 332
    :cond_b
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->targetRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->targetRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_9

    .line 333
    :cond_c
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->recommendation:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->recommendation()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_a

    .line 334
    :cond_d
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresence:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresence()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_b

    .line 335
    :cond_e
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->titleDeeplinks:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->titleDeeplinks()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_c

    .line 337
    :cond_f
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->suspendedUntilUtc:Ljava/util/Date;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->suspendedUntilUtc()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_d

    .end local v0    # "that":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :cond_10
    move v1, v2

    .line 343
    goto/16 :goto_0
.end method

.method public followersCount()J
    .locals 2

    .prologue
    .line 171
    iget-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->followersCount:J

    return-wide v0
.end method

.method public founderXuid()J
    .locals 2

    .prologue
    .line 142
    iget-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->founderXuid:J

    return-wide v0
.end method

.method public glyphImageUrl()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->glyphImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/16 v11, 0x20

    const v10, 0xf4243

    .line 348
    const/4 v0, 0x1

    .line 349
    .local v0, "h":I
    mul-int/2addr v0, v10

    .line 350
    int-to-long v4, v0

    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->id:J

    ushr-long/2addr v6, v11

    iget-wide v8, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->id:J

    xor-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 351
    mul-int/2addr v0, v10

    .line 352
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->profile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 353
    mul-int/2addr v0, v10

    .line 354
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubType:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 355
    mul-int/2addr v0, v10

    .line 356
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->shortName:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 357
    mul-int/2addr v0, v10

    .line 358
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->ownerXuid:Ljava/lang/String;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    xor-int/2addr v0, v1

    .line 359
    mul-int/2addr v0, v10

    .line 360
    int-to-long v4, v0

    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->founderXuid:J

    ushr-long/2addr v6, v11

    iget-wide v8, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->founderXuid:J

    xor-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 361
    mul-int/2addr v0, v10

    .line 362
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->creationDateUtc:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 363
    mul-int/2addr v0, v10

    .line 364
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->glyphImageUrl:Ljava/lang/String;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    xor-int/2addr v0, v1

    .line 365
    mul-int/2addr v0, v10

    .line 366
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->bannerImageUrl:Ljava/lang/String;

    if-nez v1, :cond_5

    move v1, v2

    :goto_5
    xor-int/2addr v0, v1

    .line 367
    mul-int/2addr v0, v10

    .line 368
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->settings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    if-nez v1, :cond_6

    move v1, v2

    :goto_6
    xor-int/2addr v0, v1

    .line 369
    mul-int/2addr v0, v10

    .line 370
    int-to-long v4, v0

    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->followersCount:J

    ushr-long/2addr v6, v11

    iget-wide v8, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->followersCount:J

    xor-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 371
    mul-int/2addr v0, v10

    .line 372
    int-to-long v4, v0

    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->membersCount:J

    ushr-long/2addr v6, v11

    iget-wide v8, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->membersCount:J

    xor-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 373
    mul-int/2addr v0, v10

    .line 374
    int-to-long v4, v0

    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->moderatorsCount:J

    ushr-long/2addr v6, v11

    iget-wide v8, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->moderatorsCount:J

    xor-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 375
    mul-int/2addr v0, v10

    .line 376
    int-to-long v4, v0

    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->recommendedCount:J

    ushr-long/2addr v6, v11

    iget-wide v8, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->recommendedCount:J

    xor-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 377
    mul-int/2addr v0, v10

    .line 378
    int-to-long v4, v0

    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->requestedToJoinCount:J

    ushr-long/2addr v6, v11

    iget-wide v8, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->requestedToJoinCount:J

    xor-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 379
    mul-int/2addr v0, v10

    .line 380
    int-to-long v4, v0

    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresenceCount:J

    ushr-long/2addr v6, v11

    iget-wide v8, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresenceCount:J

    xor-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 381
    mul-int/2addr v0, v10

    .line 382
    int-to-long v4, v0

    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresenceTodayCount:J

    ushr-long/2addr v6, v11

    iget-wide v8, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresenceTodayCount:J

    xor-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 383
    mul-int/2addr v0, v10

    .line 384
    int-to-long v4, v0

    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresenceInGameCount:J

    ushr-long/2addr v6, v11

    iget-wide v8, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresenceInGameCount:J

    xor-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 385
    mul-int/2addr v0, v10

    .line 386
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    if-nez v1, :cond_7

    move v1, v2

    :goto_7
    xor-int/2addr v0, v1

    .line 387
    mul-int/2addr v0, v10

    .line 388
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->targetRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    if-nez v1, :cond_8

    move v1, v2

    :goto_8
    xor-int/2addr v0, v1

    .line 389
    mul-int/2addr v0, v10

    .line 390
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->recommendation:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    if-nez v1, :cond_9

    move v1, v2

    :goto_9
    xor-int/2addr v0, v1

    .line 391
    mul-int/2addr v0, v10

    .line 392
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresence:Lcom/google/common/collect/ImmutableList;

    if-nez v1, :cond_a

    move v1, v2

    :goto_a
    xor-int/2addr v0, v1

    .line 393
    mul-int/2addr v0, v10

    .line 394
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->titleDeeplinks:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    if-nez v1, :cond_b

    move v1, v2

    :goto_b
    xor-int/2addr v0, v1

    .line 395
    mul-int/2addr v0, v10

    .line 396
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->state:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 397
    mul-int/2addr v0, v10

    .line 398
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->suspendedUntilUtc:Ljava/util/Date;

    if-nez v1, :cond_c

    :goto_c
    xor-int/2addr v0, v2

    .line 399
    mul-int/2addr v0, v10

    .line 400
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->reportCount:J

    ushr-long/2addr v4, v11

    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->reportCount:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 401
    mul-int/2addr v0, v10

    .line 402
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->reportedItemsCount:J

    ushr-long/2addr v4, v11

    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->reportedItemsCount:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 403
    mul-int/2addr v0, v10

    .line 404
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->maxMembersPerClub:J

    ushr-long/2addr v4, v11

    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->maxMembersPerClub:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 405
    mul-int/2addr v0, v10

    .line 406
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->maxMembersInGame:J

    ushr-long/2addr v4, v11

    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->maxMembersInGame:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 407
    return v0

    .line 352
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->profile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto/16 :goto_0

    .line 354
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubType:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto/16 :goto_1

    .line 356
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->shortName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto/16 :goto_2

    .line 358
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->ownerXuid:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto/16 :goto_3

    .line 364
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->glyphImageUrl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto/16 :goto_4

    .line 366
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->bannerImageUrl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto/16 :goto_5

    .line 368
    :cond_6
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->settings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto/16 :goto_6

    .line 386
    :cond_7
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto/16 :goto_7

    .line 388
    :cond_8
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->targetRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto/16 :goto_8

    .line 390
    :cond_9
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->recommendation:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto/16 :goto_9

    .line 392
    :cond_a
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresence:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    goto/16 :goto_a

    .line 394
    :cond_b
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->titleDeeplinks:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto/16 :goto_b

    .line 398
    :cond_c
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->suspendedUntilUtc:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->hashCode()I

    move-result v2

    goto/16 :goto_c
.end method

.method public id()J
    .locals 2

    .prologue
    .line 113
    iget-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->id:J

    return-wide v0
.end method

.method public maxMembersInGame()J
    .locals 2

    .prologue
    .line 268
    iget-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->maxMembersInGame:J

    return-wide v0
.end method

.method public maxMembersPerClub()J
    .locals 2

    .prologue
    .line 263
    iget-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->maxMembersPerClub:J

    return-wide v0
.end method

.method public membersCount()J
    .locals 2

    .prologue
    .line 176
    iget-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->membersCount:J

    return-wide v0
.end method

.method public moderatorsCount()J
    .locals 2

    .prologue
    .line 181
    iget-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->moderatorsCount:J

    return-wide v0
.end method

.method public ownerXuid()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->ownerXuid:Ljava/lang/String;

    return-object v0
.end method

.method public profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->profile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    return-object v0
.end method

.method public recommendation()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 224
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->recommendation:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    return-object v0
.end method

.method public recommendedCount()J
    .locals 2

    .prologue
    .line 186
    iget-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->recommendedCount:J

    return-wide v0
.end method

.method public reportCount()J
    .locals 2

    .prologue
    .line 253
    iget-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->reportCount:J

    return-wide v0
.end method

.method public reportedItemsCount()J
    .locals 2

    .prologue
    .line 258
    iget-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->reportedItemsCount:J

    return-wide v0
.end method

.method public requestedToJoinCount()J
    .locals 2

    .prologue
    .line 191
    iget-wide v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->requestedToJoinCount:J

    return-wide v0
.end method

.method public roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 212
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    return-object v0
.end method

.method public settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->settings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    return-object v0
.end method

.method public shortName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->shortName:Ljava/lang/String;

    return-object v0
.end method

.method public state()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 242
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->state:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    return-object v0
.end method

.method public suspendedUntilUtc()Ljava/util/Date;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 248
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->suspendedUntilUtc:Ljava/util/Date;

    return-object v0
.end method

.method public targetRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 218
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->targetRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    return-object v0
.end method

.method public titleDeeplinks()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 236
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->titleDeeplinks:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    return-object v0
.end method

.method public toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    .locals 1

    .prologue
    .line 412
    new-instance v0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club$Builder;-><init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 273
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Club{id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->id:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", profile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->profile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clubType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubType:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", shortName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->shortName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ownerXuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->ownerXuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", founderXuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->founderXuid:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", creationDateUtc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->creationDateUtc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", glyphImageUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->glyphImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", bannerImageUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->bannerImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", settings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->settings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", followersCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->followersCount:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", membersCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->membersCount:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", moderatorsCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->moderatorsCount:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", recommendedCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->recommendedCount:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", requestedToJoinCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->requestedToJoinCount:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clubPresenceCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresenceCount:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clubPresenceTodayCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresenceTodayCount:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clubPresenceInGameCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresenceInGameCount:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", roster="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", targetRoles="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->targetRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", recommendation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->recommendation:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clubPresence="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->clubPresence:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", titleDeeplinks="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->titleDeeplinks:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->state:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", suspendedUntilUtc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->suspendedUntilUtc:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", reportCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->reportCount:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", reportedItemsCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->reportedItemsCount:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxMembersPerClub="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->maxMembersPerClub:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxMembersInGame="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Club;->maxMembersInGame:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
