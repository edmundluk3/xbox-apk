.class Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter$1;
.super Ljava/lang/Object;
.source "GameProgressChallengesScreenAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 41
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    .line 42
    .local v0, "item":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/GameProgressChallengesScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->navigateToChallengeDetails(Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;)V

    .line 43
    return-void
.end method
