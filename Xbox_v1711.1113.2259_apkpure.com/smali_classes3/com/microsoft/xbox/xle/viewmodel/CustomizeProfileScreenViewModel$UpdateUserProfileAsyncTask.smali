.class Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "CustomizeProfileScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateUserProfileAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field partialFailure:Z

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

.field private final updateInfo:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field updatedColor:Z

.field updatedGamerpic:Z


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "updateInfo":Ljava/util/Map;, "Ljava/util/Map<Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 664
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 660
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->partialFailure:Z

    .line 661
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->updatedColor:Z

    .line 662
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->updatedGamerpic:Z

    .line 665
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->updateInfo:Ljava/util/Map;

    .line 666
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 670
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 671
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 701
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 702
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v3

    if-nez v3, :cond_1

    .line 703
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 725
    :cond_0
    :goto_0
    return-object v1

    .line 706
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 707
    .local v1, "result":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    const/4 v0, 0x0

    .line 708
    .local v0, "anySuccess":Z
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->updateInfo:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    .line 709
    .local v2, "setting":Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v5

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->updateInfo:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v2, v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->updateProfile(Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 710
    new-array v3, v7, [Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    const/4 v5, 0x0

    sget-object v6, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    aput-object v6, v3, v5

    invoke-static {v1, v3}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->merge(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;[Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    goto :goto_1

    .line 712
    :cond_3
    const/4 v0, 0x1

    .line 713
    sget-object v3, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->PreferredColor:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    if-ne v2, v3, :cond_4

    .line 714
    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->updatedColor:Z

    goto :goto_1

    .line 715
    :cond_4
    sget-object v3, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->PublicGamerpic:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    if-ne v2, v3, :cond_2

    .line 716
    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->updatedGamerpic:Z

    goto :goto_1

    .line 721
    .end local v2    # "setting":Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;
    :cond_5
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    .line 722
    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->partialFailure:Z

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 658
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 696
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 658
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 5

    .prologue
    .line 676
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 677
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->access$502(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Z)Z

    .line 678
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->partialFailure:Z

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->updatedColor:Z

    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->updatedGamerpic:Z

    invoke-static {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->access$600(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ZZZ)V

    .line 679
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 690
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->access$502(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Z)Z

    .line 691
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->partialFailure:Z

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->updatedColor:Z

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->updatedGamerpic:Z

    invoke-static {v0, p1, v1, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->access$600(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;ZZZ)V

    .line 692
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 658
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 683
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 684
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->access$502(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Z)Z

    .line 685
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel$UpdateUserProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->updateAdapter()V

    .line 686
    return-void
.end method
