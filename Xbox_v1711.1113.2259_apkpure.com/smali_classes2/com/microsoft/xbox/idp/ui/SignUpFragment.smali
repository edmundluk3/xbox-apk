.class public Lcom/microsoft/xbox/idp/ui/SignUpFragment;
.super Lcom/microsoft/xbox/idp/compat/BaseFragment;
.source "SignUpFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityContext;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/idp/ui/SignUpFragment$Callbacks;,
        Lcom/microsoft/xbox/idp/ui/SignUpFragment$Status;,
        Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;,
        Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;
    }
.end annotation


# static fields
.field public static final ARG_ACCOUNT_PROVISIONING_RESULT:Ljava/lang/String; = "ARG_ACCOUNT_PROVISIONING_RESULT"

.field private static final KEY_STATE:Ljava/lang/String; = "KEY_STATE"

.field private static final NO_OP_CALLBACKS:Lcom/microsoft/xbox/idp/ui/SignUpFragment$Callbacks;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private bottomBarShadow:Landroid/view/View;

.field private callbacks:Lcom/microsoft/xbox/idp/ui/SignUpFragment$Callbacks;

.field private claimItButton:Landroid/widget/Button;

.field private clearTextButton:Landroid/view/View;

.field private editTextGamerTag:Landroid/widget/EditText;

.field private editTextGamerTagContainer:Landroid/view/View;

.field private final gamerTagChangeListener:Landroid/text/TextWatcher;

.field private final gamerTagClaimCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Lcom/microsoft/xbox/idp/toolkit/ObjectLoader$Result",
            "<",
            "Lcom/microsoft/xbox/idp/model/GamerTag$Response;",
            ">;>;"
        }
    .end annotation
.end field

.field private final gamerTagReservationCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Lcom/microsoft/xbox/idp/toolkit/ObjectLoader$Result",
            "<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation
.end field

.field private gamerTagState:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

.field private final loaderMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/microsoft/xbox/idp/util/ErrorHelper$LoaderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final onSuggestionClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private privacyDetailsText:Landroid/widget/TextView;

.field private privacyText:Landroid/widget/TextView;

.field private provisioningResult:Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;

.field private scrollView:Landroid/widget/ScrollView;

.field private searchButton:Landroid/view/View;

.field private state:Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;

.field private final suggestionsCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Lcom/microsoft/xbox/idp/toolkit/ObjectLoader$Result",
            "<",
            "Lcom/microsoft/xbox/idp/model/Suggestions$Response;",
            ">;>;"
        }
    .end annotation
.end field

.field private suggestionsList:Landroid/widget/AbsListView;

.field private suggestionsListAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private textGamerTagComment:Landroid/widget/TextView;

.field private final xboxDotComLauncher:Landroid/text/style/ClickableSpan;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->TAG:Ljava/lang/String;

    .line 566
    new-instance v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment$9;

    invoke-direct {v0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment$9;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->NO_OP_CALLBACKS:Lcom/microsoft/xbox/idp/ui/SignUpFragment$Callbacks;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/microsoft/xbox/idp/compat/BaseFragment;-><init>()V

    .line 56
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->loaderMap:Landroid/util/SparseArray;

    .line 61
    sget-object v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->NO_OP_CALLBACKS:Lcom/microsoft/xbox/idp/ui/SignUpFragment$Callbacks;

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/SignUpFragment$Callbacks;

    .line 305
    new-instance v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment$3;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment$3;-><init>(Lcom/microsoft/xbox/idp/ui/SignUpFragment;)V

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->gamerTagChangeListener:Landroid/text/TextWatcher;

    .line 320
    new-instance v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment$4;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment$4;-><init>(Lcom/microsoft/xbox/idp/ui/SignUpFragment;)V

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->onSuggestionClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 328
    new-instance v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment$5;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment$5;-><init>(Lcom/microsoft/xbox/idp/ui/SignUpFragment;)V

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->gamerTagClaimCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    .line 371
    new-instance v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment$6;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment$6;-><init>(Lcom/microsoft/xbox/idp/ui/SignUpFragment;)V

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->gamerTagReservationCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    .line 410
    new-instance v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment$7;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment$7;-><init>(Lcom/microsoft/xbox/idp/ui/SignUpFragment;)V

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->suggestionsCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    .line 449
    new-instance v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment$8;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment$8;-><init>(Lcom/microsoft/xbox/idp/ui/SignUpFragment;)V

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->xboxDotComLauncher:Landroid/text/style/ClickableSpan;

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->loaderMap:Landroid/util/SparseArray;

    sget v1, Lcom/microsoft/xbox/idp/R$id;->loader_reserve_gamertag:I

    new-instance v2, Lcom/microsoft/xbox/idp/util/ObjectLoaderInfo;

    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->gamerTagReservationCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/idp/util/ObjectLoaderInfo;-><init>(Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->loaderMap:Landroid/util/SparseArray;

    sget v1, Lcom/microsoft/xbox/idp/R$id;->loader_claim_gamertag:I

    new-instance v2, Lcom/microsoft/xbox/idp/util/ObjectLoaderInfo;

    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->gamerTagClaimCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/idp/util/ObjectLoaderInfo;-><init>(Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->loaderMap:Landroid/util/SparseArray;

    sget v1, Lcom/microsoft/xbox/idp/R$id;->loader_suggestions:I

    new-instance v2, Lcom/microsoft/xbox/idp/util/ObjectLoaderInfo;

    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->suggestionsCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/idp/util/ObjectLoaderInfo;-><init>(Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 80
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/idp/ui/SignUpFragment;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/SignUpFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->scrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/idp/ui/SignUpFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/SignUpFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->bottomBarShadow:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/idp/ui/SignUpFragment;)Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/SignUpFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->suggestionsCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/idp/ui/SignUpFragment;Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/SignUpFragment;
    .param p1, "x1"    # Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->setGamerTagState(Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;)V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/idp/ui/SignUpFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/SignUpFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->editTextGamerTagContainer:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/idp/ui/SignUpFragment;Ljava/lang/CharSequence;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/SignUpFragment;
    .param p1, "x1"    # Ljava/lang/CharSequence;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->resetGamerTagState(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/idp/ui/SignUpFragment;)Landroid/widget/ArrayAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/SignUpFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->suggestionsListAdapter:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/idp/ui/SignUpFragment;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/SignUpFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->editTextGamerTag:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$600()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/idp/ui/SignUpFragment;)Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/SignUpFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->state:Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;

    return-object v0
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/idp/ui/SignUpFragment;)Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/SignUpFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->provisioningResult:Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;

    return-object v0
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/idp/ui/SignUpFragment;)Lcom/microsoft/xbox/idp/ui/SignUpFragment$Callbacks;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/idp/ui/SignUpFragment;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/SignUpFragment$Callbacks;

    return-object v0
.end method

.method private resetGamerTagState(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "gamertag"    # Ljava/lang/CharSequence;

    .prologue
    .line 284
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->state:Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;

    iget-object v0, v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;->gamerTag:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 285
    sget-object v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;->UNINITIALIZED:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->setGamerTagState(Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;)V

    .line 303
    :goto_0
    return-void

    .line 286
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 287
    sget-object v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;->EMPTY:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->setGamerTagState(Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;)V

    goto :goto_0

    .line 288
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->state:Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;

    iget-object v0, v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;->gamerTag:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 289
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->state:Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;

    iget-object v0, v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;->gamerTag:Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->provisioningResult:Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;

    invoke-virtual {v1}, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;->getGamerTag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 290
    sget-object v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;->INITIAL:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->setGamerTagState(Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;)V

    goto :goto_0

    .line 291
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->state:Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;

    iget-boolean v0, v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;->reserved:Z

    if-eqz v0, :cond_3

    .line 292
    sget-object v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;->AVAILABLE:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->setGamerTagState(Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;)V

    goto :goto_0

    .line 293
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->state:Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;

    iget-object v0, v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;->gamerTagWithSuggestions:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 294
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->state:Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;

    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;->hasSuggestions()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;->UNAVAILABLE_WITH_SUGGESTIONS:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    :goto_1
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->setGamerTagState(Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;)V

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;->UNAVAILABLE:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    goto :goto_1

    .line 296
    :cond_5
    sget-object v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;->UNKNOWN:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->setGamerTagState(Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;)V

    goto :goto_0

    .line 298
    :cond_6
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->state:Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;

    iget-object v0, v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;->gamerTagWithSuggestions:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 299
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->state:Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;

    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;->hasSuggestions()Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;->UNAVAILABLE_WITH_SUGGESTIONS:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    :goto_2
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->setGamerTagState(Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;)V

    goto :goto_0

    :cond_7
    sget-object v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;->UNAVAILABLE:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    goto :goto_2

    .line 301
    :cond_8
    sget-object v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;->UNKNOWN:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->setGamerTagState(Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;)V

    goto :goto_0
.end method

.method private setGamerTagState(Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;)V
    .locals 6
    .param p1, "newState"    # Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    .prologue
    const/16 v4, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 257
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->textGamerTagComment:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;->getStringId()I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    .line 258
    iget-object v5, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->textGamerTagComment:Landroid/widget/TextView;

    sget-object v1, Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;->UNKNOWN:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    if-ne p1, v1, :cond_4

    move v1, v2

    :goto_0
    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 260
    iget-object v5, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->editTextGamerTag:Landroid/widget/EditText;

    sget-object v1, Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;->CHECKING:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    if-eq p1, v1, :cond_5

    sget-object v1, Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;->UNINITIALIZED:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    if-eq p1, v1, :cond_5

    move v1, v2

    :goto_1
    invoke-virtual {v5, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 261
    sget-object v1, Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;->UNKNOWN:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    if-eq p1, v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;->ERROR:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    if-ne p1, v1, :cond_6

    :cond_0
    move v0, v2

    .line 262
    .local v0, "searchEnabled":Z
    :goto_2
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->textGamerTagComment:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 263
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->searchButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 264
    iget-object v5, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->searchButton:Landroid/view/View;

    if-eqz v0, :cond_7

    move v1, v3

    :goto_3
    invoke-virtual {v5, v1}, Landroid/view/View;->setVisibility(I)V

    .line 265
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->claimItButton:Landroid/widget/Button;

    sget-object v5, Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;->AVAILABLE:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    if-eq p1, v5, :cond_1

    sget-object v5, Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;->INITIAL:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    if-ne p1, v5, :cond_8

    :cond_1
    :goto_4
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 267
    sget-object v1, Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;->UNAVAILABLE_WITH_SUGGESTIONS:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    if-ne p1, v1, :cond_9

    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->gamerTagState:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    if-eq v1, p1, :cond_9

    .line 268
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->suggestionsListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->clear()V

    .line 269
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->state:Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;

    invoke-virtual {v1}, Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;->hasSuggestions()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 270
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->suggestionsListAdapter:Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->state:Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;

    iget-object v2, v2, Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;->suggestions:Lcom/microsoft/xbox/idp/model/Suggestions$Response;

    iget-object v2, v2, Lcom/microsoft/xbox/idp/model/Suggestions$Response;->Gamertags:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 272
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->suggestionsListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 278
    :cond_3
    :goto_5
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->clearTextButton:Landroid/view/View;

    iget-object v2, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->editTextGamerTag:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a

    :goto_6
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 280
    iput-object p1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->gamerTagState:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    .line 281
    return-void

    .end local v0    # "searchEnabled":Z
    :cond_4
    move v1, v3

    .line 258
    goto :goto_0

    :cond_5
    move v1, v3

    .line 260
    goto :goto_1

    :cond_6
    move v0, v3

    .line 261
    goto :goto_2

    .restart local v0    # "searchEnabled":Z
    :cond_7
    move v1, v4

    .line 264
    goto :goto_3

    :cond_8
    move v2, v3

    .line 265
    goto :goto_4

    .line 273
    :cond_9
    sget-object v1, Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;->UNAVAILABLE_WITH_SUGGESTIONS:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    if-eq p1, v1, :cond_3

    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->gamerTagState:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    sget-object v2, Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;->UNAVAILABLE_WITH_SUGGESTIONS:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    if-ne v1, v2, :cond_3

    .line 274
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->suggestionsListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->clear()V

    .line 275
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->suggestionsListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    goto :goto_5

    :cond_a
    move v4, v3

    .line 278
    goto :goto_6
.end method


# virtual methods
.method public bridge synthetic getActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 51
    invoke-super {p0}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    return-object v0
.end method

.method public getLoaderInfo(I)Lcom/microsoft/xbox/idp/util/ErrorHelper$LoaderInfo;
    .locals 1
    .param p1, "loaderId"    # I

    .prologue
    .line 253
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->loaderMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/idp/util/ErrorHelper$LoaderInfo;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 200
    invoke-super {p0, p1, p2, p3}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 202
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->state:Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;

    iget-object v1, v1, Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    if-eqz v1, :cond_0

    .line 203
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->state:Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;

    iget-object v1, v1, Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    invoke-virtual {v1, p1, p2, p3}, Lcom/microsoft/xbox/idp/util/ErrorHelper;->getActivityResult(IILandroid/content/Intent;)Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityResult;

    move-result-object v0

    .line 204
    .local v0, "result":Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityResult;
    if-eqz v0, :cond_0

    .line 205
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->state:Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;

    iget-object v1, v1, Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    invoke-virtual {v1}, Lcom/microsoft/xbox/idp/util/ErrorHelper;->deleteLoader()V

    .line 207
    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityResult;->isTryAgain()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 208
    sget-object v1, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->TAG:Ljava/lang/String;

    const-string v2, "Trying again"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    .end local v0    # "result":Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityResult;
    :cond_0
    :goto_0
    return-void

    .line 210
    .restart local v0    # "result":Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityResult;
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->TAG:Ljava/lang/String;

    const-string v2, "onActivityResult"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/SignUpFragment$Callbacks;

    sget-object v2, Lcom/microsoft/xbox/idp/ui/SignUpFragment$Status;->PROVIDER_ERROR:Lcom/microsoft/xbox/idp/ui/SignUpFragment$Status;

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/idp/ui/SignUpFragment$Callbacks;->onCloseWithStatus(Lcom/microsoft/xbox/idp/ui/SignUpFragment$Status;)V

    goto :goto_0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 84
    invoke-super {p0, p1}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onAttach(Landroid/content/Context;)V

    .line 85
    check-cast p1, Lcom/microsoft/xbox/idp/ui/SignUpFragment$Callbacks;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/SignUpFragment$Callbacks;

    .line 86
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 219
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 220
    .local v0, "id":I
    sget v1, Lcom/microsoft/xbox/idp/R$id;->xbid_enter_gamertag_comment:I

    if-eq v0, v1, :cond_0

    sget v1, Lcom/microsoft/xbox/idp/R$id;->xbid_search:I

    if-ne v0, v1, :cond_2

    .line 221
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;->CHECKING:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->setGamerTagState(Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;)V

    .line 222
    sget-object v1, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->TAG:Ljava/lang/String;

    const-string v2, "Restarting loader_reserve_gamertag"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->provisioningResult:Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;

    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->getActivityTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCSignup;->trackSearchGamerTag(Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;Ljava/lang/CharSequence;)V

    .line 225
    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    sget v2, Lcom/microsoft/xbox/idp/R$id;->loader_reserve_gamertag:I

    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->gamerTagReservationCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v1, v2, v4, v3}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 249
    :cond_1
    :goto_0
    return-void

    .line 226
    :cond_2
    sget v1, Lcom/microsoft/xbox/idp/R$id;->xbid_aleady_have_gamer_tag_answer:I

    if-ne v0, v1, :cond_3

    .line 228
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->provisioningResult:Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;

    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->getActivityTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCSignup;->trackSignInWithDifferentUser(Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;Ljava/lang/CharSequence;)V

    .line 229
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCUser;->setIsSilent(Z)V

    .line 230
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/SignUpFragment$Callbacks;

    sget-object v2, Lcom/microsoft/xbox/idp/ui/SignUpFragment$Status;->ERROR_SWITCH_USER:Lcom/microsoft/xbox/idp/ui/SignUpFragment$Status;

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/idp/ui/SignUpFragment$Callbacks;->onCloseWithStatus(Lcom/microsoft/xbox/idp/ui/SignUpFragment$Status;)V

    goto :goto_0

    .line 231
    :cond_3
    sget v1, Lcom/microsoft/xbox/idp/R$id;->xbid_claim_it:I

    if-ne v0, v1, :cond_5

    .line 232
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->gamerTagState:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    sget-object v2, Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;->INITIAL:Lcom/microsoft/xbox/idp/ui/SignUpFragment$GamerTagState;

    if-ne v1, v2, :cond_4

    .line 233
    sget-object v1, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->TAG:Ljava/lang/String;

    const-string v2, "Interop.SignUpStatus.NO_ERROR"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/SignUpFragment$Callbacks;

    sget-object v2, Lcom/microsoft/xbox/idp/ui/SignUpFragment$Status;->NO_ERROR:Lcom/microsoft/xbox/idp/ui/SignUpFragment$Status;

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/idp/ui/SignUpFragment$Callbacks;->onCloseWithStatus(Lcom/microsoft/xbox/idp/ui/SignUpFragment$Status;)V

    .line 241
    :goto_1
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->provisioningResult:Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;

    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->getActivityTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCSignup;->trackClaimGamerTag(Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 236
    :cond_4
    sget-object v1, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->TAG:Ljava/lang/String;

    const-string v2, "Restarting loader_claim_gamertag"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    sget v2, Lcom/microsoft/xbox/idp/R$id;->loader_claim_gamertag:I

    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->gamerTagClaimCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v1, v2, v4, v3}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_1

    .line 243
    :cond_5
    sget v1, Lcom/microsoft/xbox/idp/R$id;->xbid_clear_text:I

    if-ne v0, v1, :cond_1

    .line 244
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->editTextGamerTag:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 247
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->provisioningResult:Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;

    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->getActivityTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCSignup;->trackClearGamerTag(Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 99
    invoke-super {p0, p1}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 101
    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 102
    .local v0, "args":Landroid/os/Bundle;
    if-nez v0, :cond_1

    .line 103
    sget-object v1, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->TAG:Ljava/lang/String;

    const-string v2, "No arguments provided"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/SignUpFragment$Callbacks;

    sget-object v2, Lcom/microsoft/xbox/idp/ui/SignUpFragment$Status;->PROVIDER_ERROR:Lcom/microsoft/xbox/idp/ui/SignUpFragment$Status;

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/idp/ui/SignUpFragment$Callbacks;->onCloseWithStatus(Lcom/microsoft/xbox/idp/ui/SignUpFragment$Status;)V

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    const-string v1, "ARG_ACCOUNT_PROVISIONING_RESULT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 106
    sget-object v1, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->TAG:Ljava/lang/String;

    const-string v2, "No ARG_ACCOUNT_PROVISIONING_RESULT"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/SignUpFragment$Callbacks;

    sget-object v2, Lcom/microsoft/xbox/idp/ui/SignUpFragment$Status;->PROVIDER_ERROR:Lcom/microsoft/xbox/idp/ui/SignUpFragment$Status;

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/idp/ui/SignUpFragment$Callbacks;->onCloseWithStatus(Lcom/microsoft/xbox/idp/ui/SignUpFragment$Status;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 115
    sget v0, Lcom/microsoft/xbox/idp/R$layout;->xbid_fragment_sign_up:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 91
    invoke-static {}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCPageView;->removePage()V

    .line 93
    invoke-super {p0}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onDetach()V

    .line 94
    sget-object v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->NO_OP_CALLBACKS:Lcom/microsoft/xbox/idp/ui/SignUpFragment$Callbacks;

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/SignUpFragment$Callbacks;

    .line 95
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 188
    invoke-super {p0}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onResume()V

    .line 189
    iget-object v0, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->state:Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;

    iget-object v0, v0, Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/util/ErrorHelper;->restartLoader()Z

    .line 190
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 194
    invoke-super {p0, p1}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 195
    const-string v0, "KEY_STATE"

    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->state:Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 196
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 120
    invoke-super {p0, p1, p2}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 122
    sget v3, Lcom/microsoft/xbox/idp/R$id;->xbid_scroll_container:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ScrollView;

    iput-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->scrollView:Landroid/widget/ScrollView;

    .line 123
    sget v3, Lcom/microsoft/xbox/idp/R$id;->xbid_bottom_bar_shadow:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->bottomBarShadow:Landroid/view/View;

    .line 124
    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->scrollView:Landroid/widget/ScrollView;

    new-instance v4, Lcom/microsoft/xbox/idp/ui/SignUpFragment$1;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment$1;-><init>(Lcom/microsoft/xbox/idp/ui/SignUpFragment;)V

    invoke-virtual {v3, v4}, Landroid/widget/ScrollView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 130
    sget v3, Lcom/microsoft/xbox/idp/R$id;->xbid_enter_gamertag_container:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->editTextGamerTagContainer:Landroid/view/View;

    .line 131
    sget v3, Lcom/microsoft/xbox/idp/R$id;->xbid_enter_gamertag:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->editTextGamerTag:Landroid/widget/EditText;

    .line 132
    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->editTextGamerTag:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->gamerTagChangeListener:Landroid/text/TextWatcher;

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 133
    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->editTextGamerTag:Landroid/widget/EditText;

    new-instance v4, Lcom/microsoft/xbox/idp/ui/SignUpFragment$2;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment$2;-><init>(Lcom/microsoft/xbox/idp/ui/SignUpFragment;)V

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 139
    sget v3, Lcom/microsoft/xbox/idp/R$id;->xbid_clear_text:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->clearTextButton:Landroid/view/View;

    .line 140
    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->clearTextButton:Landroid/view/View;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    sget v3, Lcom/microsoft/xbox/idp/R$id;->xbid_search:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->searchButton:Landroid/view/View;

    .line 142
    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->searchButton:Landroid/view/View;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    sget v3, Lcom/microsoft/xbox/idp/R$id;->xbid_enter_gamertag_comment:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->textGamerTagComment:Landroid/widget/TextView;

    .line 144
    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->textGamerTagComment:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    sget v3, Lcom/microsoft/xbox/idp/R$id;->xbid_privacy:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->privacyText:Landroid/widget/TextView;

    .line 146
    sget v3, Lcom/microsoft/xbox/idp/R$id;->xbid_privacy_details:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->privacyDetailsText:Landroid/widget/TextView;

    .line 147
    sget v3, Lcom/microsoft/xbox/idp/R$id;->xbid_aleady_have_gamer_tag_answer:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 148
    .local v2, "diffAccountLink":Landroid/widget/TextView;
    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<u>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/microsoft/xbox/idp/R$string;->xbid_another_sign_in:I

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "</u>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    sget v3, Lcom/microsoft/xbox/idp/R$id;->xbid_claim_it:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->claimItButton:Landroid/widget/Button;

    .line 151
    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->claimItButton:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    sget v3, Lcom/microsoft/xbox/idp/R$id;->xbid_suggestions_list:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/AbsListView;

    iput-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->suggestionsList:Landroid/widget/AbsListView;

    .line 153
    new-instance v3, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    sget v5, Lcom/microsoft/xbox/idp/R$layout;->xbid_row_suggestion:I

    sget v6, Lcom/microsoft/xbox/idp/R$id;->xbid_suggestion_text:I

    invoke-direct {v3, v4, v5, v6}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    iput-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->suggestionsListAdapter:Landroid/widget/ArrayAdapter;

    .line 154
    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->suggestionsList:Landroid/widget/AbsListView;

    iget-object v4, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->suggestionsListAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 155
    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->suggestionsList:Landroid/widget/AbsListView;

    iget-object v4, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->onSuggestionClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/AbsListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 157
    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 158
    .local v1, "args":Landroid/os/Bundle;
    const-string v3, "ARG_ACCOUNT_PROVISIONING_RESULT"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;

    iput-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->provisioningResult:Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;

    .line 160
    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->provisioningResult:Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;

    if-eqz v3, :cond_0

    .line 161
    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->provisioningResult:Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;

    invoke-virtual {v3}, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;->getXuid()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/idp/telemetry/utc/model/UTCCommonDataModel;->setUserId(Ljava/lang/String;)V

    .line 163
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->getActivityTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/idp/telemetry/helpers/UTCSignup;->trackPageView(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->provisioningResult:Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;

    invoke-virtual {v3}, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;->getAgeGroup()Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult$AgeGroup;

    move-result-object v0

    .line 166
    .local v0, "ageGroup":Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult$AgeGroup;
    if-nez p2, :cond_1

    .line 167
    new-instance v3, Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;

    invoke-direct {v3}, Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;-><init>()V

    iput-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->state:Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;

    .line 168
    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->state:Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;

    iget-object v4, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->provisioningResult:Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;

    invoke-virtual {v4}, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult;->getGamerTag()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;->gamerTag:Ljava/lang/String;

    .line 169
    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->editTextGamerTag:Landroid/widget/EditText;

    iget-object v4, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->state:Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;

    iget-object v4, v4, Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;->gamerTag:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 175
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->state:Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;

    iget-object v3, v3, Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;->errorHelper:Lcom/microsoft/xbox/idp/util/ErrorHelper;

    invoke-virtual {v3, p0}, Lcom/microsoft/xbox/idp/util/ErrorHelper;->setActivityContext(Lcom/microsoft/xbox/idp/util/ErrorHelper$ActivityContext;)V

    .line 177
    if-eqz v0, :cond_2

    .line 178
    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->privacyText:Landroid/widget/TextView;

    sget v4, Lcom/microsoft/xbox/idp/R$string;->xbid_privacy_settings_header_android:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v7, v0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult$AgeGroup;->resIdAgeGroup:I

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {p0, v4, v5}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->privacyDetailsText:Landroid/widget/TextView;

    iget v4, v0, Lcom/microsoft/xbox/idp/ui/AccountProvisioningResult$AgeGroup;->resIdAgeGroupDetails:I

    iget-object v5, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->xboxDotComLauncher:Landroid/text/style/ClickableSpan;

    invoke-static {v3, v4, v5}, Lcom/microsoft/xbox/idp/ui/UiUtil;->ensureClickableSpanOnUnderlineSpan(Landroid/widget/TextView;ILandroid/text/style/ClickableSpan;)V

    .line 184
    :goto_1
    return-void

    .line 171
    :cond_1
    const-string v3, "KEY_STATE"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;

    iput-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->state:Lcom/microsoft/xbox/idp/ui/SignUpFragment$State;

    .line 172
    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->editTextGamerTag:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->resetGamerTagState(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 181
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->privacyText:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v3, p0, Lcom/microsoft/xbox/idp/ui/SignUpFragment;->privacyDetailsText:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
