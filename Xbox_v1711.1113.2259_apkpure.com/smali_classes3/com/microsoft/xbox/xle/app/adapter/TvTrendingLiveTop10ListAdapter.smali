.class public Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "TvTrendingLiveTop10ListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowViewResourceId"    # I

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 18
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 24
    if-nez p2, :cond_3

    .line 25
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 26
    .local v2, "vi":Landroid/view/LayoutInflater;
    const v3, 0x7f03024c

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 28
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;

    const/4 v3, 0x0

    invoke-direct {v0, v3}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$1;)V

    .line 29
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;
    const v3, 0x7f0e0b4f

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    invoke-static {v0, v3}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;->access$102(Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;)Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 30
    const v3, 0x7f0e0b50

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v0, v3}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;->access$202(Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 31
    const v3, 0x7f0e0b51

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v0, v3}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;->access$302(Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 33
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 38
    .end local v2    # "vi":Landroid/view/LayoutInflater;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;

    .line 39
    .local v1, "item":Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;
    if-eqz v1, :cond_2

    .line 40
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;->access$100(Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 41
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;->access$100(Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    move-result-object v3

    iget-object v4, v1, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;->imageURI:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageURI2(Ljava/lang/String;)V

    .line 43
    :cond_0
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;->access$200(Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 44
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;->access$200(Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v3

    iget-object v4, v1, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;->title:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    :cond_1
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;->access$300(Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 47
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;->access$300(Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v3

    iget-object v4, v1, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;->description:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    :cond_2
    return-object p2

    .line 35
    .end local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;
    .end local v1    # "item":Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$TempMockTvTrendingItem;
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;

    .restart local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/TvTrendingLiveTop10ListAdapter$ViewHolder;
    goto :goto_0
.end method
