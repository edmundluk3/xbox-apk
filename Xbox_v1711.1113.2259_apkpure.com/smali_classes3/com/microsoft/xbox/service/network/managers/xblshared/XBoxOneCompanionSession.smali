.class public Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;
.super Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;
.source "XBoxOneCompanionSession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;,
        Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;
    }
.end annotation


# static fields
.field private static final MEDIA_STATE_CHATTY_THRESHOLD:I = 0x7d0

.field private static final MYTAG:Ljava/lang/String; = "XBoxOneCompanionSession"

.field private static final instance:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;


# instance fields
.field private eventSimulator:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;

.field private oldMediaStates:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;",
            "Lcom/microsoft/xbox/smartglass/SGMediaState;",
            ">;"
        }
    .end annotation
.end field

.field protected sessionManagerListener:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->instance:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;-><init>()V

    .line 63
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->oldMediaStates:Ljava/util/Hashtable;

    .line 67
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;-><init>(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->sessionManagerListener:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;

    .line 68
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;)Ljava/util/Hashtable;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->oldMediaStates:Ljava/util/Hashtable;

    return-object v0
.end method

.method public static getInstance()Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->instance:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    return-object v0
.end method


# virtual methods
.method public getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;
    .locals 1

    .prologue
    .line 78
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->getPlatformReady()Lcom/microsoft/xbox/toolkit/Ready;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/Ready;->getIsReady()Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    const/4 v0, 0x0

    .line 81
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v0

    goto :goto_0
.end method

.method protected getSessionManagerListener()Lcom/microsoft/xbox/smartglass/SessionManagerListener;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->sessionManagerListener:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XBoxOneSessionManagerListener;

    return-object v0
.end method

.method protected getTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    const-string v0, "XBoxOneCompanionSession"

    return-object v0
.end method

.method public joinSession(Ljava/lang/String;)V
    .locals 4
    .param p1, "consoleIpAddress"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 90
    const-string v1, "XBoxOneCompanionSession"

    const-string v2, "Adding session manager listener."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->getSessionManagerListener()Lcom/microsoft/xbox/smartglass/SessionManagerListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/smartglass/SessionManager;->addListener(Ljava/lang/Object;)V

    .line 92
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->getCurrentSessionState()I

    move-result v0

    .line 93
    .local v0, "latestState":I
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 94
    const-string v1, "XBoxOneCompanionSession"

    const-string v2, "companion session is already connected, but session mode does not think it is connected. CompansionSession must have received DISCONNECTED after JoinSessionRunner has started."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :goto_0
    return-void

    .line 96
    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 97
    const-string v1, "XBoxOneCompanionSession"

    const-string v2, "companion session is connecting, but session mode does not think it is connecting, we missed a callback"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 99
    :cond_1
    const-string v1, "XBoxOneCompanionSession"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "join session because session manager state is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->joinSession(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public startSimulator()V
    .locals 2

    .prologue
    .line 365
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getNowPlayingSimulatorEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 366
    const-string v0, "XBoxOneCompanionSession"

    const-string v1, "Starting NowPlaying simulator"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->eventSimulator:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;

    if-nez v0, :cond_0

    .line 368
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;-><init>(Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->eventSimulator:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;

    .line 370
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->eventSimulator:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->start()V

    .line 372
    :cond_1
    return-void
.end method

.method public stopSimulator()V
    .locals 2

    .prologue
    .line 375
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->eventSimulator:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->eventSimulator:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 376
    const-string v0, "XBoxOneCompanionSession"

    const-string v1, "Stopping NowPlaying simulator"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->eventSimulator:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;->stop()V

    .line 378
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->eventSimulator:Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession$XboxOneEventSimulator;

    .line 380
    :cond_0
    return-void
.end method
