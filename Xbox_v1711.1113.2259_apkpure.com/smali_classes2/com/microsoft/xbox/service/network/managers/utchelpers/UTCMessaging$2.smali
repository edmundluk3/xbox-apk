.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$2;
.super Ljava/lang/Object;
.source "UTCMessaging.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackSendMessage(Ljava/lang/String;Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$conversationId:Ljava/lang/String;

.field final synthetic val$members:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$2;->val$conversationId:Ljava/lang/String;

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$2;->val$members:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 6

    .prologue
    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 67
    .local v0, "allMembers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$2;->val$conversationId:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$2;->val$members:Ljava/util/List;

    if-nez v4, :cond_0

    .line 68
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$2;->val$conversationId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/model/MessageModel;->getConversationMembers(Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    .line 69
    .local v2, "member":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getXuidFromSkypeMessageId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 70
    .local v1, "id":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 73
    .end local v1    # "id":Ljava/lang/String;
    .end local v2    # "member":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$2;->val$members:Ljava/util/List;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 76
    :cond_1
    new-instance v3, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 77
    .local v3, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v5, "ConversationId"

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$2;->val$conversationId:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "UNKNOWN"

    :goto_1
    invoke-virtual {v3, v5, v4}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 78
    const-string v4, "ConversationMembers"

    invoke-virtual {v3, v4, v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 80
    const-string v4, "Messaging - Send Message"

    invoke-static {v4, v3}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 81
    return-void

    .line 77
    :cond_2
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$2;->val$conversationId:Ljava/lang/String;

    goto :goto_1
.end method
