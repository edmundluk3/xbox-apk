.class public Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "PartyAndLfgScreenViewModel.java"


# static fields
.field private static final MPSD_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

.field private static final TAG:Ljava/lang/String;

.field private static final TITLE_HUB_MODEL:Lcom/microsoft/xbox/service/model/TitleHubModel;


# instance fields
.field private getClubPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

.field private getFollowingPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

.field private final getPersonSummariesResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;>;"
        }
    .end annotation
.end field

.field private getUpcomingPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

.field private hasClubLoadError:Z

.field private hasFollowingLoadError:Z

.field private hasUpcomingLoadError:Z

.field private isForcingRefresh:Z

.field private isInParty:Ljava/lang/Boolean;

.field private isLoadingClubSessions:Z

.field private isLoadingFollowingSessions:Z

.field private isLoadingUpcomingSessions:Z

.field private lfgClubSessions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation
.end field

.field private lfgFollowingSessions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation
.end field

.field private lfgUpcomingSessions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation
.end field

.field partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private personSummaries:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;"
        }
    .end annotation
.end field

.field systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field telemetryService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->TAG:Ljava/lang/String;

    .line 56
    sget-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->MPSD_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    .line 57
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->TITLE_HUB_MODEL:Lcom/microsoft/xbox/service/model/TitleHubModel;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isInParty:Ljava/lang/Boolean;

    .line 90
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;)V

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->lfgUpcomingSessions:Ljava/util/List;

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->lfgFollowingSessions:Ljava/util/List;

    .line 94
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->personSummaries:Ljava/util/Map;

    .line 95
    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getPersonSummariesResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 96
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 97
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->onUserDataLoadCompleted(Ljava/util/List;)V

    return-void
.end method

.method static synthetic lambda$load$0(Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponse;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;
    .param p1, "response"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 217
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSessionQueryResponse;->results()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isInParty:Ljava/lang/Boolean;

    .line 218
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->updateAdapter()V

    .line 219
    return-void

    .line 217
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$loadTitleInfoForLfgs$1(Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;)Lio/reactivex/SingleSource;
    .locals 6
    .param p0, "titleData"    # Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 431
    iget-wide v2, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->titleId:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    .line 433
    .local v0, "titleModel":Lcom/microsoft/xbox/service/model/TitleModel;
    if-eqz v0, :cond_0

    .line 434
    const/4 v1, 0x0

    invoke-virtual {v0, v1, p0}, Lcom/microsoft/xbox/service/model/TitleModel;->rxLoadGameProgressAchievements(ZLcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;)Lio/reactivex/Single;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel$$Lambda$5;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v2

    .line 435
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v1

    .line 438
    :goto_0
    return-object v1

    .line 437
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not load titleModel for title id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->titleId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    invoke-static {}, Lcom/google/common/base/Optional;->absent()Lcom/google/common/base/Optional;

    move-result-object v1

    invoke-static {v1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v1

    goto :goto_0
.end method

.method static synthetic lambda$loadTitleInfoForLfgs$2(Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;Lcom/google/common/base/Optional;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;
    .param p1, "success"    # Lcom/google/common/base/Optional;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 451
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Loaded title data"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->updateAdapter()V

    .line 462
    return-void
.end method

.method private loadTitleInfoForLfgs(Ljava/util/Set;)V
    .locals 10
    .param p1    # Ljava/util/Set;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 419
    .local p1, "titleIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 420
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 422
    .local v3, "titleQueries":Ljava/util/List;, "Ljava/util/List<Lio/reactivex/Single<Lcom/google/common/base/Optional<Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;>;>;>;"
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 424
    .local v2, "titleId":Ljava/lang/String;
    sget-object v5, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->TITLE_HUB_MODEL:Lcom/microsoft/xbox/service/model/TitleHubModel;

    const/4 v6, 0x0

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v5, v6, v8, v9}, Lcom/microsoft/xbox/service/model/TitleHubModel;->rxLoad(ZJ)Lio/reactivex/Single;

    move-result-object v1

    .line 429
    .local v1, "titleDataSingle":Lio/reactivex/Single;, "Lio/reactivex/Single<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;"
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->computation()Lio/reactivex/Scheduler;

    move-result-object v5

    invoke-virtual {v1, v5}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v5

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v6

    .line 430
    invoke-virtual {v5, v6}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 442
    .local v0, "achievementsSingle":Lio/reactivex/Single;, "Lio/reactivex/Single<Lcom/google/common/base/Optional<Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;>;>;"
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 445
    .end local v0    # "achievementsSingle":Lio/reactivex/Single;, "Lio/reactivex/Single<Lcom/google/common/base/Optional<Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;>;>;>;"
    .end local v1    # "titleDataSingle":Lio/reactivex/Single;, "Lio/reactivex/Single<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;"
    .end local v2    # "titleId":Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 446
    invoke-static {v3}, Lio/reactivex/Single;->merge(Ljava/lang/Iterable;)Lio/reactivex/Flowable;

    move-result-object v5

    .line 447
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v6

    invoke-virtual {v5, v6}, Lio/reactivex/Flowable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Flowable;

    move-result-object v5

    .line 448
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v6

    invoke-virtual {v5, v6}, Lio/reactivex/Flowable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Flowable;

    move-result-object v5

    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v6

    .line 449
    invoke-virtual {v5, v6}, Lio/reactivex/Flowable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v5

    .line 445
    invoke-virtual {v4, v5}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 466
    return-void
.end method

.method private declared-synchronized onClubLfgSessionsLoaded(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 6
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 331
    monitor-enter p0

    :try_start_0
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onClubLfgSessionsLoaded, status: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isLoadingClubSessions:Z

    .line 334
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 370
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->updateViewModelState()V

    .line 371
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->updateAdapter()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 372
    monitor-exit p0

    return-void

    .line 338
    :pswitch_0
    :try_start_1
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->MPSD_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    sget-object v4, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Clubs:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->getSessionsResult(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->lfgClubSessions:Ljava/util/List;

    .line 340
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 341
    .local v1, "hostXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 343
    .local v2, "titleIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->lfgClubSessions:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 344
    .local v0, "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->getHostXuid()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 347
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 331
    .end local v0    # "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .end local v1    # "hostXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "titleIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 351
    .restart local v1    # "hostXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "titleIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_2
    :try_start_2
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->loadTitleInfoForLfgs(Ljava/util/Set;)V

    .line 353
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 354
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getClubPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    if-eqz v3, :cond_3

    .line 355
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getClubPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->cancel()V

    .line 358
    :cond_3
    new-instance v3, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getPersonSummariesResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-direct {v3, v1, v4}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;-><init>(Ljava/util/Collection;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getClubPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    .line 359
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getClubPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->load(Z)V

    goto :goto_0

    .line 366
    .end local v1    # "hostXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "titleIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :pswitch_1
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->hasClubLoadError:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 334
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private declared-synchronized onLfgFollowingSessionsLoaded(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 7
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 285
    monitor-enter p0

    :try_start_0
    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onLfgFollowingSessionsLoaded, status: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isLoadingFollowingSessions:Z

    .line 288
    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 326
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->updateViewModelState()V

    .line 327
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->updateAdapter()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 328
    monitor-exit p0

    return-void

    .line 292
    :pswitch_0
    :try_start_1
    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->MPSD_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    sget-object v5, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Following:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->getSessionsResult(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->lfgFollowingSessions:Ljava/util/List;

    .line 293
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 294
    .local v2, "hostXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 296
    .local v3, "titleIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->lfgFollowingSessions:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 297
    .local v0, "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->getHostXuid()Ljava/lang/String;

    move-result-object v1

    .line 299
    .local v1, "hostXuid":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->personSummaries:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 300
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 302
    :cond_2
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 303
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 285
    .end local v0    # "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .end local v1    # "hostXuid":Ljava/lang/String;
    .end local v2    # "hostXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "titleIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 307
    .restart local v2    # "hostXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v3    # "titleIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_3
    :try_start_2
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->loadTitleInfoForLfgs(Ljava/util/Set;)V

    .line 309
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 310
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getFollowingPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    if-eqz v4, :cond_4

    .line 311
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getFollowingPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->cancel()V

    .line 314
    :cond_4
    new-instance v4, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getPersonSummariesResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-direct {v4, v2, v5}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;-><init>(Ljava/util/Collection;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getFollowingPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    .line 315
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getFollowingPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->load(Z)V

    goto/16 :goto_0

    .line 322
    .end local v2    # "hostXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "titleIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :pswitch_1
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->hasFollowingLoadError:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 288
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private declared-synchronized onLfgUpcomingSessionsLoaded(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 6
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 375
    monitor-enter p0

    :try_start_0
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onLfgUpcomingSessionsLoaded, status: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isLoadingUpcomingSessions:Z

    .line 378
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 414
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->updateViewModelState()V

    .line 415
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->updateAdapter()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 416
    monitor-exit p0

    return-void

    .line 382
    :pswitch_0
    :try_start_1
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->MPSD_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    sget-object v4, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Upcoming:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->getSessionsResult(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->lfgUpcomingSessions:Ljava/util/List;

    .line 384
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 385
    .local v1, "hostXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 387
    .local v2, "titleIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->lfgUpcomingSessions:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 388
    .local v0, "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->getHostXuid()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 390
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 391
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 375
    .end local v0    # "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .end local v1    # "hostXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "titleIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 395
    .restart local v1    # "hostXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "titleIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_2
    :try_start_2
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->loadTitleInfoForLfgs(Ljava/util/Set;)V

    .line 397
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 398
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getUpcomingPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    if-eqz v3, :cond_3

    .line 399
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getUpcomingPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->cancel()V

    .line 402
    :cond_3
    new-instance v3, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getPersonSummariesResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-direct {v3, v1, v4}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;-><init>(Ljava/util/Collection;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getUpcomingPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    .line 403
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getUpcomingPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->load(Z)V

    goto :goto_0

    .line 410
    .end local v1    # "hostXuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "titleIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :pswitch_1
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->hasUpcomingLoadError:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 378
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private declared-synchronized onUserDataLoadCompleted(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 517
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 518
    .local v0, "person":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->personSummaries:Ljava/util/Map;

    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 517
    .end local v0    # "person":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 521
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->updateAdapter()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 522
    monitor-exit p0

    return-void
.end method

.method private updateViewModelState()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 469
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isBusy()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 470
    sget-object v4, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 509
    :goto_0
    return-void

    .line 471
    :cond_0
    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->hasUpcomingLoadError:Z

    if-eqz v4, :cond_1

    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->hasFollowingLoadError:Z

    if-eqz v4, :cond_1

    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->hasClubLoadError:Z

    if-eqz v4, :cond_1

    .line 472
    sget-object v4, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 473
    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isForcingRefresh:Z

    goto :goto_0

    .line 474
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getLfgFollowingSessions()Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 475
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getLfgUpcomingSessions()Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 476
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getClubSessions()Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 477
    sget-object v4, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 478
    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isForcingRefresh:Z

    goto :goto_0

    .line 480
    :cond_2
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->lfgUpcomingSessions:Ljava/util/List;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 481
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->lfgFollowingSessions:Ljava/util/List;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 482
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->lfgUpcomingSessions:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 483
    .local v2, "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->lfgFollowingSessions:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 484
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 485
    .local v1, "followingHandle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->id()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->id()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 486
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 493
    .end local v1    # "followingHandle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .end local v2    # "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .end local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    :cond_5
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->lfgClubSessions:Ljava/util/List;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 494
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->lfgUpcomingSessions:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_6
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 495
    .restart local v2    # "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->lfgFollowingSessions:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    :cond_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 496
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 497
    .local v0, "clubHandle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->id()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->id()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 498
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    .line 506
    .end local v0    # "clubHandle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .end local v2    # "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .end local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;>;"
    :cond_8
    sget-object v4, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 507
    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isForcingRefresh:Z

    goto/16 :goto_0
.end method


# virtual methods
.method protected createAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 131
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/PartyAndLfgScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;)V

    return-object v0
.end method

.method public createParty()V
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->telemetryService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;->startParty()V

    .line 231
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->createParty()Lio/reactivex/Observable;

    move-result-object v0

    .line 232
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 233
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 234
    invoke-virtual {v0}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    .line 236
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->viewParty()V

    .line 237
    return-void
.end method

.method public getClubSessions()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->lfgClubSessions:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getErrorString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 105
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 107
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getCanCommunicateWithTextAndVoice()Z

    move-result v1

    if-nez v1, :cond_0

    .line 108
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0706ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 110
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0706e8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getLfgFollowingSessions()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->lfgFollowingSessions:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLfgUpcomingSessions()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->lfgUpcomingSessions:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getPeopleSummaries()Ljava/util/Map;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 513
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->personSummaries:Ljava/util/Map;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/Map;)Ljava/util/Map;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 176
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isLoadingFollowingSessions:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isLoadingUpcomingSessions:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isLoadingClubSessions:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInParty()Ljava/lang/Boolean;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 254
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isPartyEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isInParty:Ljava/lang/Boolean;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPartyEnabled()Z
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->systemSettingsModel:Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->isPartyChatEnabled()Z

    move-result v0

    return v0
.end method

.method public load(Z)V
    .locals 4
    .param p1, "forceRefresh"    # Z

    .prologue
    const/4 v3, 0x1

    .line 181
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "load "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    if-eqz p1, :cond_1

    .line 184
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isLoadingUpcomingSessions:Z

    .line 185
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isLoadingFollowingSessions:Z

    .line 186
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isLoadingClubSessions:Z

    .line 187
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 189
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->MPSD_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Following:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->loadLfgSessionsAsync(ZLcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)V

    .line 190
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->MPSD_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Upcoming:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->loadLfgSessionsAsync(ZLcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)V

    .line 191
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->MPSD_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Clubs:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->loadLfgSessionsAsync(ZLcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)V

    .line 212
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->partyChatRepository:Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyChatRepository;->getPartiesForUser(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v1

    .line 214
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v1

    .line 215
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 216
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 213
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 221
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->updateViewModelState()V

    .line 222
    return-void

    .line 193
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->MPSD_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Following:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->shouldRefresh(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 194
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 195
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isLoadingFollowingSessions:Z

    .line 196
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->MPSD_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Following:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->loadLfgSessionsAsync(ZLcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)V

    .line 199
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->MPSD_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Upcoming:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->shouldRefresh(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 200
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 201
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isLoadingUpcomingSessions:Z

    .line 202
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->MPSD_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Upcoming:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->loadLfgSessionsAsync(ZLcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)V

    .line 205
    :cond_3
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->MPSD_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Clubs:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->shouldRefresh(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 207
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isLoadingClubSessions:Z

    .line 208
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->MPSD_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Clubs:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->loadLfgSessionsAsync(ZLcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)V

    goto :goto_0
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    if-nez v0, :cond_0

    .line 146
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->createAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 148
    :cond_0
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    if-nez v0, :cond_0

    .line 137
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->createAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 140
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->MPSD_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 141
    return-void
.end method

.method protected onStopOverride()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getUpcomingPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getUpcomingPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->cancel()V

    .line 154
    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getUpcomingPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getFollowingPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    if-eqz v0, :cond_1

    .line 158
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getFollowingPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->cancel()V

    .line 159
    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getFollowingPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    .line 162
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getClubPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    if-eqz v0, :cond_2

    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getClubPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->cancel()V

    .line 164
    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->getClubPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    .line 167
    :cond_2
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isLoadingUpcomingSessions:Z

    .line 168
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isLoadingFollowingSessions:Z

    .line 169
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isLoadingClubSessions:Z

    .line 171
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->MPSD_MODEL_MANAGER:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 172
    return-void
.end method

.method public showCreateLfgDialog()V
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->telemetryService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;->createLFG()V

    .line 226
    const-class v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgSelectTitleScreen;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->NavigateTo(Ljava/lang/Class;Z)V

    .line 227
    return-void
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 5
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 259
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "updateOverride"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/UpdateData;

    .line 262
    .local v0, "result":Lcom/microsoft/xbox/service/model/UpdateData;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->isForcingRefresh:Z

    if-nez v2, :cond_1

    .line 263
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v1

    .line 265
    .local v1, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 276
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown update type ignored: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    .end local v1    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 282
    return-void

    .line 267
    .restart local v1    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :pswitch_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->onLfgFollowingSessionsLoaded(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    goto :goto_0

    .line 270
    :pswitch_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->onLfgUpcomingSessionsLoaded(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    goto :goto_0

    .line 273
    :pswitch_2
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->onClubLfgSessionsLoaded(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    goto :goto_0

    .line 265
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public viewParty()V
    .locals 4

    .prologue
    .line 241
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->telemetryService:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;

    invoke-virtual {v1}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerTelemetryService;->viewParty()V

    .line 242
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/presentation/party/PartyDetailsViewImpl;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GotoScreenWithPush(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 246
    :goto_0
    return-void

    .line 243
    :catch_0
    move-exception v0

    .line 244
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Could not navigate to party details"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
