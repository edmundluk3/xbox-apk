.class public final Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor_Factory;
.super Ljava/lang/Object;
.source "FriendPickerInteractor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final peopleHubServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;",
            ">;"
        }
    .end annotation
.end field

.field private final schedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final userSummaryDataMapperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor_Factory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor_Factory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24
    .local p1, "peopleHubServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;>;"
    .local p2, "userSummaryDataMapperProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;>;"
    .local p3, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    sget-boolean v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 26
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor_Factory;->peopleHubServiceProvider:Ljavax/inject/Provider;

    .line 27
    sget-boolean v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 28
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor_Factory;->userSummaryDataMapperProvider:Ljavax/inject/Provider;

    .line 29
    sget-boolean v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 30
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor_Factory;->schedulerProvider:Ljavax/inject/Provider;

    .line 31
    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "peopleHubServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;>;"
    .local p1, "userSummaryDataMapperProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;>;"
    .local p2, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    new-instance v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;
    .locals 4

    .prologue
    .line 35
    new-instance v3, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;

    iget-object v0, p0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor_Factory;->peopleHubServiceProvider:Ljavax/inject/Provider;

    .line 36
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;

    iget-object v1, p0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor_Factory;->userSummaryDataMapperProvider:Ljavax/inject/Provider;

    .line 37
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;

    iget-object v2, p0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor_Factory;->schedulerProvider:Ljavax/inject/Provider;

    .line 38
    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    invoke-direct {v3, v0, v1, v2}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;-><init>(Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V

    .line 35
    return-object v3
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor_Factory;->get()Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;

    move-result-object v0

    return-object v0
.end method
