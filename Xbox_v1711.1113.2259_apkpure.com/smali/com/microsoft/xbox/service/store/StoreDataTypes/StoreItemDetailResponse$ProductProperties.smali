.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;
.super Ljava/lang/Object;
.source "StoreItemDetailResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProductProperties"
.end annotation


# instance fields
.field private final attributes:Ljava/util/List;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Attributes"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;",
            ">;"
        }
    .end annotation
.end field

.field private final categories:Ljava/util/List;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Categories"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private transient coopAttributesMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;",
            ">;"
        }
    .end annotation
.end field

.field public final hasAddOns:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "HasAddOns"
    .end annotation
.end field

.field public final isDemo:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "IsDemo"
    .end annotation
.end field

.field private transient mapInitialized:Z

.field public final packageFamilyName:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "PackageFamilyName"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;ZZLjava/util/List;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "hasAddOns"    # Z
    .param p3, "isDemo"    # Z
    .param p4    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "packageFamilyName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;",
            ">;ZZ",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1241
    .local p1, "attributes":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;>;"
    .local p4, "categories":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1233
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;->mapInitialized:Z

    .line 1242
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;->attributes:Ljava/util/List;

    .line 1243
    iput-boolean p2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;->hasAddOns:Z

    .line 1244
    iput-boolean p3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;->isDemo:Z

    .line 1245
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;->categories:Ljava/util/List;

    .line 1246
    iput-object p5, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;->packageFamilyName:Ljava/lang/String;

    .line 1247
    return-void
.end method

.method private initCoopAttributesMap()V
    .locals 4

    .prologue
    .line 1264
    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;->attributes:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1265
    new-instance v1, Ljava/util/HashMap;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;->coopAttributesMap:Ljava/util/Map;

    .line 1274
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;->mapInitialized:Z

    .line 1275
    return-void

    .line 1267
    :cond_1
    new-instance v1, Ljava/util/HashMap;

    iget-object v2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;->attributes:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;->coopAttributesMap:Ljava/util/Map;

    .line 1269
    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;->attributes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;

    .line 1270
    .local v0, "attribute":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;
    iget-object v2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;->coopAttributesMap:Ljava/util/Map;

    iget-object v3, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;->name:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public getAttribute(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;
    .locals 1
    .param p1, "attributeName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1256
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;->mapInitialized:Z

    if-nez v0, :cond_0

    .line 1257
    invoke-direct {p0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;->initCoopAttributesMap()V

    .line 1260
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;->coopAttributesMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;

    return-object v0
.end method

.method public getAttributes()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1251
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;->attributes:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCategories()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1279
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;->categories:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
