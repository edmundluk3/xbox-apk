.class Lcom/microsoft/xbox/service/model/recents/RecentsModel$SingleRecentRunner;
.super Ljava/lang/Object;
.source "RecentsModel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/recents/RecentsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SingleRecentRunner"
.end annotation


# instance fields
.field private final doneSignal:Ljava/util/concurrent/CountDownLatch;

.field private final recent:Lcom/microsoft/xbox/service/model/recents/Recent;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/recents/RecentsModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/recents/RecentsModel;Lcom/microsoft/xbox/service/model/recents/Recent;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/model/recents/RecentsModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "recent"    # Lcom/microsoft/xbox/service/model/recents/Recent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "doneSignal"    # Ljava/util/concurrent/CountDownLatch;

    .prologue
    .line 186
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/recents/RecentsModel$SingleRecentRunner;->this$0:Lcom/microsoft/xbox/service/model/recents/RecentsModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 187
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 188
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 190
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/recents/RecentsModel$SingleRecentRunner;->doneSignal:Ljava/util/concurrent/CountDownLatch;

    .line 191
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/recents/RecentsModel$SingleRecentRunner;->recent:Lcom/microsoft/xbox/service/model/recents/Recent;

    .line 192
    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 198
    :try_start_0
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/recents/RecentsModel$SingleRecentRunner;->recent:Lcom/microsoft/xbox/service/model/recents/Recent;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/recents/RecentItem;->Provider:Ljava/lang/String;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->parseHexLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 199
    .local v4, "titleId":Ljava/lang/Long;
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_0

    .line 200
    invoke-static {}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->access$400()Lcom/microsoft/xbox/service/store/IStoreService;

    move-result-object v5

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-interface {v5, v6, v7}, Lcom/microsoft/xbox/service/store/IStoreService;->getProductFromTitleId(J)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v3

    .line 202
    .local v3, "productsList":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->getProducts()Ljava/util/List;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 203
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->getProducts()Ljava/util/List;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;

    .line 205
    .local v1, "itemDetail":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    iget-object v5, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->properties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;

    if-eqz v5, :cond_0

    iget-object v5, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->properties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;

    iget-object v5, v5, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;->packageFamilyName:Ljava/lang/String;

    .line 206
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 207
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getApplicationIdForPackage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 208
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "appx:%1$s!%2$s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->properties:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;

    iget-object v9, v9, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;->packageFamilyName:Ljava/lang/String;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->getApplicationIdForPackage()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 209
    .local v2, "launchUri":Ljava/lang/String;
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/recents/RecentsModel$SingleRecentRunner;->recent:Lcom/microsoft/xbox/service/model/recents/Recent;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-virtual {v5, v2}, Lcom/microsoft/xbox/service/model/recents/RecentItem;->setLaunchUri(Ljava/lang/String;)V

    .line 210
    invoke-static {}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->access$200()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Setting Launch Uri: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " for item: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/service/model/recents/RecentsModel$SingleRecentRunner;->recent:Lcom/microsoft/xbox/service/model/recents/Recent;

    iget-object v7, v7, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/recents/RecentItem;->getItemId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 217
    .end local v1    # "itemDetail":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    .end local v2    # "launchUri":Ljava/lang/String;
    .end local v3    # "productsList":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    :cond_0
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/recents/RecentsModel$SingleRecentRunner;->doneSignal:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v5}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 219
    .end local v4    # "titleId":Ljava/lang/Long;
    :goto_0
    return-void

    .line 214
    :catch_0
    move-exception v0

    .line 215
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :try_start_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/recents/RecentsModel;->access$200()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unable to assign launchUri to RecentItem: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 217
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/recents/RecentsModel$SingleRecentRunner;->doneSignal:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v5}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :catchall_0
    move-exception v5

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/recents/RecentsModel$SingleRecentRunner;->doneSignal:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v6}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v5
.end method
