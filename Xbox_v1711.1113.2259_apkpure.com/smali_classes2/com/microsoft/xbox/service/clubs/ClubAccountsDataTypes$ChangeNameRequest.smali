.class public final Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ChangeNameRequest;
.super Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$BaseClubChangeRequest;
.source "ClubAccountsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ChangeNameRequest"
.end annotation


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x4L
        .end annotation
    .end param

    .prologue
    .line 273
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;->ChangeName:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$BaseClubChangeRequest;-><init>(Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;)V

    .line 274
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 275
    const-wide/16 v0, 0x4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 276
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ChangeNameRequest;->name:Ljava/lang/String;

    .line 277
    return-void
.end method
