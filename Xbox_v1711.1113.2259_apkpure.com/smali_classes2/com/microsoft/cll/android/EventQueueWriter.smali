.class public Lcom/microsoft/cll/android/EventQueueWriter;
.super Ljava/lang/Object;
.source "EventQueueWriter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/cll/android/EventQueueWriter$SendResult;
    }
.end annotation


# static fields
.field protected static future:Ljava/util/concurrent/ScheduledFuture;

.field protected static power:I

.field protected static running:Ljava/util/concurrent/atomic/AtomicBoolean;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final batcher:Lcom/microsoft/cll/android/EventBatcher;

.field private final clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

.field private final cllEvents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/cll/android/ICllEvents;",
            ">;"
        }
    .end annotation
.end field

.field private compressor:Lcom/microsoft/cll/android/EventCompressor;

.field private endpoint:Ljava/net/URL;

.field private final event:Lcom/microsoft/cll/android/SerializedEvent;

.field private final executorService:Ljava/util/concurrent/ScheduledExecutorService;

.field private handler:Lcom/microsoft/cll/android/EventHandler;

.field private final ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final logger:Lcom/microsoft/cll/android/ILogger;

.field private removedStorages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/cll/android/IStorage;",
            ">;"
        }
    .end annotation
.end field

.field private sender:Lcom/microsoft/cll/android/EventSender;

.field private final storages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/cll/android/IStorage;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketCallback:Lcom/microsoft/cll/android/ITicketCallback;

.field private final ticketManager:Lcom/microsoft/cll/android/TicketManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/microsoft/cll/android/EventQueueWriter;->running:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 19
    const/4 v0, 0x1

    sput v0, Lcom/microsoft/cll/android/EventQueueWriter;->power:I

    return-void
.end method

.method public constructor <init>(Ljava/net/URL;Lcom/microsoft/cll/android/SerializedEvent;Ljava/util/List;Lcom/microsoft/cll/android/ClientTelemetry;Ljava/util/List;Lcom/microsoft/cll/android/ILogger;Ljava/util/concurrent/ScheduledExecutorService;Lcom/microsoft/cll/android/EventHandler;Lcom/microsoft/cll/android/ITicketCallback;)V
    .locals 2
    .param p1, "endpoint"    # Ljava/net/URL;
    .param p2, "event"    # Lcom/microsoft/cll/android/SerializedEvent;
    .param p4, "clientTelemetry"    # Lcom/microsoft/cll/android/ClientTelemetry;
    .param p6, "logger"    # Lcom/microsoft/cll/android/ILogger;
    .param p7, "executorService"    # Ljava/util/concurrent/ScheduledExecutorService;
    .param p8, "handler"    # Lcom/microsoft/cll/android/EventHandler;
    .param p9, "ticketCallback"    # Lcom/microsoft/cll/android/ITicketCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/URL;",
            "Lcom/microsoft/cll/android/SerializedEvent;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/microsoft/cll/android/ClientTelemetry;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/cll/android/ICllEvents;",
            ">;",
            "Lcom/microsoft/cll/android/ILogger;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Lcom/microsoft/cll/android/EventHandler;",
            "Lcom/microsoft/cll/android/ITicketCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .local p3, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p5, "cllEvents":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/cll/android/ICllEvents;>;"
    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-string v0, "EventQueueWriter"

    iput-object v0, p0, Lcom/microsoft/cll/android/EventQueueWriter;->TAG:Ljava/lang/String;

    .line 66
    iput-object p5, p0, Lcom/microsoft/cll/android/EventQueueWriter;->cllEvents:Ljava/util/List;

    .line 67
    iput-object p2, p0, Lcom/microsoft/cll/android/EventQueueWriter;->event:Lcom/microsoft/cll/android/SerializedEvent;

    .line 68
    iput-object p3, p0, Lcom/microsoft/cll/android/EventQueueWriter;->ids:Ljava/util/List;

    .line 69
    iput-object p6, p0, Lcom/microsoft/cll/android/EventQueueWriter;->logger:Lcom/microsoft/cll/android/ILogger;

    .line 70
    iput-object p9, p0, Lcom/microsoft/cll/android/EventQueueWriter;->ticketCallback:Lcom/microsoft/cll/android/ITicketCallback;

    .line 71
    new-instance v0, Lcom/microsoft/cll/android/EventSender;

    invoke-direct {v0, p1, p4, p6}, Lcom/microsoft/cll/android/EventSender;-><init>(Ljava/net/URL;Lcom/microsoft/cll/android/ClientTelemetry;Lcom/microsoft/cll/android/ILogger;)V

    iput-object v0, p0, Lcom/microsoft/cll/android/EventQueueWriter;->sender:Lcom/microsoft/cll/android/EventSender;

    .line 72
    iput-object v1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->batcher:Lcom/microsoft/cll/android/EventBatcher;

    .line 73
    iput-object v1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->storages:Ljava/util/List;

    .line 74
    iput-object p7, p0, Lcom/microsoft/cll/android/EventQueueWriter;->executorService:Ljava/util/concurrent/ScheduledExecutorService;

    .line 75
    iput-object p4, p0, Lcom/microsoft/cll/android/EventQueueWriter;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    .line 76
    iput-object p8, p0, Lcom/microsoft/cll/android/EventQueueWriter;->handler:Lcom/microsoft/cll/android/EventHandler;

    .line 77
    iput-object p1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->endpoint:Ljava/net/URL;

    .line 78
    new-instance v0, Lcom/microsoft/cll/android/TicketManager;

    invoke-direct {v0, p9, p6}, Lcom/microsoft/cll/android/TicketManager;-><init>(Lcom/microsoft/cll/android/ITicketCallback;Lcom/microsoft/cll/android/ILogger;)V

    iput-object v0, p0, Lcom/microsoft/cll/android/EventQueueWriter;->ticketManager:Lcom/microsoft/cll/android/TicketManager;

    .line 80
    invoke-virtual {p4}, Lcom/microsoft/cll/android/ClientTelemetry;->IncrementEventsQueuedForUpload()V

    .line 81
    return-void
.end method

.method public constructor <init>(Ljava/net/URL;Ljava/util/List;Lcom/microsoft/cll/android/ClientTelemetry;Ljava/util/List;Lcom/microsoft/cll/android/ILogger;Ljava/util/concurrent/ScheduledExecutorService;Lcom/microsoft/cll/android/ITicketCallback;)V
    .locals 2
    .param p1, "endpoint"    # Ljava/net/URL;
    .param p3, "clientTelemetry"    # Lcom/microsoft/cll/android/ClientTelemetry;
    .param p5, "logger"    # Lcom/microsoft/cll/android/ILogger;
    .param p6, "executorService"    # Ljava/util/concurrent/ScheduledExecutorService;
    .param p7, "ticketCallback"    # Lcom/microsoft/cll/android/ITicketCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/URL;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/cll/android/IStorage;",
            ">;",
            "Lcom/microsoft/cll/android/ClientTelemetry;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/cll/android/ICllEvents;",
            ">;",
            "Lcom/microsoft/cll/android/ILogger;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "Lcom/microsoft/cll/android/ITicketCallback;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "storages":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/cll/android/IStorage;>;"
    .local p4, "cllEvents":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/cll/android/ICllEvents;>;"
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-string v0, "EventQueueWriter"

    iput-object v0, p0, Lcom/microsoft/cll/android/EventQueueWriter;->TAG:Ljava/lang/String;

    .line 44
    iput-object p4, p0, Lcom/microsoft/cll/android/EventQueueWriter;->cllEvents:Ljava/util/List;

    .line 45
    iput-object p2, p0, Lcom/microsoft/cll/android/EventQueueWriter;->storages:Ljava/util/List;

    .line 46
    iput-object p5, p0, Lcom/microsoft/cll/android/EventQueueWriter;->logger:Lcom/microsoft/cll/android/ILogger;

    .line 47
    iput-object p7, p0, Lcom/microsoft/cll/android/EventQueueWriter;->ticketCallback:Lcom/microsoft/cll/android/ITicketCallback;

    .line 48
    new-instance v0, Lcom/microsoft/cll/android/EventBatcher;

    invoke-direct {v0}, Lcom/microsoft/cll/android/EventBatcher;-><init>()V

    iput-object v0, p0, Lcom/microsoft/cll/android/EventQueueWriter;->batcher:Lcom/microsoft/cll/android/EventBatcher;

    .line 49
    new-instance v0, Lcom/microsoft/cll/android/EventSender;

    invoke-direct {v0, p1, p3, p5}, Lcom/microsoft/cll/android/EventSender;-><init>(Ljava/net/URL;Lcom/microsoft/cll/android/ClientTelemetry;Lcom/microsoft/cll/android/ILogger;)V

    iput-object v0, p0, Lcom/microsoft/cll/android/EventQueueWriter;->sender:Lcom/microsoft/cll/android/EventSender;

    .line 50
    new-instance v0, Lcom/microsoft/cll/android/EventCompressor;

    invoke-direct {v0, p5}, Lcom/microsoft/cll/android/EventCompressor;-><init>(Lcom/microsoft/cll/android/ILogger;)V

    iput-object v0, p0, Lcom/microsoft/cll/android/EventQueueWriter;->compressor:Lcom/microsoft/cll/android/EventCompressor;

    .line 51
    iput-object v1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->event:Lcom/microsoft/cll/android/SerializedEvent;

    .line 52
    iput-object v1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->ids:Ljava/util/List;

    .line 53
    iput-object p6, p0, Lcom/microsoft/cll/android/EventQueueWriter;->executorService:Ljava/util/concurrent/ScheduledExecutorService;

    .line 54
    iput-object p3, p0, Lcom/microsoft/cll/android/EventQueueWriter;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    .line 55
    iput-object p1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->endpoint:Ljava/net/URL;

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/cll/android/EventQueueWriter;->removedStorages:Ljava/util/List;

    .line 57
    new-instance v0, Lcom/microsoft/cll/android/TicketManager;

    invoke-direct {v0, p7, p5}, Lcom/microsoft/cll/android/TicketManager;-><init>(Lcom/microsoft/cll/android/ITicketCallback;Lcom/microsoft/cll/android/ILogger;)V

    iput-object v0, p0, Lcom/microsoft/cll/android/EventQueueWriter;->ticketManager:Lcom/microsoft/cll/android/TicketManager;

    .line 58
    return-void
.end method

.method private cancelBackoff()V
    .locals 1

    .prologue
    .line 237
    const/4 v0, 0x0

    sput-object v0, Lcom/microsoft/cll/android/EventQueueWriter;->future:Ljava/util/concurrent/ScheduledFuture;

    .line 238
    const/4 v0, 0x1

    sput v0, Lcom/microsoft/cll/android/EventQueueWriter;->power:I

    .line 239
    return-void
.end method

.method private sendBatch(Ljava/lang/String;Lcom/microsoft/cll/android/IStorage;)Lcom/microsoft/cll/android/EventQueueWriter$SendResult;
    .locals 12
    .param p1, "batchedEvents"    # Ljava/lang/String;
    .param p2, "storage"    # Lcom/microsoft/cll/android/IStorage;

    .prologue
    const/16 v4, 0x191

    .line 242
    iget-object v1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v2, "EventQueueWriter"

    const-string v3, "Sending Batch of events"

    invoke-interface {v1, v2, v3}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 245
    iget-object v1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->removedStorages:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 246
    sget-object v1, Lcom/microsoft/cll/android/EventQueueWriter$SendResult;->SUCCESS:Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

    .line 299
    :goto_0
    return-object v1

    .line 249
    :cond_0
    iget-object v1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v2, "EventQueueWriter"

    const-string v3, "Compressing events"

    invoke-interface {v1, v2, v3}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    iget-object v1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->compressor:Lcom/microsoft/cll/android/EventCompressor;

    invoke-virtual {v1, p1}, Lcom/microsoft/cll/android/EventCompressor;->compress(Ljava/lang/String;)[B

    move-result-object v8

    .line 252
    .local v8, "compressedBatchedEvents":[B
    iget-object v1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->ticketManager:Lcom/microsoft/cll/android/TicketManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/microsoft/cll/android/TicketManager;->getHeaders(Z)Lcom/microsoft/cll/android/TicketHeaders;

    move-result-object v11

    .line 256
    .local v11, "ticketHeaders":Lcom/microsoft/cll/android/TicketHeaders;
    if-eqz v8, :cond_1

    .line 257
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->sender:Lcom/microsoft/cll/android/EventSender;

    const/4 v2, 0x1

    invoke-virtual {v1, v8, v2, v11}, Lcom/microsoft/cll/android/EventSender;->sendEvent([BZLcom/microsoft/cll/android/TicketHeaders;)I

    move-result v1

    if-ne v1, v4, :cond_2

    .line 261
    iget-object v1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v2, "EventQueueWriter"

    const-string v3, "We got a 401 while sending the events, refreshing the tokens and trying again"

    invoke-interface {v1, v2, v3}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    iget-object v1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->ticketManager:Lcom/microsoft/cll/android/TicketManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/cll/android/TicketManager;->getHeaders(Z)Lcom/microsoft/cll/android/TicketHeaders;

    move-result-object v11

    .line 264
    iget-object v1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->sender:Lcom/microsoft/cll/android/EventSender;

    const/4 v2, 0x1

    invoke-virtual {v1, v8, v2, v11}, Lcom/microsoft/cll/android/EventSender;->sendEvent([BZLcom/microsoft/cll/android/TicketHeaders;)I

    move-result v1

    if-ne v1, v4, :cond_2

    .line 265
    iget-object v1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v2, "EventQueueWriter"

    const-string v3, "After refreshing the tokens we still got a 401. Most likely we couldn\'t get new tokens so we will keep these events on disk and try to get new tokens later"

    invoke-interface {v1, v2, v3}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    sget-object v1, Lcom/microsoft/cll/android/EventQueueWriter$SendResult;->BAD_TOKEN:Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

    goto :goto_0

    .line 271
    :cond_1
    iget-object v1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->sender:Lcom/microsoft/cll/android/EventSender;

    invoke-virtual {v1, p1, v11}, Lcom/microsoft/cll/android/EventSender;->sendEvent(Ljava/lang/String;Lcom/microsoft/cll/android/TicketHeaders;)I

    move-result v1

    if-ne v1, v4, :cond_2

    .line 275
    iget-object v1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v2, "EventQueueWriter"

    const-string v3, "We got a 401 while sending the events, refreshing the tokens and trying again"

    invoke-interface {v1, v2, v3}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    iget-object v1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->ticketManager:Lcom/microsoft/cll/android/TicketManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/cll/android/TicketManager;->getHeaders(Z)Lcom/microsoft/cll/android/TicketHeaders;

    move-result-object v11

    .line 278
    iget-object v1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->sender:Lcom/microsoft/cll/android/EventSender;

    invoke-virtual {v1, p1, v11}, Lcom/microsoft/cll/android/EventSender;->sendEvent(Ljava/lang/String;Lcom/microsoft/cll/android/TicketHeaders;)I

    move-result v1

    if-ne v1, v4, :cond_2

    .line 279
    iget-object v1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v2, "EventQueueWriter"

    const-string v3, "After refreshing the tokens we still got a 401. Most likely we couldn\'t get new tokens so we will keep these events on disk and try to get new tokens later"

    invoke-interface {v1, v2, v3}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    sget-object v1, Lcom/microsoft/cll/android/EventQueueWriter$SendResult;->BAD_TOKEN:Lcom/microsoft/cll/android/EventQueueWriter$SendResult;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 285
    :catch_0
    move-exception v9

    .line 286
    .local v9, "e":Ljava/io/IOException;
    iget-object v1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v2, "EventQueueWriter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cannot send event: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    invoke-virtual {p0}, Lcom/microsoft/cll/android/EventQueueWriter;->generateBackoffInterval()I

    move-result v10

    .line 291
    .local v10, "interval":I
    iget-object v1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->storages:Ljava/util/List;

    iget-object v2, p0, Lcom/microsoft/cll/android/EventQueueWriter;->removedStorages:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 293
    new-instance v0, Lcom/microsoft/cll/android/EventQueueWriter;

    iget-object v1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->endpoint:Ljava/net/URL;

    iget-object v2, p0, Lcom/microsoft/cll/android/EventQueueWriter;->storages:Ljava/util/List;

    iget-object v3, p0, Lcom/microsoft/cll/android/EventQueueWriter;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    iget-object v4, p0, Lcom/microsoft/cll/android/EventQueueWriter;->cllEvents:Ljava/util/List;

    iget-object v5, p0, Lcom/microsoft/cll/android/EventQueueWriter;->logger:Lcom/microsoft/cll/android/ILogger;

    iget-object v6, p0, Lcom/microsoft/cll/android/EventQueueWriter;->executorService:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v7, p0, Lcom/microsoft/cll/android/EventQueueWriter;->ticketCallback:Lcom/microsoft/cll/android/ITicketCallback;

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/cll/android/EventQueueWriter;-><init>(Ljava/net/URL;Ljava/util/List;Lcom/microsoft/cll/android/ClientTelemetry;Ljava/util/List;Lcom/microsoft/cll/android/ILogger;Ljava/util/concurrent/ScheduledExecutorService;Lcom/microsoft/cll/android/ITicketCallback;)V

    .line 294
    .local v0, "eventQueueWriter":Lcom/microsoft/cll/android/EventQueueWriter;
    iget-object v1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->sender:Lcom/microsoft/cll/android/EventSender;

    invoke-virtual {v0, v1}, Lcom/microsoft/cll/android/EventQueueWriter;->setSender(Lcom/microsoft/cll/android/EventSender;)V

    .line 295
    iget-object v1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->executorService:Ljava/util/concurrent/ScheduledExecutorService;

    int-to-long v2, v10

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v0, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v1

    sput-object v1, Lcom/microsoft/cll/android/EventQueueWriter;->future:Ljava/util/concurrent/ScheduledFuture;

    .line 296
    sget-object v1, Lcom/microsoft/cll/android/EventQueueWriter$SendResult;->ERROR:Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

    goto/16 :goto_0

    .line 299
    .end local v0    # "eventQueueWriter":Lcom/microsoft/cll/android/EventQueueWriter;
    .end local v9    # "e":Ljava/io/IOException;
    .end local v10    # "interval":I
    :cond_2
    sget-object v1, Lcom/microsoft/cll/android/EventQueueWriter$SendResult;->SUCCESS:Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

    goto/16 :goto_0
.end method


# virtual methods
.method generateBackoffInterval()I
    .locals 8

    .prologue
    .line 310
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    .line 311
    .local v1, "random":Ljava/util/Random;
    sget-object v2, Lcom/microsoft/cll/android/SettingsStore$Settings;->CONSTANTFORRETRYPERIOD:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static {v2}, Lcom/microsoft/cll/android/SettingsStore;->getCllSettingsAsInt(Lcom/microsoft/cll/android/SettingsStore$Settings;)I

    move-result v2

    int-to-double v2, v2

    sget-object v4, Lcom/microsoft/cll/android/SettingsStore$Settings;->BASERETRYPERIOD:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static {v4}, Lcom/microsoft/cll/android/SettingsStore;->getCllSettingsAsInt(Lcom/microsoft/cll/android/SettingsStore$Settings;)I

    move-result v4

    int-to-double v4, v4

    sget v6, Lcom/microsoft/cll/android/EventQueueWriter;->power:I

    invoke-virtual {v1, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v6

    int-to-double v6, v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-int v0, v2

    .line 315
    .local v0, "interval":I
    sget-object v2, Lcom/microsoft/cll/android/SettingsStore$Settings;->CONSTANTFORRETRYPERIOD:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static {v2}, Lcom/microsoft/cll/android/SettingsStore;->getCllSettingsAsInt(Lcom/microsoft/cll/android/SettingsStore$Settings;)I

    move-result v2

    int-to-double v2, v2

    sget-object v4, Lcom/microsoft/cll/android/SettingsStore$Settings;->BASERETRYPERIOD:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static {v4}, Lcom/microsoft/cll/android/SettingsStore;->getCllSettingsAsInt(Lcom/microsoft/cll/android/SettingsStore$Settings;)I

    move-result v4

    int-to-double v4, v4

    sget v6, Lcom/microsoft/cll/android/EventQueueWriter;->power:I

    int-to-double v6, v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    mul-double/2addr v2, v4

    sget-object v4, Lcom/microsoft/cll/android/SettingsStore$Settings;->MAXRETRYPERIOD:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static {v4}, Lcom/microsoft/cll/android/SettingsStore;->getCllSettingsAsInt(Lcom/microsoft/cll/android/SettingsStore$Settings;)I

    move-result v4

    int-to-double v4, v4

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_0

    .line 317
    sget v2, Lcom/microsoft/cll/android/EventQueueWriter;->power:I

    add-int/lit8 v2, v2, 0x1

    sput v2, Lcom/microsoft/cll/android/EventQueueWriter;->power:I

    .line 320
    :cond_0
    return v0
.end method

.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 89
    iget-object v0, p0, Lcom/microsoft/cll/android/EventQueueWriter;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v1, "EventQueueWriter"

    const-string v2, "Starting upload"

    invoke-interface {v0, v1, v2}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/microsoft/cll/android/EventQueueWriter;->storages:Ljava/util/List;

    if-nez v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/microsoft/cll/android/EventQueueWriter;->event:Lcom/microsoft/cll/android/SerializedEvent;

    invoke-virtual {p0, v0}, Lcom/microsoft/cll/android/EventQueueWriter;->sendRealTimeEvent(Lcom/microsoft/cll/android/SerializedEvent;)V

    .line 108
    :goto_0
    return-void

    .line 100
    :cond_0
    sget-object v0, Lcom/microsoft/cll/android/EventQueueWriter;->running:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v3, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 101
    iget-object v0, p0, Lcom/microsoft/cll/android/EventQueueWriter;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v1, "EventQueueWriter"

    const-string v2, "Skipping send, event sending is already in progress on different thread."

    invoke-interface {v0, v1, v2}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 105
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/cll/android/EventQueueWriter;->send()V

    .line 106
    sget-object v0, Lcom/microsoft/cll/android/EventQueueWriter;->running:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0
.end method

.method protected send()V
    .locals 14

    .prologue
    .line 157
    iget-object v10, p0, Lcom/microsoft/cll/android/EventQueueWriter;->storages:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/cll/android/IStorage;

    .line 158
    .local v9, "storage":Lcom/microsoft/cll/android/IStorage;
    iget-object v10, p0, Lcom/microsoft/cll/android/EventQueueWriter;->executorService:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v10}, Ljava/util/concurrent/ScheduledExecutorService;->isShutdown()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 234
    .end local v9    # "storage":Lcom/microsoft/cll/android/IStorage;
    :cond_0
    :goto_1
    return-void

    .line 163
    .restart local v9    # "storage":Lcom/microsoft/cll/android/IStorage;
    :cond_1
    iget-object v10, p0, Lcom/microsoft/cll/android/EventQueueWriter;->ticketManager:Lcom/microsoft/cll/android/TicketManager;

    invoke-virtual {v10}, Lcom/microsoft/cll/android/TicketManager;->clean()V

    .line 165
    invoke-interface {v9}, Lcom/microsoft/cll/android/IStorage;->drain()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/cll/android/Tuple;

    .line 167
    .local v4, "event":Lcom/microsoft/cll/android/Tuple;, "Lcom/microsoft/cll/android/Tuple<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    iget-object v11, p0, Lcom/microsoft/cll/android/EventQueueWriter;->ticketManager:Lcom/microsoft/cll/android/TicketManager;

    iget-object v10, v4, Lcom/microsoft/cll/android/Tuple;->b:Ljava/lang/Object;

    check-cast v10, Ljava/util/List;

    invoke-virtual {v11, v10}, Lcom/microsoft/cll/android/TicketManager;->addTickets(Ljava/util/List;)V

    .line 169
    iget-object v10, p0, Lcom/microsoft/cll/android/EventQueueWriter;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    invoke-virtual {v10}, Lcom/microsoft/cll/android/ClientTelemetry;->IncrementEventsQueuedForUpload()V

    .line 172
    iget-object v10, v4, Lcom/microsoft/cll/android/Tuple;->a:Ljava/lang/Object;

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    sget-object v11, Lcom/microsoft/cll/android/SettingsStore$Settings;->MAXEVENTSIZEINBYTES:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static {v11}, Lcom/microsoft/cll/android/SettingsStore;->getCllSettingsAsInt(Lcom/microsoft/cll/android/SettingsStore$Settings;)I

    move-result v11

    if-le v10, v11, :cond_3

    .line 173
    iget-object v10, p0, Lcom/microsoft/cll/android/EventQueueWriter;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v11, "EventQueueWriter"

    const-string v12, "Dropping event because it is too large."

    invoke-interface {v10, v11, v12}, Lcom/microsoft/cll/android/ILogger;->warn(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    iget-object v10, p0, Lcom/microsoft/cll/android/EventQueueWriter;->cllEvents:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/cll/android/ICllEvents;

    .line 177
    .local v1, "cllEvent":Lcom/microsoft/cll/android/ICllEvents;
    iget-object v10, v4, Lcom/microsoft/cll/android/Tuple;->a:Ljava/lang/Object;

    check-cast v10, Ljava/lang/String;

    invoke-interface {v1, v10}, Lcom/microsoft/cll/android/ICllEvents;->eventDropped(Ljava/lang/String;)V

    goto :goto_3

    .line 183
    .end local v1    # "cllEvent":Lcom/microsoft/cll/android/ICllEvents;
    .end local v7    # "i$":Ljava/util/Iterator;
    :cond_3
    iget-object v11, p0, Lcom/microsoft/cll/android/EventQueueWriter;->batcher:Lcom/microsoft/cll/android/EventBatcher;

    iget-object v10, v4, Lcom/microsoft/cll/android/Tuple;->a:Ljava/lang/Object;

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v11, v10}, Lcom/microsoft/cll/android/EventBatcher;->canAddToBatch(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 185
    :try_start_0
    iget-object v11, p0, Lcom/microsoft/cll/android/EventQueueWriter;->batcher:Lcom/microsoft/cll/android/EventBatcher;

    iget-object v10, v4, Lcom/microsoft/cll/android/Tuple;->a:Ljava/lang/Object;

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v11, v10}, Lcom/microsoft/cll/android/EventBatcher;->addEventToBatch(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/microsoft/cll/android/EventBatcher$BatchFullException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 186
    :catch_0
    move-exception v2

    .line 187
    .local v2, "e":Lcom/microsoft/cll/android/EventBatcher$BatchFullException;
    iget-object v10, p0, Lcom/microsoft/cll/android/EventQueueWriter;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v11, "EventQueueWriter"

    const-string v12, "Could not add to batch"

    invoke-interface {v10, v11, v12}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 191
    .end local v2    # "e":Lcom/microsoft/cll/android/EventBatcher$BatchFullException;
    :cond_4
    iget-object v10, p0, Lcom/microsoft/cll/android/EventQueueWriter;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v11, "EventQueueWriter"

    const-string v12, "Got a full batch, preparing to send"

    invoke-interface {v10, v11, v12}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    iget-object v10, p0, Lcom/microsoft/cll/android/EventQueueWriter;->batcher:Lcom/microsoft/cll/android/EventBatcher;

    invoke-virtual {v10}, Lcom/microsoft/cll/android/EventBatcher;->getBatchedEvents()Ljava/lang/String;

    move-result-object v0

    .line 195
    .local v0, "batchedEvents":Ljava/lang/String;
    :try_start_1
    iget-object v11, p0, Lcom/microsoft/cll/android/EventQueueWriter;->batcher:Lcom/microsoft/cll/android/EventBatcher;

    iget-object v10, v4, Lcom/microsoft/cll/android/Tuple;->a:Ljava/lang/Object;

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v11, v10}, Lcom/microsoft/cll/android/EventBatcher;->addEventToBatch(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/microsoft/cll/android/EventBatcher$BatchFullException; {:try_start_1 .. :try_end_1} :catch_1

    .line 200
    :goto_4
    invoke-direct {p0, v0, v9}, Lcom/microsoft/cll/android/EventQueueWriter;->sendBatch(Ljava/lang/String;Lcom/microsoft/cll/android/IStorage;)Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

    move-result-object v8

    .line 201
    .local v8, "sendResult":Lcom/microsoft/cll/android/EventQueueWriter$SendResult;
    sget-object v10, Lcom/microsoft/cll/android/EventQueueWriter$SendResult;->ERROR:Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

    if-ne v8, v10, :cond_5

    .line 203
    invoke-interface {v9}, Lcom/microsoft/cll/android/IStorage;->close()V

    goto/16 :goto_1

    .line 196
    .end local v8    # "sendResult":Lcom/microsoft/cll/android/EventQueueWriter$SendResult;
    :catch_1
    move-exception v2

    .line 197
    .restart local v2    # "e":Lcom/microsoft/cll/android/EventBatcher$BatchFullException;
    iget-object v10, p0, Lcom/microsoft/cll/android/EventQueueWriter;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v11, "EventQueueWriter"

    const-string v12, "Could not add to batch"

    invoke-interface {v10, v11, v12}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 208
    .end local v2    # "e":Lcom/microsoft/cll/android/EventBatcher$BatchFullException;
    .restart local v8    # "sendResult":Lcom/microsoft/cll/android/EventQueueWriter$SendResult;
    :cond_5
    invoke-direct {p0}, Lcom/microsoft/cll/android/EventQueueWriter;->cancelBackoff()V

    goto/16 :goto_2

    .line 213
    .end local v0    # "batchedEvents":Ljava/lang/String;
    .end local v4    # "event":Lcom/microsoft/cll/android/Tuple;, "Lcom/microsoft/cll/android/Tuple<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v8    # "sendResult":Lcom/microsoft/cll/android/EventQueueWriter$SendResult;
    :cond_6
    iget-object v10, p0, Lcom/microsoft/cll/android/EventQueueWriter;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v11, "EventQueueWriter"

    const-string v12, "Preparing to send"

    invoke-interface {v10, v11, v12}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    iget-object v10, p0, Lcom/microsoft/cll/android/EventQueueWriter;->batcher:Lcom/microsoft/cll/android/EventBatcher;

    invoke-virtual {v10}, Lcom/microsoft/cll/android/EventBatcher;->getBatchedEvents()Ljava/lang/String;

    move-result-object v0

    .line 215
    .restart local v0    # "batchedEvents":Ljava/lang/String;
    invoke-direct {p0, v0, v9}, Lcom/microsoft/cll/android/EventQueueWriter;->sendBatch(Ljava/lang/String;Lcom/microsoft/cll/android/IStorage;)Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

    move-result-object v8

    .line 216
    .restart local v8    # "sendResult":Lcom/microsoft/cll/android/EventQueueWriter$SendResult;
    invoke-interface {v9}, Lcom/microsoft/cll/android/IStorage;->close()V

    .line 217
    sget-object v10, Lcom/microsoft/cll/android/EventQueueWriter$SendResult;->ERROR:Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

    if-eq v8, v10, :cond_0

    .line 219
    sget-object v10, Lcom/microsoft/cll/android/EventQueueWriter$SendResult;->SUCCESS:Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

    if-ne v8, v10, :cond_7

    .line 222
    invoke-interface {v9}, Lcom/microsoft/cll/android/IStorage;->discard()V

    .line 226
    :cond_7
    invoke-direct {p0}, Lcom/microsoft/cll/android/EventQueueWriter;->cancelBackoff()V

    goto/16 :goto_0

    .line 229
    .end local v0    # "batchedEvents":Ljava/lang/String;
    .end local v8    # "sendResult":Lcom/microsoft/cll/android/EventQueueWriter$SendResult;
    .end local v9    # "storage":Lcom/microsoft/cll/android/IStorage;
    :cond_8
    iget-object v10, p0, Lcom/microsoft/cll/android/EventQueueWriter;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v11, "EventQueueWriter"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Sent "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/microsoft/cll/android/EventQueueWriter;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    iget-object v13, v13, Lcom/microsoft/cll/android/ClientTelemetry;->snapshot:LMs/Telemetry/CllHeartBeat;

    invoke-virtual {v13}, LMs/Telemetry/CllHeartBeat;->getEventsQueued()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " events."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v10, v11, v12}, Lcom/microsoft/cll/android/ILogger;->info(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    iget-object v10, p0, Lcom/microsoft/cll/android/EventQueueWriter;->cllEvents:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/cll/android/ICllEvents;

    .line 232
    .local v3, "event":Lcom/microsoft/cll/android/ICllEvents;
    invoke-interface {v3}, Lcom/microsoft/cll/android/ICllEvents;->sendComplete()V

    goto :goto_5
.end method

.method protected sendRealTimeEvent(Lcom/microsoft/cll/android/SerializedEvent;)V
    .locals 7
    .param p1, "singleEvent"    # Lcom/microsoft/cll/android/SerializedEvent;

    .prologue
    const/16 v6, 0x191

    .line 115
    invoke-virtual {p1}, Lcom/microsoft/cll/android/SerializedEvent;->getSerializedData()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    sget-object v5, Lcom/microsoft/cll/android/SettingsStore$Settings;->MAXEVENTSIZEINBYTES:Lcom/microsoft/cll/android/SettingsStore$Settings;

    invoke-static {v5}, Lcom/microsoft/cll/android/SettingsStore;->getCllSettingsAsInt(Lcom/microsoft/cll/android/SettingsStore$Settings;)I

    move-result v5

    if-le v4, v5, :cond_1

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/microsoft/cll/android/EventQueueWriter;->ticketManager:Lcom/microsoft/cll/android/TicketManager;

    invoke-virtual {v4}, Lcom/microsoft/cll/android/TicketManager;->clean()V

    .line 121
    iget-object v4, p0, Lcom/microsoft/cll/android/EventQueueWriter;->ticketManager:Lcom/microsoft/cll/android/TicketManager;

    iget-object v5, p0, Lcom/microsoft/cll/android/EventQueueWriter;->ids:Ljava/util/List;

    invoke-virtual {v4, v5}, Lcom/microsoft/cll/android/TicketManager;->addTickets(Ljava/util/List;)V

    .line 122
    iget-object v4, p0, Lcom/microsoft/cll/android/EventQueueWriter;->ticketManager:Lcom/microsoft/cll/android/TicketManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/microsoft/cll/android/TicketManager;->getHeaders(Z)Lcom/microsoft/cll/android/TicketHeaders;

    move-result-object v3

    .line 124
    .local v3, "ticketHeaders":Lcom/microsoft/cll/android/TicketHeaders;
    iget-object v4, p0, Lcom/microsoft/cll/android/EventQueueWriter;->sender:Lcom/microsoft/cll/android/EventSender;

    invoke-virtual {p1}, Lcom/microsoft/cll/android/SerializedEvent;->getSerializedData()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Lcom/microsoft/cll/android/EventSender;->sendEvent(Ljava/lang/String;Lcom/microsoft/cll/android/TicketHeaders;)I

    move-result v4

    if-ne v4, v6, :cond_2

    .line 128
    iget-object v4, p0, Lcom/microsoft/cll/android/EventQueueWriter;->ticketManager:Lcom/microsoft/cll/android/TicketManager;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/microsoft/cll/android/TicketManager;->getHeaders(Z)Lcom/microsoft/cll/android/TicketHeaders;

    move-result-object v3

    .line 129
    iget-object v4, p0, Lcom/microsoft/cll/android/EventQueueWriter;->sender:Lcom/microsoft/cll/android/EventSender;

    invoke-virtual {p1}, Lcom/microsoft/cll/android/SerializedEvent;->getSerializedData()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Lcom/microsoft/cll/android/EventSender;->sendEvent(Ljava/lang/String;Lcom/microsoft/cll/android/TicketHeaders;)I

    move-result v4

    if-ne v4, v6, :cond_2

    .line 131
    iget-object v4, p0, Lcom/microsoft/cll/android/EventQueueWriter;->handler:Lcom/microsoft/cll/android/EventHandler;

    iget-object v5, p0, Lcom/microsoft/cll/android/EventQueueWriter;->ids:Ljava/util/List;

    invoke-virtual {v4, p1, v5}, Lcom/microsoft/cll/android/EventHandler;->addToStorage(Lcom/microsoft/cll/android/SerializedEvent;Ljava/util/List;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 135
    .end local v3    # "ticketHeaders":Lcom/microsoft/cll/android/TicketHeaders;
    :catch_0
    move-exception v0

    .line 138
    .local v0, "e":Ljava/io/IOException;
    iget-object v4, p0, Lcom/microsoft/cll/android/EventQueueWriter;->logger:Lcom/microsoft/cll/android/ILogger;

    const-string v5, "EventQueueWriter"

    const-string v6, "Cannot send event"

    invoke-interface {v4, v5, v6}, Lcom/microsoft/cll/android/ILogger;->error(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    iget-object v4, p0, Lcom/microsoft/cll/android/EventQueueWriter;->handler:Lcom/microsoft/cll/android/EventHandler;

    iget-object v5, p0, Lcom/microsoft/cll/android/EventQueueWriter;->ids:Ljava/util/List;

    invoke-virtual {v4, p1, v5}, Lcom/microsoft/cll/android/EventHandler;->addToStorage(Lcom/microsoft/cll/android/SerializedEvent;Ljava/util/List;)Z

    goto :goto_0

    .line 144
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v3    # "ticketHeaders":Lcom/microsoft/cll/android/TicketHeaders;
    :cond_2
    invoke-direct {p0}, Lcom/microsoft/cll/android/EventQueueWriter;->cancelBackoff()V

    .line 146
    iget-object v4, p0, Lcom/microsoft/cll/android/EventQueueWriter;->cllEvents:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/cll/android/ICllEvents;

    .line 147
    .local v1, "event":Lcom/microsoft/cll/android/ICllEvents;
    invoke-interface {v1}, Lcom/microsoft/cll/android/ICllEvents;->sendComplete()V

    goto :goto_1
.end method

.method setSender(Lcom/microsoft/cll/android/EventSender;)V
    .locals 0
    .param p1, "sender"    # Lcom/microsoft/cll/android/EventSender;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/microsoft/cll/android/EventQueueWriter;->sender:Lcom/microsoft/cll/android/EventSender;

    .line 85
    return-void
.end method
