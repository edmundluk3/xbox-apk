.class final synthetic Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$49;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$49;->arg$1:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;)Lio/reactivex/functions/Function;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$49;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$49;-><init>(Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/PartyInteractor$$Lambda$49;->arg$1:Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryRepository;

    check-cast p1, Ljava/util/Collection;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;->load(Ljava/util/Collection;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
