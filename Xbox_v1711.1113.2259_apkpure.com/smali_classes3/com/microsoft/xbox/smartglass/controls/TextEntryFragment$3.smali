.class Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$3;
.super Ljava/lang/Object;
.source "TextEntryFragment.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment$3;->this$0:Lcom/microsoft/xbox/smartglass/controls/TextEntryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "view"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 157
    const/4 v0, 0x0

    .line 158
    .local v0, "handled":Z
    invoke-virtual {p1}, Landroid/widget/TextView;->getInputType()I

    move-result v1

    const/high16 v2, 0x20000

    and-int/2addr v1, v2

    if-nez v1, :cond_0

    const/4 v1, 0x6

    if-ne p2, v1, :cond_0

    .line 159
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTextManager()Lcom/microsoft/xbox/smartglass/TextManager;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/smartglass/TextAction;->Accept:Lcom/microsoft/xbox/smartglass/TextAction;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/smartglass/TextManager;->complete(Lcom/microsoft/xbox/smartglass/TextAction;)V

    .line 160
    const/4 v0, 0x1

    .line 162
    :cond_0
    return v0
.end method
