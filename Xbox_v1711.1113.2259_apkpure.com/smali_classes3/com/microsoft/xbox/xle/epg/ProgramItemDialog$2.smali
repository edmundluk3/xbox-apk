.class Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$2;
.super Ljava/lang/Object;
.source "ProgramItemDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/service/model/epg/EPGChannel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;

.field final synthetic val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

.field final synthetic val$model:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

.field final synthetic val$program:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;Lcom/microsoft/xbox/service/model/epg/EPGChannel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$2;->this$0:Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$2;->val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    iput-object p3, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$2;->val$program:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    iput-object p4, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$2;->val$model:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$2;->this$0:Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->access$000(Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;)V

    .line 122
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    const-string v2, "ShowDetails"

    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$2;->val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getHeadendID()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$2;->val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelGuid()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$2;->val$program:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$2;->val$program:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    .line 123
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowGuid()Ljava/lang/String;

    move-result-object v0

    .line 122
    :goto_0
    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackOneGuideEpgPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$2;->val$model:Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$2;->val$program:Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;->showDetailsView(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)V

    .line 126
    return-void

    .line 123
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
