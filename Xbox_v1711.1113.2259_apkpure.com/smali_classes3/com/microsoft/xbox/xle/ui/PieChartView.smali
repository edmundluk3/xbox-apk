.class public Lcom/microsoft/xbox/xle/ui/PieChartView;
.super Landroid/view/View;
.source "PieChartView.java"


# instance fields
.field private earnedPercentage:F

.field private externalRectf:Landroid/graphics/RectF;

.field private paint:Landroid/graphics/Paint;

.field private strokeWidth:F

.field private unearnedPercentage:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 25
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/PieChartView;->paint:Landroid/graphics/Paint;

    .line 21
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/PieChartView;->externalRectf:Landroid/graphics/RectF;

    .line 26
    sget-object v1, Lcom/microsoft/xboxone/smartglass/R$styleable;->PieChartView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 27
    .local v0, "a":Landroid/content/res/TypedArray;
    const/16 v1, 0xd

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/PieChartView;->strokeWidth:F

    .line 28
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 29
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/PieChartView;->paint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 30
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/PieChartView;->paint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/microsoft/xbox/xle/ui/PieChartView;->strokeWidth:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 31
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/PieChartView;->setBackgroundColor(I)V

    .line 32
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    const-wide v10, 0x3fe999999999999aL    # 0.8

    const-wide v8, 0x3fc999999999999aL    # 0.2

    .line 47
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/PieChartView;->externalRectf:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    int-to-float v1, v1

    float-to-double v6, v1

    mul-double/2addr v6, v8

    double-to-float v1, v6

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/PieChartView;->externalRectf:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    int-to-float v1, v1

    float-to-double v6, v1

    mul-double/2addr v6, v8

    double-to-float v1, v6

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/PieChartView;->externalRectf:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    int-to-float v1, v1

    float-to-double v6, v1

    mul-double/2addr v6, v10

    double-to-float v1, v6

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/PieChartView;->externalRectf:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    int-to-float v1, v1

    float-to-double v6, v1

    mul-double/2addr v6, v10

    double-to-float v1, v6

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/PieChartView;->paint:Landroid/graphics/Paint;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0c00ce

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 55
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/PieChartView;->externalRectf:Landroid/graphics/RectF;

    const/high16 v3, 0x43b40000    # 360.0f

    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/PieChartView;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/PieChartView;->paint:Landroid/graphics/Paint;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0c000c

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/PieChartView;->externalRectf:Landroid/graphics/RectF;

    iget v3, p0, Lcom/microsoft/xbox/xle/ui/PieChartView;->earnedPercentage:F

    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/PieChartView;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 59
    return-void
.end method

.method public setPercentage(I)V
    .locals 4
    .param p1, "percentage"    # I

    .prologue
    const-wide v2, 0x400ccccccccccccdL    # 3.6

    .line 35
    if-ltz p1, :cond_0

    const/16 v0, 0x64

    if-le p1, v0, :cond_1

    .line 36
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/PieChartView;->earnedPercentage:F

    .line 37
    const/high16 v0, 0x43b40000    # 360.0f

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/PieChartView;->unearnedPercentage:F

    .line 42
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/PieChartView;->invalidate()V

    .line 43
    return-void

    .line 39
    :cond_1
    int-to-double v0, p1

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/PieChartView;->earnedPercentage:F

    .line 40
    rsub-int/lit8 v0, p1, 0x64

    int-to-double v0, v0

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/PieChartView;->unearnedPercentage:F

    goto :goto_0
.end method
