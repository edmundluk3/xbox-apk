.class final Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$3;
.super Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;
.source "HomeScreenPopupScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->createRecentScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter",
        "<",
        "Lcom/microsoft/xbox/service/model/recents/Recent;",
        ">;"
    }
.end annotation


# instance fields
.field private currentRecent:Lcom/microsoft/xbox/service/model/recents/Recent;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;)V
    .locals 1

    .prologue
    .line 103
    .local p1, "viewModel":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase<Lcom/microsoft/xbox/service/model/recents/Recent;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$1;)V

    return-void
.end method


# virtual methods
.method protected createAndBindHeader(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData",
            "<",
            "Lcom/microsoft/xbox/service/model/recents/Recent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "data":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;, "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData<Lcom/microsoft/xbox/service/model/recents/Recent;>;"
    const/4 v9, 0x4

    .line 108
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;->getHeaderData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/model/recents/Recent;

    .line 110
    .local v3, "recent":Lcom/microsoft/xbox/service/model/recents/Recent;
    if-eqz v3, :cond_1

    iget-object v7, v3, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$3;->currentRecent:Lcom/microsoft/xbox/service/model/recents/Recent;

    if-eq v7, v3, :cond_1

    .line 111
    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$3;->currentRecent:Lcom/microsoft/xbox/service/model/recents/Recent;

    .line 113
    const v7, 0x7f0e06d8

    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$3;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 115
    .local v4, "root":Landroid/view/View;
    const v7, 0x7f0e06df

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 116
    .local v5, "title":Landroid/widget/TextView;
    iget-object v7, v3, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v7, v7, Lcom/microsoft/xbox/service/model/recents/RecentItem;->Title:Ljava/lang/String;

    invoke-static {v5, v7, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 118
    const v7, 0x7f0e06dc

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 120
    .local v6, "titleImage":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    iget-object v7, v3, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v7, v7, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ImageUrl:Ljava/lang/String;

    if-eqz v7, :cond_0

    iget-object v7, v3, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isMusicPlayList(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 121
    :cond_0
    invoke-static {v6}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil;->setUserBackground(Landroid/view/View;)V

    .line 122
    iget-object v7, v3, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isMusicPlayList(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v7

    if-eqz v7, :cond_2

    const v7, 0x7f070fca

    :goto_0
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setText(I)V

    .line 135
    :goto_1
    const v7, 0x7f0e06e0

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 136
    .local v0, "companion":Landroid/widget/TextView;
    invoke-virtual {v4}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;->getHasState()Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->access$400(Landroid/content/res/Resources;Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;)Ljava/lang/String;

    move-result-object v2

    .line 137
    .local v2, "hasString":Ljava/lang/String;
    invoke-static {v0, v2, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 139
    .end local v0    # "companion":Landroid/widget/TextView;
    .end local v2    # "hasString":Ljava/lang/String;
    .end local v4    # "root":Landroid/view/View;
    .end local v5    # "title":Landroid/widget/TextView;
    .end local v6    # "titleImage":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    :cond_1
    return-void

    .line 122
    .restart local v4    # "root":Landroid/view/View;
    .restart local v5    # "title":Landroid/widget/TextView;
    .restart local v6    # "titleImage":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    :cond_2
    const v7, 0x7f070fdb

    goto :goto_0

    .line 124
    :cond_3
    const v7, 0x7f0e001d

    new-instance v8, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$3$1;

    invoke-direct {v8, p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$3$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter$3;Lcom/microsoft/xbox/service/model/recents/Recent;)V

    invoke-virtual {v6, v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setTag(ILjava/lang/Object;)V

    .line 130
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;->getPlaceholderRid()I

    move-result v1

    .line 131
    .local v1, "defaultRid":I
    iget-object v7, v3, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v7, v7, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ImageUrl:Ljava/lang/String;

    invoke-virtual {v6, v7, v1, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 132
    invoke-static {v6}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->access$300(Landroid/widget/ImageView;)V

    goto :goto_1
.end method
