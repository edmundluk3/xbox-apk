.class Lcom/microsoft/xbox/smartglass/controls/Gyroscope$Listener;
.super Lcom/microsoft/xbox/smartglass/GyrometerListener;
.source "Gyroscope.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/smartglass/controls/Gyroscope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Listener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/smartglass/controls/Gyroscope;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/smartglass/controls/Gyroscope;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/smartglass/controls/Gyroscope;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/Gyroscope$Listener;->this$0:Lcom/microsoft/xbox/smartglass/controls/Gyroscope;

    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/GyrometerListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onReadingChanged(Lcom/microsoft/xbox/smartglass/Gyrometer;Lcom/microsoft/xbox/smartglass/GyrometerReading;)V
    .locals 8
    .param p1, "gyrometer"    # Lcom/microsoft/xbox/smartglass/Gyrometer;
    .param p2, "reading"    # Lcom/microsoft/xbox/smartglass/GyrometerReading;

    .prologue
    .line 57
    :try_start_0
    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/JsonGyroscope;

    iget-wide v2, p2, Lcom/microsoft/xbox/smartglass/GyrometerReading;->x:D

    iget-wide v4, p2, Lcom/microsoft/xbox/smartglass/GyrometerReading;->y:D

    iget-wide v6, p2, Lcom/microsoft/xbox/smartglass/GyrometerReading;->z:D

    invoke-direct/range {v1 .. v7}, Lcom/microsoft/xbox/smartglass/controls/JsonGyroscope;-><init>(DDD)V

    .line 58
    .local v1, "json":Lcom/microsoft/xbox/smartglass/controls/JsonGyroscope;
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Gyroscope$Listener;->this$0:Lcom/microsoft/xbox/smartglass/controls/Gyroscope;

    iget-object v0, v0, Lcom/microsoft/xbox/smartglass/controls/Gyroscope;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v2, "gyroscope"

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/controls/JsonGyroscope;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->raiseEvent(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    .end local v1    # "json":Lcom/microsoft/xbox/smartglass/controls/JsonGyroscope;
    :goto_0
    return-void

    .line 59
    :catch_0
    move-exception v0

    goto :goto_0
.end method
