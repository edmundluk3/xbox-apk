.class Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$3;
.super Ljava/lang/Object;
.source "SkypeConversationDetailsActivityAdapter.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    .prologue
    .line 683
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 6
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/4 v2, 0x0

    .line 687
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    if-lez v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isBlockingBusy()Z

    move-result v3

    if-nez v3, :cond_3

    .line 688
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isSkypeGroupConversation()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getIsRecipientNonEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    const/4 v2, 0x1

    :cond_1
    invoke-static {v3, v2}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->access$600(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;Z)V

    .line 689
    sget-boolean v2, Lcom/microsoft/xbox/toolkit/Build;->EnableSkypeLongPoll:Z

    if-eqz v2, :cond_2

    .line 690
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 691
    .local v0, "currentTimestamp":J
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->access$700(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)J

    move-result-wide v2

    sub-long v2, v0, v2

    const-wide/16 v4, 0x7d0

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    .line 692
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->sendSkypeIsTypingMessage()V

    .line 693
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-static {v2, v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->access$702(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;J)J

    .line 700
    .end local v0    # "currentTimestamp":J
    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->access$800(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)V

    .line 703
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->updateMessageBody()V

    .line 704
    return-void

    .line 698
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-static {v3, v2}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->access$600(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;Z)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 707
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 710
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 711
    .local v1, "messageText":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v0, 0x1

    .line 712
    .local v0, "enableButton":Z
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-static {v2, v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->access$600(Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;Z)V

    .line 713
    return-void

    .line 711
    .end local v0    # "enableButton":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
