.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
.super Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCJsonBase;
.source "UTCAdditionalInfoModel.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UTCLOGGING"

.field private static persistentAdditionalInfo:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private additionalInfo:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->persistentAdditionalInfo:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCJsonBase;-><init>()V

    .line 28
    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->additionalInfo:Ljava/util/concurrent/ConcurrentHashMap;

    .line 29
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getCurrentMarket()Ljava/lang/String;

    move-result-object v0

    .line 30
    .local v0, "mkt":Ljava/lang/String;
    const-string v1, "RegionSelected"

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->updatePersistentValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 31
    return-void
.end method

.method public static declared-synchronized addPersistentValue(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/Object;

    .prologue
    .line 39
    const-class v1, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    monitor-enter v1

    if-eqz p1, :cond_0

    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->getPersistentAdditionalInfo()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->isGlobalKeyName(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->getPersistentAdditionalInfo()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 42
    :cond_0
    monitor-exit v1

    return-void

    .line 39
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized clearPersistentValues()V
    .locals 2

    .prologue
    .line 62
    const-class v1, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->getPersistentAdditionalInfo()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    monitor-exit v1

    return-void

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static formatXuid(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "xuid"    # Ljava/lang/String;

    .prologue
    .line 118
    const-string v0, "x:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "x:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 121
    .end local p0    # "xuid":Ljava/lang/String;
    :cond_0
    return-object p0
.end method

.method private static declared-synchronized getPersistentAdditionalInfo()Ljava/util/concurrent/ConcurrentHashMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    const-class v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->persistentAdditionalInfo:Ljava/util/concurrent/ConcurrentHashMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static isGlobalKeyName(Ljava/lang/String;)Z
    .locals 11
    .param p0, "keyName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 125
    const/4 v3, 0x0

    .line 126
    .local v3, "result":Z
    const-class v5, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$GlobalNames$DeepLink;

    invoke-virtual {v5}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v6

    array-length v7, v6

    move v5, v4

    :goto_0
    if-ge v5, v7, :cond_0

    aget-object v1, v6, v5

    .line 127
    .local v1, "field":Ljava/lang/reflect/Field;
    invoke-virtual {v1}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v8

    const-class v9, Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 128
    const-string v2, ""

    .line 130
    .local v2, "fieldString":Ljava/lang/String;
    const/4 v8, 0x0

    :try_start_0
    invoke-virtual {v1, v8}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 134
    :goto_1
    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 135
    const/4 v3, 0x1

    .line 140
    .end local v1    # "field":Ljava/lang/reflect/Field;
    .end local v2    # "fieldString":Ljava/lang/String;
    :cond_0
    if-nez v3, :cond_2

    .line 142
    const/4 v5, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :cond_1
    move v4, v5

    :goto_2
    packed-switch v4, :pswitch_data_1

    .line 148
    :cond_2
    :goto_3
    return v3

    .line 131
    .restart local v1    # "field":Ljava/lang/reflect/Field;
    .restart local v2    # "fieldString":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 132
    .local v0, "ex":Ljava/lang/Exception;
    const-string v8, "UTCLOGGING"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unable to get Field from UTCNames.GlobalNames: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 126
    .end local v0    # "ex":Ljava/lang/Exception;
    .end local v2    # "fieldString":Ljava/lang/String;
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 142
    .end local v1    # "field":Ljava/lang/reflect/Field;
    :pswitch_0
    const-string v6, "PartyId"

    invoke-virtual {p0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    goto :goto_2

    .line 144
    :pswitch_1
    const/4 v3, 0x1

    goto :goto_3

    .line 142
    nop

    :pswitch_data_0
    .packed-switch 0x33f59a81
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method private mergeAdditionalInfoObjects()Ljava/util/concurrent/ConcurrentHashMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 153
    .local v0, "mergedAdditionalInfo":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->additionalInfo:Ljava/util/concurrent/ConcurrentHashMap;

    if-eqz v1, :cond_0

    .line 154
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->additionalInfo:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->putAll(Ljava/util/Map;)V

    .line 160
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->getPersistentAdditionalInfo()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->putAll(Ljava/util/Map;)V

    .line 161
    return-object v0
.end method

.method public static declared-synchronized removePersistentValue(Ljava/lang/String;)V
    .locals 2
    .param p0, "key"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 45
    const-class v1, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 46
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->getPersistentAdditionalInfo()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 47
    monitor-exit v1

    return-void

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public addValue(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 67
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->additionalInfo:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 68
    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->isGlobalKeyName(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->additionalInfo:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->updatePersistentValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public addValues(Ljava/util/Map;)V
    .locals 4
    .param p1    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 83
    .local p1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz p1, :cond_0

    .line 84
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 85
    .local v0, "key":Ljava/lang/String;
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 86
    .local v1, "obj":Ljava/lang/Object;
    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 89
    .end local v0    # "key":Ljava/lang/String;
    .end local v1    # "obj":Ljava/lang/Object;
    :cond_0
    return-void
.end method

.method public getAdditionalInfo()Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->additionalInfo:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method public setAdditionalInfo(Ljava/util/concurrent/ConcurrentHashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 96
    .local p1, "additionalInfo":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->additionalInfo:Ljava/util/concurrent/ConcurrentHashMap;

    .line 97
    return-void
.end method

.method public toJson()Ljava/lang/String;
    .locals 6

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->mergeAdditionalInfoObjects()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    .line 102
    .local v1, "objects":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v2, ""

    .line 104
    .local v2, "result":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/gson/JsonIOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 108
    :goto_0
    return-object v2

    .line 105
    :catch_0
    move-exception v0

    .line 106
    .local v0, "e":Lcom/google/gson/JsonIOException;
    const-string v3, "UTCJsonSerializer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error in json serialization"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/gson/JsonIOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public declared-synchronized updatePersistentValue(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "value"    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 51
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 52
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 54
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->getPersistentAdditionalInfo()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->getPersistentAdditionalInfo()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->replace(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    :goto_0
    monitor-exit p0

    return-void

    .line 57
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->getPersistentAdditionalInfo()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
