.class public abstract Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;
.super Ljava/lang/Object;
.source "BeamChannelListViewState.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/logging/Loggable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static loadingInstance()Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 40
    new-instance v0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;

    const/4 v3, 0x1

    const/4 v4, 0x0

    move-object v2, v1

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;-><init>(Lcom/google/common/collect/ImmutableList;Ljava/lang/Throwable;ZZLcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;)V

    return-object v0
.end method

.method public static loadingMoreInstance(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;
    .locals 6
    .param p0    # Lcom/google/common/collect/ImmutableList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;)",
            "Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;"
        }
    .end annotation

    .prologue
    .local p0, "existingContent":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;"
    const/4 v2, 0x0

    .line 44
    new-instance v0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;

    const/4 v3, 0x0

    const/4 v4, 0x1

    move-object v1, p0

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;-><init>(Lcom/google/common/collect/ImmutableList;Ljava/lang/Throwable;ZZLcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;)V

    return-object v0
.end method

.method public static withContent(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;
    .locals 6
    .param p0    # Lcom/google/common/collect/ImmutableList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;)",
            "Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;"
        }
    .end annotation

    .prologue
    .local p0, "content":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;"
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 30
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 31
    new-instance v0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;

    move-object v1, p0

    move v4, v3

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;-><init>(Lcom/google/common/collect/ImmutableList;Ljava/lang/Throwable;ZZLcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;)V

    return-object v0
.end method

.method public static withDialog(Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;
    .locals 6
    .param p0, "prevState"    # Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "dialogViewState"    # Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 48
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 49
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 50
    new-instance v0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->content()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->error()Ljava/lang/Throwable;

    move-result-object v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->isLoading()Z

    move-result v3

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->isLoadingMore()Z

    move-result v4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;-><init>(Lcom/google/common/collect/ImmutableList;Ljava/lang/Throwable;ZZLcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;)V

    return-object v0
.end method

.method public static withError(Ljava/lang/Throwable;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;
    .locals 6
    .param p0, "err"    # Ljava/lang/Throwable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 35
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 36
    new-instance v0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;

    move-object v2, p0

    move v4, v3

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;-><init>(Lcom/google/common/collect/ImmutableList;Ljava/lang/Throwable;ZZLcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;)V

    return-object v0
.end method

.method public static withoutDialog(Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;)Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;
    .locals 6
    .param p0, "prevState"    # Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 54
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 55
    new-instance v0, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->content()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->error()Ljava/lang/Throwable;

    move-result-object v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->isLoading()Z

    move-result v3

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->isLoadingMore()Z

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/presentation/beam/AutoValue_BeamChannelListViewState;-><init>(Lcom/google/common/collect/ImmutableList;Ljava/lang/Throwable;ZZLcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;)V

    return-object v0
.end method


# virtual methods
.method public abstract content()Lcom/google/common/collect/ImmutableList;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;"
        }
    .end annotation
.end method

.method public abstract dialogViewState()Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract error()Ljava/lang/Throwable;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public isContent()Z
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->content()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->error()Ljava/lang/Throwable;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->isLoading()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->isLoadingMore()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isError()Z
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->content()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->error()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->isLoading()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->isLoadingMore()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract isLoading()Z
.end method

.method public abstract isLoadingMore()Z
.end method

.method public toLogString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->content()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 70
    .local v0, "channels":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/domain/beam/BeamChannel;>;"
    if-nez v0, :cond_0

    .line 71
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 75
    :goto_0
    return-object v2

    .line 73
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->dialogViewState()Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v1, "null"

    .line 75
    .local v1, "dialogLog":Ljava/lang/String;
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-class v3, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "{contentSize="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 76
    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", error="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 77
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->error()Ljava/lang/Throwable;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isLoading="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 78
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->isLoading()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", isLoadingMore="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 79
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->isLoadingMore()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", dialogViewState="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "}"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 73
    .end local v1    # "dialogLog":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/beam/BeamChannelListViewState;->dialogViewState()Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->toLogString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
