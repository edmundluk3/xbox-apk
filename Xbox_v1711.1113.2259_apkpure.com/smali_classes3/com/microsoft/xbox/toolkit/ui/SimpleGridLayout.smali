.class public Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;
.super Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout;
.source "SimpleGridLayout.java"


# instance fields
.field private columnCount:I

.field private columnWidth:I

.field private columnWidthMod:I

.field private gridDividerSize:I

.field private height:I

.field private rowCount:I

.field private rowHeight:I

.field private rowHeightMod:I

.field private width:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 26
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    sget-object v2, Lcom/microsoft/xboxone/smartglass/R$styleable;->SimpleGridLayout:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 29
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->gridDividerSize:I

    .line 30
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v2}, Lcom/microsoft/xbox/XLEApplication;->isAspectRatioLong()Z

    move-result v2

    if-nez v2, :cond_0

    .line 31
    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 32
    .local v1, "notLongColumnNumber":I
    if-lez v1, :cond_0

    .line 33
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->setColumnCount(I)V

    .line 36
    .end local v1    # "notLongColumnNumber":I
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 38
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->getColumnCount()I

    move-result v2

    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->columnCount:I

    .line 39
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->getRowCount()I

    move-result v2

    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->rowCount:I

    .line 41
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->getRowCount()I

    move-result v2

    if-lez v2, :cond_1

    move v2, v3

    :goto_0
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 42
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->getColumnCount()I

    move-result v2

    if-lez v2, :cond_2

    :goto_1
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 43
    return-void

    :cond_1
    move v2, v4

    .line 41
    goto :goto_0

    :cond_2
    move v3, v4

    .line 42
    goto :goto_1
.end method


# virtual methods
.method public notifyDataChanged()V
    .locals 15

    .prologue
    const/4 v14, 0x1

    const/4 v13, 0x0

    .line 66
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->getGridAdapter()Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;

    move-result-object v11

    if-nez v11, :cond_0

    .line 111
    :goto_0
    return-void

    .line 70
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->removeAllViews()V

    .line 71
    const/4 v1, 0x0

    .local v1, "c":I
    :goto_1
    iget v11, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->columnCount:I

    if-ge v1, v11, :cond_6

    .line 72
    const/4 v7, 0x0

    .local v7, "r":I
    :goto_2
    iget v11, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->rowCount:I

    if-ge v7, v11, :cond_5

    .line 73
    iget v11, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->rowCount:I

    mul-int/2addr v11, v1

    add-int v5, v11, v7

    .line 75
    .local v5, "i":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->getGridAdapter()Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;

    move-result-object v11

    invoke-virtual {v11, v5}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;->getGridView(I)Landroid/view/View;

    move-result-object v10

    .line 77
    .local v10, "view":Landroid/view/View;
    sget-object v11, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->CENTER:Landroid/widget/GridLayout$Alignment;

    invoke-static {v7, v14, v11}, Landroid/widget/GridLayout;->spec(IILandroid/widget/GridLayout$Alignment;)Landroid/widget/GridLayout$Spec;

    move-result-object v9

    .line 78
    .local v9, "rowSpec":Landroid/widget/GridLayout$Spec;
    sget-object v11, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->CENTER:Landroid/widget/GridLayout$Alignment;

    invoke-static {v1, v14, v11}, Landroid/widget/GridLayout;->spec(IILandroid/widget/GridLayout$Alignment;)Landroid/widget/GridLayout$Spec;

    move-result-object v2

    .line 79
    .local v2, "columnSpec":Landroid/widget/GridLayout$Spec;
    new-instance v6, Landroid/widget/GridLayout$LayoutParams;

    invoke-direct {v6, v9, v2}, Landroid/widget/GridLayout$LayoutParams;-><init>(Landroid/widget/GridLayout$Spec;Landroid/widget/GridLayout$Spec;)V

    .line 80
    .local v6, "params":Landroid/widget/GridLayout$LayoutParams;
    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->columnWidth:I

    .line 81
    .local v4, "gridWidth":I
    iget v11, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->columnWidthMod:I

    if-eqz v11, :cond_1

    iget v11, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->columnCount:I

    iget v12, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->columnWidthMod:I

    sub-int/2addr v11, v12

    if-lt v1, v11, :cond_1

    .line 82
    add-int/lit8 v4, v4, 0x1

    .line 84
    :cond_1
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->rowHeight:I

    .line 85
    .local v3, "gridHeight":I
    iget v11, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->rowHeightMod:I

    if-eqz v11, :cond_2

    iget v11, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->rowCount:I

    iget v12, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->rowHeightMod:I

    sub-int/2addr v11, v12

    if-lt v7, v11, :cond_2

    .line 86
    add-int/lit8 v3, v3, 0x1

    .line 88
    :cond_2
    iput v4, v6, Landroid/widget/GridLayout$LayoutParams;->width:I

    .line 89
    iput v3, v6, Landroid/widget/GridLayout$LayoutParams;->height:I

    .line 91
    iget v8, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->gridDividerSize:I

    .line 92
    .local v8, "rightMargin":I
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->gridDividerSize:I

    .line 94
    .local v0, "bottomMargin":I
    iget v11, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->columnCount:I

    add-int/lit8 v11, v11, -0x1

    if-ne v1, v11, :cond_3

    .line 95
    const/4 v8, 0x0

    .line 97
    :cond_3
    iget v11, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->rowCount:I

    add-int/lit8 v11, v11, -0x1

    if-ne v7, v11, :cond_4

    .line 98
    const/4 v0, 0x0

    .line 100
    :cond_4
    invoke-virtual {v6, v13, v13, v8, v0}, Landroid/widget/GridLayout$LayoutParams;->setMargins(IIII)V

    .line 101
    invoke-virtual {p0, v10, v6}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 72
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 71
    .end local v0    # "bottomMargin":I
    .end local v2    # "columnSpec":Landroid/widget/GridLayout$Spec;
    .end local v3    # "gridHeight":I
    .end local v4    # "gridWidth":I
    .end local v5    # "i":I
    .end local v6    # "params":Landroid/widget/GridLayout$LayoutParams;
    .end local v8    # "rightMargin":I
    .end local v9    # "rowSpec":Landroid/widget/GridLayout$Spec;
    .end local v10    # "view":Landroid/view/View;
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 105
    .end local v7    # "r":I
    :cond_6
    new-instance v11, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout$1;

    invoke-direct {v11, p0}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout$1;-><init>(Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;)V

    invoke-virtual {p0, v11}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 2
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 47
    invoke-super {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/toolkit/ui/AbstractGridLayout;->onSizeChanged(IIII)V

    .line 49
    if-lez p1, :cond_1

    if-lez p2, :cond_1

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->getGridAdapter()Lcom/microsoft/xbox/toolkit/ui/SimpleGridAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->width:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->height:I

    if-eq v0, p2, :cond_1

    .line 50
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->gridDividerSize:I

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->columnCount:I

    add-int/lit8 v1, v1, -0x1

    mul-int/2addr v0, v1

    sub-int/2addr p1, v0

    .line 51
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->gridDividerSize:I

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->rowCount:I

    add-int/lit8 v1, v1, -0x1

    mul-int/2addr v0, v1

    sub-int/2addr p2, v0

    .line 52
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->columnCount:I

    div-int v0, p1, v0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->columnWidth:I

    .line 53
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->rowCount:I

    div-int v0, p2, v0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->rowHeight:I

    .line 54
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->columnCount:I

    rem-int v0, p1, v0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->columnWidthMod:I

    .line 55
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->rowCount:I

    rem-int v0, p2, v0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->rowHeightMod:I

    .line 57
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->notifyDataChanged()V

    .line 59
    :cond_1
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->width:I

    .line 60
    iput p2, p0, Lcom/microsoft/xbox/toolkit/ui/SimpleGridLayout;->height:I

    .line 61
    return-void
.end method
