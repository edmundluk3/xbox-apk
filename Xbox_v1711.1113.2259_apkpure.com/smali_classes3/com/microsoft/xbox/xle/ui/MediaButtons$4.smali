.class Lcom/microsoft/xbox/xle/ui/MediaButtons$4;
.super Ljava/lang/Object;
.source "MediaButtons.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/MediaButtons;->updateView(Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/MediaButtons;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/MediaButtons;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/MediaButtons;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons$4;->this$0:Lcom/microsoft/xbox/xle/ui/MediaButtons;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons$4;->this$0:Lcom/microsoft/xbox/xle/ui/MediaButtons;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->access$100(Lcom/microsoft/xbox/xle/ui/MediaButtons;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons$4;->this$0:Lcom/microsoft/xbox/xle/ui/MediaButtons;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->access$200(Lcom/microsoft/xbox/xle/ui/MediaButtons;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/MediaButtons$4;->this$0:Lcom/microsoft/xbox/xle/ui/MediaButtons;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/MediaButtons;->access$000(Lcom/microsoft/xbox/xle/ui/MediaButtons;)Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->PreviousTrack:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    const-string v2, "Transport Control - SkipBack"

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->sendMediaCommands(Lcom/microsoft/xbox/smartglass/MediaControlCommands;Ljava/lang/String;)V

    .line 142
    const/4 v0, 0x1

    .line 144
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
