.class public Lcom/microsoft/xbox/service/model/serialization/ProgrammingSlot;
.super Ljava/lang/Object;
.source "ProgrammingSlot.java"


# annotations
.annotation runtime Lorg/simpleframework/xml/Root;
    name = "Slot"
.end annotation


# instance fields
.field public Action:Lcom/microsoft/xbox/service/model/serialization/ProgrammingAction;
    .annotation runtime Lorg/simpleframework/xml/Element;
        required = false
    .end annotation
.end field

.field public Description:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "Description"
        required = false
    .end annotation
.end field

.field public ImageUrl:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "ImageUrl"
        required = false
    .end annotation
.end field

.field public Name:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "Name"
        required = false
    .end annotation
.end field

.field public Title:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "Title"
        required = false
    .end annotation
.end field

.field public type:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        name = "type"
        required = false
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
