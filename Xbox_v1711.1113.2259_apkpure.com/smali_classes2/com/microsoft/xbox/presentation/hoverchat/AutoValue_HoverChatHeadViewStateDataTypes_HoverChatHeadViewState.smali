.class final Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;
.super Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;
.source "AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState.java"


# instance fields
.field private final badgeViewState:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;

.field private final imageUrls:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final isMaximized:Z

.field private final participants:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final shape:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;ZLcom/google/common/collect/ImmutableList;Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;Lcom/google/common/collect/ImmutableList;)V
    .locals 2
    .param p1, "badgeViewState"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;
    .param p2, "isMaximized"    # Z
    .param p4, "shape"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;",
            "Z",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p3, "imageUrls":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    .local p5, "participants":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;-><init>()V

    .line 23
    if-nez p1, :cond_0

    .line 24
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null badgeViewState"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->badgeViewState:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;

    .line 27
    iput-boolean p2, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->isMaximized:Z

    .line 28
    if-nez p3, :cond_1

    .line 29
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null imageUrls"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 31
    :cond_1
    iput-object p3, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->imageUrls:Lcom/google/common/collect/ImmutableList;

    .line 32
    if-nez p4, :cond_2

    .line 33
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null shape"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_2
    iput-object p4, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->shape:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;

    .line 36
    if-nez p5, :cond_3

    .line 37
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null participants"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_3
    iput-object p5, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->participants:Lcom/google/common/collect/ImmutableList;

    .line 40
    return-void
.end method


# virtual methods
.method public badgeViewState()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->badgeViewState:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 84
    if-ne p1, p0, :cond_1

    .line 95
    :cond_0
    :goto_0
    return v1

    .line 87
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 88
    check-cast v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;

    .line 89
    .local v0, "that":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->badgeViewState:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->badgeViewState()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->isMaximized:Z

    .line 90
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->isMaximized()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->imageUrls:Lcom/google/common/collect/ImmutableList;

    .line 91
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->imageUrls()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->shape:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;

    .line 92
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->shape()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->participants:Lcom/google/common/collect/ImmutableList;

    .line 93
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;->participants()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$HoverChatHeadViewState;
    :cond_3
    move v1, v2

    .line 95
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 100
    const/4 v0, 0x1

    .line 101
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 102
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->badgeViewState:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 103
    mul-int/2addr v0, v2

    .line 104
    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->isMaximized:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x4cf

    :goto_0
    xor-int/2addr v0, v1

    .line 105
    mul-int/2addr v0, v2

    .line 106
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->imageUrls:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 107
    mul-int/2addr v0, v2

    .line 108
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->shape:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;

    invoke-virtual {v1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 109
    mul-int/2addr v0, v2

    .line 110
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->participants:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 111
    return v0

    .line 104
    :cond_0
    const/16 v1, 0x4d5

    goto :goto_0
.end method

.method public imageUrls()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->imageUrls:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public isMaximized()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->isMaximized:Z

    return v0
.end method

.method public participants()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->participants:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public shape()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->shape:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HoverChatHeadViewState{badgeViewState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->badgeViewState:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$BadgeViewState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isMaximized="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->isMaximized:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", imageUrls="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->imageUrls:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", shape="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->shape:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Shape;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", participants="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/AutoValue_HoverChatHeadViewStateDataTypes_HoverChatHeadViewState;->participants:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
