.class Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener;
.super Lcom/microsoft/xbox/smartglass/controls/WebViewListener;
.source "CanvasWebViewActivityViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "XLEWebViewListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;)V
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/WebViewListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$1;

    .prologue
    .line 315
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener;-><init>(Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;)V

    return-void
.end method


# virtual methods
.method public onLoadCompleted()V
    .locals 2

    .prologue
    .line 326
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->access$102(Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;Z)Z

    .line 328
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 335
    :cond_0
    return-void
.end method

.method public onNavigating(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 339
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 340
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener$2;-><init>(Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 347
    :cond_0
    return-void
.end method

.method public onNavigationFailed(Ljava/lang/String;ILjava/lang/String;)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;

    .prologue
    .line 351
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->access$102(Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;Z)Z

    .line 353
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener$3;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener$3;-><init>(Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 360
    :cond_0
    return-void
.end method

.method public onReady()V
    .locals 2

    .prologue
    .line 365
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel$XLEWebViewListener;->this$0:Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/CanvasWebViewActivityViewModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366
    const-string v0, "CanvasWebViewActivityViewModel"

    const-string v1, "received WebViewListener.onReady"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    :cond_0
    return-void
.end method
