.class public final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_ClubHubDataTypes_ClubRecommendations.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;",
        ">;"
    }
.end annotation


# instance fields
.field private final criteriaAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultCriteria:Ljava/lang/String;

.field private defaultReasons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendationReason;",
            ">;"
        }
    .end annotation
.end field

.field private defaultTitleIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final reasonsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendationReason;",
            ">;>;"
        }
    .end annotation
.end field

.field private final titleIdsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 5
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 25
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;->defaultCriteria:Ljava/lang/String;

    .line 26
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;->defaultReasons:Ljava/util/List;

    .line 27
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;->defaultTitleIds:Ljava/util/List;

    .line 29
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;->criteriaAdapter:Lcom/google/gson/TypeAdapter;

    .line 30
    const-class v0, Ljava/util/List;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendationReason;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;->reasonsAdapter:Lcom/google/gson/TypeAdapter;

    .line 31
    const-class v0, Ljava/util/List;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Ljava/lang/Long;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;->titleIdsAdapter:Lcom/google/gson/TypeAdapter;

    .line 32
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;
    .locals 6
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v4

    sget-object v5, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v4, v5, :cond_0

    .line 64
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 65
    const/4 v4, 0x0

    .line 96
    :goto_0
    return-object v4

    .line 67
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 68
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;->defaultCriteria:Ljava/lang/String;

    .line 69
    .local v1, "criteria":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;->defaultReasons:Ljava/util/List;

    .line 70
    .local v2, "reasons":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendationReason;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;->defaultTitleIds:Ljava/util/List;

    .line 71
    .local v3, "titleIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 72
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v4

    sget-object v5, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v4, v5, :cond_1

    .line 74
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 77
    :cond_1
    const/4 v4, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v4, :pswitch_data_0

    .line 91
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 77
    :sswitch_0
    const-string v5, "criteria"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x0

    goto :goto_2

    :sswitch_1
    const-string v5, "reasons"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x1

    goto :goto_2

    :sswitch_2
    const-string v5, "titleIds"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x2

    goto :goto_2

    .line 79
    :pswitch_0
    iget-object v4, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;->criteriaAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v4, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "criteria":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 80
    .restart local v1    # "criteria":Ljava/lang/String;
    goto :goto_1

    .line 83
    :pswitch_1
    iget-object v4, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;->reasonsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v4, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "reasons":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendationReason;>;"
    check-cast v2, Ljava/util/List;

    .line 84
    .restart local v2    # "reasons":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendationReason;>;"
    goto :goto_1

    .line 87
    :pswitch_2
    iget-object v4, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;->titleIdsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v4, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "titleIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    check-cast v3, Ljava/util/List;

    .line 88
    .restart local v3    # "titleIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    goto :goto_1

    .line 95
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 96
    new-instance v4, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations;

    invoke-direct {v4, v1, v2, v3}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    goto :goto_0

    .line 77
    nop

    :sswitch_data_0
    .sparse-switch
        -0x6f7642c0 -> :sswitch_2
        0x406cb6af -> :sswitch_1
        0x7459df5f -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultCriteria(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultCriteria"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;->defaultCriteria:Ljava/lang/String;

    .line 35
    return-object p0
.end method

.method public setDefaultReasons(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendationReason;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 38
    .local p1, "defaultReasons":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendationReason;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;->defaultReasons:Ljava/util/List;

    .line 39
    return-object p0
.end method

.method public setDefaultTitleIds(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 42
    .local p1, "defaultTitleIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;->defaultTitleIds:Ljava/util/List;

    .line 43
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    if-nez p2, :cond_0

    .line 49
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 60
    :goto_0
    return-void

    .line 52
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 53
    const-string v0, "criteria"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;->criteriaAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;->criteria()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 55
    const-string v0, "reasons"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;->reasonsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;->reasons()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 57
    const-string v0, "titleIds"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;->titleIdsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;->titleIds()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 59
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    check-cast p2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRecommendations$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;)V

    return-void
.end method
