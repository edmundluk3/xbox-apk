.class public Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "AchievementsFriendsWhoEarnedListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rowViewResourceId"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 22
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v6, 0x7f020125

    const/4 v4, 0x0

    .line 28
    if-nez p2, :cond_2

    .line 29
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v5, "layout_inflater"

    invoke-virtual {v3, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 30
    .local v2, "vi":Landroid/view/LayoutInflater;
    const v3, 0x7f030021

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 32
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;

    invoke-direct {v0, v4}, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$1;)V

    .line 33
    .local v0, "holder":Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;
    const v3, 0x7f0e0119

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {v0, v3}, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;->access$102(Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;)Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 35
    const v3, 0x7f0e011a

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-static {v0, v3}, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;->access$202(Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 36
    const v3, 0x7f0e011b

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-static {v0, v3}, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;->access$302(Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 38
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 43
    .end local v2    # "vi":Landroid/view/LayoutInflater;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;

    .line 44
    .local v1, "item":Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;
    if-eqz v1, :cond_1

    .line 45
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;->access$100(Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 46
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;->access$100(Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;)Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    move-result-object v3

    iget-object v5, v1, Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;->displayPicUrl:Ljava/lang/String;

    invoke-virtual {v3, v5, v6, v6}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 49
    :cond_0
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;->access$200(Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    const-string v6, "%s %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, v1, Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;->gamerTag:Ljava/lang/String;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget-object v9, v1, Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;->realName:Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 50
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;->access$300(Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v5

    iget-object v3, v1, Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;->date:Ljava/util/Date;

    if-eqz v3, :cond_3

    iget-object v3, v1, Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;->date:Ljava/util/Date;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->shortDateUptoMonthToDurationNow(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-static {v5, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 54
    :cond_1
    return-object p2

    .line 40
    .end local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;
    .end local v1    # "item":Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;

    .restart local v0    # "holder":Lcom/microsoft/xbox/xle/app/adapter/AchievementsFriendsWhoEarnedListAdapter$ViewHolder;
    goto :goto_0

    .restart local v1    # "item":Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;
    :cond_3
    move-object v3, v4

    .line 50
    goto :goto_1
.end method
