.class Lcom/microsoft/xbox/smartglass/controls/JsonSensorStartParameters;
.super Ljava/lang/Object;
.source "JsonSensorStartParameters.java"


# instance fields
.field public final sampleRateMilliseconds:I

.field public final sendToCanvas:Z

.field public final sendToConsole:Z

.field public final titleId:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "json"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 19
    .local v0, "jsonObject":Lorg/json/JSONObject;
    const-string v1, "titleId"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonSensorStartParameters;->titleId:I

    .line 20
    const-string v1, "sampleRateMilliseconds"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonSensorStartParameters;->sampleRateMilliseconds:I

    .line 21
    const-string v1, "sendToCanvas"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonSensorStartParameters;->sendToCanvas:Z

    .line 22
    const-string v1, "sendToConsole"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonSensorStartParameters;->sendToConsole:Z

    .line 23
    return-void
.end method
