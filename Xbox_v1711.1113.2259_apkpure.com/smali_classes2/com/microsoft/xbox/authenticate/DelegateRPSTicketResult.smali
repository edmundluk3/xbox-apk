.class public Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult;
.super Ljava/lang/Object;
.source "DelegateRPSTicketResult.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult;",
            ">;"
        }
    .end annotation
.end field

.field private static final errorCodes:[Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;


# instance fields
.field private errorCode:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

.field private ticket:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    invoke-static {}, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->values()[Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult;->errorCodes:[Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    .line 36
    new-instance v0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$1;

    invoke-direct {v0}, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$1;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult;->readFromParcel(Landroid/os/Parcel;)V

    .line 54
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$1;

    .prologue
    .line 7
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;Ljava/lang/String;Landroid/content/Context;)V
    .locals 0
    .param p1, "resultCode"    # Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;
    .param p2, "ticket"    # Ljava/lang/String;
    .param p3, "ctx"    # Landroid/content/Context;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p2, p0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult;->ticket:Ljava/lang/String;

    .line 49
    iput-object p1, p0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult;->errorCode:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    .line 50
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return v0
.end method

.method public getErrorCode()I
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult;->errorCode:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    invoke-virtual {v0}, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->getValue()I

    move-result v0

    return v0
.end method

.method public getTicket()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult;->ticket:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 66
    .local v0, "rawValue":I
    sget-object v1, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->UNKNOWN_RESULT_CODE:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    invoke-virtual {v1}, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->getValue()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->UNKNOWN_RESULT_CODE:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    invoke-virtual {v1}, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->getValue()I

    move-result v0

    .line 67
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult;->errorCodes:[Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    aget-object v1, v1, v0

    iput-object v1, p0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult;->errorCode:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult;->ticket:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult;->errorCode:Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;

    invoke-virtual {v0}, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult$ResultCode;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/authenticate/DelegateRPSTicketResult;->ticket:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 80
    return-void
.end method
