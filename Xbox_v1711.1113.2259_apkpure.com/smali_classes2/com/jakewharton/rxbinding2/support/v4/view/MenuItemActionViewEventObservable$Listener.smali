.class final Lcom/jakewharton/rxbinding2/support/v4/view/MenuItemActionViewEventObservable$Listener;
.super Lio/reactivex/android/MainThreadDisposable;
.source "MenuItemActionViewEventObservable.java"

# interfaces
.implements Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jakewharton/rxbinding2/support/v4/view/MenuItemActionViewEventObservable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Listener"
.end annotation


# instance fields
.field private final handled:Lio/reactivex/functions/Predicate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/functions/Predicate",
            "<-",
            "Lcom/jakewharton/rxbinding2/view/MenuItemActionViewEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final menuItem:Landroid/view/MenuItem;

.field private final observer:Lio/reactivex/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observer",
            "<-",
            "Lcom/jakewharton/rxbinding2/view/MenuItemActionViewEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/view/MenuItem;Lio/reactivex/functions/Predicate;Lio/reactivex/Observer;)V
    .locals 0
    .param p1, "menuItem"    # Landroid/view/MenuItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/MenuItem;",
            "Lio/reactivex/functions/Predicate",
            "<-",
            "Lcom/jakewharton/rxbinding2/view/MenuItemActionViewEvent;",
            ">;",
            "Lio/reactivex/Observer",
            "<-",
            "Lcom/jakewharton/rxbinding2/view/MenuItemActionViewEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 41
    .local p2, "handled":Lio/reactivex/functions/Predicate;, "Lio/reactivex/functions/Predicate<-Lcom/jakewharton/rxbinding2/view/MenuItemActionViewEvent;>;"
    .local p3, "observer":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-Lcom/jakewharton/rxbinding2/view/MenuItemActionViewEvent;>;"
    invoke-direct {p0}, Lio/reactivex/android/MainThreadDisposable;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/jakewharton/rxbinding2/support/v4/view/MenuItemActionViewEventObservable$Listener;->menuItem:Landroid/view/MenuItem;

    .line 43
    iput-object p2, p0, Lcom/jakewharton/rxbinding2/support/v4/view/MenuItemActionViewEventObservable$Listener;->handled:Lio/reactivex/functions/Predicate;

    .line 44
    iput-object p3, p0, Lcom/jakewharton/rxbinding2/support/v4/view/MenuItemActionViewEventObservable$Listener;->observer:Lio/reactivex/Observer;

    .line 45
    return-void
.end method

.method private onEvent(Lcom/jakewharton/rxbinding2/view/MenuItemActionViewEvent;)Z
    .locals 2
    .param p1, "event"    # Lcom/jakewharton/rxbinding2/view/MenuItemActionViewEvent;

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/jakewharton/rxbinding2/support/v4/view/MenuItemActionViewEventObservable$Listener;->isDisposed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 58
    :try_start_0
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/support/v4/view/MenuItemActionViewEventObservable$Listener;->handled:Lio/reactivex/functions/Predicate;

    invoke-interface {v1, p1}, Lio/reactivex/functions/Predicate;->test(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/support/v4/view/MenuItemActionViewEventObservable$Listener;->observer:Lio/reactivex/Observer;

    invoke-interface {v1, p1}, Lio/reactivex/Observer;->onNext(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    const/4 v1, 0x1

    .line 67
    :goto_0
    return v1

    .line 62
    :catch_0
    move-exception v0

    .line 63
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/support/v4/view/MenuItemActionViewEventObservable$Listener;->observer:Lio/reactivex/Observer;

    invoke-interface {v1, v0}, Lio/reactivex/Observer;->onError(Ljava/lang/Throwable;)V

    .line 64
    invoke-virtual {p0}, Lcom/jakewharton/rxbinding2/support/v4/view/MenuItemActionViewEventObservable$Listener;->dispose()V

    .line 67
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onDispose()V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/jakewharton/rxbinding2/support/v4/view/MenuItemActionViewEventObservable$Listener;->menuItem:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/support/v4/view/MenuItemCompat;->setOnActionExpandListener(Landroid/view/MenuItem;Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;)Landroid/view/MenuItem;

    .line 72
    return-void
.end method

.method public onMenuItemActionCollapse(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 52
    invoke-static {p1}, Lcom/jakewharton/rxbinding2/view/MenuItemActionViewCollapseEvent;->create(Landroid/view/MenuItem;)Lcom/jakewharton/rxbinding2/view/MenuItemActionViewCollapseEvent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jakewharton/rxbinding2/support/v4/view/MenuItemActionViewEventObservable$Listener;->onEvent(Lcom/jakewharton/rxbinding2/view/MenuItemActionViewEvent;)Z

    move-result v0

    return v0
.end method

.method public onMenuItemActionExpand(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 48
    invoke-static {p1}, Lcom/jakewharton/rxbinding2/view/MenuItemActionViewExpandEvent;->create(Landroid/view/MenuItem;)Lcom/jakewharton/rxbinding2/view/MenuItemActionViewExpandEvent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jakewharton/rxbinding2/support/v4/view/MenuItemActionViewEventObservable$Listener;->onEvent(Lcom/jakewharton/rxbinding2/view/MenuItemActionViewEvent;)Z

    move-result v0

    return v0
.end method
