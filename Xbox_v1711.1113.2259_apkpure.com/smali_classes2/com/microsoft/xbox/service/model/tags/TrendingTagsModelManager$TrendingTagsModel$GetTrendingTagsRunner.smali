.class Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel$GetTrendingTagsRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "TrendingTagsModelManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetTrendingTagsRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;",
        ">;"
    }
.end annotation


# instance fields
.field private final activityHubService:Lcom/microsoft/xbox/service/activityHub/IActivityHubService;

.field final synthetic this$1:Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;)V
    .locals 1

    .prologue
    .line 95
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel$GetTrendingTagsRunner;->this$1:Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 96
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getActivityHubService()Lcom/microsoft/xbox/service/activityHub/IActivityHubService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel$GetTrendingTagsRunner;->activityHubService:Lcom/microsoft/xbox/service/activityHub/IActivityHubService;

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$1;

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel$GetTrendingTagsRunner;-><init>(Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;)V

    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel$GetTrendingTagsRunner;->activityHubService:Lcom/microsoft/xbox/service/activityHub/IActivityHubService;

    const/16 v1, 0x32

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel$GetTrendingTagsRunner;->this$1:Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;->access$200(Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/service/activityHub/IActivityHubService;->getTrendingTags(ILjava/lang/String;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel$GetTrendingTagsRunner;->buildData()Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 114
    const-wide/16 v0, 0x25e5

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 109
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$TrendingTagList;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel$GetTrendingTagsRunner;->this$1:Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;->access$300(Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager$TrendingTagsModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 110
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 100
    return-void
.end method
