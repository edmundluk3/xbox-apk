.class public Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;
.super Ljava/lang/Object;
.source "ThreadSafeHashtable.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private data:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private dataInverted:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<TV;TK;>;"
        }
    .end annotation
.end field

.field private syncObject:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;, "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable<TK;TV;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->data:Ljava/util/Hashtable;

    .line 14
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->dataInverted:Ljava/util/Hashtable;

    .line 15
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->syncObject:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public Keys()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 76
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;, "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable<TK;TV;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->syncObject:Ljava/lang/Object;

    monitor-enter v1

    .line 77
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->data:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keySet()Ljava/util/Set;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public Values()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 70
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;, "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable<TK;TV;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->syncObject:Ljava/lang/Object;

    monitor-enter v1

    .line 71
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->dataInverted:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->keySet()Ljava/util/Set;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 63
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;, "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable<TK;TV;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->syncObject:Ljava/lang/Object;

    monitor-enter v1

    .line 64
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->data:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->dataInverted:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    .line 66
    monitor-exit v1

    .line 67
    return-void

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)Z"
        }
    .end annotation

    .prologue
    .line 35
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;, "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->syncObject:Ljava/lang/Object;

    monitor-enter v1

    .line 36
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->data:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)Z"
        }
    .end annotation

    .prologue
    .line 41
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;, "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable<TK;TV;>;"
    .local p1, "value":Ljava/lang/Object;, "TV;"
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->syncObject:Ljava/lang/Object;

    monitor-enter v1

    .line 42
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->dataInverted:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 43
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 29
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;, "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->syncObject:Ljava/lang/Object;

    monitor-enter v1

    .line 30
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->data:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 31
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getLock()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;, "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable<TK;TV;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->syncObject:Ljava/lang/Object;

    return-object v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;, "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "value":Ljava/lang/Object;, "TV;"
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->syncObject:Ljava/lang/Object;

    monitor-enter v1

    .line 23
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->data:Ljava/util/Hashtable;

    invoke-virtual {v0, p1, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->dataInverted:Ljava/util/Hashtable;

    invoke-virtual {v0, p2, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    monitor-exit v1

    .line 26
    return-void

    .line 25
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public remove(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;, "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable<TK;TV;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->syncObject:Ljava/lang/Object;

    monitor-enter v2

    .line 48
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->data:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 49
    .local v0, "value":Ljava/lang/Object;, "TV;"
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->data:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->dataInverted:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    monitor-exit v2

    .line 52
    return-void

    .line 51
    .end local v0    # "value":Ljava/lang/Object;, "TV;"
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public removeValue(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p0, "this":Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;, "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable<TK;TV;>;"
    .local p1, "value":Ljava/lang/Object;, "TV;"
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->syncObject:Ljava/lang/Object;

    monitor-enter v2

    .line 56
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->dataInverted:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 57
    .local v0, "key":Ljava/lang/Object;, "TK;"
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->data:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->dataInverted:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    monitor-exit v2

    .line 60
    return-void

    .line 59
    .end local v0    # "key":Ljava/lang/Object;, "TK;"
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
