.class public Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SkypeMessagingDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SkypeDBHelper"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;Landroid/content/Context;)V
    .locals 3
    .param p1, "this$0"    # Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1834
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;->this$0:Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;

    .line 1835
    const-string v0, "SkypeMessaging.db"

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-direct {p0, p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 1836
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1840
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Create conversations table"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1841
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$ConversationEntry;->CREATE_CONVERSATIONS_TABLE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1843
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Create messages table"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1844
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$MessageEntry;->CREATE_MESSAGES_TABLE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1845
    return-void
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 1857
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Downgrade database"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1859
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;->reset(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1860
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 1850
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Upgrade database"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1852
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$SkypeDBHelper;->reset(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1853
    return-void
.end method

.method public reset(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 1863
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1866
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Delete conversations table"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1867
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$ConversationEntry;->DELETE_CONVERSATIONS_TABLE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1869
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Delete messages table"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1870
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$MessageEntry;->DELETE_MESSAGES_TABLE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1873
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Create conversations table"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1874
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$ConversationEntry;->CREATE_CONVERSATIONS_TABLE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1876
    invoke-static {}, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Create messages table"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1877
    sget-object v0, Lcom/microsoft/xbox/toolkit/SkypeMessagingDB$MessageEntry;->CREATE_MESSAGES_TABLE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1878
    return-void
.end method
