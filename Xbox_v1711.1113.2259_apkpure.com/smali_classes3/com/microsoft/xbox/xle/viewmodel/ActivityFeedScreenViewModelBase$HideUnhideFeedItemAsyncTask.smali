.class Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ActivityFeedScreenViewModelBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HideUnhideFeedItemAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final hide:Z

.field private final itemRoot:Ljava/lang/String;

.field private final model:Lcom/microsoft/xbox/service/model/ProfileModel;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "itemRoot"    # Ljava/lang/String;
    .param p3, "hide"    # Z

    .prologue
    .line 1333
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 1328
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 1334
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;->itemRoot:Ljava/lang/String;

    .line 1335
    iput-boolean p3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;->hide:Z

    .line 1336
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 1340
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 1341
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3

    .prologue
    .line 1357
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;->itemRoot:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;->hide:Z

    .line 1358
    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->hideFeedItem(Ljava/lang/String;Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 1357
    :goto_0
    return-object v0

    .line 1358
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1327
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 1352
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1327
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 3

    .prologue
    .line 1346
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 1347
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;->hide:Z

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->access$800(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Z)V

    .line 1348
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 1370
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;->hide:Z

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->access$800(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Z)V

    .line 1371
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1327
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 1364
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 1365
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase$HideUnhideFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedScreenViewModelBase;->updateAdapter()V

    .line 1366
    return-void
.end method
