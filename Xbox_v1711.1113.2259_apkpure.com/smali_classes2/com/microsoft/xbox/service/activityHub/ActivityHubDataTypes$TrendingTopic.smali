.class public abstract Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;
.super Ljava/lang/Object;
.source "ActivityHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "TrendingTopic"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingTopic;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    new-instance v0, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/activityHub/AutoValue_ActivityHubDataTypes_TrendingTopic$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract caption()Ljava/lang/String;
.end method

.method public abstract displayImage()Ljava/lang/String;
.end method

.method public abstract name()Ljava/lang/String;
.end method

.method public abstract sourceId()Ljava/lang/String;
.end method
