.class public final Lcom/microsoft/xbox/service/clubs/ClubDataTypes;
.super Ljava/lang/Object;
.source "ClubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotificationDetails;,
        Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubNotification;,
        Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubRecommendationRequest;,
        Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;,
        Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;,
        Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "This type should not be instantiated"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
