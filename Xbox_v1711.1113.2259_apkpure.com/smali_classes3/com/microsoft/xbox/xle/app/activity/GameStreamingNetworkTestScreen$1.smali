.class Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1;
.super Ljava/lang/Object;
.source "GameStreamingNetworkTestScreen.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1;->this$0:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 88
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/SessionManager;->getPrimaryDeviceState()Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;

    move-result-object v6

    .line 89
    .local v6, "deviceState":Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;
    iget v0, v6, Lcom/microsoft/xbox/smartglass/PrimaryDeviceState;->buildNumber:I

    const/16 v1, 0x319f

    if-ge v0, v1, :cond_0

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1;->this$0:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->access$000(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;)V

    .line 92
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1$1;

    const-wide/16 v2, 0x2710

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1$1;-><init>(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1;JJ)V

    .line 103
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1$1;->start()Landroid/os/CountDownTimer;

    .line 123
    :goto_0
    return-void

    .line 104
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getCurrentSessionState()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1;->this$0:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;

    sget-object v1, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;->CONSOLE_DISCONNECTED:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->access$200(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;)V

    goto :goto_0

    .line 106
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getPairedIdentityState()Lcom/microsoft/xbox/smartglass/PairedIdentityState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/smartglass/PairedIdentityState;->NotPaired:Lcom/microsoft/xbox/smartglass/PairedIdentityState;

    if-ne v0, v1, :cond_2

    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1;->this$0:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;

    sget-object v1, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;->USER_NOT_PAIRED:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->access$200(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;)V

    goto :goto_0

    .line 109
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1;->this$0:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->access$000(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;)V

    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1;->this$0:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;

    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->access$302(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;Ljava/util/Timer;)Ljava/util/Timer;

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1;->this$0:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->access$300(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;)Ljava/util/Timer;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1$2;-><init>(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1;)V

    const-wide/32 v2, 0x2bf20

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1;->this$0:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->access$400(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;)Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->startGameStream()Z

    goto :goto_0
.end method
