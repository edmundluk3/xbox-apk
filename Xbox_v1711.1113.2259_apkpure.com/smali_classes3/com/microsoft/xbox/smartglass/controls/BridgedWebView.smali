.class public Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;
.super Landroid/webkit/WebView;
.source "BridgedWebView.java"

# interfaces
.implements Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "SetJavaScriptEnabled"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/smartglass/controls/BridgedWebView$ChromeClient;,
        Lcom/microsoft/xbox/smartglass/controls/BridgedWebView$ViewClient;,
        Lcom/microsoft/xbox/smartglass/controls/BridgedWebView$JavaScriptBridge;
    }
.end annotation


# static fields
.field private static final CanvasTag:Ljava/lang/String; = "canvas_layout"

.field private static final DoneResult:Ljava/lang/String; = "done"

.field private static final ErrorResult:Ljava/lang/String; = "error"

.field private static final KeyboardDetectionThreshold:I = 0x78

.field private static final VersionMetric:Ljava/lang/String; = "ScriptVersion"

.field private static _destroyLock:Ljava/lang/Object;

.field private static _invokeScriptLock:Ljava/lang/Object;


# instance fields
.field private final _additionalData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation
.end field

.field private final _components:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/smartglass/controls/WebComponent;",
            ">;"
        }
    .end annotation
.end field

.field private _hostInfo:Lcom/microsoft/xbox/smartglass/controls/JsonCanvasHostInfo;

.field private _isDestroyed:Z

.field private _isReady:Z

.field private final _listeners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/controls/WebViewListener;",
            ">;"
        }
    .end annotation
.end field

.field private _scriptRunner:Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

.field private _version:Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_invokeScriptLock:Ljava/lang/Object;

    .line 43
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_destroyLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_components:Ljava/util/HashMap;

    .line 49
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_listeners:Ljava/util/HashSet;

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_additionalData:Ljava/util/HashMap;

    .line 59
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->initialize()V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_components:Ljava/util/HashMap;

    .line 49
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_listeners:Ljava/util/HashSet;

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_additionalData:Ljava/util/HashMap;

    .line 69
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->initialize()V

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 79
    invoke-direct {p0, p1, p2, p3}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_components:Ljava/util/HashMap;

    .line 49
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_listeners:Ljava/util/HashSet;

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_additionalData:Ljava/util/HashMap;

    .line 80
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->initialize()V

    .line 81
    return-void
.end method

.method private HandleBridgeMethod(ILcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;)V
    .locals 4
    .param p1, "requestId"    # I
    .param p2, "content"    # Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;

    .prologue
    .line 440
    iget-object v0, p2, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->methodName:Ljava/lang/String;

    const-string v1, "setClientInfo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 441
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->setClientInfo(ILcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;)V

    .line 447
    :goto_0
    return-void

    .line 442
    :cond_0
    iget-object v0, p2, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->methodName:Ljava/lang/String;

    const-string v1, "log"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 443
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->log(ILcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;)V

    goto :goto_0

    .line 445
    :cond_1
    const-string v0, "%s is not a valid bridged web view method."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p2, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->methodName:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->failRequest(ILjava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->onScriptNotify(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;)Lcom/microsoft/xbox/smartglass/controls/JsonCanvasHostInfo;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_hostInfo:Lcom/microsoft/xbox/smartglass/controls/JsonCanvasHostInfo;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # [Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->invokeScriptInternal(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->onLoadCompleted()V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;)Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_scriptRunner:Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->onNavigating(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->onNavigationFailed(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method private cloneListeners()Ljava/util/HashSet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/controls/WebViewListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_listeners:Ljava/util/HashSet;

    monitor-enter v2

    .line 122
    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_listeners:Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 123
    .local v0, "clone":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/microsoft/xbox/smartglass/controls/WebViewListener;>;"
    monitor-exit v2

    .line 125
    return-object v0

    .line 123
    .end local v0    # "clone":Ljava/util/HashSet;, "Ljava/util/HashSet<Lcom/microsoft/xbox/smartglass/controls/WebViewListener;>;"
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private completeRequestInternal(ILjava/lang/String;Lorg/json/JSONObject;)V
    .locals 5
    .param p1, "requestId"    # I
    .param p2, "result"    # Ljava/lang/String;
    .param p3, "json"    # Lorg/json/JSONObject;

    .prologue
    .line 339
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/JsonCompleteRequest;

    invoke-direct {v0, p1, p2, p3}, Lcom/microsoft/xbox/smartglass/controls/JsonCompleteRequest;-><init>(ILjava/lang/String;Lorg/json/JSONObject;)V

    .line 340
    .local v0, "response":Lcom/microsoft/xbox/smartglass/controls/JsonCompleteRequest;
    const-string v1, "XbcCompleteRequest"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/controls/JsonCompleteRequest;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->invokeScript(Ljava/lang/String;[Ljava/lang/String;)V

    .line 341
    return-void
.end method

.method private fireReady()V
    .locals 10

    .prologue
    .line 363
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_isReady:Z

    .line 366
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->cloneListeners()Ljava/util/HashSet;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/smartglass/controls/WebViewListener;

    .line 367
    .local v5, "listener":Lcom/microsoft/xbox/smartglass/controls/WebViewListener;
    invoke-virtual {v5}, Lcom/microsoft/xbox/smartglass/controls/WebViewListener;->onReady()V

    goto :goto_0

    .line 371
    .end local v5    # "listener":Lcom/microsoft/xbox/smartglass/controls/WebViewListener;
    :cond_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 372
    .local v3, "jsonObject":Lorg/json/JSONObject;
    iget-object v7, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_components:Ljava/util/HashMap;

    monitor-enter v7

    .line 373
    :try_start_0
    iget-object v6, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_components:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 374
    .local v4, "key":Ljava/lang/String;
    iget-object v8, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_components:Ljava/util/HashMap;

    invoke-virtual {v8, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/controls/WebComponent;

    .line 375
    .local v0, "component":Lcom/microsoft/xbox/smartglass/controls/WebComponent;
    invoke-interface {v0}, Lcom/microsoft/xbox/smartglass/controls/WebComponent;->getCurrentState()Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 376
    .local v1, "currentState":Lorg/json/JSONObject;
    if-eqz v1, :cond_1

    .line 378
    :try_start_1
    invoke-virtual {v3, v4, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 379
    :catch_0
    move-exception v8

    goto :goto_1

    .line 383
    .end local v0    # "component":Lcom/microsoft/xbox/smartglass/controls/WebComponent;
    .end local v1    # "currentState":Lorg/json/JSONObject;
    .end local v4    # "key":Ljava/lang/String;
    :cond_2
    :try_start_2
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 385
    iget-object v7, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_additionalData:Ljava/util/HashMap;

    monitor-enter v7

    .line 386
    :try_start_3
    iget-object v6, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_additionalData:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 388
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/json/JSONObject;>;"
    :try_start_4
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v3, v6, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    .line 389
    :catch_1
    move-exception v6

    goto :goto_2

    .line 383
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/json/JSONObject;>;"
    :catchall_0
    move-exception v6

    :try_start_5
    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v6

    .line 392
    :cond_3
    :try_start_6
    monitor-exit v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 395
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTraceLog()Lcom/microsoft/xbox/smartglass/TraceLog;

    move-result-object v6

    const-string v7, "[Xbc] Sending loaded event."

    sget-object v8, Lcom/microsoft/xbox/smartglass/TraceLogLevel;->Verbose:Lcom/microsoft/xbox/smartglass/TraceLogLevel;

    invoke-virtual {v6, v7, v8}, Lcom/microsoft/xbox/smartglass/TraceLog;->write(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/TraceLogLevel;)V

    .line 396
    const-string v6, "loaded"

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v6, v7}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->raiseEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    return-void

    .line 392
    :catchall_1
    move-exception v6

    :try_start_7
    monitor-exit v7
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw v6
.end method

.method private hasCanvasAncestor()Z
    .locals 4

    .prologue
    .line 537
    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 538
    .local v1, "parent":Landroid/view/ViewParent;
    :goto_0
    if-eqz v1, :cond_1

    .line 540
    move-object v0, v1

    check-cast v0, Landroid/view/View;

    move-object v2, v0

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    const-string v3, "canvas_layout"

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 541
    const/4 v2, 0x1

    .line 549
    .end local v1    # "parent":Landroid/view/ViewParent;
    :goto_1
    return v2

    .line 544
    .restart local v1    # "parent":Landroid/view/ViewParent;
    :cond_0
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 546
    .end local v1    # "parent":Landroid/view/ViewParent;
    :catch_0
    move-exception v2

    .line 549
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private initialize()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 141
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->isInEditMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 162
    :goto_0
    return-void

    .line 146
    :cond_0
    :try_start_0
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasHostInfo;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasHostInfo;-><init>(Ljava/util/UUID;)V

    iput-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_hostInfo:Lcom/microsoft/xbox/smartglass/controls/JsonCanvasHostInfo;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 150
    :goto_1
    iput-boolean v5, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_isDestroyed:Z

    .line 151
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;-><init>(Landroid/webkit/WebView;)V

    iput-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_scriptRunner:Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    .line 153
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView$ViewClient;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView$ViewClient;-><init>(Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;)V

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 154
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView$ChromeClient;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView$ChromeClient;-><init>(Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;)V

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 155
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    .line 156
    .local v1, "webSettings":Landroid/webkit/WebSettings;
    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 157
    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setDatabaseEnabled(Z)V

    .line 158
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "database"

    invoke-virtual {v2, v3, v5}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 159
    .local v0, "databasePath":Ljava/lang/String;
    invoke-virtual {v1, v0}, Landroid/webkit/WebSettings;->setDatabasePath(Ljava/lang/String;)V

    .line 160
    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 161
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView$JavaScriptBridge;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView$JavaScriptBridge;-><init>(Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;)V

    const-string v3, "smartglass"

    invoke-virtual {p0, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 147
    .end local v0    # "databasePath":Ljava/lang/String;
    .end local v1    # "webSettings":Landroid/webkit/WebSettings;
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method private varargs invokeScriptInternal(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 7
    .param p1, "scriptName"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/String;

    .prologue
    .line 248
    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_invokeScriptLock:Ljava/lang/Object;

    monitor-enter v4

    .line 249
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_scriptRunner:Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    if-eqz v3, :cond_2

    .line 250
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "javascript:"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 251
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    const-string v3, "("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_0

    .line 256
    const-string v3, "\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    aget-object v3, p2, v0

    const-string v5, "\\"

    const-string v6, "\\\\"

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "\""

    const-string v6, "\\\""

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    const-string v3, "\","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 262
    :cond_0
    array-length v3, p2

    if-lez v3, :cond_1

    .line 263
    const-string v3, "\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    array-length v3, p2

    add-int/lit8 v3, v3, -0x1

    aget-object v3, p2, v3

    const-string v5, "\\"

    const-string v6, "\\\\"

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "\""

    const-string v6, "\\\""

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    const-string v3, "\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    :cond_1
    const-string v3, ");"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/RunnableScript;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/microsoft/xbox/smartglass/controls/RunnableScript;-><init>(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 273
    .local v2, "script":Lcom/microsoft/xbox/smartglass/controls/RunnableScript;
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_scriptRunner:Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->run(Lcom/microsoft/xbox/smartglass/controls/RunnableScript;)V

    .line 275
    .end local v0    # "i":I
    .end local v1    # "sb":Ljava/lang/StringBuilder;
    .end local v2    # "script":Lcom/microsoft/xbox/smartglass/controls/RunnableScript;
    :cond_2
    monitor-exit v4

    .line 276
    return-void

    .line 275
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method private log(ILcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;)V
    .locals 5
    .param p1, "requestId"    # I
    .param p2, "content"    # Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;

    .prologue
    .line 477
    :try_start_0
    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/JsonLogMessage;

    iget-object v2, p2, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->args:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/smartglass/controls/JsonLogMessage;-><init>(Ljava/lang/String;)V

    .line 478
    .local v1, "message":Lcom/microsoft/xbox/smartglass/controls/JsonLogMessage;
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTraceLog()Lcom/microsoft/xbox/smartglass/TraceLog;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Xbc] "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Lcom/microsoft/xbox/smartglass/controls/JsonLogMessage;->message:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/smartglass/TraceLogLevel;->Information:Lcom/microsoft/xbox/smartglass/TraceLogLevel;

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/TraceLog;->write(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/TraceLogLevel;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 482
    .end local v1    # "message":Lcom/microsoft/xbox/smartglass/controls/JsonLogMessage;
    :goto_0
    return-void

    .line 479
    :catch_0
    move-exception v0

    .line 480
    .local v0, "e":Lorg/json/JSONException;
    const-string v2, "Failed to parse log message"

    invoke-virtual {p0, p1, v2}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->failRequest(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private onLoadCompleted()V
    .locals 3

    .prologue
    .line 503
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->cloneListeners()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/controls/WebViewListener;

    .line 504
    .local v0, "listener":Lcom/microsoft/xbox/smartglass/controls/WebViewListener;
    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/controls/WebViewListener;->onLoadCompleted()V

    goto :goto_0

    .line 506
    .end local v0    # "listener":Lcom/microsoft/xbox/smartglass/controls/WebViewListener;
    :cond_0
    return-void
.end method

.method private onNavigating(Ljava/lang/String;)V
    .locals 6
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 486
    iget-object v5, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_components:Ljava/util/HashMap;

    monitor-enter v5

    .line 487
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_components:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 488
    .local v1, "components":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/smartglass/controls/WebComponent;>;"
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 490
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/controls/WebComponent;

    .line 491
    .local v0, "component":Lcom/microsoft/xbox/smartglass/controls/WebComponent;
    instance-of v5, v0, Lcom/microsoft/xbox/smartglass/controls/NavigationComponent;

    if-eqz v5, :cond_0

    move-object v3, v0

    .line 492
    check-cast v3, Lcom/microsoft/xbox/smartglass/controls/NavigationComponent;

    .line 493
    .local v3, "navComponent":Lcom/microsoft/xbox/smartglass/controls/NavigationComponent;
    invoke-interface {v3, p1}, Lcom/microsoft/xbox/smartglass/controls/NavigationComponent;->onNavigate(Ljava/lang/String;)V

    goto :goto_0

    .line 488
    .end local v0    # "component":Lcom/microsoft/xbox/smartglass/controls/WebComponent;
    .end local v1    # "components":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/smartglass/controls/WebComponent;>;"
    .end local v3    # "navComponent":Lcom/microsoft/xbox/smartglass/controls/NavigationComponent;
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 497
    .restart local v1    # "components":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/smartglass/controls/WebComponent;>;"
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->cloneListeners()Ljava/util/HashSet;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/smartglass/controls/WebViewListener;

    .line 498
    .local v2, "listener":Lcom/microsoft/xbox/smartglass/controls/WebViewListener;
    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/smartglass/controls/WebViewListener;->onNavigating(Ljava/lang/String;)V

    goto :goto_1

    .line 500
    .end local v2    # "listener":Lcom/microsoft/xbox/smartglass/controls/WebViewListener;
    :cond_2
    return-void
.end method

.method private onNavigationFailed(Ljava/lang/String;ILjava/lang/String;)V
    .locals 3
    .param p1, "failingUrl"    # Ljava/lang/String;
    .param p2, "errorCode"    # I
    .param p3, "description"    # Ljava/lang/String;

    .prologue
    .line 530
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->cloneListeners()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/controls/WebViewListener;

    .line 531
    .local v0, "listener":Lcom/microsoft/xbox/smartglass/controls/WebViewListener;
    invoke-virtual {v0, p1, p2, p3}, Lcom/microsoft/xbox/smartglass/controls/WebViewListener;->onNavigationFailed(Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 533
    .end local v0    # "listener":Lcom/microsoft/xbox/smartglass/controls/WebViewListener;
    :cond_0
    return-void
.end method

.method private onScriptNotify(Ljava/lang/String;)V
    .locals 9
    .param p1, "invokeString"    # Ljava/lang/String;

    .prologue
    .line 400
    const/4 v0, 0x0

    .line 403
    .local v0, "content":Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;
    :try_start_0
    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;

    invoke-direct {v1, p1}, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 411
    .end local v0    # "content":Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;
    .local v1, "content":Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;
    :try_start_1
    iget-object v4, v1, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->key:Ljava/util/UUID;

    if-nez v4, :cond_0

    .line 412
    const-string v4, "Invalid request key."

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->raiseError(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    .line 437
    .end local v1    # "content":Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;
    .restart local v0    # "content":Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;
    :goto_0
    return-void

    .line 404
    :catch_0
    move-exception v2

    .line 405
    .local v2, "e":Lorg/json/JSONException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid request. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->raiseError(Ljava/lang/String;)V

    goto :goto_0

    .line 417
    .end local v0    # "content":Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;
    .end local v2    # "e":Lorg/json/JSONException;
    .restart local v1    # "content":Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;
    :cond_0
    :try_start_2
    iget-object v4, v1, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->className:Ljava/lang/String;

    const-string v5, "Canvas"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 418
    iget v4, v1, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->id:I

    invoke-direct {p0, v4, v1}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->HandleBridgeMethod(ILcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;)V

    :goto_1
    move-object v0, v1

    .line 437
    .end local v1    # "content":Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;
    .restart local v0    # "content":Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;
    goto :goto_0

    .line 420
    .end local v0    # "content":Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;
    .restart local v1    # "content":Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;
    :cond_1
    iget-boolean v4, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_isReady:Z

    if-nez v4, :cond_2

    .line 421
    iget v4, v1, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->id:I

    const-string v5, "Method called too early; wait for loaded event."

    invoke-virtual {p0, v4, v5}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->failRequest(ILjava/lang/String;)V

    move-object v0, v1

    .line 422
    .end local v1    # "content":Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;
    .restart local v0    # "content":Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;
    goto :goto_0

    .line 426
    .end local v0    # "content":Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;
    .restart local v1    # "content":Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;
    :cond_2
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_components:Ljava/util/HashMap;

    iget-object v5, v1, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->className:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 427
    iget v4, v1, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->id:I

    const-string v5, "%s is not a valid component name."

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, v1, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->className:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->failRequest(ILjava/lang/String;)V

    move-object v0, v1

    .line 428
    .end local v1    # "content":Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;
    .restart local v0    # "content":Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;
    goto :goto_0

    .line 431
    .end local v0    # "content":Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;
    .restart local v1    # "content":Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;
    :cond_3
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_components:Ljava/util/HashMap;

    iget-object v5, v1, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->className:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/smartglass/controls/WebComponent;

    .line 432
    .local v3, "webComponent":Lcom/microsoft/xbox/smartglass/controls/WebComponent;
    iget v4, v1, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->id:I

    iget-object v5, v1, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->methodName:Ljava/lang/String;

    iget-object v6, v1, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->args:Ljava/lang/String;

    invoke-interface {v3, v4, v5, v6}, Lcom/microsoft/xbox/smartglass/controls/WebComponent;->invoke(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 434
    .end local v3    # "webComponent":Lcom/microsoft/xbox/smartglass/controls/WebComponent;
    :catch_1
    move-exception v2

    .line 435
    .local v2, "e":Ljava/lang/Exception;
    iget v4, v1, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->id:I

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->failRequest(ILjava/lang/String;)V

    goto :goto_1
.end method

.method private setClientInfo(ILcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;)V
    .locals 7
    .param p1, "requestId"    # I
    .param p2, "content"    # Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;

    .prologue
    .line 451
    :try_start_0
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasClientInfo;

    iget-object v3, p2, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->args:Ljava/lang/String;

    invoke-direct {v0, v3}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasClientInfo;-><init>(Ljava/lang/String;)V

    .line 452
    .local v0, "clientInfo":Lcom/microsoft/xbox/smartglass/controls/JsonCanvasClientInfo;
    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasClientInfo;->getVersion()Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;

    move-result-object v2

    .line 455
    .local v2, "newVersion":Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_version:Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_version:Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;->equals(Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 456
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Using multiple script versions is not supported. Script version was "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_version:Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and is now "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->raiseError(Ljava/lang/String;)V

    .line 461
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_version:Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;

    if-nez v3, :cond_2

    sget-boolean v3, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->IsSmartGlassStudioRunning:Z

    if-eqz v3, :cond_1

    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->hasCanvasAncestor()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 462
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v3

    const-string v4, "ScriptVersion"

    sget-object v5, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->Origin:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/microsoft/xbox/smartglass/MetricsManager;->recordEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    :cond_2
    iput-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_version:Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;

    .line 466
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTraceLog()Lcom/microsoft/xbox/smartglass/TraceLog;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[Xbc] Client version set to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_version:Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/smartglass/TraceLogLevel;->Verbose:Lcom/microsoft/xbox/smartglass/TraceLogLevel;

    invoke-virtual {v3, v4, v5}, Lcom/microsoft/xbox/smartglass/TraceLog;->write(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/TraceLogLevel;)V

    .line 468
    iget v3, p2, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->id:I

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->completeRequest(I)V

    .line 469
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->fireReady()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 473
    .end local v0    # "clientInfo":Lcom/microsoft/xbox/smartglass/controls/JsonCanvasClientInfo;
    .end local v2    # "newVersion":Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;
    :goto_0
    return-void

    .line 470
    :catch_0
    move-exception v1

    .line 471
    .local v1, "e":Lorg/json/JSONException;
    iget v3, p2, Lcom/microsoft/xbox/smartglass/controls/JsonScriptNotify;->id:I

    const-string v4, "Failed to parse client information."

    invoke-virtual {p0, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->failRequest(ILjava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public addAdditionalData(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 2
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Lorg/json/JSONObject;

    .prologue
    .line 134
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_additionalData:Ljava/util/HashMap;

    monitor-enter v1

    .line 135
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_additionalData:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    monitor-exit v1

    .line 137
    return-void

    .line 136
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addComponent(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/WebComponent;)V
    .locals 2
    .param p1, "componentName"    # Ljava/lang/String;
    .param p2, "component"    # Lcom/microsoft/xbox/smartglass/controls/WebComponent;

    .prologue
    .line 197
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_components:Ljava/util/HashMap;

    monitor-enter v1

    .line 198
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_components:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    invoke-interface {p2, p0}, Lcom/microsoft/xbox/smartglass/controls/WebComponent;->initialize(Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;)V

    .line 201
    return-void

    .line 199
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public addListener(Lcom/microsoft/xbox/smartglass/controls/WebViewListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/smartglass/controls/WebViewListener;

    .prologue
    .line 103
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_listeners:Ljava/util/HashSet;

    monitor-enter v1

    .line 104
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_listeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 105
    monitor-exit v1

    .line 106
    return-void

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clearComponents()V
    .locals 4

    .prologue
    .line 205
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_components:Ljava/util/HashMap;

    monitor-enter v2

    .line 206
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_components:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/controls/WebComponent;

    .line 207
    .local v0, "component":Lcom/microsoft/xbox/smartglass/controls/WebComponent;
    invoke-interface {v0}, Lcom/microsoft/xbox/smartglass/controls/WebComponent;->dispose()V

    goto :goto_0

    .line 210
    .end local v0    # "component":Lcom/microsoft/xbox/smartglass/controls/WebComponent;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 209
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_components:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 210
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 211
    return-void
.end method

.method public completeRequest(I)V
    .locals 2
    .param p1, "requestId"    # I

    .prologue
    .line 303
    const-string v0, "done"

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->completeRequestInternal(ILjava/lang/String;Lorg/json/JSONObject;)V

    .line 304
    return-void
.end method

.method public completeRequest(ILorg/json/JSONObject;)V
    .locals 1
    .param p1, "requestId"    # I
    .param p2, "jsonResponse"    # Lorg/json/JSONObject;

    .prologue
    .line 313
    const-string v0, "done"

    invoke-direct {p0, p1, v0, p2}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->completeRequestInternal(ILjava/lang/String;Lorg/json/JSONObject;)V

    .line 314
    return-void
.end method

.method public destroy()V
    .locals 2

    .prologue
    .line 166
    sget-object v1, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_destroyLock:Ljava/lang/Object;

    monitor-enter v1

    .line 167
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_isDestroyed:Z

    .line 168
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->onPause()V

    .line 172
    sget-object v1, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_invokeScriptLock:Ljava/lang/Object;

    monitor-enter v1

    .line 173
    :try_start_1
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_scriptRunner:Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->stop()V

    .line 174
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_scriptRunner:Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    .line 175
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 176
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 177
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->removeAllViews()V

    .line 178
    invoke-super {p0}, Landroid/webkit/WebView;->destroy()V

    .line 179
    return-void

    .line 168
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 175
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public failRequest(ILjava/lang/String;)V
    .locals 4
    .param p1, "requestId"    # I
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 323
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTraceLog()Lcom/microsoft/xbox/smartglass/TraceLog;

    move-result-object v0

    const-string v1, "[Xbc] %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/smartglass/TraceLogLevel;->Error:Lcom/microsoft/xbox/smartglass/TraceLogLevel;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/smartglass/TraceLog;->write(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/TraceLogLevel;)V

    .line 324
    const-string v0, "error"

    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/JsonError;

    invoke-direct {v1, p2}, Lcom/microsoft/xbox/smartglass/controls/JsonError;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->completeRequestInternal(ILjava/lang/String;Lorg/json/JSONObject;)V

    .line 325
    return-void
.end method

.method public failRequest(ILorg/json/JSONObject;)V
    .locals 5
    .param p1, "requestId"    # I
    .param p2, "jsonResponse"    # Lorg/json/JSONObject;

    .prologue
    .line 334
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTraceLog()Lcom/microsoft/xbox/smartglass/TraceLog;

    move-result-object v0

    const-string v1, "[Xbc] %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/smartglass/controls/Json;->escapeJson(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/smartglass/TraceLogLevel;->Error:Lcom/microsoft/xbox/smartglass/TraceLogLevel;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/smartglass/TraceLog;->write(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/TraceLogLevel;)V

    .line 335
    const-string v0, "error"

    invoke-direct {p0, p1, v0, p2}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->completeRequestInternal(ILjava/lang/String;Lorg/json/JSONObject;)V

    .line 336
    return-void
.end method

.method public getVersion()Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_version:Lcom/microsoft/xbox/smartglass/controls/WebComponentVersion;

    return-object v0
.end method

.method public varargs invokeScript(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p1, "scriptName"    # Ljava/lang/String;
    .param p2, "args"    # [Ljava/lang/String;

    .prologue
    .line 240
    iget-boolean v0, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_isReady:Z

    if-nez v0, :cond_0

    .line 245
    :goto_0
    return-void

    .line 244
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->invokeScriptInternal(Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0
.end method

.method public loadUrl(Ljava/lang/String;)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 91
    sget-object v1, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_destroyLock:Ljava/lang/Object;

    monitor-enter v1

    .line 92
    :try_start_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_isDestroyed:Z

    if-nez v0, :cond_0

    .line 93
    invoke-super {p0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 95
    :cond_0
    monitor-exit v1

    .line 96
    return-void

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public navigate(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 359
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->loadUrl(Ljava/lang/String;)V

    .line 360
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 215
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 216
    .local v2, "proposedHeight":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->getHeight()I

    move-result v0

    .line 218
    .local v0, "actualHeight":I
    sub-int v1, v2, v0

    .line 219
    .local v1, "diffHeight":I
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const/16 v4, 0x78

    if-le v3, v4, :cond_0

    .line 220
    if-lez v1, :cond_1

    .line 221
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_scriptRunner:Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    if-eqz v3, :cond_0

    .line 222
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_scriptRunner:Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    invoke-virtual {v3}, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->onKeyboardDismissed()V

    .line 231
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebView;->onMeasure(II)V

    .line 232
    return-void

    .line 225
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_scriptRunner:Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    if-eqz v3, :cond_0

    .line 226
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_scriptRunner:Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    invoke-virtual {v3}, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->onKeyboardShown()V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 521
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    .line 522
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->getScrollX()I

    move-result v0

    .line 523
    .local v0, "x":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->getScrollY()I

    move-result v1

    .line 524
    .local v1, "y":I
    add-int/lit8 v2, v0, -0x1

    add-int/lit8 v3, v1, -0x1

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->onScrollChanged(IIII)V

    .line 526
    .end local v0    # "x":I
    .end local v1    # "y":I
    :cond_0
    invoke-super {p0, p1}, Landroid/webkit/WebView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    return v2
.end method

.method public raiseError(Ljava/lang/String;)V
    .locals 5
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 349
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/JsonError;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/smartglass/controls/JsonError;-><init>(Ljava/lang/String;)V

    .line 350
    .local v0, "json":Lorg/json/JSONObject;
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTraceLog()Lcom/microsoft/xbox/smartglass/TraceLog;

    move-result-object v1

    const-string v2, "[Xbc] %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/smartglass/TraceLogLevel;->Error:Lcom/microsoft/xbox/smartglass/TraceLogLevel;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/TraceLog;->write(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/TraceLogLevel;)V

    .line 351
    const-string v1, "error"

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->raiseEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    return-void
.end method

.method public raiseEvent(Ljava/lang/String;)V
    .locals 3
    .param p1, "eventName"    # Ljava/lang/String;

    .prologue
    .line 284
    const-string v0, "XbcOn"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->invokeScript(Ljava/lang/String;[Ljava/lang/String;)V

    .line 285
    return-void
.end method

.method public raiseEvent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "eventName"    # Ljava/lang/String;
    .param p2, "json"    # Ljava/lang/String;

    .prologue
    .line 294
    const-string v0, "XbcOn"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->invokeScript(Ljava/lang/String;[Ljava/lang/String;)V

    .line 295
    return-void
.end method

.method public removeListener(Lcom/microsoft/xbox/smartglass/controls/WebViewListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/smartglass/controls/WebViewListener;

    .prologue
    .line 113
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_listeners:Ljava/util/HashSet;

    monitor-enter v1

    .line 114
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/BridgedWebView;->_listeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 115
    monitor-exit v1

    .line 116
    return-void

    .line 115
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
