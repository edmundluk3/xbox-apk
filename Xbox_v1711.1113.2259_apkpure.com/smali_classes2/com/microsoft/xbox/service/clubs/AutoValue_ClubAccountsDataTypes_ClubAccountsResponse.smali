.class final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;
.super Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;
.source "AutoValue_ClubAccountsDataTypes_ClubAccountsResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/AutoValue_ClubAccountsDataTypes_ClubAccountsResponse$GsonTypeAdapter;
    }
.end annotation


# direct methods
.method constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;Ljava/lang/String;ZLjava/util/Date;Lcom/google/common/collect/ImmutableList;)V
    .locals 1
    .param p1, "code"    # I
    .param p2, "description"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "owner"    # Ljava/lang/String;
    .param p5, "id"    # J
    .param p7, "type"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    .param p8, "created"    # Ljava/lang/String;
    .param p9, "canDeleteImmediately"    # Z
    .param p10, "suspensionRequiredAfter"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Date;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p11, "suspensions":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspension;>;"
    invoke-direct/range {p0 .. p11}, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubAccountsDataTypes_ClubAccountsResponse;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;Ljava/lang/String;ZLjava/util/Date;Lcom/google/common/collect/ImmutableList;)V

    .line 24
    return-void
.end method
