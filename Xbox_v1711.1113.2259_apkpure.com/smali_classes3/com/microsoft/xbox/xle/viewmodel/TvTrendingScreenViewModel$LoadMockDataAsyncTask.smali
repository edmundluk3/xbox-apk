.class Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$LoadMockDataAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "TvTrendingScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadMockDataAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$LoadMockDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$1;

    .prologue
    .line 154
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$LoadMockDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 157
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 158
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 186
    const-wide/16 v0, 0x2bc

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$LoadMockDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;)V

    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$LoadMockDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->access$400(Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;)V

    .line 191
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0

    .line 187
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$LoadMockDataAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 180
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$LoadMockDataAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 163
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 164
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$LoadMockDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 165
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$LoadMockDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 176
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 154
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$LoadMockDataAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 169
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 170
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel$LoadMockDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;->access$202(Lcom/microsoft/xbox/xle/viewmodel/TvTrendingScreenViewModel;Z)Z

    .line 171
    return-void
.end method
