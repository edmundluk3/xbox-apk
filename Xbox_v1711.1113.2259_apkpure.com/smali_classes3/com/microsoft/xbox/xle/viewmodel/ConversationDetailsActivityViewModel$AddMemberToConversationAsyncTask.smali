.class Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ConversationDetailsActivityViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AddMemberToConversationAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final item:Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;)V
    .locals 0
    .param p1    # Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "item"    # Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    .prologue
    .line 1046
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 1047
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1048
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;->item:Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    .line 1049
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 1053
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 1054
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1081
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 1083
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;->item:Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    iget-object v2, v2, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->xuid:Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    iget-object v3, v3, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v1, v4, v2, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->addMemberToConversation(ZLjava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 1085
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    iget-object v2, v2, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v4, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->loadSkypeConversationMessages(Ljava/lang/String;ZZ)Lcom/microsoft/xbox/toolkit/AsyncResult;

    .line 1087
    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1042
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 1076
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1042
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 3

    .prologue
    .line 1059
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 1060
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;->item:Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;)V

    .line 1061
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 1071
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;->item:Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->access$700(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;)V

    .line 1072
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1042
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 1065
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 1066
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->updateAdapter()V

    .line 1067
    return-void
.end method
