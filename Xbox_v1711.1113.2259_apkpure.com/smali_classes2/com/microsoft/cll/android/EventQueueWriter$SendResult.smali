.class final enum Lcom/microsoft/cll/android/EventQueueWriter$SendResult;
.super Ljava/lang/Enum;
.source "EventQueueWriter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/cll/android/EventQueueWriter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "SendResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/cll/android/EventQueueWriter$SendResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

.field public static final enum BAD_TOKEN:Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

.field public static final enum ERROR:Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

.field public static final enum SUCCESS:Lcom/microsoft/cll/android/EventQueueWriter$SendResult;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 324
    new-instance v0, Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/cll/android/EventQueueWriter$SendResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/cll/android/EventQueueWriter$SendResult;->SUCCESS:Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

    .line 325
    new-instance v0, Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

    const-string v1, "BAD_TOKEN"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/cll/android/EventQueueWriter$SendResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/cll/android/EventQueueWriter$SendResult;->BAD_TOKEN:Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

    .line 326
    new-instance v0, Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/cll/android/EventQueueWriter$SendResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/cll/android/EventQueueWriter$SendResult;->ERROR:Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

    .line 323
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

    sget-object v1, Lcom/microsoft/cll/android/EventQueueWriter$SendResult;->SUCCESS:Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/cll/android/EventQueueWriter$SendResult;->BAD_TOKEN:Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/cll/android/EventQueueWriter$SendResult;->ERROR:Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/cll/android/EventQueueWriter$SendResult;->$VALUES:[Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 323
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/cll/android/EventQueueWriter$SendResult;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 323
    const-class v0, Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/cll/android/EventQueueWriter$SendResult;
    .locals 1

    .prologue
    .line 323
    sget-object v0, Lcom/microsoft/cll/android/EventQueueWriter$SendResult;->$VALUES:[Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

    invoke-virtual {v0}, [Lcom/microsoft/cll/android/EventQueueWriter$SendResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/cll/android/EventQueueWriter$SendResult;

    return-object v0
.end method
