.class public Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;
.source "HomeScreenPinsViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithPosition;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$AddPinTask;,
        Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final activeAddTasks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$AddPinTask;",
            ">;"
        }
    .end annotation
.end field

.field private final activeRemoveTasks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;",
            ">;"
        }
    .end annotation
.end field

.field private adpViewScrollState:Lcom/microsoft/xbox/toolkit/ui/ScrollState;

.field private final containerResourceId:I

.field private position:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 2
    .param p1, "containerResourceId"    # I
    .param p2, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;-><init>()V

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->activeRemoveTasks:Ljava/util/HashMap;

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->activeAddTasks:Ljava/util/HashMap;

    .line 29
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    invoke-direct {v0, v1, v1}, Lcom/microsoft/xbox/toolkit/ui/ScrollState;-><init>(II)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->adpViewScrollState:Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    .line 32
    iput p1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->containerResourceId:I

    .line 33
    iput p2, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->position:I

    .line 34
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;
    .param p2, "x2"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->onPinRemoved(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$AddPinTask;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$AddPinTask;
    .param p2, "x2"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->onPinAdded(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$AddPinTask;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method private onPinAdded(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$AddPinTask;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "t"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$AddPinTask;
    .param p2, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 131
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AddPinTask status "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    iget-object v0, p1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$AddPinTask;->pin:Lcom/microsoft/xbox/service/model/pins/PinItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getProviderTitleId()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher;->trackPinToggleAction(JZ)V

    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->activeAddTasks:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$AddPinTask;->pin:Lcom/microsoft/xbox/service/model/pins/PinItem;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    .line 142
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->updateAdapter()V

    .line 143
    return-void
.end method

.method private onPinRemoved(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "t"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;
    .param p2, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 116
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RemovePinTask status "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    iget-object v0, p1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;->pin:Lcom/microsoft/xbox/service/model/pins/PinItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getProviderTitleId()J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLauncher;->trackPinToggleAction(JZ)V

    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->activeRemoveTasks:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;->pin:Lcom/microsoft/xbox/service/model/pins/PinItem;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    .line 127
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->updateAdapter()V

    .line 128
    return-void
.end method

.method private pinWithProvider(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)V
    .locals 6
    .param p1, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
    .param p2, "mi"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .param p3, "provider"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    .prologue
    .line 151
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getActionMenuDialog()Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->dismiss()V

    .line 152
    invoke-static {p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->fromMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)Lcom/microsoft/xbox/service/model/pins/PinItem;

    move-result-object v0

    .line 154
    .local v0, "pi":Lcom/microsoft/xbox/service/model/pins/PinItem;
    invoke-interface {p1}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getProviderTitleIdString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->prepend0xIfNecessary(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/microsoft/xbox/service/model/pins/PinItem;->Provider:Ljava/lang/String;

    .line 155
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->activeAddTasks:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 156
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$AddPinTask;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$AddPinTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;Lcom/microsoft/xbox/service/model/pins/PinItem;)V

    .line 157
    .local v1, "t":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$AddPinTask;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->activeAddTasks:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$AddPinTask;->load(Z)V

    .line 162
    .end local v1    # "t":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$AddPinTask;
    :goto_0
    return-void

    .line 160
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Already pinning "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getItemId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/pins/PinItem;->getProviderTitleId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static prepend0xIfNecessary(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 165
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_1

    .line 167
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x30

    if-eq v1, v2, :cond_1

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .local v0, "secondChar":C
    const/16 v1, 0x78

    if-ne v0, v1, :cond_0

    const/16 v1, 0x58

    if-eq v0, v1, :cond_1

    .line 168
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 171
    .end local v0    # "secondChar":C
    :cond_1
    return-object p0
.end method


# virtual methods
.method public addPin(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 3
    .param p1, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
    .param p2, "mi"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 95
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 96
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 98
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->activeAddTasks:Ljava/util/HashMap;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 99
    invoke-interface {p1}, Lcom/microsoft/xbox/service/model/pins/LaunchableItem;->getProvider()Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    move-result-object v0

    .line 100
    .local v0, "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    if-eqz v0, :cond_1

    .line 101
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->TAG:Ljava/lang/String;

    const-string v2, "has provider, just pin it with provider."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->pinWithProvider(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)V

    .line 109
    .end local v0    # "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    :cond_0
    :goto_0
    return-void

    .line 104
    .restart local v0    # "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->TAG:Ljava/lang/String;

    const-string v2, "pinning with null provider."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->pinWithProvider(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)V

    goto :goto_0
.end method

.method protected createAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPinsAdapterPhone;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V

    return-object v0
.end method

.method public getAdpViewScrollState()Lcom/microsoft/xbox/toolkit/ui/ScrollState;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->adpViewScrollState:Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    return-object v0
.end method

.method public getContainerResourceId()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->containerResourceId:I

    return v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->position:I

    return v0
.end method

.method public getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 0

    .prologue
    .line 47
    return-object p0
.end method

.method public navigateTo(Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/xle/app/activity/ActivityBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112
    .local p1, "destScreen":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/xle/app/activity/ActivityBase;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;Z)V

    .line 113
    return-void
.end method

.method public navigateToPin(Lcom/microsoft/xbox/service/model/pins/PinItem;)V
    .locals 2
    .param p1, "pi"    # Lcom/microsoft/xbox/service/model/pins/PinItem;

    .prologue
    .line 61
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 63
    .local v0, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v1, p1, Lcom/microsoft/xbox/service/model/pins/PinItem;->ItemId:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    .line 64
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getUseEDS31()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 65
    iget-object v1, p1, Lcom/microsoft/xbox/service/model/pins/PinItem;->ContentType:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    .line 69
    :goto_0
    iget-object v1, p1, Lcom/microsoft/xbox/service/model/pins/PinItem;->ImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setImageUrl(Ljava/lang/String;)V

    .line 70
    iget-object v1, p1, Lcom/microsoft/xbox/service/model/pins/PinItem;->Title:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Name:Ljava/lang/String;

    .line 71
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 72
    return-void

    .line 67
    :cond_0
    iget-object v1, p1, Lcom/microsoft/xbox/service/model/pins/PinItem;->ContentType:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->getValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    goto :goto_0
.end method

.method public removePin(Lcom/microsoft/xbox/service/model/pins/PinItem;)V
    .locals 2
    .param p1, "pin"    # Lcom/microsoft/xbox/service/model/pins/PinItem;

    .prologue
    .line 75
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->activeRemoveTasks:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 76
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;Lcom/microsoft/xbox/service/model/pins/PinItem;)V

    .line 77
    .local v0, "t":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->activeRemoveTasks:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;->load(Z)V

    .line 80
    .end local v0    # "t":Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel$RemovePinTask;
    :cond_0
    return-void
.end method

.method public setAdpVewScrollState(Lcom/microsoft/xbox/toolkit/ui/ScrollState;)V
    .locals 0
    .param p1, "state"    # Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->adpViewScrollState:Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    .line 88
    return-void
.end method

.method public setPosition(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 57
    iput p1, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;->position:I

    .line 58
    return-void
.end method
