.class public Lcom/microsoft/xbox/xle/app/activity/AppDetailOverviewScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "AppDetailOverviewScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    const-string v0, "AppDetailOverview"

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    .line 12
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 14
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/AppDetailOverviewScreen;->onCreateContentView()V

    .line 16
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    const/4 v2, 0x0

    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;

    invoke-direct {v3}, Lcom/microsoft/xbox/xle/viewmodel/AppDetailOverviewScreenViewModel;-><init>()V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;

    const v4, 0x7f0e01bb

    invoke-direct {v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;-><init>(I)V

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;-><init>([Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/AppDetailOverviewScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 17
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 21
    const v0, 0x7f030039

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/AppDetailOverviewScreen;->setContentView(I)V

    .line 22
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    return v0
.end method
