.class public final enum Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;
.super Ljava/lang/Enum;
.source "OOBESessionDataType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OffConsoleStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;

.field public static final enum Aborted:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;

.field public static final enum Completed:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;

.field public static final enum Error:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;

.field public static final enum InProgress:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 27
    new-instance v0, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;

    const-string v1, "InProgress"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;->InProgress:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;

    .line 28
    new-instance v0, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;

    const-string v1, "Completed"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;->Completed:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;

    .line 29
    new-instance v0, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;

    const-string v1, "Error"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;->Error:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;

    .line 30
    new-instance v0, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;

    const-string v1, "Aborted"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;->Aborted:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;

    .line 26
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;

    sget-object v1, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;->InProgress:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;->Completed:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;->Error:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;->Aborted:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;->$VALUES:[Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    const-class v0, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;->$VALUES:[Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;

    return-object v0
.end method
