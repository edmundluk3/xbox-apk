.class Lcom/microsoft/xbox/xle/app/ApplicationBarManager$11;
.super Ljava/lang/Object;
.source "ApplicationBarManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->onStateChanged(Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/ApplicationBarManager;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/ApplicationBarManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    .prologue
    .line 582
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$11;->this$0:Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 587
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$11;->this$0:Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getIconButtonContainer()Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 588
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$11;->this$0:Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->collapsedAppBarView:Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/appbar/ApplicationBarView;->getIconButtonContainer()Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 589
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$11;->this$0:Lcom/microsoft/xbox/xle/app/ApplicationBarManager;

    sget-object v1, Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;->DONE:Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/ApplicationBarManager;->access$200(Lcom/microsoft/xbox/xle/app/ApplicationBarManager;Lcom/microsoft/xbox/xle/app/ApplicationBarManager$AppBarAnimationState;)V

    .line 590
    return-void
.end method
