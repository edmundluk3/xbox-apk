.class Lcom/microsoft/xbox/service/model/TitleModel$LikeGameClipRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "TitleModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/TitleModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LikeGameClipRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;",
        ">;"
    }
.end annotation


# instance fields
.field private final clip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

.field private currLikeState:Z

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/TitleModel;

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/TitleModel;ZLcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;Ljava/lang/String;)V
    .locals 0
    .param p2, "currLikeState"    # Z
    .param p3, "clip"    # Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    .param p4, "xuid"    # Ljava/lang/String;

    .prologue
    .line 1577
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/TitleModel$LikeGameClipRunner;->this$0:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 1578
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/TitleModel$LikeGameClipRunner;->clip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .line 1579
    iput-boolean p2, p0, Lcom/microsoft/xbox/service/model/TitleModel$LikeGameClipRunner;->currLikeState:Z

    .line 1580
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/TitleModel$LikeGameClipRunner;->xuid:Ljava/lang/String;

    .line 1581
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1589
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getSendLikeClipUrlFormat()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/TitleModel$LikeGameClipRunner;->clip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    iget-object v6, v6, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->xuid:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/TitleModel$LikeGameClipRunner;->clip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    iget-object v6, v6, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->scid:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/TitleModel$LikeGameClipRunner;->clip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    iget-object v6, v6, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->gameClipId:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x3

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/TitleModel$LikeGameClipRunner;->xuid:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1590
    .local v0, "likeClipUrlPath":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v2

    iget-boolean v3, p0, Lcom/microsoft/xbox/service/model/TitleModel$LikeGameClipRunner;->currLikeState:Z

    invoke-interface {v2, v0, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->sendMeLike(Ljava/lang/String;Z)Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;

    move-result-object v1

    .line 1591
    .local v1, "likeResult":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    return-object v1
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1572
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/TitleModel$LikeGameClipRunner;->buildData()Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 1608
    const-wide/16 v0, 0xd1d

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1596
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1597
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel$LikeGameClipRunner;->clip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel$LikeGameClipRunner;->clip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v1, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    .line 1598
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel$LikeGameClipRunner;->clip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->isLiked:Z

    if-eqz v0, :cond_3

    .line 1599
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel$LikeGameClipRunner;->clip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget v1, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    .line 1604
    :cond_1
    :goto_1
    return-void

    .line 1597
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1601
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel$LikeGameClipRunner;->clip:Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;->socialInfo:Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;

    iget v1, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/microsoft/xbox/service/network/managers/SocialActionsSummariesContainer$Summary;->likeCount:I

    goto :goto_1
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 1585
    return-void
.end method
