.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;
.super Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
.source "EDSV2TVSeriesMediaItem.java"


# instance fields
.field public HasSeasons:Z

.field public Networks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/rawtypes/EDSV2Network;",
            ">;"
        }
    .end annotation
.end field

.field public ParentalRating:Ljava/lang/String;

.field public ParentalRatingSystem:Ljava/lang/String;

.field public RatingId:I

.field public ReviewSources:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;",
            ">;"
        }
    .end annotation
.end field

.field public SeasonCount:I

.field public ZuneId:Ljava/lang/String;

.field private lastetSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

.field private latestEpisode:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "LatestEpisode"
    .end annotation
.end field

.field private network:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 28
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 1
    .param p1, "source"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 32
    instance-of v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 33
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    iget v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->AllTimeAverageRating:F

    iput v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->AllTimeAverageRating:F

    move-object v0, p1

    .line 34
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->Genres:Ljava/util/List;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->Genres:Ljava/util/List;

    move-object v0, p1

    .line 35
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->HasSeasons:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->HasSeasons:Z

    move-object v0, p1

    .line 36
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->Networks:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->Networks:Ljava/util/ArrayList;

    move-object v0, p1

    .line 37
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->ParentalRating:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->ParentalRating:Ljava/lang/String;

    move-object v0, p1

    .line 38
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->ParentalRatingSystem:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->ParentalRatingSystem:Ljava/lang/String;

    move-object v0, p1

    .line 39
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    iget v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->RatingId:I

    iput v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->RatingId:I

    move-object v0, p1

    .line 40
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->latestEpisode:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->latestEpisode:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    move-object v0, p1

    .line 41
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->lastetSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->lastetSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-object v0, p1

    .line 42
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    iget v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->SeasonCount:I

    iput v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->SeasonCount:I

    .line 43
    check-cast p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    .end local p1    # "source":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->ZuneId:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->ZuneId:Ljava/lang/String;

    .line 45
    :cond_0
    const/16 v0, 0x3ec

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->setMediaItemTypeFromInt(I)V

    .line 46
    return-void
.end method


# virtual methods
.method public getLatestEpisode()Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->latestEpisode:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    return-object v0
.end method

.method public getMetaCriticReviewScore()F
    .locals 4

    .prologue
    .line 73
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->ReviewSources:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 74
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->ReviewSources:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;

    .line 75
    .local v0, "reviewSource":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;
    iget-object v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;->Name:Ljava/lang/String;

    const-string v3, "Metacritic"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 76
    iget v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;->ReviewScore:F

    .line 81
    .end local v0    # "reviewSource":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ReviewSource;
    :goto_0
    return v1

    :cond_1
    const/high16 v1, -0x40800000    # -1.0f

    goto :goto_0
.end method

.method public getNetwork()Ljava/lang/String;
    .locals 4

    .prologue
    .line 59
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->network:Ljava/lang/String;

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->Networks:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->Networks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 61
    .local v0, "builder":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->Networks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/rawtypes/EDSV2Network;

    .line 62
    .local v1, "item":Lcom/microsoft/xbox/service/model/edsv2/rawtypes/EDSV2Network;
    iget-object v3, v1, Lcom/microsoft/xbox/service/model/edsv2/rawtypes/EDSV2Network;->Name:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 65
    .end local v1    # "item":Lcom/microsoft/xbox/service/model/edsv2/rawtypes/EDSV2Network;
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->network:Ljava/lang/String;

    .line 68
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->network:Ljava/lang/String;

    return-object v2
.end method

.method public getParentalRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->ParentalRating:Ljava/lang/String;

    return-object v0
.end method

.method public getSeriesId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->ID:Ljava/lang/String;

    return-object v0
.end method

.method public getSeriesName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->Name:Ljava/lang/String;

    return-object v0
.end method

.method public getZuneId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->ZuneId:Ljava/lang/String;

    return-object v0
.end method

.method public setLastestEpisode(Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;)V
    .locals 0
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->latestEpisode:Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    .line 86
    return-void
.end method

.method public setLatestSeason(Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;)V
    .locals 0
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->lastetSeason:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 94
    return-void
.end method
