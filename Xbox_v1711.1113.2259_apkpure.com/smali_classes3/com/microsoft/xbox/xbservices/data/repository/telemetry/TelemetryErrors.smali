.class public final enum Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;
.super Ljava/lang/Enum;
.source "TelemetryErrors.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatBlocked:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatCurrentUserNotFound:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatDisconnectedUnintentionally:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatDoesntExist:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToChangeRestriction:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToHandleRTATapNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToInitialize:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToInvite:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToJoin:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToKick:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToLeave:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToLoadDetails:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToMute:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToReceiveMessageUserMissing:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToSendDataChannelMessageNoPeerConnection:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToSendTextMessage:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToSendTextMessageNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToSendTextMessageUserMissing:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToStart:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToUnmute:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToUpdateAbilityToBroadcastNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToUpdateBroadcastingStateNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToUpdateConnectionIdNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToUpdateJoinabilityNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToUpdateMultiplayerSessionNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToUpdateMultiplayerSessionRTATap:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToUpdateQOSMeasurementsNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToUpdateWebRTCConfigNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFailedToUploadPingTime:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatFull:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatGeneric:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatInvalidInvitation:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatInviteExceedsCapacity:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatLocalNetwork:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatNoActiveSessionAllocateCloudCompute:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatNoActiveSessionMemberStatusChange:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatNoActiveSessionUpdateRoster:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatUDPActiveError:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatUDPInitializationError:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatWebRTCClientError:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

.field public static final enum PartyChatXBLNetworking:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;


# instance fields
.field private code:Ljava/lang/String;

.field private description:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 11
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatBlocked"

    const-string v2, "Party - Party chat is blocked by settings or enforcement actions"

    const-string v3, "90010"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatBlocked:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 12
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFull"

    const-string v2, "Party - Party chat is full"

    const-string v3, "90011"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFull:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 13
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatXBLNetworking"

    const-string v2, "Party - Party chat service error"

    const-string v3, "90012"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatXBLNetworking:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 14
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatLocalNetwork"

    const-string v2, "Party - Party chat local network error"

    const-string v3, "90013"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatLocalNetwork:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 15
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatInvalidInvitation"

    const-string v2, "Party - Party chat invitaion is no longer valid"

    const-string v3, "90014"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatInvalidInvitation:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 16
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatInviteExceedsCapacity"

    const/4 v2, 0x5

    const-string v3, "Party - Party chat invite exceeds capacity"

    const-string v4, "90015"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatInviteExceedsCapacity:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 17
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToSendTextMessage"

    const/4 v2, 0x6

    const-string v3, "Party - Party chat failed to send text message"

    const-string v4, "90016"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToSendTextMessage:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 18
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToUploadPingTime"

    const/4 v2, 0x7

    const-string v3, "Party - Party chat failed to upload ping time"

    const-string v4, "90017"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUploadPingTime:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 19
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToInvite"

    const/16 v2, 0x8

    const-string v3, "Party - Party chat failed to invite"

    const-string v4, "90018"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToInvite:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 20
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToLoadDetails"

    const/16 v2, 0x9

    const-string v3, "Party - Party chat failed to load party details"

    const-string v4, "90019"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToLoadDetails:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 21
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToChangeRestriction"

    const/16 v2, 0xa

    const-string v3, "Party - Party chat failed to change party restriction"

    const-string v4, "90020"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToChangeRestriction:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 22
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToLeave"

    const/16 v2, 0xb

    const-string v3, "Party - Party chat failed to leave party"

    const-string v4, "90021"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToLeave:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 23
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToJoin"

    const/16 v2, 0xc

    const-string v3, "Party - Party - Party chat failed to join party"

    const-string v4, "90022"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToJoin:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 24
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatDoesntExist"

    const/16 v2, 0xd

    const-string v3, "Party - Party doesn\'t exist"

    const-string v4, "90023"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatDoesntExist:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 25
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToStart"

    const/16 v2, 0xe

    const-string v3, "Party - Party chat failed to start"

    const-string v4, "90024"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToStart:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 26
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToMute"

    const/16 v2, 0xf

    const-string v3, "Party - Party chat failed to mute user"

    const-string v4, "90025"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToMute:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 27
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToKick"

    const/16 v2, 0x10

    const-string v3, "Party - Party chat failed to kick user"

    const-string v4, "90026"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToKick:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 28
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatGeneric"

    const/16 v2, 0x11

    const-string v3, "Party - Party chat generic error"

    const-string v4, "90027"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatGeneric:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 29
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToInitialize"

    const/16 v2, 0x12

    const-string v3, "Party - Party chat failed to initialize"

    const-string v4, "90028"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToInitialize:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 30
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatNoActiveSession"

    const/16 v2, 0x13

    const-string v3, "Party - No active party"

    const-string v4, "90029"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 31
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatCurrentUserNotFound"

    const/16 v2, 0x14

    const-string v3, "Party - Could not find current user"

    const-string v4, "90030"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatCurrentUserNotFound:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 32
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToUnmute"

    const/16 v2, 0x15

    const-string v3, "Party - Party chat failed to unmute user"

    const-string v4, "90031"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUnmute:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 33
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToReceiveMessageUserMissing"

    const/16 v2, 0x16

    const-string v3, "Party - Failed to receive message.  Cannot find originator in existing roster"

    const-string v4, "90032"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToReceiveMessageUserMissing:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 34
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatNoActiveSessionUpdateRoster"

    const/16 v2, 0x17

    const-string v3, "Party - Failed to update roster; No active party"

    const-string v4, "90033"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatNoActiveSessionUpdateRoster:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 35
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatNoActiveSessionMemberStatusChange"

    const/16 v2, 0x18

    const-string v3, "Party - Failed to handle member status change; No active party"

    const-string v4, "90034"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatNoActiveSessionMemberStatusChange:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 36
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatNoActiveSessionAllocateCloudCompute"

    const/16 v2, 0x19

    const-string v3, "Party - Failed to allocate cloud compute; No active party"

    const-string v4, "90035"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatNoActiveSessionAllocateCloudCompute:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 37
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToSendTextMessageUserMissing"

    const/16 v2, 0x1a

    const-string v3, "Party - Failed to send message.  Cannot find sender in existing roster"

    const-string v4, "90036"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToSendTextMessageUserMissing:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 38
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToSendTextMessageNoActiveSession"

    const/16 v2, 0x1b

    const-string v3, "Party - Failed to send message.  No active party"

    const-string v4, "90037"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToSendTextMessageNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 39
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToUpdateMultiplayerSessionNoActiveSession"

    const/16 v2, 0x1c

    const-string v3, "Party - Failed to update multiplayer session.  No active party"

    const-string v4, "90038"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateMultiplayerSessionNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 40
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToUpdateMultiplayerSessionRTATap"

    const/16 v2, 0x1d

    const-string v3, "Party - Failed to update multiplayer session when handling RTA shoulder tap"

    const-string v4, "90039"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateMultiplayerSessionRTATap:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 41
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToHandleRTATapNoActiveSession"

    const/16 v2, 0x1e

    const-string v3, "Party - Failed to handle RTA shoulder tap.  No active party"

    const-string v4, "90040"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToHandleRTATapNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 42
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToUpdateQOSMeasurementsNoActiveSession"

    const/16 v2, 0x1f

    const-string v3, "Party - Failed to update QOS measurements. No active party"

    const-string v4, "90041"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateQOSMeasurementsNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 43
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToUpdateWebRTCConfigNoActiveSession"

    const/16 v2, 0x20

    const-string v3, "Party - Failed to update web RTC config. No active party"

    const-string v4, "90042"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateWebRTCConfigNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 44
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToUpdateConnectionIdNoActiveSession"

    const/16 v2, 0x21

    const-string v3, "Party - Failed to update connection ID for session. No active party"

    const-string v4, "90043"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateConnectionIdNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 45
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToUpdateJoinabilityNoActiveSession"

    const/16 v2, 0x22

    const-string v3, "Party - Failed to update joinability of party. No active party"

    const-string v4, "90044"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateJoinabilityNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 46
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToUpdateAbilityToBroadcastNoActiveSession"

    const/16 v2, 0x23

    const-string v3, "Party - Failed to update ability to broadcast. No active party"

    const-string v4, "90045"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateAbilityToBroadcastNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 47
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToUpdateBroadcastingStateNoActiveSession"

    const/16 v2, 0x24

    const-string v3, "Party - Failed to update broadcasting state. No active party"

    const-string v4, "90046"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateBroadcastingStateNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 48
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatFailedToSendDataChannelMessageNoPeerConnection"

    const/16 v2, 0x25

    const-string v3, "Party - Failed to send data channel message.  No peer connection"

    const-string v4, "90047"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToSendDataChannelMessageNoPeerConnection:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 49
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatDisconnectedUnintentionally"

    const/16 v2, 0x26

    const-string v3, "Party - Disconnected unintentionally.  Attempting to Reconnect"

    const-string v4, "90048"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatDisconnectedUnintentionally:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 50
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatWebRTCClientError"

    const/16 v2, 0x27

    const-string v3, "Party - WebRTC Client Error"

    const-string v4, "90049"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatWebRTCClientError:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 51
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatUDPActiveError"

    const/16 v2, 0x28

    const-string v3, "Party - UDP encountered an error while active"

    const-string v4, "90050"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatUDPActiveError:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 52
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    const-string v1, "PartyChatUDPInitializationError"

    const/16 v2, 0x29

    const-string v3, "Party - UDP encountered an error during initialization"

    const-string v4, "90051"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatUDPInitializationError:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    .line 10
    const/16 v0, 0x2a

    new-array v0, v0, [Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatBlocked:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFull:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatXBLNetworking:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v1, v0, v7

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatLocalNetwork:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v1, v0, v8

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatInvalidInvitation:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatInviteExceedsCapacity:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToSendTextMessage:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUploadPingTime:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToInvite:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToLoadDetails:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToChangeRestriction:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToLeave:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToJoin:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatDoesntExist:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToStart:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToMute:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToKick:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatGeneric:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToInitialize:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatCurrentUserNotFound:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUnmute:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToReceiveMessageUserMissing:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatNoActiveSessionUpdateRoster:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatNoActiveSessionMemberStatusChange:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatNoActiveSessionAllocateCloudCompute:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToSendTextMessageUserMissing:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToSendTextMessageNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateMultiplayerSessionNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateMultiplayerSessionRTATap:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToHandleRTATapNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateQOSMeasurementsNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateWebRTCConfigNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateConnectionIdNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateJoinabilityNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateAbilityToBroadcastNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToUpdateBroadcastingStateNoActiveSession:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatFailedToSendDataChannelMessageNoPeerConnection:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatDisconnectedUnintentionally:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatWebRTCClientError:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatUDPActiveError:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->PartyChatUDPInitializationError:Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->$VALUES:[Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "code"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 54
    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->description:Ljava/lang/String;

    .line 55
    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->code:Ljava/lang/String;

    .line 58
    iput-object p3, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->description:Ljava/lang/String;

    .line 59
    iput-object p4, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->code:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->$VALUES:[Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;

    return-object v0
.end method


# virtual methods
.method public getCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->code:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryErrors;->description:Ljava/lang/String;

    return-object v0
.end method
