.class public final enum Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;
.super Ljava/lang/Enum;
.source "ClubAccountsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ClubSuspensionActor"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;

.field public static final enum Enforcement:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "enforcement"
    .end annotation
.end field

.field public static final enum Owner:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "owner"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 236
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;

    const-string v1, "Owner"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;->Owner:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;

    .line 238
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;

    const-string v1, "Enforcement"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;->Enforcement:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;

    .line 235
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;->Owner:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;->Enforcement:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;

    aput-object v1, v0, v3

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 235
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 235
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;
    .locals 1

    .prologue
    .line 235
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;

    return-object v0
.end method
