.class public Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "PeopleHubAchievementsScreenAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private achievementsHashcode:I

.field private achievementsInvalidated:Z

.field private achievementsReady:Z

.field private final compositeAchievementResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;",
            ">;"
        }
    .end annotation
.end field

.field private final compositeLeaderboardResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;",
            ">;"
        }
    .end annotation
.end field

.field private final leaderboardViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private leaderboardsHashcode:I

.field private leaderboardsInvalidated:Z

.field private leaderboardsReady:Z

.field private final listAdapter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

.field private final peopleHubViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;)V
    .locals 3
    .param p1, "achievementsScreenViewModel"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 38
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->invalidateData()V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->compositeLeaderboardResults:Ljava/util/List;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->compositeAchievementResults:Ljava/util/List;

    .line 43
    const v0, 0x7f0e090c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->screenBody:Landroid/view/View;

    .line 45
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->peopleHubViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->peopleHubViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->getGamerscoreLeaderboardViewModel()Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->leaderboardViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    .line 48
    const v0, 0x7f0e090d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 50
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->peopleHubViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->leaderboardViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    .line 51
    const v0, 0x7f0e090e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 52
    return-void
.end method


# virtual methods
.method public invalidateData()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 55
    iput v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->leaderboardsHashcode:I

    .line 56
    iput v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->achievementsHashcode:I

    .line 57
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->leaderboardsReady:Z

    .line 58
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->achievementsReady:Z

    .line 59
    return-void
.end method

.method public updateViewOverride()V
    .locals 11

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 63
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->peopleHubViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isBusy()Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->leaderboardViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->isBusy()Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_0
    move v6, v8

    :goto_0
    invoke-virtual {p0, v6}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->updateLoadingIndicator(Z)V

    .line 65
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->peopleHubViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v6

    sget-object v9, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v6, v9, :cond_1

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->leaderboardViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v6

    sget-object v9, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v6, v9, :cond_3

    .line 67
    :cond_1
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v9, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v6, v9}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 77
    :goto_1
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->leaderboardViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->getLeaderboardList()Ljava/util/List;

    move-result-object v1

    .line 78
    .local v1, "gamerscoreLeaderboardListItem":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;>;"
    if-eqz v1, :cond_7

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v6

    iget v9, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->leaderboardsHashcode:I

    if-eq v6, v9, :cond_7

    .line 79
    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v6

    iput v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->leaderboardsHashcode:I

    .line 81
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->compositeLeaderboardResults:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 82
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->leaderboardViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->getLeaderboardList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;

    .line 83
    .local v2, "item":Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->compositeLeaderboardResults:Ljava/util/List;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;->leaderboardItem(Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .end local v1    # "gamerscoreLeaderboardListItem":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;>;"
    .end local v2    # "item":Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;
    :cond_2
    move v6, v7

    .line 63
    goto :goto_0

    .line 68
    :cond_3
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->peopleHubViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v6

    sget-object v9, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v6, v9, :cond_4

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->leaderboardViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v6

    sget-object v9, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v6, v9, :cond_5

    .line 70
    :cond_4
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v9, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v6, v9}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    goto :goto_1

    .line 73
    :cond_5
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->leaderboardViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    goto :goto_1

    .line 85
    .restart local v1    # "gamerscoreLeaderboardListItem":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardListItem;>;"
    :cond_6
    iput-boolean v8, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->leaderboardsReady:Z

    .line 86
    iput-boolean v8, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->leaderboardsInvalidated:Z

    .line 90
    :cond_7
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->peopleHubViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->getFilteredResults()Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_9

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->peopleHubViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->getFilteredResults()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->hashCode()I

    move-result v6

    iget v9, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->achievementsHashcode:I

    if-eq v6, v9, :cond_9

    .line 91
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->peopleHubViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->getFilteredResults()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->hashCode()I

    move-result v6

    iput v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->achievementsHashcode:I

    .line 93
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->compositeAchievementResults:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    .line 94
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->peopleHubViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->getFilteredResults()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    .line 95
    .local v3, "item":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;>;"
    iget-object v9, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->compositeAchievementResults:Ljava/util/List;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;->achievementItem(Landroid/util/Pair;)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 97
    .end local v3    # "item":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;>;"
    :cond_8
    iput-boolean v8, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->achievementsReady:Z

    .line 98
    iput-boolean v8, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->achievementsInvalidated:Z

    .line 101
    :cond_9
    iget-boolean v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->leaderboardsReady:Z

    if-eqz v6, :cond_c

    iget-boolean v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->achievementsReady:Z

    if-eqz v6, :cond_c

    iget-boolean v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->leaderboardsInvalidated:Z

    if-nez v6, :cond_a

    iget-boolean v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->achievementsInvalidated:Z

    if-eqz v6, :cond_c

    .line 102
    :cond_a
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 104
    .local v4, "newItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;>;"
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->peopleHubViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isMeProfile()Z

    move-result v6

    if-eqz v6, :cond_b

    .line 105
    invoke-static {}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;->leaderboardHeader()Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->compositeLeaderboardResults:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 108
    :cond_b
    invoke-static {}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;->achievementHeader()Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->compositeAchievementResults:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 111
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->getDataCopy()Ljava/util/List;

    move-result-object v5

    .line 113
    .local v5, "oldItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;>;"
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->clear()V

    .line 114
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-virtual {v6, v4}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->addAll(Ljava/util/Collection;)V

    .line 116
    new-instance v6, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;

    invoke-direct {v6, v5, v4}, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-static {v6}, Landroid/support/v7/util/DiffUtil;->calculateDiff(Landroid/support/v7/util/DiffUtil$Callback;)Landroid/support/v7/util/DiffUtil$DiffResult;

    move-result-object v0

    .line 117
    .local v0, "diffResult":Landroid/support/v7/util/DiffUtil$DiffResult;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-virtual {v0, v6}, Landroid/support/v7/util/DiffUtil$DiffResult;->dispatchUpdatesTo(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 119
    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->achievementsInvalidated:Z

    .line 120
    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->leaderboardsInvalidated:Z

    .line 122
    .end local v0    # "diffResult":Landroid/support/v7/util/DiffUtil$DiffResult;
    .end local v4    # "newItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;>;"
    .end local v5    # "oldItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;>;"
    :cond_c
    return-void
.end method
