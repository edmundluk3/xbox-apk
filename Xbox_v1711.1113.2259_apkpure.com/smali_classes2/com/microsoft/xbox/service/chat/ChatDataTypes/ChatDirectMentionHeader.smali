.class public Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;
.super Ljava/lang/Object;
.source "ChatDirectMentionHeader.java"


# static fields
.field public static final CHAT_DIRECTMENTION_HEADER_SIZE:I = 0x8

.field private static final NUMMENTIONS_OFFSET:I = 0x4

.field private static final PROTOCOLVERSION_OFFSET:I = 0x0

.field private static final PROTOCOL_VERSION:I = 0x1


# instance fields
.field public final numMentions:I

.field public final protocolVersion:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "numMentions"    # I

    .prologue
    .line 32
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;-><init>(II)V

    .line 33
    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "protocolVersion"    # I
    .param p2, "numMentions"    # I

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput p1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;->protocolVersion:I

    .line 28
    iput p2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;->numMentions:I

    .line 29
    return-void
.end method

.method public static readObject(Ljava/nio/ByteBuffer;I)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;
    .locals 4
    .param p0, "buffer"    # Ljava/nio/ByteBuffer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "offset"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 37
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 38
    const-wide/16 v0, 0x0

    int-to-long v2, p1

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 40
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    sub-int/2addr v0, p1

    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    .line 41
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "not enough buffer space to read ChatDirectMentionHeader"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;

    add-int/lit8 v1, p1, 0x0

    .line 45
    invoke-virtual {p0, v1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v1

    add-int/lit8 v2, p1, 0x4

    .line 46
    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;-><init>(II)V

    .line 44
    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 63
    if-ne p1, p0, :cond_1

    .line 69
    :cond_0
    :goto_0
    return v1

    .line 65
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;

    if-nez v3, :cond_2

    move v1, v2

    .line 66
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 68
    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;

    .line 69
    .local v0, "other":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;
    iget v3, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;->protocolVersion:I

    iget v4, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;->protocolVersion:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;->numMentions:I

    iget v4, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;->numMentions:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 76
    const/16 v0, 0x11

    .line 77
    .local v0, "result":I
    iget v1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;->protocolVersion:I

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(I)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 78
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;->numMentions:I

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(I)I

    move-result v2

    add-int v0, v1, v2

    .line 80
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeObject(Ljava/nio/ByteBuffer;I)V
    .locals 4
    .param p1, "buffer"    # Ljava/nio/ByteBuffer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "offset"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 50
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 51
    const-wide/16 v0, 0x0

    int-to-long v2, p2

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 53
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    sub-int/2addr v0, p2

    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    .line 54
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "not enough buffer space to write ChatDirectMentionHeader"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_0
    add-int/lit8 v0, p2, 0x0

    iget v1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;->protocolVersion:I

    invoke-virtual {p1, v0, v1}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    .line 58
    add-int/lit8 v0, p2, 0x4

    iget v1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;->numMentions:I

    invoke-virtual {p1, v0, v1}, Ljava/nio/ByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    .line 59
    return-void
.end method
