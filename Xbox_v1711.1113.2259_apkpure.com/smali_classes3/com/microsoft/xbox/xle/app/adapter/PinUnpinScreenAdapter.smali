.class public Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
.source "PinUnpinScreenAdapter.java"


# instance fields
.field private final pinIcon:Landroid/widget/TextView;

.field private final pinLabel:Landroid/widget/TextView;

.field private final pinUnpinButton:Landroid/widget/RelativeLayout;

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;I)V
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;
    .param p2, "pinUnpinButtonId"    # I

    .prologue
    const/4 v1, 0x0

    .line 19
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;

    .line 22
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->pinUnpinButton:Landroid/widget/RelativeLayout;

    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->pinUnpinButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->pinUnpinButton:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e0218

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->pinIcon:Landroid/widget/TextView;

    .line 25
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->pinUnpinButton:Landroid/widget/RelativeLayout;

    const v1, 0x7f0e0219

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->pinLabel:Landroid/widget/TextView;

    .line 30
    :goto_0
    return-void

    .line 27
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->pinIcon:Landroid/widget/TextView;

    .line 28
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->pinLabel:Landroid/widget/TextView;

    goto :goto_0
.end method

.method static synthetic lambda$onStart$0(Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinTapped()V

    return-void
.end method

.method private setEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->pinIcon:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateEnabledIfNotNull(Landroid/view/View;Z)V

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->pinLabel:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateEnabledIfNotNull(Landroid/view/View;Z)V

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->pinUnpinButton:Landroid/widget/RelativeLayout;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateEnabledIfNotNull(Landroid/view/View;Z)V

    .line 65
    return-void
.end method

.method private setPinUnpinState(Z)V
    .locals 2
    .param p1, "checked"    # Z

    .prologue
    .line 57
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->pinIcon:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const v0, 0x7f07103b

    :goto_0
    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;I)V

    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->pinLabel:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    const v0, 0x7f07006c

    :goto_1
    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;I)V

    .line 59
    return-void

    .line 57
    :cond_0
    const v0, 0x7f070fb5

    goto :goto_0

    .line 58
    :cond_1
    const v0, 0x7f070065

    goto :goto_1
.end method


# virtual methods
.method public onStart()V
    .locals 2

    .prologue
    .line 34
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->onStart()V

    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->pinUnpinButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->pinUnpinButton:Landroid/widget/RelativeLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->pinUnpinButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->pinUnpinButton:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;->onStop()V

    .line 46
    return-void
.end method

.method protected updateViewOverride()V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->pinUnpinButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->isPinUnpinEnabled()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->setEnabled(Z)V

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->isPinned()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;->setPinUnpinState(Z)V

    .line 54
    :cond_0
    return-void
.end method
