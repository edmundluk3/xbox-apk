.class Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameProfileInfoScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadGameProfileInfoAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;)V
    .locals 0

    .prologue
    .line 238
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$1;

    .prologue
    .line 238
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 242
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 243
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 8

    .prologue
    .line 273
    const/4 v0, 0x0

    .line 274
    .local v0, "failedToRehydrate":Z
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 275
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->access$400()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Current titleId:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;)J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v3

    .line 277
    .local v3, "titleModel":Lcom/microsoft/xbox/service/model/TitleModel;
    if-eqz v3, :cond_1

    .line 278
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleImageDetailsData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v2

    .line 279
    .local v2, "temp":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v2, :cond_3

    .line 280
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-static {v4, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->access$302(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 281
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->access$400()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Setting GameItem"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    .end local v2    # "temp":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .end local v3    # "titleModel":Lcom/microsoft/xbox/service/model/TitleModel;
    :cond_1
    :goto_0
    if-nez v0, :cond_2

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 290
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iput-object v4, v5, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 291
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    iget-object v4, v4, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    if-eqz v4, :cond_2

    .line 292
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    iget-object v4, v4, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    .line 293
    .local v1, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 294
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    const/4 v5, 0x0

    iput-object v5, v4, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->gameDetailModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 300
    .end local v1    # "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v4

    iget-boolean v5, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;->forceLoad:Z

    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-static {v6}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;)J

    move-result-wide v6

    invoke-virtual {v4, v5, v6, v7}, Lcom/microsoft/xbox/service/model/TitleHubModel;->load(ZJ)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v4

    return-object v4

    .line 283
    .restart local v2    # "temp":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .restart local v3    # "titleModel":Lcom/microsoft/xbox/service/model/TitleModel;
    :cond_3
    const/4 v0, 0x1

    .line 284
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->access$400()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Failed to Rehydrate!"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 238
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 265
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 238
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 248
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 249
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->onLoadGameProfileInfoCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 250
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 260
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->onLoadGameProfileInfoCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 261
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 238
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 254
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 255
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadGameProfileInfoAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->updateAdapter()V

    .line 256
    return-void
.end method
