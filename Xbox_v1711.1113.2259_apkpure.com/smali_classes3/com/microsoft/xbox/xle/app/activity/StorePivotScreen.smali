.class public Lcom/microsoft/xbox/xle/app/activity/StorePivotScreen;
.super Lcom/microsoft/xbox/xle/app/activity/PivotActivity;
.source "StorePivotScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    const-string v0, "Store"

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 13
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->onCreate()V

    .line 15
    const v1, 0x7f03021a

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/StorePivotScreen;->setContentView(I)V

    .line 16
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->isPurchaseBlocked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 17
    const v1, 0x7f0e0a7f

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/StorePivotScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 18
    .local v0, "title":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    const v1, 0x7f070123

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(I)V

    .line 21
    .end local v0    # "title":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    :cond_0
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;

    invoke-direct {v1}, Lcom/microsoft/xbox/xle/viewmodel/StorePivotScreenViewModel;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/StorePivotScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 23
    const v1, 0x7f0e0a80

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/StorePivotScreen;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/StorePivotScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    .line 24
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/StorePivotScreen;->pivot:Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onCreate()V

    .line 25
    return-void
.end method

.method public onCreateContentView()V
    .locals 0

    .prologue
    .line 29
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 33
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/PivotActivity;->onStart()V

    .line 34
    return-void
.end method
