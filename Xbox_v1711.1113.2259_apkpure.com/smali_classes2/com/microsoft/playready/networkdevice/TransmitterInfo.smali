.class public Lcom/microsoft/playready/networkdevice/TransmitterInfo;
.super Ljava/lang/Object;
.source "TransmitterInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;
    }
.end annotation


# static fields
.field static final DefaultProtocolPort:I = 0x2710

.field static final DefaultStreamingPort:I = 0x3a98

.field private static final mExecutor:Ljava/util/concurrent/ExecutorService;


# instance fields
.field public final Name:Ljava/lang/String;

.field private mContentParser:Lcom/microsoft/playready/networkdevice/ITransmitterContentParser;

.field private mHost:Ljava/lang/String;

.field private final mNativeUPnP:Lcom/microsoft/playready/networkdevice/Native_Class4;

.field private mProtocolAddress:Ljava/net/InetSocketAddress;

.field private mStreamingPort:I

.field private mUDN:Ljava/lang/String;

.field private mURL:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mExecutor:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method constructor <init>(Lcom/microsoft/playready/networkdevice/Native_Class4;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "nativeUPnP"    # Lcom/microsoft/playready/networkdevice/Native_Class4;
    .param p2, "deviceUDN"    # Ljava/lang/String;
    .param p3, "deviceName"    # Ljava/lang/String;
    .param p4, "deviceURL"    # Ljava/lang/String;

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mContentParser:Lcom/microsoft/playready/networkdevice/ITransmitterContentParser;

    .line 93
    iput-object p2, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mUDN:Ljava/lang/String;

    .line 94
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mNativeUPnP:Lcom/microsoft/playready/networkdevice/Native_Class4;

    .line 95
    iput-object p3, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->Name:Ljava/lang/String;

    .line 96
    invoke-static {p4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mURL:Landroid/net/Uri;

    .line 97
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mURL:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mHost:Ljava/lang/String;

    .line 98
    const/16 v0, 0x3a98

    iput v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mStreamingPort:I

    .line 99
    new-instance v0, Ljava/net/InetSocketAddress;

    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mHost:Ljava/lang/String;

    const/16 v2, 0x2710

    invoke-direct {v0, v1, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mProtocolAddress:Ljava/net/InetSocketAddress;

    .line 100
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 2
    .param p1, "universalDeviceName"    # Ljava/lang/String;
    .param p2, "deviceName"    # Ljava/lang/String;
    .param p3, "deviceURL"    # Ljava/lang/String;
    .param p4, "streamingPort"    # I
    .param p5, "protocolPort"    # I

    .prologue
    const/4 v0, 0x0

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mContentParser:Lcom/microsoft/playready/networkdevice/ITransmitterContentParser;

    .line 104
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mUDN:Ljava/lang/String;

    .line 105
    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mNativeUPnP:Lcom/microsoft/playready/networkdevice/Native_Class4;

    .line 106
    iput-object p2, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->Name:Ljava/lang/String;

    .line 107
    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mURL:Landroid/net/Uri;

    .line 108
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mURL:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mHost:Ljava/lang/String;

    .line 109
    new-instance v0, Ljava/net/InetSocketAddress;

    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mHost:Ljava/lang/String;

    invoke-direct {v0, v1, p5}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mProtocolAddress:Ljava/net/InetSocketAddress;

    .line 110
    iput p4, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mStreamingPort:I

    .line 111
    return-void
.end method

.method private parseConnectionInfo(Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass2;)V
    .locals 14
    .param p1, "info"    # Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass2;

    .prologue
    .line 331
    iget-object v10, p1, Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass2;->mField1:Ljava/lang/String;

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 333
    .local v9, "sources":[Ljava/lang/String;
    move-object v0, v9

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v8, v0, v2

    .line 335
    .local v8, "source":Ljava/lang/String;
    const-string v10, ":"

    invoke-virtual {v8, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 337
    .local v1, "fields":[Ljava/lang/String;
    array-length v10, v1

    const/4 v11, 0x4

    if-ne v10, v11, :cond_0

    .line 339
    const/4 v10, 0x0

    aget-object v10, v1, v10

    const-string v11, "internal"

    invoke-virtual {v10, v11}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v10

    if-eqz v10, :cond_1

    .line 333
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 345
    :cond_1
    const/4 v10, 0x1

    aget-object v10, v1, v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    const/16 v11, 0x10

    if-ge v10, v11, :cond_0

    .line 350
    const/4 v10, 0x3

    aget-object v10, v1, v10

    const-string v11, "="

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 351
    .local v5, "portFields":[Ljava/lang/String;
    array-length v10, v5

    const/4 v11, 0x2

    if-ne v10, v11, :cond_4

    const/4 v10, 0x1

    aget-object v4, v5, v10

    .line 353
    .local v4, "port":Ljava/lang/String;
    :goto_1
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    iput v10, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mStreamingPort:I

    .line 354
    const-string v10, "http://%s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const/4 v13, 0x1

    aget-object v13, v1, v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    iput-object v10, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mURL:Landroid/net/Uri;

    .line 355
    iget-object v10, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mURL:Landroid/net/Uri;

    invoke-virtual {v10}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mHost:Ljava/lang/String;

    .line 361
    .end local v1    # "fields":[Ljava/lang/String;
    .end local v4    # "port":Ljava/lang/String;
    .end local v5    # "portFields":[Ljava/lang/String;
    .end local v8    # "source":Ljava/lang/String;
    :cond_2
    iget-object v10, p1, Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass2;->mField2:Ljava/lang/String;

    const-string v11, ","

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 363
    .local v7, "sinks":[Ljava/lang/String;
    move-object v0, v7

    array-length v3, v0

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v3, :cond_6

    aget-object v6, v0, v2

    .line 365
    .local v6, "sink":Ljava/lang/String;
    const-string v10, ":"

    invoke-virtual {v6, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 367
    .restart local v1    # "fields":[Ljava/lang/String;
    array-length v10, v1

    const/4 v11, 0x4

    if-ne v10, v11, :cond_3

    .line 369
    const/4 v10, 0x0

    aget-object v10, v1, v10

    const-string v11, "internal"

    invoke-virtual {v10, v11}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v10

    if-eqz v10, :cond_5

    .line 363
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 351
    .end local v6    # "sink":Ljava/lang/String;
    .end local v7    # "sinks":[Ljava/lang/String;
    .restart local v5    # "portFields":[Ljava/lang/String;
    .restart local v8    # "source":Ljava/lang/String;
    :cond_4
    const/4 v10, 0x0

    aget-object v4, v5, v10

    goto :goto_1

    .line 375
    .end local v5    # "portFields":[Ljava/lang/String;
    .end local v8    # "source":Ljava/lang/String;
    .restart local v6    # "sink":Ljava/lang/String;
    .restart local v7    # "sinks":[Ljava/lang/String;
    :cond_5
    const/4 v10, 0x1

    aget-object v10, v1, v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    const/16 v11, 0x10

    if-ge v10, v11, :cond_3

    const/4 v10, 0x1

    aget-object v10, v1, v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    const/4 v11, 0x7

    if-lt v10, v11, :cond_3

    .line 380
    const/4 v10, 0x3

    aget-object v10, v1, v10

    const-string v11, "="

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 381
    .restart local v5    # "portFields":[Ljava/lang/String;
    array-length v10, v5

    const/4 v11, 0x2

    if-ne v10, v11, :cond_7

    const/4 v10, 0x1

    aget-object v4, v5, v10

    .line 383
    .restart local v4    # "port":Ljava/lang/String;
    :goto_3
    new-instance v10, Ljava/net/InetSocketAddress;

    const/4 v11, 0x1

    aget-object v11, v1, v11

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    invoke-direct {v10, v11, v12}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    iput-object v10, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mProtocolAddress:Ljava/net/InetSocketAddress;

    .line 389
    .end local v1    # "fields":[Ljava/lang/String;
    .end local v4    # "port":Ljava/lang/String;
    .end local v5    # "portFields":[Ljava/lang/String;
    .end local v6    # "sink":Ljava/lang/String;
    :cond_6
    return-void

    .line 381
    .restart local v1    # "fields":[Ljava/lang/String;
    .restart local v5    # "portFields":[Ljava/lang/String;
    .restart local v6    # "sink":Ljava/lang/String;
    :cond_7
    const/4 v10, 0x0

    aget-object v4, v5, v10

    goto :goto_3
.end method


# virtual methods
.method public browseContent(Lcom/microsoft/playready/networkdevice/TransmitterContent;Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;Ljava/lang/String;Ljava/lang/String;II)Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;
    .locals 7
    .param p1, "parentContent"    # Lcom/microsoft/playready/networkdevice/TransmitterContent;
    .param p2, "browseFlag"    # Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;
    .param p3, "filter"    # Ljava/lang/String;
    .param p4, "sortCriteria"    # Ljava/lang/String;
    .param p5, "index"    # I
    .param p6, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/playready/networkdevice/TransmitterContent;",
            "Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II)",
            "Lcom/microsoft/playready/networkdevice/TransmitterOperationTask",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/playready/networkdevice/TransmitterContent;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 252
    const/4 v6, 0x0

    .line 254
    .local v6, "result":Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationTask<Ljava/util/List<Lcom/microsoft/playready/networkdevice/TransmitterContent;>;>;"
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mNativeUPnP:Lcom/microsoft/playready/networkdevice/Native_Class4;

    if-eqz v1, :cond_1

    .line 256
    new-instance v5, Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass3;

    invoke-direct {v5}, Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass3;-><init>()V

    .line 258
    .local v5, "args":Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass3;
    iget-object v1, p1, Lcom/microsoft/playready/networkdevice/TransmitterContent;->mContentID:Ljava/lang/String;

    iput-object v1, v5, Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass3;->mField1:Ljava/lang/String;

    .line 259
    invoke-virtual {p2}, Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;->getText()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v5, Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass3;->mField2:Ljava/lang/String;

    .line 260
    iput-object p3, v5, Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass3;->mField3:Ljava/lang/String;

    .line 261
    iput-object p4, v5, Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass3;->mField4:Ljava/lang/String;

    .line 262
    iput p5, v5, Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass3;->mField5:I

    .line 263
    iput p6, v5, Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass3;->mField6:I

    .line 265
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;

    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$TransmitterTaskType;->Browse:Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$TransmitterTaskType;

    iget-object v2, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mNativeUPnP:Lcom/microsoft/playready/networkdevice/Native_Class4;

    invoke-virtual {v2}, Lcom/microsoft/playready/networkdevice/Native_Class4;->internal_method_1()J

    move-result-wide v2

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;-><init>(Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$TransmitterTaskType;JLcom/microsoft/playready/networkdevice/TransmitterInfo;Ljava/lang/Object;)V

    .line 272
    .local v0, "worker":Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker<Ljava/util/List<Lcom/microsoft/playready/networkdevice/TransmitterContent;>;>;"
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mContentParser:Lcom/microsoft/playready/networkdevice/ITransmitterContentParser;

    if-eqz v1, :cond_0

    .line 274
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mContentParser:Lcom/microsoft/playready/networkdevice/ITransmitterContentParser;

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->setContentParser(Lcom/microsoft/playready/networkdevice/ITransmitterContentParser;)V

    .line 277
    :cond_0
    new-instance v6, Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;

    .end local v6    # "result":Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationTask<Ljava/util/List<Lcom/microsoft/playready/networkdevice/TransmitterContent;>;>;"
    invoke-direct {v6, v0}, Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;-><init>(Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;)V

    .line 279
    .restart local v6    # "result":Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationTask<Ljava/util/List<Lcom/microsoft/playready/networkdevice/TransmitterContent;>;>;"
    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v6}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 282
    .end local v0    # "worker":Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker<Ljava/util/List<Lcom/microsoft/playready/networkdevice/TransmitterContent;>;>;"
    .end local v5    # "args":Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass3;
    :cond_1
    return-object v6
.end method

.method public browseContentRoot(Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;Ljava/lang/String;Ljava/lang/String;II)Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;
    .locals 8
    .param p1, "browseFlag"    # Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;
    .param p2, "filter"    # Ljava/lang/String;
    .param p3, "sortCriteria"    # Ljava/lang/String;
    .param p4, "index"    # I
    .param p5, "count"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II)",
            "Lcom/microsoft/playready/networkdevice/TransmitterOperationTask",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/playready/networkdevice/TransmitterContent;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 238
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterContent;

    const-string v2, "0"

    const-string v3, "root"

    const-string v4, ""

    const/4 v5, 0x1

    move-object v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/playready/networkdevice/TransmitterContent;-><init>(Lcom/microsoft/playready/networkdevice/TransmitterContent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/Hashtable;)V

    .local v0, "root":Lcom/microsoft/playready/networkdevice/TransmitterContent;
    move-object v1, p0

    move-object v2, v0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move v7, p5

    .line 240
    invoke-virtual/range {v1 .. v7}, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->browseContent(Lcom/microsoft/playready/networkdevice/TransmitterContent;Lcom/microsoft/playready/networkdevice/TransmitterInfo$BrowseFlag;Ljava/lang/String;Ljava/lang/String;II)Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;

    move-result-object v1

    return-object v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "aThat"    # Ljava/lang/Object;

    .prologue
    .line 128
    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    .line 130
    .end local p1    # "aThat":Ljava/lang/Object;
    :goto_0
    return v0

    .restart local p1    # "aThat":Ljava/lang/Object;
    :cond_0
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mUDN:Ljava/lang/String;

    check-cast p1, Lcom/microsoft/playready/networkdevice/TransmitterInfo;

    .end local p1    # "aThat":Ljava/lang/Object;
    iget-object v1, p1, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mUDN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public getContentSortCapabilities()Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/playready/networkdevice/TransmitterOperationTask",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 210
    const/4 v6, 0x0

    .line 212
    .local v6, "result":Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationTask<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mNativeUPnP:Lcom/microsoft/playready/networkdevice/Native_Class4;

    if-eqz v1, :cond_0

    .line 214
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;

    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$TransmitterTaskType;->SortCaps:Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$TransmitterTaskType;

    iget-object v2, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mNativeUPnP:Lcom/microsoft/playready/networkdevice/Native_Class4;

    invoke-virtual {v2}, Lcom/microsoft/playready/networkdevice/Native_Class4;->internal_method_1()J

    move-result-wide v2

    const/4 v5, 0x0

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;-><init>(Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$TransmitterTaskType;JLcom/microsoft/playready/networkdevice/TransmitterInfo;Ljava/lang/Object;)V

    .line 221
    .local v0, "worker":Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker<Ljava/lang/String;>;"
    new-instance v6, Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;

    .end local v6    # "result":Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationTask<Ljava/lang/String;>;"
    invoke-direct {v6, v0}, Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;-><init>(Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;)V

    .line 223
    .restart local v6    # "result":Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationTask<Ljava/lang/String;>;"
    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v6}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 226
    .end local v0    # "worker":Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker<Ljava/lang/String;>;"
    :cond_0
    return-object v6
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mHost:Ljava/lang/String;

    return-object v0
.end method

.method public getProtocolAddress()Ljava/net/InetSocketAddress;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mProtocolAddress:Ljava/net/InetSocketAddress;

    return-object v0
.end method

.method public getStreamingPort()I
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mStreamingPort:I

    return v0
.end method

.method public getSystemUpdateID()Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/playready/networkdevice/TransmitterOperationTask",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 182
    const/4 v6, 0x0

    .line 184
    .local v6, "result":Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationTask<Ljava/lang/Integer;>;"
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mNativeUPnP:Lcom/microsoft/playready/networkdevice/Native_Class4;

    if-eqz v1, :cond_0

    .line 186
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;

    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$TransmitterTaskType;->SystemUpdateID:Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$TransmitterTaskType;

    iget-object v2, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mNativeUPnP:Lcom/microsoft/playready/networkdevice/Native_Class4;

    invoke-virtual {v2}, Lcom/microsoft/playready/networkdevice/Native_Class4;->internal_method_1()J

    move-result-wide v2

    const/4 v5, 0x0

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;-><init>(Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$TransmitterTaskType;JLcom/microsoft/playready/networkdevice/TransmitterInfo;Ljava/lang/Object;)V

    .line 193
    .local v0, "worker":Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker<Ljava/lang/Integer;>;"
    new-instance v6, Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;

    .end local v6    # "result":Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationTask<Ljava/lang/Integer;>;"
    invoke-direct {v6, v0}, Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;-><init>(Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;)V

    .line 195
    .restart local v6    # "result":Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationTask<Ljava/lang/Integer;>;"
    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v6}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 198
    .end local v0    # "worker":Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker<Ljava/lang/Integer;>;"
    :cond_0
    return-object v6
.end method

.method public getURL()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mURL:Landroid/net/Uri;

    return-object v0
.end method

.method public getUniversalDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mUDN:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mUDN:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public setTransmitterContentParser(Lcom/microsoft/playready/networkdevice/ITransmitterContentParser;)V
    .locals 0
    .param p1, "parser"    # Lcom/microsoft/playready/networkdevice/ITransmitterContentParser;

    .prologue
    .line 287
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mContentParser:Lcom/microsoft/playready/networkdevice/ITransmitterContentParser;

    .line 288
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->Name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mHost:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method tryToGetConnectionData()V
    .locals 9

    .prologue
    .line 297
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mNativeUPnP:Lcom/microsoft/playready/networkdevice/Native_Class4;

    if-nez v1, :cond_0

    .line 326
    :goto_0
    return-void

    .line 302
    :cond_0
    new-instance v0, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;

    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$TransmitterTaskType;->ProtocolInfo:Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$TransmitterTaskType;

    iget-object v2, p0, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mNativeUPnP:Lcom/microsoft/playready/networkdevice/Native_Class4;

    invoke-virtual {v2}, Lcom/microsoft/playready/networkdevice/Native_Class4;->internal_method_1()J

    move-result-wide v2

    const/4 v5, 0x0

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;-><init>(Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker$TransmitterTaskType;JLcom/microsoft/playready/networkdevice/TransmitterInfo;Ljava/lang/Object;)V

    .line 309
    .local v0, "worker":Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker<Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass2;>;"
    new-instance v8, Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;

    invoke-direct {v8, v0}, Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;-><init>(Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;)V

    .line 312
    .local v8, "protInfoTask":Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationTask<Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass2;>;"
    sget-object v1, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->mExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v8}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 316
    :try_start_0
    invoke-virtual {v8}, Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass2;

    .line 317
    .local v7, "info":Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass2;
    invoke-direct {p0, v7}, Lcom/microsoft/playready/networkdevice/TransmitterInfo;->parseConnectionInfo(Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass2;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 319
    .end local v7    # "info":Lcom/microsoft/playready/networkdevice/Native_Class7$InnerClass2;
    :catch_0
    move-exception v6

    .line 321
    .local v6, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v6}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 322
    .end local v6    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v6

    .line 324
    .local v6, "e":Ljava/util/concurrent/ExecutionException;
    invoke-virtual {v6}, Ljava/util/concurrent/ExecutionException;->printStackTrace()V

    goto :goto_0
.end method
