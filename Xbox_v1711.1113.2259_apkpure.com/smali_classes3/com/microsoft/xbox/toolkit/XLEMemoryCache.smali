.class public Lcom/microsoft/xbox/toolkit/XLEMemoryCache;
.super Ljava/lang/Object;
.source "XLEMemoryCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private itemCount:I

.field private final lruCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<TK;",
            "Lcom/microsoft/xbox/toolkit/XLEMemoryCacheEntry",
            "<TV;>;>;"
        }
    .end annotation
.end field

.field private final maxFileSizeBytes:I


# direct methods
.method public constructor <init>(II)V
    .locals 2
    .param p1, "sizeInBytes"    # I
    .param p2, "maxFileSizeInBytes"    # I

    .prologue
    .line 41
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEMemoryCache;, "Lcom/microsoft/xbox/toolkit/XLEMemoryCache<TK;TV;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/XLEMemoryCache;->itemCount:I

    .line 42
    if-gez p1, :cond_0

    .line 43
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sizeInBytes"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :cond_0
    if-gez p2, :cond_1

    .line 47
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "maxFileSizeInBytes"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_1
    iput p2, p0, Lcom/microsoft/xbox/toolkit/XLEMemoryCache;->maxFileSizeBytes:I

    .line 51
    if-nez p1, :cond_2

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEMemoryCache;->lruCache:Landroid/util/LruCache;

    .line 68
    return-void

    .line 51
    :cond_2
    new-instance v0, Lcom/microsoft/xbox/toolkit/XLEMemoryCache$1;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/XLEMemoryCache$1;-><init>(Lcom/microsoft/xbox/toolkit/XLEMemoryCache;I)V

    goto :goto_0
.end method

.method static synthetic access$006(Lcom/microsoft/xbox/toolkit/XLEMemoryCache;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/XLEMemoryCache;

    .prologue
    .line 36
    iget v0, p0, Lcom/microsoft/xbox/toolkit/XLEMemoryCache;->itemCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/toolkit/XLEMemoryCache;->itemCount:I

    return v0
.end method


# virtual methods
.method public add(Ljava/lang/Object;Ljava/lang/Object;I)Z
    .locals 5
    .param p3, "fileByteCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;I)Z"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEMemoryCache;, "Lcom/microsoft/xbox/toolkit/XLEMemoryCache<TK;TV;>;"
    .local p1, "filename":Ljava/lang/Object;, "TK;"
    .local p2, "data":Ljava/lang/Object;, "TV;"
    const/4 v1, 0x0

    .line 86
    iget v2, p0, Lcom/microsoft/xbox/toolkit/XLEMemoryCache;->maxFileSizeBytes:I

    if-le p3, v2, :cond_0

    .line 87
    const-string v2, "XLEMemoryCache"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Tried to add a "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " file!  That is more than the max file size of: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/microsoft/xbox/toolkit/XLEMemoryCache;->maxFileSizeBytes:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :goto_0
    return v1

    .line 92
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/XLEMemoryCache;->lruCache:Landroid/util/LruCache;

    if-nez v2, :cond_1

    .line 93
    const-string v2, "XLEMemoryCache"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Tried to add a "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to a cache with 0 sizeInBytes"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 97
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/toolkit/XLEMemoryCacheEntry;

    invoke-direct {v0, p2, p3}, Lcom/microsoft/xbox/toolkit/XLEMemoryCacheEntry;-><init>(Ljava/lang/Object;I)V

    .line 100
    .local v0, "entry":Lcom/microsoft/xbox/toolkit/XLEMemoryCacheEntry;, "Lcom/microsoft/xbox/toolkit/XLEMemoryCacheEntry<TV;>;"
    iget v1, p0, Lcom/microsoft/xbox/toolkit/XLEMemoryCache;->itemCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/XLEMemoryCache;->itemCount:I

    .line 101
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/XLEMemoryCache;->lruCache:Landroid/util/LruCache;

    invoke-virtual {v1, p1, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 106
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEMemoryCache;, "Lcom/microsoft/xbox/toolkit/XLEMemoryCache<TK;TV;>;"
    .local p1, "filename":Ljava/lang/Object;, "TK;"
    const/4 v1, 0x0

    .line 107
    .local v1, "value":Ljava/lang/Object;, "TV;"
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/XLEMemoryCache;->lruCache:Landroid/util/LruCache;

    if-eqz v2, :cond_0

    .line 108
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/XLEMemoryCache;->lruCache:Landroid/util/LruCache;

    invoke-virtual {v2, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/XLEMemoryCacheEntry;

    .line 109
    .local v0, "entry":Lcom/microsoft/xbox/toolkit/XLEMemoryCacheEntry;, "Lcom/microsoft/xbox/toolkit/XLEMemoryCacheEntry<TV;>;"
    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEMemoryCacheEntry;->getValue()Ljava/lang/Object;

    move-result-object v1

    .line 113
    .end local v0    # "entry":Lcom/microsoft/xbox/toolkit/XLEMemoryCacheEntry;, "Lcom/microsoft/xbox/toolkit/XLEMemoryCacheEntry<TV;>;"
    .end local v1    # "value":Ljava/lang/Object;, "TV;"
    :cond_0
    return-object v1
.end method

.method public getBytesCurrent()I
    .locals 1

    .prologue
    .line 71
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEMemoryCache;, "Lcom/microsoft/xbox/toolkit/XLEMemoryCache<TK;TV;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEMemoryCache;->lruCache:Landroid/util/LruCache;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEMemoryCache;->lruCache:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getBytesFree()I
    .locals 2

    .prologue
    .line 79
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEMemoryCache;, "Lcom/microsoft/xbox/toolkit/XLEMemoryCache<TK;TV;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEMemoryCache;->lruCache:Landroid/util/LruCache;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/XLEMemoryCache;->lruCache:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->maxSize()I

    move-result v0

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/XLEMemoryCache;->lruCache:Landroid/util/LruCache;

    invoke-virtual {v1}, Landroid/util/LruCache;->size()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public getItemsInCache()I
    .locals 1

    .prologue
    .line 75
    .local p0, "this":Lcom/microsoft/xbox/toolkit/XLEMemoryCache;, "Lcom/microsoft/xbox/toolkit/XLEMemoryCache<TK;TV;>;"
    iget v0, p0, Lcom/microsoft/xbox/toolkit/XLEMemoryCache;->itemCount:I

    return v0
.end method
