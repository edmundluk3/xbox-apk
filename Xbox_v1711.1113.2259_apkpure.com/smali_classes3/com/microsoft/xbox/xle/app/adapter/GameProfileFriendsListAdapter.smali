.class public Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "GameProfileFriendsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubMembershipViewHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$SpotlightViewHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$IGameProfileFriendsListItem;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$IGameProfileFriendsListItem;",
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$IGameProfileFriendsListItem;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final CLUB_MEMBERSHIP_LAYOUT:I = 0x7f03011e
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field

.field private static final CLUB_RECOMMENDATION_LAYOUT:I = 0x7f030069
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field

.field private static final FOLLOWERS_DATA_LAYOUT:I = 0x7f030120
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field

.field private static final SPOTLIGHT_LAYOUT:I = 0x7f030128
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private friendsWhoPlayVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

.field private final layoutInflater:Landroid/view/LayoutInflater;

.field private final seeAllClickHandler:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;",
            ">;"
        }
    .end annotation
.end field

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;
    .param p3    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 71
    .local p3, "seeAllClickHandler":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/service/model/clubs/ClubCardCategoryItem;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 72
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    .line 73
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;

    .line 74
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    invoke-virtual {p2, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->findFirstChildViewModel(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->friendsWhoPlayVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    .line 75
    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->seeAllClickHandler:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 76
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->friendsWhoPlayVM:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->seeAllClickHandler:Lcom/microsoft/xbox/toolkit/generics/Action;

    return-object v0
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 104
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$IGameProfileFriendsListItem;

    .line 105
    .local v0, "item":Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$IGameProfileFriendsListItem;
    instance-of v1, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$GameProfileSpotlightDataListItem;

    if-eqz v1, :cond_0

    .line 106
    const v1, 0x7f030128

    .line 121
    :goto_0
    return v1

    .line 109
    :cond_0
    instance-of v1, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel$GameProfileFollowersDataListItem;

    if-eqz v1, :cond_1

    .line 110
    const v1, 0x7f030120

    goto :goto_0

    .line 113
    :cond_1
    instance-of v1, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubRecommendationsViewModel$GameProfileClubRecommendationsListItem;

    if-eqz v1, :cond_2

    .line 114
    const v1, 0x7f030069

    goto :goto_0

    .line 117
    :cond_2
    instance-of v1, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;

    if-eqz v1, :cond_3

    .line 118
    const v1, 0x7f03011e

    goto :goto_0

    .line 121
    :cond_3
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 49
    check-cast p1, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->onBindViewHolder(Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;I)V
    .locals 1
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
            "<",
            "Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$IGameProfileFriendsListItem;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 99
    .local p1, "holder":Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;, "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase<Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$IGameProfileFriendsListItem;>;"
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;->onBind(Ljava/lang/Object;)V

    .line 100
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 80
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 81
    .local v0, "view":Landroid/view/View;
    sparse-switch p2, :sswitch_data_0

    .line 91
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown view type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 94
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 83
    :sswitch_0
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$SpotlightViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$SpotlightViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 85
    :sswitch_1
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$FollowersDataViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 87
    :sswitch_2
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubCardCategoryViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 89
    :sswitch_3
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubMembershipViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubMembershipViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 81
    :sswitch_data_0
    .sparse-switch
        0x7f030069 -> :sswitch_2
        0x7f03011e -> :sswitch_3
        0x7f030120 -> :sswitch_1
        0x7f030128 -> :sswitch_0
    .end sparse-switch
.end method
