.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse;
.super Ljava/lang/Object;
.source "StoreItemDetailResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ContentRating;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$ProductProperties;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$RelatedProduct;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingData;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$MarketProperty;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductImage;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Remediation;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Affirmation;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$EligibilityProperties;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$LocalizedProperty;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformDependency;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackageApplication;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuPropertyPackage;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$HardwareProperties;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BundledSku;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuProperties;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$SkuLocalizedProperty;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Sku;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AvailabilityProperties;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Price;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Availability;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$DisplaySkuAvailability;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateId;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingTimeSpan;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "This type shouldn\'t be instantiated."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse;->TAG:Ljava/lang/String;

    return-object v0
.end method
