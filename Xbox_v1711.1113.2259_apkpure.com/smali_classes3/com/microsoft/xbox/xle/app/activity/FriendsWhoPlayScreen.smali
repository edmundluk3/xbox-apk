.class public Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "FriendsWhoPlayScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen$FriendsWhoPlayAdapterProvider;
    }
.end annotation


# instance fields
.field protected adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 20
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen$FriendsWhoPlayAdapterProvider;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen$FriendsWhoPlayAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen;Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen$FriendsWhoPlayAdapterProvider;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen$FriendsWhoPlayAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen;Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .line 26
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    const-string v0, "FriendsWhoPlay"

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 30
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 32
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen;->onCreateContentView()V

    .line 34
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;-><init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 35
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->setAsPivotPane()V

    .line 36
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsWhoPlayScreenViewModel;->onCreate()V

    .line 38
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 39
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 43
    const v0, 0x7f03010c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/FriendsWhoPlayScreen;->setContentView(I)V

    .line 44
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    return v0
.end method
