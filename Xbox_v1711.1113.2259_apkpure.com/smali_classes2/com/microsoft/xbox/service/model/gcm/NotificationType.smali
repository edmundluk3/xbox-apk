.class public final enum Lcom/microsoft/xbox/service/model/gcm/NotificationType;
.super Ljava/lang/Enum;
.source "NotificationType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/gcm/NotificationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/gcm/NotificationType;

.field public static final enum ACHIEVEMENT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

.field public static final enum CHAT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

.field public static final enum CLUB_DEMOTION:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

.field public static final enum CLUB_INVITED:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

.field public static final enum CLUB_JOINED:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

.field public static final enum CLUB_MODERATION_INVITE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

.field public static final enum CLUB_MODERATION_REPORT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

.field public static final enum CLUB_NEW_MEMBER:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

.field public static final enum CLUB_PROMOTION:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

.field public static final enum CLUB_RECOMMENDATION:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

.field public static final enum CLUB_TRANSFER:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

.field public static final enum FAV_BROADCAST:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

.field public static final enum FAV_ONLINE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

.field public static final enum LFG:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

.field public static final enum MESSAGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

.field public static final enum PARTY:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

.field public static final enum PARTY_INVITE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

.field public static final enum TOURNAMENT_MATCH:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

.field public static final enum TOURNAMENT_STATUS_CHANGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

.field public static final enum TOURNAMENT_TEAM_INVITE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

.field public static final enum TOURNAMENT_TEAM_JOIN:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

.field public static final enum TOURNAMENT_TEAM_LEAVE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

.field public static final enum TOURNAMENT_TEAM_STATUS_CHANGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;


# instance fields
.field private final flag:I

.field private final type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/16 v9, 0x10

    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 7
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const-string v1, "FAV_ONLINE"

    const/4 v2, 0x0

    const-string v3, "presence"

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->FAV_ONLINE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 8
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const-string v1, "FAV_BROADCAST"

    const-string v2, "broadcast"

    invoke-direct {v0, v1, v5, v2, v6}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->FAV_BROADCAST:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 9
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const-string v1, "MESSAGE"

    const-string v2, "new_message"

    invoke-direct {v0, v1, v6, v2, v7}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->MESSAGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 10
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const-string v1, "LFG"

    const/4 v2, 0x3

    const-string v3, "lfgNotification"

    invoke-direct {v0, v1, v2, v3, v8}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->LFG:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 11
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const-string v1, "CLUB_NEW_MEMBER"

    const-string v2, "Club_Notification_NewMember_Android"

    invoke-direct {v0, v1, v7, v2, v9}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_NEW_MEMBER:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 12
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const-string v1, "CLUB_PROMOTION"

    const/4 v2, 0x5

    const-string v3, "Club_Notification_AdminPromotion_Android"

    const/16 v4, 0x20

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_PROMOTION:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 13
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const-string v1, "CLUB_DEMOTION"

    const/4 v2, 0x6

    const-string v3, "Club_Notification_Demotion_Android"

    const/16 v4, 0x40

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_DEMOTION:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 14
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const-string v1, "CLUB_MODERATION_INVITE"

    const/4 v2, 0x7

    const-string v3, "Club_Notification_ModerationInvitation_Android"

    const/16 v4, 0x80

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_MODERATION_INVITE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 15
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const-string v1, "CLUB_RECOMMENDATION"

    const-string v2, "Club_Notification_Recommendation_Android"

    const/16 v3, 0x100

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_RECOMMENDATION:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 16
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const-string v1, "CLUB_INVITED"

    const/16 v2, 0x9

    const-string v3, "Club_Notification_Invitation_Android"

    const/16 v4, 0x200

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_INVITED:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 17
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const-string v1, "CLUB_MODERATION_REPORT"

    const/16 v2, 0xa

    const-string v3, "Club_Notification_ModerationReport_Android"

    const/16 v4, 0x400

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_MODERATION_REPORT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 18
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const-string v1, "CLUB_TRANSFER"

    const/16 v2, 0xb

    const-string v3, "Club_Notification_OwnerPromotion_Android"

    const/16 v4, 0x800

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_TRANSFER:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 19
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const-string v1, "CLUB_JOINED"

    const/16 v2, 0xc

    const-string v3, "Club_Notification_Joined_Android"

    const/16 v4, 0x1000

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_JOINED:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 20
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const-string v1, "CHAT"

    const/16 v2, 0xd

    const-string v3, "Chat_Notification"

    const/16 v4, 0x2000

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CHAT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 21
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const-string v1, "ACHIEVEMENT"

    const/16 v2, 0xe

    const-string/jumbo v3, "xbox_live_achievement_unlock"

    const/16 v4, 0x4000

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->ACHIEVEMENT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 22
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const-string v1, "TOURNAMENT_MATCH"

    const/16 v2, 0xf

    const-string v3, "tournamentMatchAvailable"

    const/high16 v4, 0x10000

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_MATCH:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 23
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const-string v1, "TOURNAMENT_STATUS_CHANGE"

    const-string v2, "tournamentStatusChange"

    const/high16 v3, 0x20000

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_STATUS_CHANGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 24
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const-string v1, "TOURNAMENT_TEAM_STATUS_CHANGE"

    const/16 v2, 0x11

    const-string v3, "tournamentTeamStatusChange"

    const/high16 v4, 0x40000

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_TEAM_STATUS_CHANGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 25
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const-string v1, "PARTY_INVITE"

    const/16 v2, 0x12

    const-string v3, "partyInvite"

    const/high16 v4, 0x80000

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->PARTY_INVITE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 26
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const-string v1, "TOURNAMENT_TEAM_INVITE"

    const/16 v2, 0x13

    const-string v3, "teamInvite"

    const/high16 v4, 0x100000

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_TEAM_INVITE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 27
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const-string v1, "TOURNAMENT_TEAM_JOIN"

    const/16 v2, 0x14

    const-string v3, "teamJoin"

    const/high16 v4, 0x100000

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_TEAM_JOIN:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 28
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const-string v1, "TOURNAMENT_TEAM_LEAVE"

    const/16 v2, 0x15

    const-string v3, "teamLeave"

    const/high16 v4, 0x100000

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_TEAM_LEAVE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 29
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const-string v1, "PARTY"

    const/16 v2, 0x16

    const-string v3, "party"

    const/high16 v4, 0x200000

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->PARTY:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 5
    const/16 v0, 0x17

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->FAV_ONLINE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->FAV_BROADCAST:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->MESSAGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v1, v0, v6

    const/4 v1, 0x3

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->LFG:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_NEW_MEMBER:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_PROMOTION:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_DEMOTION:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_MODERATION_INVITE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_RECOMMENDATION:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v1, v0, v8

    const/16 v1, 0x9

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_INVITED:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_MODERATION_REPORT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_TRANSFER:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_JOINED:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CHAT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->ACHIEVEMENT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_MATCH:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_STATUS_CHANGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v1, v0, v9

    const/16 v1, 0x11

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_TEAM_STATUS_CHANGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->PARTY_INVITE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_TEAM_INVITE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_TEAM_JOIN:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_TEAM_LEAVE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->PARTY:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->$VALUES:[Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .param p3, "type"    # Ljava/lang/String;
    .param p4, "flag"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 37
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->type:Ljava/lang/String;

    .line 38
    iput p4, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->flag:I

    .line 39
    return-void
.end method

.method public static fromSkypeType(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    .locals 5
    .param p0, "type"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 65
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->values()[Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 66
    .local v0, "t":Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_0

    .line 71
    .end local v0    # "t":Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    :goto_1
    return-object v0

    .line 65
    .restart local v0    # "t":Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 71
    .end local v0    # "t":Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static fromType(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    .locals 6
    .param p0, "type"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 51
    const-string v2, "_Android"

    invoke-virtual {p0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 52
    .local v1, "typeWithAndroidSuffix":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->values()[Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v0, v3, v2

    .line 53
    .local v0, "t":Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_0

    .line 55
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_1

    .line 60
    .end local v0    # "t":Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    .end local v1    # "typeWithAndroidSuffix":Ljava/lang/String;
    :cond_0
    :goto_1
    return-object v0

    .line 52
    .restart local v0    # "t":Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    .restart local v1    # "typeWithAndroidSuffix":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 60
    .end local v0    # "t":Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    .end local v1    # "typeWithAndroidSuffix":Ljava/lang/String;
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 5
    const-class v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/gcm/NotificationType;
    .locals 1

    .prologue
    .line 5
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->$VALUES:[Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/gcm/NotificationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    return-object v0
.end method


# virtual methods
.method public getFlag()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->flag:I

    return v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->type:Ljava/lang/String;

    return-object v0
.end method
