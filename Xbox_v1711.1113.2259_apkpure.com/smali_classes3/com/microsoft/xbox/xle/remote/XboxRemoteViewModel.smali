.class public Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;
.source "XboxRemoteViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel$DSGTitleTester;
    }
.end annotation


# instance fields
.field private isTvEnabled:Z

.field private titleTester:Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel$DSGTitleTester;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;)V
    .locals 1
    .param p1, "view"    # Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;-><init>(Lcom/microsoft/xbox/toolkit/IViewUpdate;)V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->isTvEnabled:Z

    .line 38
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->isTvEnabled:Z

    return v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;)Lcom/microsoft/xbox/toolkit/IViewUpdate;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->view:Lcom/microsoft/xbox/toolkit/IViewUpdate;

    return-object v0
.end method

.method private getIsTvEnabled()Z
    .locals 2

    .prologue
    .line 138
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    .line 139
    .local v0, "session":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    if-nez v0, :cond_0

    .line 140
    const/4 v1, 0x0

    .line 142
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->isVolumeEnabled()Z

    move-result v1

    goto :goto_0
.end method

.method private registerTvEventListener()V
    .locals 1

    .prologue
    .line 146
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    .line 147
    .local v0, "session":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    if-eqz v0, :cond_0

    .line 148
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->addListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;)V

    .line 150
    :cond_0
    return-void
.end method

.method private unRegisterTvEventListener()V
    .locals 1

    .prologue
    .line 153
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    .line 154
    .local v0, "session":Lcom/microsoft/xbox/xle/urc/net/BranchSession;
    if-eqz v0, :cond_0

    .line 155
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->removeListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;)V

    .line 157
    :cond_0
    return-void
.end method

.method private updateTvStatus()V
    .locals 2

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->getIsTvEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->isTvEnabled:Z

    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->view:Lcom/microsoft/xbox/toolkit/IViewUpdate;

    check-cast v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->isTvEnabled:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->updateTvView(Z)V

    .line 176
    return-void
.end method


# virtual methods
.method public asyncSendTouchFrame(Lcom/microsoft/xbox/smartglass/TouchFrame;)V
    .locals 5
    .param p1, "frame"    # Lcom/microsoft/xbox/smartglass/TouchFrame;

    .prologue
    .line 115
    new-instance v1, Lcom/microsoft/xbox/smartglass/MessageTarget;

    sget-object v2, Lcom/microsoft/xbox/smartglass/ServiceChannel;->SystemInput:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/smartglass/MessageTarget;-><init>(Lcom/microsoft/xbox/smartglass/ServiceChannel;)V

    .line 119
    .local v1, "target":Lcom/microsoft/xbox/smartglass/MessageTarget;
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSensorManager()Lcom/microsoft/xbox/smartglass/SensorManager;

    move-result-object v2

    invoke-virtual {v2, p1, v1}, Lcom/microsoft/xbox/smartglass/SensorManager;->sendTouchFrame(Lcom/microsoft/xbox/smartglass/TouchFrame;Lcom/microsoft/xbox/smartglass/MessageTarget;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    :goto_0
    return-void

    .line 120
    :catch_0
    move-exception v0

    .line 121
    .local v0, "ex":Ljava/lang/Exception;
    const-string v2, "XboxRemoteViewModel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendTouchFrame error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .locals 4

    .prologue
    .line 64
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/SessionManager;->getActiveTitles()Ljava/util/List;

    move-result-object v0

    .line 65
    .local v0, "activeTitles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/smartglass/ActiveTitleState;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    .line 66
    .local v1, "atl":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    iget-boolean v3, v1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->hasFocus:Z

    if-eqz v3, :cond_0

    .line 67
    iget-object v2, v1, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .line 70
    .end local v1    # "atl":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    :goto_0
    return-object v2

    :cond_1
    sget-object v2, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    goto :goto_0
.end method

.method public isDPadXInverted()Z
    .locals 1

    .prologue
    .line 126
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->isInvertHSwipe()Z

    move-result v0

    return v0
.end method

.method public isDPadYInverted()Z
    .locals 1

    .prologue
    .line 130
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->isInvertVSwipe()Z

    move-result v0

    return v0
.end method

.method public isTvEnabled()Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->isTvEnabled:Z

    return v0
.end method

.method public onConfigChanged([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V
    .locals 1
    .param p1, "devices"    # [Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    .prologue
    .line 185
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->isVolumeEnabled()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->onTvEvent(Z)V

    .line 186
    return-void
.end method

.method public onConnectionStateChanged(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;Ljava/lang/String;)V
    .locals 0
    .param p1, "state"    # Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;
    .param p2, "connectionError"    # Ljava/lang/String;

    .prologue
    .line 181
    return-void
.end method

.method public onHeadendChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 0
    .param p1, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 191
    return-void
.end method

.method public onHeadendSettingChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 0
    .param p1, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 196
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 52
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->onPause()V

    .line 53
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->unRegisterTvEventListener()V

    .line 57
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 42
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;->onResume()V

    .line 43
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->registerTvEventListener()V

    .line 44
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->updateTvStatus()V

    .line 48
    return-void
.end method

.method public onTvEvent(Z)V
    .locals 1
    .param p1, "tvEnabled"    # Z

    .prologue
    .line 164
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->isTvEnabled:Z

    .line 165
    new-instance v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel$2;-><init>(Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 171
    return-void
.end method

.method public sendKey(Lcom/microsoft/xbox/smartglass/GamePadButtons;)V
    .locals 1
    .param p1, "gamePadButtons"    # Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .prologue
    .line 84
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/SessionModel;->sendKey(Lcom/microsoft/xbox/smartglass/GamePadButtons;)V

    .line 85
    return-void
.end method

.method public sendKey(Lcom/microsoft/xbox/smartglass/GamePadButtons;Z)V
    .locals 1
    .param p1, "gamePadButtons"    # Lcom/microsoft/xbox/smartglass/GamePadButtons;
    .param p2, "simulateButtonPress"    # Z

    .prologue
    .line 88
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/service/model/SessionModel;->sendKey(Lcom/microsoft/xbox/smartglass/GamePadButtons;Z)V

    .line 89
    return-void
.end method

.method public sendPowerUpDownKey()V
    .locals 2

    .prologue
    .line 111
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/smartglass/GamePadButtons;->Nexus:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/SessionModel;->sendKey(Lcom/microsoft/xbox/smartglass/GamePadButtons;)V

    .line 112
    return-void
.end method

.method public sendToggleViewKey()V
    .locals 4

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->sendXboxKey()V

    .line 103
    new-instance v0, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel$1;-><init>(Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;)V

    const-wide/16 v2, 0xc8

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPostDelayed(Ljava/lang/Runnable;J)V

    .line 108
    return-void
.end method

.method public sendXboxKey()V
    .locals 3

    .prologue
    .line 92
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/smartglass/GamePadButtons;->Nexus:Lcom/microsoft/xbox/smartglass/GamePadButtons;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/SessionModel;->sendKey(Lcom/microsoft/xbox/smartglass/GamePadButtons;Z)V

    .line 93
    return-void
.end method

.method public useClientSideDPadHandler()Z
    .locals 1

    .prologue
    .line 80
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getRemoteControlSpecialTitleIds()[I

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/remote/XboxRemoteViewModel;->isAppRunning([I)Z

    move-result v0

    return v0
.end method
