.class final synthetic Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$6;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/BiFunction;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$6;->arg$1:Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;)Lio/reactivex/functions/BiFunction;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$6;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$6;-><init>(Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter$$Lambda$6;->arg$1:Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;

    check-cast p1, Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;

    check-cast p2, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;

    check-cast p2, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;

    invoke-static {v0, p1, p2}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;->access$lambda$0(Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)Lcom/microsoft/xbox/presentation/tutorial/TutorialViewState;

    move-result-object v0

    return-object v0
.end method
