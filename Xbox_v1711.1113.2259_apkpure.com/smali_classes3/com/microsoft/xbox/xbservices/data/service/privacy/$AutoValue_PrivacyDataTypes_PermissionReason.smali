.class abstract Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_PermissionReason;
.super Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReason;
.source "$AutoValue_PrivacyDataTypes_PermissionReason.java"


# instance fields
.field private final reason:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;)V
    .locals 2
    .param p1, "reason"    # Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReason;-><init>()V

    .line 14
    if-nez p1, :cond_0

    .line 15
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null reason"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 17
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_PermissionReason;->reason:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    .line 18
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 35
    if-ne p1, p0, :cond_0

    .line 36
    const/4 v1, 0x1

    .line 42
    :goto_0
    return v1

    .line 38
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReason;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 39
    check-cast v0, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReason;

    .line 40
    .local v0, "that":Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReason;
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_PermissionReason;->reason:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReason;->reason()Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 42
    .end local v0    # "that":Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReason;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 47
    const/4 v0, 0x1

    .line 48
    .local v0, "h":I
    const v1, 0xf4243

    mul-int/2addr v0, v1

    .line 49
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_PermissionReason;->reason:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 50
    return v0
.end method

.method public reason()Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_PermissionReason;->reason:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PermissionReason{reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_PermissionReason;->reason:Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionReasonTypes;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
