.class abstract Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentRef;
.super Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;
.source "$AutoValue_TournamentNotificationDataTypes_TournamentRef.java"


# instance fields
.field private final organizer:Ljava/lang/String;

.field private final tournamentId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "organizer"    # Ljava/lang/String;
    .param p2, "tournamentId"    # Ljava/lang/String;

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;-><init>()V

    .line 16
    if-nez p1, :cond_0

    .line 17
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null organizer"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentRef;->organizer:Ljava/lang/String;

    .line 20
    if-nez p2, :cond_1

    .line 21
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null tournamentId"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 23
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentRef;->tournamentId:Ljava/lang/String;

    .line 24
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 48
    if-ne p1, p0, :cond_1

    .line 56
    :cond_0
    :goto_0
    return v1

    .line 51
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 52
    check-cast v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    .line 53
    .local v0, "that":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;
    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentRef;->organizer:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;->organizer()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentRef;->tournamentId:Ljava/lang/String;

    .line 54
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;->tournamentId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;
    :cond_3
    move v1, v2

    .line 56
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 61
    const/4 v0, 0x1

    .line 62
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 63
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentRef;->organizer:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 64
    mul-int/2addr v0, v2

    .line 65
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentRef;->tournamentId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 66
    return v0
.end method

.method public organizer()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentRef;->organizer:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TournamentRef{organizer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentRef;->organizer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tournamentId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentRef;->tournamentId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public tournamentId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/$AutoValue_TournamentNotificationDataTypes_TournamentRef;->tournamentId:Ljava/lang/String;

    return-object v0
.end method
