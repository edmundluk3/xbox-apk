.class public Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;
.super Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;
.source "TrendingTagSelectorScreenViewModel.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private scid:Ljava/lang/String;

.field private final tagsModelManager:Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;J)V
    .locals 4
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "titleId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/tags/TagSelectorViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 26
    sget-object v1, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;->tagsModelManager:Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;

    .line 31
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 32
    const-wide/16 v2, 0x1

    invoke-static {v2, v3, p2, p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 34
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getResult(J)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v0

    .line 35
    .local v0, "titleData":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->serviceConfigId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 36
    iget-object v1, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->serviceConfigId:Ljava/lang/String;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;->scid:Ljava/lang/String;

    .line 37
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 41
    :goto_0
    return-void

    .line 39
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method

.method private onTrendingTagsChanged(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 100
    sget-object v0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 116
    :goto_0
    return-void

    .line 104
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;->tags:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;->tags:Ljava/util/List;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;->tagsModelManager:Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;->scid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;->getTrendingTags(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;->tags:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 108
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;->updateAdapter()V

    goto :goto_0

    .line 106
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 113
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Social tags changed to invalid state. Using old data"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 100
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getErrorString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 65
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070702

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNoContentString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070703

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return v0
.end method

.method public load(Z)V
    .locals 3
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 75
    sget-object v0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "load forceRefresh: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;->scid:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;->tagsModelManager:Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;->scid:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;->loadAsync(ZLjava/lang/String;)V

    .line 81
    :cond_0
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;->tagsModelManager:Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 51
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;->tagsModelManager:Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/tags/TrendingTagsModelManager;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 56
    return-void
.end method

.method public setTagSelectedListener(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;)V
    .locals 1
    .param p1, "tagSelectedListener"    # Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v0, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/tags/TagSelectorScreenAdapter;->setSelectedTagListener(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;)V

    .line 46
    return-void
.end method

.method public updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 85
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    sget-object v2, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;->TAG:Ljava/lang/String;

    const-string v3, "updateOverride"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    move-object v0, v2

    .line 88
    .local v0, "result":Lcom/microsoft/xbox/service/model/UpdateData;
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 89
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v1

    .line 91
    .local v1, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v2, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 97
    .end local v1    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :cond_0
    :goto_1
    return-void

    .line 87
    .end local v0    # "result":Lcom/microsoft/xbox/service/model/UpdateData;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 93
    .restart local v0    # "result":Lcom/microsoft/xbox/service/model/UpdateData;
    .restart local v1    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :pswitch_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/tags/TrendingTagSelectorScreenViewModel;->onTrendingTagsChanged(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    goto :goto_1

    .line 91
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
