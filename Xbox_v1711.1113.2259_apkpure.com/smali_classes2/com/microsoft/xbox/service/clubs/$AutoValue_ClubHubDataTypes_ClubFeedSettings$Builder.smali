.class final Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubFeedSettings$Builder;
.super Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;
.source "$AutoValue_ClubHubDataTypes_ClubFeedSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubFeedSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# instance fields
.field private post:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;"
        }
    .end annotation
.end field

.field private view:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;-><init>()V

    .line 72
    return-void
.end method

.method constructor <init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;)V
    .locals 1
    .param p1, "source"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;-><init>()V

    .line 74
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;->post()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubFeedSettings$Builder;->post:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 75
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubFeedSettings$Builder;->view:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 76
    return-void
.end method


# virtual methods
.method public build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;
    .locals 3

    .prologue
    .line 89
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubFeedSettings;

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubFeedSettings$Builder;->post:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    iget-object v2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubFeedSettings$Builder;->view:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubFeedSettings;-><init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)V

    return-object v0
.end method

.method public post(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;"
        }
    .end annotation

    .prologue
    .line 79
    .local p1, "post":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubFeedSettings$Builder;->post:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 80
    return-object p0
.end method

.method public view(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;
    .locals 0
    .param p1    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;"
        }
    .end annotation

    .prologue
    .line 84
    .local p1, "view":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubFeedSettings$Builder;->view:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    .line 85
    return-object p0
.end method
