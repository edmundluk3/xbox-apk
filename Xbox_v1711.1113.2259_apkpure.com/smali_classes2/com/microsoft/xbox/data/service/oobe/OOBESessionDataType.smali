.class public Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType;
.super Ljava/lang/Object;
.source "OOBESessionDataType.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;,
        Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBERequestBody;,
        Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;,
        Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OffConsoleStatus;
    }
.end annotation


# static fields
.field private static final CLIENT_VERSION:Ljava/lang/String; = "1.0"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
