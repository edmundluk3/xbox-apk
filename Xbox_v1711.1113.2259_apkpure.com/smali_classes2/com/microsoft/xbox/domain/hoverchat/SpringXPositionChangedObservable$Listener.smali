.class final Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable$Listener;
.super Ljava/lang/Object;
.source "SpringXPositionChangedObservable.java"

# interfaces
.implements Lio/reactivex/disposables/Disposable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "Listener"
.end annotation


# instance fields
.field private final spring:Lcom/facebook/rebound/Spring;

.field private final springListener:Lcom/facebook/rebound/SpringListener;

.field final synthetic this$0:Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable;

.field private final unsubscribed:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable;Lcom/facebook/rebound/Spring;Lio/reactivex/Observer;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable;
    .param p2, "spring"    # Lcom/facebook/rebound/Spring;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/rebound/Spring;",
            "Lio/reactivex/Observer",
            "<-",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p3, "observer":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-Ljava/lang/Object;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable$Listener;->this$0:Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable$Listener;->unsubscribed:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 39
    iput-object p2, p0, Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable$Listener;->spring:Lcom/facebook/rebound/Spring;

    .line 40
    new-instance v0, Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable$Listener$1;

    invoke-direct {v0, p0, p1, p3}, Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable$Listener$1;-><init>(Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable$Listener;Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable;Lio/reactivex/Observer;)V

    iput-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable$Listener;->springListener:Lcom/facebook/rebound/SpringListener;

    .line 58
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable$Listener;)Lcom/facebook/rebound/SpringListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable$Listener;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable$Listener;->springListener:Lcom/facebook/rebound/SpringListener;

    return-object v0
.end method


# virtual methods
.method public dispose()V
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable$Listener;->unsubscribed:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable$Listener;->spring:Lcom/facebook/rebound/Spring;

    iget-object v1, p0, Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable$Listener;->springListener:Lcom/facebook/rebound/SpringListener;

    invoke-virtual {v0, v1}, Lcom/facebook/rebound/Spring;->removeListener(Lcom/facebook/rebound/SpringListener;)Lcom/facebook/rebound/Spring;

    .line 65
    :cond_0
    return-void
.end method

.method public isDisposed()Z
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/domain/hoverchat/SpringXPositionChangedObservable$Listener;->unsubscribed:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method
