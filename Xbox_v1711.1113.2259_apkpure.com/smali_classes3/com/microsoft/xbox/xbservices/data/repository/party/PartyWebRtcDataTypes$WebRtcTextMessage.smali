.class public abstract Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcTextMessage;
.super Ljava/lang/Object;
.source "PartyWebRtcDataTypes.java"

# interfaces
.implements Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$PartyWebRtcEvent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "WebRtcTextMessage"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcDataTypes$WebRtcTextMessage;
    .locals 1
    .param p0, "fromIndex"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "textMessage"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 41
    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 42
    invoke-static {p1}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 44
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/AutoValue_PartyWebRtcDataTypes_WebRtcTextMessage;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/AutoValue_PartyWebRtcDataTypes_WebRtcTextMessage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public abstract fromIndex()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract textMessage()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
