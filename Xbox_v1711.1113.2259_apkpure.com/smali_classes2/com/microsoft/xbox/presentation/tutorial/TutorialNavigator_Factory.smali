.class public final Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator_Factory;
.super Ljava/lang/Object;
.source "TutorialNavigator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final facebookManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;",
            ">;"
        }
    .end annotation
.end field

.field private final navigationManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/NavigationManager;",
            ">;"
        }
    .end annotation
.end field

.field private final telemetryServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator_Factory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator_Factory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/NavigationManager;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p1, "navigationManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/ui/NavigationManager;>;"
    .local p2, "facebookManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;>;"
    .local p3, "tutorialRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;>;"
    .local p4, "telemetryServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    sget-boolean v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 30
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator_Factory;->navigationManagerProvider:Ljavax/inject/Provider;

    .line 31
    sget-boolean v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 32
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator_Factory;->facebookManagerProvider:Ljavax/inject/Provider;

    .line 33
    sget-boolean v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 34
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator_Factory;->tutorialRepositoryProvider:Ljavax/inject/Provider;

    .line 35
    sget-boolean v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 36
    :cond_3
    iput-object p4, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator_Factory;->telemetryServiceProvider:Ljavax/inject/Provider;

    .line 37
    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/NavigationManager;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    .local p0, "navigationManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/ui/NavigationManager;>;"
    .local p1, "facebookManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;>;"
    .local p2, "tutorialRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;>;"
    .local p3, "telemetryServiceProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;>;"
    new-instance v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;
    .locals 5

    .prologue
    .line 41
    new-instance v4, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator_Factory;->navigationManagerProvider:Ljavax/inject/Provider;

    .line 42
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator_Factory;->facebookManagerProvider:Ljavax/inject/Provider;

    .line 43
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator_Factory;->tutorialRepositoryProvider:Ljavax/inject/Provider;

    .line 44
    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator_Factory;->telemetryServiceProvider:Ljavax/inject/Provider;

    .line 45
    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;-><init>(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;Lcom/microsoft/xbox/data/service/tutorial/TutorialTelemetryService;)V

    .line 41
    return-object v4
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator_Factory;->get()Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;

    move-result-object v0

    return-object v0
.end method
