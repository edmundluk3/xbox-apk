.class public final Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_EditorialDataTypes_AchievementTag.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;",
        ">;"
    }
.end annotation


# instance fields
.field private defaultGamerscore:Ljava/lang/String;

.field private defaultId:Ljava/lang/String;

.field private defaultName:Ljava/lang/String;

.field private final gamerscoreAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final idAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final nameAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 21
    iput-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag$GsonTypeAdapter;->defaultId:Ljava/lang/String;

    .line 22
    iput-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag$GsonTypeAdapter;->defaultName:Ljava/lang/String;

    .line 23
    iput-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag$GsonTypeAdapter;->defaultGamerscore:Ljava/lang/String;

    .line 25
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag$GsonTypeAdapter;->idAdapter:Lcom/google/gson/TypeAdapter;

    .line 26
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag$GsonTypeAdapter;->nameAdapter:Lcom/google/gson/TypeAdapter;

    .line 27
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag$GsonTypeAdapter;->gamerscoreAdapter:Lcom/google/gson/TypeAdapter;

    .line 28
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;
    .locals 6
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v4

    sget-object v5, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v4, v5, :cond_0

    .line 60
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 61
    const/4 v4, 0x0

    .line 92
    :goto_0
    return-object v4

    .line 63
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 64
    iget-object v2, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag$GsonTypeAdapter;->defaultId:Ljava/lang/String;

    .line 65
    .local v2, "id":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag$GsonTypeAdapter;->defaultName:Ljava/lang/String;

    .line 66
    .local v3, "name":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag$GsonTypeAdapter;->defaultGamerscore:Ljava/lang/String;

    .line 67
    .local v1, "gamerscore":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 68
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 69
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v4

    sget-object v5, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v4, v5, :cond_1

    .line 70
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 73
    :cond_1
    const/4 v4, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v4, :pswitch_data_0

    .line 87
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 73
    :sswitch_0
    const-string v5, "id"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x0

    goto :goto_2

    :sswitch_1
    const-string v5, "name"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x1

    goto :goto_2

    :sswitch_2
    const-string v5, "gamerscore"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x2

    goto :goto_2

    .line 75
    :pswitch_0
    iget-object v4, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag$GsonTypeAdapter;->idAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v4, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "id":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 76
    .restart local v2    # "id":Ljava/lang/String;
    goto :goto_1

    .line 79
    :pswitch_1
    iget-object v4, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag$GsonTypeAdapter;->nameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v4, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "name":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 80
    .restart local v3    # "name":Ljava/lang/String;
    goto :goto_1

    .line 83
    :pswitch_2
    iget-object v4, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag$GsonTypeAdapter;->gamerscoreAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v4, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "gamerscore":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 84
    .restart local v1    # "gamerscore":Ljava/lang/String;
    goto :goto_1

    .line 91
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 92
    new-instance v4, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag;

    invoke-direct {v4, v2, v3, v1}, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 73
    nop

    :sswitch_data_0
    .sparse-switch
        0xd1b -> :sswitch_0
        0x337a8b -> :sswitch_1
        0x5a3bc3d2 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultGamerscore(Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultGamerscore"    # Ljava/lang/String;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag$GsonTypeAdapter;->defaultGamerscore:Ljava/lang/String;

    .line 39
    return-object p0
.end method

.method public setDefaultId(Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultId"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag$GsonTypeAdapter;->defaultId:Ljava/lang/String;

    .line 31
    return-object p0
.end method

.method public setDefaultName(Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultName"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag$GsonTypeAdapter;->defaultName:Ljava/lang/String;

    .line 35
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    if-nez p2, :cond_0

    .line 45
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 56
    :goto_0
    return-void

    .line 48
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 49
    const-string v0, "id"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag$GsonTypeAdapter;->idAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;->id()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 51
    const-string v0, "name"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag$GsonTypeAdapter;->nameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 53
    const-string v0, "gamerscore"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag$GsonTypeAdapter;->gamerscoreAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;->gamerscore()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 55
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17
    check-cast p2, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_AchievementTag$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$AchievementTag;)V

    return-void
.end method
