.class Lcom/microsoft/playready/ReactiveLicenseAcquirer;
.super Lcom/microsoft/playready/LicenseAcquirerBase;
.source "LicenseAcquirer.java"

# interfaces
.implements Lcom/microsoft/playready/IReactiveLicenseAcquirer;


# instance fields
.field private final mExecService:Ljava/util/concurrent/ExecutorService;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/microsoft/playready/LicenseAcquirerBase;-><init>()V

    .line 84
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/playready/ReactiveLicenseAcquirer;->mExecService:Ljava/util/concurrent/ExecutorService;

    .line 94
    new-instance v0, Lcom/microsoft/playready/SimpleLicenseAcquisitionPlugin;

    invoke-direct {v0}, Lcom/microsoft/playready/SimpleLicenseAcquisitionPlugin;-><init>()V

    invoke-virtual {p0, v0}, Lcom/microsoft/playready/ReactiveLicenseAcquirer;->setLicenseAcquisitionPlugin(Lcom/microsoft/playready/ILicenseAcquisitionPlugin;)V

    .line 95
    return-void
.end method


# virtual methods
.method public acquireLicense(Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionContext;)Lcom/microsoft/playready/ILicenseAcquisitionTask;
    .locals 3
    .param p1, "context"    # Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionContext;

    .prologue
    .line 118
    new-instance v1, Lcom/microsoft/playready/ReactiveLicenseAcquisitionWorker;

    .line 120
    invoke-virtual {p0}, Lcom/microsoft/playready/ReactiveLicenseAcquirer;->getLicenseAcquisitionPlugin()Lcom/microsoft/playready/ILicenseAcquisitionPlugin;

    move-result-object v2

    .line 118
    invoke-direct {v1, p1, v2}, Lcom/microsoft/playready/ReactiveLicenseAcquisitionWorker;-><init>(Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionContext;Lcom/microsoft/playready/ILicenseAcquisitionPlugin;)V

    .line 122
    .local v1, "worker":Lcom/microsoft/playready/ReactiveLicenseAcquisitionWorker;
    new-instance v0, Lcom/microsoft/playready/LicenseAcquisitionTask;

    invoke-direct {v0, v1}, Lcom/microsoft/playready/LicenseAcquisitionTask;-><init>(Lcom/microsoft/playready/ReactiveLicenseAcquisitionWorker;)V

    .line 123
    .local v0, "task":Lcom/microsoft/playready/LicenseAcquisitionTask;
    invoke-virtual {v0, p0}, Lcom/microsoft/playready/LicenseAcquisitionTask;->addLicenseAcquirerListener(Lcom/microsoft/playready/ILicenseAcquirerListener;)V

    .line 124
    iget-object v2, p0, Lcom/microsoft/playready/ReactiveLicenseAcquirer;->mExecService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v2, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 126
    return-object v0
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 103
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/ReactiveLicenseAcquirer;->mExecService:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 109
    return-void

    .line 106
    :catchall_0
    move-exception v0

    .line 107
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 108
    throw v0
.end method
