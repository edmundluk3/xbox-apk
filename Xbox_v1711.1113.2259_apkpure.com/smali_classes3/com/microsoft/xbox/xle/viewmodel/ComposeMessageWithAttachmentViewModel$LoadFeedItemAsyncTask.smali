.class Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "ComposeMessageWithAttachmentViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadFeedItemAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final model:Lcom/microsoft/xbox/service/model/entity/EntityModel;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;)V
    .locals 1

    .prologue
    .line 296
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 298
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->access$100(Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;)Lcom/microsoft/xbox/service/model/entity/EntityModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;->model:Lcom/microsoft/xbox/service/model/entity/EntityModel;

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$1;

    .prologue
    .line 296
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;->model:Lcom/microsoft/xbox/service/model/entity/EntityModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->shouldRefresh()Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 321
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;->model:Lcom/microsoft/xbox/service/model/entity/EntityModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->loadSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 296
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 312
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 296
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 308
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 326
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 327
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 296
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel$LoadFeedItemAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 317
    return-void
.end method
