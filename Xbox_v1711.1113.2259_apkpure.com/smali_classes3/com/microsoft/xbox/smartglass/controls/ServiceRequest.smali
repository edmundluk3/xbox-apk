.class Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;
.super Ljava/lang/Object;
.source "ServiceRequest.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static _cookieStore:Lorg/apache/http/client/CookieStore;

.field private static _executor:Ljava/util/concurrent/Executor;


# instance fields
.field private _listener:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;

.field private _request:Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;

.field private _response:Lcom/microsoft/xbox/smartglass/controls/ServiceResponse;

.field private _tokenResult:Lcom/microsoft/xbox/smartglass/TokenResult;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lorg/apache/http/impl/client/BasicCookieStore;

    invoke-direct {v0}, Lorg/apache/http/impl/client/BasicCookieStore;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_cookieStore:Lorg/apache/http/client/CookieStore;

    .line 44
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_executor:Ljava/util/concurrent/Executor;

    .line 45
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;)V
    .locals 1
    .param p1, "request"    # Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;
    .param p2, "listener"    # Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_request:Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_response:Lcom/microsoft/xbox/smartglass/controls/ServiceResponse;

    .line 50
    iput-object p2, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_listener:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;

    .line 51
    return-void
.end method

.method static synthetic access$002(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;Lcom/microsoft/xbox/smartglass/TokenResult;)Lcom/microsoft/xbox/smartglass/TokenResult;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;
    .param p1, "x1"    # Lcom/microsoft/xbox/smartglass/TokenResult;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_tokenResult:Lcom/microsoft/xbox/smartglass/TokenResult;

    return-object p1
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;)Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_listener:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;)Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_request:Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;

    return-object v0
.end method

.method private runInternal()Ljava/lang/String;
    .locals 15

    .prologue
    const/4 v11, 0x0

    .line 74
    iget-object v12, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_request:Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;

    iget v12, v12, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->timeout:I

    invoke-static {v12}, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->create(I)Lcom/microsoft/xbox/smartglass/controls/HttpClient;

    move-result-object v0

    .line 77
    .local v0, "client":Lcom/microsoft/xbox/smartglass/controls/HttpClient;
    :try_start_0
    iget-object v12, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_request:Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;

    iget-object v2, v12, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->data:Ljava/lang/String;

    .line 78
    .local v2, "data":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_0

    const/4 v11, 0x1

    :cond_0
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 79
    .local v3, "doOutput":Ljava/lang/Boolean;
    const/4 v5, 0x0

    .line 80
    .local v5, "entity":Lorg/apache/http/entity/StringEntity;
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 81
    new-instance v5, Lorg/apache/http/entity/StringEntity;

    .end local v5    # "entity":Lorg/apache/http/entity/StringEntity;
    const-string v11, "UTF-8"

    invoke-direct {v5, v2, v11}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    .restart local v5    # "entity":Lorg/apache/http/entity/StringEntity;
    iget-object v11, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_request:Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;

    iget-object v11, v11, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->contentType:Ljava/lang/String;

    invoke-virtual {v5, v11}, Lorg/apache/http/entity/StringEntity;->setContentType(Ljava/lang/String;)V

    .line 86
    :cond_1
    iget-object v11, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_request:Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;

    iget-object v11, v11, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->method:Ljava/lang/String;

    iget-object v12, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_request:Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;

    iget-object v12, v12, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->url:Ljava/lang/String;

    invoke-static {v11, v12, v5}, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->getHttpMethod(Ljava/lang/String;Ljava/lang/String;Lorg/apache/http/entity/StringEntity;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v10

    .line 87
    .local v10, "method":Lorg/apache/http/client/methods/HttpUriRequest;
    if-nez v10, :cond_3

    .line 88
    const-string v9, "Unsupported http method."

    .line 151
    .end local v2    # "data":Ljava/lang/String;
    .end local v3    # "doOutput":Ljava/lang/Boolean;
    .end local v5    # "entity":Lorg/apache/http/entity/StringEntity;
    .end local v10    # "method":Lorg/apache/http/client/methods/HttpUriRequest;
    :cond_2
    :goto_0
    return-object v9

    .line 92
    .restart local v2    # "data":Ljava/lang/String;
    .restart local v3    # "doOutput":Ljava/lang/Boolean;
    .restart local v5    # "entity":Lorg/apache/http/entity/StringEntity;
    .restart local v10    # "method":Lorg/apache/http/client/methods/HttpUriRequest;
    :cond_3
    instance-of v11, v10, Lorg/apache/http/client/methods/HttpGet;

    if-eqz v11, :cond_4

    iget-object v11, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_request:Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;

    iget-object v11, v11, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->data:Ljava/lang/String;

    if-eqz v11, :cond_4

    iget-object v11, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_request:Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;

    iget-object v11, v11, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->data:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_4

    .line 93
    const-string v9, "Cannot send a content-body with this verb-type."

    goto :goto_0

    .line 97
    :cond_4
    iget-object v11, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_request:Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;

    iget-object v8, v11, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->headers:Lorg/json/JSONObject;

    .line 98
    .local v8, "headers":Lorg/json/JSONObject;
    if-eqz v8, :cond_5

    .line 99
    invoke-virtual {v8}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "fields":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 100
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 101
    .local v6, "field":Ljava/lang/String;
    invoke-virtual {v8, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v6, v11}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 141
    .end local v2    # "data":Ljava/lang/String;
    .end local v3    # "doOutput":Ljava/lang/Boolean;
    .end local v5    # "entity":Lorg/apache/http/entity/StringEntity;
    .end local v6    # "field":Ljava/lang/String;
    .end local v7    # "fields":Ljava/util/Iterator;, "Ljava/util/Iterator<*>;"
    .end local v8    # "headers":Lorg/json/JSONObject;
    .end local v10    # "method":Lorg/apache/http/client/methods/HttpUriRequest;
    :catch_0
    move-exception v4

    .line 142
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    .line 143
    .local v9, "message":Ljava/lang/String;
    if-nez v9, :cond_2

    .line 144
    const-string v9, "Unknown failure."

    goto :goto_0

    .line 106
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v9    # "message":Ljava/lang/String;
    .restart local v2    # "data":Ljava/lang/String;
    .restart local v3    # "doOutput":Ljava/lang/Boolean;
    .restart local v5    # "entity":Lorg/apache/http/entity/StringEntity;
    .restart local v8    # "headers":Lorg/json/JSONObject;
    .restart local v10    # "method":Lorg/apache/http/client/methods/HttpUriRequest;
    :cond_5
    :try_start_1
    iget-object v11, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_request:Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;

    iget-object v11, v11, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->withCredentials:Ljava/lang/Boolean;

    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    if-eqz v11, :cond_6

    sget-object v1, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_cookieStore:Lorg/apache/http/client/CookieStore;

    .line 107
    .local v1, "cookies":Lorg/apache/http/client/CookieStore;
    :goto_2
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->setCookieStore(Lorg/apache/http/client/CookieStore;)V

    .line 110
    iget-object v11, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_request:Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;

    iget-object v11, v11, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->sendUserToken:Ljava/lang/Boolean;

    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    if-eqz v11, :cond_7

    .line 111
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTokenManager()Lcom/microsoft/xbox/smartglass/TokenManager;

    move-result-object v11

    iget-object v12, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_request:Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;

    iget-object v12, v12, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->audienceUri:Ljava/lang/String;

    const/4 v13, 0x0

    new-instance v14, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest$1;

    invoke-direct {v14, p0, v10, v0}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest$1;-><init>(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;Lorg/apache/http/client/methods/HttpUriRequest;Lcom/microsoft/xbox/smartglass/controls/HttpClient;)V

    invoke-virtual {v11, v12, v13, v14}, Lcom/microsoft/xbox/smartglass/TokenManager;->getToken(Ljava/lang/String;ZLcom/microsoft/xbox/smartglass/TokenListener;)Lcom/microsoft/xbox/smartglass/TokenResult;

    move-result-object v11

    iput-object v11, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_tokenResult:Lcom/microsoft/xbox/smartglass/TokenResult;

    .line 151
    :goto_3
    const/4 v9, 0x0

    goto :goto_0

    .line 106
    .end local v1    # "cookies":Lorg/apache/http/client/CookieStore;
    :cond_6
    new-instance v1, Lorg/apache/http/impl/client/BasicCookieStore;

    invoke-direct {v1}, Lorg/apache/http/impl/client/BasicCookieStore;-><init>()V

    goto :goto_2

    .line 139
    .restart local v1    # "cookies":Lorg/apache/http/client/CookieStore;
    :cond_7
    invoke-virtual {p0, v0, v10}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->processRequest(Lcom/microsoft/xbox/smartglass/controls/HttpClient;Lorg/apache/http/client/methods/HttpUriRequest;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 62
    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_listener:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;

    .line 63
    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_tokenResult:Lcom/microsoft/xbox/smartglass/TokenResult;

    .line 64
    return-void
.end method

.method public getResponse()Lcom/microsoft/xbox/smartglass/controls/ServiceResponse;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_response:Lcom/microsoft/xbox/smartglass/controls/ServiceResponse;

    return-object v0
.end method

.method processRequest(Lcom/microsoft/xbox/smartglass/controls/HttpClient;Lorg/apache/http/client/methods/HttpUriRequest;)V
    .locals 7
    .param p1, "client"    # Lcom/microsoft/xbox/smartglass/controls/HttpClient;
    .param p2, "method"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 155
    invoke-virtual {p1, p2}, Lcom/microsoft/xbox/smartglass/controls/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;

    move-result-object v1

    .line 158
    .local v1, "httpResponse":Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;
    const-string v3, ""

    .line 159
    .local v3, "responseBody":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;->getStream()Ljava/io/InputStream;

    move-result-object v2

    .line 160
    .local v2, "in":Ljava/io/InputStream;
    if-eqz v2, :cond_0

    .line 161
    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/controls/InputStreamUtils;->readToEnd(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 162
    .local v0, "buffer":[B
    new-instance v3, Ljava/lang/String;

    .end local v3    # "responseBody":Ljava/lang/String;
    const-string v4, "UTF-8"

    invoke-direct {v3, v0, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 163
    .restart local v3    # "responseBody":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 167
    .end local v0    # "buffer":[B
    :cond_0
    new-instance v4, Lcom/microsoft/xbox/smartglass/controls/ServiceResponse;

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;->getStatusCode()I

    move-result v5

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/controls/SGHttpResponse;->getHeaderMap()Ljava/util/Map;

    move-result-object v6

    invoke-direct {v4, v5, v3, v6}, Lcom/microsoft/xbox/smartglass/controls/ServiceResponse;-><init>(ILjava/lang/String;Ljava/util/Map;)V

    iput-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_response:Lcom/microsoft/xbox/smartglass/controls/ServiceResponse;

    .line 170
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_listener:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;

    if-eqz v4, :cond_1

    .line 171
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_listener:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;

    invoke-virtual {v4, p0}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;->onComplete(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;)V

    .line 173
    :cond_1
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->runInternal()Ljava/lang/String;

    move-result-object v0

    .line 68
    .local v0, "error":Ljava/lang/String;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_listener:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;

    if-eqz v1, :cond_0

    .line 69
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_listener:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;

    invoke-virtual {v1, p0, v0}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;->onError(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;Ljava/lang/String;)V

    .line 71
    :cond_0
    return-void
.end method

.method public runAsync()V
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->_executor:Ljava/util/concurrent/Executor;

    invoke-interface {v0, p0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 55
    return-void
.end method
