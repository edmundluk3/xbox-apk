.class Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$LoadUserPrivacySettingsAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "SettingsGeneralPageViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadUserPrivacySettingsAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field privacyResult:Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)V
    .locals 0

    .prologue
    .line 709
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$LoadUserPrivacySettingsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$1;

    .prologue
    .line 709
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$LoadUserPrivacySettingsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 714
    const/4 v0, 0x0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 740
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getUserProfilePrivacySettings()Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$LoadUserPrivacySettingsAsyncTask;->privacyResult:Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 745
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    :goto_0
    return-object v1

    .line 741
    :catch_0
    move-exception v0

    .line 742
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    const-string v1, "SettingsActivityViewModel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to get user privacy settings: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 743
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 709
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$LoadUserPrivacySettingsAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 734
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 709
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$LoadUserPrivacySettingsAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 719
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 729
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$LoadUserPrivacySettingsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$LoadUserPrivacySettingsAsyncTask;->privacyResult:Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$1300(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/privacy/PrivacySettingsResult;)V

    .line 730
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 709
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$LoadUserPrivacySettingsAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 723
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 724
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel$LoadUserPrivacySettingsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->access$1202(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;Z)Z

    .line 725
    return-void
.end method
