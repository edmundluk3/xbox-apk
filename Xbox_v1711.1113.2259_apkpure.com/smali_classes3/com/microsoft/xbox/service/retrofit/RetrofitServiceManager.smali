.class public final enum Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;
.super Ljava/lang/Enum;
.source "RetrofitServiceManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;


# instance fields
.field private final factoryMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class;",
            "Lcom/microsoft/xbox/service/retrofit/RetrofitFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final instanceMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final stubMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 18
    new-instance v0, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;->INSTANCE:Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;

    .line 16
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;

    sget-object v1, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;->INSTANCE:Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;->$VALUES:[Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;->instanceMap:Ljava/util/Map;

    .line 26
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;->stubMap:Ljava/util/Map;

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;->factoryMap:Ljava/util/Map;

    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;->factoryMap:Ljava/util/Map;

    const-class v1, Lcom/microsoft/xbox/service/dlAssets/DLAssetsService;

    new-instance v2, Lcom/microsoft/xbox/service/dlAssets/DLAssetsServiceFactory;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/dlAssets/DLAssetsServiceFactory;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;->factoryMap:Ljava/util/Map;

    const-class v1, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    new-instance v2, Lcom/microsoft/xbox/service/mediaHub/MediaHubServiceFactory;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubServiceFactory;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 16
    const-class v0, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;->$VALUES:[Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized get(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 34
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;->instanceMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 36
    .local v0, "instance":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 37
    iget-object v1, p0, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;->factoryMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/retrofit/RetrofitFactory;

    invoke-interface {v1}, Lcom/microsoft/xbox/service/retrofit/RetrofitFactory;->getRetrofit()Lretrofit2/Retrofit;

    move-result-object v1

    invoke-virtual {v1, p1}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 38
    iget-object v1, p0, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;->instanceMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 41
    :cond_0
    monitor-exit p0

    return-object v0

    .line 34
    .end local v0    # "instance":Ljava/lang/Object;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getStub(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 45
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    monitor-enter p0

    :try_start_0
    iget-object v6, p0, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;->stubMap:Ljava/util/Map;

    invoke-interface {v6, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 47
    .local v5, "stub":Ljava/lang/Object;
    if-nez v5, :cond_0

    .line 48
    iget-object v6, p0, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;->factoryMap:Ljava/util/Map;

    invoke-interface {v6, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/retrofit/RetrofitFactory;

    .line 49
    .local v2, "factory":Lcom/microsoft/xbox/service/retrofit/RetrofitFactory;
    invoke-interface {v2}, Lcom/microsoft/xbox/service/retrofit/RetrofitFactory;->getRetrofit()Lretrofit2/Retrofit;

    move-result-object v4

    .line 51
    .local v4, "retrofit":Lretrofit2/Retrofit;
    invoke-static {}, Lretrofit2/mock/NetworkBehavior;->create()Lretrofit2/mock/NetworkBehavior;

    move-result-object v0

    .line 52
    .local v0, "behavior":Lretrofit2/mock/NetworkBehavior;
    new-instance v6, Lretrofit2/mock/MockRetrofit$Builder;

    invoke-direct {v6, v4}, Lretrofit2/mock/MockRetrofit$Builder;-><init>(Lretrofit2/Retrofit;)V

    .line 53
    invoke-virtual {v6, v0}, Lretrofit2/mock/MockRetrofit$Builder;->networkBehavior(Lretrofit2/mock/NetworkBehavior;)Lretrofit2/mock/MockRetrofit$Builder;

    move-result-object v6

    .line 54
    invoke-virtual {v6}, Lretrofit2/mock/MockRetrofit$Builder;->build()Lretrofit2/mock/MockRetrofit;

    move-result-object v3

    .line 56
    .local v3, "mockRetrofit":Lretrofit2/mock/MockRetrofit;
    invoke-virtual {v3, p1}, Lretrofit2/mock/MockRetrofit;->create(Ljava/lang/Class;)Lretrofit2/mock/BehaviorDelegate;

    move-result-object v1

    .line 58
    .local v1, "delegate":Lretrofit2/mock/BehaviorDelegate;, "Lretrofit2/mock/BehaviorDelegate<TT;>;"
    invoke-interface {v2, v1}, Lcom/microsoft/xbox/service/retrofit/RetrofitFactory;->getStubInstance(Lretrofit2/mock/BehaviorDelegate;)Ljava/lang/Object;

    move-result-object v5

    .line 60
    iget-object v6, p0, Lcom/microsoft/xbox/service/retrofit/RetrofitServiceManager;->stubMap:Ljava/util/Map;

    invoke-interface {v6, p1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    .end local v0    # "behavior":Lretrofit2/mock/NetworkBehavior;
    .end local v1    # "delegate":Lretrofit2/mock/BehaviorDelegate;, "Lretrofit2/mock/BehaviorDelegate<TT;>;"
    .end local v2    # "factory":Lcom/microsoft/xbox/service/retrofit/RetrofitFactory;
    .end local v3    # "mockRetrofit":Lretrofit2/mock/MockRetrofit;
    .end local v4    # "retrofit":Lretrofit2/Retrofit;
    :cond_0
    monitor-exit p0

    return-object v5

    .line 45
    .end local v5    # "stub":Ljava/lang/Object;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6
.end method
