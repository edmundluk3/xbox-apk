.class public Lcom/microsoft/xbox/service/model/gcm/GcmModel;
.super Lcom/microsoft/xbox/toolkit/XLEObservable;
.source "GcmModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;,
        Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;,
        Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;,
        Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;,
        Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;,
        Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;,
        Lcom/microsoft/xbox/service/model/gcm/GcmModel$EnsureRegistrationIdTask;,
        Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;,
        Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;,
        Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;,
        Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEObservable",
        "<",
        "Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static final BACKOFF_SECONDS:[I

.field private static final CHAT_SERVICE:Lcom/microsoft/xbox/service/chat/IChatService;

.field private static final CLUB_CHAT_MANAGEMENT_SERVICE:Lcom/microsoft/xbox/service/clubs/IClubChatManagementService;

.field private static final DEFAULT_USER_FLAGS:I

.field private static final ES_SERVICE_MANAGER:Lcom/microsoft/xbox/service/network/managers/IESServiceManager;

.field private static final PROPERTIES_FILE:Ljava/lang/String; = "gcm.properties"

.field private static final PROP_APP_VERSION:Ljava/lang/String; = "app.version"

.field private static final PROP_CHAT_ALL_MESSAGES_CHANNELS:Ljava/lang/String; = "chat.am.channels"

.field private static final PROP_CHAT_DIRECT_MENTION_CHANNELS:Ljava/lang/String; = "chat.dm.channels"

.field private static final PROP_CHAT_NO_NOTIFICATION_CHANNELS:Ljava/lang/String; = "chat.no.channels"

.field private static final PROP_EDF_NODE_ID:Ljava/lang/String; = "edf.node.id"

.field private static final PROP_EDF_REG_ID:Ljava/lang/String; = "edf.reg.id"

.field private static final PROP_ENDPOINT_FLAGS:Ljava/lang/String; = "endpoint.flags"

.field private static final PROP_ENDPOINT_ID:Ljava/lang/String; = "endpoint.id"

.field private static final PROP_LAST_EDF_REG_TIME:Ljava/lang/String; = "last.edf.registration.time.03"

.field private static final PROP_LAST_SLS_REG_TIME:Ljava/lang/String; = "last.sls.registration.time.04"

.field private static final PROP_REG_ID:Ljava/lang/String; = "registration.id"

.field private static final PROP_SKYPE_REG_TOKEN:Ljava/lang/String; = "skype.reg.token"

.field private static final PROP_SYSTEM_ID:Ljava/lang/String; = "system.id"

.field private static final PROP_USER_FLAGS:Ljava/lang/String; = "user.flags"

.field private static final SENDER_ID:Ljava/lang/String; = "86584527366"

.field private static final SKYPE_REG_TIMEOUT:J = 0x9a7ec800L

.field private static final SLS_REG_TIMEOUT:J = 0x337f9800L

.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

.field private static final lock:Ljava/lang/Object;


# instance fields
.field private final ctx:Landroid/content/Context;

.field private final disp:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

.field private ensureRegistrationIdTask:Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

.field private final prefs:Landroid/content/SharedPreferences;

.field private registerChatTask:Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

.field private registerSLSTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private registerSkypeTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private skypeRegTokenWithEndpoint:Ljava/lang/String;

.field private unregisterChatTask:Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

.field private unregisterSLSTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private unregisterSkypeTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 69
    const-class v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->TAG:Ljava/lang/String;

    .line 70
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->BACKOFF_SECONDS:[I

    .line 73
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->FAV_BROADCAST:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 74
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v0

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->MESSAGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 75
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v1

    or-int/2addr v0, v1

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->LFG:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 76
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v1

    or-int/2addr v0, v1

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_NEW_MEMBER:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 77
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v1

    or-int/2addr v0, v1

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_PROMOTION:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 78
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v1

    or-int/2addr v0, v1

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_DEMOTION:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 79
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v1

    or-int/2addr v0, v1

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_MODERATION_INVITE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 80
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v1

    or-int/2addr v0, v1

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_RECOMMENDATION:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 81
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v1

    or-int/2addr v0, v1

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_INVITED:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 82
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v1

    or-int/2addr v0, v1

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_JOINED:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 83
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v1

    or-int/2addr v0, v1

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_TRANSFER:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 84
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v1

    or-int/2addr v0, v1

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->CLUB_MODERATION_REPORT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 85
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v1

    or-int/2addr v0, v1

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->ACHIEVEMENT:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 86
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v1

    or-int/2addr v0, v1

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_MATCH:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 87
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v1

    or-int/2addr v0, v1

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_STATUS_CHANGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 88
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v1

    or-int/2addr v0, v1

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_TEAM_STATUS_CHANGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 89
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v1

    or-int/2addr v0, v1

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->TOURNAMENT_TEAM_INVITE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .line 90
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v1

    or-int/2addr v0, v1

    sput v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->DEFAULT_USER_FLAGS:I

    .line 122
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->lock:Ljava/lang/Object;

    .line 124
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getESServiceManager()Lcom/microsoft/xbox/service/network/managers/IESServiceManager;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->ES_SERVICE_MANAGER:Lcom/microsoft/xbox/service/network/managers/IESServiceManager;

    .line 125
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getChatService()Lcom/microsoft/xbox/service/chat/IChatService;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->CHAT_SERVICE:Lcom/microsoft/xbox/service/chat/IChatService;

    .line 126
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getClubChatManagementService()Lcom/microsoft/xbox/service/clubs/IClubChatManagementService;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->CLUB_CHAT_MANAGEMENT_SERVICE:Lcom/microsoft/xbox/service/clubs/IClubChatManagementService;

    return-void

    .line 70
    :array_0
    .array-data 4
        0x0
        0x2
        0x4
        0x8
    .end array-data
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/XLEObservable;-><init>()V

    .line 142
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->ctx:Landroid/content/Context;

    .line 143
    const-string v1, "gcm.properties"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    .line 144
    invoke-static {p1}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->ensureInstance(Landroid/content/Context;)Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->disp:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    .line 145
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 146
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 147
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->ctx:Landroid/content/Context;

    new-instance v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$1;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$1;-><init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 168
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->ensureSystemId()V

    .line 169
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->clearEndpointId()V

    return-void
.end method

.method static synthetic access$1000(Landroid/content/SharedPreferences;Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/SharedPreferences;
    .param p1, "x1"    # Landroid/content/SharedPreferences$Editor;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 68
    invoke-static {p0, p1, p2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->ensureSystemId(Landroid/content/SharedPreferences;Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->onLogin()V

    return-void
.end method

.method static synthetic access$1200(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getRegistration()Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 68
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->generateSystemId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400()Lcom/microsoft/xbox/service/network/managers/IESServiceManager;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->ES_SERVICE_MANAGER:Lcom/microsoft/xbox/service/network/managers/IESServiceManager;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/microsoft/xbox/service/model/gcm/GcmModel;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel;
    .param p1, "x1"    # Landroid/os/AsyncTask;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerSLSTask:Landroid/os/AsyncTask;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->skypeRegTokenWithEndpoint:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/microsoft/xbox/service/model/gcm/GcmModel;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->skypeRegTokenWithEndpoint:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1702(Lcom/microsoft/xbox/service/model/gcm/GcmModel;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel;
    .param p1, "x1"    # Landroid/os/AsyncTask;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerSkypeTask:Landroid/os/AsyncTask;

    return-object p1
.end method

.method static synthetic access$1800()[I
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->BACKOFF_SECONDS:[I

    return-object v0
.end method

.method static synthetic access$1902(Lcom/microsoft/xbox/service/model/gcm/GcmModel;Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;)Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerChatTask:Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

    return-object p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->clearEDFRegistration()V

    return-void
.end method

.method static synthetic access$2000()Lcom/microsoft/xbox/service/chat/IChatService;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->CHAT_SERVICE:Lcom/microsoft/xbox/service/chat/IChatService;

    return-object v0
.end method

.method static synthetic access$2100()Lcom/microsoft/xbox/service/clubs/IClubChatManagementService;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->CLUB_CHAT_MANAGEMENT_SERVICE:Lcom/microsoft/xbox/service/clubs/IClubChatManagementService;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/microsoft/xbox/service/model/gcm/GcmModel;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel;
    .param p1, "x1"    # Landroid/os/AsyncTask;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterSLSTask:Landroid/os/AsyncTask;

    return-object p1
.end method

.method static synthetic access$2302(Lcom/microsoft/xbox/service/model/gcm/GcmModel;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel;
    .param p1, "x1"    # Landroid/os/AsyncTask;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterSkypeTask:Landroid/os/AsyncTask;

    return-object p1
.end method

.method static synthetic access$2402(Lcom/microsoft/xbox/service/model/gcm/GcmModel;Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;)Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterChatTask:Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

    return-object p1
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/service/model/gcm/GcmModel;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerSLSInBackground(IZ)V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/service/model/gcm/GcmModel;ZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerSkypeInBackground(ZZ)V

    return-void
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private cancelAllChatTasks()V
    .locals 0

    .prologue
    .line 641
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->cancelChatRegister()V

    .line 642
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->cancelChatUnregister()V

    .line 643
    return-void
.end method

.method private cancelAllSLSTasks()V
    .locals 0

    .prologue
    .line 631
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->cancelSLSRegister()V

    .line 632
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->cancelSLSUnregister()V

    .line 633
    return-void
.end method

.method private cancelAllSkypeTasks()V
    .locals 0

    .prologue
    .line 636
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->cancelSkypeRegister()V

    .line 637
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->cancelSkypeUnregister()V

    .line 638
    return-void
.end method

.method private cancelChatRegister()V
    .locals 2

    .prologue
    .line 678
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerChatTask:Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

    if-eqz v0, :cond_0

    .line 679
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->TAG:Ljava/lang/String;

    const-string v1, "Canceling chat registration"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerChatTask:Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;->cancel(Z)Z

    .line 681
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerChatTask:Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

    .line 683
    :cond_0
    return-void
.end method

.method private cancelChatUnregister()V
    .locals 2

    .prologue
    .line 686
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterChatTask:Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

    if-eqz v0, :cond_0

    .line 687
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->TAG:Ljava/lang/String;

    const-string v1, "Canceling chat unregistration"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterChatTask:Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;->cancel(Z)Z

    .line 689
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterChatTask:Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

    .line 691
    :cond_0
    return-void
.end method

.method private cancelSLSRegister()V
    .locals 2

    .prologue
    .line 646
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerSLSTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 647
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->TAG:Ljava/lang/String;

    const-string v1, "Canceling SLS registration"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerSLSTask:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 649
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerSLSTask:Landroid/os/AsyncTask;

    .line 651
    :cond_0
    return-void
.end method

.method private cancelSLSUnregister()V
    .locals 2

    .prologue
    .line 654
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterSLSTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 655
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->TAG:Ljava/lang/String;

    const-string v1, "Canceling SLS unregistration"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterSLSTask:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 657
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterSLSTask:Landroid/os/AsyncTask;

    .line 659
    :cond_0
    return-void
.end method

.method private cancelSkypeRegister()V
    .locals 2

    .prologue
    .line 662
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerSkypeTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 663
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->TAG:Ljava/lang/String;

    const-string v1, "Canceling Skype registration"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 664
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerSkypeTask:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 665
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerSkypeTask:Landroid/os/AsyncTask;

    .line 667
    :cond_0
    return-void
.end method

.method private cancelSkypeUnregister()V
    .locals 2

    .prologue
    .line 670
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterSkypeTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 671
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->TAG:Ljava/lang/String;

    const-string v1, "Canceling Skype unregistration"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterSkypeTask:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 673
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterSkypeTask:Landroid/os/AsyncTask;

    .line 675
    :cond_0
    return-void
.end method

.method private clearEDFRegistration()V
    .locals 3

    .prologue
    .line 398
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->TAG:Ljava/lang/String;

    const-string v2, "Clearing edf registration and node id"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 400
    .local v0, "edt":Landroid/content/SharedPreferences$Editor;
    const-string v1, "edf.node.id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 401
    const-string v1, "last.edf.registration.time.03"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 402
    const-string v1, "skype.reg.token"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 403
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 404
    return-void
.end method

.method private clearEndpointId()V
    .locals 3

    .prologue
    .line 389
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->TAG:Ljava/lang/String;

    const-string v2, "Clearing endpoint id"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 391
    .local v0, "edt":Landroid/content/SharedPreferences$Editor;
    const-string v1, "endpoint.id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 392
    const-string v1, "endpoint.flags"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 393
    const-string v1, "last.sls.registration.time.04"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 394
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 395
    return-void
.end method

.method public static ensureInstance(Landroid/content/Context;)Lcom/microsoft/xbox/service/model/gcm/GcmModel;
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 176
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->instance:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    if-nez v0, :cond_1

    .line 177
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->lock:Ljava/lang/Object;

    monitor-enter v1

    .line 178
    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->instance:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    if-nez v0, :cond_0

    .line 179
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->instance:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    .line 181
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->instance:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    return-object v0

    .line 181
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private ensureSystemId()V
    .locals 5

    .prologue
    .line 713
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    const-string v3, "registration.id"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 714
    .local v1, "registrationId":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    const-string v3, "system.id"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 715
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 716
    .local v0, "edt":Landroid/content/SharedPreferences$Editor;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    invoke-static {v2, v0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->ensureSystemId(Landroid/content/SharedPreferences;Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    .line 717
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 719
    .end local v0    # "edt":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void
.end method

.method private static ensureSystemId(Landroid/content/SharedPreferences;Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V
    .locals 4
    .param p0, "prefs"    # Landroid/content/SharedPreferences;
    .param p1, "edt"    # Landroid/content/SharedPreferences$Editor;
    .param p2, "registrationId"    # Ljava/lang/String;

    .prologue
    .line 734
    const-string v1, "system.id"

    invoke-interface {p0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 735
    invoke-static {p2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->generateSystemId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 736
    .local v0, "systemId":Ljava/lang/String;
    const-string v1, "system.id"

    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 737
    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Generated new system ID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 739
    .end local v0    # "systemId":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private static generateSystemId(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "registrationId"    # Ljava/lang/String;

    .prologue
    .line 722
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Ljava/util/UUID;->nameUUIDFromBytes([B)Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getChatChannelSet(Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;)Ljava/util/Set;
    .locals 4
    .param p1, "setting"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 514
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$3;->$SwitchMap$com$microsoft$xbox$service$model$gcm$GcmModel$ChatNotificationSetting:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 522
    const-string v0, "getChatChannelSet - no such setting type"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 523
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 516
    :pswitch_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    const-string v2, "chat.am.channels"

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    goto :goto_0

    .line 518
    :pswitch_1
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    const-string v2, "chat.dm.channels"

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    goto :goto_0

    .line 520
    :pswitch_2
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    const-string v2, "chat.no.channels"

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    goto :goto_0

    .line 514
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getCurAppVersion()I
    .locals 5

    .prologue
    .line 705
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->ctx:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->ctx:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 706
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return v2

    .line 707
    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v0

    .line 708
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static getInstance()Lcom/microsoft/xbox/service/model/gcm/GcmModel;
    .locals 1

    .prologue
    .line 172
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->instance:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    return-object v0
.end method

.method private getRegistration()Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 694
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    const-string v2, "registration.id"

    .line 695
    invoke-interface {v1, v2, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    const-string v3, "endpoint.id"

    .line 696
    invoke-interface {v2, v3, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 697
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getSLSFlags()I

    move-result v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    const-string v5, "edf.reg.id"

    .line 698
    invoke-interface {v4, v5, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    const-string v6, "edf.node.id"

    .line 699
    invoke-interface {v5, v6, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    const-string v7, "skype.reg.token"

    .line 700
    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    return-object v0
.end method

.method private getSLSFlags()I
    .locals 3

    .prologue
    .line 420
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "endpoint.flags"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private isChatRegistered()Z
    .locals 1

    .prologue
    .line 197
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->AllMessages:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getChatChannelSet(Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->DirectMention:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    .line 198
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getChatChannelSet(Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 197
    :goto_0
    return v0

    .line 198
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isChatUnregisteredOrUnregistering()Z
    .locals 1

    .prologue
    .line 619
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isChatRegistered()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isUnregisteringChat()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isEDFRegistered()Z
    .locals 2

    .prologue
    .line 411
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "edf.reg.id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "edf.node.id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isRegisteringChat()Z
    .locals 1

    .prologue
    .line 623
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerChatTask:Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerChatTask:Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isRegisteringSLS()Z
    .locals 1

    .prologue
    .line 599
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerSLSTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerSLSTask:Landroid/os/AsyncTask;

    invoke-virtual {v0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isRegisteringSkype()Z
    .locals 1

    .prologue
    .line 611
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerSkypeTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerSkypeTask:Landroid/os/AsyncTask;

    invoke-virtual {v0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSLSRegistered()Z
    .locals 2

    .prologue
    .line 407
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "endpoint.id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private isSLSRegisteredTooLongAgo()Z
    .locals 10

    .prologue
    const/4 v4, 0x1

    .line 577
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    const-string v6, "last.sls.registration.time.04"

    invoke-interface {v5, v6}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 582
    :cond_0
    :goto_0
    return v4

    .line 580
    :cond_1
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    const-string v6, "last.sls.registration.time.04"

    const-wide/high16 v8, -0x8000000000000000L

    invoke-interface {v5, v6, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 581
    .local v2, "regTime":J
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 582
    .local v0, "curTime":J
    sub-long v6, v0, v2

    const-wide/32 v8, 0x337f9800

    cmp-long v5, v6, v8

    if-gtz v5, :cond_0

    const/4 v4, 0x0

    goto :goto_0
.end method

.method private isSLSUnregisteredOrUnregistering()Z
    .locals 1

    .prologue
    .line 595
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isSLSRegistered()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isUnregisteringSLS()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isServiceAvailable()Z
    .locals 1

    .prologue
    .line 338
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getServiceCode()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isServiceAvailable(I)Z

    move-result v0

    return v0
.end method

.method private isSetByUser(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)Z
    .locals 2
    .param p1, "notifType"    # Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getUserFlags()I

    move-result v0

    .line 222
    .local v0, "flags":I
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v1

    and-int/2addr v1, v0

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isSkypeRegistered()Z
    .locals 1

    .prologue
    .line 193
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isEDFRegistered()Z

    move-result v0

    return v0
.end method

.method private isSkypeRegisteredTooLongAgo()Z
    .locals 10

    .prologue
    const/4 v4, 0x1

    .line 586
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    const-string v6, "last.edf.registration.time.03"

    invoke-interface {v5, v6}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 591
    :cond_0
    :goto_0
    return v4

    .line 589
    :cond_1
    iget-object v5, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    const-string v6, "last.edf.registration.time.03"

    const-wide/high16 v8, -0x8000000000000000L

    invoke-interface {v5, v6, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 590
    .local v2, "regTime":J
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 591
    .local v0, "curTime":J
    sub-long v6, v0, v2

    const-wide v8, 0x9a7ec800L

    cmp-long v5, v6, v8

    if-gtz v5, :cond_0

    const/4 v4, 0x0

    goto :goto_0
.end method

.method private isSkypeUnregisteredOrUnregistering()Z
    .locals 1

    .prologue
    .line 607
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isSkypeRegistered()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isUnregisteringSkype()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isUnregisteringChat()Z
    .locals 1

    .prologue
    .line 627
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterChatTask:Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterChatTask:Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isUnregisteringSLS()Z
    .locals 1

    .prologue
    .line 603
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterSLSTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterSLSTask:Landroid/os/AsyncTask;

    invoke-virtual {v0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isUnregisteringSkype()Z
    .locals 1

    .prologue
    .line 615
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterSkypeTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterSkypeTask:Landroid/os/AsyncTask;

    invoke-virtual {v0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$onLogin$0(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    .prologue
    const/4 v0, 0x1

    .line 245
    invoke-direct {p0, v0, v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerSkypeInBackground(ZZ)V

    return-void
.end method

.method static synthetic lambda$onLogin$1(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)V
    .locals 7
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    .prologue
    .line 249
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getRegistration()Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    move-result-object v1

    .line 250
    .local v1, "registration":Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;
    iget-object v2, v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->registrationId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 251
    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->TAG:Ljava/lang/String;

    const-string v3, "Cleanup push notification registration started"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    :try_start_0
    iget-object v2, v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->edfRegId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 254
    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->TAG:Ljava/lang/String;

    const-string v3, "Cleanup registration endpoints except the current one %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->edfRegId:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    :cond_0
    iget-object v2, v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->registrationId:Ljava/lang/String;

    iget-object v3, v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->edfRegId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->cleanupUnusedSkypeEndpointsForPushNotification(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->TAG:Ljava/lang/String;

    const-string v3, "Cleanup push notification registration ended"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 266
    :goto_0
    return-void

    .line 259
    :catch_0
    move-exception v0

    .line 260
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 263
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->TAG:Ljava/lang/String;

    const-string v3, "Not registered with gcm so nothing to clean"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private onLogin()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 227
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isServiceAvailable()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 228
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getUserFlags()I

    move-result v5

    .line 229
    .local v5, "userFlags":I
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getSLSFlags()I

    move-result v2

    .line 230
    .local v2, "endpointFlags":I
    sget-object v6, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SLS user flags: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", endpoint flags: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    if-eqz v5, :cond_3

    .line 232
    if-ne v5, v2, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isSLSRegisteredTooLongAgo()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isSkypeRegisteredTooLongAgo()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 233
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isSLSRegisteredTooLongAgo()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 234
    sget-object v6, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->TAG:Ljava/lang/String;

    const-string v7, "SLS registration expired, clearing endpoint id"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->clearEndpointId()V

    .line 238
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isSkypeRegisteredTooLongAgo()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 239
    sget-object v6, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->TAG:Ljava/lang/String;

    const-string v7, "Skype registration expired, clearing endpoint id"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->clearEDFRegistration()V

    .line 243
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getUserFlags()I

    move-result v6

    invoke-direct {p0, v6, v10}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerSLSInBackground(IZ)V

    .line 245
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Ljava/lang/Runnable;

    move-result-object v6

    const-wide/16 v8, 0x1388

    invoke-static {v6, v8, v9}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPostDelayed(Ljava/lang/Runnable;J)V

    .line 273
    :cond_3
    :goto_0
    sget-object v6, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->TAG:Ljava/lang/String;

    const-string v7, "onLogin - start registering chat on login"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    sget-object v6, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->AllMessages:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    invoke-direct {p0, v6}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getChatChannelSet(Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;)Ljava/util/Set;

    move-result-object v0

    .line 275
    .local v0, "allMessagesChannels":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    sget-object v6, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->DirectMention:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    invoke-direct {p0, v6}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getChatChannelSet(Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;)Ljava/util/Set;

    move-result-object v1

    .line 276
    .local v1, "directMentionChannels":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    sget-object v6, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->None:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    invoke-direct {p0, v6}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getChatChannelSet(Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;)Ljava/util/Set;

    move-result-object v3

    .line 278
    .local v3, "noNotificationChannels":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {p0, v10, v0, v1, v3}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerChatInBackground(ZLjava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    .line 282
    .end local v0    # "allMessagesChannels":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v1    # "directMentionChannels":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v2    # "endpointFlags":I
    .end local v3    # "noNotificationChannels":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v5    # "userFlags":I
    :goto_1
    return-void

    .line 247
    .restart local v2    # "endpointFlags":I
    .restart local v5    # "userFlags":I
    :cond_4
    new-instance v4, Lcom/microsoft/xbox/service/model/gcm/GcmModel$2;

    sget-object v6, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Ljava/lang/Runnable;

    move-result-object v7

    invoke-direct {v4, p0, v6, v7}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$2;-><init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel;Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 268
    .local v4, "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;->execute()V

    goto :goto_0

    .line 280
    .end local v2    # "endpointFlags":I
    .end local v4    # "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    .end local v5    # "userFlags":I
    :cond_5
    sget-object v6, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->TAG:Ljava/lang/String;

    const-string v7, "Google Play Service is unavailable. If the service is valid the user will be able to register for push notifications from settings"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private registerChatInBackground(ZLjava/util/Set;Ljava/util/Set;Ljava/util/Set;)V
    .locals 6
    .param p1, "shouldLoadChannelData"    # Z
    .param p2    # Ljava/util/Set;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/Set;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Ljava/util/Set;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 557
    .local p2, "allMessagesChannels":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p3, "directMentionChannels":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p4, "noNotificationChannels":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 558
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 559
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 561
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->cancelAllChatTasks()V

    .line 562
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;-><init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel;ZLjava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerChatTask:Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

    .line 567
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerChatTask:Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 568
    return-void
.end method

.method private registerSLSInBackground(IZ)V
    .locals 2
    .param p1, "flags"    # I
    .param p2, "isLogin"    # Z

    .prologue
    .line 528
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->cancelAllSLSTasks()V

    .line 529
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSLSTask;-><init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel;IZ)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerSLSTask:Landroid/os/AsyncTask;

    .line 530
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerSLSTask:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 531
    return-void
.end method

.method private registerSkypeInBackground(ZZ)V
    .locals 2
    .param p1, "forceSubscriptionRefresh"    # Z
    .param p2, "isLogin"    # Z

    .prologue
    .line 540
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isNotificationsMessageEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 541
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->cancelAllSkypeTasks()V

    .line 542
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterSkypeTask;-><init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel;ZZ)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerSkypeTask:Landroid/os/AsyncTask;

    .line 543
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerSkypeTask:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 545
    :cond_0
    return-void
.end method

.method public static reset()V
    .locals 1

    .prologue
    .line 187
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->instance:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    if-eqz v0, :cond_0

    .line 188
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->instance:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->resetModel()V

    .line 190
    :cond_0
    return-void
.end method

.method private resetModel()V
    .locals 5

    .prologue
    .line 342
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isRegisteringSLS()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 343
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->cancelSLSRegister()V

    .line 346
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isRegisteringSkype()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 347
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->cancelSkypeRegister()V

    .line 350
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isRegisteringChat()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 351
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->cancelChatRegister()V

    .line 354
    :cond_2
    const/4 v1, 0x0

    .line 356
    .local v1, "unregisteredSLSOrSkype":Z
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isSLSUnregisteredOrUnregistering()Z

    move-result v2

    if-nez v2, :cond_3

    .line 357
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterSLSInBackground(Z)V

    .line 358
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->clearEndpointId()V

    .line 359
    const/4 v1, 0x1

    .line 362
    :cond_3
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isSkypeUnregisteredOrUnregistering()Z

    move-result v2

    if-nez v2, :cond_4

    .line 363
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterSkypeInBackground()V

    .line 364
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->clearEDFRegistration()V

    .line 365
    const/4 v1, 0x1

    .line 368
    :cond_4
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isChatUnregisteredOrUnregistering()Z

    move-result v2

    if-nez v2, :cond_5

    .line 369
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterChatInBackground()V

    .line 372
    :cond_5
    if-nez v1, :cond_6

    .line 373
    new-instance v2, Lcom/microsoft/xbox/toolkit/AsyncResult;

    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;->STATE_CHANGED:Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    const/4 v4, 0x0

    invoke-direct {v2, v3, p0, v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 377
    :cond_6
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getRegistration()Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->isGCMRegistered()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 378
    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->TAG:Ljava/lang/String;

    const-string v3, "resetModel - clear registration Id"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 380
    .local v0, "edt":Landroid/content/SharedPreferences$Editor;
    const-string v2, "registration.id"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 381
    const-string v2, "app.version"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 382
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 385
    .end local v0    # "edt":Landroid/content/SharedPreferences$Editor;
    :cond_7
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->disp:Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/gcm/NotificationDisplay;->removeAllNotifications()V

    .line 386
    return-void
.end method

.method private unregisterChatInBackground()V
    .locals 2

    .prologue
    .line 571
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->cancelAllChatTasks()V

    .line 572
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterChatTask;-><init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel;Lcom/microsoft/xbox/service/model/gcm/GcmModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterChatTask:Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

    .line 573
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterChatTask:Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 574
    return-void
.end method

.method private unregisterSLSInBackground(Z)V
    .locals 2
    .param p1, "isLogin"    # Z

    .prologue
    .line 534
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->cancelAllSLSTasks()V

    .line 535
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSLSTask;-><init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel;Z)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterSLSTask:Landroid/os/AsyncTask;

    .line 536
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterSLSTask:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 537
    return-void
.end method

.method private unregisterSkypeInBackground()V
    .locals 2

    .prologue
    .line 548
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->cancelAllSkypeTasks()V

    .line 549
    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;-><init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel;Lcom/microsoft/xbox/service/model/gcm/GcmModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterSkypeTask:Landroid/os/AsyncTask;

    .line 550
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterSkypeTask:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 551
    return-void
.end method


# virtual methods
.method public canSLSNotify()Z
    .locals 1

    .prologue
    .line 314
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isSLSRegistered()Z

    move-result v0

    return v0
.end method

.method public ensureRegIdOnLogin()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 285
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    const-string v4, "app.version"

    const/high16 v5, -0x80000000

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 286
    .local v2, "regAppVersion":I
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getCurAppVersion()I

    move-result v0

    .line 287
    .local v0, "curAppVersion":I
    if-eq v2, v0, :cond_0

    .line 289
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 290
    .local v1, "edt":Landroid/content/SharedPreferences$Editor;
    const-string v3, "app.version"

    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 291
    const-string v3, "endpoint.id"

    invoke-interface {v1, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 292
    const-string v3, "endpoint.flags"

    invoke-interface {v1, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 293
    const-string v3, "edf.node.id"

    invoke-interface {v1, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 294
    const-string v3, "skype.reg.token"

    invoke-interface {v1, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 295
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 298
    .end local v1    # "edt":Landroid/content/SharedPreferences$Editor;
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getRegistration()Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->isGCMRegistered()Z

    move-result v3

    if-nez v3, :cond_2

    .line 299
    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->TAG:Ljava/lang/String;

    const-string v4, "ensureRegIdOnLogin - registration Id not stored"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->ensureRegistrationIdTask:Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

    if-eqz v3, :cond_1

    .line 301
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->ensureRegistrationIdTask:Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;->cancel(Z)Z

    .line 304
    :cond_1
    new-instance v3, Lcom/microsoft/xbox/service/model/gcm/GcmModel$EnsureRegistrationIdTask;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$EnsureRegistrationIdTask;-><init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel;Lcom/microsoft/xbox/service/model/gcm/GcmModel$1;)V

    iput-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->ensureRegistrationIdTask:Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

    .line 305
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->ensureRegistrationIdTask:Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

    new-array v4, v6, [Ljava/lang/Void;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 311
    :goto_0
    return-void

    .line 308
    :cond_2
    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->TAG:Ljava/lang/String;

    const-string v4, "ensureRegIdOnLogin - registration Id stored, call onLogin directly"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->onLogin()V

    goto :goto_0
.end method

.method public getChatNotificationSetting(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;
    .locals 3
    .param p1, "channel"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 486
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 488
    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->AllMessages:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getChatChannelSet(Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;)Ljava/util/Set;

    move-result-object v0

    .line 489
    .local v0, "allMessagesChannels":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->DirectMention:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getChatChannelSet(Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;)Ljava/util/Set;

    move-result-object v1

    .line 491
    .local v1, "directMentionChannels":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 492
    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->AllMessages:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    .line 498
    :goto_0
    return-object v2

    .line 494
    :cond_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 495
    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->DirectMention:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    goto :goto_0

    .line 498
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->None:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    goto :goto_0
.end method

.method public getEDFRegId()Ljava/lang/String;
    .locals 3
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 416
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "edf.reg.id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPushNotificationRegToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->skypeRegTokenWithEndpoint:Ljava/lang/String;

    return-object v0
.end method

.method public getRegistrationTokenWithEndpoint()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 504
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getRegistration()Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    move-result-object v0

    .line 505
    .local v0, "curRegistration":Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;
    if-eqz v0, :cond_0

    .line 506
    iget-object v1, v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->skypeRegToken:Ljava/lang/String;

    .line 509
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getServiceCode()I
    .locals 2

    .prologue
    .line 318
    invoke-static {}, Lcom/google/android/gms/common/GoogleApiAvailability;->getInstance()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->ctx:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/GoogleApiAvailability;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public getUserFlags()I
    .locals 3

    .prologue
    .line 482
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "user.flags"

    sget v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->DEFAULT_USER_FLAGS:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 211
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isRegisteringSLS()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isUnregisteringSLS()Z

    move-result v0

    if-nez v0, :cond_0

    .line 212
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isRegisteringSkype()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isUnregisteringSkype()Z

    move-result v0

    if-nez v0, :cond_0

    .line 213
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isRegisteringChat()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isUnregisteringChat()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 211
    :goto_0
    return v0

    .line 213
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNotificationsMessageEnabled()Z
    .locals 1

    .prologue
    .line 217
    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->MESSAGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->isSetByUser(Lcom/microsoft/xbox/service/model/gcm/NotificationType;)Z

    move-result v0

    return v0
.end method

.method public isServiceAvailable(I)Z
    .locals 1
    .param p1, "serviceCode"    # I

    .prologue
    .line 322
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isServiceErrorRecoverable(I)Z
    .locals 1
    .param p1, "serviceCode"    # I

    .prologue
    .line 326
    invoke-static {}, Lcom/google/android/gms/common/GoogleApiAvailability;->getInstance()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/GoogleApiAvailability;->isUserResolvableError(I)Z

    move-result v0

    return v0
.end method

.method public isServiceInvalid(I)Z
    .locals 1
    .param p1, "serviceCode"    # I

    .prologue
    .line 330
    const/16 v0, 0x9

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public launchServiceErrorRecovery(II)V
    .locals 2
    .param p1, "serviceCode"    # I
    .param p2, "requestCode"    # I

    .prologue
    .line 334
    invoke-static {}, Lcom/google/android/gms/common/GoogleApiAvailability;->getInstance()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/gms/common/GoogleApiAvailability;->getErrorDialog(Landroid/app/Activity;II)Landroid/app/Dialog;

    .line 335
    return-void
.end method

.method public registerPushNotificationWithWithSkype()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 468
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 469
    .local v0, "edt":Landroid/content/SharedPreferences$Editor;
    const-string v1, "user.flags"

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getUserFlags()I

    move-result v2

    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->MESSAGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v3

    or-int/2addr v2, v3

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 470
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 471
    invoke-direct {p0, v4, v4}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerSkypeInBackground(ZZ)V

    .line 472
    return-void
.end method

.method public setChatNotificationSetting(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;)V
    .locals 5
    .param p1, "channel"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    .param p2, "setting"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    .prologue
    .line 439
    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->AllMessages:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getChatChannelSet(Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;)Ljava/util/Set;

    move-result-object v0

    .line 440
    .local v0, "allMessagesChannels":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->DirectMention:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getChatChannelSet(Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;)Ljava/util/Set;

    move-result-object v1

    .line 441
    .local v1, "directMentionChannels":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->None:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getChatChannelSet(Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;)Ljava/util/Set;

    move-result-object v2

    .line 443
    .local v2, "noNotificationChannels":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 444
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 445
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 447
    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/GcmModel$3;->$SwitchMap$com$microsoft$xbox$service$model$gcm$GcmModel$ChatNotificationSetting:[I

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 461
    const-string v3, "setChatNotificationSetting - unrecognized chat notification setting"

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 464
    :goto_0
    const/4 v3, 0x0

    invoke-direct {p0, v3, v0, v1, v2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerChatInBackground(ZLjava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    .line 465
    return-void

    .line 449
    :pswitch_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 453
    :pswitch_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 457
    :pswitch_2
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 447
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setPushNotificationRegToken(Ljava/lang/String;)V
    .locals 1
    .param p1, "skypeRegTokenWithEndpoint"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 206
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->skypeRegTokenWithEndpoint:Ljava/lang/String;

    .line 207
    sget-object v0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->setRegistrationToken(Ljava/lang/String;)V

    .line 208
    return-void
.end method

.method public setUserFlags(I)V
    .locals 4
    .param p1, "userFlags"    # I

    .prologue
    const/4 v3, 0x0

    .line 424
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 425
    .local v0, "edt":Landroid/content/SharedPreferences$Editor;
    const-string v2, "user.flags"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 426
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 428
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getSLSFlags()I

    move-result v1

    .line 429
    .local v1, "endpointFlags":I
    if-eq v1, p1, :cond_0

    .line 430
    if-eqz p1, :cond_1

    .line 431
    invoke-direct {p0, p1, v3}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->registerSLSInBackground(IZ)V

    .line 436
    :cond_0
    :goto_0
    return-void

    .line 433
    :cond_1
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterSLSInBackground(Z)V

    goto :goto_0
.end method

.method public unregisterPushNotificationWithWithSkype()V
    .locals 4

    .prologue
    .line 475
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 476
    .local v0, "edt":Landroid/content/SharedPreferences$Editor;
    const-string v1, "user.flags"

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getUserFlags()I

    move-result v2

    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->MESSAGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v3

    xor-int/lit8 v3, v3, -0x1

    and-int/2addr v2, v3

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 477
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 478
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->unregisterSkypeInBackground()V

    .line 479
    return-void
.end method
