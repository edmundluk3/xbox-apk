.class final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember;
.super Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;
.source "AutoValue_ClubRosterDataTypes_ClubMember.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/AutoValue_ClubRosterDataTypes_ClubMember$GsonTypeAdapter;
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 0
    .param p1, "userId"    # Ljava/lang/String;
    .param p3, "followQuotaMax"    # Ljava/lang/Integer;
    .param p4, "followQuotaRemaining"    # Ljava/lang/Integer;
    .param p5, "memberQuotaMax"    # Ljava/lang/Integer;
    .param p6, "memberQuotaRemaining"    # Ljava/lang/Integer;
    .param p7, "moderatorQuotaMax"    # Ljava/lang/Integer;
    .param p8, "moderatorQuotaRemaining"    # Ljava/lang/Integer;
    .param p9, "error"    # Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;
    .param p10, "errorCode"    # Ljava/lang/Integer;
    .param p11, "errorDescription"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 21
    .local p2, "roles":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    invoke-direct/range {p0 .. p11}, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubRosterDataTypes_ClubMember;-><init>(Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 22
    return-void
.end method
