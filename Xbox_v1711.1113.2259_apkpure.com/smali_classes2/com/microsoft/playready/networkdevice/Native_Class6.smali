.class Lcom/microsoft/playready/networkdevice/Native_Class6;
.super Ljava/lang/Object;
.source "Native_Class6.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;,
        Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_4;,
        Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;,
        Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_3;,
        Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_2;
    }
.end annotation


# instance fields
.field protected mField1:J

.field mHandler2:Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_2;

.field mHandler3:Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_3;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 15
    invoke-static {}, Lcom/microsoft/playready/networkdevice/NativeLoadLibrary;->loadLibrary()V

    .line 18
    invoke-static {}, Lcom/microsoft/playready/networkdevice/Native_Class6;->_method_1()V

    .line 19
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class6;->mHandler2:Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_2;

    .line 151
    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class6;->mHandler3:Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_3;

    .line 168
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class6;->mField1:J

    .line 25
    invoke-virtual {p0}, Lcom/microsoft/playready/networkdevice/Native_Class6;->_method_2()V

    .line 26
    return-void
.end method

.method protected static final native _method_1()V
.end method


# virtual methods
.method protected native _method_2()V
.end method

.method protected native _method_3()V
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/microsoft/playready/networkdevice/Native_Class6;->_method_3()V

    .line 34
    return-void
.end method

.method public native method_10(Ljava/lang/String;I[B[B[B)[B
.end method

.method public native method_11(I[B[B[B)Z
.end method

.method public native method_12()Z
.end method

.method public native method_13()V
.end method

.method public native method_14(Ljava/lang/String;)Z
.end method

.method public native method_4(Ljava/lang/Object;)V
.end method

.method r_method_2(Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;)Z
    .locals 2
    .param p1, "arg1"    # Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;

    .prologue
    .line 215
    const/4 v0, 0x1

    .line 217
    .local v0, "retVal":Z
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/Native_Class6;->mHandler2:Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_2;

    if-eqz v1, :cond_0

    .line 219
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/Native_Class6;->mHandler2:Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_2;

    invoke-interface {v1, p1}, Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_2;->call(Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_5;)Z

    move-result v0

    .line 222
    :cond_0
    return v0
.end method

.method r_method_3(I[B)Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;
    .locals 2
    .param p1, "arg1"    # I
    .param p2, "arg2"    # [B

    .prologue
    .line 228
    const/4 v0, 0x0

    .line 230
    .local v0, "retVal":Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/Native_Class6;->mHandler3:Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_3;

    if-eqz v1, :cond_0

    .line 232
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/Native_Class6;->mHandler3:Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_3;

    invoke-interface {v1, p1, p2}, Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_3;->call_1(I[B)Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;

    move-result-object v0

    .line 235
    :cond_0
    return-object v0
.end method

.method r_method_4(Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;)Z
    .locals 2
    .param p1, "arg1"    # Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;

    .prologue
    .line 242
    const/4 v0, 0x1

    .line 244
    .local v0, "retVal":Z
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/Native_Class6;->mHandler3:Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_3;

    if-eqz v1, :cond_0

    .line 246
    iget-object v1, p0, Lcom/microsoft/playready/networkdevice/Native_Class6;->mHandler3:Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_3;

    invoke-interface {v1, p1}, Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_3;->call_2(Lcom/microsoft/playready/networkdevice/Native_Class6$internal_class_3;)Z

    move-result v0

    .line 249
    :cond_0
    return v0
.end method

.method public setHandler2(Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_2;)V
    .locals 0
    .param p1, "handler"    # Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_2;

    .prologue
    .line 155
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/Native_Class6;->mHandler2:Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_2;

    .line 156
    return-void
.end method

.method public setHandler3(Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_3;)V
    .locals 0
    .param p1, "handler"    # Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_3;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/Native_Class6;->mHandler3:Lcom/microsoft/playready/networkdevice/Native_Class6$IHandler_3;

    .line 161
    return-void
.end method
