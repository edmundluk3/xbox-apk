.class public Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;
.super Ljava/lang/Object;
.source "SGPlatformInstance.java"


# static fields
.field private static final APPKEY:Ljava/lang/String; = "4F670DE6-2922-BA51-94A1-3010D8423060"

.field private static initialized:Z

.field private static instance:Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;

.field private static platformReady:Lcom/microsoft/xbox/toolkit/Ready;


# instance fields
.field tm:Lcom/microsoft/xbox/toolkit/TimeMonitor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->instance:Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;

    .line 23
    new-instance v0, Lcom/microsoft/xbox/toolkit/Ready;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/Ready;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->platformReady:Lcom/microsoft/xbox/toolkit/Ready;

    .line 25
    const/4 v0, 0x0

    sput-boolean v0, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->initialized:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v1, Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/TimeMonitor;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->tm:Lcom/microsoft/xbox/toolkit/TimeMonitor;

    .line 33
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->tm:Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->start()V

    .line 34
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->platformReady:Lcom/microsoft/xbox/toolkit/Ready;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/Ready;->reset()V

    .line 35
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/xblshared/ProtectedRunnable;

    new-instance v1, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance$1;-><init>(Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;)V

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ProtectedRunnable;-><init>(Ljava/lang/Runnable;)V

    .line 46
    .local v0, "runnable":Ljava/lang/Runnable;
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 48
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->platformReady:Lcom/microsoft/xbox/toolkit/Ready;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/Ready;->setReady()V

    .line 49
    const-string v1, "SGPlatfromInstance"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "time to create platfrom instance "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->tm:Lcom/microsoft/xbox/toolkit/TimeMonitor;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/TimeMonitor;->currentTime()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 21
    sput-boolean p0, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->initialized:Z

    return p0
.end method

.method public static declared-synchronized getInstance()Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;
    .locals 3

    .prologue
    .line 57
    const-class v1, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->instance:Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;

    if-nez v0, :cond_0

    .line 58
    const-string v0, "SGPlatformInstance"

    const-string v2, "create sgplatform instance"

    invoke-static {v0, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->instance:Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;

    .line 62
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->instance:Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getPlatformReady()Lcom/microsoft/xbox/toolkit/Ready;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->platformReady:Lcom/microsoft/xbox/toolkit/Ready;

    return-object v0
.end method

.method private loadConfiguration()Ljava/lang/String;
    .locals 12

    .prologue
    .line 110
    const-string v3, ""

    .line 112
    .local v3, "config":Ljava/lang/String;
    :try_start_0
    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v10}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f060003

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 113
    .local v8, "stream":Ljava/io/InputStream;
    const/4 v6, 0x0

    .line 114
    .local v6, "reader":Ljava/io/Reader;
    if-eqz v8, :cond_2

    .line 116
    :try_start_1
    new-instance v7, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/InputStreamReader;

    const-string v11, "UTF-8"

    invoke-direct {v10, v8, v11}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v7, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 118
    .end local v6    # "reader":Ljava/io/Reader;
    .local v7, "reader":Ljava/io/Reader;
    const/4 v0, 0x0

    .line 119
    .local v0, "bomParsed":Z
    const/16 v10, 0x400

    :try_start_2
    new-array v1, v10, [C

    .line 120
    .local v1, "buffer":[C
    new-instance v9, Ljava/io/StringWriter;

    invoke-direct {v9}, Ljava/io/StringWriter;-><init>()V

    .line 121
    .local v9, "writer":Ljava/io/StringWriter;
    :goto_0
    invoke-virtual {v7, v1}, Ljava/io/Reader;->read([C)I

    move-result v2

    .local v2, "bytesRead":I
    if-lez v2, :cond_3

    .line 122
    const/4 v5, 0x0

    .line 125
    .local v5, "offset":I
    if-nez v0, :cond_0

    const/4 v10, 0x0

    aget-char v10, v1, v10

    const v11, 0xfeff

    if-ne v10, v11, :cond_0

    .line 126
    const/4 v5, 0x1

    .line 127
    const/4 v0, 0x1

    .line 130
    :cond_0
    invoke-virtual {v9, v1, v5, v2}, Ljava/io/StringWriter;->write([CII)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 135
    .end local v1    # "buffer":[C
    .end local v2    # "bytesRead":I
    .end local v5    # "offset":I
    .end local v9    # "writer":Ljava/io/StringWriter;
    :catchall_0
    move-exception v10

    move-object v6, v7

    .end local v0    # "bomParsed":Z
    .end local v7    # "reader":Ljava/io/Reader;
    .restart local v6    # "reader":Ljava/io/Reader;
    :goto_1
    if-eqz v6, :cond_1

    .line 136
    :try_start_3
    invoke-virtual {v6}, Ljava/io/Reader;->close()V

    .line 138
    :cond_1
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V

    throw v10
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 141
    .end local v6    # "reader":Ljava/io/Reader;
    .end local v8    # "stream":Ljava/io/InputStream;
    :catch_0
    move-exception v4

    .line 143
    .local v4, "ex":Ljava/lang/Exception;
    const-string v10, "SGPlatform"

    const-string v11, "failed to load SG signed config file."

    invoke-static {v10, v11}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    .end local v4    # "ex":Ljava/lang/Exception;
    :cond_2
    :goto_2
    return-object v3

    .line 133
    .restart local v0    # "bomParsed":Z
    .restart local v1    # "buffer":[C
    .restart local v2    # "bytesRead":I
    .restart local v7    # "reader":Ljava/io/Reader;
    .restart local v8    # "stream":Ljava/io/InputStream;
    .restart local v9    # "writer":Ljava/io/StringWriter;
    :cond_3
    :try_start_4
    invoke-virtual {v9}, Ljava/io/StringWriter;->toString()Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v3

    .line 135
    if-eqz v7, :cond_4

    .line 136
    :try_start_5
    invoke-virtual {v7}, Ljava/io/Reader;->close()V

    .line 138
    :cond_4
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    move-object v6, v7

    .line 139
    .end local v7    # "reader":Ljava/io/Reader;
    .restart local v6    # "reader":Ljava/io/Reader;
    goto :goto_2

    .line 135
    .end local v0    # "bomParsed":Z
    .end local v1    # "buffer":[C
    .end local v2    # "bytesRead":I
    .end local v9    # "writer":Ljava/io/StringWriter;
    :catchall_1
    move-exception v10

    goto :goto_1
.end method

.method public static declared-synchronized reset()V
    .locals 2

    .prologue
    .line 69
    const-class v1, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->instance:Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;

    if-eqz v0, :cond_0

    .line 70
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->uninitialize()V

    .line 71
    const/4 v0, 0x0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->instance:Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;

    .line 72
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->platformReady:Lcom/microsoft/xbox/toolkit/Ready;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/Ready;->reset()V

    .line 73
    const/4 v0, 0x0

    sput-boolean v0, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->initialized:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75
    :cond_0
    monitor-exit v1

    return-void

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public getClientInformation()Lcom/microsoft/xbox/smartglass/ClientInformation;
    .locals 2

    .prologue
    .line 78
    new-instance v0, Lcom/microsoft/xbox/smartglass/ClientInformation;

    invoke-direct {v0}, Lcom/microsoft/xbox/smartglass/ClientInformation;-><init>()V

    .line 79
    .local v0, "clientInfo":Lcom/microsoft/xbox/smartglass/ClientInformation;
    const-string v1, "4F670DE6-2922-BA51-94A1-3010D8423060"

    iput-object v1, v0, Lcom/microsoft/xbox/smartglass/ClientInformation;->appKey:Ljava/lang/String;

    .line 80
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->loadConfiguration()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/smartglass/ClientInformation;->configuration:Ljava/lang/String;

    .line 81
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getVersionCode()I

    move-result v1

    iput v1, v0, Lcom/microsoft/xbox/smartglass/ClientInformation;->version:I

    .line 82
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v1, v0, Lcom/microsoft/xbox/smartglass/ClientInformation;->osMajorVersion:I

    .line 83
    const/4 v1, 0x0

    iput v1, v0, Lcom/microsoft/xbox/smartglass/ClientInformation;->osMinorVersion:I

    .line 84
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->PackageName:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/smartglass/ClientInformation;->displayName:Ljava/lang/String;

    .line 85
    sget-object v1, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->SupportsAll:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    invoke-static {v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/smartglass/ClientInformation;->capabilities:Ljava/util/EnumSet;

    .line 86
    return-object v0
.end method

.method public setEnvironement(Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;)V
    .locals 2
    .param p1, "env"    # Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    .prologue
    .line 90
    sget-boolean v0, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance;->initialized:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 91
    const-string v0, "SGPaltformInstance"

    const-string v1, "set environment"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/SGPlatformInstance$2;->$SwitchMap$com$microsoft$xbox$toolkit$network$XboxLiveEnvironment$Environment:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Environemnt is not supported"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 107
    :goto_0
    return-void

    .line 95
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getEnvironmentManager()Lcom/microsoft/xbox/smartglass/EnvironmentManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/smartglass/Environment;->Production:Lcom/microsoft/xbox/smartglass/Environment;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/smartglass/EnvironmentManager;->setEnvironment(Lcom/microsoft/xbox/smartglass/Environment;)V

    goto :goto_0

    .line 98
    :pswitch_1
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getEnvironmentManager()Lcom/microsoft/xbox/smartglass/EnvironmentManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/smartglass/Environment;->DNet:Lcom/microsoft/xbox/smartglass/Environment;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/smartglass/EnvironmentManager;->setEnvironment(Lcom/microsoft/xbox/smartglass/Environment;)V

    goto :goto_0

    .line 101
    :pswitch_2
    const-string v0, "SGPlatformInstance"

    const-string v1, "Using STUB environment and setting SGPlatform environment to Production."

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getEnvironmentManager()Lcom/microsoft/xbox/smartglass/EnvironmentManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/smartglass/Environment;->Production:Lcom/microsoft/xbox/smartglass/Environment;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/smartglass/EnvironmentManager;->setEnvironment(Lcom/microsoft/xbox/smartglass/Environment;)V

    goto :goto_0

    .line 93
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
