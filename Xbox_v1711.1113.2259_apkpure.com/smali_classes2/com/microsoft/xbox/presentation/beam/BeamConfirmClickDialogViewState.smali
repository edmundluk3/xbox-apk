.class public Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;
.super Lcom/microsoft/xbox/presentation/base/OkCancelDialogViewState;
.source "BeamConfirmClickDialogViewState.java"


# instance fields
.field public final deeplinkToAppEnabled:Z


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;Z)V
    .locals 0
    .param p1, "confirmedIntent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "cancelledIntent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "deeplinkToAppEnabled"    # Z

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/base/OkCancelDialogViewState;-><init>(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)V

    .line 21
    iput-boolean p3, p0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->deeplinkToAppEnabled:Z

    .line 22
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 35
    if-ne p0, p1, :cond_1

    .line 41
    :cond_0
    :goto_0
    return v1

    .line 37
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

    if-nez v3, :cond_2

    move v1, v2

    .line 38
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 40
    check-cast v0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

    .line 41
    .local v0, "other":Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;
    iget-boolean v3, p0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->deeplinkToAppEnabled:Z

    iget-boolean v4, v0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->deeplinkToAppEnabled:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->confirmedIntent:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    iget-object v4, v0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->confirmedIntent:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    .line 42
    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->cancelledIntent:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    iget-object v4, v0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->cancelledIntent:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    .line 43
    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 49
    iget v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->hashCode:I

    if-nez v0, :cond_0

    .line 50
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->hashCode:I

    .line 51
    iget v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->deeplinkToAppEnabled:Z

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->hashCode:I

    .line 52
    iget v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->confirmedIntent:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->hashCode:I

    .line 53
    iget v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->cancelledIntent:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->hashCode:I

    .line 56
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->hashCode:I

    return v0
.end method

.method public toLogString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 26
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "{deeplinkToAppEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->deeplinkToAppEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", confirmedIntent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->confirmedIntent:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    .line 28
    invoke-interface {v1}, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;->toLogString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cancelledIntent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/beam/BeamConfirmClickDialogViewState;->cancelledIntent:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    .line 29
    invoke-interface {v1}, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;->toLogString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 26
    return-object v0
.end method
