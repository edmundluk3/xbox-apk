.class public final Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "ClubBannedListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;,
        Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$BannedItemClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;",
        "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final clickListener:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$BannedItemClickListener;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$BannedItemClickListener;)V
    .locals 0
    .param p1, "clickListener"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$BannedItemClickListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 31
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 32
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;->clickListener:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$BannedItemClickListener;

    .line 33
    return-void
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 48
    const v0, 0x7f030055

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;->onBindViewHolder(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;I)V
    .locals 2
    .param p1, "holder"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 43
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;->clickListener:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$BannedItemClickListener;

    invoke-virtual {p1, v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;->bindTo(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$BannedItemClickListener;)V

    .line 44
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 37
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 38
    .local v0, "v":Landroid/view/View;
    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;Landroid/view/View;)V

    return-object v1
.end method
