.class public final Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;
.super Ljava/lang/Object;
.source "ClubProfileDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClubProfileUpdateRequest"
.end annotation


# instance fields
.field private final modifiedFields:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final requestContract:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;Ljava/util/List;)V
    .locals 1
    .param p1, "requestContract"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 60
    .local p2, "modifiedFields":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 62
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 64
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;->requestContract:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;

    .line 65
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;->modifiedFields:Ljava/util/List;

    .line 66
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;->modifiedFields:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public getModifiedFields()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;->modifiedFields:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
