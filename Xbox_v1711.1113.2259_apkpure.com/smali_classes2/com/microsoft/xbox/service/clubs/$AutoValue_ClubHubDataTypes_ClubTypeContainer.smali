.class abstract Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;
.super Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;
.source "$AutoValue_ClubHubDataTypes_ClubTypeContainer.java"


# instance fields
.field private final genre:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

.field private final localizedTitleFamilyName:Ljava/lang/String;

.field private final titleFamilyId:Ljava/lang/String;

.field private final type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "genre"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "localizedTitleFamilyName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "titleFamilyId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 21
    iput-object p2, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->genre:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    .line 22
    iput-object p3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->localizedTitleFamilyName:Ljava/lang/String;

    .line 23
    iput-object p4, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->titleFamilyId:Ljava/lang/String;

    .line 24
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 62
    if-ne p1, p0, :cond_1

    .line 72
    :cond_0
    :goto_0
    return v1

    .line 65
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    if-eqz v3, :cond_7

    move-object v0, p1

    .line 66
    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    .line 67
    .local v0, "that":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->genre:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    if-nez v3, :cond_4

    .line 68
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->genre()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->localizedTitleFamilyName:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 69
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->localizedTitleFamilyName()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->titleFamilyId:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 70
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->titleFamilyId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 67
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 68
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->genre:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->genre()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 69
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->localizedTitleFamilyName:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->localizedTitleFamilyName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_3

    .line 70
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->titleFamilyId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->titleFamilyId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;
    :cond_7
    move v1, v2

    .line 72
    goto :goto_0
.end method

.method public genre()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->genre:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const v3, 0xf4243

    const/4 v2, 0x0

    .line 77
    const/4 v0, 0x1

    .line 78
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 79
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 80
    mul-int/2addr v0, v3

    .line 81
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->genre:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 82
    mul-int/2addr v0, v3

    .line 83
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->localizedTitleFamilyName:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 84
    mul-int/2addr v0, v3

    .line 85
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->titleFamilyId:Ljava/lang/String;

    if-nez v1, :cond_3

    :goto_3
    xor-int/2addr v0, v2

    .line 86
    return v0

    .line 79
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->hashCode()I

    move-result v1

    goto :goto_0

    .line 81
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->genre:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;->hashCode()I

    move-result v1

    goto :goto_1

    .line 83
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->localizedTitleFamilyName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 85
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->titleFamilyId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3
.end method

.method public localizedTitleFamilyName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->localizedTitleFamilyName:Ljava/lang/String;

    return-object v0
.end method

.method public titleFamilyId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->titleFamilyId:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClubTypeContainer{type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", genre="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->genre:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubGenre;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", localizedTitleFamilyName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->localizedTitleFamilyName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", titleFamilyId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->titleFamilyId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public type()Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubTypeContainer;->type:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    return-object v0
.end method
