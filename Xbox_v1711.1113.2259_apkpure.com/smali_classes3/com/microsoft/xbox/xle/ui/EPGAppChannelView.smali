.class public Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;
.super Landroid/widget/LinearLayout;
.source "EPGAppChannelView.java"

# interfaces
.implements Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;
.implements Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;
.implements Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;
    }
.end annotation


# static fields
.field private static final SCROLL_BLOCK_TIMEOUT_MS:I = 0x2710

.field private static mCache:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

.field private mConnectionErrorMessage:Landroid/view/View;

.field private mItemViewIterator:Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;

.field private mItemWidth:I

.field private mItemsView:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

.field private mNoShowsErrorMessage:Landroid/view/View;

.field private mSelectedView:Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 395
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mCache:Ljava/util/Stack;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 398
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 193
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;-><init>(Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mItemViewIterator:Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;

    .line 399
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;

    .prologue
    .line 44
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mItemWidth:I

    return v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;)Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;)Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mItemsView:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    return-object v0
.end method

.method public static getInstance(Landroid/view/ViewGroup;)Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;
    .locals 3
    .param p0, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 403
    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mCache:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 405
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 406
    .local v0, "inflator":Landroid/view/LayoutInflater;
    const v1, 0x7f0300ec

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;

    .line 408
    .end local v0    # "inflator":Landroid/view/LayoutInflater;
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mCache:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;

    goto :goto_0
.end method

.method private static recycle(Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;)V
    .locals 1
    .param p0, "view"    # Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;

    .prologue
    .line 413
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mCache:Ljava/util/Stack;

    invoke-virtual {v0, p0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 414
    return-void
.end method

.method private setItemWidth(I)V
    .locals 1
    .param p1, "itemWidth"    # I

    .prologue
    .line 366
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mItemWidth:I

    if-ne p1, v0, :cond_0

    .line 390
    :goto_0
    return-void

    .line 371
    :cond_0
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mItemWidth:I

    .line 376
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$2;-><init>(Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private setViewVisibilityIfNotNull(Landroid/view/View;I)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 59
    if-eqz p1, :cond_0

    .line 60
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 61
    invoke-virtual {p1}, Landroid/view/View;->refreshDrawableState()V

    .line 63
    :cond_0
    return-void
.end method

.method private updateItemWidth()V
    .locals 3

    .prologue
    .line 349
    const/4 v0, 0x0

    .line 350
    .local v0, "itemWidth":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->isLoaded()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mItemsView:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getHeight()I

    move-result v2

    if-eqz v2, :cond_0

    .line 353
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mItemsView:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->getInstance(Landroid/view/ViewGroup;)Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;

    move-result-object v1

    .line 354
    .local v1, "view":Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getImageAspectRatio()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->setAspectRatio(F)V

    .line 355
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->forceLayout()V

    .line 357
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mItemsView:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->measureChild(Landroid/view/View;)V

    .line 358
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->getMeasuredWidth()I

    move-result v0

    .line 361
    .end local v1    # "view":Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;
    :cond_0
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->setItemWidth(I)V

    .line 362
    return-void
.end method

.method private updateView()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/16 v3, 0x8

    .line 316
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->isLoaded()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    .line 318
    .local v0, "connectionSuccessful":Z
    :goto_0
    if-eqz v0, :cond_2

    .line 320
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->updateItemWidth()V

    .line 323
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mConnectionErrorMessage:Landroid/view/View;

    invoke-direct {p0, v2, v3}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->setViewVisibilityIfNotNull(Landroid/view/View;I)V

    .line 325
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getItemCount()I

    move-result v2

    if-gtz v2, :cond_1

    .line 326
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mNoShowsErrorMessage:Landroid/view/View;

    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->setViewVisibilityIfNotNull(Landroid/view/View;I)V

    .line 327
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mItemsView:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-direct {p0, v1, v3}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->setViewVisibilityIfNotNull(Landroid/view/View;I)V

    .line 345
    :goto_1
    return-void

    .end local v0    # "connectionSuccessful":Z
    :cond_0
    move v0, v1

    .line 316
    goto :goto_0

    .line 329
    .restart local v0    # "connectionSuccessful":Z
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mNoShowsErrorMessage:Landroid/view/View;

    invoke-direct {p0, v2, v3}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->setViewVisibilityIfNotNull(Landroid/view/View;I)V

    .line 330
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mItemsView:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->setViewVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_1

    .line 334
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getError()Ljava/lang/Throwable;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 335
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mConnectionErrorMessage:Landroid/view/View;

    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->setViewVisibilityIfNotNull(Landroid/view/View;I)V

    .line 336
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mItemsView:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-direct {p0, v1, v3}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->setViewVisibilityIfNotNull(Landroid/view/View;I)V

    .line 342
    :goto_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mNoShowsErrorMessage:Landroid/view/View;

    invoke-direct {p0, v1, v3}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->setViewVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_1

    .line 338
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mConnectionErrorMessage:Landroid/view/View;

    invoke-direct {p0, v2, v3}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->setViewVisibilityIfNotNull(Landroid/view/View;I)V

    .line 339
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mItemsView:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->setViewVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_2
.end method


# virtual methods
.method protected alignItems(I)I
    .locals 2
    .param p1, "offset"    # I

    .prologue
    .line 168
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mItemWidth:I

    if-nez v0, :cond_0

    .end local p1    # "offset":I
    :goto_0
    return p1

    .restart local p1    # "offset":I
    :cond_0
    int-to-float v0, p1

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mItemWidth:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mItemWidth:I

    mul-int p1, v0, v1

    goto :goto_0
.end method

.method public contentOffsetChanged(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;I)V
    .locals 0
    .param p1, "view"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;
    .param p2, "offset"    # I

    .prologue
    .line 302
    return-void
.end method

.method public getViewIterator(IZ)Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter$IViewIterator;
    .locals 1
    .param p1, "offset"    # I
    .param p2, "forward"    # Z

    .prologue
    .line 259
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mItemViewIterator:Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;->reset(IZ)Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$ItemViewIterator;

    move-result-object v0

    return-object v0
.end method

.method protected loadImages(Z)V
    .locals 4
    .param p1, "animate"    # Z

    .prologue
    .line 173
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mItemsView:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildCount()I

    move-result v0

    .line 174
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 175
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mItemsView:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;

    .line 176
    .local v2, "view":Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;
    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->loadImageIfNeeded(Z)V

    .line 174
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 178
    .end local v2    # "view":Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;
    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->addListener(Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;)V

    .line 117
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->updateView()V

    .line 119
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 120
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->removeListener(Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;)V

    .line 129
    :cond_0
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 130
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 67
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 69
    const v0, 0x7f0e0571

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mItemsView:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mItemsView:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setAdapter(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter;)V

    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mItemsView:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setListener(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IListener;)V

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mItemsView:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    new-instance v1, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$1;-><init>(Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    const v0, 0x7f0e0572

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mConnectionErrorMessage:Landroid/view/View;

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mConnectionErrorMessage:Landroid/view/View;

    invoke-direct {p0, v0, v2}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->setViewVisibilityIfNotNull(Landroid/view/View;I)V

    .line 82
    const v0, 0x7f0e0574

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mNoShowsErrorMessage:Landroid/view/View;

    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mNoShowsErrorMessage:Landroid/view/View;

    invoke-direct {p0, v0, v2}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->setViewVisibilityIfNotNull(Landroid/view/View;I)V

    .line 85
    return-void
.end method

.method protected onItemViewClicked(Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;)V
    .locals 3
    .param p1, "view"    # Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;

    .prologue
    .line 146
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->getData()Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    move-result-object v0

    .line 147
    .local v0, "item":Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->show(Landroid/content/Context;Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;)V

    .line 148
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 135
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 137
    if-eqz p1, :cond_0

    .line 138
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->updateItemWidth()V

    .line 140
    :cond_0
    return-void
.end method

.method public onPropertyChanged(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 185
    const-string v0, "loading"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->updateView()V

    .line 188
    :cond_0
    return-void
.end method

.method public reclaimView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 265
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mSelectedView:Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;

    if-ne v0, p1, :cond_0

    .line 266
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mSelectedView:Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;

    .line 269
    :cond_0
    check-cast p1, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;

    .end local p1    # "view":Landroid/view/View;
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->recycle()V

    .line 270
    return-void
.end method

.method public recycle()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 308
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->setSelected(Z)V

    .line 309
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->setPressed(Z)V

    .line 311
    invoke-static {p0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->recycle(Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;)V

    .line 312
    return-void
.end method

.method public scrollStateChanged(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)V
    .locals 3
    .param p1, "view"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;
    .param p2, "state"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    .prologue
    .line 278
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView$3;->$SwitchMap$com$microsoft$xbox$xle$ui$virtualgrid$HorizontalViewScroller$ScrollState:[I

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 298
    :goto_0
    return-void

    .line 280
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;->ListScroll:Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;

    const/16 v2, 0x2710

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->setBlocking(Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;I)V

    .line 281
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getFlingStop()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->alignItems(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setFlingStop(I)V

    goto :goto_0

    .line 286
    :pswitch_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;->ListScroll:Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;

    const/16 v2, 0xc8

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->setBlocking(Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;I)V

    goto :goto_0

    .line 290
    :pswitch_2
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;->ListScroll:Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->clearBlocking(Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;)V

    .line 291
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getContentOffset()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->alignItems(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->smoothScrollTo(I)V

    .line 292
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->loadImages(Z)V

    goto :goto_0

    .line 278
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setChannel(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;)V
    .locals 2
    .param p1, "channel"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;

    .prologue
    .line 89
    invoke-interface {p1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;->getData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    .line 90
    .local v0, "model":Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    if-eq v1, v0, :cond_2

    .line 92
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    if-eqz v1, :cond_0

    .line 93
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->removeListener(Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;)V

    .line 96
    :cond_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    .line 98
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    if-eqz v1, :cond_1

    .line 99
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mChannel:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->addListener(Lcom/microsoft/xbox/service/model/epg/PropertyNotification$IPropertyListener;)V

    .line 102
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->updateView()V

    .line 105
    :cond_2
    if-eqz v0, :cond_3

    .line 106
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->refresh()V

    .line 108
    :cond_3
    return-void
.end method

.method public setSelected(Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;)V
    .locals 2
    .param p1, "view"    # Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;

    .prologue
    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mSelectedView:Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;

    if-ne v0, p1, :cond_1

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mSelectedView:Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;

    if-eqz v0, :cond_2

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mSelectedView:Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->setSelected(Z)V

    .line 160
    :cond_2
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mSelectedView:Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;

    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mSelectedView:Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGAppChannelView;->mSelectedView:Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGAppChannelItemView;->setSelected(Z)V

    goto :goto_0
.end method
