.class public final enum Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;
.super Ljava/lang/Enum;
.source "EnforcementServiceStub.java"

# interfaces
.implements Lcom/microsoft/xbox/service/enforcement/IEnforcementService;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;",
        ">;",
        "Lcom/microsoft/xbox/service/enforcement/IEnforcementService;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 12
    new-instance v0, Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;

    .line 11
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;

    sget-object v1, Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;->$VALUES:[Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;

    .line 14
    sget-object v0, Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    const-class v0, Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;->$VALUES:[Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/enforcement/EnforcementServiceStub;

    return-object v0
.end method


# virtual methods
.method public sendReport(Lcom/microsoft/xbox/service/model/sls/ReportUserData;Ljava/lang/String;)Z
    .locals 1
    .param p1, "reportUserData"    # Lcom/microsoft/xbox/service/model/sls/ReportUserData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "reportTargetId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 18
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 19
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/sls/ReportUserData;->feedbackContext:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 20
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/sls/ReportUserData;->feedbackType:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 21
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 22
    const/4 v0, 0x1

    return v0
.end method
