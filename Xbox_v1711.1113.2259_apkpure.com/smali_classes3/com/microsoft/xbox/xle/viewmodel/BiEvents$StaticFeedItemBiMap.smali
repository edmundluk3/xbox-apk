.class Lcom/microsoft/xbox/xle/viewmodel/BiEvents$StaticFeedItemBiMap;
.super Ljava/lang/Object;
.source "BiEvents.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/BiEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "StaticFeedItemBiMap"
.end annotation


# instance fields
.field private final map:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;",
            "Lcom/microsoft/xbox/xle/viewmodel/BiEvents$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final sharedMap:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;",
            "Lcom/microsoft/xbox/xle/viewmodel/BiEvents$Event;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "map":Ljava/util/Map;, "Ljava/util/Map<Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$Event;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$StaticFeedItemBiMap;-><init>(Ljava/util/Map;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;)V
    .locals 0
    .param p2, "sharedMap"    # Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;",
            "Lcom/microsoft/xbox/xle/viewmodel/BiEvents$Event;",
            ">;",
            "Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;",
            ")V"
        }
    .end annotation

    .prologue
    .line 50
    .local p1, "map":Ljava/util/Map;, "Ljava/util/Map<Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$Event;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$StaticFeedItemBiMap;->map:Ljava/util/Map;

    .line 52
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$StaticFeedItemBiMap;->sharedMap:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

    .line 53
    return-void
.end method


# virtual methods
.method public getBiForSharedItem()Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$StaticFeedItemBiMap;->sharedMap:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

    return-object v0
.end method

.method public getEvent(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;)Lcom/microsoft/xbox/xle/viewmodel/BiEvents$Event;
    .locals 1
    .param p1, "biPoint"    # Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$StaticFeedItemBiMap;->map:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$Event;

    return-object v0
.end method
