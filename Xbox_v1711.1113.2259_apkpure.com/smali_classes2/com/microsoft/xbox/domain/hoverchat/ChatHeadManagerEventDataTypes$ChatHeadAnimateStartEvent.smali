.class public abstract Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadAnimateStartEvent;
.super Ljava/lang/Object;
.source "ChatHeadManagerEventDataTypes.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadManagerEvent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ChatHeadAnimateStartEvent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljava/io/Serializable;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadManagerEvent",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 67
    .local p0, "this":Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadAnimateStartEvent;, "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadAnimateStartEvent<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lcom/flipkart/chatheads/ui/ChatHead;)Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadAnimateStartEvent;
    .locals 1
    .param p0    # Lcom/flipkart/chatheads/ui/ChatHead;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/io/Serializable;",
            ">(",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;)",
            "Lcom/microsoft/xbox/domain/hoverchat/ChatHeadManagerEventDataTypes$ChatHeadAnimateStartEvent",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 72
    .local p0, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<TT;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 73
    new-instance v0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadAnimateStartEvent;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_ChatHeadManagerEventDataTypes_ChatHeadAnimateStartEvent;-><init>(Lcom/flipkart/chatheads/ui/ChatHead;)V

    return-object v0
.end method


# virtual methods
.method public abstract chatHead()Lcom/flipkart/chatheads/ui/ChatHead;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<TT;>;"
        }
    .end annotation
.end method
