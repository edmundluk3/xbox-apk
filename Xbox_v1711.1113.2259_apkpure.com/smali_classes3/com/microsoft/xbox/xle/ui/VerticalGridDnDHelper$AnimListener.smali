.class abstract Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;
.super Ljava/lang/Object;
.source "VerticalGridDnDHelper.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "AnimListener"
.end annotation


# instance fields
.field protected firstHeight:I

.field protected firstPosition:I

.field protected repeatCounter:I

.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

.field protected topOffset:I


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)V
    .locals 0

    .prologue
    .line 388
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper<TT;>.AnimListener;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$1;

    .prologue
    .line 388
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper<TT;>.AnimListener;"
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;-><init>(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 410
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper<TT;>.AnimListener;"
    invoke-static {}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->access$1000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onAnimationCancel"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 405
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper<TT;>.AnimListener;"
    invoke-static {}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->access$1000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onAnimationEnd"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 415
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper<TT;>.AnimListener;"
    invoke-static {}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->access$1000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onAnimationRepeat"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;->repeatCounter:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;->repeatCounter:I

    .line 417
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;, "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper<TT;>.AnimListener;"
    const/4 v3, 0x0

    .line 395
    invoke-static {}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->access$1000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onAnimationStart"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    iput v3, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;->repeatCounter:I

    .line 397
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->access$400(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getFirstVisiblePosition()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;->firstPosition:I

    .line 398
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;->this$0:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->access$400(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;)Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 399
    .local v0, "v":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;->firstHeight:I

    .line 400
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$AnimListener;->topOffset:I

    .line 401
    return-void
.end method
