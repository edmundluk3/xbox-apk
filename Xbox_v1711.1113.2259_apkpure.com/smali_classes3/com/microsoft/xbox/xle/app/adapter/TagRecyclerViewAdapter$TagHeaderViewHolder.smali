.class public Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagHeaderViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "TagRecyclerViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "TagHeaderViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field headerContainer:Landroid/widget/LinearLayout;

.field headerText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;Landroid/view/View;)V
    .locals 2
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 213
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagHeaderViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    .line 214
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 216
    const v0, 0x7f0e0a9d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagHeaderViewHolder;->headerContainer:Landroid/widget/LinearLayout;

    .line 217
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagHeaderViewHolder;->headerContainer:Landroid/widget/LinearLayout;

    const v1, 0x7f0e0a9e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagHeaderViewHolder;->headerText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 218
    return-void
.end method


# virtual methods
.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 209
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagHeaderViewHolder;->onBind(Ljava/lang/String;)V

    return-void
.end method

.method public onBind(Ljava/lang/String;)V
    .locals 1
    .param p1, "dataObject"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 222
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 224
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagHeaderViewHolder;->headerText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 225
    return-void
.end method
