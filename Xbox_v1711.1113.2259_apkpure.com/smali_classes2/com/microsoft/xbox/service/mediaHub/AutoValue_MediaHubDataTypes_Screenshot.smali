.class final Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot;
.super Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Screenshot;
.source "AutoValue_MediaHubDataTypes_Screenshot.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_Screenshot$GsonTypeAdapter;
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/util/Date;Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;)V
    .locals 0
    .param p1, "captureDate"    # Ljava/util/Date;
    .param p2, "contentId"    # Ljava/lang/String;
    .param p4, "creationType"    # Ljava/lang/String;
    .param p5, "localId"    # Ljava/lang/String;
    .param p6, "ownerXuid"    # Ljava/lang/Long;
    .param p7, "resolutionHeight"    # Ljava/lang/Integer;
    .param p8, "resolutionWidth"    # Ljava/lang/Integer;
    .param p9, "sandboxId"    # Ljava/lang/String;
    .param p10, "titleData"    # Ljava/lang/String;
    .param p11, "titleId"    # Ljava/lang/String;
    .param p12, "titleName"    # Ljava/lang/String;
    .param p13, "dateUploaded"    # Ljava/util/Date;
    .param p14, "uploadLanguage"    # Ljava/lang/String;
    .param p15, "uploadRegion"    # Ljava/lang/String;
    .param p16, "uploadTitleId"    # Ljava/lang/Long;
    .param p17, "uploadDeviceType"    # Ljava/lang/String;
    .param p18, "userCaption"    # Ljava/lang/String;
    .param p19, "commentCount"    # Ljava/lang/Integer;
    .param p20, "likeCount"    # Ljava/lang/Integer;
    .param p21, "shareCount"    # Ljava/lang/Integer;
    .param p22, "viewCount"    # Ljava/lang/Integer;
    .param p23, "contentState"    # Ljava/lang/String;
    .param p24, "enforcementState"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            "Ljava/lang/String;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Session;",
            ">;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Stat;",
            ">;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p3, "contentLocators":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;>;"
    .local p25, "sessions":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Session;>;"
    .local p26, "stats":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Stat;>;"
    .local p27, "tournaments":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;>;"
    invoke-direct/range {p0 .. p27}, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_Screenshot;-><init>(Ljava/util/Date;Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;)V

    .line 29
    return-void
.end method
