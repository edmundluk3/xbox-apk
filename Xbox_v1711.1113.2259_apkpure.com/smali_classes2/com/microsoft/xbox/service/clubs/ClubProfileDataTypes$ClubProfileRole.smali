.class public final enum Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
.super Ljava/lang/Enum;
.source "ClubProfileDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ClubProfileRole"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

.field public static final enum Member:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

.field public static final enum Moderator:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

.field public static final enum Nonmember:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

.field public static final enum Owner:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    const-string v1, "Owner"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 26
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    const-string v1, "Moderator"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 27
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    const-string v1, "Member"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 28
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    const-string v1, "Nonmember"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->Nonmember:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 24
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->Nonmember:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromClubHubSettingsRole(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    .locals 3
    .param p0, "role"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 31
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 33
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$1;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubHubDataTypes$ClubHubSettingsRole:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 47
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t convert "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to ClubProfileRole"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    .line 44
    :goto_0
    return-object v0

    .line 38
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    goto :goto_0

    .line 41
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    goto :goto_0

    .line 44
    :pswitch_3
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->Nonmember:Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    goto :goto_0

    .line 33
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 24
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;

    return-object v0
.end method
