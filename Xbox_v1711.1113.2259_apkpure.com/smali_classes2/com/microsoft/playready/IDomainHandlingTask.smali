.class public interface abstract Lcom/microsoft/playready/IDomainHandlingTask;
.super Ljava/lang/Object;
.source "IDomainHandlingTask.java"

# interfaces
.implements Ljava/util/concurrent/Future;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Future",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract addDomainHandlerListener(Lcom/microsoft/playready/IDomainHandlerListener;)V
.end method

.method public abstract getDomainOperationType()Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;
.end method

.method public abstract getResponseCustomData()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public abstract removeDomainHandlerListener(Lcom/microsoft/playready/IDomainHandlerListener;)V
.end method
