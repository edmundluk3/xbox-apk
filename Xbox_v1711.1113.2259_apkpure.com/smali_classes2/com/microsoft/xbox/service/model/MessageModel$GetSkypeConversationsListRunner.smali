.class Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationsListRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "MessageModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/MessageModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetSkypeConversationsListRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final syncState:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/MessageModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;)V
    .locals 0
    .param p2, "syncState"    # Ljava/lang/String;

    .prologue
    .line 1184
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationsListRunner;->this$0:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 1185
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationsListRunner;->syncState:Ljava/lang/String;

    .line 1186
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;
    .locals 21
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1192
    const/4 v3, 0x0

    .line 1193
    .local v3, "currentPaginationCount":I
    const/4 v2, 0x0

    .line 1194
    .local v2, "backwardLink":Ljava/lang/String;
    const/4 v4, 0x1

    .line 1195
    .local v4, "firstLoadConversationList":Z
    const/4 v6, 0x0

    .line 1196
    .local v6, "list":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 1197
    .local v11, "senderXuids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    if-nez v4, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_1

    .line 1199
    :cond_0
    const/16 v18, 0x1

    move/from16 v0, v18

    if-lt v3, v0, :cond_4

    .line 1249
    :cond_1
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v18

    if-lez v18, :cond_10

    .line 1251
    new-instance v16, Lcom/microsoft/xbox/service/model/sls/UserPresenceBatchRequest;

    move-object/from16 v0, v16

    invoke-direct {v0, v11}, Lcom/microsoft/xbox/service/model/sls/UserPresenceBatchRequest;-><init>(Ljava/util/ArrayList;)V

    .line 1252
    .local v16, "userPresenceBatchRequest":Lcom/microsoft/xbox/service/model/sls/UserPresenceBatchRequest;
    const/4 v7, 0x0

    .line 1253
    .local v7, "presence":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/sls/UserPresenceBatchRequest;->users:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v18

    if-nez v18, :cond_2

    .line 1254
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v18

    invoke-static/range {v16 .. v16}, Lcom/microsoft/xbox/service/model/sls/UserPresenceBatchRequest;->getUserPresenceBatchRequestBody(Lcom/microsoft/xbox/service/model/sls/UserPresenceBatchRequest;)Ljava/lang/String;

    move-result-object v19

    invoke-interface/range {v18 .. v19}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getUserListPresenceInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;

    move-result-object v7

    .line 1257
    :cond_2
    new-instance v12, Ljava/util/Hashtable;

    invoke-direct {v12}, Ljava/util/Hashtable;-><init>()V

    .line 1258
    .local v12, "status":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/model/UserStatus;>;"
    if-eqz v7, :cond_c

    iget-object v0, v7, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;->userPresence:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v18

    if-nez v18, :cond_c

    .line 1259
    iget-object v0, v7, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;->userPresence:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_3
    :goto_1
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_c

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;

    .line 1260
    .local v14, "up":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;
    iget-object v0, v14, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->xuid:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_3

    .line 1261
    iget-object v0, v14, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->xuid:Ljava/lang/String;

    move-object/from16 v19, v0

    iget-object v0, v14, Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;->state:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/microsoft/xbox/service/model/UserStatus;->getStatusFromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/UserStatus;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v12, v0, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1203
    .end local v7    # "presence":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;
    .end local v12    # "status":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/model/UserStatus;>;"
    .end local v14    # "up":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$UserPresence;
    .end local v16    # "userPresenceBatchRequest":Lcom/microsoft/xbox/service/model/sls/UserPresenceBatchRequest;
    :cond_4
    if-eqz v4, :cond_5

    .line 1204
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationsListRunner;->syncState:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_9

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getSkypeConversationsList()Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;

    move-result-object v6

    .line 1206
    :goto_2
    if-eqz v6, :cond_5

    iget-object v0, v6, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    move-object/from16 v18, v0

    if-eqz v18, :cond_5

    .line 1207
    iget-object v0, v6, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->backwardLink:Ljava/lang/String;

    .line 1211
    :cond_5
    if-nez v4, :cond_6

    iget-object v0, v6, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    move-object/from16 v18, v0

    if-eqz v18, :cond_6

    .line 1212
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v18

    iget-object v0, v6, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->syncState:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-interface/range {v18 .. v19}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getSkypeConversationsList(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;

    move-result-object v8

    .line 1213
    .local v8, "result":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;
    add-int/lit8 v3, v3, 0x1

    .line 1215
    const/4 v2, 0x0

    .line 1216
    if-eqz v8, :cond_6

    iget-object v0, v8, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    move-object/from16 v18, v0

    if-eqz v18, :cond_6

    .line 1217
    iget-object v0, v6, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    move-object/from16 v18, v0

    iget-object v0, v8, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->syncState:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->syncState:Ljava/lang/String;

    .line 1218
    iget-object v0, v6, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    move-object/from16 v18, v0

    iget-object v0, v8, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->totalCount:I

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->totalCount:I

    .line 1219
    iget-object v0, v8, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;->_metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;->backwardLink:Ljava/lang/String;

    .line 1221
    iget-object v0, v8, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;->conversations:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v18

    if-nez v18, :cond_6

    .line 1222
    iget-object v0, v6, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;->conversations:Ljava/util/List;

    move-object/from16 v18, v0

    iget-object v0, v8, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;->conversations:Ljava/util/List;

    move-object/from16 v19, v0

    invoke-interface/range {v18 .. v19}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1227
    .end local v8    # "result":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;
    :cond_6
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1228
    .local v9, "sanitizedConversations":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;>;"
    if-eqz v6, :cond_b

    iget-object v0, v6, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;->conversations:Ljava/util/List;

    move-object/from16 v18, v0

    if-eqz v18, :cond_b

    .line 1229
    iget-object v0, v6, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;->conversations:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_7
    :goto_3
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_a

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;

    .line 1230
    .local v13, "summary":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;
    iget-object v0, v13, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    move-object/from16 v19, v0

    if-eqz v19, :cond_7

    .line 1231
    invoke-static {v13}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getXuidFromSkypeMessage(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;)Ljava/lang/String;

    move-result-object v10

    .line 1233
    .local v10, "senderXuid":Ljava/lang/String;
    iget-object v0, v13, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->id:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->isSkypeGroupMessage(Ljava/lang/String;)Z

    move-result v19

    move/from16 v0, v19

    iput-boolean v0, v13, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->isGroupConversation:Z

    .line 1235
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_8

    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_8

    const-string v19, "0"

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v19

    if-nez v19, :cond_8

    .line 1236
    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1239
    :cond_8
    invoke-interface {v9, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1204
    .end local v9    # "sanitizedConversations":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;>;"
    .end local v10    # "senderXuid":Ljava/lang/String;
    .end local v13    # "summary":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;
    :cond_9
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationsListRunner;->syncState:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-interface/range {v18 .. v19}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getSkypeConversationsList(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;

    move-result-object v6

    goto/16 :goto_2

    .line 1243
    .restart local v9    # "sanitizedConversations":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;>;"
    :cond_a
    iput-object v9, v6, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;->conversations:Ljava/util/List;

    .line 1246
    :cond_b
    const/4 v4, 0x0

    .line 1247
    goto/16 :goto_0

    .line 1266
    .end local v9    # "sanitizedConversations":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;>;"
    .restart local v7    # "presence":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;
    .restart local v12    # "status":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/model/UserStatus;>;"
    .restart local v16    # "userPresenceBatchRequest":Lcom/microsoft/xbox/service/model/sls/UserPresenceBatchRequest;
    :cond_c
    new-instance v17, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;

    move-object/from16 v0, v17

    invoke-direct {v0, v11}, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;-><init>(Ljava/util/ArrayList;)V

    .line 1267
    .local v17, "userProfileRequest":Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v18

    invoke-static/range {v17 .. v17}, Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;->getUserProfileRequestBody(Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;)Ljava/lang/String;

    move-result-object v19

    invoke-interface/range {v18 .. v19}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getUserProfileInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;

    move-result-object v8

    .line 1269
    .local v8, "result":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    if-eqz v6, :cond_10

    if-eqz v8, :cond_10

    iget-object v0, v8, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v18

    if-nez v18, :cond_10

    .line 1270
    iget-object v0, v8, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;->profileUsers:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_d
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_10

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;

    .line 1271
    .local v15, "user":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    sget-object v18, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->GameDisplayPicRaw:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    move-object/from16 v0, v18

    invoke-static {v15, v0}, Lcom/microsoft/xbox/service/model/MessageModel;->access$1000(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v5

    .line 1273
    .local v5, "imageUrl":Ljava/lang/String;
    iget-object v0, v6, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;->conversations:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v20

    :cond_e
    :goto_4
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_d

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;

    .line 1274
    .restart local v13    # "summary":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;
    invoke-static {v13}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getXuidFromSkypeMessage(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;)Ljava/lang/String;

    move-result-object v10

    .line 1275
    .restart local v10    # "senderXuid":Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_e

    iget-object v0, v15, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->id:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_e

    .line 1276
    iput-object v5, v13, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->gamerPicUrl:Ljava/lang/String;

    .line 1277
    invoke-static {v15}, Lcom/microsoft/xbox/service/model/MessageModel;->access$1100(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v13, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->realName:Ljava/lang/String;

    .line 1278
    invoke-virtual {v12, v10}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/microsoft/xbox/service/model/UserStatus;

    move-object/from16 v0, v18

    iput-object v0, v13, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    .line 1279
    iget-object v0, v13, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    move-object/from16 v18, v0

    if-nez v18, :cond_f

    .line 1280
    sget-object v18, Lcom/microsoft/xbox/service/model/UserStatus;->Offline:Lcom/microsoft/xbox/service/model/UserStatus;

    move-object/from16 v0, v18

    iput-object v0, v13, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    .line 1283
    :cond_f
    sget-object v18, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->Gamertag:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    move-object/from16 v0, v18

    invoke-static {v15, v0}, Lcom/microsoft/xbox/service/model/MessageModel;->access$1000(Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v13, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->senderGamerTag:Ljava/lang/String;

    goto :goto_4

    .line 1290
    .end local v5    # "imageUrl":Ljava/lang/String;
    .end local v7    # "presence":Lcom/microsoft/xbox/service/network/managers/IFollowerPresenceResult$FollowersPresenceResult;
    .end local v8    # "result":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$UserProfileResult;
    .end local v10    # "senderXuid":Ljava/lang/String;
    .end local v12    # "status":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/model/UserStatus;>;"
    .end local v13    # "summary":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;
    .end local v15    # "user":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    .end local v16    # "userPresenceBatchRequest":Lcom/microsoft/xbox/service/model/sls/UserPresenceBatchRequest;
    .end local v17    # "userProfileRequest":Lcom/microsoft/xbox/service/model/sls/UserProfileRequest;
    :cond_10
    invoke-static {v6}, Lcom/microsoft/xbox/service/model/MessageModel;->access$1200(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    move-result-object v18

    return-object v18
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1179
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationsListRunner;->buildData()Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 1306
    const-wide/16 v0, 0xbea

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1300
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationListResult;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationsListRunner;->this$0:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationsListRunner;->syncState:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->access$1300(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V

    .line 1301
    return-void
.end method

.method public onPreExecute()V
    .locals 2

    .prologue
    .line 1295
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel$GetSkypeConversationsListRunner;->this$0:Lcom/microsoft/xbox/service/model/MessageModel;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/microsoft/xbox/service/model/MessageModel;->isLoading:Z

    .line 1296
    return-void
.end method
