.class final Lcom/jakewharton/rxrelay2/SerializedRelay;
.super Lcom/jakewharton/rxrelay2/Relay;
.source "SerializedRelay.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/jakewharton/rxrelay2/Relay",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final actual:Lcom/jakewharton/rxrelay2/Relay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/Relay",
            "<TT;>;"
        }
    .end annotation
.end field

.field private emitting:Z

.field private queue:Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/jakewharton/rxrelay2/Relay;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jakewharton/rxrelay2/Relay",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 33
    .local p0, "this":Lcom/jakewharton/rxrelay2/SerializedRelay;, "Lcom/jakewharton/rxrelay2/SerializedRelay<TT;>;"
    .local p1, "actual":Lcom/jakewharton/rxrelay2/Relay;, "Lcom/jakewharton/rxrelay2/Relay<TT;>;"
    invoke-direct {p0}, Lcom/jakewharton/rxrelay2/Relay;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/jakewharton/rxrelay2/SerializedRelay;->actual:Lcom/jakewharton/rxrelay2/Relay;

    .line 35
    return-void
.end method

.method private emitLoop()V
    .locals 2

    .prologue
    .line 65
    .local p0, "this":Lcom/jakewharton/rxrelay2/SerializedRelay;, "Lcom/jakewharton/rxrelay2/SerializedRelay<TT;>;"
    :goto_0
    monitor-enter p0

    .line 66
    :try_start_0
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/SerializedRelay;->queue:Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;

    .line 67
    .local v0, "q":Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;, "Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList<TT;>;"
    if-nez v0, :cond_0

    .line 68
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/jakewharton/rxrelay2/SerializedRelay;->emitting:Z

    .line 69
    monitor-exit p0

    return-void

    .line 71
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/jakewharton/rxrelay2/SerializedRelay;->queue:Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;

    .line 72
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    iget-object v1, p0, Lcom/jakewharton/rxrelay2/SerializedRelay;->actual:Lcom/jakewharton/rxrelay2/Relay;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;->accept(Lcom/jakewharton/rxrelay2/Relay;)Z

    goto :goto_0

    .line 72
    .end local v0    # "q":Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;, "Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList<TT;>;"
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method


# virtual methods
.method public accept(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "this":Lcom/jakewharton/rxrelay2/SerializedRelay;, "Lcom/jakewharton/rxrelay2/SerializedRelay<TT;>;"
    .local p1, "value":Ljava/lang/Object;, "TT;"
    monitor-enter p0

    .line 46
    :try_start_0
    iget-boolean v1, p0, Lcom/jakewharton/rxrelay2/SerializedRelay;->emitting:Z

    if-eqz v1, :cond_1

    .line 47
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/SerializedRelay;->queue:Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;

    .line 48
    .local v0, "q":Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;, "Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList<TT;>;"
    if-nez v0, :cond_0

    .line 49
    new-instance v0, Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;

    .end local v0    # "q":Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;, "Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList<TT;>;"
    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;-><init>(I)V

    .line 50
    .restart local v0    # "q":Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;, "Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList<TT;>;"
    iput-object v0, p0, Lcom/jakewharton/rxrelay2/SerializedRelay;->queue:Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;

    .line 52
    :cond_0
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;->add(Ljava/lang/Object;)V

    .line 53
    monitor-exit p0

    .line 59
    .end local v0    # "q":Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList;, "Lcom/jakewharton/rxrelay2/AppendOnlyLinkedArrayList<TT;>;"
    :goto_0
    return-void

    .line 55
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/jakewharton/rxrelay2/SerializedRelay;->emitting:Z

    .line 56
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    iget-object v1, p0, Lcom/jakewharton/rxrelay2/SerializedRelay;->actual:Lcom/jakewharton/rxrelay2/Relay;

    invoke-virtual {v1, p1}, Lcom/jakewharton/rxrelay2/Relay;->accept(Ljava/lang/Object;)V

    .line 58
    invoke-direct {p0}, Lcom/jakewharton/rxrelay2/SerializedRelay;->emitLoop()V

    goto :goto_0

    .line 56
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public hasObservers()Z
    .locals 1

    .prologue
    .line 79
    .local p0, "this":Lcom/jakewharton/rxrelay2/SerializedRelay;, "Lcom/jakewharton/rxrelay2/SerializedRelay<TT;>;"
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/SerializedRelay;->actual:Lcom/jakewharton/rxrelay2/Relay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/Relay;->hasObservers()Z

    move-result v0

    return v0
.end method

.method protected subscribeActual(Lio/reactivex/Observer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observer",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p0, "this":Lcom/jakewharton/rxrelay2/SerializedRelay;, "Lcom/jakewharton/rxrelay2/SerializedRelay<TT;>;"
    .local p1, "observer":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-TT;>;"
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/SerializedRelay;->actual:Lcom/jakewharton/rxrelay2/Relay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/Relay;->subscribe(Lio/reactivex/Observer;)V

    .line 40
    return-void
.end method
