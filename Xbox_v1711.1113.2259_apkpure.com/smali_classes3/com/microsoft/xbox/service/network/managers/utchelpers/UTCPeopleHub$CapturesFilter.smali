.class public final enum Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;
.super Ljava/lang/Enum;
.source "UTCPeopleHub.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CapturesFilter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;

.field public static final enum Everything:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;

.field public static final enum GameClips:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;

.field public static final enum Screenshots:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;

.field public static final enum Unknown:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 70
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;

    const-string v1, "Unknown"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;->Unknown:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;

    .line 71
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;

    const-string v1, "Everything"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;->Everything:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;

    .line 72
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;

    const-string v1, "GameClips"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;->GameClips:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;

    .line 73
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;

    const-string v1, "Screenshots"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;->Screenshots:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;

    .line 69
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;->Unknown:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;->Everything:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;->GameClips:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;->Screenshots:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 69
    const-class v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub$CapturesFilter;

    return-object v0
.end method
