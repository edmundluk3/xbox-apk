.class public Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "WebViewDialog.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private url:Ljava/lang/String;

.field private final webView:Landroid/webkit/WebView;

.field private final webViewClient:Landroid/webkit/WebViewClient;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 25
    const v0, 0x7f08021b

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 26
    const v0, 0x7f030275

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;->setContentView(I)V

    .line 28
    const v0, 0x7f0e0bd3

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;->webView:Landroid/webkit/WebView;

    .line 29
    new-instance v0, Landroid/webkit/WebViewClient;

    invoke-direct {v0}, Landroid/webkit/WebViewClient;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;->webViewClient:Landroid/webkit/WebViewClient;

    .line 30
    const v0, 0x7f0e0bd2

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 32
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;->url:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog$1;-><init>(Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;->webView:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;->webViewClient:Landroid/webkit/WebViewClient;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;->webView:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 53
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    :cond_0
    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;->url:Ljava/lang/String;

    .line 64
    return-void
.end method
