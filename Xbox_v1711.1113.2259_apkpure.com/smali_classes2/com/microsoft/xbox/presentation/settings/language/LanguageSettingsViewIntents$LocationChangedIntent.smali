.class public abstract Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewIntents$LocationChangedIntent;
.super Ljava/lang/Object;
.source "LanguageSettingsViewIntents.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewIntents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "LocationChangedIntent"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;)Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewIntents$LocationChangedIntent;
    .locals 1
    .param p0, "newLocation"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 35
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 36
    new-instance v0, Lcom/microsoft/xbox/presentation/settings/language/AutoValue_LanguageSettingsViewIntents_LocationChangedIntent;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/presentation/settings/language/AutoValue_LanguageSettingsViewIntents_LocationChangedIntent;-><init>(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;)V

    return-object v0
.end method


# virtual methods
.method public abstract newLocation()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
