.class Lretrofit2/mock/BehaviorDelegate$1;
.super Ljava/lang/Object;
.source "BehaviorDelegate.java"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lretrofit2/mock/BehaviorDelegate;->returning(Lretrofit2/Call;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lretrofit2/mock/BehaviorDelegate;

.field final synthetic val$behaviorCall:Lretrofit2/Call;


# direct methods
.method constructor <init>(Lretrofit2/mock/BehaviorDelegate;Lretrofit2/Call;)V
    .locals 0
    .param p1, "this$0"    # Lretrofit2/mock/BehaviorDelegate;

    .prologue
    .line 57
    .local p0, "this":Lretrofit2/mock/BehaviorDelegate$1;, "Lretrofit2/mock/BehaviorDelegate$1;"
    iput-object p1, p0, Lretrofit2/mock/BehaviorDelegate$1;->this$0:Lretrofit2/mock/BehaviorDelegate;

    iput-object p2, p0, Lretrofit2/mock/BehaviorDelegate$1;->val$behaviorCall:Lretrofit2/Call;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1, "proxy"    # Ljava/lang/Object;
    .param p2, "method"    # Ljava/lang/reflect/Method;
    .param p3, "args"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/reflect/Method;",
            "[",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 60
    .local p0, "this":Lretrofit2/mock/BehaviorDelegate$1;, "Lretrofit2/mock/BehaviorDelegate$1;"
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getGenericReturnType()Ljava/lang/reflect/Type;

    move-result-object v2

    .line 61
    .local v2, "returnType":Ljava/lang/reflect/Type;
    invoke-virtual {p2}, Ljava/lang/reflect/Method;->getAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v1

    .line 62
    .local v1, "methodAnnotations":[Ljava/lang/annotation/Annotation;
    iget-object v3, p0, Lretrofit2/mock/BehaviorDelegate$1;->this$0:Lretrofit2/mock/BehaviorDelegate;

    iget-object v3, v3, Lretrofit2/mock/BehaviorDelegate;->retrofit:Lretrofit2/Retrofit;

    .line 63
    invoke-virtual {v3, v2, v1}, Lretrofit2/Retrofit;->callAdapter(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;)Lretrofit2/CallAdapter;

    move-result-object v0

    .line 64
    .local v0, "callAdapter":Lretrofit2/CallAdapter;, "Lretrofit2/CallAdapter<TR;TT;>;"
    iget-object v3, p0, Lretrofit2/mock/BehaviorDelegate$1;->val$behaviorCall:Lretrofit2/Call;

    invoke-interface {v0, v3}, Lretrofit2/CallAdapter;->adapt(Lretrofit2/Call;)Ljava/lang/Object;

    move-result-object v3

    return-object v3
.end method
