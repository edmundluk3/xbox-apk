.class public final Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;
.super Ljava/lang/Object;
.source "TrendingBeamInteractor.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final initialLoadTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final loadMoreTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreResult",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final refreshTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/data/repository/beam/BeamRepository;)V
    .locals 1
    .param p1, "repository"    # Lcom/microsoft/xbox/data/repository/beam/BeamRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {p1}, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/beam/BeamRepository;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;->initialLoadTransformer:Lio/reactivex/ObservableTransformer;

    .line 48
    invoke-static {p1}, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/beam/BeamRepository;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;->loadMoreTransformer:Lio/reactivex/ObservableTransformer;

    .line 58
    invoke-static {p1}, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/beam/BeamRepository;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;->refreshTransformer:Lio/reactivex/ObservableTransformer;

    .line 67
    return-void
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/data/repository/beam/BeamRepository;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/beam/BeamRepository;
    .param p1, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 38
    invoke-static {p0}, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor$$Lambda$12;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/beam/BeamRepository;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 39
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 38
    return-object v0
.end method

.method static synthetic lambda$new$5(Lcom/microsoft/xbox/data/repository/beam/BeamRepository;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/beam/BeamRepository;
    .param p1, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 48
    invoke-static {p0}, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/beam/BeamRepository;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 49
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 48
    return-object v0
.end method

.method static synthetic lambda$new$8(Lcom/microsoft/xbox/data/repository/beam/BeamRepository;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/beam/BeamRepository;
    .param p1, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 58
    invoke-static {p0}, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/beam/BeamRepository;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 59
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 58
    return-object v0
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 45
    sget-object v0, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/data/repository/beam/BeamRepository;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/beam/BeamRepository;
    .param p1, "action"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/repository/beam/BeamRepository;->load()Lio/reactivex/Single;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor$$Lambda$13;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 42
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor$$Lambda$14;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 43
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 44
    invoke-static {}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->inFlightInstance()Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor$$Lambda$15;->lambdaFactory$(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 45
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 40
    return-object v0
.end method

.method static synthetic lambda$null$3(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 55
    sget-object v0, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$4(Lcom/microsoft/xbox/data/repository/beam/BeamRepository;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/beam/BeamRepository;
    .param p1, "action"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/repository/beam/BeamRepository;->load()Lio/reactivex/Single;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor$$Lambda$9;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 52
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor$$Lambda$10;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 53
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 54
    invoke-static {}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreResult;->inFlightInstance()Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor$$Lambda$11;->lambdaFactory$(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 55
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 50
    return-object v0
.end method

.method static synthetic lambda$null$6(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 65
    sget-object v0, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$7(Lcom/microsoft/xbox/data/repository/beam/BeamRepository;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "repository"    # Lcom/microsoft/xbox/data/repository/beam/BeamRepository;
    .param p1, "action"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/repository/beam/BeamRepository;->refresh()Lio/reactivex/Single;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor$$Lambda$5;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 62
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor$$Lambda$6;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 63
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 64
    invoke-static {}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;->inFlightInstance()Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 65
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 60
    return-object v0
.end method


# virtual methods
.method public initialLoadTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;->initialLoadTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public loadMoreTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$LoadMoreResult",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;->loadMoreTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public refreshTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$RefreshResult",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/domain/beam/BeamChannel;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/domain/beam/TrendingBeamInteractor;->refreshTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method
