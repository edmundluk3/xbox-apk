.class public final Lcom/microsoft/xbox/idp/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/idp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f070000

.field public static final abc_action_bar_home_description_format:I = 0x7f070001

.field public static final abc_action_bar_home_subtitle_description_format:I = 0x7f070002

.field public static final abc_action_bar_up_description:I = 0x7f070003

.field public static final abc_action_menu_overflow_description:I = 0x7f070004

.field public static final abc_action_mode_done:I = 0x7f070005

.field public static final abc_activity_chooser_view_see_all:I = 0x7f070006

.field public static final abc_activitychooserview_choose_application:I = 0x7f070007

.field public static final abc_capital_off:I = 0x7f070008

.field public static final abc_capital_on:I = 0x7f070009

.field public static final abc_font_family_body_1_material:I = 0x7f070e2e

.field public static final abc_font_family_body_2_material:I = 0x7f070e2f

.field public static final abc_font_family_button_material:I = 0x7f070e30

.field public static final abc_font_family_caption_material:I = 0x7f070e31

.field public static final abc_font_family_display_1_material:I = 0x7f070e32

.field public static final abc_font_family_display_2_material:I = 0x7f070e33

.field public static final abc_font_family_display_3_material:I = 0x7f070e34

.field public static final abc_font_family_display_4_material:I = 0x7f070e35

.field public static final abc_font_family_headline_material:I = 0x7f070e36

.field public static final abc_font_family_menu_material:I = 0x7f070e37

.field public static final abc_font_family_subhead_material:I = 0x7f070e38

.field public static final abc_font_family_title_material:I = 0x7f070e39

.field public static final abc_search_hint:I = 0x7f07000a

.field public static final abc_searchview_description_clear:I = 0x7f07000b

.field public static final abc_searchview_description_query:I = 0x7f07000c

.field public static final abc_searchview_description_search:I = 0x7f07000d

.field public static final abc_searchview_description_submit:I = 0x7f07000e

.field public static final abc_searchview_description_voice:I = 0x7f07000f

.field public static final abc_shareactionprovider_share_with:I = 0x7f070010

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f070011

.field public static final abc_toolbar_collapse_description:I = 0x7f070012

.field public static final account_menu_add_account:I = 0x7f070047

.field public static final account_picker_list_body:I = 0x7f070048

.field public static final account_picker_list_header:I = 0x7f070049

.field public static final account_picker_menu_dismiss:I = 0x7f07004a

.field public static final account_setting_up:I = 0x7f07004b

.field public static final account_setting_up_header:I = 0x7f07004c

.field public static final app_market:I = 0x7f07004d

.field public static final authentication_button_finish:I = 0x7f07004e

.field public static final authentication_button_next:I = 0x7f07004f

.field public static final authentication_button_previous:I = 0x7f070050

.field public static final error_body_generic_failure:I = 0x7f070051

.field public static final error_header_generic_failure:I = 0x7f070052

.field public static final error_header_server_network_error:I = 0x7f070053

.field public static final error_overlay_no_network:I = 0x7f070054

.field public static final sdk_version_name:I = 0x7f07108d

.field public static final search_menu_title:I = 0x7f070045

.field public static final sign_out_dialog_button_cancel:I = 0x7f070055

.field public static final sign_out_dialog_button_sign_out:I = 0x7f070056

.field public static final sign_out_dialog_checkbox:I = 0x7f070057

.field public static final sign_out_dialog_title:I = 0x7f070058

.field public static final status_bar_notification_info_overflow:I = 0x7f070046

.field public static final user_tile_image_content_description:I = 0x7f070059

.field public static final webflow_header:I = 0x7f07005a

.field public static final xbid_age_group_adult:I = 0x7f070dcf

.field public static final xbid_age_group_adult_details_android:I = 0x7f070dd0

.field public static final xbid_age_group_child:I = 0x7f070dd1

.field public static final xbid_age_group_child_details_android:I = 0x7f070dd2

.field public static final xbid_age_group_teen:I = 0x7f070dd3

.field public static final xbid_age_group_teen_details_android:I = 0x7f070dd4

.field public static final xbid_another_gamertag:I = 0x7f070dd5

.field public static final xbid_another_sign_in:I = 0x7f070dd6

.field public static final xbid_ban_error_body_android:I = 0x7f070dd7

.field public static final xbid_ban_error_header_android:I = 0x7f070dd8

.field public static final xbid_catchall_error_android:I = 0x7f070dd9

.field public static final xbid_claim_it:I = 0x7f070dda

.field public static final xbid_close:I = 0x7f070ddb

.field public static final xbid_cobrand_id:I = 0x7f07109b

.field public static final xbid_creation_error_android:I = 0x7f070ddc

.field public static final xbid_customize_profile:I = 0x7f070ddd

.field public static final xbid_different_gamer_tag_answer:I = 0x7f070dde

.field public static final xbid_error_offline_android:I = 0x7f070ddf

.field public static final xbid_first_and_last_name_android:I = 0x7f070de0

.field public static final xbid_friend_finder_opt_in:I = 0x7f070de1

.field public static final xbid_gamertag_available:I = 0x7f070de2

.field public static final xbid_gamertag_check_availability:I = 0x7f070de3

.field public static final xbid_gamertag_checking_android:I = 0x7f070de4

.field public static final xbid_gamertag_checking_error:I = 0x7f070de5

.field public static final xbid_gamertag_not_available_android:I = 0x7f070de6

.field public static final xbid_gamertag_not_available_no_suggestions_android:I = 0x7f070de7

.field public static final xbid_gamertag_placeholder:I = 0x7f070de8

.field public static final xbid_introducing_android:I = 0x7f070de9

.field public static final xbid_lets_play_android:I = 0x7f070dea

.field public static final xbid_more_info:I = 0x7f070deb

.field public static final xbid_next:I = 0x7f070dec

.field public static final xbid_privacy_settings_header_android:I = 0x7f070ded

.field public static final xbid_sign_up_header:I = 0x7f070dee

.field public static final xbid_sign_up_subheader_android:I = 0x7f070def

.field public static final xbid_tools_email:I = 0x7f07109c

.field public static final xbid_tools_empty:I = 0x7f07109d

.field public static final xbid_tools_gamertag:I = 0x7f07109e

.field public static final xbid_tools_user_name:I = 0x7f07109f

.field public static final xbid_try_again:I = 0x7f070df0

.field public static final xbid_voice_over_close_button_text:I = 0x7f070df1

.field public static final xbid_voice_over_game_score_label_text:I = 0x7f070df2

.field public static final xbid_welcome_back:I = 0x7f070df3


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 838
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
