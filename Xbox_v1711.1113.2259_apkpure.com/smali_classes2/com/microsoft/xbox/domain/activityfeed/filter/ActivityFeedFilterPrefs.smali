.class public abstract Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
.super Ljava/lang/Object;
.source "ActivityFeedFilterPrefs.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/logging/Loggable;


# static fields
.field private static final DEFAULTS:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 16
    invoke-static {v0, v0, v0, v0, v0}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->with(ZZZZZ)Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->DEFAULTS:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static defaultPrefs()Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;->DEFAULTS:Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    return-object v0
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    new-instance v0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(ZZZZZ)Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    .locals 6
    .param p0, "showFriends"    # Z
    .param p1, "showFavorites"    # Z
    .param p2, "showClubs"    # Z
    .param p3, "showGames"    # Z
    .param p4, "showPopular"    # Z

    .prologue
    .line 24
    new-instance v0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs;

    move v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterPrefs;-><init>(ZZZZZ)V

    return-object v0
.end method


# virtual methods
.method public abstract showClubs()Z
.end method

.method public abstract showFavorites()Z
.end method

.method public abstract showFriends()Z
.end method

.method public abstract showGames()Z
.end method

.method public abstract showPopular()Z
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
