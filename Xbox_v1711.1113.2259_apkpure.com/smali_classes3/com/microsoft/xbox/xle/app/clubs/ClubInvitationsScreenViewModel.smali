.class public Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "ClubInvitationsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$IgnoreInviteCallback;,
        Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$AcceptInviteCallback;,
        Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$LoadUserClubsAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

.field private final getPersonSummariesResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;>;"
        }
    .end annotation
.end field

.field private final ignoreAllInvitationSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private ignoreInvitationsError:Z

.field private final invitationClubList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation
.end field

.field private final invitationList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;",
            ">;"
        }
    .end annotation
.end field

.field private isLoadingUserClubs:Z

.field private final isProcessingInvitations:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private loadUserClubsAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$LoadUserClubsAsyncTask;

.field private meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private final userDataMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;"
        }
    .end annotation
.end field

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 90
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
    .locals 2
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .param p2, "parent"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 73
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->setParent(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 74
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->initializeAdapter()V

    .line 76
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->invitationClubList:Ljava/util/List;

    .line 78
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->userDataMap:Ljava/util/Map;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->invitationList:Ljava/util/List;

    .line 80
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->ignoreAllInvitationSet:Ljava/util/Set;

    .line 81
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->getPersonSummariesResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 83
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->isLoadingUserClubs:Z

    .line 84
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->isProcessingInvitations:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 85
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->ignoreInvitationsError:Z

    .line 86
    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    return-object v0
.end method

.method static synthetic access$402(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->isLoadingUserClubs:Z

    return p1
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->onLoadUserClubsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;JLjava/util/List;)V
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;
    .param p1, "x1"    # J
    .param p3, "x2"    # Ljava/util/List;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->onAcceptInviteSuccess(JLjava/util/List;)V

    return-void
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;Ljava/util/List;J)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # J

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->onAcceptInviteFailure(Ljava/util/List;J)V

    return-void
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;J)V
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;
    .param p1, "x1"    # J

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->onIgnoreInviteSuccess(J)V

    return-void
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;J)V
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;
    .param p1, "x1"    # J

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->onIgnoreInviteFailure(J)V

    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->onUserDataLoadCompleted(Ljava/util/List;)V

    return-void
.end method

.method private buildInvitationList()V
    .locals 8

    .prologue
    .line 364
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->invitationList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 366
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->invitationClubList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 367
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 368
    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Invited:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->getTargetRole(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;

    move-result-object v1

    .line 369
    .local v1, "invitationItem":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;
    if-eqz v1, :cond_0

    .line 370
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->invitationList:Ljava/util/List;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->userDataMap:Ljava/util/Map;

    .line 372
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;->actorXuid()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->userDataMap:Ljava/util/Map;

    .line 373
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->ownerXuid()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 370
    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;->with(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 377
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .end local v1    # "invitationItem":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;
    :cond_1
    return-void
.end method

.method private cancelTasks()V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->loadUserClubsAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$LoadUserClubsAsyncTask;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->loadUserClubsAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$LoadUserClubsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$LoadUserClubsAsyncTask;->cancel()V

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    if-eqz v0, :cond_1

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->cancel()V

    .line 121
    :cond_1
    return-void
.end method

.method private initializeAdapter()V
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->getParent()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v0

    if-nez v0, :cond_0

    .line 95
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubInvitationsScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 97
    :cond_0
    return-void
.end method

.method static synthetic lambda$ignoreAllClicked$0(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;)V
    .locals 8
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    .prologue
    .line 161
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v3, "Declining all invitations"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->isProcessingInvitations:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 166
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->invitationClubList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 167
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->ignoreAllInvitationSet:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 170
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 171
    .local v1, "profileXuid":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->invitationClubList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 172
    .restart local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    sget-object v3, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->getClubModel(J)Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    move-result-object v3

    new-instance v4, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$IgnoreInviteCallback;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v6

    const/4 v5, 0x0

    invoke-direct {v4, p0, v6, v7, v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$IgnoreInviteCallback;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;JLcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$1;)V

    invoke-virtual {v3, v1, v4}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->ignoreInvites(Ljava/util/List;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    goto :goto_1

    .line 175
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->updateAdapter()V

    .line 176
    return-void
.end method

.method static synthetic lambda$onLoadUserClubsCompleted$1(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z
    .locals 1
    .param p0, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .prologue
    .line 387
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Invited:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->hasTargetRole(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;)Z

    move-result v0

    return v0
.end method

.method private onAcceptInviteFailure(Ljava/util/List;J)V
    .locals 12
    .param p2, "xuid"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .local p1, "failedMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 305
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 306
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    .line 307
    .local v7, "clubMember":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    invoke-virtual {v7}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->getError()Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    move-result-object v8

    .line 308
    .local v8, "error":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;
    if-eqz v8, :cond_3

    :goto_1
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 310
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->isProcessingInvitations:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 312
    if-eqz v8, :cond_4

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;->code()I

    move-result v0

    const/16 v1, 0x3ec

    if-ne v0, v1, :cond_4

    .line 313
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->invitationList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;

    .line 314
    .local v9, "item":Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;
    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;->getClubId()J

    move-result-wide v2

    cmp-long v1, v2, p2

    if-nez v1, :cond_0

    .line 315
    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 317
    .local v10, "res":Landroid/content/res/Resources;
    const v0, 0x7f070158

    .line 318
    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f07015d

    .line 319
    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v0, 0x7f070738

    .line 320
    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    const-string v5, ""

    sget-object v6, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    move-object v0, p0

    .line 317
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 330
    .end local v9    # "item":Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;
    .end local v10    # "res":Landroid/content/res/Resources;
    :cond_1
    :goto_2
    return-void

    .end local v7    # "clubMember":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .end local v8    # "error":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;
    :cond_2
    move v0, v2

    .line 305
    goto :goto_0

    .restart local v7    # "clubMember":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .restart local v8    # "error":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;
    :cond_3
    move v1, v2

    .line 308
    goto :goto_1

    .line 328
    :cond_4
    const v0, 0x7f070052

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->showError(I)V

    goto :goto_2
.end method

.method private onAcceptInviteSuccess(JLjava/util/List;)V
    .locals 9
    .param p1, "xuid"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "successfulMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 226
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->removeInvitation(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v7

    .line 228
    .local v7, "acceptedClub":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->isProcessingInvitations:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 230
    invoke-static {v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 233
    .local v8, "res":Landroid/content/res/Resources;
    const v0, 0x7f070156

    new-array v1, v3, [Ljava/lang/Object;

    .line 234
    invoke-virtual {v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v8, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f070155

    new-array v3, v3, [Ljava/lang/Object;

    .line 235
    invoke-interface {p3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->memberQuotaRemaining()Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-virtual {v8, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const v0, 0x7f070738

    .line 236
    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    const-string v5, ""

    sget-object v6, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    move-object v0, p0

    .line 233
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 242
    .end local v8    # "res":Landroid/content/res/Resources;
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->updateAdapter()V

    .line 243
    return-void
.end method

.method private onIgnoreInviteFailure(J)V
    .locals 3
    .param p1, "xuid"    # J

    .prologue
    .line 280
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->ignoreAllInvitationSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 281
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->ignoreInvitationsError:Z

    .line 282
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->processIgnoreAllResult(J)V

    .line 289
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->updateAdapter()V

    .line 290
    return-void

    .line 284
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->isProcessingInvitations:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 286
    const v0, 0x7f070052

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->showError(I)V

    goto :goto_0
.end method

.method private onIgnoreInviteSuccess(J)V
    .locals 3
    .param p1, "xuid"    # J

    .prologue
    .line 246
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->removeInvitation(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 248
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->ignoreAllInvitationSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 249
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->processIgnoreAllResult(J)V

    .line 254
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->updateAdapter()V

    .line 255
    return-void

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->isProcessingInvitations:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0
.end method

.method private onLoadUserClubsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 8
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 380
    sget-object v5, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onLoadUserClubsCompleted "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->invitationClubList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 383
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 385
    .local v4, "xuidList":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/ProfileModel;->getClubs()Ljava/util/List;

    move-result-object v3

    .line 386
    .local v3, "userClubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 387
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->invitationClubList:Ljava/util/List;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$$Lambda$3;->lambdaFactory$()Lcom/microsoft/xbox/toolkit/java8/Predicate;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/microsoft/xbox/toolkit/ListUtil;->filter(Ljava/util/List;Lcom/microsoft/xbox/toolkit/java8/Predicate;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 391
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->invitationClubList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 392
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    sget-object v6, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Invited:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-virtual {v0, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->getTargetRole(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;

    move-result-object v1

    .line 393
    .local v1, "invitationItem":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;
    if-eqz v1, :cond_0

    .line 394
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;->actorXuid()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 395
    .local v2, "otherXuid":Ljava/lang/String;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->userDataMap:Ljava/util/Map;

    invoke-interface {v6, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 396
    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 399
    :cond_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->ownerXuid()Ljava/lang/String;

    move-result-object v2

    .line 400
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->userDataMap:Ljava/util/Map;

    invoke-interface {v6, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 401
    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 407
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .end local v1    # "invitationItem":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;
    .end local v2    # "otherXuid":Ljava/lang/String;
    :cond_2
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 408
    new-instance v5, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->getPersonSummariesResultAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-direct {v5, v6, v7}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;-><init>(Ljava/util/Collection;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    .line 409
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->getPersonSummariesAsyncTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->load(Z)V

    .line 418
    :goto_1
    return-void

    .line 411
    :cond_3
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->buildInvitationList()V

    .line 413
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->isLoadingUserClubs:Z

    .line 415
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->updateViewModelState()Z

    .line 416
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->updateAdapter()V

    goto :goto_1
.end method

.method private onUserDataLoadCompleted(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 333
    .local p1, "userData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onUserDataLoadCompleted "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez p1, :cond_0

    const-string v1, "NULL"

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    if-eqz p1, :cond_1

    .line 336
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 337
    .local v0, "user":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->userDataMap:Ljava/util/Map;

    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 333
    .end local v0    # "user":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    .line 341
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->buildInvitationList()V

    .line 343
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->isLoadingUserClubs:Z

    .line 345
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->updateViewModelState()Z

    .line 346
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->updateAdapter()V

    .line 347
    return-void
.end method

.method private processIgnoreAllResult(J)V
    .locals 3
    .param p1, "xuid"    # J

    .prologue
    .line 293
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->ignoreAllInvitationSet:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 295
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->ignoreAllInvitationSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->isProcessingInvitations:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->ignoreInvitationsError:Z

    if-eqz v0, :cond_1

    .line 297
    const v0, 0x7f07022e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->showError(I)V

    .line 302
    :cond_0
    :goto_0
    return-void

    .line 299
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->goBack()V

    goto :goto_0
.end method

.method private removeInvitation(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .locals 7
    .param p1, "xuid"    # J

    .prologue
    .line 258
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->invitationList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 259
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;

    .line 261
    .local v2, "item":Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;->getClubId()J

    move-result-wide v4

    cmp-long v3, v4, p1

    if-nez v3, :cond_0

    .line 262
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 267
    .end local v2    # "item":Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->invitationClubList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 268
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 270
    .local v2, "item":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v4

    cmp-long v3, v4, p1

    if-nez v3, :cond_2

    .line 271
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 276
    .end local v2    # "item":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :goto_0
    return-object v2

    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private updateViewModelState()Z
    .locals 2

    .prologue
    .line 350
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 352
    .local v0, "oldState":Lcom/microsoft/xbox/toolkit/network/ListState;
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->isLoadingUserClubs:Z

    if-eqz v1, :cond_0

    .line 353
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 360
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    return v1

    .line 354
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->invitationList:Ljava/util/List;

    if-nez v1, :cond_1

    .line 355
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 357
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 360
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public acceptInvite(J)V
    .locals 7
    .param p1, "xuid"    # J

    .prologue
    const/4 v4, 0x1

    .line 183
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->isBusy()Z

    move-result v0

    if-nez v0, :cond_0

    .line 184
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToCreateJoinParticipateClubs()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 185
    invoke-static {p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackDiscoverAcceptInvitation(J)V

    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->isProcessingInvitations:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 189
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->getClubModel(J)Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$AcceptInviteCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, p2, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$AcceptInviteCallback;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;JLcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$1;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->acceptInvitation(Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 191
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->updateAdapter()V

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 193
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0704a4

    .line 194
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0704a3

    .line 196
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070494

    .line 197
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 195
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0707c7

    .line 198
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    .line 193
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public getInviteList()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/ClubInviteListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->invitationList:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public goToClub(J)V
    .locals 1
    .param p1, "xuid"    # J

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->isBusy()Z

    move-result v0

    if-nez v0, :cond_0

    .line 220
    const-string v0, "Clubs - Discover Navigate to Invited Club"

    invoke-static {v0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackDiscoverNavigateToClub(Ljava/lang/String;J)V

    .line 221
    invoke-static {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToClub(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;J)V

    .line 223
    :cond_0
    return-void
.end method

.method public ignoreAllClicked()V
    .locals 7

    .prologue
    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->isBusy()Z

    move-result v0

    if-nez v0, :cond_0

    .line 157
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f0701d5

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0701d6

    .line 158
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 159
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v3, 0x7f0707c7

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v4

    .line 177
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v5, 0x7f070736

    invoke-virtual {v0, v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    move-object v0, p0

    .line 156
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 180
    :cond_0
    return-void
.end method

.method public ignoreInvite(J)V
    .locals 5
    .param p1, "xuid"    # J

    .prologue
    .line 205
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->isBusy()Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    .line 207
    invoke-static {p1, p2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackDiscoverIgnoreInvitation(J)V

    .line 209
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->isProcessingInvitations:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 211
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->getClubModel(J)Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$IgnoreInviteCallback;

    const/4 v3, 0x0

    invoke-direct {v2, p0, p1, p2, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$IgnoreInviteCallback;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;JLcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$1;)V

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->ignoreInvites(Ljava/util/List;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 213
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->updateAdapter()V

    .line 216
    :cond_0
    return-void
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->isLoadingUserClubs:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->isProcessingInvitations:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 130
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-nez v0, :cond_1

    .line 131
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 134
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->cancelTasks()V

    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_2

    .line 137
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$LoadUserClubsAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$LoadUserClubsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->loadUserClubsAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$LoadUserClubsAsyncTask;

    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->loadUserClubsAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$LoadUserClubsAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel$LoadUserClubsAsyncTask;->load(Z)V

    .line 141
    :cond_2
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->updateViewModelState()Z

    .line 142
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->updateAdapter()V

    .line 143
    return-void
.end method

.method public onRehydrate()V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->initializeAdapter()V

    .line 106
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 101
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->cancelTasks()V

    .line 111
    return-void
.end method
