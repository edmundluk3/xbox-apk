.class Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$8;
.super Ljava/lang/Object;
.source "EnterIPAddressDialog.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;

    .prologue
    .line 331
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$8;->this$0:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 4
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    .line 334
    new-instance v1, Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-interface {p4, v2, p5}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 335
    .local v1, "newIpAddr":Ljava/lang/StringBuilder;
    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 336
    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v2

    invoke-interface {p4, p6, v2}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 337
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$8;->this$0:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;)Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$8;->this$0:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;

    invoke-static {v3, v1}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->access$500(Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;Ljava/lang/CharSequence;)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/ui/LeadingIconTextView;->setEnabled(Z)V

    .line 338
    invoke-static {}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->access$600()Ljava/util/regex/Pattern;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    .line 339
    .local v0, "matches":Z
    if-eqz v0, :cond_0

    .line 340
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog$8;->this$0:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->access$400(Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;)Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/microsoft/xbox/xle/model/ConsoleData;->host:Ljava/lang/String;

    .line 342
    :cond_0
    if-eqz v0, :cond_1

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_1
    const-string v2, ""

    goto :goto_0
.end method
