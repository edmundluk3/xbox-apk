.class Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "PopularWithFriendsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadPopularGamesAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final MAX_ENTRIES:I = 0x32


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$1;

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;)V

    return-void
.end method

.method private processData()V
    .locals 32

    .prologue
    .line 171
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-static/range {v25 .. v26}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->access$202(Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 172
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v12

    .line 173
    .local v12, "meModel":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v12, :cond_13

    .line 174
    invoke-virtual {v12}, Lcom/microsoft/xbox/service/model/ProfileModel;->getFollowingData()Ljava/util/ArrayList;

    move-result-object v7

    .line 176
    .local v7, "friends":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    if-eqz v7, :cond_13

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v25

    if-lez v25, :cond_13

    .line 178
    new-instance v24, Ljava/util/Hashtable;

    invoke-direct/range {v24 .. v24}, Ljava/util/Hashtable;-><init>()V

    .line 179
    .local v24, "xuidToGamerTagLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v23, Ljava/util/Hashtable;

    invoke-direct/range {v23 .. v23}, Ljava/util/Hashtable;-><init>()V

    .line 180
    .local v23, "xuidToGameNowPlayingLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Landroid/util/Pair<Ljava/lang/Long;Ljava/util/Date;>;>;"
    new-instance v17, Ljava/util/Hashtable;

    invoke-direct/range {v17 .. v17}, Ljava/util/Hashtable;-><init>()V

    .line 181
    .local v17, "nowPlayingGamesLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Ljava/lang/Boolean;>;"
    new-instance v6, Ljava/util/Hashtable;

    invoke-direct {v6}, Ljava/util/Hashtable;-><init>()V

    .line 183
    .local v6, "favoriteFriendsLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :goto_0
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-eqz v26, :cond_3

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 184
    .local v4, "f":Lcom/microsoft/xbox/service/model/FollowersData;
    iget-object v0, v4, Lcom/microsoft/xbox/service/model/FollowersData;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    move-object/from16 v26, v0

    sget-object v27, Lcom/microsoft/xbox/service/model/UserStatus;->Online:Lcom/microsoft/xbox/service/model/UserStatus;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-ne v0, v1, :cond_1

    iget-wide v0, v4, Lcom/microsoft/xbox/service/model/FollowersData;->titleId:J

    move-wide/from16 v26, v0

    const-wide/16 v28, -0x1

    cmp-long v26, v26, v28

    if-eqz v26, :cond_1

    .line 185
    iget-wide v0, v4, Lcom/microsoft/xbox/service/model/FollowersData;->titleId:J

    move-wide/from16 v26, v0

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v26

    if-nez v26, :cond_0

    .line 186
    iget-wide v0, v4, Lcom/microsoft/xbox/service/model/FollowersData;->titleId:J

    move-wide/from16 v26, v0

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    const/16 v27, 0x1

    invoke-static/range {v27 .. v27}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v27

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    :cond_0
    iget-object v0, v4, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    move-object/from16 v26, v0

    new-instance v27, Landroid/util/Pair;

    iget-wide v0, v4, Lcom/microsoft/xbox/service/model/FollowersData;->titleId:J

    move-wide/from16 v28, v0

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v28

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/FollowersData;->getTimeStamp()Ljava/util/Date;

    move-result-object v29

    invoke-direct/range {v27 .. v29}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    :cond_1
    iget-boolean v0, v4, Lcom/microsoft/xbox/service/model/FollowersData;->isFavorite:Z

    move/from16 v26, v0

    if-eqz v26, :cond_2

    .line 191
    iget-object v0, v4, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-static/range {v27 .. v27}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v6, v0, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    :cond_2
    iget-object v0, v4, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamertag()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 196
    .end local v4    # "f":Lcom/microsoft/xbox/service/model/FollowersData;
    :cond_3
    invoke-virtual {v12}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPopularGamesWithFriends()Ljava/util/ArrayList;

    move-result-object v11

    .line 197
    .local v11, "games":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/PopularGame;>;"
    if-eqz v11, :cond_13

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v25

    if-lez v25, :cond_13

    .line 198
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 199
    .local v16, "nowPlayingGames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/PopularGame;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 201
    .local v15, "nonNowPlayingGames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/PopularGame;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 202
    .local v5, "favoriteFriends":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/lang/String;Ljava/util/Date;>;>;"
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 204
    .local v14, "nonFavoriteFriends":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/lang/String;Ljava/util/Date;>;>;"
    new-instance v20, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask$1;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;)V

    .line 211
    .local v20, "sortByDateComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Landroid/util/Pair<Ljava/lang/String;Ljava/util/Date;>;>;"
    new-instance v9, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask$2;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask$2;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;)V

    .line 229
    .local v9, "gameComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/microsoft/xbox/service/network/managers/PopularGame;>;"
    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v26

    :cond_4
    :goto_1
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_10

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/service/network/managers/PopularGame;

    .line 230
    .local v8, "g":Lcom/microsoft/xbox/service/network/managers/PopularGame;
    iget-wide v0, v8, Lcom/microsoft/xbox/service/network/managers/PopularGame;->titleId:J

    move-wide/from16 v28, v0

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    move-object/from16 v0, v17

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_d

    .line 231
    iget-object v0, v8, Lcom/microsoft/xbox/service/network/managers/PopularGame;->xuids:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    if-eqz v25, :cond_c

    iget-object v0, v8, Lcom/microsoft/xbox/service/network/managers/PopularGame;->xuids:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v25

    if-lez v25, :cond_c

    .line 233
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 234
    invoke-virtual {v14}, Ljava/util/ArrayList;->clear()V

    .line 236
    iget-object v0, v8, Lcom/microsoft/xbox/service/network/managers/PopularGame;->xuids:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :cond_5
    :goto_2
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_7

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/String;

    .line 237
    .local v22, "xuid":Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeXuid(Ljava/lang/String;)Z

    move-result v25

    if-nez v25, :cond_5

    .line 239
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/util/Pair;

    .line 240
    .local v10, "gameInfo":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/util/Date;>;"
    if-eqz v10, :cond_5

    iget-object v0, v10, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v25, v0

    check-cast v25, Ljava/lang/Long;

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Long;->longValue()J

    move-result-wide v28

    iget-wide v0, v8, Lcom/microsoft/xbox/service/network/managers/PopularGame;->titleId:J

    move-wide/from16 v30, v0

    cmp-long v25, v28, v30

    if-nez v25, :cond_5

    .line 241
    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_6

    .line 242
    new-instance v25, Landroid/util/Pair;

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v28

    iget-object v0, v10, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v29, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 244
    :cond_6
    new-instance v25, Landroid/util/Pair;

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v28

    iget-object v0, v10, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v29, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 251
    .end local v10    # "gameInfo":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/util/Date;>;"
    .end local v22    # "xuid":Ljava/lang/String;
    :cond_7
    move-object/from16 v0, v20

    invoke-static {v5, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 252
    move-object/from16 v0, v20

    invoke-static {v14, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 255
    const/4 v13, 0x0

    .line 256
    .local v13, "mostRecentPlayer":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/Date;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v25

    if-lez v25, :cond_a

    .line 257
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    .end local v13    # "mostRecentPlayer":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/Date;>;"
    check-cast v13, Landroid/util/Pair;

    .line 261
    .restart local v13    # "mostRecentPlayer":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/Date;>;"
    :cond_8
    :goto_3
    if-eqz v13, :cond_9

    .line 262
    iget-object v0, v13, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v25, v0

    check-cast v25, Ljava/util/Date;

    move-object/from16 v0, v25

    iput-object v0, v8, Lcom/microsoft/xbox/service/network/managers/PopularGame;->date:Ljava/util/Date;

    .line 266
    :cond_9
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v25

    iput-object v0, v8, Lcom/microsoft/xbox/service/network/managers/PopularGame;->friendsPlayed:Ljava/util/ArrayList;

    .line 267
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :goto_4
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_b

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/util/Pair;

    .line 268
    .local v19, "p":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/Date;>;"
    iget-object v0, v8, Lcom/microsoft/xbox/service/network/managers/PopularGame;->friendsPlayed:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 258
    .end local v19    # "p":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/Date;>;"
    :cond_a
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v25

    if-lez v25, :cond_8

    .line 259
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    .end local v13    # "mostRecentPlayer":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/Date;>;"
    check-cast v13, Landroid/util/Pair;

    .restart local v13    # "mostRecentPlayer":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/Date;>;"
    goto :goto_3

    .line 270
    :cond_b
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :goto_5
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_c

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/util/Pair;

    .line 271
    .restart local v19    # "p":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/Date;>;"
    iget-object v0, v8, Lcom/microsoft/xbox/service/network/managers/PopularGame;->friendsPlayed:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 274
    .end local v13    # "mostRecentPlayer":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/Date;>;"
    .end local v19    # "p":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/Date;>;"
    :cond_c
    iget-object v0, v8, Lcom/microsoft/xbox/service/network/managers/PopularGame;->friendsPlayed:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    if-eqz v25, :cond_4

    iget-object v0, v8, Lcom/microsoft/xbox/service/network/managers/PopularGame;->friendsPlayed:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v25

    if-lez v25, :cond_4

    .line 275
    const/16 v25, 0x1

    move/from16 v0, v25

    invoke-virtual {v8, v0}, Lcom/microsoft/xbox/service/network/managers/PopularGame;->setIsNowPlaying(Z)V

    .line 276
    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 279
    :cond_d
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v25

    iput-object v0, v8, Lcom/microsoft/xbox/service/network/managers/PopularGame;->friendsPlayed:Ljava/util/ArrayList;

    .line 280
    iget-object v0, v8, Lcom/microsoft/xbox/service/network/managers/PopularGame;->xuids:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :cond_e
    :goto_6
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_f

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/String;

    .line 281
    .restart local v22    # "xuid":Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeXuid(Ljava/lang/String;)Z

    move-result v27

    if-nez v27, :cond_e

    .line 282
    iget-object v0, v8, Lcom/microsoft/xbox/service/network/managers/PopularGame;->friendsPlayed:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 285
    .end local v22    # "xuid":Ljava/lang/String;
    :cond_f
    iget-object v0, v8, Lcom/microsoft/xbox/service/network/managers/PopularGame;->friendsPlayed:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    if-eqz v25, :cond_4

    iget-object v0, v8, Lcom/microsoft/xbox/service/network/managers/PopularGame;->friendsPlayed:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v25

    if-lez v25, :cond_4

    .line 286
    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 291
    .end local v8    # "g":Lcom/microsoft/xbox/service/network/managers/PopularGame;
    :cond_10
    move-object/from16 v0, v16

    invoke-static {v0, v9}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 292
    invoke-static {v15, v9}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 294
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;

    move-object/from16 v25, v0

    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    invoke-static/range {v25 .. v26}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->access$202(Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 295
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :goto_7
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-eqz v26, :cond_11

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/service/network/managers/PopularGame;

    .line 296
    .restart local v8    # "g":Lcom/microsoft/xbox/service/network/managers/PopularGame;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;)Ljava/util/ArrayList;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 298
    .end local v8    # "g":Lcom/microsoft/xbox/service/network/managers/PopularGame;
    :cond_11
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v25

    :goto_8
    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-eqz v26, :cond_12

    invoke-interface/range {v25 .. v25}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/service/network/managers/PopularGame;

    .line 299
    .restart local v8    # "g":Lcom/microsoft/xbox/service/network/managers/PopularGame;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;)Ljava/util/ArrayList;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 302
    .end local v8    # "g":Lcom/microsoft/xbox/service/network/managers/PopularGame;
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;)Ljava/util/ArrayList;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v18

    .line 303
    .local v18, "originalSize":I
    const/16 v25, 0x32

    move/from16 v0, v18

    move/from16 v1, v25

    if-le v0, v1, :cond_13

    .line 304
    add-int/lit8 v21, v18, -0x32

    .local v21, "trimCount":I
    :goto_9
    if-lez v21, :cond_13

    .line 305
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;)Ljava/util/ArrayList;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;)Ljava/util/ArrayList;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v26

    add-int/lit8 v26, v26, -0x1

    invoke-virtual/range {v25 .. v26}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 304
    add-int/lit8 v21, v21, -0x1

    goto :goto_9

    .line 311
    .end local v5    # "favoriteFriends":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/lang/String;Ljava/util/Date;>;>;"
    .end local v6    # "favoriteFriendsLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/Boolean;>;"
    .end local v7    # "friends":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    .end local v9    # "gameComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/microsoft/xbox/service/network/managers/PopularGame;>;"
    .end local v11    # "games":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/PopularGame;>;"
    .end local v14    # "nonFavoriteFriends":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/lang/String;Ljava/util/Date;>;>;"
    .end local v15    # "nonNowPlayingGames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/PopularGame;>;"
    .end local v16    # "nowPlayingGames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/PopularGame;>;"
    .end local v17    # "nowPlayingGamesLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Ljava/lang/Boolean;>;"
    .end local v18    # "originalSize":I
    .end local v20    # "sortByDateComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Landroid/util/Pair<Ljava/lang/String;Ljava/util/Date;>;>;"
    .end local v21    # "trimCount":I
    .end local v23    # "xuidToGameNowPlayingLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Landroid/util/Pair<Ljava/lang/Long;Ljava/util/Date;>;>;"
    .end local v24    # "xuidToGamerTagLookup":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_13
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 2

    .prologue
    .line 121
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 122
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 123
    .local v0, "meModel":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefreshPopularGamesWithFriends()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 151
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 152
    .local v0, "meModel":Lcom/microsoft/xbox/service/model/ProfileModel;
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 153
    .local v1, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    if-eqz v0, :cond_1

    .line 154
    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;->forceLoad:Z

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadPopularGamesWithFriends(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    .line 155
    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v1, v3, :cond_1

    .line 157
    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;->forceLoad:Z

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadFollowingProfile(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    .line 158
    .local v2, "statusFriends":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v2, v3, :cond_2

    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v1, v3, :cond_0

    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v2, v3, :cond_2

    .line 160
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;->processData()V

    .line 161
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 167
    .end local v2    # "statusFriends":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_1
    :goto_0
    return-object v1

    .line 163
    .restart local v2    # "statusFriends":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :cond_2
    move-object v1, v2

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 146
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 128
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->onLoadPopularGamesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 130
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->onLoadPopularGamesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 142
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 116
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 134
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 135
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->access$102(Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;Z)Z

    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel;->updateAdapter()V

    .line 137
    return-void
.end method
