.class public Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "MediaItemListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        ">",
        "Landroid/widget/ArrayAdapter",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private defaultResourceIdToIndexTable:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private resourceId:I

.field private shouldAlwaysShowTitle:Z

.field private useBlackPrimaryText:Z

.field private final yearFormatter:Ljava/text/SimpleDateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;I)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "resourceId"    # I

    .prologue
    .line 48
    .local p0, "this":Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;, "Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter<TT;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;-><init>(Landroid/app/Activity;IZ)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;IZ)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "resourceId"    # I
    .param p3, "useBlackPrimaryText"    # Z

    .prologue
    .line 52
    .local p0, "this":Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;, "Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter<TT;>;"
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->shouldAlwaysShowTitle:Z

    .line 36
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->defaultResourceIdToIndexTable:Landroid/util/SparseArray;

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->useBlackPrimaryText:Z

    .line 53
    iput p2, p0, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->resourceId:I

    .line 54
    iput-boolean p3, p0, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->useBlackPrimaryText:Z

    .line 56
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->yearFormatter:Ljava/text/SimpleDateFormat;

    .line 57
    return-void
.end method

.method public static getMediaTypeIconResourceId(I)I
    .locals 1
    .param p0, "mediaType"    # I

    .prologue
    .line 134
    sparse-switch p0, :sswitch_data_0

    .line 186
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 137
    :sswitch_0
    const v0, 0x7f070eaf

    goto :goto_0

    .line 146
    :sswitch_1
    const v0, 0x7f070f49

    goto :goto_0

    .line 152
    :sswitch_2
    const v0, 0x7f071035

    goto :goto_0

    .line 157
    :sswitch_3
    const v0, 0x7f070f93

    goto :goto_0

    .line 163
    :sswitch_4
    const v0, 0x7f070f7d

    goto :goto_0

    .line 183
    :sswitch_5
    const v0, 0x7f070f46

    goto :goto_0

    .line 134
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_5
        0x5 -> :sswitch_5
        0x12 -> :sswitch_1
        0x13 -> :sswitch_5
        0x14 -> :sswitch_5
        0x15 -> :sswitch_5
        0x16 -> :sswitch_5
        0x17 -> :sswitch_5
        0x18 -> :sswitch_1
        0x1e -> :sswitch_5
        0x22 -> :sswitch_5
        0x24 -> :sswitch_5
        0x25 -> :sswitch_5
        0x2f -> :sswitch_5
        0x39 -> :sswitch_5
        0x3a -> :sswitch_5
        0x3b -> :sswitch_5
        0x3c -> :sswitch_5
        0x3d -> :sswitch_0
        0x3e -> :sswitch_5
        0x3f -> :sswitch_1
        0x40 -> :sswitch_1
        0x3e8 -> :sswitch_3
        0x3ea -> :sswitch_2
        0x3eb -> :sswitch_2
        0x3ec -> :sswitch_2
        0x3ed -> :sswitch_2
        0x3ee -> :sswitch_4
        0x3ef -> :sswitch_4
        0x3f0 -> :sswitch_4
        0x3f1 -> :sswitch_4
        0x3f2 -> :sswitch_3
        0x3f3 -> :sswitch_3
        0x2328 -> :sswitch_0
        0x2329 -> :sswitch_5
        0x232a -> :sswitch_1
        0x232b -> :sswitch_1
        0x232c -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 192
    .local p0, "this":Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;, "Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter<TT;>;"
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v0

    .line 193
    .local v0, "resourceId":I
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->defaultResourceIdToIndexTable:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 194
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->defaultResourceIdToIndexTable:Landroid/util/SparseArray;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->defaultResourceIdToIndexTable:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 197
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->defaultResourceIdToIndexTable:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    return v1
.end method

.method public getShouldAlwaysShowTitle()Z
    .locals 1

    .prologue
    .line 44
    .local p0, "this":Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;, "Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter<TT;>;"
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->shouldAlwaysShowTitle:Z

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 15
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 62
    .local p0, "this":Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;, "Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter<TT;>;"
    move-object/from16 v10, p2

    .line 64
    .local v10, "v":Landroid/view/View;
    if-nez v10, :cond_3

    .line 65
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->getContext()Landroid/content/Context;

    move-result-object v12

    const-string v13, "layout_inflater"

    invoke-virtual {v12, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/view/LayoutInflater;

    .line 66
    .local v11, "vi":Landroid/view/LayoutInflater;
    iget v12, p0, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->resourceId:I

    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v11, v12, v0, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    .line 68
    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;

    invoke-direct {v2}, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;-><init>()V

    .line 69
    .local v2, "holder":Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;
    const v12, 0x7f0e079f

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

    iput-object v12, v2, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

    .line 70
    const v12, 0x7f0e07a0

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v12, v2, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;->primaryTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 71
    const v12, 0x7f0e07a1

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v12, v2, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;->secondaryTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 72
    const v12, 0x7f0e07a2

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    iput-object v12, v2, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;->smartGlassIcon:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 74
    iget-boolean v12, p0, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->useBlackPrimaryText:Z

    if-eqz v12, :cond_0

    .line 75
    iget-object v12, v2, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;->primaryTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v13

    const v14, 0x7f080133

    invoke-virtual {v12, v13, v14}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 78
    :cond_0
    invoke-virtual {v10, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 83
    .end local v11    # "vi":Landroid/view/LayoutInflater;
    :goto_0
    invoke-virtual/range {p0 .. p1}, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 85
    .local v4, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v4, :cond_2

    .line 87
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v12

    invoke-static {v12}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v6

    .line 89
    .local v6, "resId":I
    iget-object v12, v2, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

    instance-of v12, v12, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;

    if-eqz v12, :cond_4

    .line 90
    iget-object v3, v2, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;

    .line 91
    .local v3, "img":Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getImageUrl()Ljava/lang/String;

    move-result-object v12

    const v13, 0x7f0201fa

    invoke-virtual {v3, v12, v13}, Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;->setImageURI2(Ljava/lang/String;I)V

    .line 101
    .end local v3    # "img":Lcom/microsoft/xbox/toolkit/ui/XLEUniformImageView;
    :goto_1
    const/16 v8, 0x8

    .line 102
    .local v8, "titleVisibility":I
    iget-boolean v12, p0, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->shouldAlwaysShowTitle:Z

    if-eqz v12, :cond_1

    .line 103
    const/4 v8, 0x0

    .line 106
    :cond_1
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getReleaseDate()Ljava/util/Date;

    move-result-object v12

    iget-object v13, p0, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->yearFormatter:Ljava/text/SimpleDateFormat;

    invoke-static {v12, v13}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getYearString(Ljava/util/Date;Ljava/text/SimpleDateFormat;)Ljava/lang/String;

    move-result-object v5

    .line 107
    .local v5, "releaseYear":Ljava/lang/String;
    const-string v7, ""

    .line 108
    .local v7, "secondLine":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v12

    packed-switch v12, :pswitch_data_0

    .line 123
    :goto_2
    iget-object v12, v2, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;->primaryTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitle()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v12, v2, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;->primaryTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v12, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 125
    iget-object v13, v2, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;->smartGlassIcon:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getHasSmartGlassActivity()Z

    move-result v12

    if-eqz v12, :cond_6

    const/4 v12, 0x0

    :goto_3
    invoke-virtual {v13, v12}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setVisibility(I)V

    .line 126
    iget-object v12, v2, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;->secondaryTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_7

    .end local v5    # "releaseYear":Ljava/lang/String;
    :goto_4
    invoke-static {v12, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfTextNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 129
    .end local v6    # "resId":I
    .end local v7    # "secondLine":Ljava/lang/String;
    .end local v8    # "titleVisibility":I
    :cond_2
    return-object v10

    .line 80
    .end local v2    # "holder":Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;
    .end local v4    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :cond_3
    invoke-virtual {v10}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;

    .restart local v2    # "holder":Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;
    goto :goto_0

    .line 92
    .restart local v4    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .restart local v6    # "resId":I
    :cond_4
    iget-object v12, v2, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

    instance-of v12, v12, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;

    if-eqz v12, :cond_5

    .line 93
    iget-object v3, v2, Lcom/microsoft/xbox/xle/app/adapter/SearchResultRowViewHolder;->imageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageView;

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;

    .line 94
    .local v3, "img":Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getImageUrl()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->setImageURI2(Ljava/lang/String;I)V

    .line 95
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v12

    invoke-static {v12}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultAspectXY(I)[I

    move-result-object v12

    invoke-virtual {v3, v12}, Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;->setAspectXY([I)V

    goto :goto_1

    .line 97
    .end local v3    # "img":Lcom/microsoft/xbox/toolkit/ui/XLEConstrainedImageView;
    :cond_5
    sget-object v12, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->TAG:Ljava/lang/String;

    const-string v13, "Unknown image type"

    invoke-static {v12, v13}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .restart local v5    # "releaseYear":Ljava/lang/String;
    .restart local v7    # "secondLine":Ljava/lang/String;
    .restart local v8    # "titleVisibility":I
    :pswitch_0
    move-object v9, v4

    .line 110
    check-cast v9, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    .line 111
    .local v9, "trackItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;
    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->getArtistName()Ljava/lang/String;

    move-result-object v7

    .line 112
    goto :goto_2

    .end local v9    # "trackItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;
    :pswitch_1
    move-object v1, v4

    .line 115
    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    .line 116
    .local v1, "albumItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;->getArtistName()Ljava/lang/String;

    move-result-object v7

    .line 117
    goto :goto_2

    .line 125
    .end local v1    # "albumItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;
    :cond_6
    const/16 v12, 0x8

    goto :goto_3

    .line 126
    :cond_7
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_4

    .line 108
    :pswitch_data_0
    .packed-switch 0x3ee
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 202
    .local p0, "this":Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;, "Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter<TT;>;"
    const/4 v0, 0x6

    return v0
.end method

.method public setShouldAlwaysShowTitle(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 40
    .local p0, "this":Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;, "Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter<TT;>;"
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/adapter/MediaItemListAdapter;->shouldAlwaysShowTitle:Z

    .line 41
    return-void
.end method
