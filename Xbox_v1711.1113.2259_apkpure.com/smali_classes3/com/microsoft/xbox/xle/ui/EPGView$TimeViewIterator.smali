.class Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;
.super Ljava/lang/Object;
.source "EPGView.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$IAdapter$IViewIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/EPGView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TimeViewIterator"
.end annotation


# instance fields
.field private mIncrement:I

.field private mInterval:I

.field private final mTempDate:Ljava/util/Date;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/EPGView;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/ui/EPGView;)V
    .locals 1

    .prologue
    .line 973
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 977
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;->mTempDate:Ljava/util/Date;

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/ui/EPGView;Lcom/microsoft/xbox/xle/ui/EPGView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/ui/EPGView;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/ui/EPGView$1;

    .prologue
    .line 973
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;-><init>(Lcom/microsoft/xbox/xle/ui/EPGView;)V

    return-void
.end method


# virtual methods
.method public getNext(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 993
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;->mTempDate:Ljava/util/Date;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/EPGView;->access$1200(Lcom/microsoft/xbox/xle/ui/EPGView;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iget v4, p0, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;->mInterval:I

    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/ui/EPGView;->access$1300(Lcom/microsoft/xbox/xle/ui/EPGView;)I

    move-result v5

    mul-int/2addr v4, v5

    mul-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    add-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/util/Date;->setTime(J)V

    .line 996
    invoke-static {p1}, Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;->getInstance(Landroid/view/ViewGroup;)Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;

    move-result-object v0

    .line 997
    .local v0, "view":Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;->mTempDate:Ljava/util/Date;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGHeaderTimeView;->setTime(Ljava/util/Date;)V

    .line 1000
    iget v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;->mInterval:I

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/EPGView;->access$1100(Lcom/microsoft/xbox/xle/ui/EPGView;)I

    move-result v2

    mul-int/2addr v1, v2

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/EPGView;->access$1100(Lcom/microsoft/xbox/xle/ui/EPGView;)I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setChildViewLayoutParams(Landroid/view/View;IIZ)Landroid/view/View;

    .line 1003
    iget v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;->mInterval:I

    iget v2, p0, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;->mIncrement:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;->mInterval:I

    .line 1005
    return-object v0
.end method

.method public reset(IZ)V
    .locals 2
    .param p1, "pos"    # I
    .param p2, "forward"    # Z

    .prologue
    .line 981
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;->this$0:Lcom/microsoft/xbox/xle/ui/EPGView;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGView;->access$1100(Lcom/microsoft/xbox/xle/ui/EPGView;)I

    move-result v0

    div-int v0, p1, v0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;->mInterval:I

    .line 982
    const/4 v0, 0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;->mIncrement:I

    .line 983
    if-nez p2, :cond_0

    .line 984
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;->mIncrement:I

    neg-int v0, v0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;->mIncrement:I

    .line 985
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;->mInterval:I

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;->mIncrement:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/EPGView$TimeViewIterator;->mInterval:I

    .line 987
    :cond_0
    return-void
.end method
