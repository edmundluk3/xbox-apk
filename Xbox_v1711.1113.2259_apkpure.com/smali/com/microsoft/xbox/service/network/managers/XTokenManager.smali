.class public Lcom/microsoft/xbox/service/network/managers/XTokenManager;
.super Ljava/lang/Object;
.source "XTokenManager.java"


# static fields
.field private static instance:Lcom/microsoft/xbox/service/network/managers/XTokenManager;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    return-void
.end method

.method public static clearAllTokens()V
    .locals 1

    .prologue
    .line 39
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/XTokenManager;

    move-result-object v0

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->getManager()Lcom/microsoft/xbox/service/network/managers/IXTokenManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/microsoft/xbox/service/network/managers/IXTokenManager;->clearAllTokens()V

    .line 40
    return-void
.end method

.method public static getAgeGroup(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "auidence"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 31
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/XTokenManager;

    move-result-object v0

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->getManager()Lcom/microsoft/xbox/service/network/managers/IXTokenManager;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/service/network/managers/IXTokenManager;->getAgeGroup(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getInstance()Lcom/microsoft/xbox/service/network/managers/XTokenManager;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->instance:Lcom/microsoft/xbox/service/network/managers/XTokenManager;

    if-nez v0, :cond_0

    .line 16
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/XTokenManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/XTokenManager;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->instance:Lcom/microsoft/xbox/service/network/managers/XTokenManager;

    .line 18
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->instance:Lcom/microsoft/xbox/service/network/managers/XTokenManager;

    return-object v0
.end method

.method public static getLastXTokenString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "audience"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 26
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/XTokenManager;

    move-result-object v0

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->getManager()Lcom/microsoft/xbox/service/network/managers/IXTokenManager;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/service/network/managers/IXTokenManager;->getLastXTokenString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getManager()Lcom/microsoft/xbox/service/network/managers/IXTokenManager;
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/XsapiTokenManager;

    move-result-object v0

    return-object v0
.end method

.method public static getPrivilegesFromToken(Ljava/lang/String;)[I
    .locals 1
    .param p0, "audience"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 47
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/XTokenManager;

    move-result-object v0

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->getManager()Lcom/microsoft/xbox/service/network/managers/IXTokenManager;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/service/network/managers/IXTokenManager;->getPrivilegesFromToken(Ljava/lang/String;)[I

    move-result-object v0

    return-object v0
.end method

.method public static getXTokenString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "audience"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 22
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/XTokenManager;

    move-result-object v0

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->getManager()Lcom/microsoft/xbox/service/network/managers/IXTokenManager;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/service/network/managers/IXTokenManager;->getXTokenString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getXuidFromToken(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "audience"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 43
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/XTokenManager;

    move-result-object v0

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->getManager()Lcom/microsoft/xbox/service/network/managers/IXTokenManager;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/service/network/managers/IXTokenManager;->getXuidFromToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static reset()V
    .locals 1

    .prologue
    .line 35
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/XTokenManager;

    move-result-object v0

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->getManager()Lcom/microsoft/xbox/service/network/managers/IXTokenManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/microsoft/xbox/service/network/managers/IXTokenManager;->reset()V

    .line 36
    return-void
.end method

.method public static setReplayMode(Z)V
    .locals 0
    .param p0, "replayMode"    # Z

    .prologue
    .line 51
    return-void
.end method
