.class public Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;
.super Ljava/lang/Object;
.source "HoverChatViewAdapter.java"

# interfaces
.implements Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/flipkart/chatheads/ui/ChatHeadViewAdapter",
        "<",
        "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private activeScreen:Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

.field private final circularDrawableFactory:Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;

.field private final context:Landroid/content/Context;

.field private final repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

.field private final viewCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            "Lcom/microsoft/xbox/xle/app/activity/ActivityBase;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "circularDrawableFactory"    # Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;
    .param p3, "repository"    # Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->context:Landroid/content/Context;

    .line 43
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->circularDrawableFactory:Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;

    .line 44
    iput-object p3, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    .line 45
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->viewCache:Ljava/util/Map;

    .line 46
    return-void
.end method

.method private getClubScreenForKey(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;)Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
    .locals 4
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    .prologue
    .line 94
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;

    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;->clubId()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;-><init>(J)V

    .line 95
    .local v0, "parameters":Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;
    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreen;

    invoke-direct {v1, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreen;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;)V

    return-object v1
.end method

.method private getMenuScreen()Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;

    invoke-direct {v0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatMenuScreen;-><init>()V

    return-object v0
.end method

.method private getMessageScreenForKey(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;)Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
    .locals 2
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    .prologue
    .line 99
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 100
    .local v0, "parameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->setForceReload(Z)V

    .line 101
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;->conversationId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSenderXuid(Ljava/lang/String;)V

    .line 102
    new-instance v1, Lcom/microsoft/xbox/xle/app/activity/ConversationsDetailsActivity;

    invoke-direct {v1, v0}, Lcom/microsoft/xbox/xle/app/activity/ConversationsDetailsActivity;-><init>(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    return-object v1
.end method

.method private setupClubChatScreen(Lcom/microsoft/xbox/xle/app/activity/ActivityBase;)V
    .locals 2
    .param p1, "screen"    # Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    .prologue
    .line 121
    const v0, 0x7f0e02f7

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0201d3

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 122
    const v0, 0x7f0e02f6

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0201d1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 123
    return-void
.end method

.method private setupMessageScreen(Lcom/microsoft/xbox/xle/app/activity/ActivityBase;)V
    .locals 2
    .param p1, "screen"    # Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    .prologue
    .line 126
    const v0, 0x7f0e0463

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0201d1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 127
    const v0, 0x7f0e0461

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0201d3

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 128
    const v0, 0x7f0e0476

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 129
    return-void
.end method

.method private setupScreen(Lcom/microsoft/xbox/xle/app/activity/ActivityBase;)V
    .locals 1
    .param p1, "screen"    # Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    .prologue
    .line 110
    const v0, 0x7f0201d2

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->setBackgroundResource(I)V

    .line 111
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->setClickable(Z)V

    .line 113
    instance-of v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreen;

    if-eqz v0, :cond_1

    .line 114
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->setupClubChatScreen(Lcom/microsoft/xbox/xle/app/activity/ActivityBase;)V

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    instance-of v0, p1, Lcom/microsoft/xbox/xle/app/activity/ConversationsDetailsActivity;

    if-eqz v0, :cond_0

    .line 116
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->setupMessageScreen(Lcom/microsoft/xbox/xle/app/activity/ActivityBase;)V

    goto :goto_0
.end method


# virtual methods
.method public attachView(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<+",
            "Ljava/io/Serializable;",
            ">;",
            "Landroid/view/ViewGroup;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .local p2, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<+Ljava/io/Serializable;>;"
    const/4 v2, 0x0

    .line 50
    sget-object v1, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "attachView: key: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->viewCache:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    .line 54
    .local v0, "cachedScreen":Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
    if-nez v0, :cond_0

    .line 55
    instance-of v1, p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MenuKey;

    if-eqz v1, :cond_2

    .line 56
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->getMenuScreen()Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    move-result-object v0

    .line 66
    :goto_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 67
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onStart()V

    .line 68
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onResume()V

    .line 70
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->setupScreen(Lcom/microsoft/xbox/xle/app/activity/ActivityBase;)V

    .line 71
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->viewCache:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onSetActive()V

    .line 75
    invoke-virtual {p3, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 77
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->activeScreen:Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    if-eq v0, v1, :cond_1

    .line 78
    iput-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->activeScreen:Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    .line 79
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v1, p1}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->onChatHeadAttached(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)V

    .line 84
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedConversationSummary(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    move-object v1, v0

    .line 86
    :goto_1
    return-object v1

    .line 57
    :cond_2
    instance-of v1, p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    if-eqz v1, :cond_3

    move-object v1, p1

    .line 58
    check-cast v1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->getClubScreenForKey(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;)Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    move-result-object v0

    goto :goto_0

    .line 59
    :cond_3
    instance-of v1, p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    if-eqz v1, :cond_4

    move-object v1, p1

    .line 60
    check-cast v1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->getMessageScreenForKey(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;)Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    move-result-object v0

    goto :goto_0

    .line 62
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected key: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    move-object v1, v2

    .line 63
    goto :goto_1
.end method

.method public bridge synthetic attachView(Ljava/lang/Object;Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 30
    check-cast p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;

    invoke-virtual {p0, p1, p2, p3}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->attachView(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public detachView(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)V
    .locals 4
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<+",
            "Ljava/io/Serializable;",
            ">;",
            "Landroid/view/ViewGroup;",
            ")V"
        }
    .end annotation

    .prologue
    .line 133
    .local p2, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<+Ljava/io/Serializable;>;"
    sget-object v1, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "detachView: key: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->viewCache:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    .line 137
    .local v0, "cachedScreen":Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
    if-eqz v0, :cond_0

    .line 138
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onSetInactive()V

    .line 139
    invoke-virtual {p3, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 142
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->activeScreen:Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    if-ne v0, v1, :cond_1

    .line 143
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->activeScreen:Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    .line 146
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v1, p1}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->onChatHeadDetached(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)V

    .line 147
    return-void
.end method

.method public bridge synthetic detachView(Ljava/lang/Object;Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;

    invoke-virtual {p0, p1, p2, p3}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->detachView(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)V

    return-void
.end method

.method public getChatHeadDrawable(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;

    .prologue
    const v1, 0x7f020125

    .line 169
    instance-of v0, p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MenuKey;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->circularDrawableFactory:Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;

    const v1, 0x7f020141

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;->get(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 177
    :goto_0
    return-object v0

    .line 171
    :cond_0
    instance-of v0, p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    if-eqz v0, :cond_1

    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 173
    :cond_1
    instance-of v0, p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    if-eqz v0, :cond_2

    .line 174
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->circularDrawableFactory:Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/CircularDrawableFactory;->get(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 176
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown key type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 177
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic getChatHeadDrawable(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 30
    check-cast p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->getChatHeadDrawable(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public removeView(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)V
    .locals 4
    .param p1, "key"    # Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;",
            "Lcom/flipkart/chatheads/ui/ChatHead",
            "<+",
            "Ljava/io/Serializable;",
            ">;",
            "Landroid/view/ViewGroup;",
            ")V"
        }
    .end annotation

    .prologue
    .line 151
    .local p2, "chatHead":Lcom/flipkart/chatheads/ui/ChatHead;, "Lcom/flipkart/chatheads/ui/ChatHead<+Ljava/io/Serializable;>;"
    sget-object v1, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "removeView: key: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->viewCache:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;

    .line 155
    .local v0, "cachedScreen":Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
    if-eqz v0, :cond_0

    .line 156
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onSetInactive()V

    .line 157
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onPause()V

    .line 158
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onStop()V

    .line 159
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onDestroy()V

    .line 161
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->viewCache:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->repository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v1, p1}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->onChatHeadRemoved(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;)V

    .line 165
    return-void
.end method

.method public bridge synthetic removeView(Ljava/lang/Object;Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;

    invoke-virtual {p0, p1, p2, p3}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatViewAdapter;->removeView(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$BaseKey;Lcom/flipkart/chatheads/ui/ChatHead;Landroid/view/ViewGroup;)V

    return-void
.end method
