.class public abstract Lcom/microsoft/xbox/presentation/party/PartyTextViewIntents$SendTextIntent;
.super Ljava/lang/Object;
.source "PartyTextViewIntents.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/party/PartyTextViewIntents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "SendTextIntent"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Ljava/lang/String;)Lcom/microsoft/xbox/presentation/party/PartyTextViewIntents$SendTextIntent;
    .locals 1
    .param p0, "text"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 17
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 18
    new-instance v0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewIntents_SendTextIntent;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyTextViewIntents_SendTextIntent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public abstract text()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
