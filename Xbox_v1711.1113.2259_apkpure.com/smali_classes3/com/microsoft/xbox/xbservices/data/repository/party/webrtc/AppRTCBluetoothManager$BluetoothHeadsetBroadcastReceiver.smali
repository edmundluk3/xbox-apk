.class Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AppRTCBluetoothManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BluetoothHeadsetBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;
    .param p2, "x1"    # Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$1;

    .prologue
    .line 125
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;-><init>(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v6, 0xa

    const/4 v5, 0x0

    .line 128
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-static {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$100(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->UNINITIALIZED:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-ne v2, v3, :cond_0

    .line 188
    :goto_0
    return-void

    .line 131
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 136
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 137
    const-string v2, "android.bluetooth.profile.extra.STATE"

    .line 138
    invoke-virtual {p2, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 139
    .local v1, "state":I
    const-string v2, "AppRTCBluetoothManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BluetoothHeadsetBroadcastReceiver.onReceive: a=ACTION_CONNECTION_STATE_CHANGED, s="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    .line 141
    invoke-static {v4, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$500(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", sb="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 142
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;->isInitialStickyBroadcast()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", BT state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    .line 143
    invoke-static {v4}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$100(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 139
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 145
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    iput v5, v2, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->scoConnectionAttempts:I

    .line 146
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-static {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$300(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)V

    .line 187
    .end local v1    # "state":I
    :cond_1
    :goto_1
    const-string v2, "AppRTCBluetoothManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onReceive done: BT state="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-static {v4}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$100(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 147
    .restart local v1    # "state":I
    :cond_2
    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 149
    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    .line 151
    if-nez v1, :cond_1

    .line 153
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->stopScoAudio()V

    .line 154
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-static {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$300(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)V

    goto :goto_1

    .line 158
    .end local v1    # "state":I
    :cond_3
    const-string v2, "android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 159
    const-string v2, "android.bluetooth.profile.extra.STATE"

    invoke-virtual {p2, v2, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 161
    .restart local v1    # "state":I
    const-string v2, "AppRTCBluetoothManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BluetoothHeadsetBroadcastReceiver.onReceive: a=ACTION_AUDIO_STATE_CHANGED, s="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    .line 163
    invoke-static {v4, v1}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$500(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", sb="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 164
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;->isInitialStickyBroadcast()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", BT state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    .line 165
    invoke-static {v4}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$100(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 161
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    const/16 v2, 0xc

    if-ne v1, v2, :cond_5

    .line 167
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-static {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$600(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)V

    .line 168
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-static {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$100(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->SCO_CONNECTING:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    if-ne v2, v3, :cond_4

    .line 169
    const-string v2, "AppRTCBluetoothManager"

    const-string v3, "+++ Bluetooth audio SCO is now connected"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    sget-object v3, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;->SCO_CONNECTED:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    invoke-static {v2, v3}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$102(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$State;

    .line 171
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    iput v5, v2, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->scoConnectionAttempts:I

    .line 172
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-static {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$300(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)V

    goto/16 :goto_1

    .line 174
    :cond_4
    const-string v2, "AppRTCBluetoothManager"

    const-string v3, "Unexpected state BluetoothHeadset.STATE_AUDIO_CONNECTED"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 176
    :cond_5
    const/16 v2, 0xb

    if-ne v1, v2, :cond_6

    .line 177
    const-string v2, "AppRTCBluetoothManager"

    const-string v3, "+++ Bluetooth audio SCO is now connecting..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 178
    :cond_6
    if-ne v1, v6, :cond_1

    .line 179
    const-string v2, "AppRTCBluetoothManager"

    const-string v3, "+++ Bluetooth audio SCO is now disconnected"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;->isInitialStickyBroadcast()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 181
    const-string v2, "AppRTCBluetoothManager"

    const-string v3, "Ignore STATE_AUDIO_DISCONNECTED initial sticky broadcast."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 184
    :cond_7
    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager$BluetoothHeadsetBroadcastReceiver;->this$0:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;

    invoke-static {v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;->access$300(Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCBluetoothManager;)V

    goto/16 :goto_1
.end method
