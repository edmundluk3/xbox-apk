.class public final Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;
.super Ljava/lang/Object;
.source "TutorialPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final facebookManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;",
            ">;"
        }
    .end annotation
.end field

.field private final interactorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;",
            ">;"
        }
    .end annotation
.end field

.field private final schedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialNavigatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialPresenterMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final welcomeCardCompletionRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p1, "tutorialPresenterMembersInjector":Ldagger/MembersInjector;, "Ldagger/MembersInjector<Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;>;"
    .local p2, "interactorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;>;"
    .local p3, "tutorialNavigatorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;>;"
    .local p4, "welcomeCardCompletionRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;>;"
    .local p5, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    .local p6, "facebookManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    sget-boolean v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 38
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;->tutorialPresenterMembersInjector:Ldagger/MembersInjector;

    .line 39
    sget-boolean v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 40
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;->interactorProvider:Ljavax/inject/Provider;

    .line 41
    sget-boolean v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 42
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;->tutorialNavigatorProvider:Ljavax/inject/Provider;

    .line 43
    sget-boolean v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 44
    :cond_3
    iput-object p4, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;->welcomeCardCompletionRepositoryProvider:Ljavax/inject/Provider;

    .line 45
    sget-boolean v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 46
    :cond_4
    iput-object p5, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;->schedulerProvider:Ljavax/inject/Provider;

    .line 47
    sget-boolean v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 48
    :cond_5
    iput-object p6, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;->facebookManagerProvider:Ljavax/inject/Provider;

    .line 49
    return-void
.end method

.method public static create(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    .local p0, "tutorialPresenterMembersInjector":Ldagger/MembersInjector;, "Ldagger/MembersInjector<Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;>;"
    .local p1, "interactorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;>;"
    .local p2, "tutorialNavigatorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;>;"
    .local p3, "welcomeCardCompletionRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;>;"
    .local p4, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    .local p5, "facebookManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;>;"
    new-instance v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;-><init>(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;
    .locals 7

    .prologue
    .line 53
    iget-object v6, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;->tutorialPresenterMembersInjector:Ldagger/MembersInjector;

    new-instance v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;->interactorProvider:Ljavax/inject/Provider;

    .line 56
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;->tutorialNavigatorProvider:Ljavax/inject/Provider;

    .line 57
    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;->welcomeCardCompletionRepositoryProvider:Ljavax/inject/Provider;

    .line 58
    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;

    iget-object v4, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;->schedulerProvider:Ljavax/inject/Provider;

    .line 59
    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    iget-object v5, p0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;->facebookManagerProvider:Ljavax/inject/Provider;

    .line 60
    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;-><init>(Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;Lcom/microsoft/xbox/presentation/tutorial/TutorialNavigator;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)V

    .line 53
    invoke-static {v6, v0}, Ldagger/internal/MembersInjectors;->injectMembers(Ldagger/MembersInjector;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter_Factory;->get()Lcom/microsoft/xbox/presentation/tutorial/TutorialPresenter;

    move-result-object v0

    return-object v0
.end method
