.class Lcom/microsoft/xbox/service/model/MessageModel$MuteConversationRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "MessageModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/MessageModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MuteConversationRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final caller:Lcom/microsoft/xbox/service/model/MessageModel;

.field private final conversationId:Ljava/lang/String;

.field private final isMuted:Z

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/MessageModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;Z)V
    .locals 0
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .param p3, "conversationId"    # Ljava/lang/String;
    .param p4, "isMuted"    # Z

    .prologue
    .line 2187
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/MessageModel$MuteConversationRunner;->this$0:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 2188
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/MessageModel$MuteConversationRunner;->caller:Lcom/microsoft/xbox/service/model/MessageModel;

    .line 2189
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/MessageModel$MuteConversationRunner;->conversationId:Ljava/lang/String;

    .line 2190
    iput-boolean p4, p0, Lcom/microsoft/xbox/service/model/MessageModel$MuteConversationRunner;->isMuted:Z

    .line 2191
    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2180
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MessageModel$MuteConversationRunner;->buildData()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/lang/Void;
    .locals 7
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2196
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MessageModel$MuteConversationRunner;->conversationId:Ljava/lang/String;

    invoke-static {v3}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->isSkypeGroupMessage(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel$MuteConversationRunner;->conversationId:Ljava/lang/String;

    .line 2198
    .local v0, "id":Ljava/lang/String;
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->access$500()Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;

    move-result-object v3

    iget-boolean v4, p0, Lcom/microsoft/xbox/service/model/MessageModel$MuteConversationRunner;->isMuted:Z

    invoke-interface {v3, v0, v4}, Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;->setConversationToMute(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2199
    .local v1, "result":Z
    :goto_1
    if-nez v1, :cond_2

    .line 2200
    new-instance v2, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v4, 0x264a

    invoke-direct {v2, v4, v5}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v2

    .line 2196
    .end local v0    # "id":Ljava/lang/String;
    .end local v1    # "result":Z
    :cond_0
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "8:xbox:%s"

    new-array v5, v1, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/MessageModel$MuteConversationRunner;->conversationId:Ljava/lang/String;

    aput-object v6, v5, v2

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .restart local v0    # "id":Ljava/lang/String;
    :cond_1
    move v1, v2

    .line 2198
    goto :goto_1

    .line 2203
    .restart local v1    # "result":Z
    :cond_2
    const/4 v2, 0x0

    return-object v2
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 2217
    const-wide/16 v0, 0x264a

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2212
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Void;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel$MuteConversationRunner;->caller:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/MessageModel;->access$1900(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 2213
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 2208
    return-void
.end method
