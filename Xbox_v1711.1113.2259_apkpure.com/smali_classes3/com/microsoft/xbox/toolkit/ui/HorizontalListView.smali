.class public Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;
.super Landroid/widget/AdapterView;
.source "HorizontalListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$OnScrollListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/AdapterView",
        "<",
        "Landroid/widget/ListAdapter;",
        ">;"
    }
.end annotation


# static fields
.field private static final FLING_BLOCK_TIMEOUT_MS:I = 0x3e8

.field private static final SCROLL_BLOCK_TIMEOUT_MS:I = 0x1f4


# instance fields
.field protected mAdapter:Landroid/widget/ListAdapter;

.field public mAlwaysOverrideTouch:Z

.field protected mCurrentX:I

.field private mDataChanged:Z

.field private mDataObserver:Landroid/database/DataSetObserver;

.field private mDisplayOffset:I

.field private mGesture:Landroid/view/GestureDetector;

.field private mLeftViewIndex:I

.field private mMaxX:I

.field protected mNextX:I

.field private mOnGesture:Landroid/view/GestureDetector$OnGestureListener;

.field private mOnItemClicked:Landroid/widget/AdapterView$OnItemClickListener;

.field private mOnItemLongClicked:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private mOnItemSelected:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mRemovedViewQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mRightViewIndex:I

.field protected mScroller:Landroid/widget/Scroller;

.field private onScrollListener:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$OnScrollListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 95
    invoke-direct {p0, p1, p2}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mAlwaysOverrideTouch:Z

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mLeftViewIndex:I

    .line 73
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mRightViewIndex:I

    .line 76
    const v0, 0x7fffffff

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mMaxX:I

    .line 77
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mDisplayOffset:I

    .line 80
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    .line 84
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mDataChanged:Z

    .line 145
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$1;-><init>(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mDataObserver:Landroid/database/DataSetObserver;

    .line 361
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$3;-><init>(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mOnGesture:Landroid/view/GestureDetector$OnGestureListener;

    .line 96
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->initView()V

    .line 97
    return-void
.end method

.method static synthetic access$002(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mDataChanged:Z

    return p1
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->reset()V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$OnScrollListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->onScrollListener:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$OnScrollListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    .prologue
    .line 56
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mLeftViewIndex:I

    return v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    .prologue
    .line 56
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mRightViewIndex:I

    return v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mOnItemClicked:Landroid/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)Landroid/widget/AdapterView$OnItemSelectedListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mOnItemSelected:Landroid/widget/AdapterView$OnItemSelectedListener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mOnItemLongClicked:Landroid/widget/AdapterView$OnItemLongClickListener;

    return-object v0
.end method

.method private addAndMeasureChild(Landroid/view/View;I)V
    .locals 4
    .param p1, "child"    # Landroid/view/View;
    .param p2, "viewPos"    # I

    .prologue
    const/4 v1, -0x1

    const/high16 v3, -0x80000000

    .line 199
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 200
    .local v0, "params":Landroid/view/ViewGroup$LayoutParams;
    if-nez v0, :cond_0

    .line 201
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    .end local v0    # "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 204
    .restart local v0    # "params":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    .line 205
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getWidth()I

    move-result v1

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getHeight()I

    move-result v2

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/view/View;->measure(II)V

    .line 206
    return-void
.end method

.method private fillList(I)V
    .locals 3
    .param p1, "dx"    # I

    .prologue
    .line 258
    const/4 v1, 0x0

    .line 259
    .local v1, "edge":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 260
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 261
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v1

    .line 263
    :cond_0
    invoke-direct {p0, v1, p1}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->fillListRight(II)V

    .line 265
    const/4 v1, 0x0

    .line 266
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 267
    if-eqz v0, :cond_1

    .line 268
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 270
    :cond_1
    invoke-direct {p0, v1, p1}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->fillListLeft(II)V

    .line 272
    return-void
.end method

.method private fillListLeft(II)V
    .locals 4
    .param p1, "leftEdge"    # I
    .param p2, "dx"    # I

    .prologue
    .line 294
    :goto_0
    add-int v1, p1, p2

    if-lez v1, :cond_0

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mLeftViewIndex:I

    if-ltz v1, :cond_0

    .line 295
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mLeftViewIndex:I

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-interface {v2, v3, v1, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 296
    .local v0, "child":Landroid/view/View;
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->addAndMeasureChild(Landroid/view/View;I)V

    .line 297
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr p1, v1

    .line 298
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mLeftViewIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mLeftViewIndex:I

    .line 299
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mDisplayOffset:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mDisplayOffset:I

    goto :goto_0

    .line 301
    .end local v0    # "child":Landroid/view/View;
    :cond_0
    return-void
.end method

.method private fillListRight(II)V
    .locals 4
    .param p1, "rightEdge"    # I
    .param p2, "dx"    # I

    .prologue
    .line 275
    :goto_0
    add-int v1, p1, p2

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getWidth()I

    move-result v2

    if-ge v1, v2, :cond_2

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mRightViewIndex:I

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 277
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mRightViewIndex:I

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-interface {v2, v3, v1, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 278
    .local v0, "child":Landroid/view/View;
    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->addAndMeasureChild(Landroid/view/View;I)V

    .line 279
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr p1, v1

    .line 281
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mRightViewIndex:I

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_0

    .line 282
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mCurrentX:I

    add-int/2addr v1, p1

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mMaxX:I

    .line 285
    :cond_0
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mMaxX:I

    if-gez v1, :cond_1

    .line 286
    const/4 v1, 0x0

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mMaxX:I

    .line 288
    :cond_1
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mRightViewIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mRightViewIndex:I

    goto :goto_0

    .line 291
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    return-void
.end method

.method private declared-synchronized initView()V
    .locals 3

    .prologue
    .line 100
    monitor-enter p0

    const/4 v0, -0x1

    :try_start_0
    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mLeftViewIndex:I

    .line 101
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mRightViewIndex:I

    .line 102
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mDisplayOffset:I

    .line 103
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mCurrentX:I

    .line 104
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mNextX:I

    .line 105
    const v0, 0x7fffffff

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mMaxX:I

    .line 106
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    .line 107
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mOnGesture:Landroid/view/GestureDetector$OnGestureListener;

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mGesture:Landroid/view/GestureDetector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    monitor-exit p0

    return-void

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private positionItems(I)V
    .locals 7
    .param p1, "dx"    # I

    .prologue
    .line 324
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getChildCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 325
    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mDisplayOffset:I

    add-int/2addr v4, p1

    iput v4, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mDisplayOffset:I

    .line 326
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mDisplayOffset:I

    .line 327
    .local v3, "left":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getChildCount()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 328
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 329
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 330
    .local v1, "childWidth":I
    const/4 v4, 0x0

    add-int v5, v3, v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 331
    add-int/2addr v3, v1

    .line 327
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 334
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "childWidth":I
    .end local v2    # "i":I
    .end local v3    # "left":I
    :cond_0
    return-void
.end method

.method private removeNonVisibleItems(I)V
    .locals 4
    .param p1, "dx"    # I

    .prologue
    const/4 v3, 0x0

    .line 304
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 305
    .local v0, "child":Landroid/view/View;
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v1

    add-int/2addr v1, p1

    if-gtz v1, :cond_0

    .line 306
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mDisplayOffset:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mDisplayOffset:I

    .line 307
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 308
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->removeViewInLayout(Landroid/view/View;)V

    .line 309
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mLeftViewIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mLeftViewIndex:I

    .line 310
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 314
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 315
    :goto_1
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getWidth()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 316
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 317
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->removeViewInLayout(Landroid/view/View;)V

    .line 318
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mRightViewIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mRightViewIndex:I

    .line 319
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 321
    :cond_1
    return-void
.end method

.method private declared-synchronized reset()V
    .locals 1

    .prologue
    .line 188
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->initView()V

    .line 189
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->removeAllViewsInLayout()V

    .line 190
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->requestLayout()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    monitor-exit p0

    return-void

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 343
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mGesture:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 344
    .local v0, "handled":Z
    return v0
.end method

.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public getCurrentX()I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mCurrentX:I

    return v0
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 1

    .prologue
    .line 174
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onDown(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 357
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 358
    return v1
.end method

.method protected onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 9
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 348
    monitor-enter p0

    .line 349
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mNextX:I

    const/4 v2, 0x0

    neg-float v3, p3

    float-to-int v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mMaxX:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 350
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 351
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->requestLayout()V

    .line 353
    const/4 v0, 0x1

    return v0

    .line 350
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected declared-synchronized onLayout(ZIIII)V
    .locals 5
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 210
    monitor-enter p0

    :try_start_0
    invoke-super/range {p0 .. p5}, Landroid/widget/AdapterView;->onLayout(ZIIII)V

    .line 212
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_1

    .line 255
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 216
    :cond_1
    :try_start_1
    iget-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mDataChanged:Z

    if-eqz v3, :cond_2

    .line 217
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mCurrentX:I

    .line 218
    .local v1, "oldCurrentX":I
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->initView()V

    .line 219
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->removeAllViewsInLayout()V

    .line 220
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mNextX:I

    .line 221
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mDataChanged:Z

    .line 224
    .end local v1    # "oldCurrentX":I
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 225
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    .line 226
    .local v2, "scrollx":I
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mNextX:I

    .line 229
    .end local v2    # "scrollx":I
    :cond_3
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mNextX:I

    if-gtz v3, :cond_4

    .line 230
    const/4 v3, 0x0

    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mNextX:I

    .line 231
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 233
    :cond_4
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mNextX:I

    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mMaxX:I

    if-lt v3, v4, :cond_5

    .line 234
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mMaxX:I

    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mNextX:I

    .line 235
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 238
    :cond_5
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mCurrentX:I

    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mNextX:I

    sub-int v0, v3, v4

    .line 240
    .local v0, "dx":I
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->removeNonVisibleItems(I)V

    .line 241
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->fillList(I)V

    .line 242
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->positionItems(I)V

    .line 244
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mNextX:I

    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mCurrentX:I

    .line 246
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->isFinished()Z

    move-result v3

    if-nez v3, :cond_0

    .line 247
    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$2;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$2;-><init>(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;)V

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 210
    .end local v0    # "dx":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized scrollTo(I)V
    .locals 5
    .param p1, "x"    # I

    .prologue
    .line 337
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mNextX:I

    const/4 v2, 0x0

    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mNextX:I

    sub-int v3, p1, v3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/Scroller;->startScroll(IIII)V

    .line 338
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->requestLayout()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 339
    monitor-exit p0

    return-void

    .line 337
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 56
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    .line 179
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mDataObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 182
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    .line 183
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mDataObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 184
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->reset()V

    .line 185
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemClickListener;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mOnItemClicked:Landroid/widget/AdapterView$OnItemClickListener;

    .line 118
    return-void
.end method

.method public setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemLongClickListener;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mOnItemLongClicked:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 123
    return-void
.end method

.method public setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemSelectedListener;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->mOnItemSelected:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 113
    return-void
.end method

.method public setOnScrollListener(Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$OnScrollListener;)V
    .locals 0
    .param p1, "onScrollListener"    # Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$OnScrollListener;

    .prologue
    .line 130
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/HorizontalListView;->onScrollListener:Lcom/microsoft/xbox/toolkit/ui/HorizontalListView$OnScrollListener;

    .line 131
    return-void
.end method

.method public setSelection(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 196
    return-void
.end method
