.class Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView$2;
.super Ljava/lang/Object;
.source "NowPlayingMediaView.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/ui/MediaProgressBar$OnUserInitiatedProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->update()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView$2;->this$0:Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Lcom/microsoft/xbox/toolkit/MediaProgressData;)V
    .locals 4
    .param p1, "newProgress"    # Lcom/microsoft/xbox/toolkit/MediaProgressData;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView$2;->this$0:Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->stopMediaTimer()V

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView$2;->this$0:Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->access$000(Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;Lcom/microsoft/xbox/toolkit/MediaProgressData;)V

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView$2;->this$0:Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/ui/NowPlayingMediaView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    iget-wide v2, p1, Lcom/microsoft/xbox/toolkit/MediaProgressData;->positionInSeconds:J

    long-to-float v1, v2

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XBLSharedUtil;->secondsToHundredNanoseconds(F)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;->sendSeekCommand(J)V

    .line 69
    return-void
.end method
