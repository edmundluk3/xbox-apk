.class Lcom/flipkart/chatheads/ui/MinimizedArrangement$1;
.super Lcom/facebook/rebound/SimpleSpringListener;
.source "MinimizedArrangement.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flipkart/chatheads/ui/MinimizedArrangement;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flipkart/chatheads/ui/MinimizedArrangement;


# direct methods
.method constructor <init>(Lcom/flipkart/chatheads/ui/MinimizedArrangement;)V
    .locals 0
    .param p1, "this$0"    # Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    .prologue
    .line 37
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement$1;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement$1;"
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement$1;->this$0:Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    invoke-direct {p0}, Lcom/facebook/rebound/SimpleSpringListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onSpringAtRest(Lcom/facebook/rebound/Spring;)V
    .locals 2
    .param p1, "spring"    # Lcom/facebook/rebound/Spring;

    .prologue
    .line 47
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement$1;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement$1;"
    invoke-super {p0, p1}, Lcom/facebook/rebound/SimpleSpringListener;->onSpringAtRest(Lcom/facebook/rebound/Spring;)V

    .line 48
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement$1;->this$0:Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    invoke-static {v0}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->access$400(Lcom/flipkart/chatheads/ui/MinimizedArrangement;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement$1;->this$0:Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->access$402(Lcom/flipkart/chatheads/ui/MinimizedArrangement;Z)Z

    .line 51
    :cond_0
    return-void
.end method

.method public onSpringUpdate(Lcom/facebook/rebound/Spring;)V
    .locals 8
    .param p1, "spring"    # Lcom/facebook/rebound/Spring;

    .prologue
    .line 40
    .local p0, "this":Lcom/flipkart/chatheads/ui/MinimizedArrangement$1;, "Lcom/flipkart/chatheads/ui/MinimizedArrangement$1;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement$1;->this$0:Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    iget-object v1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement$1;->this$0:Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    invoke-static {v1}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->access$100(Lcom/flipkart/chatheads/ui/MinimizedArrangement;)F

    move-result v1

    float-to-double v2, v1

    iget-object v1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement$1;->this$0:Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    invoke-static {v1}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->access$200(Lcom/flipkart/chatheads/ui/MinimizedArrangement;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-double v4, v1

    invoke-virtual {p1}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v6

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    iget-object v1, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement$1;->this$0:Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    invoke-static {v1}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->access$200(Lcom/flipkart/chatheads/ui/MinimizedArrangement;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-double v4, v1

    div-double/2addr v2, v4

    double-to-float v1, v2

    invoke-static {v0, v1}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->access$002(Lcom/flipkart/chatheads/ui/MinimizedArrangement;F)F

    .line 41
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement$1;->this$0:Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    invoke-static {v0}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->access$300(Lcom/flipkart/chatheads/ui/MinimizedArrangement;)Lcom/facebook/rebound/SpringChain;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/MinimizedArrangement$1;->this$0:Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    invoke-static {v0}, Lcom/flipkart/chatheads/ui/MinimizedArrangement;->access$300(Lcom/flipkart/chatheads/ui/MinimizedArrangement;)Lcom/facebook/rebound/SpringChain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/rebound/SpringChain;->getControlSpring()Lcom/facebook/rebound/Spring;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/rebound/Spring;->getCurrentValue()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/facebook/rebound/Spring;->setCurrentValue(D)Lcom/facebook/rebound/Spring;

    .line 43
    :cond_0
    return-void
.end method
