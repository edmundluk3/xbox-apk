.class Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$11;
.super Ljava/lang/Object;
.source "PivotWithTabs.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/functions/GenericFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->getIndexOfScreen(Ljava/lang/Class;)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/toolkit/functions/GenericFunction",
        "<",
        "Ljava/lang/Boolean;",
        "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

.field final synthetic val$screenClass:Ljava/lang/Class;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;Ljava/lang/Class;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    .prologue
    .line 400
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$11;->this$0:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$11;->val$screenClass:Ljava/lang/Class;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public eval(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)Ljava/lang/Boolean;
    .locals 2
    .param p1, "arg"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 403
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$11;->val$screenClass:Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic eval(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 400
    check-cast p1, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs$11;->eval(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
