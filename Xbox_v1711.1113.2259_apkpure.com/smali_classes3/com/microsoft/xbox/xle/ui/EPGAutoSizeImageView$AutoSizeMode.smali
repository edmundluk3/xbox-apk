.class public final enum Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;
.super Ljava/lang/Enum;
.source "EPGAutoSizeImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AutoSizeMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

.field public static final enum ADJUST_HEIGHT:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

.field public static final enum ADJUST_WIDTH:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

.field public static final enum AUTO:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

.field public static final enum NONE:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 16
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;->NONE:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    .line 17
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    const-string v1, "AUTO"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;->AUTO:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    .line 18
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    const-string v1, "ADJUST_WIDTH"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;->ADJUST_WIDTH:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    .line 19
    new-instance v0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    const-string v1, "ADJUST_HEIGHT"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;->ADJUST_HEIGHT:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    .line 15
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;->NONE:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;->AUTO:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;->ADJUST_WIDTH:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;->ADJUST_HEIGHT:Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;->$VALUES:[Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 15
    const-class v0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;->$VALUES:[Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/ui/EPGAutoSizeImageView$AutoSizeMode;

    return-object v0
.end method
