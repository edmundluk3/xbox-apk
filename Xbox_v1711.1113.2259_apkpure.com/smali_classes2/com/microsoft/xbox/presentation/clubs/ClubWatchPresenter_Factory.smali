.class public final Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;
.super Ljava/lang/Object;
.source "ClubWatchPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final clubWatchPresenterMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final interactorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;",
            ">;"
        }
    .end annotation
.end field

.field private final navigationManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/NavigationManager;",
            ">;"
        }
    .end annotation
.end field

.field private final navigatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/beam/BeamNavigator;",
            ">;"
        }
    .end annotation
.end field

.field private final schedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final systemSettingsModelProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xle/model/SystemSettingsModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/beam/BeamNavigator;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xle/model/SystemSettingsModel;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/NavigationManager;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p1, "clubWatchPresenterMembersInjector":Ldagger/MembersInjector;, "Ldagger/MembersInjector<Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;>;"
    .local p2, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    .local p3, "interactorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;>;"
    .local p4, "navigatorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/presentation/beam/BeamNavigator;>;"
    .local p5, "systemSettingsModelProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xle/model/SystemSettingsModel;>;"
    .local p6, "navigationManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/ui/NavigationManager;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    sget-boolean v0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 39
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;->clubWatchPresenterMembersInjector:Ldagger/MembersInjector;

    .line 40
    sget-boolean v0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 41
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;->schedulerProvider:Ljavax/inject/Provider;

    .line 42
    sget-boolean v0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 43
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;->interactorProvider:Ljavax/inject/Provider;

    .line 44
    sget-boolean v0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 45
    :cond_3
    iput-object p4, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;->navigatorProvider:Ljavax/inject/Provider;

    .line 46
    sget-boolean v0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_4

    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 47
    :cond_4
    iput-object p5, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;->systemSettingsModelProvider:Ljavax/inject/Provider;

    .line 48
    sget-boolean v0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_5

    if-nez p6, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 49
    :cond_5
    iput-object p6, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;->navigationManagerProvider:Ljavax/inject/Provider;

    .line 50
    return-void
.end method

.method public static create(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/beam/BeamNavigator;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xle/model/SystemSettingsModel;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/NavigationManager;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    .local p0, "clubWatchPresenterMembersInjector":Ldagger/MembersInjector;, "Ldagger/MembersInjector<Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;>;"
    .local p1, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    .local p2, "interactorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;>;"
    .local p3, "navigatorProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/presentation/beam/BeamNavigator;>;"
    .local p4, "systemSettingsModelProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xle/model/SystemSettingsModel;>;"
    .local p5, "navigationManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/ui/NavigationManager;>;"
    new-instance v0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;-><init>(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;
    .locals 7

    .prologue
    .line 54
    iget-object v6, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;->clubWatchPresenterMembersInjector:Ldagger/MembersInjector;

    new-instance v0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;->schedulerProvider:Ljavax/inject/Provider;

    .line 57
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;->interactorProvider:Ljavax/inject/Provider;

    .line 58
    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;->navigatorProvider:Ljavax/inject/Provider;

    .line 59
    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/presentation/beam/BeamNavigator;

    iget-object v4, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;->systemSettingsModelProvider:Ljavax/inject/Provider;

    .line 60
    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    iget-object v5, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;->navigationManagerProvider:Ljavax/inject/Provider;

    .line 61
    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;-><init>(Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;Lcom/microsoft/xbox/presentation/beam/BeamNavigator;Lcom/microsoft/xbox/xle/model/SystemSettingsModel;Lcom/microsoft/xbox/toolkit/ui/NavigationManager;)V

    .line 54
    invoke-static {v6, v0}, Ldagger/internal/MembersInjectors;->injectMembers(Ldagger/MembersInjector;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter_Factory;->get()Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;

    move-result-object v0

    return-object v0
.end method
