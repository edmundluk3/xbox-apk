.class public final Lcom/microsoft/xbox/service/model/XPrivilegeConstants;
.super Ljava/lang/Object;
.source "XPrivilegeConstants.java"


# static fields
.field public static final XPRIVILEGE_ADD_FRIEND:I = 0xff

.field public static final XPRIVILEGE_BROADCAST:I = 0xbe

.field public static final XPRIVILEGE_CLUBS_SESSIONS:I = 0xbc

.field public static final XPRIVILEGE_COMMUNICATIONS:I = 0xfc

.field public static final XPRIVILEGE_GAME_DVR:I = 0xc6

.field public static final XPRIVILEGE_MANAGE_PROFILE_PRIVACY:I = 0xc4

.field public static final XPRIVILEGE_MULTIPLAYER_SESSIONS:I = 0xfe

.field public static final XPRIVILEGE_PII_ACCESS:I = 0xdd

.field public static final XPRIVILEGE_PROFILE_VIEWING:I = 0xf9

.field public static final XPRIVILEGE_PURCHASE_CONTENT:I = 0xf5

.field public static final XPRIVILEGE_SHARE_CONTENT:I = 0xd3

.field public static final XPRIVILEGE_SHARE_KINECT_CONTENT:I = 0xc7

.field public static final XPRIVILEGE_SOCIAL_NETWORK_SHARING:I = 0xdc

.field public static final XPRIVILEGE_USER_CREATED_CONTENT:I = 0xf7


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
