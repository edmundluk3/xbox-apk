.class Lcom/microsoft/xbox/smartglass/controls/ScriptRunner$1;
.super Ljava/util/TimerTask;
.source "ScriptRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;-><init>(Landroid/webkit/WebView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 44
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    invoke-static {v4}, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->access$000(Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;)J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 45
    .local v0, "timeDelta":J
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->access$100(Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;)Ljava/util/Queue;

    move-result-object v3

    monitor-enter v3

    .line 46
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->access$200(Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 47
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->access$100(Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;)Ljava/util/Queue;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Queue;->size()I

    move-result v2

    const/16 v4, 0x20

    if-gt v2, v4, :cond_0

    const-wide/16 v4, 0x7530

    cmp-long v2, v0, v4

    if-gtz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->access$300(Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;)I

    move-result v2

    const/high16 v4, 0x1000000

    if-le v2, v4, :cond_1

    .line 48
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner$1;->this$0:Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;

    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;->access$400(Lcom/microsoft/xbox/smartglass/controls/ScriptRunner;)V

    .line 51
    :cond_1
    monitor-exit v3

    .line 52
    return-void

    .line 51
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
