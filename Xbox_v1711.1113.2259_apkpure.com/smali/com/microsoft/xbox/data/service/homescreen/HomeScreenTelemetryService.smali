.class public Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;
.super Ljava/lang/Object;
.source "HomeScreenTelemetryService.java"


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    return-void
.end method


# virtual methods
.method public changeDefault(ILcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;)V
    .locals 1
    .param p1, "currentSetting"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2, "preference"    # Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 26
    new-instance v0, Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService$1;-><init>(Lcom/microsoft/xbox/data/service/homescreen/HomeScreenTelemetryService;ILcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 38
    return-void
.end method
