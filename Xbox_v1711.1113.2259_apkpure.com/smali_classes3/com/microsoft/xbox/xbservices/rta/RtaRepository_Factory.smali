.class public final Lcom/microsoft/xbox/xbservices/rta/RtaRepository_Factory;
.super Ljava/lang/Object;
.source "RtaRepository_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/xbservices/rta/RtaRepository;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final httpClientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field

.field private final loggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;",
            ">;"
        }
    .end annotation
.end field

.field private final rtaRepositoryMembersInjector:Ldagger/MembersInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xbservices/rta/RtaRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final telemetryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository_Factory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository_Factory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xbservices/rta/RtaRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p1, "rtaRepositoryMembersInjector":Ldagger/MembersInjector;, "Ldagger/MembersInjector<Lcom/microsoft/xbox/xbservices/rta/RtaRepository;>;"
    .local p2, "httpClientProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lokhttp3/OkHttpClient;>;"
    .local p3, "loggerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;>;"
    .local p4, "telemetryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    sget-boolean v0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 31
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository_Factory;->rtaRepositoryMembersInjector:Ldagger/MembersInjector;

    .line 32
    sget-boolean v0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 33
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository_Factory;->httpClientProvider:Ljavax/inject/Provider;

    .line 34
    sget-boolean v0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 35
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository_Factory;->loggerProvider:Ljavax/inject/Provider;

    .line 36
    sget-boolean v0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository_Factory;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 37
    :cond_3
    iput-object p4, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository_Factory;->telemetryProvider:Ljavax/inject/Provider;

    .line 38
    return-void
.end method

.method public static create(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/xbservices/rta/RtaRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lokhttp3/OkHttpClient;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/xbservices/rta/RtaRepository;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    .local p0, "rtaRepositoryMembersInjector":Ldagger/MembersInjector;, "Ldagger/MembersInjector<Lcom/microsoft/xbox/xbservices/rta/RtaRepository;>;"
    .local p1, "httpClientProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lokhttp3/OkHttpClient;>;"
    .local p2, "loggerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;>;"
    .local p3, "telemetryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;>;"
    new-instance v0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/xbservices/rta/RtaRepository_Factory;-><init>(Ldagger/MembersInjector;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/xbservices/rta/RtaRepository;
    .locals 5

    .prologue
    .line 42
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository_Factory;->rtaRepositoryMembersInjector:Ldagger/MembersInjector;

    new-instance v4, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;

    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository_Factory;->httpClientProvider:Ljavax/inject/Provider;

    .line 44
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lokhttp3/OkHttpClient;

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository_Factory;->loggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository_Factory;->telemetryProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;

    invoke-direct {v4, v0, v1, v2}, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;-><init>(Lokhttp3/OkHttpClient;Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;Lcom/microsoft/xbox/xbservices/data/repository/telemetry/TelemetryProvider;)V

    .line 42
    invoke-static {v3, v4}, Ldagger/internal/MembersInjectors;->injectMembers(Ldagger/MembersInjector;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/rta/RtaRepository;

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/microsoft/xbox/xbservices/rta/RtaRepository_Factory;->get()Lcom/microsoft/xbox/xbservices/rta/RtaRepository;

    move-result-object v0

    return-object v0
.end method
