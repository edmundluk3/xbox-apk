.class public final Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_ValidatePermissionRequest$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_PrivacyDataTypes_ValidatePermissionRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_ValidatePermissionRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$ValidatePermissionRequest;",
        ">;"
    }
.end annotation


# instance fields
.field private final permissionsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionTypes;",
            ">;>;"
        }
    .end annotation
.end field

.field private final usersAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 5
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 28
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 29
    const-class v0, Lcom/google/common/collect/ImmutableList;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_ValidatePermissionRequest$GsonTypeAdapter;->usersAdapter:Lcom/google/gson/TypeAdapter;

    .line 30
    const-class v0, Lcom/google/common/collect/ImmutableList;

    new-array v1, v4, [Ljava/lang/reflect/Type;

    const-class v2, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionTypes;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_ValidatePermissionRequest$GsonTypeAdapter;->permissionsAdapter:Lcom/google/gson/TypeAdapter;

    .line 31
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$ValidatePermissionRequest;
    .locals 5
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v3

    sget-object v4, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v3, v4, :cond_0

    .line 50
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 51
    const/4 v3, 0x0

    .line 77
    :goto_0
    return-object v3

    .line 53
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 54
    const/4 v2, 0x0

    .line 55
    .local v2, "users":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;>;"
    const/4 v1, 0x0

    .line 56
    .local v1, "permissions":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionTypes;>;"
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 57
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 58
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v3

    sget-object v4, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v3, v4, :cond_1

    .line 59
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 62
    :cond_1
    const/4 v3, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v3, :pswitch_data_0

    .line 72
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 62
    :sswitch_0
    const-string v4, "Users"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x0

    goto :goto_2

    :sswitch_1
    const-string v4, "Permissions"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x1

    goto :goto_2

    .line 64
    :pswitch_0
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_ValidatePermissionRequest$GsonTypeAdapter;->usersAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v3, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "users":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;>;"
    check-cast v2, Lcom/google/common/collect/ImmutableList;

    .line 65
    .restart local v2    # "users":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;>;"
    goto :goto_1

    .line 68
    :pswitch_1
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_ValidatePermissionRequest$GsonTypeAdapter;->permissionsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v3, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "permissions":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionTypes;>;"
    check-cast v1, Lcom/google/common/collect/ImmutableList;

    .line 69
    .restart local v1    # "permissions":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$PermissionTypes;>;"
    goto :goto_1

    .line 76
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 77
    new-instance v3, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_ValidatePermissionRequest;

    invoke-direct {v3, v2, v1}, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_ValidatePermissionRequest;-><init>(Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;)V

    goto :goto_0

    .line 62
    :sswitch_data_0
    .sparse-switch
        -0x5903639c -> :sswitch_1
        0x4e39de8 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_ValidatePermissionRequest$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$ValidatePermissionRequest;

    move-result-object v0

    return-object v0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$ValidatePermissionRequest;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$ValidatePermissionRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    if-nez p2, :cond_0

    .line 36
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 45
    :goto_0
    return-void

    .line 39
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 40
    const-string v0, "Users"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_ValidatePermissionRequest$GsonTypeAdapter;->usersAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$ValidatePermissionRequest;->users()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 42
    const-string v0, "Permissions"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_ValidatePermissionRequest$GsonTypeAdapter;->permissionsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$ValidatePermissionRequest;->permissions()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 44
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    check-cast p2, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$ValidatePermissionRequest;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_ValidatePermissionRequest$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$ValidatePermissionRequest;)V

    return-void
.end method
