.class public Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "ClubBackgroundAchievementDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$GetUserClubsTask;,
        Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameViewHolder;,
        Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameListAdapter;
    }
.end annotation


# instance fields
.field private final clubNameListAdapter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameListAdapter;

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private userClubsTask:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$GetUserClubsTask;

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    .line 43
    const v3, 0x7f080238

    invoke-direct {p0, p1, v3}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 44
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 45
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 46
    const v3, 0x7f03007a

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->setContentView(I)V

    .line 47
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    .line 49
    const v3, 0x7f0e0327

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 50
    const v3, 0x7f0e0328

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/RecyclerView;

    .line 51
    .local v2, "recyclerView":Landroid/support/v7/widget/RecyclerView;
    new-instance v3, Landroid/support/v7/widget/LinearLayoutManager;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {v3, p1, v4, v5}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 52
    new-instance v3, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameListAdapter;

    invoke-direct {v3, p0, v6}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameListAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$1;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->clubNameListAdapter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameListAdapter;

    .line 53
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->clubNameListAdapter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameListAdapter;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 55
    const v3, 0x7f0e0326

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 56
    .local v0, "achievementImage":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getImageUrl()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f020122

    const v5, 0x7f020115

    invoke-virtual {v0, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 58
    const v3, 0x7f0e0325

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 59
    .local v1, "achievementTitle":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getTitleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    new-instance v3, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$GetUserClubsTask;

    invoke-direct {v3, p0, v6}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$GetUserClubsTask;-><init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$1;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->userClubsTask:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$GetUserClubsTask;

    .line 63
    const v3, 0x7f0e0324

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;)Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;)Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->onUserClubsLoaded(Ljava/util/List;)V

    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->dismiss()V

    return-void
.end method

.method private onUserClubsLoaded(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 79
    .local p1, "userClubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    if-nez p1, :cond_0

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 89
    :goto_0
    return-void

    .line 81
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    goto :goto_0

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->clubNameListAdapter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameListAdapter;->clear()V

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->clubNameListAdapter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameListAdapter;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameListAdapter;->addAll(Ljava/util/Collection;)V

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->clubNameListAdapter:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$ClubNameListAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method


# virtual methods
.method protected onStart()V
    .locals 2

    .prologue
    .line 68
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->onStart()V

    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->userClubsTask:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$GetUserClubsTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$GetUserClubsTask;->load(Z)V

    .line 70
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 74
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;->onStop()V

    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;->userClubsTask:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$GetUserClubsTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog$GetUserClubsTask;->cancel()V

    .line 76
    return-void
.end method
