.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$Messaging;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Messaging"
.end annotation


# static fields
.field public static final AddPeople:Ljava/lang/String; = "Messaging - Add People"

.field public static final BlockUser:Ljava/lang/String; = "Messaging - Block User"

.field public static final CopyText:Ljava/lang/String; = "Messaging - Copy Text"

.field public static final CreateGroupConversation:Ljava/lang/String; = "Messaging - Create Group Conversation"

.field public static final CreateMessage:Ljava/lang/String; = "Messaging - Create Message"

.field public static final DeleteConversation:Ljava/lang/String; = "Messaging - Delete Conversation"

.field public static final DeleteMessage:Ljava/lang/String; = "Messaging - Delete Message"

.field public static final LeaveConversation:Ljava/lang/String; = "Messaging - Leave Conversation"

.field public static final MarkConversationRead:Ljava/lang/String; = "Messaging - MarkConversationRead"

.field public static final MuteConversation:Ljava/lang/String; = "Messaging - Mute Conversation"

.field public static final PlayBroadcast:Ljava/lang/String; = "Messaging - Play Broadcast"

.field public static final PlayClip:Ljava/lang/String; = "Messaging - Play Clip"

.field public static final ReadConversation:Ljava/lang/String; = "Messaging - Read Conversation"

.field public static final RenameConversation:Ljava/lang/String; = "Messaging - Rename Conversation"

.field public static final Report:Ljava/lang/String; = "Messaging - Report"

.field public static final SendMessage:Ljava/lang/String; = "Messaging - Send Message"

.field public static final ShowAchievement:Ljava/lang/String; = "Messaging - Show Achievement"

.field public static final ShowScreenshot:Ljava/lang/String; = "Messaging - Show Screenshot"

.field public static final ShowSharedItem:Ljava/lang/String; = "Messaging - Show Shared Item"

.field public static final UnBlockUser:Ljava/lang/String; = "Messaging - Unblock User"

.field public static final UnMuteConversation:Ljava/lang/String; = "Messaging - UnMute Conversation"

.field public static final ViewPeople:Ljava/lang/String; = "Messaging - View People"

.field public static final ViewProfile:Ljava/lang/String; = "Messaging - View Profile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 866
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
