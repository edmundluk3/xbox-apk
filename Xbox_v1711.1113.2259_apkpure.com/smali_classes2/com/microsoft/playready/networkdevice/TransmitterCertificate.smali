.class public Lcom/microsoft/playready/networkdevice/TransmitterCertificate;
.super Ljava/lang/Object;
.source "TransmitterCertificate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;,
        Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;
    }
.end annotation


# instance fields
.field public final ClientID:[B

.field public final ExpirationDate:Ljava/util/Date;

.field public final ModelDigest:[B

.field public final ModelManufacturerName:Ljava/lang/String;

.field public final ModelName:Ljava/lang/String;

.field public final ModelNumber:Ljava/lang/String;

.field public final PlatformIdentifier:I

.field public final SecurityLevel:I

.field public final SecurityVersion:I

.field public final SupportedFeatures:[Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

.field public final Type:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;


# direct methods
.method public constructor <init>(Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;IIII[Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;[B[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "type"    # Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;
    .param p2, "platformID"    # I
    .param p3, "securityLevel"    # I
    .param p4, "securityVersion"    # I
    .param p5, "expirationDate"    # I
    .param p6, "supportedFeatures"    # [Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;
    .param p7, "clientID"    # [B
    .param p8, "modelDigest"    # [B
    .param p9, "modelMfgName"    # Ljava/lang/String;
    .param p10, "modelName"    # Ljava/lang/String;
    .param p11, "modelNumber"    # Ljava/lang/String;

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate;->Type:Lcom/microsoft/playready/networkdevice/TransmitterCertificate$CertificateType;

    .line 151
    iput p2, p0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate;->PlatformIdentifier:I

    .line 152
    iput p3, p0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate;->SecurityLevel:I

    .line 153
    iput p4, p0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate;->SecurityVersion:I

    .line 154
    new-instance v0, Ljava/util/Date;

    int-to-long v2, p5

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate;->ExpirationDate:Ljava/util/Date;

    .line 155
    invoke-virtual {p6}, [Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate;->SupportedFeatures:[Lcom/microsoft/playready/networkdevice/TransmitterCertificate$Feature;

    .line 156
    invoke-virtual {p7}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate;->ClientID:[B

    .line 157
    invoke-virtual {p8}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate;->ModelDigest:[B

    .line 158
    iput-object p9, p0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate;->ModelManufacturerName:Ljava/lang/String;

    .line 159
    iput-object p10, p0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate;->ModelName:Ljava/lang/String;

    .line 160
    iput-object p11, p0, Lcom/microsoft/playready/networkdevice/TransmitterCertificate;->ModelNumber:Ljava/lang/String;

    .line 161
    return-void
.end method
