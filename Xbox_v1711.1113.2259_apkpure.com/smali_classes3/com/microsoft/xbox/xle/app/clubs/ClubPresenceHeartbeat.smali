.class public final enum Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;
.super Ljava/lang/Enum;
.source "ClubPresenceHeartbeat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

.field private static final HEARTBEAT_INTERVAL:J = 0x493e0L

.field public static final enum INSTANCE:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

.field private static final TAG:Ljava/lang/String;

.field private static final THROTTLE_INTERVAL:J = 0xea60L


# instance fields
.field private activeClub:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

.field private activeClubState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

.field private lastReportingTime:J

.field private uploadClubPresenceAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 13
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->INSTANCE:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    .line 12
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->INSTANCE:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->$VALUES:[Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    .line 15
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 19
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->lastReportingTime:J

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->updateReportingTime()V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->stopHeartbeat()V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;
    .param p1, "x1"    # Z

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->uploadUserPresence(Z)V

    return-void
.end method

.method private declared-synchronized shouldHeartbeat()Z
    .locals 1

    .prologue
    .line 45
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->activeClub:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->activeClubState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized stopHeartbeat()V
    .locals 1

    .prologue
    .line 36
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->activeClub:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->activeClubState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38
    monitor-exit p0

    return-void

    .line 36
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized updateReportingTime()V
    .locals 2

    .prologue
    .line 41
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->lastReportingTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 42
    monitor-exit p0

    return-void

    .line 41
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized uploadUserPresence(Z)V
    .locals 8
    .param p1, "immediate"    # Z

    .prologue
    .line 50
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->shouldHeartbeat()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 63
    :goto_0
    monitor-exit p0

    return-void

    .line 54
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->uploadClubPresenceAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;

    if-eqz v0, :cond_1

    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->uploadClubPresenceAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;->cancel()V

    .line 58
    :cond_1
    if-eqz p1, :cond_2

    const-wide/32 v6, 0xea60

    .line 59
    .local v6, "updateInterval":J
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->lastReportingTime:J

    sub-long/2addr v0, v2

    sub-long v0, v6, v0

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 61
    .local v4, "delay":J
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->activeClub:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->activeClubState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;J)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->uploadClubPresenceAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;

    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->uploadClubPresenceAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat$UploadClubPresenceAsyncTask;->load(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 50
    .end local v4    # "delay":J
    .end local v6    # "updateInterval":J
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 58
    :cond_2
    const-wide/32 v6, 0x493e0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->$VALUES:[Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized setClubState(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)V
    .locals 1
    .param p1, "activeClub"    # Lcom/microsoft/xbox/service/model/clubs/ClubModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "activeClubState"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 26
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 27
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->activeClub:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    .line 30
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->activeClubState:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    .line 32
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->uploadUserPresence(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    monitor-exit p0

    return-void

    .line 26
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
