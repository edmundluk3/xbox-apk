.class public final Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;
.super Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
.source "ClubChatChannelId.java"


# instance fields
.field private final clubId:Ljava/lang/String;


# direct methods
.method public constructor <init>(J)V
    .locals 1
    .param p1, "clubId"    # J

    .prologue
    .line 10
    sget-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->Club:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId$ChannelType;->getValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;-><init>(I)V

    .line 11
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;->clubId:Ljava/lang/String;

    .line 12
    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 17
    iget-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;->clubId:Ljava/lang/String;

    return-object v0
.end method
