.class Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$1;
.super Ljava/lang/Object;
.source "ClubSearchScreenAdapter.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;

.field final synthetic val$viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$1;->val$viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 136
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 123
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 127
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 129
    .local v0, "isEmpty":Z
    if-nez v0, :cond_0

    .line 130
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$1;->val$viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->addSuggestionTerm(Ljava/lang/String;)V

    .line 132
    :cond_0
    return-void
.end method
