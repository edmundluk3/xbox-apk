.class public Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "RecentlyPlayedModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/RecentlyPlayedModel$GetRecentlyPlayedRunner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
        ">;>;"
    }
.end annotation


# static fields
.field private static instance:Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;

.field private static final titleHubService:Lcom/microsoft/xbox/service/titleHub/ITitleHubService;


# instance fields
.field private runner:Lcom/microsoft/xbox/service/model/RecentlyPlayedModel$GetRecentlyPlayedRunner;

.field private titleData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getTitleHubService()Lcom/microsoft/xbox/service/titleHub/ITitleHubService;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->titleHubService:Lcom/microsoft/xbox/service/titleHub/ITitleHubService;

    .line 20
    new-instance v0, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->instance:Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    return-void
.end method

.method static synthetic access$100()Lcom/microsoft/xbox/service/titleHub/ITitleHubService;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->titleHubService:Lcom/microsoft/xbox/service/titleHub/ITitleHubService;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->onGetRecentlyPlayedCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->instance:Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;

    return-object v0
.end method

.method private onGetRecentlyPlayedCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;>;"
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/ModelBase;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 45
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->titleData:Ljava/util/List;

    .line 49
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->TitleFeedLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 50
    return-void
.end method


# virtual methods
.method public getRecentTitles()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->titleData:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public loadRecentlyPlayed(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->runner:Lcom/microsoft/xbox/service/model/RecentlyPlayedModel$GetRecentlyPlayedRunner;

    if-nez v0, :cond_0

    .line 36
    new-instance v0, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel$GetRecentlyPlayedRunner;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel$GetRecentlyPlayedRunner;-><init>(Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;Lcom/microsoft/xbox/service/model/RecentlyPlayedModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->runner:Lcom/microsoft/xbox/service/model/RecentlyPlayedModel$GetRecentlyPlayedRunner;

    .line 39
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->TitleFeedLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->runner:Lcom/microsoft/xbox/service/model/RecentlyPlayedModel$GetRecentlyPlayedRunner;

    invoke-virtual {p0, p1, v0, v1}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->loadInternal(ZLcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)V

    .line 40
    return-void
.end method
