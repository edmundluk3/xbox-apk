.class public abstract Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;
.super Ljava/lang/Object;
.source "TournamentNotificationDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "GameTypes"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    new-instance v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract era()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract uwpDesktop()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "uwp_desktop"
    .end annotation
.end method

.method public abstract uwpXboxOne()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "uwp_xboxone"
    .end annotation
.end method
