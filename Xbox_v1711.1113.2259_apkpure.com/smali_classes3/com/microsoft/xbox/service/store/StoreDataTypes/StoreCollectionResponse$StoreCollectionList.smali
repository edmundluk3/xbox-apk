.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;
.super Ljava/lang/Object;
.source "StoreCollectionResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StoreCollectionList"
.end annotation


# instance fields
.field private volatile transient hashCode:I

.field private final items:Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;->items:Ljava/util/List;

    .line 37
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 41
    if-ne p1, p0, :cond_0

    .line 42
    const/4 v1, 0x1

    .line 47
    :goto_0
    return v1

    .line 43
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;

    if-nez v1, :cond_1

    .line 44
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 46
    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;

    .line 47
    .local v0, "other":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;
    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;->items:Ljava/util/List;

    iget-object v2, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;->items:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getItems()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;->items:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;->items:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 69
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 53
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;->hashCode:I

    if-nez v0, :cond_0

    .line 54
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;->hashCode:I

    .line 55
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;->items:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;->hashCode:I

    .line 57
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
