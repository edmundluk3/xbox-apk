.class public Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ClubChatDirectMentionListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter$DirectMentionViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final inflater:Landroid/view/LayoutInflater;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    const v0, 0x7f03006a

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 25
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 26
    return-void
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 30
    const v0, 0x7f03006a

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;

    .line 38
    .local v0, "item":Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;
    if-eqz p2, :cond_0

    .line 39
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter$DirectMentionViewHolder;

    .line 45
    .local v1, "viewHolder":Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter$DirectMentionViewHolder;
    :goto_0
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter$DirectMentionViewHolder;->bindTo(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel$DirectMentionListItem;)V

    .line 46
    return-object p2

    .line 41
    .end local v1    # "viewHolder":Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter$DirectMentionViewHolder;
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;->inflater:Landroid/view/LayoutInflater;

    const v3, 0x7f03006a

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 42
    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter$DirectMentionViewHolder;

    invoke-direct {v1, p0, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter$DirectMentionViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter;Landroid/view/View;)V

    .restart local v1    # "viewHolder":Lcom/microsoft/xbox/xle/app/clubs/ClubChatDirectMentionListAdapter$DirectMentionViewHolder;
    goto :goto_0
.end method
