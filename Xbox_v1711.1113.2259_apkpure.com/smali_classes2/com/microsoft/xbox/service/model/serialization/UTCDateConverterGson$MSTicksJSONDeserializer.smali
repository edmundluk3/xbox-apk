.class public Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$MSTicksJSONDeserializer;
.super Ljava/lang/Object;
.source "UTCDateConverterGson.java"

# interfaces
.implements Lcom/google/gson/JsonDeserializer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MSTicksJSONDeserializer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gson/JsonDeserializer",
        "<",
        "Ljava/util/Date;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/gson/JsonParseException;
        }
    .end annotation

    .prologue
    .line 201
    invoke-virtual {p0, p1, p2, p3}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$MSTicksJSONDeserializer;->deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Ljava/util/Date;
    .locals 14
    .param p1, "json"    # Lcom/google/gson/JsonElement;
    .param p2, "typeOfT"    # Ljava/lang/reflect/Type;
    .param p3, "context"    # Lcom/google/gson/JsonDeserializationContext;

    .prologue
    .line 205
    invoke-virtual {p1}, Lcom/google/gson/JsonElement;->getAsJsonPrimitive()Lcom/google/gson/JsonPrimitive;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/gson/JsonPrimitive;->getAsString()Ljava/lang/String;

    move-result-object v6

    .line 206
    .local v6, "raw":Ljava/lang/String;
    const-string v10, "/Date"

    invoke-virtual {v6, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 207
    invoke-static {v6}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson;->convert(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 235
    :goto_0
    return-object v0

    .line 209
    :cond_0
    const-string v10, "("

    invoke-virtual {v6, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v10

    add-int/lit8 v7, v10, 0x1

    .line 210
    .local v7, "start":I
    const-string v10, ")"

    invoke-virtual {v6, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 211
    .local v1, "end":I
    invoke-virtual {v6, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 213
    .local v4, "ms":Ljava/lang/String;
    const-string v10, "+"

    invoke-virtual {v4, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 214
    .local v3, "middle":I
    const-wide/16 v8, 0x0

    .line 215
    .local v8, "ticks":J
    const/4 v10, -0x1

    if-eq v3, v10, :cond_1

    .line 217
    const/4 v10, 0x0

    invoke-virtual {v4, v10, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 218
    .local v2, "main":Ljava/lang/String;
    add-int/lit8 v10, v3, 0x1

    invoke-virtual {v4, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 219
    .local v5, "offset":Ljava/lang/String;
    const-wide/16 v10, 0x0

    invoke-static {v2, v10, v11}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson;->access$400(Ljava/lang/String;J)J

    move-result-wide v10

    const-wide/16 v12, 0x0

    invoke-static {v5, v12, v13}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson;->access$400(Ljava/lang/String;J)J

    move-result-wide v12

    add-long v8, v10, v12

    .line 220
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v8, v9}, Ljava/util/Date;-><init>(J)V

    goto :goto_0

    .line 223
    .end local v2    # "main":Ljava/lang/String;
    .end local v5    # "offset":Ljava/lang/String;
    :cond_1
    const-string v10, "-"

    invoke-virtual {v4, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 224
    const/4 v10, -0x1

    if-eq v3, v10, :cond_2

    .line 226
    const/4 v10, 0x0

    invoke-virtual {v4, v10, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 227
    .restart local v2    # "main":Ljava/lang/String;
    add-int/lit8 v10, v3, 0x1

    invoke-virtual {v4, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 228
    .restart local v5    # "offset":Ljava/lang/String;
    const-wide/16 v10, 0x0

    invoke-static {v2, v10, v11}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson;->access$400(Ljava/lang/String;J)J

    move-result-wide v10

    const-wide/16 v12, 0x0

    invoke-static {v5, v12, v13}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson;->access$400(Ljava/lang/String;J)J

    move-result-wide v12

    sub-long v8, v10, v12

    .line 229
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v8, v9}, Ljava/util/Date;-><init>(J)V

    goto :goto_0

    .line 232
    .end local v2    # "main":Ljava/lang/String;
    .end local v5    # "offset":Ljava/lang/String;
    :cond_2
    const/4 v0, 0x0

    .line 233
    .local v0, "date":Ljava/util/Date;
    new-instance v0, Ljava/util/Date;

    .end local v0    # "date":Ljava/util/Date;
    const-wide/16 v10, 0x0

    invoke-static {v4, v10, v11}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson;->access$400(Ljava/lang/String;J)J

    move-result-wide v10

    invoke-direct {v0, v10, v11}, Ljava/util/Date;-><init>(J)V

    .line 235
    .restart local v0    # "date":Ljava/util/Date;
    goto :goto_0
.end method
