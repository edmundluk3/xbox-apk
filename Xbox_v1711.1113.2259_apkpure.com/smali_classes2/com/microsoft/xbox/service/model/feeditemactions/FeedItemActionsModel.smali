.class public Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;
.super Ljava/lang/Object;
.source "FeedItemActionsModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;
    }
.end annotation


# static fields
.field public static final CACHE_SIZE:I = 0x80

.field private static final SETTINGS:[Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

.field private static final TAG:Ljava/lang/String;

.field private static final map:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;",
            ">;"
        }
    .end annotation
.end field

.field private static final sanitizationPattern:Ljava/util/regex/Pattern;


# instance fields
.field private final itemRoot:Ljava/lang/String;

.field private final socialActionModels:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    const-class v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->TAG:Ljava/lang/String;

    .line 29
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    const/4 v1, 0x0

    sget-object v2, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->Gamertag:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->RealName:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->AppDisplayPicRaw:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->SETTINGS:[Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    .line 30
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->map:Landroid/util/LruCache;

    .line 31
    const-string v0, "%([,\\+\\(\\-oOxX#sScCdfeEgGaAbBhHnt\\d])+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->sanitizationPattern:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 10
    .param p1, "itemRoot"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->itemRoot:Ljava/lang/String;

    .line 73
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "%s/%s/%%s?maxItems=200"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getCommentsServiceUrl()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v4

    const/4 v8, 0x1

    invoke-static {p1}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->sanitizeFormatterString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, "fmt":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->values()[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    move-result-object v3

    .line 76
    .local v3, "types":[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    array-length v5, v3

    new-array v5, v5, [Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;

    iput-object v5, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->socialActionModels:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;

    .line 77
    const/4 v1, -0x1

    .line 78
    .local v1, "idx":I
    array-length v5, v3

    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v2, v3, v4

    .line 79
    .local v2, "t":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    iget-object v6, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->socialActionModels:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;

    add-int/lit8 v1, v1, 0x1

    new-instance v7, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;

    invoke-direct {v7, v2, v0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;-><init>(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;Ljava/lang/String;)V

    aput-object v7, v6, v1

    .line 78
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 81
    .end local v2    # "t":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    :cond_0
    return-void
.end method

.method static synthetic access$300()[Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->SETTINGS:[Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    return-object v0
.end method

.method static synthetic access$400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static getInstance(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;
    .locals 3
    .param p0, "itemRoot"    # Ljava/lang/String;

    .prologue
    .line 38
    sget-object v2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->map:Landroid/util/LruCache;

    monitor-enter v2

    .line 39
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->map:Landroid/util/LruCache;

    invoke-virtual {v1, p0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;

    .line 40
    .local v0, "model":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;
    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;

    .end local v0    # "model":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;
    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;-><init>(Ljava/lang/String;)V

    .line 42
    .restart local v0    # "model":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;
    sget-object v1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->map:Landroid/util/LruCache;

    invoke-virtual {v1, p0, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    :cond_0
    monitor-exit v2

    return-object v0

    .line 45
    .end local v0    # "model":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static invalidateModels(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)V
    .locals 4
    .param p0, "modelType"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .prologue
    .line 56
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 58
    sget-object v3, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->map:Landroid/util/LruCache;

    monitor-enter v3

    .line 59
    :try_start_0
    sget-object v2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->map:Landroid/util/LruCache;

    invoke-virtual {v2}, Landroid/util/LruCache;->snapshot()Ljava/util/Map;

    move-result-object v1

    .line 60
    .local v1, "snapshot":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;>;"
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;

    .line 62
    .local v0, "m":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->getModel(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;->invalidateData()V

    goto :goto_0

    .line 60
    .end local v0    # "m":Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;
    .end local v1    # "snapshot":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;>;"
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 64
    .restart local v1    # "snapshot":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;>;"
    :cond_0
    return-void
.end method

.method public static reset()V
    .locals 2

    .prologue
    .line 49
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 50
    sget-object v1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->map:Landroid/util/LruCache;

    monitor-enter v1

    .line 51
    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->map:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 52
    monitor-exit v1

    .line 53
    return-void

    .line 52
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static sanitizeFormatterString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 94
    sget-object v2, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->sanitizationPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v2, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 95
    .local v0, "matcher":Ljava/util/regex/Matcher;
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 96
    .local v1, "sb":Ljava/lang/StringBuffer;
    :goto_0
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 97
    const-string v2, "%%$1"

    invoke-virtual {v0, v1, v2}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    goto :goto_0

    .line 99
    :cond_0
    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 100
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public getItemRoot()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->itemRoot:Ljava/lang/String;

    return-object v0
.end method

.method public getModel(Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;
    .locals 2
    .param p1, "modelType"    # Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel;->socialActionModels:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionsModel$SocialActionModel;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method
