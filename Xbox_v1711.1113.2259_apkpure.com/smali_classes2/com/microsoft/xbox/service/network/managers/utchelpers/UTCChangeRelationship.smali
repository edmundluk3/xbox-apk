.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship;
.super Ljava/lang/Object;
.source "UTCChangeRelationship.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;,
        Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$Relationship;
    }
.end annotation


# static fields
.field private static followersData:Lcom/microsoft/xbox/service/model/FollowersData;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship;->followersData:Lcom/microsoft/xbox/service/model/FollowersData;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getGamerType(Lcom/microsoft/xbox/service/model/FollowersData;Ljava/lang/String;Z)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;
    .locals 2
    .param p0, "data"    # Lcom/microsoft/xbox/service/model/FollowersData;
    .param p1, "xuid"    # Ljava/lang/String;
    .param p2, "isFacebook"    # Z

    .prologue
    .line 155
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->NORMAL:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    .line 158
    .local v0, "gamerType":Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;
    if-eqz p0, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    instance-of v1, p0, Lcom/microsoft/xbox/service/model/GameProfileVipData;

    if-nez v1, :cond_0

    instance-of v1, p0, Lcom/microsoft/xbox/service/model/RecommendationsPeopleData;

    if-eqz v1, :cond_1

    .line 159
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->SUGGESTED:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    .line 164
    :cond_1
    if-eqz p2, :cond_2

    .line 165
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->FACEBOOK:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    .line 168
    :cond_2
    return-object v0
.end method

.method public static setFollowersData(Lcom/microsoft/xbox/service/model/FollowersData;)V
    .locals 0
    .param p0, "data"    # Lcom/microsoft/xbox/service/model/FollowersData;

    .prologue
    .line 65
    sput-object p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship;->followersData:Lcom/microsoft/xbox/service/model/FollowersData;

    .line 66
    return-void
.end method

.method public static trackChangeRelationshipAction(Ljava/lang/String;Z)V
    .locals 1
    .param p0, "xuid"    # Ljava/lang/String;
    .param p1, "isFollowing"    # Z

    .prologue
    .line 87
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$2;

    invoke-direct {v0, p1, p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$2;-><init>(ZLjava/lang/String;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 102
    return-void
.end method

.method public static trackChangeRelationshipDone(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;)V
    .locals 1
    .param p0, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "state"    # Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 112
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$3;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$3;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;)V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 129
    return-void
.end method

.method public static trackChangeRelationshipDoneAutoAdd(Ljava/lang/String;Z)V
    .locals 2
    .param p0, "xuid"    # Ljava/lang/String;
    .param p1, "isFacebookFriend"    # Z

    .prologue
    .line 138
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;-><init>()V

    .line 139
    .local v0, "state":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;
    const-string v1, "NotFavorited"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->setCurrentFavoriteStatus(Ljava/lang/String;)V

    .line 140
    const-string v1, "SharingOn"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->setCurrentRealNameStatus(Ljava/lang/String;)V

    .line 141
    const-string v1, "FriendAdd"

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->setCurrentRelationshipStatus(Ljava/lang/String;)V

    .line 142
    if-eqz p1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->FACEBOOK:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    :goto_0
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;->setCurrentGamerType(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;)V

    .line 143
    invoke-static {p0, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship;->trackChangeRelationshipDone(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCChangeRelationshipState;)V

    .line 144
    return-void

    .line 142
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;->SUGGESTED:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$GamerType;

    goto :goto_0
.end method

.method public static trackChangeRelationshipView()V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$1;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCChangeRelationship$1;-><init>()V

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker;->callTrackWrapper(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;)V

    .line 78
    return-void
.end method
