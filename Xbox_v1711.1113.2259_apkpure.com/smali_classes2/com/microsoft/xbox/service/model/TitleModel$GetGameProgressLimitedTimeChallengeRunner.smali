.class Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressLimitedTimeChallengeRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "TitleModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/TitleModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetGameProgressLimitedTimeChallengeRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;",
        ">;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/service/model/TitleModel;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/TitleModel;

.field private titleId:Ljava/lang/String;

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/model/TitleModel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "titleModel"    # Lcom/microsoft/xbox/service/model/TitleModel;
    .param p3, "xuid"    # Ljava/lang/String;
    .param p4, "titleId"    # Ljava/lang/String;

    .prologue
    .line 1006
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressLimitedTimeChallengeRunner;->this$0:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 1007
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressLimitedTimeChallengeRunner;->caller:Lcom/microsoft/xbox/service/model/TitleModel;

    .line 1008
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressLimitedTimeChallengeRunner;->xuid:Ljava/lang/String;

    .line 1009
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressLimitedTimeChallengeRunner;->titleId:Ljava/lang/String;

    .line 1010
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 1014
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressLimitedTimeChallengeRunner;->xuid:Ljava/lang/String;

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressLimitedTimeChallengeRunner;->titleId:Ljava/lang/String;

    invoke-interface {v4, v5, v6}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getGameProgressLimitedTimeChallengeInfo(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    move-result-object v3

    .line 1016
    .local v3, "response":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    if-eqz v3, :cond_2

    iget-object v4, v3, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->achievements:Ljava/util/List;

    if-eqz v4, :cond_2

    .line 1017
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1018
    .local v0, "challengeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;>;"
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070140

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1019
    .local v1, "invalidTime":Ljava/lang/String;
    iget-object v4, v3, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->achievements:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    .line 1020
    .local v2, "item":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->timeWindow:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;

    if-eqz v5, :cond_0

    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->timeWindow:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;

    iget-object v5, v5, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;->endDate:Ljava/util/Date;

    if-eqz v5, :cond_0

    .line 1021
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->timeWindow:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;

    iget-object v5, v5, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;->endDate:Ljava/util/Date;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->dateToTimeRemainingShort(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->progressState:Ljava/lang/String;

    sget-object v6, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    .line 1022
    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1023
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1028
    .end local v2    # "item":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;
    :cond_1
    iput-object v0, v3, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->achievements:Ljava/util/List;

    .line 1031
    .end local v0    # "challengeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;>;"
    .end local v1    # "invalidTime":Ljava/lang/String;
    :cond_2
    return-object v3
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1000
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressLimitedTimeChallengeRunner;->buildData()Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 1045
    const-wide/16 v0, 0xbe0

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1040
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressLimitedTimeChallengeRunner;->caller:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/TitleModel;->access$400(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 1041
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 1036
    return-void
.end method
