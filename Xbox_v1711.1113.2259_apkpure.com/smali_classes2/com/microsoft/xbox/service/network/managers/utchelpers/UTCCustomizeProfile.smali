.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCCustomizeProfile;
.super Ljava/lang/Object;
.source "UTCCustomizeProfile.java"


# static fields
.field private static final BIO_KEY:Ljava/lang/String; = "Bio"

.field private static final GAMER_PIC_KEY:Ljava/lang/String; = "GamerPic"

.field private static final GAMER_TAG_KEY:Ljava/lang/String; = "GamerTag"

.field private static final LOCATION_KEY:Ljava/lang/String; = "Location"

.field private static final PROFILE_COLOR_KEY:Ljava/lang/String; = "ProfileColor"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static trackGamerTagChange()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 43
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 44
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "ProfileColor"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 45
    const-string v1, "GamerPic"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 46
    const-string v1, "Location"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 47
    const-string v1, "Bio"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 48
    const-string v1, "GamerTag"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 50
    const-string v1, "CustomizeProfile - Save Customize Profile"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 51
    return-void
.end method

.method public static trackSaveCustomizeProfile(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p0, "customizeProfileScreenViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;
    .param p1, "location"    # Ljava/lang/String;
    .param p2, "bio"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 22
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getLocation()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    move v3, v6

    .line 23
    .local v3, "hasLocationChanged":Z
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getBio()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    move v0, v6

    .line 24
    .local v0, "hasBioChanged":Z
    :goto_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getCurrentColorObject()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v5

    .line 25
    .local v5, "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    if-nez v5, :cond_3

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfilePreferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v8

    if-eqz v8, :cond_2

    move v1, v6

    .line 26
    .local v1, "hasColorChanged":Z
    :goto_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getCurrentGamerpic()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerPicImageUrl()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_5

    move v2, v6

    .line 28
    .local v2, "hasGamerPicChanged":Z
    :goto_3
    new-instance v4, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v4}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 29
    .local v4, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v9, "ProfileColor"

    if-eqz v1, :cond_6

    move v8, v6

    :goto_4
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v9, v8}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 30
    const-string v9, "GamerPic"

    if-eqz v2, :cond_7

    move v8, v6

    :goto_5
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v9, v8}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 31
    const-string v9, "Location"

    if-eqz v3, :cond_8

    move v8, v6

    :goto_6
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v9, v8}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 32
    const-string v8, "Bio"

    if-eqz v0, :cond_9

    :goto_7
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v8, v6}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 33
    const-string v6, "GamerTag"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 35
    const-string v6, "CustomizeProfile - Save Customize Profile"

    invoke-static {v6, v4}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 36
    return-void

    .end local v0    # "hasBioChanged":Z
    .end local v1    # "hasColorChanged":Z
    .end local v2    # "hasGamerPicChanged":Z
    .end local v3    # "hasLocationChanged":Z
    .end local v4    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    .end local v5    # "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    :cond_0
    move v3, v7

    .line 22
    goto :goto_0

    .restart local v3    # "hasLocationChanged":Z
    :cond_1
    move v0, v7

    .line 23
    goto :goto_1

    .restart local v0    # "hasBioChanged":Z
    .restart local v5    # "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    :cond_2
    move v1, v7

    .line 25
    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfilePreferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    move v1, v6

    goto :goto_2

    :cond_4
    move v1, v7

    goto :goto_2

    .restart local v1    # "hasColorChanged":Z
    :cond_5
    move v2, v7

    .line 26
    goto :goto_3

    .restart local v2    # "hasGamerPicChanged":Z
    .restart local v4    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    :cond_6
    move v8, v7

    .line 29
    goto :goto_4

    :cond_7
    move v8, v7

    .line 30
    goto :goto_5

    :cond_8
    move v8, v7

    .line 31
    goto :goto_6

    :cond_9
    move v6, v7

    .line 32
    goto :goto_7
.end method
