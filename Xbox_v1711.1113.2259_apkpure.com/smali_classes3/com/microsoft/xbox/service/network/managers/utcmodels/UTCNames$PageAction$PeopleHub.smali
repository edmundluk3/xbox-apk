.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$PeopleHub;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PeopleHub"
.end annotation


# static fields
.field public static final AchievementsView:Ljava/lang/String; = "People Hub - Achievements View"

.field public static final ActivityView:Ljava/lang/String; = "People Hub - Activity View"

.field public static final BanFromClub:Ljava/lang/String; = "People Hub - Ban From Club"

.field public static final Block:Ljava/lang/String; = "People Hub - Block"

.field public static final BlockConfirmed:Ljava/lang/String; = "People Hub - Block Confirmed"

.field public static final CapturesView:Ljava/lang/String; = "People Hub - Captures View"

.field public static final CompareFriend:Ljava/lang/String; = "People Hub - Compare with Friends"

.field public static final CompareGame:Ljava/lang/String; = "People Hub - Compare Game"

.field public static final Customize:Ljava/lang/String; = "People Hub - Customize"

.field public static final FilterAchievements:Ljava/lang/String; = "People Hub - Achievements Filter"

.field public static final FilterFriends:Ljava/lang/String; = "People Hub - Friends Filter"

.field public static final FriendFind:Ljava/lang/String; = "Friend Find"

.field public static final FriendsView:Ljava/lang/String; = "People Hub - Friends View"

.field public static final InfoView:Ljava/lang/String; = "People Hub - Info View"

.field public static final InviteToClub:Ljava/lang/String; = "People Hub - Invite To Club"

.field public static final InviteToParty:Ljava/lang/String; = "PeopleHub - Invite to party"

.field public static final JoinParty:Ljava/lang/String; = "PeopleHub - Join party"

.field public static final PeopleGamerProfile:Ljava/lang/String; = "People Gamer Profile"

.field public static final RealNameSharing:Ljava/lang/String; = "People Hub - Open Real Name Sharing Settings"

.field public static final Report:Ljava/lang/String; = "People Hub - Report"

.field public static final SelectClub:Ljava/lang/String; = "People Hub - Select club from list"

.field public static final SelectFriend:Ljava/lang/String; = "People Hub - Select from Friends List"

.field public static final SendMessage:Ljava/lang/String; = "People Hub - Send Message"

.field public static final ShareAchievementsProgress:Ljava/lang/String; = "People Hub - Share Achievements Progress"

.field public static final Unblock:Ljava/lang/String; = "People Hub - Unblock"

.field public static final UseCustomGamerpic:Ljava/lang/String; = "People Hub - Use Custom Gamerpic"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 953
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
