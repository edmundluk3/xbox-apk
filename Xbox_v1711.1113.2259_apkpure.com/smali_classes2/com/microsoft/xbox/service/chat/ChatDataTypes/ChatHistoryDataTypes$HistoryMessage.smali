.class public final Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;
.super Ljava/lang/Object;
.source "ChatHistoryDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "HistoryMessage"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field public final clientSeqNum:I

.field public final flags:I

.field public final message:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private volatile transient messageDate:Ljava/util/Date;

.field public final messageId:J

.field public final messageStatus:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field public final messageTime:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field

.field public final messageType:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final protocolVersion:I

.field public final senderGamertag:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end field

.field public final senderXuid:J
    .annotation build Landroid/support/annotation/IntRange;
        from = 0x1L
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    const-class v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(IILjava/lang/String;JLcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;Ljava/lang/String;Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;ILjava/lang/String;J)V
    .locals 3
    .param p1, "clientSeqNum"    # I
    .param p2, "flags"    # I
    .param p3, "message"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4, "messageId"    # J
    .param p6, "messageStatus"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p7, "messageTime"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p8, "messageType"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p9, "protocolVersion"    # I
    .param p10, "senderGamertag"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p11, "senderXuid"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 129
    invoke-static {p6}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 130
    invoke-static {p7}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 131
    invoke-static {p10}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 132
    const-wide/16 v0, 0x1

    invoke-static {v0, v1, p11, p12}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 134
    iput p1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->clientSeqNum:I

    .line 135
    iput p2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->flags:I

    .line 136
    iput-object p3, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->message:Ljava/lang/String;

    .line 137
    iput-wide p4, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->messageId:J

    .line 138
    iput-object p6, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->messageStatus:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$MessageStatus;

    .line 139
    iput-object p7, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->messageTime:Ljava/lang/String;

    .line 140
    iput-object p8, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->messageType:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .line 141
    iput p9, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->protocolVersion:I

    .line 142
    iput-object p10, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->senderGamertag:Ljava/lang/String;

    .line 143
    iput-wide p11, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->senderXuid:J

    .line 144
    return-void
.end method


# virtual methods
.method public containsLinks()Z
    .locals 2

    .prologue
    .line 160
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->flags:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatFlags;->containsFlag(II)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 165
    if-ne p1, p0, :cond_1

    .line 171
    :cond_0
    :goto_0
    return v1

    .line 167
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;

    if-nez v3, :cond_2

    move v1, v2

    .line 168
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 170
    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;

    .line 171
    .local v0, "other":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;
    iget-wide v4, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->messageId:J

    iget-wide v6, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->messageId:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getMessageDate()Ljava/util/Date;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 148
    iget-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->messageDate:Ljava/util/Date;

    if-nez v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->messageTime:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson;->convert(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->messageDate:Ljava/util/Date;

    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->messageDate:Ljava/util/Date;

    if-nez v0, :cond_0

    .line 151
    sget-object v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not parse message date: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->messageTime:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->messageDate:Ljava/util/Date;

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->messageDate:Ljava/util/Date;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 177
    const/16 v0, 0x11

    .line 178
    .local v0, "result":I
    iget-wide v2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;->messageId:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 180
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 185
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
