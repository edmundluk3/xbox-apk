.class public Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;
.source "GameProfileCapturesScreenAdapter.java"


# instance fields
.field private final filterSpinner:Landroid/widget/Spinner;

.field private final filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;",
            ">;"
        }
    .end annotation
.end field

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;"
        }
    .end annotation
.end field

.field private final listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

.field private final noContentText:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;)V
    .locals 5
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 43
    const v0, 0x7f0e064e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->screenBody:Landroid/view/View;

    .line 44
    const v0, 0x7f0e0650

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 46
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    .line 47
    const v0, 0x7f0e0651

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->setListView(Landroid/support/v7/widget/RecyclerView;)V

    .line 49
    const v0, 0x7f0e064f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    .line 50
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x1090008

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;->values()[Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    const v1, 0x7f03020a

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->setDropDownViewResource(I)V

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->getCapturesFilter()Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 54
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0301d8

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;-><init>(Landroid/content/Context;IZLcom/microsoft/xbox/xle/viewmodel/ILikeControl;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/DividerItemDecoration;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    const v3, 0x7f020163

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/DividerItemDecoration;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 57
    const v0, 0x7f0e0652

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->noContentText:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    .line 58
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    return-object v0
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    return-object v0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 91
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onResume()V

    .line 92
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->restoreListPosition()V

    .line 93
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onStart()V

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 87
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    if-eqz v0, :cond_1

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->cleanupLikeClicks()V

    .line 104
    :cond_1
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onStop()V

    .line 105
    return-void
.end method

.method public updateViewOverride()V
    .locals 5

    .prologue
    .line 109
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->isBusy()Z

    move-result v3

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->updateLoadingIndicator(Z)V

    .line 110
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->getCapturesFilter()Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesFilter;->ordinal()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setSelection(I)V

    .line 112
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->noContentText:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->getNoContentText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->setText(Ljava/lang/String;)V

    .line 114
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->getData()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 115
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->getData()Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->items:Ljava/util/List;

    .line 116
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->clear()V

    .line 118
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->items:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 119
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->items:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 120
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->items:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    invoke-virtual {v4, v3}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->add(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)V

    .line 119
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 123
    .end local v0    # "i":I
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->notifyDataSetChanged()V

    .line 125
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->getLastSelectedCapture()Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    move-result-object v1

    .line 126
    .local v1, "lastSelected":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    if-eqz v1, :cond_2

    .line 127
    const/4 v2, 0x0

    .line 128
    .local v2, "pos":I
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->items:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 129
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->items:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-ne v3, v1, :cond_3

    .line 130
    move v2, v0

    .line 134
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v3, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->scrollToPosition(Landroid/support/v7/widget/RecyclerView;I)V

    .line 138
    .end local v0    # "i":I
    .end local v1    # "lastSelected":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    .end local v2    # "pos":I
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileCapturesScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileCapturesScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 139
    return-void

    .line 128
    .restart local v0    # "i":I
    .restart local v1    # "lastSelected":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    .restart local v2    # "pos":I
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
