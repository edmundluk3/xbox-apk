.class public Lcom/microsoft/xbox/smartglass/MessageTarget;
.super Ljava/lang/Object;
.source "MessageTarget.java"


# instance fields
.field public final service:Lcom/microsoft/xbox/smartglass/ServiceChannel;

.field public final titleId:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "titleId"    # I

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput p1, p0, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    .line 20
    sget-object v0, Lcom/microsoft/xbox/smartglass/ServiceChannel;->None:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/MessageTarget;->service:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    .line 21
    return-void
.end method

.method constructor <init>(II)V
    .locals 1
    .param p1, "titleId"    # I
    .param p2, "service"    # I

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput p1, p0, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    .line 44
    invoke-static {p2}, Lcom/microsoft/xbox/smartglass/ServiceChannel;->fromInt(I)Lcom/microsoft/xbox/smartglass/ServiceChannel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/MessageTarget;->service:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    .line 45
    return-void
.end method

.method public constructor <init>(ILcom/microsoft/xbox/smartglass/ServiceChannel;)V
    .locals 0
    .param p1, "titleId"    # I
    .param p2, "service"    # Lcom/microsoft/xbox/smartglass/ServiceChannel;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput p1, p0, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    .line 39
    iput-object p2, p0, Lcom/microsoft/xbox/smartglass/MessageTarget;->service:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    .line 40
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/smartglass/ServiceChannel;)V
    .locals 1
    .param p1, "service"    # Lcom/microsoft/xbox/smartglass/ServiceChannel;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/MessageTarget;->service:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    .line 30
    return-void
.end method


# virtual methods
.method public equals(Lcom/microsoft/xbox/smartglass/MessageTarget;)Z
    .locals 2
    .param p1, "other"    # Lcom/microsoft/xbox/smartglass/MessageTarget;

    .prologue
    .line 77
    iget v0, p0, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    iget v1, p1, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/MessageTarget;->service:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/MessageTarget;->service:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isService()Z
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/MessageTarget;->service:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    sget-object v1, Lcom/microsoft/xbox/smartglass/ServiceChannel;->None:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTitleId()Z
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValid()Z
    .locals 2

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/MessageTarget;->isTitleId()Z

    move-result v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/MessageTarget;->isService()Z

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
