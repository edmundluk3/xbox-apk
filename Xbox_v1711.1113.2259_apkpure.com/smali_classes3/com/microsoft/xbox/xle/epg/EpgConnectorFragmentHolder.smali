.class public Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;
.super Ljava/lang/Object;
.source "EpgConnectorFragmentHolder.java"


# instance fields
.field private datagram:[C

.field private datagramId:I

.field private datagramSize:I

.field private finished:Z

.field private holeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private ttlTimer:Ljava/util/Timer;


# direct methods
.method public constructor <init>(II)V
    .locals 4
    .param p1, "datagramId"    # I
    .param p2, "datagramSize"    # I

    .prologue
    const/4 v2, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->finished:Z

    .line 24
    iput p1, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->datagramId:I

    .line 25
    iput p2, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->datagramSize:I

    .line 26
    new-array v0, p2, [C

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->datagram:[C

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->holeList:Ljava/util/List;

    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->holeList:Ljava/util/List;

    new-instance v1, Ljava/util/AbstractMap$SimpleEntry;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    add-int/lit8 v3, p2, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/util/AbstractMap$SimpleEntry;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 29
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->ttlTimer:Ljava/util/Timer;

    .line 30
    return-void
.end method

.method private AddToHoleList(II)V
    .locals 11
    .param p1, "fragmentOffset"    # I
    .param p2, "fragmentLength"    # I

    .prologue
    .line 68
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 69
    .local v5, "holesToAdd":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;>;"
    move v2, p1

    .line 70
    .local v2, "fragmentStart":I
    add-int v8, p1, p2

    add-int/lit8 v1, v8, -0x1

    .line 73
    .local v1, "fragmentEnd":I
    iget-object v8, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->holeList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    .line 74
    .local v0, "count":I
    add-int/lit8 v6, v0, -0x1

    .local v6, "i":I
    :goto_0
    if-ltz v6, :cond_3

    .line 75
    iget-object v8, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->holeList:Ljava/util/List;

    invoke-interface {v8, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map$Entry;

    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 76
    .local v4, "holeStart":I
    iget-object v8, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->holeList:Ljava/util/List;

    invoke-interface {v8, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map$Entry;

    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 77
    .local v3, "holeEnd":I
    if-gt v2, v3, :cond_0

    if-ge v1, v4, :cond_1

    .line 74
    :cond_0
    :goto_1
    add-int/lit8 v6, v6, -0x1

    goto :goto_0

    .line 81
    :cond_1
    iget-object v8, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->holeList:Ljava/util/List;

    invoke-interface {v8, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 82
    if-le v2, v4, :cond_2

    .line 83
    new-instance v8, Ljava/util/AbstractMap$SimpleEntry;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    add-int/lit8 v10, v2, -0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Ljava/util/AbstractMap$SimpleEntry;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    :cond_2
    if-ge v1, v3, :cond_0

    .line 87
    new-instance v8, Ljava/util/AbstractMap$SimpleEntry;

    add-int/lit8 v9, v1, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Ljava/util/AbstractMap$SimpleEntry;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 92
    .end local v3    # "holeEnd":I
    .end local v4    # "holeStart":I
    :cond_3
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    .line 93
    .local v7, "kvp":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v9, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->holeList:Ljava/util/List;

    invoke-interface {v9, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 96
    .end local v7    # "kvp":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_4
    iget-object v8, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->holeList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-nez v8, :cond_5

    const/4 v8, 0x1

    :goto_3
    iput-boolean v8, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->finished:Z

    .line 97
    return-void

    .line 96
    :cond_5
    const/4 v8, 0x0

    goto :goto_3
.end method


# virtual methods
.method public AddBytes(IILjava/lang/String;)V
    .locals 3
    .param p1, "fragmentOffset"    # I
    .param p2, "fragmentLength"    # I
    .param p3, "fragmentData"    # Ljava/lang/String;

    .prologue
    .line 58
    add-int v0, p1, p2

    iget v1, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->datagramSize:I

    if-gt v0, v1, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-eq v0, p2, :cond_1

    .line 59
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Fragment data size mismatch!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->datagram:[C

    invoke-virtual {p3, v0, v1, v2, p1}, Ljava/lang/String;->getChars(II[CI)V

    .line 63
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->AddToHoleList(II)V

    .line 64
    return-void
.end method

.method public GetDataIfReady()Ljava/lang/String;
    .locals 4

    .prologue
    .line 45
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->finished:Z

    if-eqz v2, :cond_0

    .line 46
    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->datagram:[C

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([C)V

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 48
    .local v0, "data":[B
    :try_start_0
    new-instance v2, Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-direct {v2, v0, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    .end local v0    # "data":[B
    :goto_0
    return-object v2

    .line 49
    .restart local v0    # "data":[B
    :catch_0
    move-exception v1

    .line 50
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    const-string v2, "EpgConnectorFragmentHolder"

    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .end local v0    # "data":[B
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getDatagramId()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->datagramId:I

    return v0
.end method

.method public getDatagramSize()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->datagramSize:I

    return v0
.end method

.method public getDatagramTimer()Ljava/util/Timer;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentHolder;->ttlTimer:Ljava/util/Timer;

    return-object v0
.end method
