.class public final Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "LfgInterestedListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "LfgInterestedViewHolder"
.end annotation


# instance fields
.field private approveButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private final gameStat:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private final gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private final gamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private interestedText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private moreButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private popup:Landroid/widget/PopupMenu;

.field private rejectButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;Landroid/view/View;)V
    .locals 3
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;

    .line 83
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 85
    const v0, 0x7f0e076d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 86
    const v0, 0x7f0e076e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->gamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 87
    const v0, 0x7f0e076f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->gameStat:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 88
    const v0, 0x7f0e0751

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->interestedText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 89
    const v0, 0x7f0e0752

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->approveButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 90
    const v0, 0x7f0e0753

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->rejectButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 91
    const v0, 0x7f0e0754

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->moreButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 92
    new-instance v0, Landroid/widget/PopupMenu;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->moreButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-direct {v0, v1, v2}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->popup:Landroid/widget/PopupMenu;

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->popup:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0009

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->popup:Landroid/widget/PopupMenu;

    invoke-virtual {v2}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->moreButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    return-void
.end method

.method static synthetic lambda$bindTo$1(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->access$600(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->access$500(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->getAdapterPosition()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$bindTo$2(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->access$400(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->access$300(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->getAdapterPosition()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$bindTo$3(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;Landroid/view/MenuItem;)Z
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "member"    # Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v4, 0x1

    .line 140
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0e0c30

    if-ne v1, v2, :cond_0

    .line 141
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 142
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->member:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedProfile(Ljava/lang/String;)V

    .line 144
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->member:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->getXuid()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg;->trackViewProfile(Ljava/lang/String;)V

    .line 146
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    invoke-virtual {v1, v2, v4, v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 153
    .end local v0    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :goto_0
    return v4

    .line 147
    :cond_0
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0e0c31

    if-ne v1, v2, :cond_1

    .line 148
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->access$200(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->getAdapterPosition()I

    move-result v3

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    goto :goto_0

    .line 150
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown menu item "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->popup:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    return-void
.end method


# virtual methods
.method public bindTo(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;)V
    .locals 9
    .param p1, "member"    # Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v6, 0x7f0201fa

    const/4 v8, 0x0

    .line 98
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 100
    iget-object v4, p1, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->member:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->properties()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberProperties;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p1, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->member:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    .line 101
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->properties()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberProperties;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberProperties;->system()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystem;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p1, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->member:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    .line 102
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->properties()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberProperties;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberProperties;->system()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystem;->description()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 103
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->interestedText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v5, p1, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->member:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->properties()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberProperties;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberProperties;->system()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystem;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMemberPropertiesSystem;->description()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;->text()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 106
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v5, p1, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->memberSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v5, v5, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayPicRaw:Ljava/lang/String;

    invoke-virtual {v4, v5, v6, v6}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 107
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->gamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v5, p1, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->member:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerMember;->gamertag()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 109
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->getTimePlayed()Ljava/lang/Integer;

    move-result-object v3

    .line 110
    .local v3, "timePlayed":Ljava/lang/Integer;
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;->getHeroStats()Ljava/util/List;

    move-result-object v0

    .line 112
    .local v0, "heroStats":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;>;"
    if-nez v3, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 113
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->gameStat:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 136
    :goto_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->approveButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->rejectButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->popup:Landroid/widget/PopupMenu;

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedMember;)Landroid/widget/PopupMenu$OnMenuItemClickListener;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 156
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->approveButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setEnabled(Z)V

    .line 157
    return-void

    .line 115
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->gameStat:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v4, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 117
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 119
    .local v1, "statBuilder":Ljava/lang/StringBuilder;
    if-eqz v3, :cond_2

    .line 120
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f0706d0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    sget-object v4, Lcom/microsoft/xbox/toolkit/JavaUtil;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .line 125
    .local v2, "statistic":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 126
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->groupproperties:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;

    iget-object v5, v5, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;->DisplayName:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    const-string v5, ": "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    iget-object v5, v2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    sget-object v5, Lcom/microsoft/xbox/toolkit/JavaUtil;->LINE_SEPARATOR:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 133
    .end local v2    # "statistic":Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    :cond_4
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/LfgInterestedListAdapter$LfgInterestedViewHolder;->gameStat:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method
