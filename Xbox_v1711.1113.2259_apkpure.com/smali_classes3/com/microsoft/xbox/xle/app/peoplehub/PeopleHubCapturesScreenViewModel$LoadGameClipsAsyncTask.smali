.class Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "PeopleHubCapturesScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadGameClipsAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;)V
    .locals 0

    .prologue
    .line 345
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$1;

    .prologue
    .line 345
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 349
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 350
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/TrendingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TrendingModel;->getData()Lcom/microsoft/xbox/service/activityHub/ActivityHubDataTypes$TrendingResponseList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/TrendingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TrendingModel;->shouldRefresh()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3

    .prologue
    .line 366
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;)Lcom/microsoft/xbox/service/model/TrendingModel;

    move-result-object v1

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;->forceLoad:Z

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/TrendingModel;->loadSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 368
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 369
    invoke-static {}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->access$400()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Unable to get Game Clips"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    :cond_0
    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 345
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 361
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 345
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 355
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 356
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 357
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 384
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->access$502(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;Z)Z

    .line 385
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 386
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 345
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 377
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 378
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->access$502(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;Z)Z

    .line 379
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel$LoadGameClipsAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;->access$600(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubCapturesScreenViewModel;)V

    .line 380
    return-void
.end method
