.class Lcom/microsoft/xbox/service/model/ProfileModel$GetActivityFeedItemLikeRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProfileModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetActivityFeedItemLikeRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;",
        ">;"
    }
.end annotation


# instance fields
.field private item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V
    .locals 0
    .param p2, "item"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .prologue
    .line 4295
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetActivityFeedItemLikeRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 4296
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetActivityFeedItemLikeRunner;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 4297
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4274
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 4275
    .local v1, "likeUrlWrapper":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetActivityFeedItemLikeRunner;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-eqz v4, :cond_0

    .line 4276
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetActivityFeedItemLikeRunner;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v4, v4, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->itemRoot:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4279
    const/4 v2, 0x0

    .line 4280
    .local v2, "meLikeResult":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    new-instance v4, Lcom/microsoft/xbox/service/model/sls/CommentsServiceBatchRequest;

    invoke-direct {v4, v1}, Lcom/microsoft/xbox/service/model/sls/CommentsServiceBatchRequest;-><init>(Ljava/util/List;)V

    invoke-static {v4}, Lcom/microsoft/xbox/service/model/sls/CommentsServiceBatchRequest;->getCommentsServiceBatchRequestBody(Lcom/microsoft/xbox/service/model/sls/CommentsServiceBatchRequest;)Ljava/lang/String;

    move-result-object v3

    .line 4282
    .local v3, "postBody":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v4

    invoke-interface {v4, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getMeLikeInfo(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;

    move-result-object v2

    .line 4285
    iget-object v4, v2, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;->likes:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 4286
    iget-object v4, v2, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;->likes:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4292
    .end local v2    # "meLikeResult":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    .end local v3    # "postBody":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 4288
    .restart local v2    # "meLikeResult":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    .restart local v3    # "postBody":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 4289
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "GetActivityFeedItemLikeRunner"

    const-string v5, "Activity Feed Item Like Request Failed"

    invoke-static {v4, v5, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 4292
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "meLikeResult":Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$MeLikeResult;
    .end local v3    # "postBody":Ljava/lang/String;
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4266
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$GetActivityFeedItemLikeRunner;->buildData()Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 4302
    const-wide/16 v0, 0xd1c

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 4311
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/MeLikeResultContainer$Like;>;"
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 4307
    return-void
.end method
