.class public abstract Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBERequestBody;
.super Ljava/lang/Object;
.source "OOBESessionDataType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "OOBERequestBody"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBERequestBody;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    new-instance v0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBERequestBody$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBERequestBody$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;)Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBERequestBody;
    .locals 1
    .param p0, "consoleSettings"    # Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 103
    new-instance v0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBERequestBody;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBERequestBody;-><init>(Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;)V

    return-object v0
.end method


# virtual methods
.method public abstract consoleSettings()Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ConsoleSettings"
    .end annotation
.end method
