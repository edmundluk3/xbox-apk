.class Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$1;
.super Ljava/lang/Object;
.source "EditTextDialog.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->access$000(Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$1;->this$0:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->access$200(Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;)I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->access$300(Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;Landroid/widget/TextView;Landroid/widget/TextView;I)V

    .line 114
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 105
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 109
    return-void
.end method
