.class public final enum Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;
.super Ljava/lang/Enum;
.source "PrivacySettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/privacy/PrivacySettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PermissionSetting"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;

.field public static final enum CommunicateUsingText:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 85
    new-instance v0, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;

    const-string v1, "CommunicateUsingText"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;->CommunicateUsingText:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;

    .line 84
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;

    sget-object v1, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;->CommunicateUsingText:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;->$VALUES:[Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 84
    const-class v0, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;->$VALUES:[Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;

    return-object v0
.end method
