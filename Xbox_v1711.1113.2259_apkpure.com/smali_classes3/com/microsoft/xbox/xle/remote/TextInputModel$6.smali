.class Lcom/microsoft/xbox/xle/remote/TextInputModel$6;
.super Ljava/lang/Object;
.source "TextInputModel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/remote/TextInputModel;->onConfigurationChanged(Lcom/microsoft/xbox/smartglass/TextConfiguration;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/remote/TextInputModel;

.field final synthetic val$configuration:Lcom/microsoft/xbox/smartglass/TextConfiguration;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/remote/TextInputModel;Lcom/microsoft/xbox/smartglass/TextConfiguration;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/remote/TextInputModel;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel$6;->this$0:Lcom/microsoft/xbox/xle/remote/TextInputModel;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel$6;->val$configuration:Lcom/microsoft/xbox/smartglass/TextConfiguration;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel$6;->this$0:Lcom/microsoft/xbox/xle/remote/TextInputModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/remote/TextInputModel;->access$100(Lcom/microsoft/xbox/xle/remote/TextInputModel;)Lcom/microsoft/xbox/smartglass/TextManagerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel$6;->this$0:Lcom/microsoft/xbox/xle/remote/TextInputModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/remote/TextInputModel;->access$100(Lcom/microsoft/xbox/xle/remote/TextInputModel;)Lcom/microsoft/xbox/smartglass/TextManagerListener;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/TextInputModel$6;->val$configuration:Lcom/microsoft/xbox/smartglass/TextConfiguration;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/smartglass/TextManagerListener;->onConfigurationChanged(Lcom/microsoft/xbox/smartglass/TextConfiguration;)V

    .line 156
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    instance-of v0, v0, Lcom/microsoft/xbox/xle/app/activity/CanvasWebViewActivity;

    if-nez v0, :cond_1

    .line 157
    invoke-static {}, Lcom/microsoft/xbox/xle/remote/TextInputControl;->launchIfNotRunning()V

    .line 159
    :cond_1
    return-void
.end method
