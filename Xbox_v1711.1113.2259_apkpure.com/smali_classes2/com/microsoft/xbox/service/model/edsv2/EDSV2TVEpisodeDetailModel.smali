.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;
.super Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
.source "EDSV2TVEpisodeDetailModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;",
        ">;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 0
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 9
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 10
    return-void
.end method


# virtual methods
.method protected bridge synthetic createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;->createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    move-result-object v0

    return-object v0
.end method

.method protected createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;
    .locals 1
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 14
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    return-object v0
.end method

.method public geSeasonCanonicalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->getSeasonCanonicalId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEpisodeNumber()I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->getEpisodeNumber()I

    move-result v0

    return v0
.end method

.method public getLaunchType()Lcom/microsoft/xbox/service/model/LaunchType;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/microsoft/xbox/service/model/LaunchType;->AppLaunchType:Lcom/microsoft/xbox/service/model/LaunchType;

    return-object v0
.end method

.method public getMediaGroup()I
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x4

    return v0
.end method

.method public getNetwork()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->getNetwork()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParentSeason()Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->getParentSeason()Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeasonMediaItem;

    move-result-object v0

    return-object v0
.end method

.method public getParentSeries()Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->getParentSeries()Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    move-result-object v0

    return-object v0
.end method

.method protected getRelatedCanonicalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->getSeriesCanonicalId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getRelatedMediaType()I
    .locals 1

    .prologue
    .line 65
    const/16 v0, 0x3ec

    return v0
.end method

.method public getSeasonNumber()I
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->getSeasonNumber()I

    move-result v0

    return v0
.end method

.method public getSeasonTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->getSeasonTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSeriesCanonicalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->getSeriesCanonicalId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSeriesTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVEpisodeMediaItem;->getSeriesTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitleType()Lcom/microsoft/xbox/service/model/JTitleType;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/microsoft/xbox/service/model/JTitleType;->Application:Lcom/microsoft/xbox/service/model/JTitleType;

    return-object v0
.end method

.method public shouldGetProviderActivities()Z
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x1

    return v0
.end method
