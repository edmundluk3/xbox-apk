var authTokens = new Array();
var authRequestInProgress;
function navigateToAuthMessage(audienceUri, navRetry)
{		
	if (authRequestInProgress == 1)
	{
		if (navRetry < 50)
		{
			setTimeout(function () { navigateToAuthMessage(audienceUri, navRetry + 1); }, 200);
		}
		
		return;
	}
	
	authRequestInProgress = 1;
	window.location.href = 'purchase://title?audienceUri=' + audienceUri;
}
function navigateToDoneMessage(status)
{
    window.location.href = 'purchase://title?status=' + status;
}
function installWebBlendMessageHandler()
{
    window.addEventListener('message', webBlendMessageHandler, false);
}
function getTokenForAudienceUri(audienceUri)
{
    navigateToAuthMessage(audienceUri, 0);
}
function authenticateResponse(data, evt, retry)
{
	if (typeof authTokens[data.relyingParty] == 'undefined')
    {    	
        if (retry == 0)
        {
            getTokenForAudienceUri(data.relyingParty);
        }        
        if (retry < 50)
        {
            setTimeout(function () { authenticateResponse(data, evt, retry + 1); }, 200);
        }
        return;
    }
    
	console.log('RELYING PARTY ' + data.relyingParty);
    console.log('TOKEN REQUEST ' + authTokens[data.relyingParty]);
    var authResponse = JSON.stringify({ message: 'authResponse', flowId: data.flowId, relyingParty: data.relyingParty, auth: authTokens[data.relyingParty] });
    authRequestInProgress = 0;
    evt.source.postMessage(authResponse, '*');
}
function webBlendMessageHandler(event)
{
    var data = JSON.parse(event.data);
    if (data.message === 'authRequest')
    {    	
        console.log('REQUEST RELYING PARTY ' + data.relyingParty);
        authenticateResponse(data, event, 0);
    }
    else if (data.message === 'done')
    {
        navigateToDoneMessage(data.status)
    }
    else if (data.message === 'error')
    {
    	console.log('data.message = ' + data.message);
    	console.log('data.source = ' + data.source);
    	console.log('data.code = ' + data.code);
    	navigateToDoneMessage(data.message);
    }
    else
    {
    	console.log('data.message = ' + data.message);
    }
}