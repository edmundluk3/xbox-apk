.class Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;
.super Ljava/lang/Object;
.source "GcmModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/gcm/GcmModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Registration"
.end annotation


# instance fields
.field public final edfNodeId:Ljava/lang/String;

.field public final edfRegId:Ljava/lang/String;

.field public final endpointId:Ljava/lang/String;

.field public final flags:I

.field public final registrationId:Ljava/lang/String;

.field public final skypeRegToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7
    .param p1, "registrationId"    # Ljava/lang/String;
    .param p2, "endpointId"    # Ljava/lang/String;
    .param p3, "flags"    # I

    .prologue
    const/4 v4, 0x0

    .line 750
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "registrationId"    # Ljava/lang/String;
    .param p2, "endpointId"    # Ljava/lang/String;
    .param p3, "flags"    # I
    .param p4, "edfRegId"    # Ljava/lang/String;
    .param p5, "edfNodeId"    # Ljava/lang/String;
    .param p6, "skypeRegToken"    # Ljava/lang/String;

    .prologue
    .line 753
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 754
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->registrationId:Ljava/lang/String;

    .line 755
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->endpointId:Ljava/lang/String;

    .line 756
    iput p3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->flags:I

    .line 757
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->edfRegId:Ljava/lang/String;

    .line 758
    iput-object p5, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->edfNodeId:Ljava/lang/String;

    .line 759
    iput-object p6, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->skypeRegToken:Ljava/lang/String;

    .line 760
    return-void
.end method


# virtual methods
.method public isEDFRegistered()Z
    .locals 1

    .prologue
    .line 771
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->edfNodeId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->edfRegId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGCMRegistered()Z
    .locals 1

    .prologue
    .line 763
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->registrationId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSLSRegistered()Z
    .locals 1

    .prologue
    .line 767
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->endpointId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
