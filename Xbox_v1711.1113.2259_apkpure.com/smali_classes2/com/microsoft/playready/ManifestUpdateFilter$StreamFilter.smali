.class Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;
.super Ljava/lang/Object;
.source "ManifestUpdateFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/ManifestUpdateFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StreamFilter"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/playready/IStreamUpdateListener;",
            ">;"
        }
    .end annotation
.end field

.field private mFirstFragmentStartTimeInUs:J

.field private mFragmentCount:J

.field private mMediaProxy:Lcom/microsoft/playready/MediaProxy;

.field private mStreamIndex:I

.field final synthetic this$0:Lcom/microsoft/playready/ManifestUpdateFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/microsoft/playready/ManifestUpdateFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/playready/ManifestUpdateFilter;Lcom/microsoft/playready/MediaStream;Lcom/microsoft/playready/MediaProxy;)V
    .locals 2
    .param p2, "mediaStream"    # Lcom/microsoft/playready/MediaStream;
    .param p3, "mediaProxy"    # Lcom/microsoft/playready/MediaProxy;

    .prologue
    const-wide/16 v0, 0x0

    .line 34
    iput-object p1, p0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->this$0:Lcom/microsoft/playready/ManifestUpdateFilter;

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-wide v0, p0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->mFirstFragmentStartTimeInUs:J

    .line 28
    iput-wide v0, p0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->mFragmentCount:J

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->listeners:Ljava/util/List;

    .line 35
    iput-object p3, p0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    .line 36
    invoke-virtual {p2}, Lcom/microsoft/playready/MediaStream;->getStreamIndex()I

    move-result v0

    iput v0, p0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->mStreamIndex:I

    .line 38
    invoke-virtual {p2}, Lcom/microsoft/playready/MediaStream;->getFragmentCount()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->mFragmentCount:J

    .line 39
    iget-object v0, p0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    iget v1, p0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->mStreamIndex:I

    invoke-direct {p0, v0, v1}, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->_findFirstFragmentStartTimeInUs(Lcom/microsoft/playready/MediaProxy;I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->mFirstFragmentStartTimeInUs:J

    .line 41
    return-void
.end method

.method private _findFirstFragmentStartTimeInUs(Lcom/microsoft/playready/MediaProxy;I)J
    .locals 6
    .param p1, "mediaProxy"    # Lcom/microsoft/playready/MediaProxy;
    .param p2, "streamIndex"    # I

    .prologue
    .line 69
    const-wide/16 v2, 0x0

    .line 76
    .local v2, "firstFragmentStartTimeInUs":J
    const-wide/16 v4, 0x0

    :try_start_0
    invoke-virtual {p1, p2, v4, v5}, Lcom/microsoft/playready/MediaProxy;->getFragmentIndexAtTime(IJ)I

    move-result v0

    .line 77
    .local v0, "firstFragmentIndex":I
    invoke-virtual {p1, p2, v0}, Lcom/microsoft/playready/MediaProxy;->getFragmentInfo(II)Lcom/microsoft/playready/FragmentIterator$FragmentInfo;

    move-result-object v1

    .line 79
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v4}, Lcom/microsoft/playready/FragmentIterator$FragmentInfo;->getStartTime(Ljava/util/concurrent/TimeUnit;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 82
    .end local v0    # "firstFragmentIndex":I
    :goto_0
    return-wide v2

    .line 80
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public addStreamUpdateListener(Lcom/microsoft/playready/IStreamUpdateListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/playready/IStreamUpdateListener;

    .prologue
    .line 45
    monitor-enter p0

    .line 47
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    monitor-exit p0

    .line 49
    return-void

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public hasListeners()Z
    .locals 1

    .prologue
    .line 61
    monitor-enter p0

    .line 63
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->listeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onUpdateStreamInformation(Lcom/microsoft/playready/MediaStream;)V
    .locals 11
    .param p1, "mediaStream"    # Lcom/microsoft/playready/MediaStream;

    .prologue
    .line 87
    sget-boolean v8, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->$assertionsDisabled:Z

    if-nez v8, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/playready/MediaStream;->getStreamIndex()I

    move-result v8

    iget v9, p0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->mStreamIndex:I

    if-eq v8, v9, :cond_0

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 89
    :cond_0
    const-wide/16 v4, 0x0

    .line 90
    .local v4, "newFirstFragmentStartTimeInUs":J
    const-wide/16 v6, 0x0

    .line 91
    .local v6, "newFragmentCount":J
    const/4 v1, 0x0

    .line 92
    .local v1, "fragmentsHaveChanged":Z
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 94
    .local v3, "listenerCopy":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/playready/IStreamUpdateListener;>;"
    monitor-enter p0

    .line 96
    :try_start_0
    invoke-virtual {p1}, Lcom/microsoft/playready/MediaStream;->getFragmentCount()J

    move-result-wide v6

    .line 97
    iget-wide v8, p0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->mFragmentCount:J

    cmp-long v8, v6, v8

    if-eqz v8, :cond_4

    .line 99
    const/4 v1, 0x1

    .line 114
    :cond_1
    :goto_0
    if-eqz v1, :cond_2

    .line 116
    iget-object v8, p0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->listeners:Ljava/util/List;

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 94
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    if-eqz v1, :cond_3

    .line 128
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_5

    .line 140
    iput-wide v6, p0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->mFragmentCount:J

    .line 141
    iput-wide v4, p0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->mFirstFragmentStartTimeInUs:J

    .line 151
    :cond_3
    return-void

    .line 107
    :cond_4
    :try_start_1
    iget-object v8, p0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->mMediaProxy:Lcom/microsoft/playready/MediaProxy;

    iget v9, p0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->mStreamIndex:I

    invoke-direct {p0, v8, v9}, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->_findFirstFragmentStartTimeInUs(Lcom/microsoft/playready/MediaProxy;I)J

    move-result-wide v4

    .line 108
    iget-wide v8, p0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->mFirstFragmentStartTimeInUs:J

    cmp-long v8, v4, v8

    if-eqz v8, :cond_1

    .line 110
    const/4 v1, 0x1

    goto :goto_0

    .line 94
    :catchall_0
    move-exception v8

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v8

    .line 128
    :cond_5
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/playready/IStreamUpdateListener;

    .line 132
    .local v2, "listener":Lcom/microsoft/playready/IStreamUpdateListener;
    :try_start_2
    invoke-interface {v2, p1}, Lcom/microsoft/playready/IStreamUpdateListener;->onStreamUpdate(Lcom/microsoft/playready/MediaStream;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 134
    :catch_0
    move-exception v0

    .line 136
    .local v0, "e":Ljava/lang/Exception;
    const-string v9, "StreamFilter"

    const-string v10, "Uncaught exception thrown during onStreamUpdate callback"

    invoke-static {v9, v10, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public removeStreamUpdateListener(Lcom/microsoft/playready/IStreamUpdateListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/playready/IStreamUpdateListener;

    .prologue
    .line 53
    monitor-enter p0

    .line 55
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/ManifestUpdateFilter$StreamFilter;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 53
    monitor-exit p0

    .line 57
    return-void

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
