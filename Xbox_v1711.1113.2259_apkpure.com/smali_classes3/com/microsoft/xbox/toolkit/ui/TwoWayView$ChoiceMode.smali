.class public final enum Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;
.super Ljava/lang/Enum;
.source "TwoWayView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ChoiceMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

.field public static final enum MULTIPLE:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

.field public static final enum NONE:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

.field public static final enum SINGLE:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 143
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->NONE:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    const-string v1, "SINGLE"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->SINGLE:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    const-string v1, "MULTIPLE"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->MULTIPLE:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    .line 142
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->NONE:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->SINGLE:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->MULTIPLE:Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->$VALUES:[Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 142
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 142
    const-class v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;
    .locals 1

    .prologue
    .line 142
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->$VALUES:[Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/toolkit/ui/TwoWayView$ChoiceMode;

    return-object v0
.end method
