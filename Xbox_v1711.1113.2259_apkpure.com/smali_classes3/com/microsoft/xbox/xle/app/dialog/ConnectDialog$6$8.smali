.class Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$8;
.super Ljava/lang/Object;
.source "ConnectDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->bindConsoleRow(Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;Lcom/microsoft/xbox/xle/model/ConsoleData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;

.field final synthetic val$console:Lcom/microsoft/xbox/xle/model/ConsoleData;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;Lcom/microsoft/xbox/xle/model/ConsoleData;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;

    .prologue
    .line 555
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$8;->this$1:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$8;->val$console:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 559
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Connection - Forget Xbox"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$8;->this$1:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$700(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$8;->val$console:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->removeDiscoveredConsole(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    .line 561
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$8;->this$1:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$800(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Landroid/widget/BaseAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 562
    return-void
.end method
