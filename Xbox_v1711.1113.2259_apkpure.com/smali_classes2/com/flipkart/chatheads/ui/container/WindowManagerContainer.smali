.class public Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;
.super Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;
.source "WindowManagerContainer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flipkart/chatheads/ui/container/WindowManagerContainer$MotionCaptureView;,
        Lcom/flipkart/chatheads/ui/container/WindowManagerContainer$MotionCapturingTouchListener;
    }
.end annotation


# instance fields
.field private cachedHeight:I

.field private cachedWidth:I

.field private currentArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

.field private motionCaptureView:Landroid/view/View;

.field private motionCaptureViewAdded:Z

.field private windowManager:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;-><init>(Landroid/content/Context;)V

    .line 49
    return-void
.end method

.method private removeContainer(Landroid/view/View;)V
    .locals 1
    .param p1, "motionCaptureView"    # Landroid/view/View;

    .prologue
    .line 242
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->windowManager:Landroid/view/WindowManager;

    invoke-interface {v0, p1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 243
    return-void
.end method


# virtual methods
.method public addContainer(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;)V
    .locals 1
    .param p1, "container"    # Landroid/view/View;
    .param p2, "containerLayoutParams"    # Landroid/view/WindowManager$LayoutParams;

    .prologue
    .line 149
    invoke-virtual {p1, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 150
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 151
    return-void
.end method

.method public addContainer(Landroid/view/View;Z)V
    .locals 1
    .param p1, "container"    # Landroid/view/View;
    .param p2, "focusable"    # Z

    .prologue
    .line 144
    invoke-virtual {p0, p2}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->createContainerLayoutParams(Z)Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 145
    .local v0, "containerLayoutParams":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {p0, p1, v0}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->addContainer(Landroid/view/View;Landroid/view/WindowManager$LayoutParams;)V

    .line 146
    return-void
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "layoutParams"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 221
    invoke-super {p0, p1, p2}, Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 222
    iget-boolean v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->motionCaptureViewAdded:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getManager()Lcom/flipkart/chatheads/ui/ChatHeadManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getChatHeads()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 223
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->motionCaptureView:Landroid/view/View;

    invoke-virtual {p0, v1, v3}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->addContainer(Landroid/view/View;Z)V

    .line 224
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->motionCaptureView:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getOrCreateLayoutParamsForContainer(Landroid/view/View;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 225
    .local v0, "motionCaptureParams":Landroid/view/WindowManager$LayoutParams;
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 226
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 227
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->windowManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->motionCaptureView:Landroid/view/View;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 228
    iput-boolean v3, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->motionCaptureViewAdded:Z

    .line 230
    .end local v0    # "motionCaptureParams":Landroid/view/WindowManager$LayoutParams;
    :cond_0
    return-void
.end method

.method protected createContainerLayoutParams(Z)Landroid/view/WindowManager$LayoutParams;
    .locals 7
    .param p1, "focusable"    # Z

    .prologue
    const/4 v6, 0x0

    const/4 v1, -0x1

    .line 126
    if-eqz p1, :cond_0

    .line 127
    const/16 v4, 0x20

    .line 132
    .local v4, "focusableFlag":I
    :goto_0
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7d2

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    .line 136
    .local v0, "layoutParams":Landroid/view/WindowManager$LayoutParams;
    iput v6, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 137
    iput v6, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 138
    const v1, 0x800033

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 139
    return-object v0

    .line 129
    .end local v0    # "layoutParams":Landroid/view/WindowManager$LayoutParams;
    .end local v4    # "focusableFlag":I
    :cond_0
    const/16 v4, 0x18

    .restart local v4    # "focusableFlag":I
    goto :goto_0
.end method

.method public destroy()V
    .locals 2

    .prologue
    .line 246
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->windowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->motionCaptureView:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 247
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->windowManager:Landroid/view/WindowManager;

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getFrameLayout()Lcom/flipkart/chatheads/ui/HostFrameLayout;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 248
    return-void
.end method

.method protected getContainerX(Landroid/view/View;)I
    .locals 2
    .param p1, "container"    # Landroid/view/View;

    .prologue
    .line 108
    invoke-virtual {p0, p1}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getOrCreateLayoutParamsForContainer(Landroid/view/View;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 109
    .local v0, "layoutParams":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    return v1
.end method

.method protected getContainerY(Landroid/view/View;)I
    .locals 2
    .param p1, "container"    # Landroid/view/View;

    .prologue
    .line 120
    invoke-virtual {p0, p1}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getOrCreateLayoutParamsForContainer(Landroid/view/View;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 121
    .local v0, "layoutParams":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    return v1
.end method

.method protected getOrCreateLayoutParamsForContainer(Landroid/view/View;)Landroid/view/WindowManager$LayoutParams;
    .locals 2
    .param p1, "container"    # Landroid/view/View;

    .prologue
    .line 93
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    .line 94
    .local v0, "layoutParams":Landroid/view/WindowManager$LayoutParams;
    if-nez v0, :cond_0

    .line 95
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->createContainerLayoutParams(Z)Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 96
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 98
    :cond_0
    return-object v0
.end method

.method public getWindowManager()Landroid/view/WindowManager;
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->windowManager:Landroid/view/WindowManager;

    if-nez v0, :cond_0

    .line 75
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->windowManager:Landroid/view/WindowManager;

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->windowManager:Landroid/view/WindowManager;

    return-object v0
.end method

.method public onArrangementChanged(Lcom/flipkart/chatheads/ui/ChatHeadArrangement;Lcom/flipkart/chatheads/ui/ChatHeadArrangement;)V
    .locals 4
    .param p1, "oldArrangement"    # Lcom/flipkart/chatheads/ui/ChatHeadArrangement;
    .param p2, "newArrangement"    # Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    .prologue
    const/4 v3, 0x0

    .line 179
    iput-object p2, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->currentArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    .line 181
    instance-of v1, p1, Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    if-eqz v1, :cond_1

    instance-of v1, p2, Lcom/flipkart/chatheads/ui/MaximizedArrangement;

    if-eqz v1, :cond_1

    .line 184
    iget-boolean v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->motionCaptureViewAdded:Z

    if-eqz v1, :cond_0

    .line 185
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->motionCaptureView:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getOrCreateLayoutParamsForContainer(Landroid/view/View;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 186
    .local v0, "layoutParams":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v1, v1, 0x18

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 187
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->windowManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->motionCaptureView:Landroid/view/View;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 190
    .end local v0    # "layoutParams":Landroid/view/WindowManager$LayoutParams;
    :cond_0
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getFrameLayout()Lcom/flipkart/chatheads/ui/HostFrameLayout;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getOrCreateLayoutParamsForContainer(Landroid/view/View;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 191
    .restart local v0    # "layoutParams":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit8 v1, v1, -0x9

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 192
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit8 v1, v1, -0x11

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 193
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v1, v1, 0x20

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 195
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->windowManager:Landroid/view/WindowManager;

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getFrameLayout()Lcom/flipkart/chatheads/ui/HostFrameLayout;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 197
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->motionCaptureView:Landroid/view/View;

    invoke-virtual {p0, v1, v3}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->setContainerX(Landroid/view/View;I)V

    .line 198
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->motionCaptureView:Landroid/view/View;

    invoke-virtual {p0, v1, v3}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->setContainerY(Landroid/view/View;I)V

    .line 199
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->motionCaptureView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getFrameLayout()Lcom/flipkart/chatheads/ui/HostFrameLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flipkart/chatheads/ui/HostFrameLayout;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->setContainerWidth(Landroid/view/View;I)V

    .line 200
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->motionCaptureView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getFrameLayout()Lcom/flipkart/chatheads/ui/HostFrameLayout;

    move-result-object v2

    invoke-virtual {v2}, Lcom/flipkart/chatheads/ui/HostFrameLayout;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->setContainerHeight(Landroid/view/View;I)V

    .line 217
    :goto_0
    return-void

    .line 205
    .end local v0    # "layoutParams":Landroid/view/WindowManager$LayoutParams;
    :cond_1
    iget-boolean v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->motionCaptureViewAdded:Z

    if-eqz v1, :cond_2

    .line 206
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->motionCaptureView:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getOrCreateLayoutParamsForContainer(Landroid/view/View;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 207
    .restart local v0    # "layoutParams":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 208
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit8 v1, v1, -0x11

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 209
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v1, v1, 0x20

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 210
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->windowManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->motionCaptureView:Landroid/view/View;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 213
    .end local v0    # "layoutParams":Landroid/view/WindowManager$LayoutParams;
    :cond_2
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getFrameLayout()Lcom/flipkart/chatheads/ui/HostFrameLayout;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getOrCreateLayoutParamsForContainer(Landroid/view/View;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 214
    .restart local v0    # "layoutParams":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v1, v1, 0x18

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 215
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->windowManager:Landroid/view/WindowManager;

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getFrameLayout()Lcom/flipkart/chatheads/ui/HostFrameLayout;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public onInitialized(Lcom/flipkart/chatheads/ui/ChatHeadManager;)V
    .locals 3
    .param p1, "manager"    # Lcom/flipkart/chatheads/ui/ChatHeadManager;

    .prologue
    .line 53
    invoke-super {p0, p1}, Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;->onInitialized(Lcom/flipkart/chatheads/ui/ChatHeadManager;)V

    .line 54
    new-instance v1, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer$MotionCaptureView;

    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer$MotionCaptureView;-><init>(Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->motionCaptureView:Landroid/view/View;

    .line 56
    new-instance v0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer$MotionCapturingTouchListener;

    invoke-direct {v0, p0}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer$MotionCapturingTouchListener;-><init>(Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;)V

    .line 57
    .local v0, "listener":Lcom/flipkart/chatheads/ui/container/WindowManagerContainer$MotionCapturingTouchListener;
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->motionCaptureView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 58
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->registerReceiver(Landroid/content/Context;)V

    .line 59
    return-void
.end method

.method public registerReceiver(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 62
    new-instance v0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer$1;

    invoke-direct {v0, p0}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer$1;-><init>(Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;)V

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 71
    return-void
.end method

.method public removeView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 234
    invoke-super {p0, p1}, Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;->removeView(Landroid/view/View;)V

    .line 235
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getManager()Lcom/flipkart/chatheads/ui/ChatHeadManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/flipkart/chatheads/ui/ChatHeadManager;->getChatHeads()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->windowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->motionCaptureView:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 237
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->motionCaptureViewAdded:Z

    .line 239
    :cond_0
    return-void
.end method

.method protected setContainerHeight(Landroid/view/View;I)V
    .locals 2
    .param p1, "container"    # Landroid/view/View;
    .param p2, "height"    # I

    .prologue
    .line 81
    invoke-virtual {p0, p1}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getOrCreateLayoutParamsForContainer(Landroid/view/View;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 82
    .local v0, "layoutParams":Landroid/view/WindowManager$LayoutParams;
    iput p2, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 83
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 84
    return-void
.end method

.method protected setContainerWidth(Landroid/view/View;I)V
    .locals 2
    .param p1, "container"    # Landroid/view/View;
    .param p2, "width"    # I

    .prologue
    .line 87
    invoke-virtual {p0, p1}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getOrCreateLayoutParamsForContainer(Landroid/view/View;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 88
    .local v0, "layoutParams":Landroid/view/WindowManager$LayoutParams;
    iput p2, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 89
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 90
    return-void
.end method

.method protected setContainerX(Landroid/view/View;I)V
    .locals 2
    .param p1, "container"    # Landroid/view/View;
    .param p2, "xPosition"    # I

    .prologue
    .line 102
    invoke-virtual {p0, p1}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getOrCreateLayoutParamsForContainer(Landroid/view/View;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 103
    .local v0, "layoutParams":Landroid/view/WindowManager$LayoutParams;
    iput p2, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 104
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 105
    return-void
.end method

.method protected setContainerY(Landroid/view/View;I)V
    .locals 2
    .param p1, "container"    # Landroid/view/View;
    .param p2, "yPosition"    # I

    .prologue
    .line 114
    invoke-virtual {p0, p1}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getOrCreateLayoutParamsForContainer(Landroid/view/View;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 115
    .local v0, "layoutParams":Landroid/view/WindowManager$LayoutParams;
    iput p2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 116
    invoke-virtual {p0}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 117
    return-void
.end method

.method public setViewX(Landroid/view/View;I)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "xPosition"    # I

    .prologue
    .line 155
    invoke-super {p0, p1, p2}, Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;->setViewX(Landroid/view/View;I)V

    .line 156
    instance-of v1, p1, Lcom/flipkart/chatheads/ui/ChatHead;

    if-eqz v1, :cond_0

    move-object v1, p1

    .line 157
    check-cast v1, Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v1}, Lcom/flipkart/chatheads/ui/ChatHead;->isHero()Z

    move-result v0

    .line 158
    .local v0, "hero":Z
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->currentArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    instance-of v1, v1, Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    if-eqz v1, :cond_0

    .line 159
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->motionCaptureView:Landroid/view/View;

    invoke-virtual {p0, v1, p2}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->setContainerX(Landroid/view/View;I)V

    .line 160
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->motionCaptureView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->setContainerWidth(Landroid/view/View;I)V

    .line 163
    .end local v0    # "hero":Z
    :cond_0
    return-void
.end method

.method public setViewY(Landroid/view/View;I)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "yPosition"    # I

    .prologue
    .line 167
    invoke-super {p0, p1, p2}, Lcom/flipkart/chatheads/ui/FrameChatHeadContainer;->setViewY(Landroid/view/View;I)V

    .line 168
    instance-of v1, p1, Lcom/flipkart/chatheads/ui/ChatHead;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->currentArrangement:Lcom/flipkart/chatheads/ui/ChatHeadArrangement;

    instance-of v1, v1, Lcom/flipkart/chatheads/ui/MinimizedArrangement;

    if-eqz v1, :cond_0

    move-object v1, p1

    .line 169
    check-cast v1, Lcom/flipkart/chatheads/ui/ChatHead;

    invoke-virtual {v1}, Lcom/flipkart/chatheads/ui/ChatHead;->isHero()Z

    move-result v0

    .line 170
    .local v0, "hero":Z
    if-eqz v0, :cond_0

    .line 171
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->motionCaptureView:Landroid/view/View;

    invoke-virtual {p0, v1, p2}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->setContainerY(Landroid/view/View;I)V

    .line 172
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->motionCaptureView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->setContainerHeight(Landroid/view/View;I)V

    .line 175
    .end local v0    # "hero":Z
    :cond_0
    return-void
.end method
