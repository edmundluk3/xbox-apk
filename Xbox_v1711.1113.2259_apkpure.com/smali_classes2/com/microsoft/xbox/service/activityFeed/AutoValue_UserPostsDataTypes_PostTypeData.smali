.class final Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData;
.super Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;
.source "AutoValue_UserPostsDataTypes_PostTypeData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/activityFeed/AutoValue_UserPostsDataTypes_PostTypeData$GsonTypeAdapter;
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;Ljava/util/Date;Ljava/util/Date;)V
    .locals 0
    .param p1, "locator"    # Ljava/lang/String;
    .param p2, "hostXuid"    # Ljava/lang/String;
    .param p3, "titleId"    # Ljava/lang/String;
    .param p4, "scid"    # Ljava/lang/String;
    .param p5, "sessionId"    # Ljava/lang/String;
    .param p6, "description"    # Ljava/lang/String;
    .param p9, "startTime"    # Ljava/util/Date;
    .param p10, "postedTime"    # Ljava/util/Date;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Date;",
            "Ljava/util/Date;",
            ")V"
        }
    .end annotation

    .prologue
    .line 19
    .local p7, "tags":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    .local p8, "achievementIds":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Ljava/lang/String;>;"
    invoke-direct/range {p0 .. p10}, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_UserPostsDataTypes_PostTypeData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;Ljava/util/Date;Ljava/util/Date;)V

    .line 20
    return-void
.end method
