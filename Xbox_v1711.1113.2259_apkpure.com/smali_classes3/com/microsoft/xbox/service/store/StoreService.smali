.class public final enum Lcom/microsoft/xbox/service/store/StoreService;
.super Ljava/lang/Enum;
.source "StoreService.java"

# interfaces
.implements Lcom/microsoft/xbox/service/store/IStoreService;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/store/StoreService;",
        ">;",
        "Lcom/microsoft/xbox/service/store/IStoreService;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/store/StoreService;

.field private static final AUTO_SUGGEST_ENDPOINT:Ljava/lang/String; = "https://displaycatalog.mp.microsoft.com/v7.0/productFamilies/autosuggest?query=%1$s&productFamilyNames=Games&market=%2$s&languages=%3$s&platformdependencyname=windows.xbox"

.field private static final COLLECTIONS_INSTALL_ENDPOINT:Ljava/lang/String; = "https://collections.mp.microsoft.com/v7.0/beneficiaries/me/install"

.field private static final COLLECTIONS_QUERY_ENDPOINT:Ljava/lang/String; = "https://collections.mp.microsoft.com/v7.0/collections/query"

.field private static final CONTENT_RATING_ENDPOINT:Ljava/lang/String; = "https://displaycatalog.md.mp.microsoft.com/v7.0/ratings?market=%s&languages=%s"

.field private static final CORRELATION_VECTOR_KEY:Ljava/lang/String; = "MS-CV"

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/store/StoreService;

.field private static final LOCALIZED_GAME_PROPERTY_ENDPOINT:Ljava/lang/String; = "https://displaycatalog.mp.microsoft.com/v7/productFamilies/games?market=%s&languages=%s"

.field private static final PRODUCTS_FROM_IDS_ENDPOINT:Ljava/lang/String; = "https://displaycatalog.mp.microsoft.com/v7.0/products/?fieldsTemplate=%s&bigIds=%s&market=%s&languages=%s"

.field private static final PRODUCT_ADDON_LOOK_UP_ENDPOINT:Ljava/lang/String; = "https://reco-public.rec.mp.microsoft.com/channels/Reco/V8.0/Lists/Mapping/addonsbyparentwithdetails/%s?ItemTypes=Consumable,Durable&Market=%s&Language=%s&deviceFamily=Windows.Xbox&count=10"

.field private static final PRODUCT_ID_LOOK_UP_ENDPOINT:Ljava/lang/String; = "https://displaycatalog.md.mp.microsoft.com/v7.0/products/lookup?market=%1$s&languages=%2$s&alternateId=%4$s&value=%3$s&fieldsTemplate=details"

.field private static final PRODUCT_IN_BUNDLE_LIST_ENDPOINT:Ljava/lang/String; = "https://reco-public.rec.mp.microsoft.com/channels/reco/v8.0/Lists/Mapping/BundlesBySeed/%s?itemTypes=Game,Consumable,Durable&DeviceFamily=Windows.Xbox&count=%d&skipitems=%d&market=%s&language=%s"

.field private static final PRODUCT_RELATED_ITEM_LIST_ENDPOINT:Ljava/lang/String; = "https://reco-public.rec.mp.microsoft.com/channels/reco/v8.0/related/Game/%s?itemTypes=Game&DeviceFamily=Windows.Xbox&count=10&market=%s&language=%s"

.field private static final RECOMMENDATION_ADDON_LIST_ENDPOINT:Ljava/lang/String; = "https://reco-public.rec.mp.microsoft.com/channels/reco/v8.0/lists/computed/%s?itemTypes=Durable&DeviceFamily=Windows.Xbox&count=%d&clientType=XboxApp&market=%s&PreferredLanguages=%s&skipItems=%d"

.field private static final RECOMMENDATION_APP_LIST_ENDPOINT:Ljava/lang/String; = "https://reco-public.rec.mp.microsoft.com/channels/Reco/V8.0/Lists/Collection/%s?ItemTypes=Apps&Market=%s&Language=%s&deviceFamily=Windows.Xbox&count=%d&skipItems=%d"

.field private static final RECOMMENDATION_GOLD_LIST_ENDPOINT:Ljava/lang/String; = "https://reco-public.rec.mp.microsoft.com/channels/reco/v8.0/lists/collection/%s?ItemTypes=Game&Market=%s&PreferredLanguages=%s&deviceFamily=Windows.Xbox&uid=%d&count=%d&skipItems=%d"

.field private static final RECOMMENDATION_LIST_ENDPOINT:Ljava/lang/String; = "https://reco-public.rec.mp.microsoft.com/channels/reco/v8.0/lists/computed/%s?itemTypes=game&DeviceFamily=Windows.Xbox&count=%d&clientType=XboxApp&market=%s&preferredlanguages=%s&skipItems=%d"

.field private static final SEARCH_ENDPOINT:Ljava/lang/String; = "https://displaycatalog.mp.microsoft.com/v7.0/productFamilies/%1$s/products?query=%2$s&$top=%6$d&market=%3$s&languages=%4$s&$skip=%5$d&fieldsTemplate=details&platformdependencyname=windows.xbox"

.field private static final SEARCH_TYPES:Ljava/lang/String; = "Games"

.field private static final STATIC_HEADERS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 46
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreService;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/store/StoreService;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreService;->INSTANCE:Lcom/microsoft/xbox/service/store/StoreService;

    .line 44
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/store/StoreService;

    sget-object v1, Lcom/microsoft/xbox/service/store/StoreService;->INSTANCE:Lcom/microsoft/xbox/service/store/StoreService;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreService;->$VALUES:[Lcom/microsoft/xbox/service/store/StoreService;

    .line 48
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreService;->INSTANCE:Lcom/microsoft/xbox/service/store/StoreService;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreService;->TAG:Ljava/lang/String;

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreService;->STATIC_HEADERS:Ljava/util/List;

    .line 95
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Content-type"

    const-string v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/store/StoreService;->STATIC_HEADERS:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->addStaticSLSHeaders(Ljava/util/List;)V

    .line 97
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static getHeaderWithCVValue()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation

    .prologue
    .line 610
    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lcom/microsoft/xbox/service/store/StoreService;->STATIC_HEADERS:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 611
    .local v0, "header":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "MS-CV"

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCCorrelationVector;->increment()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 613
    return-object v0
.end method

.method private getProductFromAlternateId(Ljava/lang/String;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    .locals 11
    .param p1, "productId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "altIdType"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 374
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 376
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 379
    const/4 v0, 0x0

    .line 381
    .local v0, "itemDetail":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    sget-object v6, Lcom/microsoft/xbox/service/store/StoreService$1;->$SwitchMap$com$microsoft$xbox$service$store$StoreDataTypes$StoreItemDetailResponse$AlternateIdType:[I

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 398
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 399
    new-instance v4, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-direct {v4, v6, v5}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;-><init>(Ljava/util/List;Z)V

    .line 416
    :goto_1
    return-object v4

    .line 383
    :pswitch_0
    sget-object v6, Lcom/microsoft/xbox/service/store/StoreServiceCacheManager;->INSTANCE:Lcom/microsoft/xbox/service/store/StoreServiceCacheManager;

    sget-object v7, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;->Details:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;

    invoke-virtual {v6, p1, v7}, Lcom/microsoft/xbox/service/store/StoreServiceCacheManager;->getCachedItemByLegacyProductId(Ljava/lang/String;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;

    move-result-object v0

    .line 384
    goto :goto_0

    .line 387
    :pswitch_1
    invoke-static {p1, v8, v9}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseLong(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 388
    .local v2, "titleId":Ljava/lang/Long;
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v6, v6, v8

    if-lez v6, :cond_0

    .line 389
    sget-object v6, Lcom/microsoft/xbox/service/store/StoreServiceCacheManager;->INSTANCE:Lcom/microsoft/xbox/service/store/StoreServiceCacheManager;

    sget-object v7, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;->Details:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;

    invoke-virtual {v6, v2, v7}, Lcom/microsoft/xbox/service/store/StoreServiceCacheManager;->getCachedItemByTitleId(Ljava/lang/Long;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;

    move-result-object v0

    goto :goto_0

    .line 394
    .end local v2    # "titleId":Ljava/lang/Long;
    :pswitch_2
    sget-object v6, Lcom/microsoft/xbox/service/store/StoreServiceCacheManager;->INSTANCE:Lcom/microsoft/xbox/service/store/StoreServiceCacheManager;

    sget-object v7, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;->Details:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;

    invoke-virtual {v6, p1, v7}, Lcom/microsoft/xbox/service/store/StoreServiceCacheManager;->getCachedItemByPackageFamilyName(Ljava/lang/String;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;

    move-result-object v0

    goto :goto_0

    .line 402
    :cond_1
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "https://displaycatalog.md.mp.microsoft.com/v7.0/products/lookup?market=%1$s&languages=%2$s&alternateId=%4$s&value=%3$s&fieldsTemplate=details"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    .line 405
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v5

    .line 406
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v4

    const/4 v9, 0x2

    aput-object p1, v8, v9

    const/4 v9, 0x3

    .line 408
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    .line 402
    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 411
    .local v3, "url":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/store/StoreService;->getHeaderWithCVValue()Ljava/util/List;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newGetInstance(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v6

    const-class v7, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    .line 410
    invoke-static {v6, v7}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    .line 416
    .local v1, "productsList":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    if-nez v1, :cond_2

    const/4 v4, 0x0

    goto :goto_1

    :cond_2
    new-instance v6, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    sget-object v7, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;->Details:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;

    .line 417
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->getProducts()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-le v8, v4, :cond_3

    :goto_2
    invoke-static {v1, v7, v4}, Lcom/microsoft/xbox/service/store/StoreService;->getXboxItemsAndSaveInCache(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;Z)Ljava/util/List;

    move-result-object v4

    iget-boolean v5, v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->hasMorePages:Z

    invoke-direct {v6, v4, v5}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;-><init>(Ljava/util/List;Z)V

    move-object v4, v6

    goto/16 :goto_1

    :cond_3
    move v4, v5

    goto :goto_2

    .line 381
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getStoreRecommendationAddonList(ILjava/lang/String;I)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    .locals 6
    .param p1, "count"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "typeString"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "skipCount"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 201
    const-wide/16 v2, 0x1

    int-to-long v4, p1

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 202
    const-wide/16 v2, 0x0

    int-to-long v4, p3

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 203
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 205
    sget-object v1, Lcom/microsoft/xbox/service/store/StoreService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getStoreRecommendationAddonList of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 208
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "https://reco-public.rec.mp.microsoft.com/channels/reco/v8.0/lists/computed/%s?itemTypes=Durable&DeviceFamily=Windows.Xbox&count=%d&clientType=XboxApp&market=%s&PreferredLanguages=%s&skipItems=%d"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    .line 212
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    .line 213
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    .line 214
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    .line 215
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 208
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 217
    .local v0, "url":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/service/store/StoreService;->STATIC_HEADERS:Ljava/util/List;

    .line 218
    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newGetInstance(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    .line 217
    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    return-object v1
.end method

.method private getStoreRecommendationAppList(ILjava/lang/String;I)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    .locals 6
    .param p1, "count"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "typeString"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "skipCount"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 238
    const-wide/16 v2, 0x1

    int-to-long v4, p1

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 239
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 240
    const-wide/16 v2, 0x0

    int-to-long v4, p3

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 242
    sget-object v1, Lcom/microsoft/xbox/service/store/StoreService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getStoreRecommendationAppList of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 245
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "https://reco-public.rec.mp.microsoft.com/channels/Reco/V8.0/Lists/Collection/%s?ItemTypes=Apps&Market=%s&Language=%s&deviceFamily=Windows.Xbox&count=%d&skipItems=%d"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    .line 249
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    .line 250
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    .line 251
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    .line 252
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 245
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 254
    .local v0, "url":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/service/store/StoreService;->STATIC_HEADERS:Ljava/util/List;

    .line 255
    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newGetInstance(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    .line 254
    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    return-object v1
.end method

.method private getStoreRecommendationList(ILjava/lang/String;I)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    .locals 6
    .param p1, "count"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "typeString"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "skipCount"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 157
    const-wide/16 v2, 0x1

    int-to-long v4, p1

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 158
    const-wide/16 v2, 0x0

    int-to-long v4, p3

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 159
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 162
    sget-object v1, Lcom/microsoft/xbox/service/store/StoreService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getStoreRecommendationList of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 165
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "https://reco-public.rec.mp.microsoft.com/channels/reco/v8.0/lists/computed/%s?itemTypes=game&DeviceFamily=Windows.Xbox&count=%d&clientType=XboxApp&market=%s&preferredlanguages=%s&skipItems=%d"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    .line 169
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    .line 170
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    .line 171
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    .line 172
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 165
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 174
    .local v0, "url":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/service/store/StoreService;->STATIC_HEADERS:Ljava/util/List;

    .line 175
    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newGetInstance(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    .line 174
    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    return-object v1
.end method

.method private getStoreRecommendationListWithGold(ILjava/lang/String;II)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    .locals 8
    .param p1, "count"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "typeString"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "puid"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p4, "skipCount"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    .line 325
    const-wide/16 v2, 0x1

    int-to-long v4, p1

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 326
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 327
    int-to-long v2, p3

    invoke-static {v6, v7, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 328
    int-to-long v2, p4

    invoke-static {v6, v7, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 330
    sget-object v1, Lcom/microsoft/xbox/service/store/StoreService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getStoreRecommendationListWithGold of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 333
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "https://reco-public.rec.mp.microsoft.com/channels/reco/v8.0/lists/collection/%s?ItemTypes=Game&Market=%s&PreferredLanguages=%s&deviceFamily=Windows.Xbox&uid=%d&count=%d&skipItems=%d"

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    .line 337
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    .line 338
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    .line 339
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    .line 340
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    .line 341
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 333
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 343
    .local v0, "url":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/service/store/StoreService;->STATIC_HEADERS:Ljava/util/List;

    .line 344
    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newGetInstance(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    .line 343
    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    return-object v1
.end method

.method private static getXboxItemsAndSaveInCache(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;Z)Ljava/util/List;
    .locals 5
    .param p0, "productsList"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "fieldTemplateType"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "shouldCheckPlatform"    # Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;",
            ">;"
        }
    .end annotation

    .prologue
    .line 620
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 621
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 623
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 624
    .local v1, "xboxItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->getProducts()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;

    .line 625
    .local v0, "storeItemDetail":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    if-eqz p2, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->isXboxProduct()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 627
    :cond_1
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->setTemplateType(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;)V

    .line 628
    sget-object v3, Lcom/microsoft/xbox/service/store/StoreServiceCacheManager;->INSTANCE:Lcom/microsoft/xbox/service/store/StoreServiceCacheManager;

    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->productId:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Lcom/microsoft/xbox/service/store/StoreServiceCacheManager;->put(Ljava/lang/String;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)Lcom/microsoft/xbox/service/store/StoreServiceCacheManager;

    .line 629
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 632
    .end local v0    # "storeItemDetail":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    :cond_2
    return-object v1
.end method

.method private hydrateProductsFromId(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    .locals 15
    .param p1, "fieldTemplate"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "url"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 501
    .local p2, "productIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static/range {p2 .. p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 503
    sget-object v11, Lcom/microsoft/xbox/service/store/StoreService;->TAG:Ljava/lang/String;

    const-string v12, "getProductsFromId"

    invoke-static {v11, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 506
    const/4 v3, 0x0

    .line 507
    .local v3, "hasMorePages":Z
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 508
    .local v1, "cachedProducts":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;>;"
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 510
    .local v10, "searchIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 511
    .local v8, "productId":Ljava/lang/String;
    sget-object v12, Lcom/microsoft/xbox/service/store/StoreServiceCacheManager;->INSTANCE:Lcom/microsoft/xbox/service/store/StoreServiceCacheManager;

    move-object/from16 v0, p1

    invoke-virtual {v12, v8, v0}, Lcom/microsoft/xbox/service/store/StoreServiceCacheManager;->get(Ljava/lang/String;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;

    move-result-object v6

    .line 512
    .local v6, "itemDetail":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    if-eqz v6, :cond_0

    .line 514
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 517
    :cond_0
    invoke-interface {v10, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 521
    .end local v6    # "itemDetail":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    .end local v8    # "productId":Ljava/lang/String;
    :cond_1
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v11

    if-lez v11, :cond_3

    .line 523
    const-string v11, ","

    invoke-static {v11, v10}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    .line 524
    .local v4, "idString":Ljava/lang/String;
    sget-object v11, Lcom/microsoft/xbox/service/store/StoreService;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "   - (product ids: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ")"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v12, 0x4

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    .line 529
    invoke-virtual/range {p1 .. p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    aput-object v4, v12, v13

    const/4 v13, 0x2

    .line 531
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v14

    invoke-virtual {v14}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x3

    .line 532
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v14

    invoke-virtual {v14}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    .line 526
    move-object/from16 v0, p3

    invoke-static {v11, v0, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 535
    .local v2, "formattedUrl":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/store/StoreService;->getHeaderWithCVValue()Ljava/util/List;

    move-result-object v11

    invoke-static {v2, v11}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newGetInstance(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v11

    const-class v12, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    .line 534
    invoke-static {v11, v12}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    .line 538
    .local v9, "productsList":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    if-eqz v9, :cond_3

    .line 539
    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-static {v9, v0, v11}, Lcom/microsoft/xbox/service/store/StoreService;->getXboxItemsAndSaveInCache(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;Z)Ljava/util/List;

    move-result-object v7

    .line 540
    .local v7, "items":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;>;"
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;

    .line 541
    .local v5, "item":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 544
    .end local v5    # "item":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    :cond_2
    invoke-static/range {p2 .. p2}, Lcom/microsoft/xbox/service/store/StoreService$$Lambda$1;->lambdaFactory$(Ljava/util/List;)Ljava/util/Comparator;

    move-result-object v11

    invoke-static {v1, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 545
    iget-boolean v3, v9, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->hasMorePages:Z

    .line 549
    .end local v2    # "formattedUrl":Ljava/lang/String;
    .end local v4    # "idString":Ljava/lang/String;
    .end local v7    # "items":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;>;"
    .end local v9    # "productsList":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    :cond_3
    new-instance v11, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    invoke-direct {v11, v1, v3}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;-><init>(Ljava/util/List;Z)V

    return-object v11
.end method

.method static synthetic lambda$hydrateProductsFromId$0(Ljava/util/List;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)I
    .locals 2
    .param p0, "productIds"    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "lhs"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    .param p2, "rhs"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;

    .prologue
    .line 544
    iget-object v0, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->productId:Ljava/lang/String;

    invoke-interface {p0, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iget-object v1, p2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->productId:Ljava/lang/String;

    invoke-interface {p0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private static urlWithMarketAndLanguage(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 602
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 605
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 606
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 602
    invoke-static {v0, p0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreService;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 44
    const-class v0, Lcom/microsoft/xbox/service/store/StoreService;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreService;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/store/StoreService;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreService;->$VALUES:[Lcom/microsoft/xbox/service/store/StoreService;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/store/StoreService;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/store/StoreService;

    return-object v0
.end method


# virtual methods
.method public getComingSoonList(II)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    .locals 1
    .param p1, "count"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "skipCount"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 150
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GamesComingSoon:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/microsoft/xbox/service/store/StoreService;->getStoreRecommendationList(ILjava/lang/String;I)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    return-object v0
.end method

.method public getGamepassList(Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;III)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    .locals 1
    .param p1, "storeListType"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "count"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "puid"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p4, "skipCount"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 274
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 275
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0, p3, p4}, Lcom/microsoft/xbox/service/store/StoreService;->getStoreRecommendationListWithGold(ILjava/lang/String;II)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    return-object v0
.end method

.method public getGoldDealList(III)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    .locals 1
    .param p1, "count"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "puid"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p3, "skipCount"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 262
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GoldDeals:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/microsoft/xbox/service/store/StoreService;->getStoreRecommendationListWithGold(ILjava/lang/String;II)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    return-object v0
.end method

.method public getGoldGameList(III)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    .locals 1
    .param p1, "count"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "puid"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p3, "skipCount"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 268
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GoldGame:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/microsoft/xbox/service/store/StoreService;->getStoreRecommendationListWithGold(ILjava/lang/String;II)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    return-object v0
.end method

.method public getLocalizedGameProperty()Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedGamePropertyResponse;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 567
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 569
    const-string v0, "https://displaycatalog.mp.microsoft.com/v7/productFamilies/games?market=%s&languages=%s"

    .line 571
    invoke-static {v0}, Lcom/microsoft/xbox/service/store/StoreService;->urlWithMarketAndLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 572
    invoke-static {}, Lcom/microsoft/xbox/service/store/StoreService;->getHeaderWithCVValue()Ljava/util/List;

    move-result-object v1

    .line 570
    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newGetInstance(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedGamePropertyResponse;

    .line 569
    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedGamePropertyResponse;

    return-object v0
.end method

.method public getMostPlayedList(II)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    .locals 1
    .param p1, "count"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "skipCount"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 138
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GamesMostPlayed:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/microsoft/xbox/service/store/StoreService;->getStoreRecommendationList(ILjava/lang/String;I)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    return-object v0
.end method

.method public getNewAddonList(II)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    .locals 1
    .param p1, "count"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "skipCount"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 182
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->AddonsNew:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/microsoft/xbox/service/store/StoreService;->getStoreRecommendationAddonList(ILjava/lang/String;I)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    return-object v0
.end method

.method public getNewAppList(II)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    .locals 1
    .param p1, "count"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "skipCount"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 225
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->AppsNew:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/microsoft/xbox/service/store/StoreService;->getStoreRecommendationAppList(ILjava/lang/String;I)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    return-object v0
.end method

.method public getPopularAppList(II)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    .locals 1
    .param p1, "count"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "skipCount"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 231
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->AppsListsPopularOnXbox:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/microsoft/xbox/service/store/StoreService;->getStoreRecommendationAppList(ILjava/lang/String;I)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    return-object v0
.end method

.method public getProductAddOns(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    .locals 6
    .param p1, "productId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 424
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 425
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 427
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "https://reco-public.rec.mp.microsoft.com/channels/Reco/V8.0/Lists/Mapping/addonsbyparentwithdetails/%s?ItemTypes=Consumable,Durable&Market=%s&Language=%s&deviceFamily=Windows.Xbox&count=10"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    .line 431
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    .line 432
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 427
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 434
    .local v0, "url":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/service/store/StoreService;->STATIC_HEADERS:Ljava/util/List;

    .line 435
    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newGetInstance(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    .line 434
    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    return-object v1
.end method

.method public getProductFromLegacyProductId(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    .locals 1
    .param p1, "legacyId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 367
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 369
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;->LegacyXboxProductId:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/service/store/StoreService;->getProductFromAlternateId(Ljava/lang/String;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v0

    return-object v0
.end method

.method public getProductFromPackageFamilyName(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    .locals 1
    .param p1, "packageFamilyName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 350
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 352
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;->PackageFamilyName:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/service/store/StoreService;->getProductFromAlternateId(Ljava/lang/String;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v0

    return-object v0
.end method

.method public getProductFromTitleId(J)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    .locals 3
    .param p1, "titleId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 358
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getProductFromTitleId: titleId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    const-wide/16 v0, 0x0

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 361
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;->XboxTitleId:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/service/store/StoreService;->getProductFromAlternateId(Ljava/lang/String;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$AlternateIdType;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v0

    return-object v0
.end method

.method public getProductInBundleList(Ljava/lang/String;I)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    .locals 6
    .param p1, "productId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "skipCount"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 300
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 301
    const-wide/16 v2, 0x0

    int-to-long v4, p2

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 303
    sget-object v1, Lcom/microsoft/xbox/service/store/StoreService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "get product related item list of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 306
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "https://reco-public.rec.mp.microsoft.com/channels/reco/v8.0/Lists/Mapping/BundlesBySeed/%s?itemTypes=Game,Consumable,Durable&DeviceFamily=Windows.Xbox&count=%d&skipitems=%d&market=%s&language=%s"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    const/16 v5, 0x19

    .line 310
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    .line 311
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    .line 312
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    .line 313
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 306
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 315
    .local v0, "url":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/service/store/StoreService;->STATIC_HEADERS:Ljava/util/List;

    .line 316
    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newGetInstance(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    .line 315
    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    return-object v1
.end method

.method public getProductRelatedItemList(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    .locals 6
    .param p1, "productId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 281
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 283
    sget-object v1, Lcom/microsoft/xbox/service/store/StoreService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "get product related item list of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 286
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "https://reco-public.rec.mp.microsoft.com/channels/reco/v8.0/related/Game/%s?itemTypes=Game&DeviceFamily=Windows.Xbox&count=10&market=%s&language=%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    .line 289
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    .line 290
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 286
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 292
    .local v0, "url":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/service/store/StoreService;->STATIC_HEADERS:Ljava/util/List;

    .line 293
    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newGetInstance(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    .line 292
    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    return-object v1
.end method

.method public getProductsFromIds(Ljava/util/List;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 477
    .local p1, "productIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;->Details:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;

    const-string v1, "https://displaycatalog.mp.microsoft.com/v7.0/products/?fieldsTemplate=%s&bigIds=%s&market=%s&languages=%s"

    invoke-direct {p0, v0, p1, v1}, Lcom/microsoft/xbox/service/store/StoreService;->hydrateProductsFromId(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v0

    return-object v0
.end method

.method public getProductsReducedInfoFromIds(Ljava/util/List;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 455
    .local p1, "productIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/store/StoreService;->getProductsReducedInfoFromIdsWithFilter(Ljava/util/List;Ljava/util/List;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v0

    return-object v0
.end method

.method public getProductsReducedInfoFromIdsWithFilter(Ljava/util/List;Ljava/util/List;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    .locals 5
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 462
    .local p1, "productIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "filters":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 463
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 465
    const-string v1, "https://displaycatalog.mp.microsoft.com/v7.0/products/?fieldsTemplate=%s&bigIds=%s&market=%s&languages=%s"

    .line 467
    .local v1, "url":Ljava/lang/String;
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/util/Pair;

    .line 468
    .local v0, "filter":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, v0, Landroid/support/v4/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget-object v3, v0, Landroid/support/v4/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->urlAppendFilter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 469
    goto :goto_0

    .line 471
    .end local v0    # "filter":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;->Browse:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;

    invoke-direct {p0, v2, p1, v1}, Lcom/microsoft/xbox/service/store/StoreService;->hydrateProductsFromId(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail$FieldTemplateType;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v2

    return-object v2
.end method

.method public getProductsReducedInfoFromList(Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    .locals 4
    .param p1, "list"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 442
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 443
    .local v1, "productIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 444
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;

    .line 445
    .local v0, "item":Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;
    iget-object v3, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;->id:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 447
    .end local v0    # "item":Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationListItem;
    :cond_0
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/store/StoreService;->getProductsReducedInfoFromIds(Ljava/util/List;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    move-result-object v2

    .line 449
    :goto_1
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public getRatings()Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 555
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 557
    const-string v0, "https://displaycatalog.md.mp.microsoft.com/v7.0/ratings?market=%s&languages=%s"

    .line 559
    invoke-static {v0}, Lcom/microsoft/xbox/service/store/StoreService;->urlWithMarketAndLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 560
    invoke-static {}, Lcom/microsoft/xbox/service/store/StoreService;->getHeaderWithCVValue()Ljava/util/List;

    move-result-object v1

    .line 558
    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newGetInstance(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;

    .line 557
    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;

    return-object v0
.end method

.method public getRecentList(II)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    .locals 1
    .param p1, "count"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "skipCount"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 144
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->GamesRecent:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/microsoft/xbox/service/store/StoreService;->getStoreRecommendationList(ILjava/lang/String;I)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    return-object v0
.end method

.method public getSearchAutoSuggestions(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;
    .locals 6
    .param p1, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 483
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 484
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 486
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "https://displaycatalog.mp.microsoft.com/v7.0/productFamilies/autosuggest?query=%1$s&productFamilyNames=Games&market=%2$s&languages=%3$s&platformdependencyname=windows.xbox"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    .line 490
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    .line 491
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 486
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 494
    .local v0, "url":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/store/StoreService;->getHeaderWithCVValue()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newGetInstance(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;

    .line 493
    invoke-static {v1, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;

    return-object v1
.end method

.method public getSearchResults(Ljava/lang/String;I)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    .locals 9
    .param p1, "searchText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "page"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 579
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 580
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 582
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v1

    .line 583
    .local v1, "market":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v0

    .line 584
    .local v0, "languages":Ljava/lang/String;
    mul-int/lit8 v2, p2, 0x19

    .line 586
    .local v2, "skip":I
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "https://displaycatalog.mp.microsoft.com/v7.0/productFamilies/%1$s/products?query=%2$s&$top=%6$d&market=%3$s&languages=%4$s&$skip=%5$d&fieldsTemplate=details&platformdependencyname=windows.xbox"

    const/4 v6, 0x6

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "Games"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object p1, v6, v7

    const/4 v7, 0x2

    aput-object v1, v6, v7

    const/4 v7, 0x3

    aput-object v0, v6, v7

    const/4 v7, 0x4

    .line 593
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x5

    const/16 v8, 0x19

    .line 594
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    .line 586
    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 597
    .local v3, "url":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/store/StoreService;->getHeaderWithCVValue()Ljava/util/List;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newGetInstance(Ljava/lang/String;Ljava/util/List;)Ljava/util/concurrent/Callable;

    move-result-object v4

    const-class v5, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    .line 596
    invoke-static {v4, v5}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    return-object v4
.end method

.method public getStoreCollectionList(Ljava/util/List;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 102
    .local p1, "productIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 104
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreService;->TAG:Ljava/lang/String;

    const-string v1, "getStoreCollectionList"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 107
    const-string v0, "https://collections.mp.microsoft.com/v7.0/collections/query"

    .line 110
    invoke-static {}, Lcom/microsoft/xbox/service/store/StoreService;->getHeaderWithCVValue()Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionPost;

    invoke-direct {v2, p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionPost;-><init>(Ljava/util/List;)V

    .line 111
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 108
    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newPostInstance(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;

    .line 107
    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->responseFromRequestAccepting2xxs(Ljava/util/concurrent/Callable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionList;

    return-object v0
.end method

.method public getTopFreeAddonList(II)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    .locals 1
    .param p1, "count"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "skipCount"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 194
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->AddonsTopFree:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/microsoft/xbox/service/store/StoreService;->getStoreRecommendationAddonList(ILjava/lang/String;I)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    return-object v0
.end method

.method public getTopPaidAddonList(II)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;
    .locals 1
    .param p1, "count"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2, "skipCount"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 188
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->AddonsTopPaid:Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$StoreListType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/microsoft/xbox/service/store/StoreService;->getStoreRecommendationAddonList(ILjava/lang/String;I)Lcom/microsoft/xbox/service/store/StoreDataTypes/RecommendationListResponse$RecommendationList;

    move-result-object v0

    return-object v0
.end method

.method public pushInstallProduct(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "productId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "skuId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 117
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 118
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 120
    sget-object v2, Lcom/microsoft/xbox/service/store/StoreService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "attempting push install of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with SkuId "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 123
    new-instance v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$ProductSkuId;

    invoke-direct {v1, p1, p2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$ProductSkuId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    .local v1, "productSkuId":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$ProductSkuId;
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/PushInstallDataTypes$PushInstallRequest;

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/PushInstallDataTypes$PushInstallRequest;-><init>(Ljava/util/List;)V

    .line 126
    .local v0, "installBody":Lcom/microsoft/xbox/service/store/StoreDataTypes/PushInstallDataTypes$PushInstallRequest;
    const-string v2, "https://collections.mp.microsoft.com/v7.0/beneficiaries/me/install"

    .line 129
    invoke-static {}, Lcom/microsoft/xbox/service/store/StoreService;->getHeaderWithCVValue()Ljava/util/List;

    move-result-object v3

    .line 130
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 127
    invoke-static {v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/XLEHttpStatusAndStreamCallable;->newPostInstance(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v2

    .line 126
    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->requestAccepting2xxs(Ljava/util/concurrent/Callable;)Z

    move-result v2

    return v2
.end method
