.class final Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;
.super Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;
.source "AutoValue_GameProfileAchievementsCompositeItem.java"


# instance fields
.field private final achievementItem:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

.field private final isAchievementHeader:Z

.field private final isAchievementItem:Z

.field private final isEmptyAchievementItems:Z

.field private final isLeaderboardHeader:Z

.field private final isLeaderboardItem:Z

.field private final leaderboardItem:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;


# direct methods
.method constructor <init>(ZZZZZLcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;)V
    .locals 0
    .param p1, "isLeaderboardHeader"    # Z
    .param p2, "isAchievementHeader"    # Z
    .param p3, "isLeaderboardItem"    # Z
    .param p4, "isAchievementItem"    # Z
    .param p5, "isEmptyAchievementItems"    # Z
    .param p6, "leaderboardItem"    # Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p7, "achievementItem"    # Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;-><init>()V

    .line 28
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isLeaderboardHeader:Z

    .line 29
    iput-boolean p2, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isAchievementHeader:Z

    .line 30
    iput-boolean p3, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isLeaderboardItem:Z

    .line 31
    iput-boolean p4, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isAchievementItem:Z

    .line 32
    iput-boolean p5, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isEmptyAchievementItems:Z

    .line 33
    iput-object p6, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->leaderboardItem:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;

    .line 34
    iput-object p7, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->achievementItem:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 35
    return-void
.end method


# virtual methods
.method public achievementItem()Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->achievementItem:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 89
    if-ne p1, p0, :cond_1

    .line 102
    :cond_0
    :goto_0
    return v1

    .line 92
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;

    if-eqz v3, :cond_5

    move-object v0, p1

    .line 93
    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;

    .line 94
    .local v0, "that":Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;
    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isLeaderboardHeader:Z

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->isLeaderboardHeader()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isAchievementHeader:Z

    .line 95
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->isAchievementHeader()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isLeaderboardItem:Z

    .line 96
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->isLeaderboardItem()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isAchievementItem:Z

    .line 97
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->isAchievementItem()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isEmptyAchievementItems:Z

    .line 98
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->isEmptyAchievementItems()Z

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->leaderboardItem:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;

    if-nez v3, :cond_3

    .line 99
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->leaderboardItem()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->achievementItem:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-nez v3, :cond_4

    .line 100
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->achievementItem()Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 99
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->leaderboardItem:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->leaderboardItem()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 100
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->achievementItem:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->achievementItem()Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;
    :cond_5
    move v1, v2

    .line 102
    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const v5, 0xf4243

    .line 107
    const/4 v0, 0x1

    .line 108
    .local v0, "h":I
    mul-int/2addr v0, v5

    .line 109
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isLeaderboardHeader:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 110
    mul-int/2addr v0, v5

    .line 111
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isAchievementHeader:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 112
    mul-int/2addr v0, v5

    .line 113
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isLeaderboardItem:Z

    if-eqz v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 114
    mul-int/2addr v0, v5

    .line 115
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isAchievementItem:Z

    if-eqz v1, :cond_3

    move v1, v2

    :goto_3
    xor-int/2addr v0, v1

    .line 116
    mul-int/2addr v0, v5

    .line 117
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isEmptyAchievementItems:Z

    if-eqz v1, :cond_4

    :goto_4
    xor-int/2addr v0, v2

    .line 118
    mul-int/2addr v0, v5

    .line 119
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->leaderboardItem:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;

    if-nez v1, :cond_5

    move v1, v4

    :goto_5
    xor-int/2addr v0, v1

    .line 120
    mul-int/2addr v0, v5

    .line 121
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->achievementItem:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-nez v1, :cond_6

    :goto_6
    xor-int/2addr v0, v4

    .line 122
    return v0

    :cond_0
    move v1, v3

    .line 109
    goto :goto_0

    :cond_1
    move v1, v3

    .line 111
    goto :goto_1

    :cond_2
    move v1, v3

    .line 113
    goto :goto_2

    :cond_3
    move v1, v3

    .line 115
    goto :goto_3

    :cond_4
    move v2, v3

    .line 117
    goto :goto_4

    .line 119
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->leaderboardItem:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_5

    .line 121
    :cond_6
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->achievementItem:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v4

    goto :goto_6
.end method

.method public isAchievementHeader()Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isAchievementHeader:Z

    return v0
.end method

.method public isAchievementItem()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isAchievementItem:Z

    return v0
.end method

.method public isEmptyAchievementItems()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isEmptyAchievementItems:Z

    return v0
.end method

.method public isLeaderboardHeader()Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isLeaderboardHeader:Z

    return v0
.end method

.method public isLeaderboardItem()Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isLeaderboardItem:Z

    return v0
.end method

.method public leaderboardItem()Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->leaderboardItem:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GameProfileAchievementsCompositeItem{isLeaderboardHeader="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isLeaderboardHeader:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isAchievementHeader="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isAchievementHeader:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isLeaderboardItem="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isLeaderboardItem:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isAchievementItem="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isAchievementItem:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isEmptyAchievementItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->isEmptyAchievementItems:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", leaderboardItem="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->leaderboardItem:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$StatisticsWithRank;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", achievementItem="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AutoValue_GameProfileAchievementsCompositeItem;->achievementItem:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
