.class Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultMoreRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "EDSV2SearchDataModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetSearchResultMoreRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;",
        ">;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

.field private isCanceled:Z

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;)V
    .locals 1
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

    .prologue
    .line 231
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultMoreRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 232
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultMoreRunner;->caller:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

    .line 233
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultMoreRunner;->isCanceled:Z

    .line 234
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 242
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getEDSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultMoreRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

    .line 243
    invoke-static {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->access$300(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultMoreRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->access$400(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;->getValue()I

    move-result v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultMoreRunner;->this$0:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->access$600(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;->getContinuationToken()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0xa

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;->searchMediaItems(Ljava/lang/String;ILjava/lang/String;I)Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v0

    .line 242
    return-object v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 227
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultMoreRunner;->buildData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;

    move-result-object v0

    return-object v0
.end method

.method public cancel()V
    .locals 1

    .prologue
    .line 237
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultMoreRunner;->isCanceled:Z

    .line 238
    return-void
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 259
    const-wide/16 v0, 0xfb4

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 252
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchResult;>;"
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultMoreRunner;->isCanceled:Z

    if-nez v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel$GetSearchResultMoreRunner;->caller:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;->access$700(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchDataModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 255
    :cond_0
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 248
    return-void
.end method
