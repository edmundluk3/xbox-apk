.class public final enum Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;
.super Ljava/lang/Enum;
.source "MediaPlaybackStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

.field public static final enum Changing:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

.field public static final enum Closed:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

.field public static final enum Paused:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

.field public static final enum Playing:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

.field public static final enum Stopped:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

.field private static final _lookup:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final _value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 12
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    const-string v3, "Closed"

    invoke-direct {v2, v3, v1, v1}, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->Closed:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    .line 14
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    const-string v3, "Changing"

    invoke-direct {v2, v3, v4, v4}, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->Changing:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    .line 16
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    const-string v3, "Stopped"

    invoke-direct {v2, v3, v5, v5}, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->Stopped:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    .line 18
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    const-string v3, "Playing"

    invoke-direct {v2, v3, v6, v6}, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->Playing:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    .line 20
    new-instance v2, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    const-string v3, "Paused"

    invoke-direct {v2, v3, v7, v7}, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->Paused:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    .line 10
    const/4 v2, 0x5

    new-array v2, v2, [Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    sget-object v3, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->Closed:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->Changing:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    aput-object v3, v2, v4

    sget-object v3, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->Stopped:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    aput-object v3, v2, v5

    sget-object v3, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->Playing:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    aput-object v3, v2, v6

    sget-object v3, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->Paused:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    aput-object v3, v2, v7

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->$VALUES:[Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    .line 23
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->_lookup:Landroid/util/SparseArray;

    .line 26
    invoke-static {}, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->values()[Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    move-result-object v2

    array-length v3, v2

    .local v0, "val":Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 27
    sget-object v4, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->_lookup:Landroid/util/SparseArray;

    iget v5, v0, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->_value:I

    invoke-virtual {v4, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 26
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 29
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 32
    iput p3, p0, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->_value:I

    .line 33
    return-void
.end method

.method public static fromInt(I)Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 49
    sget-object v1, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->_lookup:Landroid/util/SparseArray;

    invoke-virtual {v1, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    .line 50
    .local v0, "val":Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;
    if-nez v0, :cond_0

    .line 51
    sget-object v0, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->Closed:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    .line 54
    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->$VALUES:[Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->_value:I

    return v0
.end method
