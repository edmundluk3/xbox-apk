.class public abstract Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$UserListItem;
.super Ljava/lang/Object;
.source "ClubHomeScreenPlayersListAdapter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$PlayerListItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "UserListItem"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;)Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenPlayersListAdapter$UserListItem;
    .locals 1
    .param p0, "userSummary"    # Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    .prologue
    .line 56
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/AutoValue_ClubHomeScreenPlayersListAdapter_UserListItem;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/clubs/AutoValue_ClubHomeScreenPlayersListAdapter_UserListItem;-><init>(Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;)V

    return-object v0
.end method


# virtual methods
.method abstract userSummary()Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
