.class public final enum Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;
.super Ljava/lang/Enum;
.source "LfgListViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LfgListViewAdapterItemType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

.field public static final enum GameProfile:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

.field public static final enum Suggested:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

.field public static final enum Upcoming:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

.field public static final enum UpcomingNoData:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 97
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    const-string v1, "Upcoming"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;->Upcoming:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    const-string v1, "UpcomingNoData"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;->UpcomingNoData:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    const-string v1, "Suggested"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;->Suggested:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    const-string v1, "GameProfile"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;->GameProfile:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    .line 96
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;->Upcoming:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;->UpcomingNoData:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;->Suggested:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;->GameProfile:Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;->$VALUES:[Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 96
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;->$VALUES:[Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/adapter/LfgListViewAdapter$LfgListViewAdapterItemType;

    return-object v0
.end method
