.class public Lcom/microsoft/xbox/smartglass/AuthInfo;
.super Ljava/lang/Object;
.source "AuthInfo.java"


# static fields
.field public static DefaultPolicy:Ljava/lang/String;


# instance fields
.field public authTicket:Ljava/lang/String;

.field public clientId:Ljava/lang/String;

.field public policy:Ljava/lang/String;

.field public refreshToken:Ljava/lang/String;

.field public sandboxId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-string v0, "MBI_SSL"

    sput-object v0, Lcom/microsoft/xbox/smartglass/AuthInfo;->DefaultPolicy:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "authTicket"    # Ljava/lang/String;
    .param p2, "sandboxId"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/AuthInfo;->authTicket:Ljava/lang/String;

    .line 38
    iput-object p2, p0, Lcom/microsoft/xbox/smartglass/AuthInfo;->sandboxId:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "refreshToken"    # Ljava/lang/String;
    .param p2, "clientId"    # Ljava/lang/String;
    .param p3, "policy"    # Ljava/lang/String;
    .param p4, "sandboxId"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/AuthInfo;->refreshToken:Ljava/lang/String;

    .line 50
    iput-object p2, p0, Lcom/microsoft/xbox/smartglass/AuthInfo;->clientId:Ljava/lang/String;

    .line 51
    iput-object p3, p0, Lcom/microsoft/xbox/smartglass/AuthInfo;->policy:Ljava/lang/String;

    .line 52
    iput-object p4, p0, Lcom/microsoft/xbox/smartglass/AuthInfo;->sandboxId:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "authTicket"    # Ljava/lang/String;
    .param p2, "refreshToken"    # Ljava/lang/String;
    .param p3, "clientId"    # Ljava/lang/String;
    .param p4, "policy"    # Ljava/lang/String;
    .param p5, "sandboxId"    # Ljava/lang/String;

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/AuthInfo;->authTicket:Ljava/lang/String;

    .line 65
    iput-object p2, p0, Lcom/microsoft/xbox/smartglass/AuthInfo;->refreshToken:Ljava/lang/String;

    .line 66
    iput-object p3, p0, Lcom/microsoft/xbox/smartglass/AuthInfo;->clientId:Ljava/lang/String;

    .line 67
    iput-object p4, p0, Lcom/microsoft/xbox/smartglass/AuthInfo;->policy:Ljava/lang/String;

    .line 68
    iput-object p5, p0, Lcom/microsoft/xbox/smartglass/AuthInfo;->sandboxId:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public static getAnonymous()Lcom/microsoft/xbox/smartglass/AuthInfo;
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/microsoft/xbox/smartglass/AuthInfo;

    invoke-direct {v0}, Lcom/microsoft/xbox/smartglass/AuthInfo;-><init>()V

    return-object v0
.end method
