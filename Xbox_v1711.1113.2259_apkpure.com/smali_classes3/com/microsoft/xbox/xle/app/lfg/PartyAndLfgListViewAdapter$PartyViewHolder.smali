.class public Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "PartyAndLfgListViewAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PartyViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private headerLayout:Landroid/widget/RelativeLayout;

.field private hereNowCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private hostGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private hostImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;Landroid/view/View;)V
    .locals 2
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;

    .line 129
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0853

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->headerLayout:Landroid/widget/RelativeLayout;

    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0854

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->hostImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 133
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0858

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0856

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->hereNowCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 135
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e0857

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->hostGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    return-void
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;)V
    .locals 12
    .param p1, "partyListItem"    # Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v5, 0x7f020122

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 142
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 144
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;->party:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;

    .line 145
    .local v2, "party":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;
    iget-object v0, p1, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;->host:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 146
    .local v0, "host":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    if-eqz v0, :cond_1

    .line 147
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->hostImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {v3, v11}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 148
    iget-object v3, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayPicRaw:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 149
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->hostImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayPicRaw:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v5}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 155
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->hostGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayName:Ljava/lang/String;

    invoke-static {v3, v4, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 156
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->headerLayout:Landroid/widget/RelativeLayout;

    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->preferredColor:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPreferredColor;

    iget-object v4, v4, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPreferredColor;->secondaryColor:Ljava/lang/String;

    invoke-static {v4}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->convertColorFromString(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 163
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->hereNowCount:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f07024e

    new-array v6, v11, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;->accepted()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 166
    sget-object v3, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$1;->$SwitchMap$com$microsoft$xbox$service$multiplayer$MultiplayerSessionDataTypes$MultiplayerSessionRestriction:[I

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;->joinRestriction()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionRestriction;->getMultiplayerSessionRestriction(Ljava/lang/String;)Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionRestriction;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionRestriction;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 173
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0701f7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 176
    .local v1, "inviteStatus":Ljava/lang/String;
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->description:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f07024f

    new-array v6, v11, [Ljava/lang/Object;

    aput-object v1, v6, v10

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 177
    return-void

    .line 158
    .end local v1    # "inviteStatus":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->hostImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-static {v3, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 159
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->hostGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v3, v10}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 160
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->headerLayout:Landroid/widget/RelativeLayout;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->access$000()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    goto :goto_0

    .line 169
    :pswitch_0
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070a51

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 170
    .restart local v1    # "inviteStatus":Ljava/lang/String;
    goto :goto_1

    .line 166
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 121
    check-cast p1, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->onBind(Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 181
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->access$100(Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 182
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->access$200(Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->getAdapterPosition()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;

    .line 183
    .local v0, "item":Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;
    instance-of v1, v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;

    if-eqz v1, :cond_0

    .line 184
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->access$100(Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v1

    check-cast v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;

    .end local v0    # "item":Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$PartyListItem;->party:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSessionQueryResponseItem;

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 187
    :cond_0
    return-void
.end method
