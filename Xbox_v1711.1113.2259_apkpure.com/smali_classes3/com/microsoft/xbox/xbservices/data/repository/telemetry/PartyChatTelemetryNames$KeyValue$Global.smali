.class public Lcom/microsoft/xbox/xbservices/data/repository/telemetry/PartyChatTelemetryNames$KeyValue$Global;
.super Ljava/lang/Object;
.source "PartyChatTelemetryNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/repository/telemetry/PartyChatTelemetryNames$KeyValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Global"
.end annotation


# static fields
.field public static final DeviceBluetooth:Ljava/lang/String; = "Bluetooth"

.field public static final DeviceEarpiece:Ljava/lang/String; = "Earpiece"

.field public static final DeviceNone:Ljava/lang/String; = "None"

.field public static final DeviceSpeakerPhone:Ljava/lang/String; = "Speaker phone"

.field public static final DeviceWiredHeadset:Ljava/lang/String; = "Wired headset"

.field public static final Unknown:Ljava/lang/String; = "UNKNOWN"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
