.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingData;
.super Ljava/lang/Object;
.source "StoreItemDetailResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UserRatingData"
.end annotation


# instance fields
.field public final aggregateTimeSpan:Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "AggregateTimeSpan"
    .end annotation
.end field

.field public final averageRating:F
    .annotation build Landroid/support/annotation/FloatRange;
        from = 0.0
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "AverageRating"
    .end annotation
.end field

.field public final ratingCount:I
    .annotation build Landroid/support/annotation/IntRange;
        from = 0x0L
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "RatingCount"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;FI)V
    .locals 4
    .param p1, "aggregateTimeSpan"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "averageRating"    # F
        .annotation build Landroid/support/annotation/FloatRange;
            from = 0.0
        .end annotation
    .end param
    .param p3, "ratingCount"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param

    .prologue
    .line 1152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1153
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 1154
    const-wide/16 v0, 0x0

    float-to-double v2, p2

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->floatRangeFrom(DD)V

    .line 1155
    const-wide/16 v0, 0x0

    int-to-long v2, p3

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 1157
    iput-object p1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingData;->aggregateTimeSpan:Ljava/lang/String;

    .line 1158
    iput p2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingData;->averageRating:F

    .line 1159
    iput p3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$UserRatingData;->ratingCount:I

    .line 1160
    return-void
.end method
