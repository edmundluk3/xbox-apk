.class Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;
.super Landroid/widget/BaseAdapter;
.source "ChooseGamerpicDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChooseGamerpicDialogAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$1;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;)V

    return-void
.end method

.method static synthetic lambda$getView$0(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;Ljava/lang/String;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;)Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->setCurrentGamerpic(Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;)Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->closeGamerpicPickerDialog()V

    .line 112
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;)Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;)Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getGamerpicList()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;)Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getGamerpicList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 78
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;)Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;)Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getGamerpicList()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;)Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getGamerpicList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 87
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;)Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;)Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getGamerpicList()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;)Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getGamerpicList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    .line 96
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v8, 0x7f020125

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 102
    move-object v0, p2

    .line 103
    .local v0, "gamerpicView":Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 105
    .local v2, "url":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 106
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->access$200(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f030051

    invoke-virtual {v5, v6, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 109
    :cond_0
    invoke-static {p0, v2}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070d71

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    add-int/lit8 v7, p1, 0x1

    .line 115
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;->getCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    .line 114
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 117
    const v5, 0x7f0e0257

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 120
    .local v1, "image":Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    if-eqz v2, :cond_1

    .line 122
    invoke-virtual {v1, v2, v8, v8}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 126
    :cond_1
    if-eqz v2, :cond_2

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;)Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getCurrentGamerpic()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog$ChooseGamerpicDialogAdapter;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->access$100(Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;)Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;->getCurrentGamerpic()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    :goto_0
    invoke-virtual {v0, v3}, Landroid/view/View;->setSelected(Z)V

    .line 128
    return-object v0

    :cond_2
    move v3, v4

    .line 126
    goto :goto_0
.end method
