.class public Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "ClubChatNotificationScreenAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final hoverChatCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

.field private final noNotificationButton:Landroid/widget/RadioButton;

.field private final notificationSettingListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

.field private final notifyByAllMessagesButton:Landroid/widget/RadioButton;

.field private final notifyByDirectMentionButton:Landroid/widget/RadioButton;

.field private final notifyByGroup:Landroid/widget/RadioGroup;

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;)V
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 32
    const v0, 0x7f0e02e2

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->screenBody:Landroid/view/View;

    .line 34
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;

    .line 36
    const v0, 0x7f0e02e5

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 37
    const v0, 0x7f0e02e7

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->notifyByGroup:Landroid/widget/RadioGroup;

    .line 38
    const v0, 0x7f0e02e8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->notifyByAllMessagesButton:Landroid/widget/RadioButton;

    .line 39
    const v0, 0x7f0e02e9

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->notifyByDirectMentionButton:Landroid/widget/RadioButton;

    .line 40
    const v0, 0x7f0e02ea

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->noNotificationButton:Landroid/widget/RadioButton;

    .line 41
    const v0, 0x7f0e02eb

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->hoverChatCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;)Landroid/widget/RadioGroup$OnCheckedChangeListener;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->notificationSettingListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->hoverChatCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;)Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->notifyByGroup:Landroid/widget/RadioGroup;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->notificationSettingListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 77
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->close()V

    .line 45
    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;Landroid/widget/RadioGroup;I)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;
    .param p1, "group"    # Landroid/widget/RadioGroup;
    .param p2, "checkedId"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 48
    invoke-virtual {p1, v2}, Landroid/widget/RadioGroup;->setEnabled(Z)V

    .line 49
    packed-switch p2, :pswitch_data_0

    .line 70
    const-string v0, "notificationSettingListener: not recognized radio button id"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 72
    :goto_0
    return-void

    .line 51
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->TAG:Ljava/lang/String;

    const-string v1, "clicked on notify by all messages"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->AllMessages:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->setChatNotificationSetting(Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;)V

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->hoverChatCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    goto :goto_0

    .line 57
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->TAG:Ljava/lang/String;

    const-string v1, "clicked on notify by direct mention"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->DirectMention:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->setChatNotificationSetting(Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;)V

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->hoverChatCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    goto :goto_0

    .line 63
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->TAG:Ljava/lang/String;

    const-string v1, "clicked on no notification"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->None:Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->setChatNotificationSetting(Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;)V

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->hoverChatCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->hoverChatCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    goto :goto_0

    .line 49
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e02e8
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic lambda$new$2(Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;
    .param p1, "v"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->setHoverChatEnabled(Z)V

    return-void
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 81
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->getChatNotificationSetting()Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;

    move-result-object v0

    .line 83
    .local v0, "setting":Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->notifyByGroup:Landroid/widget/RadioGroup;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 84
    sget-object v3, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter$1;->$SwitchMap$com$microsoft$xbox$service$model$gcm$GcmModel$ChatNotificationSetting:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$ChatNotificationSetting;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 102
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->notifyByGroup:Landroid/widget/RadioGroup;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->notificationSettingListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {v3, v4}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 104
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->hoverChatCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->hoverChatGloballyEnabled()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 105
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->hoverChatCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->hoverChatEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ClubChatNotificationScreenViewModel;->hoverChatGloballyEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_1
    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    .line 106
    return-void

    .line 86
    :pswitch_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->notifyByGroup:Landroid/widget/RadioGroup;

    const v4, 0x7f0e02e8

    invoke-virtual {v3, v4}, Landroid/widget/RadioGroup;->check(I)V

    .line 87
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->hoverChatCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    goto :goto_0

    .line 91
    :pswitch_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->notifyByGroup:Landroid/widget/RadioGroup;

    const v4, 0x7f0e02e9

    invoke-virtual {v3, v4}, Landroid/widget/RadioGroup;->check(I)V

    .line 92
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->hoverChatCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    goto :goto_0

    .line 96
    :pswitch_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->notifyByGroup:Landroid/widget/RadioGroup;

    const v4, 0x7f0e02ea

    invoke-virtual {v3, v4}, Landroid/widget/RadioGroup;->check(I)V

    .line 97
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->hoverChatCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 98
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/ClubChatNotificationScreenAdapter;->hoverChatCheckbox:Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    goto :goto_0

    :cond_0
    move v1, v2

    .line 105
    goto :goto_1

    .line 84
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
