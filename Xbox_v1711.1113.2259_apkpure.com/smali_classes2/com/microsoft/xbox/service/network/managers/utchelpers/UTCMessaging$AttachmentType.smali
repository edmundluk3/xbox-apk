.class public final enum Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;
.super Ljava/lang/Enum;
.source "UTCMessaging.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AttachmentType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

.field public static final enum Achievement:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

.field public static final enum Broadcast:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

.field public static final enum Clip:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

.field public static final enum Screenshot:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

.field public static final enum SharedItem:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 35
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

    const-string v1, "Achievement"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;->Achievement:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

    .line 36
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

    const-string v1, "Broadcast"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;->Broadcast:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

    .line 37
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

    const-string v1, "Clip"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;->Clip:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

    .line 38
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

    const-string v1, "Screenshot"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;->Screenshot:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

    .line 39
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

    const-string v1, "SharedItem"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;->SharedItem:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

    .line 34
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;->Achievement:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;->Broadcast:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;->Clip:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;->Screenshot:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;->SharedItem:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 34
    const-class v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$AttachmentType;

    return-object v0
.end method
