.class final Lretrofit2/mock/Calls$DeferredCall;
.super Ljava/lang/Object;
.source "Calls.java"

# interfaces
.implements Lretrofit2/Call;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lretrofit2/mock/Calls;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "DeferredCall"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lretrofit2/Call",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final callable:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<",
            "Lretrofit2/Call",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private delegate:Lretrofit2/Call;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lretrofit2/Call",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/concurrent/Callable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lretrofit2/Call",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 123
    .local p0, "this":Lretrofit2/mock/Calls$DeferredCall;, "Lretrofit2/mock/Calls$DeferredCall<TT;>;"
    .local p1, "callable":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<Lretrofit2/Call<TT;>;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    iput-object p1, p0, Lretrofit2/mock/Calls$DeferredCall;->callable:Ljava/util/concurrent/Callable;

    .line 125
    return-void
.end method

.method private declared-synchronized getDelegate()Lretrofit2/Call;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lretrofit2/Call",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 128
    .local p0, "this":Lretrofit2/mock/Calls$DeferredCall;, "Lretrofit2/mock/Calls$DeferredCall<TT;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lretrofit2/mock/Calls$DeferredCall;->delegate:Lretrofit2/Call;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 129
    .local v0, "delegate":Lretrofit2/Call;, "Lretrofit2/Call<TT;>;"
    if-nez v0, :cond_0

    .line 131
    :try_start_1
    iget-object v2, p0, Lretrofit2/mock/Calls$DeferredCall;->callable:Ljava/util/concurrent/Callable;

    invoke-interface {v2}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "delegate":Lretrofit2/Call;, "Lretrofit2/Call<TT;>;"
    check-cast v0, Lretrofit2/Call;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 137
    .restart local v0    # "delegate":Lretrofit2/Call;, "Lretrofit2/Call<TT;>;"
    :goto_0
    :try_start_2
    iput-object v0, p0, Lretrofit2/mock/Calls$DeferredCall;->delegate:Lretrofit2/Call;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 139
    :cond_0
    monitor-exit p0

    return-object v0

    .line 132
    .end local v0    # "delegate":Lretrofit2/Call;, "Lretrofit2/Call<TT;>;"
    :catch_0
    move-exception v1

    .line 133
    .local v1, "e":Ljava/io/IOException;
    :try_start_3
    invoke-static {v1}, Lretrofit2/mock/Calls;->failure(Ljava/io/IOException;)Lretrofit2/Call;

    move-result-object v0

    .line 136
    .restart local v0    # "delegate":Lretrofit2/Call;, "Lretrofit2/Call<TT;>;"
    goto :goto_0

    .line 134
    .end local v0    # "delegate":Lretrofit2/Call;, "Lretrofit2/Call<TT;>;"
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 135
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Callable threw unrecoverable exception"

    invoke-direct {v2, v3, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 128
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 155
    .local p0, "this":Lretrofit2/mock/Calls$DeferredCall;, "Lretrofit2/mock/Calls$DeferredCall<TT;>;"
    invoke-direct {p0}, Lretrofit2/mock/Calls$DeferredCall;->getDelegate()Lretrofit2/Call;

    move-result-object v0

    invoke-interface {v0}, Lretrofit2/Call;->cancel()V

    .line 156
    return-void
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 119
    .local p0, "this":Lretrofit2/mock/Calls$DeferredCall;, "Lretrofit2/mock/Calls$DeferredCall<TT;>;"
    invoke-virtual {p0}, Lretrofit2/mock/Calls$DeferredCall;->clone()Lretrofit2/Call;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lretrofit2/Call;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lretrofit2/Call",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 163
    .local p0, "this":Lretrofit2/mock/Calls$DeferredCall;, "Lretrofit2/mock/Calls$DeferredCall<TT;>;"
    new-instance v0, Lretrofit2/mock/Calls$DeferredCall;

    iget-object v1, p0, Lretrofit2/mock/Calls$DeferredCall;->callable:Ljava/util/concurrent/Callable;

    invoke-direct {v0, v1}, Lretrofit2/mock/Calls$DeferredCall;-><init>(Ljava/util/concurrent/Callable;)V

    return-object v0
.end method

.method public enqueue(Lretrofit2/Callback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lretrofit2/Callback",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 147
    .local p0, "this":Lretrofit2/mock/Calls$DeferredCall;, "Lretrofit2/mock/Calls$DeferredCall<TT;>;"
    .local p1, "callback":Lretrofit2/Callback;, "Lretrofit2/Callback<TT;>;"
    invoke-direct {p0}, Lretrofit2/mock/Calls$DeferredCall;->getDelegate()Lretrofit2/Call;

    move-result-object v0

    invoke-interface {v0, p1}, Lretrofit2/Call;->enqueue(Lretrofit2/Callback;)V

    .line 148
    return-void
.end method

.method public execute()Lretrofit2/Response;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lretrofit2/Response",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 143
    .local p0, "this":Lretrofit2/mock/Calls$DeferredCall;, "Lretrofit2/mock/Calls$DeferredCall<TT;>;"
    invoke-direct {p0}, Lretrofit2/mock/Calls$DeferredCall;->getDelegate()Lretrofit2/Call;

    move-result-object v0

    invoke-interface {v0}, Lretrofit2/Call;->execute()Lretrofit2/Response;

    move-result-object v0

    return-object v0
.end method

.method public isCanceled()Z
    .locals 1

    .prologue
    .line 159
    .local p0, "this":Lretrofit2/mock/Calls$DeferredCall;, "Lretrofit2/mock/Calls$DeferredCall<TT;>;"
    invoke-direct {p0}, Lretrofit2/mock/Calls$DeferredCall;->getDelegate()Lretrofit2/Call;

    move-result-object v0

    invoke-interface {v0}, Lretrofit2/Call;->isCanceled()Z

    move-result v0

    return v0
.end method

.method public isExecuted()Z
    .locals 1

    .prologue
    .line 151
    .local p0, "this":Lretrofit2/mock/Calls$DeferredCall;, "Lretrofit2/mock/Calls$DeferredCall<TT;>;"
    invoke-direct {p0}, Lretrofit2/mock/Calls$DeferredCall;->getDelegate()Lretrofit2/Call;

    move-result-object v0

    invoke-interface {v0}, Lretrofit2/Call;->isExecuted()Z

    move-result v0

    return v0
.end method

.method public request()Lokhttp3/Request;
    .locals 1

    .prologue
    .line 167
    .local p0, "this":Lretrofit2/mock/Calls$DeferredCall;, "Lretrofit2/mock/Calls$DeferredCall<TT;>;"
    invoke-direct {p0}, Lretrofit2/mock/Calls$DeferredCall;->getDelegate()Lretrofit2/Call;

    move-result-object v0

    invoke-interface {v0}, Lretrofit2/Call;->request()Lokhttp3/Request;

    move-result-object v0

    return-object v0
.end method
