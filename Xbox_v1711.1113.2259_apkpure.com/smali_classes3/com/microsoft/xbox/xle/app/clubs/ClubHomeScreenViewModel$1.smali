.class Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$1;
.super Ljava/lang/Object;
.source "ClubHomeScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    .prologue
    .line 155
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPartialRosterChangeSuccess(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 202
    .local p1, "successfulMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    .local p2, "failedMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    const-string v0, "This shouldn\'t be possible, since the call is for a single xuid."

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 203
    return-void
.end method

.method public onRosterChangeFailure(Ljava/util/List;)V
    .locals 11
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "failedMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 175
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 176
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 177
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    .line 178
    .local v8, "clubMember":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->getError()Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    move-result-object v9

    .line 179
    .local v9, "error":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;
    if-eqz v9, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 181
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v7

    .line 183
    .local v7, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v9, :cond_2

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;->code()I

    move-result v0

    const/16 v1, 0x3ec

    if-ne v0, v1, :cond_2

    .line 184
    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 186
    .local v10, "res":Landroid/content/res/Resources;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    const v1, 0x7f070158

    .line 187
    invoke-virtual {v10, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f07015d

    .line 188
    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f070738

    .line 189
    invoke-virtual {v10, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v5, ""

    move-object v6, v4

    .line 186
    invoke-static/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 196
    .end local v10    # "res":Landroid/content/res/Resources;
    :goto_2
    return-void

    .end local v7    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .end local v8    # "clubMember":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .end local v9    # "error":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;
    :cond_0
    move v0, v2

    .line 176
    goto :goto_0

    .restart local v8    # "clubMember":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .restart local v9    # "error":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;
    :cond_1
    move v1, v2

    .line 179
    goto :goto_1

    .line 194
    .restart local v7    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    const v1, 0x7f070052

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;I)V

    goto :goto_2
.end method

.method public onRosterChangeSuccess(Ljava/util/List;)V
    .locals 10
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "successfulMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v9, 0x0

    .line 158
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v7

    .line 160
    .local v7, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    sget-object v8, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 163
    .local v8, "res":Landroid/content/res/Resources;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    const v1, 0x7f070156

    new-array v2, v5, [Ljava/lang/Object;

    .line 164
    invoke-virtual {v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-virtual {v8, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f070155

    new-array v5, v5, [Ljava/lang/Object;

    .line 165
    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->memberQuotaRemaining()Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v5, v9

    invoke-virtual {v8, v3, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f070738

    .line 166
    invoke-virtual {v8, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v5, ""

    move-object v6, v4

    .line 163
    invoke-static/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 171
    .end local v8    # "res":Landroid/content/res/Resources;
    :cond_0
    return-void
.end method
