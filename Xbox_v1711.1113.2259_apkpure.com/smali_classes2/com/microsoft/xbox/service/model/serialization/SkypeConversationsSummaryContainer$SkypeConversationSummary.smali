.class public Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;
.super Ljava/lang/Object;
.source "SkypeConversationsSummaryContainer.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SkypeConversationSummary"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;",
        ">;"
    }
.end annotation


# instance fields
.field public gamerPicUrl:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public transient isGroupConversation:Z

.field public lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

.field public messageType:Ljava/lang/String;

.field public properties:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$MessageProperties;

.field public realName:Ljava/lang/String;

.field public senderGamerTag:Ljava/lang/String;

.field public status:Lcom/microsoft/xbox/service/model/UserStatus;

.field public syncstate:Ljava/lang/String;

.field public threadProperties:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$ThreadProperties;

.field public type:Ljava/lang/String;

.field public transient unreadConversation:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;)I
    .locals 3
    .param p1, "newSkypeConversationSummary"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;

    .prologue
    const/4 v0, 0x0

    .line 46
    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    if-nez v1, :cond_1

    .line 55
    :cond_0
    :goto_0
    return v0

    .line 49
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    invoke-virtual {v1, v2}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 50
    const/4 v0, -0x1

    goto :goto_0

    .line 51
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    invoke-virtual {v1, v2}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 52
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 27
    check-cast p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;->compareTo(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;)I

    move-result v0

    return v0
.end method
