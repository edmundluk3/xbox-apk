.class public final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_ClubModerationDataTypes_ClubReportedItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final contentIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final contentTypeAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;",
            ">;"
        }
    .end annotation
.end field

.field private defaultContentId:Ljava/lang/String;

.field private defaultContentType:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

.field private defaultLastReported:Ljava/util/Date;

.field private defaultReportCount:I

.field private defaultReportId:J

.field private defaultReports:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;",
            ">;"
        }
    .end annotation
.end field

.field private final lastReportedAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private final reportCountAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final reportIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final reportsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 4
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 37
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 31
    iput-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->defaultContentId:Ljava/lang/String;

    .line 32
    iput-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->defaultContentType:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    .line 33
    iput-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->defaultLastReported:Ljava/util/Date;

    .line 34
    iput v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->defaultReportCount:I

    .line 35
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->defaultReportId:J

    .line 36
    iput-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->defaultReports:Lcom/google/common/collect/ImmutableList;

    .line 38
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->contentIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 39
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->contentTypeAdapter:Lcom/google/gson/TypeAdapter;

    .line 40
    const-class v0, Ljava/util/Date;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->lastReportedAdapter:Lcom/google/gson/TypeAdapter;

    .line 41
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->reportCountAdapter:Lcom/google/gson/TypeAdapter;

    .line 42
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->reportIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 43
    const-class v0, Lcom/google/common/collect/ImmutableList;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/reflect/Type;

    const-class v2, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->reportsAdapter:Lcom/google/gson/TypeAdapter;

    .line 44
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;
    .locals 10
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v1

    sget-object v9, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v1, v9, :cond_0

    .line 94
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 95
    const/4 v1, 0x0

    .line 141
    :goto_0
    return-object v1

    .line 97
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 98
    iget-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->defaultContentId:Ljava/lang/String;

    .line 99
    .local v2, "contentId":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->defaultContentType:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    .line 100
    .local v3, "contentType":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;
    iget-object v4, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->defaultLastReported:Ljava/util/Date;

    .line 101
    .local v4, "lastReported":Ljava/util/Date;
    iget v5, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->defaultReportCount:I

    .line 102
    .local v5, "reportCount":I
    iget-wide v6, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->defaultReportId:J

    .line 103
    .local v6, "reportId":J
    iget-object v8, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->defaultReports:Lcom/google/common/collect/ImmutableList;

    .line 104
    .local v8, "reports":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;>;"
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 105
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 106
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v1

    sget-object v9, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v1, v9, :cond_1

    .line 107
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 110
    :cond_1
    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v1, :pswitch_data_0

    .line 136
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 110
    :sswitch_0
    const-string v9, "contentId"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v1, 0x0

    goto :goto_2

    :sswitch_1
    const-string v9, "contentType"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :sswitch_2
    const-string v9, "lastReported"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v1, 0x2

    goto :goto_2

    :sswitch_3
    const-string v9, "reportCount"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v1, 0x3

    goto :goto_2

    :sswitch_4
    const-string v9, "reportId"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v1, 0x4

    goto :goto_2

    :sswitch_5
    const-string v9, "reports"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v1, 0x5

    goto :goto_2

    .line 112
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->contentIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "contentId":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 113
    .restart local v2    # "contentId":Ljava/lang/String;
    goto :goto_1

    .line 116
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->contentTypeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "contentType":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;
    check-cast v3, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    .line 117
    .restart local v3    # "contentType":Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;
    goto :goto_1

    .line 120
    :pswitch_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->lastReportedAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "lastReported":Ljava/util/Date;
    check-cast v4, Ljava/util/Date;

    .line 121
    .restart local v4    # "lastReported":Ljava/util/Date;
    goto :goto_1

    .line 124
    :pswitch_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->reportCountAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 125
    goto/16 :goto_1

    .line 128
    :pswitch_4
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->reportIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 129
    goto/16 :goto_1

    .line 132
    :pswitch_5
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->reportsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "reports":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;>;"
    check-cast v8, Lcom/google/common/collect/ImmutableList;

    .line 133
    .restart local v8    # "reports":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;>;"
    goto/16 :goto_1

    .line 140
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 141
    new-instance v1, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem;

    invoke-direct/range {v1 .. v8}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;Ljava/util/Date;IJLcom/google/common/collect/ImmutableList;)V

    goto/16 :goto_0

    .line 110
    :sswitch_data_0
    .sparse-switch
        -0x19741e91 -> :sswitch_4
        -0x1843fc8c -> :sswitch_0
        -0x1731acad -> :sswitch_1
        -0x103ed3c5 -> :sswitch_3
        0x413e51bf -> :sswitch_5
        0x7fffe589 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultContentId(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultContentId"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->defaultContentId:Ljava/lang/String;

    .line 47
    return-object p0
.end method

.method public setDefaultContentType(Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultContentType"    # Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->defaultContentType:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    .line 51
    return-object p0
.end method

.method public setDefaultLastReported(Ljava/util/Date;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultLastReported"    # Ljava/util/Date;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->defaultLastReported:Ljava/util/Date;

    .line 55
    return-object p0
.end method

.method public setDefaultReportCount(I)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultReportCount"    # I

    .prologue
    .line 58
    iput p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->defaultReportCount:I

    .line 59
    return-object p0
.end method

.method public setDefaultReportId(J)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultReportId"    # J

    .prologue
    .line 62
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->defaultReportId:J

    .line 63
    return-object p0
.end method

.method public setDefaultReports(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 66
    .local p1, "defaultReports":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->defaultReports:Lcom/google/common/collect/ImmutableList;

    .line 67
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;)V
    .locals 4
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    if-nez p2, :cond_0

    .line 73
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 90
    :goto_0
    return-void

    .line 76
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 77
    const-string v0, "contentId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->contentIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->contentId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 79
    const-string v0, "contentType"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->contentTypeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->contentType()Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportContentType;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 81
    const-string v0, "lastReported"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->lastReportedAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->lastReported()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 83
    const-string v0, "reportCount"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->reportCountAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->reportCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 85
    const-string v0, "reportId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->reportIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->reportId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 87
    const-string v0, "reports"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->reportsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->reports()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 89
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    check-cast p2, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubReportedItem$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;)V

    return-void
.end method
