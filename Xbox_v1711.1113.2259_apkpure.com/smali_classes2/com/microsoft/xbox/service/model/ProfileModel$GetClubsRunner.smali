.class Lcom/microsoft/xbox/service/model/ProfileModel$GetClubsRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ProfileModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ProfileModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetClubsRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ProfileModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;)V
    .locals 0

    .prologue
    .line 4792
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetClubsRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/service/model/ProfileModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/ProfileModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/ProfileModel$1;

    .prologue
    .line 4792
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/ProfileModel$GetClubsRunner;-><init>(Lcom/microsoft/xbox/service/model/ProfileModel;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4792
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ProfileModel$GetClubsRunner;->buildData()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 4800
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetClubsRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuidLong()J

    move-result-wide v0

    .line 4801
    .local v0, "xuid":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 4802
    sget-object v2, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-virtual {v2, v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->loadUserClubs(J)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopyWritable(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 4805
    :goto_0
    return-object v2

    .line 4804
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/service/model/ProfileModel;->TAG:Ljava/lang/String;

    const-string v3, "Attempted to load clubs for profile with invalid xuid"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 4805
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 4816
    const-wide/16 v0, 0x2580

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 4811
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ProfileModel$GetClubsRunner;->this$0:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/ProfileModel;->access$5600(Lcom/microsoft/xbox/service/model/ProfileModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 4812
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 4796
    return-void
.end method
