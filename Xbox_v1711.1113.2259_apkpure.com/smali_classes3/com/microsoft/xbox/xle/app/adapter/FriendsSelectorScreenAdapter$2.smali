.class Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$2;
.super Ljava/lang/Object;
.source "FriendsSelectorScreenAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/16 v3, 0x8

    const/4 v4, 0x0

    .line 98
    const/4 v0, 0x0

    .line 99
    .local v0, "friend":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->canSelectFriends()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Landroid/widget/AbsListView;

    move-result-object v2

    invoke-virtual {v2, p3}, Landroid/widget/AbsListView;->isItemChecked(I)Z

    move-result v2

    if-nez v2, :cond_5

    .line 101
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 102
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 103
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$300(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 105
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    if-eqz v2, :cond_4

    .line 106
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "friend":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    .line 107
    .restart local v0    # "friend":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getIsEnable()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 109
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Landroid/widget/AbsListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/AbsListView;->getChoiceMode()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$600(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 110
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$600(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    .line 111
    .local v1, "friendSelectorItem":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->toggleSelection(Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;)V

    .line 112
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$600(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 115
    .end local v1    # "friendSelectorItem":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->toggleSelection(Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;)V

    .line 116
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getIsSelected()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 117
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$600(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 118
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$600(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$700(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)V

    .line 132
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;->getInstance()Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 133
    return-void

    .line 121
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$600(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 125
    :cond_4
    const-string v2, "FriendSelectorListModule"

    const-string v3, "view.getTag type is unexpected"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 128
    :cond_5
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$500(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Landroid/widget/AbsListView;

    move-result-object v2

    invoke-virtual {v2, p3, v4}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    .line 129
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$600(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
