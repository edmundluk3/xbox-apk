.class public final Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_TournamentNotificationDataTypes_TournamentGameInvite.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;",
        ">;"
    }
.end annotation


# instance fields
.field private final continuationUrisAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;",
            ">;"
        }
    .end annotation
.end field

.field private defaultContinuationUris:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;

.field private defaultGameTypes:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;

.field private defaultInvitedXuid:Ljava/lang/String;

.field private defaultSessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

.field private defaultTeamId:Ljava/lang/String;

.field private defaultTeamName:Ljava/lang/String;

.field private defaultTitleId:J

.field private defaultTitleImageUri:Ljava/lang/String;

.field private defaultTitleName:Ljava/lang/String;

.field private defaultTournamentImageUri:Ljava/lang/String;

.field private defaultTournamentName:Ljava/lang/String;

.field private defaultTournamentRef:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

.field private final gameTypesAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;",
            ">;"
        }
    .end annotation
.end field

.field private final invitedXuidAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final sessionRefAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;",
            ">;"
        }
    .end annotation
.end field

.field private final teamIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final teamNameAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final titleIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final titleImageUriAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final titleNameAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final tournamentImageUriAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final tournamentNameAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final tournamentRefAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 3
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v2, 0x0

    .line 48
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 36
    iput-object v2, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultSessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    .line 37
    iput-object v2, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultTournamentRef:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    .line 38
    iput-object v2, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultInvitedXuid:Ljava/lang/String;

    .line 39
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultTitleId:J

    .line 40
    iput-object v2, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultTitleName:Ljava/lang/String;

    .line 41
    iput-object v2, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultTitleImageUri:Ljava/lang/String;

    .line 42
    iput-object v2, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultTeamName:Ljava/lang/String;

    .line 43
    iput-object v2, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultTeamId:Ljava/lang/String;

    .line 44
    iput-object v2, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultTournamentName:Ljava/lang/String;

    .line 45
    iput-object v2, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultTournamentImageUri:Ljava/lang/String;

    .line 46
    iput-object v2, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultGameTypes:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;

    .line 47
    iput-object v2, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultContinuationUris:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;

    .line 49
    const-class v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->sessionRefAdapter:Lcom/google/gson/TypeAdapter;

    .line 50
    const-class v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->tournamentRefAdapter:Lcom/google/gson/TypeAdapter;

    .line 51
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->invitedXuidAdapter:Lcom/google/gson/TypeAdapter;

    .line 52
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->titleIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 53
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->titleNameAdapter:Lcom/google/gson/TypeAdapter;

    .line 54
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->titleImageUriAdapter:Lcom/google/gson/TypeAdapter;

    .line 55
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->teamNameAdapter:Lcom/google/gson/TypeAdapter;

    .line 56
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->teamIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 57
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->tournamentNameAdapter:Lcom/google/gson/TypeAdapter;

    .line 58
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->tournamentImageUriAdapter:Lcom/google/gson/TypeAdapter;

    .line 59
    const-class v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->gameTypesAdapter:Lcom/google/gson/TypeAdapter;

    .line 60
    const-class v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->continuationUrisAdapter:Lcom/google/gson/TypeAdapter;

    .line 61
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;
    .locals 18
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 147
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v2

    sget-object v17, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    move-object/from16 v0, v17

    if-ne v2, v0, :cond_0

    .line 148
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 149
    const/4 v2, 0x0

    .line 225
    :goto_0
    return-object v2

    .line 151
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 152
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultSessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    .line 153
    .local v3, "sessionRef":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultTournamentRef:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    .line 154
    .local v4, "tournamentRef":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultInvitedXuid:Ljava/lang/String;

    .line 155
    .local v5, "invitedXuid":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultTitleId:J

    .line 156
    .local v6, "titleId":J
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultTitleName:Ljava/lang/String;

    .line 157
    .local v8, "titleName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultTitleImageUri:Ljava/lang/String;

    .line 158
    .local v9, "titleImageUri":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultTeamName:Ljava/lang/String;

    .line 159
    .local v10, "teamName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultTeamId:Ljava/lang/String;

    .line 160
    .local v11, "teamId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultTournamentName:Ljava/lang/String;

    .line 161
    .local v12, "tournamentName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultTournamentImageUri:Ljava/lang/String;

    .line 162
    .local v13, "tournamentImageUri":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultGameTypes:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;

    .line 163
    .local v14, "gameTypes":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultContinuationUris:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;

    .line 164
    .local v15, "continuationUris":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 165
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v16

    .line 166
    .local v16, "_name":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v2

    sget-object v17, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    move-object/from16 v0, v17

    if-ne v2, v0, :cond_1

    .line 167
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 170
    :cond_1
    const/4 v2, -0x1

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->hashCode()I

    move-result v17

    sparse-switch v17, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v2, :pswitch_data_0

    .line 220
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 170
    :sswitch_0
    const-string v17, "sessionRef"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    const/4 v2, 0x0

    goto :goto_2

    :sswitch_1
    const-string v17, "tournamentRef"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    const/4 v2, 0x1

    goto :goto_2

    :sswitch_2
    const-string v17, "invitedXuid"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    const/4 v2, 0x2

    goto :goto_2

    :sswitch_3
    const-string v17, "titleId"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    const/4 v2, 0x3

    goto :goto_2

    :sswitch_4
    const-string v17, "titleName"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    const/4 v2, 0x4

    goto :goto_2

    :sswitch_5
    const-string v17, "titleImageUri"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    const/4 v2, 0x5

    goto :goto_2

    :sswitch_6
    const-string v17, "teamName"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    const/4 v2, 0x6

    goto :goto_2

    :sswitch_7
    const-string v17, "teamId"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    const/4 v2, 0x7

    goto :goto_2

    :sswitch_8
    const-string v17, "tournamentName"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    const/16 v2, 0x8

    goto :goto_2

    :sswitch_9
    const-string v17, "tournamentImageUri"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    const/16 v2, 0x9

    goto :goto_2

    :sswitch_a
    const-string v17, "gameTypes"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    const/16 v2, 0xa

    goto :goto_2

    :sswitch_b
    const-string v17, "continuationUris"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    const/16 v2, 0xb

    goto/16 :goto_2

    .line 172
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->sessionRefAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "sessionRef":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
    check-cast v3, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    .line 173
    .restart local v3    # "sessionRef":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
    goto/16 :goto_1

    .line 176
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->tournamentRefAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "tournamentRef":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;
    check-cast v4, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    .line 177
    .restart local v4    # "tournamentRef":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;
    goto/16 :goto_1

    .line 180
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->invitedXuidAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "invitedXuid":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 181
    .restart local v5    # "invitedXuid":Ljava/lang/String;
    goto/16 :goto_1

    .line 184
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->titleIdAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 185
    goto/16 :goto_1

    .line 188
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->titleNameAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "titleName":Ljava/lang/String;
    check-cast v8, Ljava/lang/String;

    .line 189
    .restart local v8    # "titleName":Ljava/lang/String;
    goto/16 :goto_1

    .line 192
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->titleImageUriAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "titleImageUri":Ljava/lang/String;
    check-cast v9, Ljava/lang/String;

    .line 193
    .restart local v9    # "titleImageUri":Ljava/lang/String;
    goto/16 :goto_1

    .line 196
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->teamNameAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "teamName":Ljava/lang/String;
    check-cast v10, Ljava/lang/String;

    .line 197
    .restart local v10    # "teamName":Ljava/lang/String;
    goto/16 :goto_1

    .line 200
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->teamIdAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "teamId":Ljava/lang/String;
    check-cast v11, Ljava/lang/String;

    .line 201
    .restart local v11    # "teamId":Ljava/lang/String;
    goto/16 :goto_1

    .line 204
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->tournamentNameAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v12

    .end local v12    # "tournamentName":Ljava/lang/String;
    check-cast v12, Ljava/lang/String;

    .line 205
    .restart local v12    # "tournamentName":Ljava/lang/String;
    goto/16 :goto_1

    .line 208
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->tournamentImageUriAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v13

    .end local v13    # "tournamentImageUri":Ljava/lang/String;
    check-cast v13, Ljava/lang/String;

    .line 209
    .restart local v13    # "tournamentImageUri":Ljava/lang/String;
    goto/16 :goto_1

    .line 212
    :pswitch_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->gameTypesAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v14

    .end local v14    # "gameTypes":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;
    check-cast v14, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;

    .line 213
    .restart local v14    # "gameTypes":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;
    goto/16 :goto_1

    .line 216
    :pswitch_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->continuationUrisAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v15

    .end local v15    # "continuationUris":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;
    check-cast v15, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;

    .line 217
    .restart local v15    # "continuationUris":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;
    goto/16 :goto_1

    .line 224
    .end local v16    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 225
    new-instance v2, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;

    invoke-direct/range {v2 .. v15}, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite;-><init>(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;)V

    goto/16 :goto_0

    .line 170
    :sswitch_data_0
    .sparse-switch
        -0x7f4fdafd -> :sswitch_4
        -0x7701ce16 -> :sswitch_1
        -0x6fdfd8e6 -> :sswitch_9
        -0x6939d3ec -> :sswitch_8
        -0x63774578 -> :sswitch_6
        -0x59663e0d -> :sswitch_2
        -0x4deb0a6d -> :sswitch_3
        -0x3450d7a8 -> :sswitch_7
        -0x301120a2 -> :sswitch_b
        0x11761089 -> :sswitch_5
        0x3b13c3a7 -> :sswitch_a
        0x630dae1d -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultContinuationUris(Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;)Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultContinuationUris"    # Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultContinuationUris:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;

    .line 108
    return-object p0
.end method

.method public setDefaultGameTypes(Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;)Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultGameTypes"    # Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultGameTypes:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;

    .line 104
    return-object p0
.end method

.method public setDefaultInvitedXuid(Ljava/lang/String;)Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultInvitedXuid"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultInvitedXuid:Ljava/lang/String;

    .line 72
    return-object p0
.end method

.method public setDefaultSessionRef(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;)Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultSessionRef"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultSessionRef:Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    .line 64
    return-object p0
.end method

.method public setDefaultTeamId(Ljava/lang/String;)Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTeamId"    # Ljava/lang/String;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultTeamId:Ljava/lang/String;

    .line 92
    return-object p0
.end method

.method public setDefaultTeamName(Ljava/lang/String;)Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTeamName"    # Ljava/lang/String;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultTeamName:Ljava/lang/String;

    .line 88
    return-object p0
.end method

.method public setDefaultTitleId(J)Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultTitleId"    # J

    .prologue
    .line 75
    iput-wide p1, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultTitleId:J

    .line 76
    return-object p0
.end method

.method public setDefaultTitleImageUri(Ljava/lang/String;)Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTitleImageUri"    # Ljava/lang/String;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultTitleImageUri:Ljava/lang/String;

    .line 84
    return-object p0
.end method

.method public setDefaultTitleName(Ljava/lang/String;)Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTitleName"    # Ljava/lang/String;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultTitleName:Ljava/lang/String;

    .line 80
    return-object p0
.end method

.method public setDefaultTournamentImageUri(Ljava/lang/String;)Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTournamentImageUri"    # Ljava/lang/String;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultTournamentImageUri:Ljava/lang/String;

    .line 100
    return-object p0
.end method

.method public setDefaultTournamentName(Ljava/lang/String;)Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTournamentName"    # Ljava/lang/String;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultTournamentName:Ljava/lang/String;

    .line 96
    return-object p0
.end method

.method public setDefaultTournamentRef(Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;)Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTournamentRef"    # Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->defaultTournamentRef:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    .line 68
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;)V
    .locals 4
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    if-nez p2, :cond_0

    .line 114
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 143
    :goto_0
    return-void

    .line 117
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 118
    const-string v0, "sessionRef"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->sessionRefAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->sessionRef()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 120
    const-string v0, "tournamentRef"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->tournamentRefAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->tournamentRef()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentRef;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 122
    const-string v0, "invitedXuid"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->invitedXuidAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->invitedXuid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 124
    const-string v0, "titleId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->titleIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->titleId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 126
    const-string v0, "titleName"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->titleNameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->titleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 128
    const-string v0, "titleImageUri"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->titleImageUriAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->titleImageUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 130
    const-string v0, "teamName"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->teamNameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->teamName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 132
    const-string v0, "teamId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 133
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->teamIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->teamId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 134
    const-string v0, "tournamentName"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 135
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->tournamentNameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->tournamentName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 136
    const-string v0, "tournamentImageUri"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->tournamentImageUriAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->tournamentImageUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 138
    const-string v0, "gameTypes"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 139
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->gameTypesAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->gameTypes()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 140
    const-string v0, "continuationUris"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->continuationUrisAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;->continuationUris()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$ContinuationUris;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 142
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto/16 :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23
    check-cast p2, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_TournamentGameInvite$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$TournamentGameInvite;)V

    return-void
.end method
