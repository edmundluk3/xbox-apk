.class public Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "CreateLfgDetailsViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;
    }
.end annotation


# static fields
.field public static final ACCOUNT_SETTING_URI:Ljava/lang/String; = "https://account.xbox.com/settings"

.field private static final MAXIMUM_NEED_COUNT:I = 0xf

.field private static final MS_IN_15_MIN:I = 0xdbba0

.field private static final MS_IN_DAY:I = 0x5265c00

.field private static MULTIPLAYER_SESSION_MODEL:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

.field private static final TAG:Ljava/lang/String;

.field private static TITLE_HUB_MODEL:Lcom/microsoft/xbox/service/model/TitleHubModel;


# instance fields
.field private dateOffset:I

.field private defaultLanguageOffSet:I

.field private description:Ljava/lang/String;

.field private isCreatingLfg:Z

.field private isLeavingPreviousSession:Z

.field private languageOffset:I

.field private languages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;",
            ">;"
        }
    .end annotation
.end field

.field private needCount:I

.field private final needCountLabels:[Ljava/lang/String;

.field private scid:Ljava/lang/String;

.field private selectedClub:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

.field private selectedTags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
            ">;"
        }
    .end annotation
.end field

.field private selectedTitleId:J

.field private selectedVisibility:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

.field private shouldShareLfgToFeed:Z

.field private timeOffset:I

.field private upcomingLfgs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    const-class v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->TAG:Ljava/lang/String;

    .line 82
    sget-object v0, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    sput-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->MULTIPLAYER_SESSION_MODEL:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    .line 83
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->TITLE_HUB_MODEL:Lcom/microsoft/xbox/service/model/TitleHubModel;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 10
    .param p1, "parentScreen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    const-wide/16 v8, 0x0

    const/4 v4, 0x0

    .line 104
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 95
    const/4 v5, -0x1

    iput v5, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->defaultLanguageOffSet:I

    .line 98
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->languages:Ljava/util/List;

    .line 101
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->shouldShareLfgToFeed:Z

    .line 106
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedTags:Ljava/util/List;

    .line 107
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v3

    .line 108
    .local v3, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getTitleId()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedTitleId:J

    .line 109
    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedScid()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->scid:Ljava/lang/String;

    .line 110
    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedClubId()Ljava/lang/Long;

    move-result-object v0

    .line 112
    .local v0, "clubId":Ljava/lang/Long;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v5, v6, v8

    if-lez v5, :cond_0

    .line 113
    sget-object v5, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->getClubModel(J)Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedClub:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 116
    :cond_0
    const-string v5, "CreateLfgDetailsViewModel requires a valid titleId in the activity parameters."

    iget-wide v6, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedTitleId:J

    cmp-long v6, v6, v8

    if-lez v6, :cond_1

    const/4 v4, 0x1

    :cond_1
    invoke-static {v5, v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 118
    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getMultiplayerFilters()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;

    move-result-object v1

    .line 120
    .local v1, "filters":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;->getTags()Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 121
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedTags:Ljava/util/List;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandleFilters;->getTags()Ljava/util/List;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 124
    :cond_2
    const/16 v4, 0xf

    new-array v4, v4, [Ljava/lang/String;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->needCountLabels:[Ljava/lang/String;

    .line 125
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->needCountLabels:[Ljava/lang/String;

    array-length v4, v4

    if-ge v2, v4, :cond_3

    .line 126
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->needCountLabels:[Ljava/lang/String;

    add-int/lit8 v5, v2, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    .line 125
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 129
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v4

    invoke-virtual {v4, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getCreateLfgDetailsAdapter(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 130
    sget-object v4, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->XboxLive:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedVisibility:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    .line 131
    return-void
.end method

.method private createLfgInternal()V
    .locals 14

    .prologue
    const v5, 0x7f0707c7

    const/16 v3, 0xe

    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 534
    const/4 v4, 0x0

    .line 536
    .local v4, "startTime":Ljava/util/Date;
    iget v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->timeOffset:I

    if-lez v0, :cond_2

    .line 537
    new-instance v10, Ljava/util/Date;

    invoke-direct {v10}, Ljava/util/Date;-><init>()V

    .line 538
    .local v10, "now":Ljava/util/Date;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v9

    .line 539
    .local v9, "cal":Ljava/util/Calendar;
    invoke-virtual {v9, v10}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 542
    const/16 v0, 0xb

    invoke-virtual {v9, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 543
    const/16 v0, 0xc

    invoke-virtual {v9, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 544
    const/16 v0, 0xd

    invoke-virtual {v9, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 545
    invoke-virtual {v9, v3, v1}, Ljava/util/Calendar;->set(II)V

    .line 547
    const v0, 0xdbba0

    iget v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->timeOffset:I

    add-int/lit8 v1, v1, -0x1

    mul-int/2addr v0, v1

    const v1, 0x5265c00

    iget v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->dateOffset:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    invoke-virtual {v9, v3, v0}, Ljava/util/Calendar;->set(II)V

    .line 549
    invoke-virtual {v9}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 550
    invoke-virtual {v9}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    .line 568
    .end local v9    # "cal":Ljava/util/Calendar;
    .end local v10    # "now":Ljava/util/Date;
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->isCreatingLfg:Z

    .line 571
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->languages:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v8, "en-US"

    .line 573
    .local v8, "locale":Ljava/lang/String;
    :goto_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->MULTIPLAYER_SESSION_MODEL:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->scid:Ljava/lang/String;

    .line 575
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->getDescription()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->needCount:I

    .line 576
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 578
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->getSelectedTags()Ljava/util/List;

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedVisibility:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    .line 579
    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->getSessionVisibility()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;

    move-result-object v6

    iget-object v11, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedClub:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    if-nez v11, :cond_4

    .line 573
    :goto_1
    invoke-virtual/range {v0 .. v8}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->createLfgSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/util/Date;Ljava/util/List;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchHandleVisibility;Ljava/lang/Long;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 582
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 583
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 584
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 621
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->updateAdapter()V

    .line 622
    .end local v8    # "locale":Ljava/lang/String;
    :goto_2
    return-void

    .line 552
    .restart local v9    # "cal":Ljava/util/Calendar;
    .restart local v10    # "now":Ljava/util/Date;
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const-string v1, ""

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0706d4

    .line 554
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 555
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 552
    invoke-virtual {v0, v1, v2, v3, v7}, Lcom/microsoft/xbox/toolkit/DialogManager;->showFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_2

    .line 559
    .end local v9    # "cal":Ljava/util/Calendar;
    .end local v10    # "now":Ljava/util/Date;
    :cond_2
    iget v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->dateOffset:I

    if-lez v0, :cond_0

    .line 560
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const-string v1, ""

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0706d5

    .line 562
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 563
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 560
    invoke-virtual {v0, v1, v2, v3, v7}, Lcom/microsoft/xbox/toolkit/DialogManager;->showFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_2

    .line 571
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->languages:Ljava/util/List;

    iget v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->languageOffset:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;->languageCode()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    .line 579
    .restart local v8    # "locale":Ljava/lang/String;
    :cond_4
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedClub:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 580
    invoke-virtual {v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    goto :goto_1
.end method

.method private isHostingLfg()Z
    .locals 4

    .prologue
    .line 625
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->upcomingLfgs:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 626
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->upcomingLfgs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 627
    .local v0, "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->getHostXuid()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 628
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->scheduledTime()Ljava/util/Date;

    move-result-object v2

    if-nez v2, :cond_0

    .line 629
    const/4 v1, 0x1

    .line 634
    .end local v0    # "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$createLfg$7(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    .prologue
    .line 506
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->upcomingLfgs:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 507
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->upcomingLfgs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .line 508
    .local v0, "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->getHostXuid()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 509
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->scheduledTime()Ljava/util/Date;

    move-result-object v2

    if-nez v2, :cond_0

    .line 510
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->id()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 511
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->isLeavingPreviousSession:Z

    .line 512
    sget-object v1, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->MULTIPLAYER_SESSION_MODEL:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->id()Ljava/lang/String;

    move-result-object v2

    const-string v3, "me"

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->declineLfgMember(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    .end local v0    # "handle":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    :cond_1
    return-void
.end method

.method static synthetic lambda$createLfgInternal$8(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;
    .param p1, "sessionHandle"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 586
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->isCreatingLfg:Z

    .line 588
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 589
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "SessionRef"

    .line 591
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->sessionRef()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-result-object v2

    .line 589
    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 592
    const-string v1, "LFG - Post Successful"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 595
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->shouldShareLfgToFeed:Z

    if-eqz v1, :cond_0

    .line 596
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->postedTime()Ljava/util/Date;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 597
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->shareLfgToFeed(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V

    .line 603
    :cond_0
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/viewmodel/GameProfileLfgScreenViewModel;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->AddForceRefresh(Ljava/lang/Class;)V

    .line 604
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/viewmodel/PartyAndLfgScreenViewModel;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->AddForceRefresh(Ljava/lang/Class;)V

    .line 605
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->AddForceRefresh(Ljava/lang/Class;)V

    .line 606
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->AddForceRefresh(Ljava/lang/Class;)V

    .line 608
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->goBack()V

    .line 609
    return-void

    .line 599
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Received invalid session handle after creating new handle! RelatedInfo.postedTime is null"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic lambda$createLfgInternal$9(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;Ljava/lang/Throwable;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;
    .param p1, "error"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 611
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->isCreatingLfg:Z

    .line 615
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0706d6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(Ljava/lang/String;)V

    .line 617
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->updateAdapter()V

    .line 618
    return-void
.end method

.method static synthetic lambda$load$0(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    .prologue
    .line 340
    const-string v0, "https://account.xbox.com/settings"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->NavigateToUri(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic lambda$load$1(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    .prologue
    .line 342
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->goBack()V

    return-void
.end method

.method static synthetic lambda$shareLfgToFeed$2(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;)Ljava/lang/Object;
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;
    .param p1, "postTypeData"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 428
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getUserPostsService()Lcom/microsoft/xbox/service/activityFeed/IUserPostsService;

    move-result-object v2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedClub:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedClub:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v0

    :goto_0
    invoke-interface {v2, p1, v0, v1}, Lcom/microsoft/xbox/service/activityFeed/IUserPostsService;->shareLfgPost(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;J)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;

    move-result-object v0

    return-object v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$shareLfgToFeed$3()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 431
    return-void
.end method

.method static synthetic lambda$shareLfgToFeed$4(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;
    .param p1, "err"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 433
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to create user post when making LFG"

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 434
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->showSharingError()V

    .line 435
    return-void
.end method

.method static synthetic lambda$showEditDescriptionDialog$6(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;Ljava/lang/String;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;
    .param p1, "updatedText"    # Ljava/lang/String;

    .prologue
    .line 488
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissEditDialog()V

    .line 490
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 491
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->setDescription(Ljava/lang/String;)V

    .line 493
    :cond_0
    return-void
.end method

.method static synthetic lambda$showTagPickerDialog$5(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;Ljava/util/List;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;
    .param p1, "selectedTags"    # Ljava/util/List;

    .prologue
    .line 474
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopyWritable(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedTags:Ljava/util/List;

    .line 475
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->updateAdapter()V

    .line 476
    return-void
.end method

.method private onLoadUpcomingLfgSessionsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 451
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLoadUpcomingLfgSessionsCompleted - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 466
    :goto_0
    return-void

    .line 457
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->MULTIPLAYER_SESSION_MODEL:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Upcoming:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->getSessionsResult(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->upcomingLfgs:Ljava/util/List;

    .line 458
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLoadUpcomingLfgSessionsCompleted - number of sessions: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->upcomingLfgs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 463
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to load upcoming LFG sessions"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 453
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private shareLfgToFeed(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V
    .locals 14
    .param p1, "sessionHandle"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 394
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 396
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->sessionRef()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;

    move-result-object v11

    .line 397
    .local v11, "sessionRef":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->searchAttributes()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;

    move-result-object v10

    .line 399
    .local v10, "searchAttributes":Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;
    if-eqz v11, :cond_3

    if-eqz v10, :cond_3

    .line 400
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v0

    .line 401
    .local v0, "hostXuid":Ljava/lang/String;
    iget-wide v12, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedTitleId:J

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 402
    .local v1, "selectedTitleId":Ljava/lang/String;
    invoke-virtual {v11}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionReference;->scid()Ljava/lang/String;

    move-result-object v2

    .line 403
    .local v2, "scid":Ljava/lang/String;
    const/4 v4, 0x0

    .line 404
    .local v4, "description":Ljava/lang/String;
    invoke-virtual {v10}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;->tags()Ljava/util/List;

    move-result-object v5

    .line 405
    .local v5, "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v10}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerSearchAttributes;->achievementIds()Ljava/util/List;

    move-result-object v6

    .line 406
    .local v6, "achievementIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .line 407
    .local v7, "scheduledTime":Ljava/util/Date;
    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    .line 409
    .local v8, "postedTime":Ljava/util/Date;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 410
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->description()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 411
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->description()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$SessionDescription;->text()Ljava/lang/String;

    move-result-object v4

    .line 413
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->scheduledTime()Ljava/util/Date;

    move-result-object v7

    .line 414
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RelatedInfo;->postedTime()Ljava/util/Date;

    move-result-object v8

    .line 417
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->id()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 421
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->id()Ljava/lang/String;

    move-result-object v3

    .line 418
    invoke-static/range {v0 .. v8}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;->with(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/Date;Ljava/util/Date;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;

    move-result-object v9

    .line 428
    .local v9, "postTypeData":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
    invoke-static {p0, v9}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;)Ljava/util/concurrent/Callable;

    move-result-object v3

    invoke-static {v3}, Lio/reactivex/Completable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Completable;

    move-result-object v3

    .line 429
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v12

    invoke-virtual {v3, v12}, Lio/reactivex/Completable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object v3

    .line 430
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v12

    invoke-virtual {v3, v12}, Lio/reactivex/Completable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object v3

    invoke-static {}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$$Lambda$4;->lambdaFactory$()Lio/reactivex/functions/Action;

    move-result-object v12

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;)Lio/reactivex/functions/Consumer;

    move-result-object v13

    .line 431
    invoke-virtual {v3, v12, v13}, Lio/reactivex/Completable;->subscribe(Lio/reactivex/functions/Action;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 444
    .end local v0    # "hostXuid":Ljava/lang/String;
    .end local v1    # "selectedTitleId":Ljava/lang/String;
    .end local v2    # "scid":Ljava/lang/String;
    .end local v4    # "description":Ljava/lang/String;
    .end local v5    # "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v6    # "achievementIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v7    # "scheduledTime":Ljava/util/Date;
    .end local v8    # "postedTime":Ljava/util/Date;
    .end local v9    # "postTypeData":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostTypeData;
    :goto_0
    return-void

    .line 437
    .restart local v0    # "hostXuid":Ljava/lang/String;
    .restart local v1    # "selectedTitleId":Ljava/lang/String;
    .restart local v2    # "scid":Ljava/lang/String;
    .restart local v4    # "description":Ljava/lang/String;
    .restart local v5    # "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v6    # "achievementIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v7    # "scheduledTime":Ljava/util/Date;
    .restart local v8    # "postedTime":Ljava/util/Date;
    :cond_2
    sget-object v3, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->TAG:Ljava/lang/String;

    const-string v12, "HostXuid or session ID were null when trying to share LFG during creation"

    invoke-static {v3, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->showSharingError()V

    goto :goto_0

    .line 441
    .end local v0    # "hostXuid":Ljava/lang/String;
    .end local v1    # "selectedTitleId":Ljava/lang/String;
    .end local v2    # "scid":Ljava/lang/String;
    .end local v4    # "description":Ljava/lang/String;
    .end local v5    # "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v6    # "achievementIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v7    # "scheduledTime":Ljava/util/Date;
    .end local v8    # "postedTime":Ljava/util/Date;
    :cond_3
    sget-object v3, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->TAG:Ljava/lang/String;

    const-string v12, "Session Reference or search attributes were null when trying to share LFG during creation"

    invoke-static {v3, v12}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->showSharingError()V

    goto :goto_0
.end method

.method private showSharingError()V
    .locals 3

    .prologue
    .line 447
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->getScreen()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070b6f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 448
    return-void
.end method


# virtual methods
.method public createLfg(Z)V
    .locals 8
    .param p1, "shareToFeed"    # Z

    .prologue
    const/4 v6, 0x0

    .line 497
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->shouldShareLfgToFeed:Z

    .line 499
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->scid:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 500
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->isHostingLfg()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->timeOffset:I

    if-nez v0, :cond_0

    .line 501
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const-string v1, ""

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0706b3

    .line 503
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0706b4

    .line 504
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;)Ljava/lang/Runnable;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f0706b5

    .line 518
    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 501
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/DialogManager;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 531
    :goto_0
    return-void

    .line 521
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->createLfgInternal()V

    goto :goto_0

    .line 524
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to create LFG because SCID is null"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const-string v1, ""

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070b6d

    .line 527
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0707c7

    .line 528
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 525
    invoke-virtual {v0, v1, v2, v3, v6}, Lcom/microsoft/xbox/toolkit/DialogManager;->showFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public getDateOptions()Ljava/util/List;
    .locals 8
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x7

    .line 194
    const/4 v3, 0x7

    .line 195
    .local v3, "daysInAWeek":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 196
    .local v2, "dateOptions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Ljava/text/SimpleDateFormat;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0706b6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-direct {v1, v5, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 198
    .local v1, "dateFormat":Ljava/text/SimpleDateFormat;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v7, :cond_2

    .line 199
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 201
    .local v0, "cal":Ljava/util/Calendar;
    if-nez v4, :cond_0

    .line 202
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0706f1

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 198
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 203
    :cond_0
    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 204
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0706f2

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 206
    :cond_1
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 207
    const/4 v5, 0x6

    invoke-virtual {v0, v5, v4}, Ljava/util/Calendar;->add(II)V

    .line 208
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 212
    .end local v0    # "cal":Ljava/util/Calendar;
    :cond_2
    return-object v2
.end method

.method public getDefaultLanguageOffset()I
    .locals 9

    .prologue
    .line 258
    iget v7, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->defaultLanguageOffSet:I

    if-gez v7, :cond_1

    .line 259
    const/4 v0, -0x1

    .line 260
    .local v0, "en_us":I
    const/4 v6, -0x1

    .line 261
    .local v6, "region_match":I
    const/4 v4, -0x1

    .line 262
    .local v4, "language_match":I
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v5

    .line 263
    .local v5, "region":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    .line 264
    .local v3, "language":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->languages:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ge v1, v7, :cond_0

    .line 265
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->languages:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;

    .line 266
    .local v2, "lan":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;->regionCode()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 267
    move v6, v1

    .line 278
    .end local v2    # "lan":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;
    :cond_0
    if-ltz v6, :cond_5

    .end local v6    # "region_match":I
    :goto_1
    iput v6, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->defaultLanguageOffSet:I

    .line 281
    .end local v0    # "en_us":I
    .end local v1    # "i":I
    .end local v3    # "language":Ljava/lang/String;
    .end local v4    # "language_match":I
    .end local v5    # "region":Ljava/lang/String;
    :cond_1
    iget v7, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->defaultLanguageOffSet:I

    return v7

    .line 269
    .restart local v0    # "en_us":I
    .restart local v1    # "i":I
    .restart local v2    # "lan":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;
    .restart local v3    # "language":Ljava/lang/String;
    .restart local v4    # "language_match":I
    .restart local v5    # "region":Ljava/lang/String;
    .restart local v6    # "region_match":I
    :cond_2
    if-gez v4, :cond_3

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;->languageCode()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 270
    move v4, v1

    .line 273
    :cond_3
    if-gez v0, :cond_4

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;->regionCode()Ljava/lang/String;

    move-result-object v7

    const-string v8, "en-US"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 274
    move v0, v1

    .line 264
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 278
    .end local v2    # "lan":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;
    :cond_5
    if-ltz v4, :cond_6

    move v6, v4

    goto :goto_1

    :cond_6
    move v6, v0

    goto :goto_1
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getLanguageOptions()Ljava/util/List;
    .locals 7
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 240
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 241
    .local v1, "languageOptions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v3, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->MULTIPLAYER_SESSION_MODEL:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->getLfgLanguages()Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;

    move-result-object v2

    .line 242
    .local v2, "list":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;->languageList()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 243
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->languages:Ljava/util/List;

    .line 244
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->languages:Ljava/util/List;

    const-string v4, "English"

    const-string v5, "en"

    const-string v6, "en-US"

    invoke-static {v4, v5, v6}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;->with(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    const-string v3, "English"

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    :cond_1
    return-object v1

    .line 247
    :cond_2
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;->languageList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;

    .line 248
    .local v0, "lan":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;->regionCode()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 249
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->languages:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 250
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;->displayName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getNeedCountLabels()Ljava/util/List;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->needCountLabels:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getSelectedTags()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 291
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedTags:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedTitle()Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 140
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->TITLE_HUB_MODEL:Lcom/microsoft/xbox/service/model/TitleHubModel;

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedTitleId:J

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getResult(J)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v0

    return-object v0
.end method

.method public getShareDescription()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 179
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedClub:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0706ec

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 182
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0706ee

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTimeOptions()Ljava/util/List;
    .locals 9
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v8, 0xe

    const/4 v7, 0x0

    .line 217
    const/16 v2, 0x60

    .line 219
    .local v2, "segmentsInDay":I
    new-instance v4, Ljava/util/ArrayList;

    const/16 v5, 0x61

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 220
    .local v4, "timeOptions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0706f3

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    const/4 v5, 0x3

    invoke-static {v5}, Ljava/text/SimpleDateFormat;->getTimeInstance(I)Ljava/text/DateFormat;

    move-result-object v3

    .line 222
    .local v3, "timeFormat":Ljava/text/DateFormat;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 223
    .local v0, "cal":Ljava/util/Calendar;
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 225
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v5, 0x60

    if-ge v1, v5, :cond_0

    .line 227
    const/16 v5, 0xb

    invoke-virtual {v0, v5, v7}, Ljava/util/Calendar;->set(II)V

    .line 228
    const/16 v5, 0xc

    invoke-virtual {v0, v5, v7}, Ljava/util/Calendar;->set(II)V

    .line 229
    const/16 v5, 0xd

    invoke-virtual {v0, v5, v7}, Ljava/util/Calendar;->set(II)V

    .line 230
    invoke-virtual {v0, v8, v7}, Ljava/util/Calendar;->set(II)V

    .line 232
    const v5, 0xdbba0

    mul-int/2addr v5, v1

    invoke-virtual {v0, v8, v5}, Ljava/util/Calendar;->set(II)V

    .line 233
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 225
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 235
    :cond_0
    return-object v4
.end method

.method public getVisibilityOptions()Ljava/util/List;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 162
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->values()[Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    move-result-object v1

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 163
    .local v0, "options":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedClub:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 164
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedClub:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedClub:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->join()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Nonmember:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    if-ne v1, v2, :cond_0

    .line 165
    sget-object v1, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->XboxLive:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->Club:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedClub:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->setDisplayName(Ljava/lang/String;)V

    .line 169
    sget-object v1, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->Club:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    :goto_0
    return-object v0

    .line 171
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->XboxLive:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    sget-object v1, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->Private:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public isBlockingBusy()Z
    .locals 1

    .prologue
    .line 327
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->isCreatingLfg:Z

    return v0
.end method

.method public isBusy()Z
    .locals 4

    .prologue
    .line 322
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->TITLE_HUB_MODEL:Lcom/microsoft/xbox/service/model/TitleHubModel;

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedTitleId:J

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/TitleHubModel;->isLoading(J)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->isCreatingLfg:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 8
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 332
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Loading title ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedTitleId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v7

    .line 335
    .local v7, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/ProfileModel;->getCanCommunicateWithTextAndVoice()Z

    move-result v0

    if-nez v0, :cond_0

    .line 336
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const-string v1, ""

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0706ba

    .line 338
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0706b9

    .line 339
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;)Ljava/lang/Runnable;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070736

    .line 341
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;)Ljava/lang/Runnable;

    move-result-object v6

    .line 336
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/DialogManager;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 345
    :cond_0
    if-nez p1, :cond_1

    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->MULTIPLAYER_SESSION_MODEL:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Upcoming:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->shouldRefresh(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 346
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Refreshing upcoming sessions"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->MULTIPLAYER_SESSION_MODEL:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Upcoming:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->loadLfgSessionsAsync(ZLcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)V

    .line 353
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->scid:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->TITLE_HUB_MODEL:Lcom/microsoft/xbox/service/model/TitleHubModel;

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedTitleId:J

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/TitleHubModel;->shouldRefresh(J)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 354
    :cond_2
    sget-object v1, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->TITLE_HUB_MODEL:Lcom/microsoft/xbox/service/model/TitleHubModel;

    if-nez p1, :cond_3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->scid:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_3
    const/4 v0, 0x1

    :goto_1
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedTitleId:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/microsoft/xbox/service/model/TitleHubModel;->loadAsync(ZJ)V

    .line 356
    :cond_4
    return-void

    .line 349
    :cond_5
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Using cached upcoming sessions"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->MULTIPLAYER_SESSION_MODEL:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    sget-object v1, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;->Upcoming:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->getSessionsResult(Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager$LfgFullListType;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->upcomingLfgs:Ljava/util/List;

    goto :goto_0

    .line 354
    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 311
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getCreateLfgDetailsAdapter(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 312
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 305
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->TITLE_HUB_MODEL:Lcom/microsoft/xbox/service/model/TitleHubModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/TitleHubModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 306
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->MULTIPLAYER_SESSION_MODEL:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 307
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 316
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->TITLE_HUB_MODEL:Lcom/microsoft/xbox/service/model/TitleHubModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/TitleHubModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 317
    sget-object v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->MULTIPLAYER_SESSION_MODEL:Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/MultiplayerSessionModelManager;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 318
    return-void
.end method

.method public onTapCloseButton()V
    .locals 0

    .prologue
    .line 300
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->goBack()V

    .line 301
    return-void
.end method

.method public setDateOffset(I)V
    .locals 0
    .param p1, "dateOffset"    # I

    .prologue
    .line 149
    iput p1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->dateOffset:I

    .line 150
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 285
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->description:Ljava/lang/String;

    .line 286
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->updateAdapter()V

    .line 287
    return-void
.end method

.method public setLanguageOffset(I)V
    .locals 0
    .param p1, "languageOffset"    # I

    .prologue
    .line 157
    iput p1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->languageOffset:I

    .line 158
    return-void
.end method

.method public setNeedCount(I)V
    .locals 0
    .param p1, "needCount"    # I

    .prologue
    .line 295
    iput p1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->needCount:I

    .line 296
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->updateAdapter()V

    .line 297
    return-void
.end method

.method public setSelectedVisibility(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;)V
    .locals 0
    .param p1, "visibility"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 187
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 189
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedVisibility:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    .line 190
    return-void
.end method

.method public setTimeOffset(I)V
    .locals 0
    .param p1, "timeOffset"    # I

    .prologue
    .line 153
    iput p1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->timeOffset:I

    .line 154
    return-void
.end method

.method public showEditDescriptionDialog()V
    .locals 7

    .prologue
    const v5, 0x7f0706ae

    .line 481
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 482
    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 483
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->getDescription()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0b0017

    .line 484
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 485
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 486
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0004

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;)Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$OnEditTextCompletedHandler;

    move-result-object v6

    .line 481
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showEditTextDialog(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ILcom/microsoft/xbox/xle/app/dialog/EditTextDialog$OnEditTextCompletedHandler;)V

    .line 494
    return-void
.end method

.method public showTagPickerDialog()V
    .locals 6

    .prologue
    .line 470
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg;->trackAddTags()V

    .line 471
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedTags:Ljava/util/List;

    .line 472
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopyWritable(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;)Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;

    move-result-object v2

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedTitleId:J

    .line 477
    invoke-static {v4, v5}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;->enableAll(J)Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;

    move-result-object v3

    .line 471
    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showTagPickerDialog(Ljava/util/List;Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;)V

    .line 478
    return-void
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 6
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 360
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 361
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/UpdateData;

    .line 363
    .local v0, "result":Lcom/microsoft/xbox/service/model/UpdateData;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 364
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v2

    .line 366
    .local v2, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v3, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 387
    sget-object v3, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown update type ignored: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    .end local v2    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :cond_0
    :goto_0
    return-void

    .line 368
    .restart local v2    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :pswitch_0
    sget-object v3, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->TITLE_HUB_MODEL:Lcom/microsoft/xbox/service/model/TitleHubModel;

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->selectedTitleId:J

    invoke-virtual {v3, v4, v5}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getResult(J)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v1

    .line 370
    .local v1, "titleData":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    if-eqz v1, :cond_1

    .line 371
    iget-object v3, v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->serviceConfigId:Ljava/lang/String;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->scid:Ljava/lang/String;

    .line 374
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->updateAdapter()V

    goto :goto_0

    .line 377
    .end local v1    # "titleData":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    :pswitch_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->onLoadUpcomingLfgSessionsCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    goto :goto_0

    .line 380
    :pswitch_2
    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->isLeavingPreviousSession:Z

    if-eqz v3, :cond_0

    .line 381
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->isLeavingPreviousSession:Z

    .line 382
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->createLfgInternal()V

    goto :goto_0

    .line 366
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
