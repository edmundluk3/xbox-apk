.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyValue$GameDetail;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GameDetail"
.end annotation


# static fields
.field public static final CommunityDVRRecent:Ljava/lang/String; = "CommunityDVRRecent"

.field public static final CommunityDVRSaved:Ljava/lang/String; = "CommunityDVRSaved"

.field public static final CommunityScreenshotRecent:Ljava/lang/String; = "CommunityScreenshotRecent"

.field public static final CommunityScreenshotSaved:Ljava/lang/String; = "CommunityScreenshotSaved"

.field public static final Everything:Ljava/lang/String; = "Everything"

.field public static final Friend:Ljava/lang/String; = "Friend"

.field public static final GameClips:Ljava/lang/String; = "GameClips"

.field public static final MyDVRRecent:Ljava/lang/String; = "MyDVRRecent"

.field public static final MyDVRSaved:Ljava/lang/String; = "MyDVRSaved"

.field public static final MyScreenshotRecent:Ljava/lang/String; = "MyScreenshotRecent"

.field public static final MyScreenshotSaved:Ljava/lang/String; = "MyScreenshotSaved"

.field public static final Screenshots:Ljava/lang/String; = "Screenshots"

.field public static final VIP:Ljava/lang/String; = "VIP"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
