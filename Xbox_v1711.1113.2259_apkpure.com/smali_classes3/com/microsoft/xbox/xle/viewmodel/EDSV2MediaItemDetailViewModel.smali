.class public abstract Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;
.source "EDSV2MediaItemDetailViewModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;",
        ">",
        "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final MAX_ACTORS:I = 0x4


# instance fields
.field private actors:Ljava/lang/String;

.field private cleanedDescription:Ljava/lang/String;

.field private forceRefresh:Z

.field protected needToTryAddActivityPane:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel<TT;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;-><init>()V

    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->needToTryAddActivityPane:Z

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->forceRefresh:Z

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->cleanedDescription:Ljava/lang/String;

    .line 38
    return-void
.end method


# virtual methods
.method public getBackgroundImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBoxArtBackgroundColor()I
    .locals 2
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 198
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel<TT;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    .line 199
    .local v0, "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBoxArtBackgroundColor()I

    move-result v1

    goto :goto_0
.end method

.method public getCurrentScreenData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1

    .prologue
    .line 150
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->hasValidData()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDefaultImageRid()I
    .locals 1

    .prologue
    .line 144
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaType()I

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v0

    return v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->cleanedDescription:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getDescription()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->cleanedDescription:Ljava/lang/String;

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->cleanedDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getDurationInMinutes()I
    .locals 1

    .prologue
    .line 114
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getDurationInMinutes()I

    move-result v0

    return v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMediaGroup()I
    .locals 1

    .prologue
    .line 118
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaGroup()I

    move-result v0

    return v0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 122
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaType()I

    move-result v0

    return v0
.end method

.method public getParentalRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getParentalRating()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParentalRatings()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getParentalRatings()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPosterImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getPosterImageUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProviders()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;",
            ">;"
        }
    .end annotation

    .prologue
    .line 140
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getProviders()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getReleaseDate()Ljava/lang/String;
    .locals 2

    .prologue
    .line 101
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel<TT;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getReleaseDate()Ljava/util/Date;

    move-result-object v0

    .line 102
    .local v0, "date":Ljava/util/Date;
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getDateStringAsMonthDateYear(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getReleaseYear()Ljava/lang/String;
    .locals 4

    .prologue
    .line 62
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel<TT;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getReleaseDate()Ljava/util/Date;

    move-result-object v0

    .line 63
    .local v0, "date":Ljava/util/Date;
    if-eqz v0, :cond_1

    .line 64
    invoke-virtual {v0}, Ljava/util/Date;->getYear()I

    move-result v1

    add-int/lit16 v1, v1, 0x76c

    const/16 v2, 0xaef

    if-lt v1, v2, :cond_0

    .line 65
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f0703e9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 70
    :goto_0
    return-object v1

    .line 67
    :cond_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 70
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getStars()Ljava/lang/String;
    .locals 6

    .prologue
    .line 74
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel<TT;>;"
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->actors:Ljava/lang/String;

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getActors()Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getActors()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_3

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 76
    .local v0, "builder":Ljava/lang/StringBuilder;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getActors()Ljava/util/ArrayList;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 77
    .local v3, "obj":Ljava/lang/Object;
    instance-of v4, v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;

    if-eqz v4, :cond_0

    move-object v1, v3

    .line 78
    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;

    .line 79
    .local v1, "contributor":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;
    iget-object v4, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;->Name:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    .end local v1    # "contributor":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;
    :cond_0
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getActors()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_2

    const/4 v4, 0x4

    if-ge v2, v4, :cond_2

    .line 83
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getActors()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 85
    instance-of v4, v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;

    if-eqz v4, :cond_1

    move-object v1, v3

    .line 86
    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;

    .line 88
    .restart local v1    # "contributor":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;
    const-string v4, "; "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    iget-object v4, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;->Name:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    .end local v1    # "contributor":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 94
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->actors:Ljava/lang/String;

    .line 97
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v2    # "i":I
    .end local v3    # "obj":Ljava/lang/Object;
    :cond_3
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->actors:Ljava/lang/String;

    return-object v4
.end method

.method public getSubscriptionImageResourceId()I
    .locals 2
    .annotation build Landroid/support/annotation/IntRange;
        from = 0x0L
    .end annotation

    .prologue
    .line 179
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getSubscriptionType()Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->XboxGold:Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    if-ne v0, v1, :cond_0

    .line 180
    const v0, 0x7f020215

    .line 183
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSubscriptionText()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 165
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel<TT;>;"
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel$1;->$SwitchMap$com$microsoft$xbox$service$store$StoreDataTypes$SubscriptionType:[I

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getSubscriptionType()Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 171
    const-string v0, ""

    :goto_0
    return-object v0

    .line 167
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070d5a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 169
    :pswitch_1
    const-string v0, "EA Access"

    goto :goto_0

    .line 165
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitleId()J
    .locals 2

    .prologue
    .line 42
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel<TT;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getTitleId()J

    move-result-wide v0

    return-wide v0
.end method

.method public isBlockingBusy()Z
    .locals 1

    .prologue
    .line 135
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel<TT;>;"
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->isLaunching:Z

    return v0
.end method

.method public load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 188
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel<TT;>;"
    if-eqz p1, :cond_0

    .line 189
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->needToTryAddActivityPane:Z

    .line 192
    :cond_0
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;->forceRefresh:Z

    .line 193
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemViewModel;->load(Z)V

    .line 194
    return-void
.end method

.method public showStarRatingDialog()V
    .locals 0

    .prologue
    .line 159
    .local p0, "this":Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel;, "Lcom/microsoft/xbox/xle/viewmodel/EDSV2MediaItemDetailViewModel<TT;>;"
    return-void
.end method
