.class public Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;
.source "AttainmentDetailScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private hasFriendsWhoEarnedAchievement:Z

.field private isAchievementDetailLoading:Z

.field private isLoadingTitleData:Z

.field protected item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

.field private items:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;",
            ">;"
        }
    .end annotation
.end field

.field private loadAchievementDetailDataTask:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;

.field protected loadFriendsWhoEarnedThisAchievement:Z

.field private loadTitleDataTask:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;

.field private final model:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

.field protected titleDetailInfo:Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

.field private titleId:J

.field private titleImageDetail:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

.field private titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

.field protected viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 70
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;-><init>()V

    .line 61
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->hasFriendsWhoEarnedAchievement:Z

    .line 64
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->loadFriendsWhoEarnedThisAchievement:Z

    .line 67
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 72
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 73
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 74
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 75
    if-eqz v0, :cond_1

    .line 76
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getTitleDetailInfo()Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleDetailInfo:Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    .line 77
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleDetailInfo:Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    if-nez v2, :cond_0

    .line 78
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getGameProgressAchievement()Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 79
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 80
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    instance-of v2, v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    iget-object v1, v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->serviceConfigId:Ljava/lang/String;

    .line 81
    .local v1, "serviceConfigId":Ljava/lang/String;
    :goto_0
    new-instance v4, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->id:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getResponseType()Lcom/microsoft/xbox/service/network/managers/SLSResponseType;

    move-result-object v2

    sget-object v6, Lcom/microsoft/xbox/service/network/managers/SLSResponseType;->XboxOne:Lcom/microsoft/xbox/service/network/managers/SLSResponseType;

    if-ne v2, v6, :cond_3

    const-string v2, "dgame"

    :goto_1
    invoke-direct {v4, v5, v1, v2}, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleDetailInfo:Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    .line 84
    .end local v1    # "serviceConfigId":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleDetailInfo:Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 86
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getTitleId()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleId:J

    .line 87
    iget-wide v4, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleId:J

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_2
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 89
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getAttainmentDetailAdapter(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 90
    return-void

    .line 80
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 81
    .restart local v1    # "serviceConfigId":Ljava/lang/String;
    :cond_3
    const-string v2, "360game"

    goto :goto_1

    .end local v1    # "serviceConfigId":Ljava/lang/String;
    :cond_4
    move v2, v3

    .line 87
    goto :goto_2
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleId:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)Lcom/microsoft/xbox/service/model/TitleModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->onLoadTitleDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isLoadingTitleData:Z

    return p1
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I
    .param p4, "x4"    # Z

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->onLoadAchievementDetailDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;IZ)V

    return-void
.end method

.method static synthetic access$702(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isAchievementDetailLoading:Z

    return p1
.end method

.method static synthetic lambda$onAchievementSetAsClubImage$0()V
    .locals 2

    .prologue
    .line 417
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const v1, 0x7f0701bd

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(I)V

    .line 418
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Error updating background image"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    return-void
.end method

.method private onLoadAchievementDetailDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;IZ)V
    .locals 10
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "scid"    # Ljava/lang/String;
    .param p3, "achievementId"    # I
    .param p4, "friendsWhoEarnedRequestFailed"    # Z

    .prologue
    const/4 v5, 0x0

    .line 314
    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isAchievementDetailLoading:Z

    .line 316
    sget-object v6, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 364
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->updateAdapter()V

    .line 365
    return-void

    .line 320
    :pswitch_0
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v6, p2, p3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getAchievementDetailData(Ljava/lang/String;I)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    move-result-object v6

    iput-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 321
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleImageDetailsData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v6

    iput-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleImageDetail:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 322
    iget-boolean v6, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->loadFriendsWhoEarnedThisAchievement:Z

    if-eqz v6, :cond_5

    .line 323
    if-nez p4, :cond_3

    .line 324
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->items:Ljava/util/ArrayList;

    .line 326
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/ProfileModel;->getFriendsWhoEarnedAchievement()Ljava/util/ArrayList;

    move-result-object v1

    .line 327
    .local v1, "friendsWhoEarnedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementItem;>;"
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 329
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileFriendsWhoEarnedAchievement()Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    move-result-object v3

    .line 331
    .local v3, "profileUsersLookup":Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;, "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementItem;

    .line 332
    .local v2, "item":Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementItem;
    iget-object v7, v2, Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementItem;->userXuid:Ljava/lang/String;

    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;

    .line 333
    .local v4, "user":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    if-nez v4, :cond_2

    .line 334
    sget-object v7, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Could not load profile for xuid: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v2, Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementItem;->userXuid:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 335
    :cond_2
    iget-object v7, v2, Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementItem;->userXuid:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    iget-object v7, v2, Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementItem;->userXuid:Ljava/lang/String;

    iget-object v8, v4, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->id:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 336
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;-><init>()V

    .line 337
    .local v0, "friendItem":Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;
    iget-object v7, v2, Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementItem;->userXuid:Ljava/lang/String;

    iput-object v7, v0, Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;->userXuid:Ljava/lang/String;

    .line 338
    iget v7, v2, Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementItem;->gamerscore:I

    iput v7, v0, Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;->gamerscore:I

    .line 339
    iget-boolean v7, v2, Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementItem;->isSecret:Z

    iput-boolean v7, v0, Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;->isSecret:Z

    .line 340
    sget-object v7, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->Gamertag:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->getSettingValue(Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v0, Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;->gamerTag:Ljava/lang/String;

    .line 341
    sget-object v7, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->RealName:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->getSettingValue(Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v0, Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;->realName:Ljava/lang/String;

    .line 342
    sget-object v7, Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;->AppDisplayPicRaw:Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;

    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;->getSettingValue(Lcom/microsoft/xbox/service/model/sls/UserProfileSetting;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v0, Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;->displayPicUrl:Ljava/lang/String;

    .line 343
    iget-object v7, v2, Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementItem;->date:Ljava/util/Date;

    iput-object v7, v0, Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;->date:Ljava/util/Date;

    .line 345
    iget-object v7, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->items:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 353
    .end local v0    # "friendItem":Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;
    .end local v1    # "friendsWhoEarnedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementItem;>;"
    .end local v2    # "item":Lcom/microsoft/xbox/service/network/managers/FriendsAchievementResultContainer$FriendsAchievementItem;
    .end local v3    # "profileUsersLookup":Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;, "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable<Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;>;"
    .end local v4    # "user":Lcom/microsoft/xbox/service/network/managers/IUserProfileResult$ProfileUser;
    :cond_3
    iget-object v6, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->items:Ljava/util/ArrayList;

    invoke-static {v6}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v6

    if-nez v6, :cond_4

    const/4 v5, 0x1

    :cond_4
    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->hasFriendsWhoEarnedAchievement:Z

    .line 355
    :cond_5
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->updateViewModelState()V

    goto/16 :goto_0

    .line 359
    :pswitch_1
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v6, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v5, v6, :cond_0

    .line 360
    sget-object v5, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto/16 :goto_0

    .line 316
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onLoadTitleDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 6
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v5, 0x0

    .line 368
    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isLoadingTitleData:Z

    .line 369
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 406
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->updateViewModelState()V

    .line 407
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->updateAdapter()V

    .line 408
    return-void

    .line 373
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v3

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleId:J

    invoke-virtual {v3, v4, v5}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getResult(J)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    .line 374
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleImageDetailsData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleImageDetail:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 376
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgress360EarnedAchievements()Ljava/util/ArrayList;

    move-result-object v2

    .line 377
    .local v2, "earnedAchievements":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;>;"
    if-eqz v2, :cond_2

    .line 378
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;

    .line 379
    .local v0, "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;
    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;->id:Ljava/lang/String;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleDetailInfo:Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;->getAchievementId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 380
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    .line 386
    .end local v0    # "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgress360Achievements()Ljava/util/ArrayList;

    move-result-object v1

    .line 387
    .local v1, "allAchievements":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-nez v3, :cond_0

    if-eqz v1, :cond_0

    .line 388
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;

    .line 389
    .restart local v0    # "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;
    iget-object v4, v0, Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;->id:Ljava/lang/String;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleDetailInfo:Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;->getAchievementId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 390
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    goto :goto_0

    .line 402
    .end local v0    # "achievement":Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;
    .end local v1    # "allAchievements":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;>;"
    .end local v2    # "earnedAchievements":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/GameProgress360AchievementsResultContainer$GameProgress360AchievementsItem;>;"
    :pswitch_1
    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->loadFriendsWhoEarnedThisAchievement:Z

    goto :goto_0

    .line 369
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private updateViewModelState()V
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-nez v0, :cond_2

    .line 467
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isAchievementDetailLoading:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isLoadingTitleData:Z

    if-eqz v0, :cond_1

    .line 468
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 475
    :goto_0
    return-void

    .line 470
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 473
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method


# virtual methods
.method public getBackgroundImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleImageDetail:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleImageDetail:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleImageDetail:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleImageDetail:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v0

    .line 144
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v0, :cond_1

    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-ne v0, v1, :cond_0

    .line 174
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->description:Ljava/lang/String;

    .line 179
    :goto_0
    return-object v0

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->lockedDescription:Ljava/lang/String;

    goto :goto_0

    .line 179
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFriendsWhoEarnedAchievementData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->items:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getHasAvatarReward()Z
    .locals 4

    .prologue
    .line 153
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->hasRewards()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    instance-of v1, v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    if-eqz v1, :cond_1

    .line 154
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->rewards:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;

    .line 155
    .local v0, "reward":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;
    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;->type:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "avatar"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 156
    const/4 v1, 0x1

    .line 160
    .end local v0    # "reward":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getAchievementIconUri()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIsChallenge()Z
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    instance-of v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->achievementType:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->achievementType:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "challenge"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 108
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIsRare()Z
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getIsRare()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemGamerScore()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getGamerscore()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->name:Ljava/lang/String;

    .line 133
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPercentComplete()I
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getPercentageComplete()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRarityPercent()F
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getAchievementRarity()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRewards()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$Reward;",
            ">;"
        }
    .end annotation

    .prologue
    .line 195
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    instance-of v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->rewards:Ljava/util/ArrayList;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStartDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    instance-of v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->timeWindow:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->timeWindow:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TimeWindow;->startDate:Ljava/util/Date;

    .line 206
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTimeRemaining()Ljava/util/Date;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getRemainingTime()Ljava/util/Date;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTitleName()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 112
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    instance-of v1, v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    if-eqz v1, :cond_0

    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    .line 114
    .local v0, "achievementsItem":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->titleAssociations:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->titleAssociations:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TitleAssociation;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TitleAssociation;->name:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 115
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->titleAssociations:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TitleAssociation;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$TitleAssociation;->name:Ljava/lang/String;

    .line 126
    .end local v0    # "achievementsItem":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;
    :goto_0
    return-object v1

    .line 119
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleImageDetail:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    if-eqz v1, :cond_1

    .line 120
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleImageDetail:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Name:Ljava/lang/String;

    goto :goto_0

    .line 123
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    if-eqz v1, :cond_2

    .line 124
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleData:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    iget-object v1, v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->name:Ljava/lang/String;

    goto :goto_0

    .line 126
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getUnlockedDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getTimeUnlocked()Ljava/util/Date;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 224
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isAchievementDetailLoading:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->isLoadingTitleData:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGame()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 240
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleDetailInfo:Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    if-eqz v4, :cond_2

    .line 241
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleDetailInfo:Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;->getTitleType()Ljava/lang/String;

    move-result-object v1

    .line 243
    .local v1, "titleType":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 244
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "game"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 261
    .end local v1    # "titleType":Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .restart local v1    # "titleType":Ljava/lang/String;
    :cond_1
    move v2, v3

    .line 247
    goto :goto_0

    .line 252
    .end local v1    # "titleType":Ljava/lang/String;
    :cond_2
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleImageDetail:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    if-eqz v4, :cond_3

    .line 253
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleImageDetail:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v0

    .line 254
    .local v0, "mediaType":I
    const/16 v4, 0x2329

    if-eq v0, v4, :cond_0

    const/16 v4, 0x232a

    if-eq v0, v4, :cond_0

    move v2, v3

    .line 257
    goto :goto_0

    .end local v0    # "mediaType":I
    :cond_3
    move v2, v3

    .line 261
    goto :goto_0
.end method

.method public isSecret()Z
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->isSecret:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStarted()Z
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->NotStarted:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTitleGame()Ljava/lang/Boolean;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 290
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleDetailInfo:Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    if-eqz v2, :cond_1

    .line 291
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleDetailInfo:Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;->getTitleType()Ljava/lang/String;

    move-result-object v1

    .line 293
    .local v1, "titleType":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 294
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "game"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 295
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 310
    .end local v1    # "titleType":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 296
    .restart local v1    # "titleType":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "app"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 297
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 302
    .end local v1    # "titleType":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleImageDetail:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    if-eqz v2, :cond_4

    .line 303
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleImageDetail:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v0

    .line 304
    .local v0, "mediaType":I
    const/16 v2, 0x2329

    if-eq v0, v2, :cond_2

    const/16 v2, 0x232a

    if-ne v0, v2, :cond_3

    .line 305
    :cond_2
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 306
    :cond_3
    const/16 v2, 0x2328

    if-ne v0, v2, :cond_4

    .line 307
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 310
    .end local v0    # "mediaType":I
    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isUnlocked()Z
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isXboxOneGame()Z
    .locals 2

    .prologue
    .line 265
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleDetailInfo:Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    if-eqz v0, :cond_0

    const-string v0, "dgame"

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleDetailInfo:Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;->getTitleType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 3
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 479
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleDetailInfo:Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleDetailInfo:Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;->getServiceConfigId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 480
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->loadAchievementDetailDataTask:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->loadAchievementDetailDataTask:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->cancel()V

    .line 483
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleDetailInfo:Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;->getServiceConfigId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleDetailInfo:Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;->getAchievementId()I

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->loadAchievementDetailDataTask:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;

    .line 484
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->loadAchievementDetailDataTask:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->load(Z)V

    .line 492
    :goto_0
    return-void

    .line 486
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->loadTitleDataTask:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;

    if-eqz v0, :cond_2

    .line 487
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->loadTitleDataTask:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;->cancel()V

    .line 489
    :cond_2
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->loadTitleDataTask:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;

    .line 490
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->loadTitleDataTask:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadTitleDataAsyncTask;->load(Z)V

    goto :goto_0
.end method

.method public navigateToFriendsProfile(Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;)V
    .locals 1
    .param p1, "friendData"    # Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;

    .prologue
    .line 495
    if-eqz p1, :cond_0

    .line 496
    iget-object v0, p1, Lcom/microsoft/xbox/xle/viewmodel/FriendWhoEarnedAchievementItem;->userXuid:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    .line 498
    :cond_0
    return-void
.end method

.method public navigateToGamehub()V
    .locals 2

    .prologue
    .line 269
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleId:J

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->navigateToGameProfile(J)V

    .line 270
    return-void
.end method

.method public onAchievementSetAsClubImage(J)V
    .locals 3
    .param p1, "clubId"    # J

    .prologue
    .line 415
    sget-object v0, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->INSTANCE:Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/service/model/clubs/ClubModelManager;->getClubModel(J)Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$$Lambda$1;->lambdaFactory$()Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->setClubBackgroundImage(Ljava/lang/String;Lcom/microsoft/xbox/service/model/clubs/ClubModel$ClubProfileChangeFailedCallback;)V

    .line 420
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeScreen;

    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;

    invoke-direct {v1, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase$ClubBaseScreenParameters;-><init>(J)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 421
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 94
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getAttainmentDetailAdapter(Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 95
    return-void
.end method

.method protected onStartOverride()V
    .locals 2

    .prologue
    .line 229
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleId:J

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleModel:Lcom/microsoft/xbox/service/model/TitleModel;

    .line 230
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->loadAchievementDetailDataTask:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;

    if-eqz v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->loadAchievementDetailDataTask:Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel$LoadAchievementDetailAsyncTask;->cancel()V

    .line 237
    :cond_0
    return-void
.end method

.method public startShareToFlow()V
    .locals 6

    .prologue
    .line 273
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-nez v1, :cond_0

    .line 274
    const-string v1, "AttainmentDetailScreenViewModel"

    const-string v2, "Profile model is null"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    :goto_0
    return-void

    .line 277
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleDetailInfo:Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    if-nez v1, :cond_1

    .line 278
    const-string v1, "AttainmentDetailScreenViewModel"

    const-string v2, "Title detail info is null"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 282
    :cond_1
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getAchievementDetailItemRootFormat()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleDetailInfo:Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;->getServiceConfigId()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;->titleDetailInfo:Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    .line 283
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;->getAchievementId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 282
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 284
    .local v0, "itemRoot":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    const-string v2, "Achievement Details Share"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;)V

    .line 285
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Achievement:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    invoke-virtual {v1, p0, v0, v2}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showShareDecisionDialog(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)V

    goto :goto_0
.end method

.method public useAsBackgroundImage()V
    .locals 3

    .prologue
    .line 411
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundAchievementDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 412
    return-void
.end method
