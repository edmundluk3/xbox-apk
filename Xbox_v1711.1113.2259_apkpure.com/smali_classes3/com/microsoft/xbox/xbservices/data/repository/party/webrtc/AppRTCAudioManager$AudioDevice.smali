.class public final enum Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;
.super Ljava/lang/Enum;
.source "AppRTCAudioManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AudioDevice"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

.field public static final enum BLUETOOTH:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

.field public static final enum EARPIECE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

.field public static final enum NONE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

.field public static final enum SPEAKER_PHONE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

.field public static final enum WIRED_HEADSET:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;


# instance fields
.field private telemetryName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 47
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    const-string v1, "SPEAKER_PHONE"

    const-string v2, "Speaker phone"

    invoke-direct {v0, v1, v3, v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->SPEAKER_PHONE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    .line 48
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    const-string v1, "WIRED_HEADSET"

    const-string v2, "Wired headset"

    invoke-direct {v0, v1, v4, v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->WIRED_HEADSET:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    .line 49
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    const-string v1, "EARPIECE"

    const-string v2, "Earpiece"

    invoke-direct {v0, v1, v5, v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->EARPIECE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    .line 50
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    const-string v1, "BLUETOOTH"

    const-string v2, "Bluetooth"

    invoke-direct {v0, v1, v6, v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->BLUETOOTH:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    .line 51
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    const-string v1, "NONE"

    const-string v2, "None"

    invoke-direct {v0, v1, v7, v2}, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->NONE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    .line 45
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->SPEAKER_PHONE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->WIRED_HEADSET:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->EARPIECE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->BLUETOOTH:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->NONE:Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    aput-object v1, v0, v7

    sput-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->$VALUES:[Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p3, "telemtryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->telemetryName:Ljava/lang/String;

    .line 56
    iput-object p3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->telemetryName:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 45
    const-class v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->$VALUES:[Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;

    return-object v0
.end method


# virtual methods
.method public getTelemetryName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/AppRTCAudioManager$AudioDevice;->telemetryName:Ljava/lang/String;

    return-object v0
.end method
