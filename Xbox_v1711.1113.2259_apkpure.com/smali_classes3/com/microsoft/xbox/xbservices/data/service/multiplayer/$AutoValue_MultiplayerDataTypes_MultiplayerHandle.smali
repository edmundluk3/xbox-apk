.class abstract Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;
.super Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;
.source "$AutoValue_MultiplayerDataTypes_MultiplayerHandle.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle$Builder;
    }
.end annotation


# instance fields
.field private final activityInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ActivityInfo;

.field private final id:Ljava/lang/String;

.field private final invitedXuid:Ljava/lang/String;

.field private final relatedInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;

.field private final roleInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;

.field private final searchAttributes:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSearchAttributes;

.field private final sessionRef:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;

.field private final type:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ActivityInfo;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSearchAttributes;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "sessionRef"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "activityInfo"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ActivityInfo;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "searchAttributes"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSearchAttributes;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "relatedInfo"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p7, "roleInfo"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p8, "invitedXuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->id:Ljava/lang/String;

    .line 30
    if-nez p2, :cond_0

    .line 31
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null type"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 33
    :cond_0
    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->type:Ljava/lang/String;

    .line 34
    iput-object p3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->sessionRef:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;

    .line 35
    iput-object p4, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->activityInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ActivityInfo;

    .line 36
    iput-object p5, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->searchAttributes:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSearchAttributes;

    .line 37
    iput-object p6, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->relatedInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;

    .line 38
    iput-object p7, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->roleInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;

    .line 39
    iput-object p8, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->invitedXuid:Ljava/lang/String;

    .line 40
    return-void
.end method


# virtual methods
.method public activityInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ActivityInfo;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->activityInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ActivityInfo;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 106
    if-ne p1, p0, :cond_1

    .line 120
    :cond_0
    :goto_0
    return v1

    .line 109
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;

    if-eqz v3, :cond_a

    move-object v0, p1

    .line 110
    check-cast v0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;

    .line 111
    .local v0, "that":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->id:Ljava/lang/String;

    if-nez v3, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->id()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->type:Ljava/lang/String;

    .line 112
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->type()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->sessionRef:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;

    if-nez v3, :cond_4

    .line 113
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->sessionRef()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->activityInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ActivityInfo;

    if-nez v3, :cond_5

    .line 114
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ActivityInfo;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->searchAttributes:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSearchAttributes;

    if-nez v3, :cond_6

    .line 115
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->searchAttributes()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSearchAttributes;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_4
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->relatedInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;

    if-nez v3, :cond_7

    .line 116
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_5
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->roleInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;

    if-nez v3, :cond_8

    .line 117
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->roleInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_6
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->invitedXuid:Ljava/lang/String;

    if-nez v3, :cond_9

    .line 118
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->invitedXuid()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 111
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->id:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->id()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 113
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->sessionRef:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->sessionRef()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 114
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->activityInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ActivityInfo;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ActivityInfo;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_3

    .line 115
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->searchAttributes:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSearchAttributes;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->searchAttributes()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSearchAttributes;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_4

    .line 116
    :cond_7
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->relatedInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->relatedInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_5

    .line 117
    :cond_8
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->roleInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->roleInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_6

    .line 118
    :cond_9
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->invitedXuid:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;->invitedXuid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerHandle;
    :cond_a
    move v1, v2

    .line 120
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    const v3, 0xf4243

    .line 125
    const/4 v0, 0x1

    .line 126
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 127
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->id:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 128
    mul-int/2addr v0, v3

    .line 129
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->type:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 130
    mul-int/2addr v0, v3

    .line 131
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->sessionRef:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 132
    mul-int/2addr v0, v3

    .line 133
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->activityInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ActivityInfo;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 134
    mul-int/2addr v0, v3

    .line 135
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->searchAttributes:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSearchAttributes;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    xor-int/2addr v0, v1

    .line 136
    mul-int/2addr v0, v3

    .line 137
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->relatedInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    xor-int/2addr v0, v1

    .line 138
    mul-int/2addr v0, v3

    .line 139
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->roleInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;

    if-nez v1, :cond_5

    move v1, v2

    :goto_5
    xor-int/2addr v0, v1

    .line 140
    mul-int/2addr v0, v3

    .line 141
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->invitedXuid:Ljava/lang/String;

    if-nez v1, :cond_6

    :goto_6
    xor-int/2addr v0, v2

    .line 142
    return v0

    .line 127
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->id:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 131
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->sessionRef:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    .line 133
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->activityInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ActivityInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    .line 135
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->searchAttributes:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSearchAttributes;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_3

    .line 137
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->relatedInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_4

    .line 139
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->roleInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_5

    .line 141
    :cond_6
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->invitedXuid:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_6
.end method

.method public id()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->id:Ljava/lang/String;

    return-object v0
.end method

.method public invitedXuid()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->invitedXuid:Ljava/lang/String;

    return-object v0
.end method

.method public relatedInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->relatedInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;

    return-object v0
.end method

.method public roleInfo()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->roleInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;

    return-object v0
.end method

.method public searchAttributes()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSearchAttributes;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->searchAttributes:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSearchAttributes;

    return-object v0
.end method

.method public sessionRef()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->sessionRef:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MultiplayerHandle{id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sessionRef="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->sessionRef:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionReference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", activityInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->activityInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$ActivityInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", searchAttributes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->searchAttributes:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSearchAttributes;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", relatedInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->relatedInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RelatedInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", roleInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->roleInfo:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RoleInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", invitedXuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->invitedXuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public type()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/$AutoValue_MultiplayerDataTypes_MultiplayerHandle;->type:Ljava/lang/String;

    return-object v0
.end method
