.class public Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;
.super Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;
.source "ClubInviteScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;
    }
.end annotation


# static fields
.field private static final FOLLOWERS_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private canInvite:Z

.field private final followers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field private loadFollowerProfileAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;

.field private final meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private final selectedFollowers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->TAG:Ljava/lang/String;

    .line 38
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$$Lambda$1;->lambdaFactory$()Ljava/util/Comparator;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->FOLLOWERS_COMPARATOR:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 78
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->followers:Ljava/util/List;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->selectedFollowers:Ljava/util/List;

    .line 81
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubInviteScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 82
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->onLoadFollowerProfileCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method private cancelActiveTasks()V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->loadFollowerProfileAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->loadFollowerProfileAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;->cancel()V

    .line 174
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->loadFollowerProfileAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;

    .line 176
    :cond_0
    return-void
.end method

.method static synthetic lambda$static$0(Lcom/microsoft/xbox/service/model/FollowersData;Lcom/microsoft/xbox/service/model/FollowersData;)I
    .locals 6
    .param p0, "lhs"    # Lcom/microsoft/xbox/service/model/FollowersData;
    .param p1, "rhs"    # Lcom/microsoft/xbox/service/model/FollowersData;

    .prologue
    const/4 v2, 0x1

    const/4 v3, -0x1

    .line 39
    if-ne p0, p1, :cond_1

    .line 40
    const/4 v2, 0x0

    .line 60
    :cond_0
    :goto_0
    return v2

    .line 41
    :cond_1
    if-eqz p0, :cond_0

    .line 43
    if-nez p1, :cond_2

    move v2, v3

    .line 44
    goto :goto_0

    .line 46
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/FollowersData;->getIsOnline()Z

    move-result v0

    .line 47
    .local v0, "lhsOnline":Z
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getIsOnline()Z

    move-result v1

    .line 49
    .local v1, "rhsOnline":Z
    if-ne v0, v1, :cond_4

    .line 50
    iget-boolean v4, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isFavorite:Z

    iget-boolean v5, p1, Lcom/microsoft/xbox/service/model/FollowersData;->isFavorite:Z

    if-ne v4, v5, :cond_3

    .line 51
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamertag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamertag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    goto :goto_0

    .line 52
    :cond_3
    iget-boolean v4, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isFavorite:Z

    if-eqz v4, :cond_0

    move v2, v3

    .line 53
    goto :goto_0

    .line 57
    :cond_4
    if-eqz v0, :cond_0

    move v2, v3

    .line 58
    goto :goto_0
.end method

.method private onClubSettingsLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V
    .locals 4
    .param p1, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .prologue
    const/4 v2, 0x0

    .line 220
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    .line 222
    .local v0, "settings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-virtual {v1, v3}, Lcom/google/common/collect/ImmutableList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 223
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->inviteOrAccept()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->canViewerAct()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->state()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    move-result-object v1

    sget-object v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;->Suspended:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    if-eq v1, v3, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->canInvite:Z

    .line 225
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->cancelActiveTasks()V

    .line 227
    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$1;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->loadFollowerProfileAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;

    .line 228
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->loadFollowerProfileAsyncTask:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$LoadFollowerProfileAsyncTask;->load(Z)V

    .line 233
    :goto_1
    return-void

    :cond_0
    move v1, v2

    .line 223
    goto :goto_0

    .line 231
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1
.end method

.method private declared-synchronized onLoadFollowerProfileCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 276
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 296
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->updateAdapter()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 297
    monitor-exit p0

    return-void

    .line 280
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->followers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 282
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->followers:Ljava/util/List;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getFollowingData()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 284
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->followers:Ljava/util/List;

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->FOLLOWERS_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->followers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 276
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 287
    :cond_1
    :try_start_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 292
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 276
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->goBack()V

    .line 86
    return-void
.end method

.method public declared-synchronized getFollowers()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->followers:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getRecipientGamertags()Ljava/util/List;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 125
    .local v1, "gamertags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->selectedFollowers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 126
    .local v0, "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamertag()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->replaceSpacesWithNonBreakSpaces(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 123
    .end local v0    # "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    .end local v1    # "gamertags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 129
    .restart local v1    # "gamertags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    monitor-exit p0

    return-object v1
.end method

.method public declared-synchronized getSelectedFollowerPositions()Ljava/util/List;
    .locals 5
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 140
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->selectedFollowers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 142
    .local v2, "positions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->selectedFollowers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 143
    .local v0, "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->followers:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 144
    .local v1, "position":I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    const/4 v3, 0x1

    :goto_1
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 145
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 140
    .end local v0    # "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    .end local v1    # "position":I
    .end local v2    # "positions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 144
    .restart local v0    # "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    .restart local v1    # "position":I
    .restart local v2    # "positions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    .line 148
    .end local v0    # "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    .end local v1    # "position":I
    :cond_1
    monitor-exit p0

    return-object v2
.end method

.method public getSendText()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->canInvite:Z

    if-eqz v0, :cond_0

    .line 154
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f0702ae

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 153
    :goto_0
    return-object v0

    .line 155
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f070a52

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTitleText()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->canInvite:Z

    if-eqz v0, :cond_0

    .line 161
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f070208

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 160
    :goto_0
    return-object v0

    .line 162
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f0702a5

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public isBusy()Z
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadOverride(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 190
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->cancelActiveTasks()V

    .line 191
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 192
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->Settings:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->loadAsync(ZLjava/util/List;)V

    .line 193
    return-void
.end method

.method protected onClubModelChanged(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 197
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 216
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->updateAdapter()V

    .line 217
    return-void

    .line 201
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 203
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 204
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->onClubSettingsLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    goto :goto_0

    .line 211
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 212
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Club model changed to invalid state."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 197
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onPartialRosterChangeSuccess(Ljava/util/List;Ljava/util/List;)V
    .locals 7
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 256
    .local p1, "successfulMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    .local p2, "failedMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 257
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 259
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->selectedFollowers:Ljava/util/List;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 261
    .local v2, "selectedFollowersCopy":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 262
    .local v0, "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    .line 263
    .local v1, "member":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    iget-object v5, v0, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->userId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 264
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->selectedFollowers:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 270
    .end local v0    # "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    .end local v1    # "member":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    :cond_2
    const v3, 0x7f070209

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->showError(I)V

    .line 271
    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 272
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->updateAdapter()V

    .line 273
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 180
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubInviteScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 181
    return-void
.end method

.method public onRosterChangeFailure(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 248
    .local p1, "failedMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    const v0, 0x7f070b6d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->showError(I)V

    .line 249
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->updateAdapter()V

    .line 250
    return-void
.end method

.method public onRosterChangeSuccess(Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 237
    .local p1, "successfulMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->canInvite:Z

    if-eqz v0, :cond_0

    .line 238
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const v1, 0x7f070213

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(I)V

    .line 243
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->goBack()V

    .line 244
    return-void

    .line 240
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const v1, 0x7f07026e

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(I)V

    goto :goto_0
.end method

.method public onSelectionChanged(IZ)V
    .locals 3
    .param p1, "position"    # I
    .param p2, "isSelected"    # Z

    .prologue
    .line 105
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->followers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    .line 106
    .local v1, "numFollowers":I
    if-ltz p1, :cond_1

    if-ge p1, v1, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 108
    if-ltz p1, :cond_0

    if-ge p1, v1, :cond_0

    .line 109
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->followers:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 111
    .local v0, "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    if-eqz p2, :cond_2

    .line 112
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->selectedFollowers:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    :goto_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->updateAdapter()V

    .line 119
    .end local v0    # "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    :cond_0
    return-void

    .line 106
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 114
    .restart local v0    # "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->selectedFollowers:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 167
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onStopOverride()V

    .line 168
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->cancelActiveTasks()V

    .line 169
    return-void
.end method

.method public declared-synchronized send()V
    .locals 4

    .prologue
    .line 89
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->selectedFollowers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 90
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 92
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->selectedFollowers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 94
    .local v1, "xuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->selectedFollowers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 95
    .local v0, "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    iget-object v3, v0, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 89
    .end local v0    # "follower":Lcom/microsoft/xbox/service/model/FollowersData;
    .end local v1    # "xuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 98
    .restart local v1    # "xuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v2, v1, p0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->sendInvites(Ljava/util/List;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 100
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->updateAdapter()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 102
    .end local v1    # "xuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    monitor-exit p0

    return-void
.end method
