.class public abstract Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
.super Landroid/widget/FrameLayout;
.source "ScreenLayout.java"


# static fields
.field private static badList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private allEventsEnabled:Z

.field private desiredDrawerLockState:I

.field private isActive:Z

.field private isEditable:Z

.field private isReady:Z

.field private isStarted:Z

.field protected isTombstoned:Z

.field private onLayoutChangedListener:Ljava/lang/Runnable;

.field private orientation:I

.field private screenPercent:I

.field private usesAccelerometer:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->badList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;-><init>(Landroid/content/Context;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    const/4 v0, 0x7

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;-><init>(Landroid/content/Context;I)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "orientation"    # I

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;-><init>(Landroid/content/Context;IZ)V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IZ)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "orientation"    # I
    .param p3, "usesAccelerometer"    # Z

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onLayoutChangedListener:Ljava/lang/Runnable;

    .line 44
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isEditable:Z

    .line 46
    const/16 v0, 0x64

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->screenPercent:I

    .line 47
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->desiredDrawerLockState:I

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->allEventsEnabled:Z

    .line 64
    invoke-virtual {p0, p2, p3}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->Initialize(IZ)V

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 77
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onLayoutChangedListener:Ljava/lang/Runnable;

    .line 44
    iput-boolean v4, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isEditable:Z

    .line 46
    const/16 v2, 0x64

    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->screenPercent:I

    .line 47
    iput v4, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->desiredDrawerLockState:I

    .line 48
    iput-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->allEventsEnabled:Z

    .line 78
    sget-object v2, Lcom/microsoft/xboxone/smartglass/R$styleable;->ScreenLayout:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 80
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 81
    invoke-static {}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->getScreenWidth()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 82
    .local v1, "screenPixelWidth":I
    int-to-float v2, v1

    invoke-static {}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->getScreenWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x42c80000    # 100.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->screenPercent:I

    .line 86
    .end local v1    # "screenPixelWidth":I
    :goto_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 88
    const/4 v2, 0x7

    invoke-virtual {p0, v2, v4}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->Initialize(IZ)V

    .line 89
    return-void

    .line 84
    :cond_0
    const/4 v2, -0x2

    invoke-virtual {v0, v4, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->screenPercent:I

    goto :goto_0
.end method

.method public static addViewThatCausesAndroidLeaks(Landroid/view/View;)V
    .locals 1
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 342
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->badList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 343
    return-void
.end method

.method private removeAllViewsAndWorkaroundAndroidLeaks()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 346
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    sget-object v4, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v1, v4, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 348
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->removeAllViews()V

    .line 350
    const-string v1, "ScreenLayout"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "removing %d problematic views"

    new-array v2, v2, [Ljava/lang/Object;

    sget-object v6, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->badList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v2, v3

    invoke-static {v4, v5, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->badList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 353
    .local v0, "v":Landroid/view/View;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->removeViewAndWorkaroundAndroidLeaks(Landroid/view/View;)V

    goto :goto_1

    .end local v0    # "v":Landroid/view/View;
    :cond_0
    move v1, v3

    .line 346
    goto :goto_0

    .line 356
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->badList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 357
    return-void
.end method

.method public static removeViewAndWorkaroundAndroidLeaks(Landroid/view/View;)V
    .locals 6
    .param p0, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 360
    if-eqz p0, :cond_1

    .line 361
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .line 362
    .local v2, "viewparent":Landroid/view/ViewParent;
    instance-of v3, v2, Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    move-object v0, v2

    .line 363
    check-cast v0, Landroid/view/ViewGroup;

    .line 364
    .local v0, "parent":Landroid/view/ViewGroup;
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 366
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-nez v3, :cond_2

    move v3, v4

    :goto_0
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 369
    .end local v0    # "parent":Landroid/view/ViewGroup;
    :cond_0
    instance-of v3, p0, Landroid/view/ViewGroup;

    if-eqz v3, :cond_1

    move-object v1, p0

    .line 370
    check-cast v1, Landroid/view/ViewGroup;

    .line 371
    .local v1, "view":Landroid/view/ViewGroup;
    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 372
    invoke-virtual {v1}, Landroid/view/ViewGroup;->destroyDrawingCache()V

    .line 374
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-nez v3, :cond_3

    :goto_1
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 377
    .end local v1    # "view":Landroid/view/ViewGroup;
    .end local v2    # "viewparent":Landroid/view/ViewParent;
    :cond_1
    return-void

    .restart local v0    # "parent":Landroid/view/ViewGroup;
    .restart local v2    # "viewparent":Landroid/view/ViewParent;
    :cond_2
    move v3, v5

    .line 366
    goto :goto_0

    .end local v0    # "parent":Landroid/view/ViewGroup;
    .restart local v1    # "view":Landroid/view/ViewGroup;
    :cond_3
    move v4, v5

    .line 374
    goto :goto_1
.end method


# virtual methods
.method protected Initialize(IZ)V
    .locals 1
    .param p1, "orientation"    # I
    .param p2, "usesAccelerometer"    # Z

    .prologue
    const/4 v0, 0x0

    .line 68
    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isReady:Z

    .line 69
    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isActive:Z

    .line 70
    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isStarted:Z

    .line 71
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->orientation:I

    .line 72
    iput-boolean p2, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->usesAccelerometer:Z

    .line 73
    return-void
.end method

.method public adjustBottomMargin(I)V
    .locals 0
    .param p1, "bottomMargin"    # I

    .prologue
    .line 316
    return-void
.end method

.method public abstract forceRefresh()V
.end method

.method public abstract forceUpdateViewImmediately()V
.end method

.method public getAnimateIn(Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    .locals 1
    .param p1, "goingBack"    # Z

    .prologue
    .line 243
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAnimateOut(Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    .locals 1
    .param p1, "goingBack"    # Z

    .prologue
    .line 239
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCanAutoLaunch()Z
    .locals 1

    .prologue
    .line 330
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isEditable:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getContent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 398
    const/4 v0, 0x0

    return-object v0
.end method

.method public getContentKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 403
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDesiredDrawerLockState()I
    .locals 1

    .prologue
    .line 393
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->desiredDrawerLockState:I

    return v0
.end method

.method public abstract getHeaderName()Ljava/lang/String;
.end method

.method public getIsActive()Z
    .locals 1

    .prologue
    .line 247
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isActive:Z

    return v0
.end method

.method public getIsEditable()Z
    .locals 1

    .prologue
    .line 325
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isEditable:Z

    return v0
.end method

.method public getIsReady()Z
    .locals 1

    .prologue
    .line 235
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isReady:Z

    return v0
.end method

.method public getIsStarted()Z
    .locals 1

    .prologue
    .line 251
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isStarted:Z

    return v0
.end method

.method public getIsTombstoned()Z
    .locals 1

    .prologue
    .line 231
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isTombstoned:Z

    return v0
.end method

.method public getLocalClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 200
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public getRelativeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 408
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRelativeIdKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 413
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRequestedOrientation()I
    .locals 2

    .prologue
    .line 212
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 214
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 215
    invoke-virtual {v0}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v1

    .line 217
    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getScreenPercent()I
    .locals 1

    .prologue
    .line 380
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->screenPercent:I

    return v0
.end method

.method public getShouldShowAppbar()Z
    .locals 1

    .prologue
    .line 334
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isEditable:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTrackPage()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getUseUTCTelemetry()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public isAllEventsEnabled()Z
    .locals 1

    .prologue
    .line 446
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->allEventsEnabled:Z

    return v0
.end method

.method public isAnimateOnPop()Z
    .locals 1

    .prologue
    .line 434
    const/4 v0, 0x1

    return v0
.end method

.method public isAnimateOnPush()Z
    .locals 1

    .prologue
    .line 430
    const/4 v0, 0x1

    return v0
.end method

.method public isKeepPreviousScreen()Z
    .locals 1

    .prologue
    .line 438
    const/4 v0, 0x0

    return v0
.end method

.method public leaveScreen(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "leaveHandler"    # Ljava/lang/Runnable;

    .prologue
    .line 426
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 427
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 103
    return-void
.end method

.method public abstract onAnimateInCompleted()V
.end method

.method public abstract onAnimateInStarted()V
.end method

.method public onApplicationPause()V
    .locals 0

    .prologue
    .line 158
    return-void
.end method

.method public onApplicationResume()V
    .locals 0

    .prologue
    .line 154
    return-void
.end method

.method public onBackButtonPressed()V
    .locals 0

    .prologue
    .line 222
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 303
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 99
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 307
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 181
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->removeAllViewsAndWorkaroundAndroidLeaks()V

    .line 182
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 295
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->allEventsEnabled:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onInterceptHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 280
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->allEventsEnabled:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 285
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->allEventsEnabled:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onLayout(ZIIII)V
    .locals 2
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 264
    const/4 v0, 0x0

    .line 265
    .local v0, "sample":Lcom/microsoft/xbox/toolkit/TimeSample;
    invoke-static {}, Lcom/microsoft/xle/test/interop/TestInterop;->getMonitorLPS()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 266
    invoke-static {}, Lcom/microsoft/xbox/toolkit/TimeTool;->getInstance()Lcom/microsoft/xbox/toolkit/TimeTool;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/TimeTool;->start()Lcom/microsoft/xbox/toolkit/TimeSample;

    move-result-object v0

    .line 268
    :cond_0
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 270
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onLayoutChangedListener:Ljava/lang/Runnable;

    if-eqz v1, :cond_1

    .line 271
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onLayoutChangedListener:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 273
    :cond_1
    invoke-static {}, Lcom/microsoft/xle/test/interop/TestInterop;->getMonitorLPS()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 274
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/TimeSample;->setFinished()V

    .line 276
    :cond_2
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isReady:Z

    .line 163
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->usesAccelerometer:Z

    if-eqz v0, :cond_0

    .line 164
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Accelerometer:Lcom/microsoft/xbox/toolkit/XLEAccelerometer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->onPause()V

    .line 168
    :cond_0
    const/4 v0, 0x4

    invoke-static {p0, v0}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    .line 169
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 191
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isTombstoned:Z

    .line 192
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onRehydrateOverride()V

    .line 193
    return-void
.end method

.method public abstract onRehydrateOverride()V
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 228
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 138
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isReady:Z

    .line 140
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->usesAccelerometer:Z

    if-eqz v0, :cond_0

    .line 141
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Accelerometer:Lcom/microsoft/xbox/toolkit/XLEAccelerometer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/XLEAccelerometer;->onResume()V

    .line 143
    :cond_0
    invoke-static {p0, v1}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    .line 144
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 225
    return-void
.end method

.method public onSetActive()V
    .locals 1

    .prologue
    .line 255
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isActive:Z

    .line 256
    return-void
.end method

.method public onSetInactive()V
    .locals 1

    .prologue
    .line 259
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isActive:Z

    .line 260
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getRequestedOrientation()I

    move-result v0

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->orientation:I

    if-eq v0, v1, :cond_0

    .line 131
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->orientation:I

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setRequestedOrientation(I)V

    .line 134
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isStarted:Z

    .line 135
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isStarted:Z

    .line 173
    return-void
.end method

.method public onTombstone()V
    .locals 2

    .prologue
    .line 185
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isTombstoned:Z

    .line 186
    const-string v0, "ScreenLayout"

    const-string v1, "onTombstone removing all views"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->removeAllViewsAndWorkaroundAndroidLeaks()V

    .line 188
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 290
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->allEventsEnabled:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public removeBottomMargin()V
    .locals 0

    .prologue
    .line 322
    return-void
.end method

.method public resetBottomMargin()V
    .locals 0

    .prologue
    .line 319
    return-void
.end method

.method public setAllEventsEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 442
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->allEventsEnabled:Z

    .line 443
    return-void
.end method

.method public setContentView(I)V
    .locals 3
    .param p1, "screenLayoutId"    # I

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 94
    .local v0, "vi":Landroid/view/LayoutInflater;
    const/4 v1, 0x1

    invoke-virtual {v0, p1, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 95
    return-void
.end method

.method protected setDesiredDrawerLockState(I)V
    .locals 0
    .param p1, "drawerEnabled"    # I

    .prologue
    .line 389
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->desiredDrawerLockState:I

    .line 390
    return-void
.end method

.method public abstract setHeaderName(Ljava/lang/String;)V
.end method

.method public setIsEditable(Z)V
    .locals 0
    .param p1, "isEditable"    # Z

    .prologue
    .line 338
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->isEditable:Z

    .line 339
    return-void
.end method

.method public abstract setIsPivotPane(Z)V
.end method

.method public abstract setIsSubPane(Z)V
.end method

.method public abstract setName(Ljava/lang/String;)V
.end method

.method public setOnLayoutChangedListener(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 299
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onLayoutChangedListener:Ljava/lang/Runnable;

    .line 300
    return-void
.end method

.method public setRequestedOrientation(I)V
    .locals 1
    .param p1, "requestedOrientation"    # I

    .prologue
    .line 204
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 206
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 207
    invoke-virtual {v0, p1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 209
    :cond_0
    return-void
.end method

.method public setScreenPercent(I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 384
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->screenPercent:I

    .line 385
    return-object p0
.end method

.method public setScreenState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 176
    return-void
.end method

.method public trackPageViewTelemetry()V
    .locals 0

    .prologue
    .line 127
    return-void
.end method

.method public xleFindViewId(I)Landroid/view/View;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 456
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
