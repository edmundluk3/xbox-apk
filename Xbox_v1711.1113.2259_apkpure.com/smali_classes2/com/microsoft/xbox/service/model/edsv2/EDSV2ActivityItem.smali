.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
.super Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
.source "EDSV2ActivityItem.java"


# static fields
.field private static final XBOX_MUSIC_ACTIVITY_TOKEN:Ljava/lang/String; = "music"

.field private static final XBOX_VIDEO_ACTIVITY_TOKEN:Ljava/lang/String; = "video"


# instance fields
.field public ParentalRatings:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;",
            ">;"
        }
    .end annotation
.end field

.field public RelatedMedia:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2RelatedMedia;",
            ">;"
        }
    .end annotation
.end field

.field private activityLaunchInfo:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;

.field private allowedTitleIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private displayPurchasePrice:Ljava/lang/String;

.field private icon2x1Height:I

.field private icon2x1Url:Ljava/lang/String;

.field private icon2x1Width:I

.field private iconUrl:Ljava/lang/String;

.field private isProviderSpecific:Z

.field private isPurchaseStatusVerified:Z

.field private parentIDs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private parentId:Ljava/lang/String;

.field private parentItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private priceString:Ljava/lang/String;

.field private providerPolicies:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;",
            ">;"
        }
    .end annotation
.end field

.field private providerString:Ljava/lang/String;

.field private purchaseStatus:I

.field private screenshots:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/net/URI;",
            ">;"
        }
    .end annotation
.end field

.field private slideshows:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;",
            ">;"
        }
    .end annotation
.end field

.field private splashImageUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 54
    return-void
.end method


# virtual methods
.method public addAllowedTitleId(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 269
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->allowedTitleIds:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 270
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->allowedTitleIds:Ljava/util/ArrayList;

    .line 273
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->allowedTitleIds:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 274
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->allowedTitleIds:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 276
    :cond_1
    return-void
.end method

.method public getActivityLaunchInfo()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->activityLaunchInfo:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;

    return-object v0
.end method

.method public getAllowedTitleIds()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 261
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->allowedTitleIds:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getDisplayPurchasePrice()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->displayPurchasePrice:Ljava/lang/String;

    return-object v0
.end method

.method public getIcon2x1Height()I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->icon2x1Height:I

    return v0
.end method

.method public getIcon2x1Url()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->icon2x1Url:Ljava/lang/String;

    return-object v0
.end method

.method public getIcon2x1Width()I
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->icon2x1Width:I

    return v0
.end method

.method public getIconUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->iconUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getIsHelp()Z
    .locals 1

    .prologue
    .line 300
    const/4 v0, 0x0

    return v0
.end method

.method public getIsProviderSpecific()Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->isProviderSpecific:Z

    return v0
.end method

.method public getIsPurchaseStatusVerified()Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->isPurchaseStatusVerified:Z

    return v0
.end method

.method public getParentIDs()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218
    const/4 v1, 0x0

    .line 219
    .local v1, "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->parentIDs:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->RelatedMedia:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 220
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 221
    .restart local v1    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->RelatedMedia:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RelatedMedia;

    .line 222
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2RelatedMedia;
    const-string v3, "Parent"

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RelatedMedia;->RelationType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 223
    iget-object v3, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2RelatedMedia;->ID:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 228
    .end local v0    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2RelatedMedia;
    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 229
    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->parentIDs:Ljava/util/ArrayList;

    .line 232
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->parentIDs:Ljava/util/ArrayList;

    return-object v2
.end method

.method public getParentId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->parentId:Ljava/lang/String;

    return-object v0
.end method

.method public getParentItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->parentItems:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getParentalRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->ParentalRatings:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Util;->getLocalizedParentalRating(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParentalRatings()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;",
            ">;"
        }
    .end annotation

    .prologue
    .line 153
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->ParentalRatings:Ljava/util/ArrayList;

    return-object v0
.end method

.method public bridge synthetic getParentalRatings()Ljava/util/List;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getParentalRatings()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getPriceString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->priceString:Ljava/lang/String;

    return-object v0
.end method

.method public getProviderPolicies()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->providerPolicies:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getProviderString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->providerString:Ljava/lang/String;

    return-object v0
.end method

.method public getPurchaseStatus()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->purchaseStatus:I

    return v0
.end method

.method public getRatingDescriptors()Ljava/util/List;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->ParentalRatings:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->ParentalRatings:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;->RatingDescriptors:Ljava/util/List;

    .line 162
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSlideShow()Ljava/util/ArrayList;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;",
            ">;"
        }
    .end annotation

    .prologue
    .line 281
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->slideshows:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->Images:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 282
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->Images:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->slideshows:Ljava/util/ArrayList;

    .line 283
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->Images:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;

    .line 285
    .local v0, "image":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;->getPurpose()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ImageGalleryTablet"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 286
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->slideshows:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 291
    .end local v0    # "image":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->slideshows:Ljava/util/ArrayList;

    return-object v1
.end method

.method public bridge synthetic getSlideShow()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getSlideShow()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getSplashImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->splashImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public isDefaultForAtLeastOneProvider()Z
    .locals 3

    .prologue
    .line 185
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->providerPolicies:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->providerPolicies:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 186
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->providerPolicies:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;

    .line 187
    .local v0, "policy":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;->getIsDefault()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 188
    const/4 v1, 0x1

    .line 192
    .end local v0    # "policy":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isDefaultForProvider(J)Z
    .locals 5
    .param p1, "providerTitleId"    # J

    .prologue
    .line 196
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->providerPolicies:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->providerPolicies:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 197
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->providerPolicies:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;

    .line 198
    .local v0, "policy":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;->getTitleId()J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;->getIsDefault()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 199
    const/4 v1, 0x1

    .line 203
    .end local v0    # "policy":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isGoldRequired()Z
    .locals 2

    .prologue
    .line 237
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getPurchaseStatus()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 238
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getPurchaseStatus()I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    .line 239
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getPurchaseStatus()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 237
    :goto_0
    return v0

    .line 239
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNativeCompanion()Z
    .locals 2

    .prologue
    .line 176
    const-string v0, "DNativeApp"

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->MediaItemType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isPurchaseRequired()Z
    .locals 2

    .prologue
    .line 244
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getPurchaseStatus()I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    .line 245
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getPurchaseStatus()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 244
    :goto_0
    return v0

    .line 245
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValidActivity()Z
    .locals 1

    .prologue
    .line 181
    const/4 v0, 0x1

    return v0
.end method

.method public setActivityLaunchInfo(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;)V
    .locals 0
    .param p1, "launchInfo"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->activityLaunchInfo:Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;

    .line 63
    return-void
.end method

.method public setAllowedTitleIds(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 265
    .local p1, "titleIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->allowedTitleIds:Ljava/util/ArrayList;

    .line 266
    return-void
.end method

.method public setDisplayPurchasePrice(Ljava/lang/String;)V
    .locals 0
    .param p1, "price"    # Ljava/lang/String;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->displayPurchasePrice:Ljava/lang/String;

    .line 79
    return-void
.end method

.method public setIcon2x1Url(Ljava/lang/String;II)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 114
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->icon2x1Url:Ljava/lang/String;

    .line 115
    iput p2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->icon2x1Width:I

    .line 116
    iput p3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->icon2x1Height:I

    .line 117
    return-void
.end method

.method public setIsProviderSpecific(Z)V
    .locals 0
    .param p1, "isProviderSpecific"    # Z

    .prologue
    .line 102
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->isProviderSpecific:Z

    .line 103
    return-void
.end method

.method public setIsPurchaseStatusVerified(Z)V
    .locals 0
    .param p1, "verified"    # Z

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->isPurchaseStatusVerified:Z

    .line 95
    return-void
.end method

.method public setParentId(Ljava/lang/String;)V
    .locals 0
    .param p1, "parentId"    # Ljava/lang/String;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->parentId:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public setParentItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 213
    .local p1, "parents":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->parentItems:Ljava/util/List;

    .line 214
    return-void
.end method

.method public setProviderPolicies(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 137
    .local p1, "policies":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->providerPolicies:Ljava/util/ArrayList;

    .line 139
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->allowedTitleIds:Ljava/util/ArrayList;

    .line 140
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->providerPolicies:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 141
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->providerPolicies:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;

    .line 142
    .local v0, "policy":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->allowedTitleIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;->getTitleId()J

    move-result-wide v4

    long-to-int v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 145
    .end local v0    # "policy":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityProviderPolicy;
    :cond_0
    return-void
.end method

.method public setPurchaseStatus(I)V
    .locals 0
    .param p1, "status"    # I

    .prologue
    .line 86
    iput p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->purchaseStatus:I

    .line 87
    return-void
.end method

.method public supportsProvider(J)Z
    .locals 3
    .param p1, "providerTitleId"    # J

    .prologue
    .line 254
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->providerPolicies:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->allowedTitleIds:Ljava/util/ArrayList;

    long-to-int v1, p1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 255
    const/4 v0, 0x1

    .line 257
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
