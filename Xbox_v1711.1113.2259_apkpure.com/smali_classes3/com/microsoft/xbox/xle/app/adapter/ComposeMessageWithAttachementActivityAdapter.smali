.class public Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "ComposeMessageWithAttachementActivityAdapter.java"


# static fields
.field private static final MAX_THUMBS_PHONE:I = 0x5

.field private static final STATE_NO_RECIPIENTS:I


# instance fields
.field private cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

.field private itemContainer:Landroid/view/ViewGroup;

.field private messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

.field private recipientCountDrawable:Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;

.field private recipientHolderStringView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private recipientsHolderView:Landroid/view/View;

.field private sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private sendButtonView:Landroid/view/View;

.field private separator:Landroid/view/View;

.field private shareButtonView:Landroid/widget/FrameLayout;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;)V
    .locals 4
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    .prologue
    const/16 v3, 0x8

    .line 49
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>()V

    .line 50
    const v1, 0x7f0e041c

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->screenBody:Landroid/view/View;

    .line 52
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    .line 54
    const v1, 0x7f0e0421

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->itemContainer:Landroid/view/ViewGroup;

    .line 55
    const v1, 0x7f0e0420

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    .line 58
    const v1, 0x7f0e041d

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;

    .line 59
    .local v0, "parent":Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->setContainer(Lcom/microsoft/xbox/toolkit/ui/EditTextContainer;)V

    .line 60
    const v1, 0x7f0e041a

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->recipientHolderStringView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 61
    const v1, 0x7f0e0417

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->recipientsHolderView:Landroid/view/View;

    .line 63
    const v1, 0x7f0e040c

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->shareButtonView:Landroid/widget/FrameLayout;

    .line 64
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->shareButtonView:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 65
    const v1, 0x7f0e0130

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->separator:Landroid/view/View;

    .line 67
    new-instance v1, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;

    invoke-direct {v1}, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->recipientCountDrawable:Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;

    .line 68
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->recipientCountDrawable:Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->setMaxRecipientsDisplayedOnScreenCount(I)V

    .line 70
    const v1, 0x7f0e041e

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 71
    const v1, 0x7f0e0413

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 72
    const v1, 0x7f0e040f

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->sendButtonView:Landroid/view/View;

    .line 74
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v1, :cond_0

    .line 75
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter$1;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;->getInstance()Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 85
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter$2;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 113
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->recipientsHolderView:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 114
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->recipientsHolderView:Landroid/view/View;

    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter$3;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter$3;-><init>(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->recipientsHolderView:Landroid/view/View;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;->getInstance()Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 124
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->isFromConversationDetailScreen()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 125
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->recipientsHolderView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 126
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->separator:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 127
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 130
    :cond_2
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;)Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;)Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->setSendButtonEnabled(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->dismissKeyboard()V

    return-void
.end method

.method static synthetic lambda$onStart$0(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->dismissKeyboard()V

    .line 161
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->sendSkypeMessageWithAttachment()V

    .line 162
    return-void
.end method

.method private setSendButtonEnabled(Z)V
    .locals 1
    .param p1, "enableSendButton"    # Z

    .prologue
    .line 238
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 240
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->sendButtonView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 243
    :cond_0
    return-void
.end method


# virtual methods
.method public onAnimateInCompleted()V
    .locals 2

    .prologue
    .line 177
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onAnimateInCompleted()V

    .line 178
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->getShouldAutoShowKeyboard()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->getEditTextView()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 181
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->getEditTextView()Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->showKeyboard(Landroid/view/View;I)V

    .line 183
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 134
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onPause()V

    .line 135
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->dismissKeyboard()V

    .line 137
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 151
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStart()V

    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->getMessageBody()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->getMessageBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->setText(Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->getMessageBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->setSelection(I)V

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->sendButtonView:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->sendButtonView:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->setSendButtonEnabled(Z)V

    .line 165
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->sendButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;->getInstance()Lcom/microsoft/xbox/xle/app/HideKeyboardAccessibilityDelegate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 168
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->getText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 169
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->setSendButtonEnabled(Z)V

    .line 173
    :goto_0
    return-void

    .line 171
    :cond_2
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->setSendButtonEnabled(Z)V

    goto :goto_0
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 142
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;->onStop()V

    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->sendButtonView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->sendButtonView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    :cond_0
    return-void
.end method

.method public updateMessageBody()V
    .locals 2

    .prologue
    .line 234
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->setMessageBody(Ljava/lang/String;)V

    .line 235
    return-void
.end method

.method public updateViewOverride()V
    .locals 10

    .prologue
    const/4 v7, 0x1

    .line 194
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f070668

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 195
    .local v4, "recipientsHolderString":Ljava/lang/String;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->getRecipients()Ljava/util/ArrayList;

    move-result-object v3

    .line 196
    .local v3, "recipients":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;>;"
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 197
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 198
    .local v2, "recipientCount":I
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->recipientCountDrawable:Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;

    invoke-virtual {v6, v2}, Lcom/microsoft/xbox/xle/ui/RecipientCountDrawable;->setRecipientCount(I)V

    .line 200
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 201
    add-int/lit8 v6, v2, -0x1

    if-ne v0, v6, :cond_0

    .line 203
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getGamerName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 200
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 205
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getGamerName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 209
    :cond_1
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->recipientHolderStringView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v6, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    .end local v0    # "i":I
    .end local v2    # "recipientCount":I
    :cond_2
    :goto_2
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->getItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v1

    .line 218
    .local v1, "newItem":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    if-eqz v1, :cond_3

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-eq v1, v6, :cond_3

    .line 219
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    .line 221
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->itemContainer:Landroid/view/ViewGroup;

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->item:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    invoke-static {v6, v8, v7}, Lcom/microsoft/xbox/xle/app/adapter/ActivityFeedActionsScreenAdapterUtil;->createAndBindFeedItemViewForSharing(Landroid/view/ViewGroup;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Z)V

    .line 227
    :cond_3
    const v8, 0x7f0e0413

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->getText()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->messageEditView:Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/ui/EditViewFixedLength;->getText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_5

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->getIsRecipientNonEmpty()Z

    move-result v6

    if-eqz v6, :cond_5

    move v6, v7

    :goto_3
    invoke-virtual {p0, v8, v6}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->setAppBarButtonEnabled(IZ)V

    .line 229
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->isBlockingBusy()Z

    move-result v6

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->getBlockingStatusText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v6, v7}, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->setBlocking(ZLjava/lang/String;)V

    .line 231
    return-void

    .line 211
    .end local v1    # "newItem":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :cond_4
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/ComposeMessageWithAttachmentViewModel;->getGroupTopic()Ljava/lang/String;

    move-result-object v5

    .line 212
    .local v5, "topic":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 213
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/ComposeMessageWithAttachementActivityAdapter;->recipientHolderStringView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 227
    .end local v5    # "topic":Ljava/lang/String;
    .restart local v1    # "newItem":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    :cond_5
    const/4 v6, 0x0

    goto :goto_3
.end method
