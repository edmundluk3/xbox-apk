.class public Lcom/microsoft/xbox/toolkit/XBLSharedUtil;
.super Ljava/lang/Object;
.source "XBLSharedUtil.java"


# static fields
.field private static SECONDS_PER_HOUR:I

.field private static SECONDS_PER_MINUTE:I

.field private static final SECOND_TO_HUNDRED_NANOSECOND:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 12
    const/16 v0, 0x3c

    sput v0, Lcom/microsoft/xbox/toolkit/XBLSharedUtil;->SECONDS_PER_MINUTE:I

    .line 13
    sget v0, Lcom/microsoft/xbox/toolkit/XBLSharedUtil;->SECONDS_PER_MINUTE:I

    mul-int/lit8 v0, v0, 0x3c

    sput v0, Lcom/microsoft/xbox/toolkit/XBLSharedUtil;->SECONDS_PER_HOUR:I

    .line 14
    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    const-wide/high16 v2, 0x401c000000000000L    # 7.0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-long v0, v0

    sput-wide v0, Lcom/microsoft/xbox/toolkit/XBLSharedUtil;->SECOND_TO_HUNDRED_NANOSECOND:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static HHMMSSStringToMinutes(Ljava/lang/String;)I
    .locals 2
    .param p0, "HHMMSSString"    # Ljava/lang/String;

    .prologue
    .line 61
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/XBLSharedUtil;->HHMMSSStringToSeconds(Ljava/lang/String;)I

    move-result v0

    sget v1, Lcom/microsoft/xbox/toolkit/XBLSharedUtil;->SECONDS_PER_MINUTE:I

    div-int/2addr v0, v1

    goto :goto_0
.end method

.method public static HHMMSSStringToSeconds(Ljava/lang/String;)I
    .locals 6
    .param p0, "HHMMSSString"    # Ljava/lang/String;

    .prologue
    .line 66
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "HH:mm:ss"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 67
    .local v2, "sdf":Ljava/text/SimpleDateFormat;
    const-string v3, "UTC"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 69
    :try_start_0
    invoke-virtual {v2, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 70
    .local v0, "date":Ljava/util/Date;
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    long-to-int v3, v4

    div-int/lit16 v3, v3, 0x3e8
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    .end local v0    # "date":Ljava/util/Date;
    :goto_0
    return v3

    .line 71
    :catch_0
    move-exception v1

    .line 72
    .local v1, "e":Ljava/text/ParseException;
    const-string v3, "XBLSharedUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cant\' parse "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static durationStringToMinutes(Ljava/lang/String;)I
    .locals 2
    .param p0, "durationString"    # Ljava/lang/String;

    .prologue
    .line 80
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/XBLSharedUtil;->durationStringToSeconds(Ljava/lang/String;)I

    move-result v0

    sget v1, Lcom/microsoft/xbox/toolkit/XBLSharedUtil;->SECONDS_PER_MINUTE:I

    div-int/2addr v0, v1

    return v0
.end method

.method public static durationStringToSeconds(Ljava/lang/String;)I
    .locals 11
    .param p0, "durationString"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 17
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_1

    .line 57
    :cond_0
    :goto_0
    return v8

    .line 22
    :cond_1
    const/4 v2, 0x0

    .line 23
    .local v2, "hours":I
    const/4 v4, 0x0

    .line 24
    .local v4, "minutes":I
    const/4 v7, 0x0

    .line 27
    .local v7, "seconds":I
    :try_start_0
    new-instance v6, Ljava/util/Scanner;

    invoke-direct {v6, p0}, Ljava/util/Scanner;-><init>(Ljava/lang/String;)V

    .line 32
    .local v6, "scanner":Ljava/util/Scanner;
    const-string v8, "PT(\\d+H)?(\\d+M)?(\\d+S)?"

    invoke-virtual {v6, v8}, Ljava/util/Scanner;->findInLine(Ljava/lang/String;)Ljava/lang/String;

    .line 34
    invoke-virtual {v6}, Ljava/util/Scanner;->match()Ljava/util/regex/MatchResult;

    move-result-object v5

    .line 35
    .local v5, "result":Ljava/util/regex/MatchResult;
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_1
    invoke-interface {v5}, Ljava/util/regex/MatchResult;->groupCount()I

    move-result v8

    if-gt v3, v8, :cond_4

    .line 36
    invoke-interface {v5, v3}, Ljava/util/regex/MatchResult;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 37
    .local v1, "group":Ljava/lang/String;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_3

    .line 35
    :cond_2
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 41
    :cond_3
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v1, v8}, Ljava/lang/String;->charAt(I)C

    move-result v8

    sparse-switch v8, :sswitch_data_0

    goto :goto_2

    .line 43
    :sswitch_0
    const/4 v8, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseInteger(Ljava/lang/String;I)I

    move-result v2

    .line 44
    goto :goto_2

    .line 46
    :sswitch_1
    const/4 v8, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseInteger(Ljava/lang/String;I)I

    move-result v4

    .line 47
    goto :goto_2

    .line 49
    :sswitch_2
    const/4 v8, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseInteger(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    goto :goto_2

    .line 53
    .end local v1    # "group":Ljava/lang/String;
    .end local v3    # "i":I
    .end local v5    # "result":Ljava/util/regex/MatchResult;
    .end local v6    # "scanner":Ljava/util/Scanner;
    :catch_0
    move-exception v0

    .line 54
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v8, "XBLSharedUtil"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Couldn\'t parse duration string: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :cond_4
    sget v8, Lcom/microsoft/xbox/toolkit/XBLSharedUtil;->SECONDS_PER_HOUR:I

    mul-int/2addr v8, v2

    sget v9, Lcom/microsoft/xbox/toolkit/XBLSharedUtil;->SECONDS_PER_MINUTE:I

    mul-int/2addr v9, v4

    add-int/2addr v8, v9

    add-int/2addr v8, v7

    goto/16 :goto_0

    .line 41
    nop

    :sswitch_data_0
    .sparse-switch
        0x48 -> :sswitch_0
        0x4d -> :sswitch_1
        0x53 -> :sswitch_2
    .end sparse-switch
.end method

.method public static hundredNanosecondsToSeconds(J)J
    .locals 2
    .param p0, "hundredNanoSeconds"    # J

    .prologue
    .line 88
    sget-wide v0, Lcom/microsoft/xbox/toolkit/XBLSharedUtil;->SECOND_TO_HUNDRED_NANOSECOND:J

    div-long v0, p0, v0

    return-wide v0
.end method

.method public static secondsToHundredNanoseconds(F)J
    .locals 2
    .param p0, "seconds"    # F

    .prologue
    .line 84
    sget-wide v0, Lcom/microsoft/xbox/toolkit/XBLSharedUtil;->SECOND_TO_HUNDRED_NANOSECOND:J

    long-to-float v0, v0

    mul-float/2addr v0, p0

    float-to-long v0, v0

    return-wide v0
.end method
