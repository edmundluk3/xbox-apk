.class public Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;
.super Lcom/microsoft/xbox/service/model/ModelBase;
.source "GameClipModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$GameClipRunnable;,
        Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/ModelBase",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;",
        ">;"
    }
.end annotation


# static fields
.field public static final CACHE_SIZE:I = 0x80

.field private static final map:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;",
            "Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private gameClip:Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;

.field private final key:Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->map:Landroid/util/LruCache;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;)V
    .locals 2
    .param p1, "key"    # Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/ModelBase;-><init>()V

    .line 40
    new-instance v0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$GameClipRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$GameClipRunnable;-><init>(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->loaderRunnable:Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;

    .line 41
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->key:Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;

    .line 42
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;)Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->key:Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;

    return-object v0
.end method

.method public static getInstance(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;)Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;
    .locals 3
    .param p0, "key"    # Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;

    .prologue
    .line 22
    sget-object v2, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->map:Landroid/util/LruCache;

    monitor-enter v2

    .line 23
    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->map:Landroid/util/LruCache;

    invoke-virtual {v1, p0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;

    .line 24
    .local v0, "model":Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;
    if-nez v0, :cond_0

    .line 25
    new-instance v0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;

    .end local v0    # "model":Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;
    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;-><init>(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;)V

    .line 26
    .restart local v0    # "model":Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;
    sget-object v1, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->map:Landroid/util/LruCache;

    invoke-virtual {v1, p0, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    :cond_0
    monitor-exit v2

    return-object v0

    .line 29
    .end local v0    # "model":Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static reset()V
    .locals 2

    .prologue
    .line 33
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 34
    sget-object v1, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->map:Landroid/util/LruCache;

    monitor-enter v1

    .line 35
    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->map:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 36
    monitor-exit v1

    .line 37
    return-void

    .line 36
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public getGameClip()Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->gameClip:Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;

    return-object v0
.end method

.method public getKey()Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->key:Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;

    return-object v0
.end method

.method public loadSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 1
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->loaderRunnable:Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 58
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;>;"
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/ModelBase;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 59
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_0

    .line 60
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->gameClip:Lcom/microsoft/xbox/service/network/managers/CaptureContainer$GameClipResult;

    .line 62
    :cond_0
    return-void
.end method
