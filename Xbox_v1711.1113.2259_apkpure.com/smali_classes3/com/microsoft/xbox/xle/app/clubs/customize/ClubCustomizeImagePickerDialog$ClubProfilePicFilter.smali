.class final enum Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;
.super Ljava/lang/Enum;
.source "ClubCustomizeImagePickerDialog.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ClubProfilePicFilter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;",
        ">;",
        "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

.field public static final enum ClubPics:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

.field public static final enum Gamerpics:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;


# instance fields
.field private final resId:I

.field private final telemetryName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 279
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

    const-string v1, "ClubPics"

    const v2, 0x7f0701ac

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;->ClubPics:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

    .line 280
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

    const-string v1, "Gamerpics"

    const v2, 0x7f0701ad

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;->Gamerpics:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

    .line 278
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;->ClubPics:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;->Gamerpics:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;->$VALUES:[Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "resId"    # I
    .param p4, "telemetryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 285
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 286
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 288
    iput p3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;->resId:I

    .line 289
    iput-object p4, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;->telemetryName:Ljava/lang/String;

    .line 290
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 278
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;
    .locals 1

    .prologue
    .line 278
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;->$VALUES:[Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;

    return-object v0
.end method


# virtual methods
.method public getDisplayName()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 295
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    iget v1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;->resId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTelemetryName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 301
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubCustomizeImagePickerDialog$ClubProfilePicFilter;->telemetryName:Ljava/lang/String;

    return-object v0
.end method
