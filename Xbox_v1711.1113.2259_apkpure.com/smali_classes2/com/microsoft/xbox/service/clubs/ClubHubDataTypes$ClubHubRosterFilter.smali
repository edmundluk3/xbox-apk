.class public final enum Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;
.super Ljava/lang/Enum;
.source "ClubHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ClubHubRosterFilter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

.field public static final enum Banned:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

.field public static final enum Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

.field public static final enum Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

.field public static final enum Recommended:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

.field public static final enum RequestedToJoin:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 48
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

    const-string v1, "Banned"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;->Banned:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

    .line 49
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

    const-string v1, "Member"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

    .line 50
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

    const-string v1, "Moderator"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

    .line 51
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

    const-string v1, "Recommended"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;->Recommended:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

    .line 52
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

    const-string v1, "RequestedToJoin"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;->RequestedToJoin:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

    .line 47
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;->Banned:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;->Recommended:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;->RequestedToJoin:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

    aput-object v1, v0, v6

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 47
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;

    return-object v0
.end method
