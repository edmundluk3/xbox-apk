.class final Lcom/microsoft/xbox/toolkit/AutoValue_ActivityResult;
.super Lcom/microsoft/xbox/toolkit/ActivityResult;
.source "AutoValue_ActivityResult.java"


# instance fields
.field private final data:Landroid/content/Intent;

.field private final requestCode:I

.field private final resultCode:I


# direct methods
.method constructor <init>(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ActivityResult;-><init>()V

    .line 19
    iput p1, p0, Lcom/microsoft/xbox/toolkit/AutoValue_ActivityResult;->requestCode:I

    .line 20
    iput p2, p0, Lcom/microsoft/xbox/toolkit/AutoValue_ActivityResult;->resultCode:I

    .line 21
    iput-object p3, p0, Lcom/microsoft/xbox/toolkit/AutoValue_ActivityResult;->data:Landroid/content/Intent;

    .line 22
    return-void
.end method


# virtual methods
.method public data()Landroid/content/Intent;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/AutoValue_ActivityResult;->data:Landroid/content/Intent;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 51
    if-ne p1, p0, :cond_1

    .line 60
    :cond_0
    :goto_0
    return v1

    .line 54
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/toolkit/ActivityResult;

    if-eqz v3, :cond_4

    move-object v0, p1

    .line 55
    check-cast v0, Lcom/microsoft/xbox/toolkit/ActivityResult;

    .line 56
    .local v0, "that":Lcom/microsoft/xbox/toolkit/ActivityResult;
    iget v3, p0, Lcom/microsoft/xbox/toolkit/AutoValue_ActivityResult;->requestCode:I

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ActivityResult;->requestCode()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget v3, p0, Lcom/microsoft/xbox/toolkit/AutoValue_ActivityResult;->resultCode:I

    .line 57
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ActivityResult;->resultCode()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/AutoValue_ActivityResult;->data:Landroid/content/Intent;

    if-nez v3, :cond_3

    .line 58
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ActivityResult;->data()Landroid/content/Intent;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/AutoValue_ActivityResult;->data:Landroid/content/Intent;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ActivityResult;->data()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/toolkit/ActivityResult;
    :cond_4
    move v1, v2

    .line 60
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 65
    const/4 v0, 0x1

    .line 66
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 67
    iget v1, p0, Lcom/microsoft/xbox/toolkit/AutoValue_ActivityResult;->requestCode:I

    xor-int/2addr v0, v1

    .line 68
    mul-int/2addr v0, v2

    .line 69
    iget v1, p0, Lcom/microsoft/xbox/toolkit/AutoValue_ActivityResult;->resultCode:I

    xor-int/2addr v0, v1

    .line 70
    mul-int/2addr v0, v2

    .line 71
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/AutoValue_ActivityResult;->data:Landroid/content/Intent;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    xor-int/2addr v0, v1

    .line 72
    return v0

    .line 71
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/AutoValue_ActivityResult;->data:Landroid/content/Intent;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public requestCode()I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/microsoft/xbox/toolkit/AutoValue_ActivityResult;->requestCode:I

    return v0
.end method

.method public resultCode()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/microsoft/xbox/toolkit/AutoValue_ActivityResult;->resultCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ActivityResult{requestCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/toolkit/AutoValue_ActivityResult;->requestCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", resultCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/toolkit/AutoValue_ActivityResult;->resultCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/AutoValue_ActivityResult;->data:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
