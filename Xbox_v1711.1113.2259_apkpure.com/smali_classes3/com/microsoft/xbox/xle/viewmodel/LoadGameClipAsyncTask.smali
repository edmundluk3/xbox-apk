.class Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "LoadGameClipAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask$CompletionHandler;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final completionHandler:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask$CompletionHandler;

.field private final model:Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask$CompletionHandler;)V
    .locals 1
    .param p1, "key"    # Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;
    .param p2, "completionHandler"    # Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask$CompletionHandler;

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 14
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;->completionHandler:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask$CompletionHandler;

    .line 15
    invoke-static {p1}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->getInstance(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel$Key;)Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;->model:Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;

    .line 16
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 20
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;->model:Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->shouldRefresh()Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;->model:Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;->loadSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 3

    .prologue
    .line 26
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;->completionHandler:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask$CompletionHandler;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;->model:Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-interface {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask$CompletionHandler;->onComplete(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 28
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;->completionHandler:Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask$CompletionHandler;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;->model:Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;

    invoke-interface {v0, v1, p1}, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask$CompletionHandler;->onComplete(Lcom/microsoft/xbox/service/model/gameclip/GameClipModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 48
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 9
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/LoadGameClipAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 42
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 43
    return-void
.end method
