.class Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;
.super Landroid/os/AsyncTask;
.source "GcmModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/gcm/GcmModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UnregisterSkypeTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/microsoft/xbox/toolkit/AsyncResult",
        "<",
        "Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;",
        ">;>;"
    }
.end annotation


# instance fields
.field private curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)V
    .locals 0

    .prologue
    .line 1311
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel;Lcom/microsoft/xbox/service/model/gcm/GcmModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/gcm/GcmModel$1;

    .prologue
    .line 1311
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;-><init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 6
    .param p1, "params"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1324
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Un-registering from EDF"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1327
    :try_start_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->isEDFRegistered()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1328
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$1400()Lcom/microsoft/xbox/service/network/managers/IESServiceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->edfRegId:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/service/network/managers/IESServiceManager;->deleteEDFRegistrationForSkypeNotifications(Ljava/lang/String;)Z

    .line 1331
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$1400()Lcom/microsoft/xbox/service/network/managers/IESServiceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->edfRegId:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/service/network/managers/IESServiceManager;->deleteWithSkypeChatServiceForNotifications(Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1334
    :try_start_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->registrationId:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->cleanupUnusedSkypeEndpointsForPushNotification(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1341
    :cond_0
    :goto_0
    :try_start_2
    new-instance v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->registrationId:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget v4, v4, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->flags:I

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;
    :try_end_2
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1345
    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;->STATE_CHANGED:Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-direct {v1, v2, v3, v5}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    :goto_1
    return-object v1

    .line 1335
    :catch_0
    move-exception v0

    .line 1336
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :try_start_3
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error in getting edf endpoints"

    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 1342
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :catch_1
    move-exception v0

    .line 1343
    .restart local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;->STATE_CHANGED:Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-direct {v1, v2, v3, v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1311
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;->doInBackground([Ljava/lang/Void;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;>;"
    const/4 v4, 0x0

    .line 1350
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1351
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1352
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$900(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1353
    .local v0, "edt":Landroid/content/SharedPreferences$Editor;
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->isEDFRegistered()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1354
    const-string v1, "edf.node.id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1355
    const-string v1, "last.edf.registration.time.03"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1356
    const-string v1, "skype.reg.token"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1357
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getInstance()Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->setPushNotificationRegToken(Ljava/lang/String;)V

    .line 1358
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Successfully unregistered from Skype"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1364
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1366
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v1, v4}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$2302(Lcom/microsoft/xbox/service/model/gcm/GcmModel;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;

    .line 1367
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 1369
    .end local v0    # "edt":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void

    .line 1361
    .restart local v0    # "edt":Landroid/content/SharedPreferences$Editor;
    :cond_1
    const-string v1, "user.flags"

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget v2, v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->flags:I

    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->MESSAGE:Lcom/microsoft/xbox/service/model/gcm/NotificationType;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/gcm/NotificationType;->getFlag()I

    move-result v3

    or-int/2addr v2, v3

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1362
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to unregister from Skype"

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1311
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 5

    .prologue
    .line 1316
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 1317
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$1200(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    .line 1318
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->skypeRegToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$1602(Lcom/microsoft/xbox/service/model/gcm/GcmModel;Ljava/lang/String;)Ljava/lang/String;

    .line 1319
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;->STATE_CHANGED:Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$UnregisterSkypeTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 1320
    return-void
.end method
