.class Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$3;
.super Ljava/lang/Object;
.source "EPGScrollAccelerator.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->onHandlePost_tweenOut()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    .prologue
    .line 267
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$3;->this$0:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 282
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$3;->this$0:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;->NOT_INITIATED:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->access$302(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;)Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    .line 283
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$3;->this$0:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->access$400(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->removeListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 284
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$3;->this$0:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->access$402(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;

    .line 285
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 275
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$3;->this$0:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    sget-object v1, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;->NOT_INITIATED:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->access$302(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;)Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$TweenInitiator;

    .line 276
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$3;->this$0:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->access$400(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->removeListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 277
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator$3;->this$0:Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;->access$402(Lcom/microsoft/xbox/xle/ui/EPGScrollAccelerator;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;

    .line 278
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 289
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 271
    return-void
.end method
