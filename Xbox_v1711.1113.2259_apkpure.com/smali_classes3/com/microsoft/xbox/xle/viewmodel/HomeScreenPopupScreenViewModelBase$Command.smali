.class public final enum Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;
.super Ljava/lang/Enum;
.source "HomeScreenPopupScreenViewModelBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Command"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

.field public static final enum ADD_PIN:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

.field public static final enum GET_ON_XBOX_ONE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

.field public static final enum LAUNCH_GAMEPROFILE_PAGE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

.field public static final enum ONE_GUIDE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

.field public static final enum PLAY_LOCAL:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

.field public static final enum PLAY_TO_FILL:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

.field public static final enum PLAY_TO_FULL:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

.field public static final enum PLAY_TO_SNAP:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

.field public static final enum PLAY_TO_XBOX_ONE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

.field public static final enum REMOTE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

.field public static final enum REMOVE_PIN:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

.field public static final enum SHOW_COMPANION:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

.field public static final enum SHOW_DETAILS_PAGE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

.field public static final enum STREAM_TV:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

.field public static final enum UNSNAP:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;


# instance fields
.field public final imageResId:I

.field public final titleResId:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const v4, 0x7f070fb8

    .line 59
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    const-string v1, "PLAY_LOCAL"

    const v2, 0x7f07069d

    invoke-direct {v0, v1, v5, v2, v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->PLAY_LOCAL:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    .line 60
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    const-string v1, "GET_ON_XBOX_ONE"

    const v2, 0x7f07005f

    const v3, 0x7f070f1f

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->GET_ON_XBOX_ONE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    .line 61
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    const-string v1, "PLAY_TO_XBOX_ONE"

    const v2, 0x7f070693

    invoke-direct {v0, v1, v7, v2, v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->PLAY_TO_XBOX_ONE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    .line 62
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    const-string v1, "PLAY_TO_FULL"

    const v2, 0x7f070691

    invoke-direct {v0, v1, v8, v2, v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->PLAY_TO_FULL:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    .line 63
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    const-string v1, "PLAY_TO_FILL"

    const/4 v2, 0x4

    const v3, 0x7f070690

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->PLAY_TO_FILL:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    .line 64
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    const-string v1, "PLAY_TO_SNAP"

    const/4 v2, 0x5

    const v3, 0x7f070692

    const v4, 0x7f070eaa

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->PLAY_TO_SNAP:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    .line 65
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    const-string v1, "SHOW_DETAILS_PAGE"

    const/4 v2, 0x6

    const v3, 0x7f070672

    const v4, 0x7f070f18

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->SHOW_DETAILS_PAGE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    .line 66
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    const-string v1, "LAUNCH_GAMEPROFILE_PAGE"

    const/4 v2, 0x7

    const v3, 0x7f070685

    const v4, 0x7f070f4d

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->LAUNCH_GAMEPROFILE_PAGE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    .line 67
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    const-string v1, "SHOW_COMPANION"

    const/16 v2, 0x8

    const v3, 0x7f070697

    const v4, 0x7f071014

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->SHOW_COMPANION:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    .line 68
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    const-string v1, "ONE_GUIDE"

    const/16 v2, 0x9

    const v3, 0x7f070385

    const v4, 0x7f070f28

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->ONE_GUIDE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    .line 69
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    const-string v1, "STREAM_TV"

    const/16 v2, 0xa

    const v3, 0x7f07048c

    const v4, 0x7f071024

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->STREAM_TV:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    .line 70
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    const-string v1, "ADD_PIN"

    const/16 v2, 0xb

    const v3, 0x7f070065

    const v4, 0x7f070fb5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->ADD_PIN:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    .line 71
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    const-string v1, "REMOVE_PIN"

    const/16 v2, 0xc

    const v3, 0x7f07006c

    const v4, 0x7f07103b

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->REMOVE_PIN:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    .line 72
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    const-string v1, "REMOTE"

    const/16 v2, 0xd

    const v3, 0x7f070673

    const v4, 0x7f071015

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->REMOTE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    .line 73
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    const-string v1, "UNSNAP"

    const/16 v2, 0xe

    const v3, 0x7f07069b

    const v4, 0x7f071038    # 1.7953E38f

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->UNSNAP:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    .line 57
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->PLAY_LOCAL:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->GET_ON_XBOX_ONE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->PLAY_TO_XBOX_ONE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    aput-object v1, v0, v7

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->PLAY_TO_FULL:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    aput-object v1, v0, v8

    const/4 v1, 0x4

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->PLAY_TO_FILL:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->PLAY_TO_SNAP:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->SHOW_DETAILS_PAGE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->LAUNCH_GAMEPROFILE_PAGE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->SHOW_COMPANION:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->ONE_GUIDE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->STREAM_TV:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->ADD_PIN:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->REMOVE_PIN:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->REMOTE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->UNSNAP:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p3, "titleResId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 85
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;-><init>(Ljava/lang/String;III)V

    .line 86
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "titleResId"    # I
    .param p4, "imageResId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 80
    iput p4, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->imageResId:I

    .line 81
    iput p3, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->titleResId:I

    .line 82
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 57
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    return-object v0
.end method


# virtual methods
.method public getContentDescriptionId()I
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->ONE_GUIDE:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    if-ne p0, v0, :cond_0

    .line 90
    const v0, 0x7f070d6d

    .line 92
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;->titleResId:I

    goto :goto_0
.end method
