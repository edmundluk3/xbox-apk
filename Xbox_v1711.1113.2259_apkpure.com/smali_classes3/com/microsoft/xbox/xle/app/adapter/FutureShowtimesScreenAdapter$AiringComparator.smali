.class Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter$AiringComparator;
.super Ljava/lang/Object;
.source "FutureShowtimesScreenAdapter.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AiringComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter$AiringComparator;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter$1;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter$AiringComparator;-><init>(Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter;)V

    return-void
.end method


# virtual methods
.method public compare(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;)I
    .locals 5
    .param p1, "lhs"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;
    .param p2, "rhs"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;

    .prologue
    .line 42
    iget-object v3, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->StartTime:Ljava/util/Date;

    iget-object v4, p2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->StartTime:Ljava/util/Date;

    invoke-virtual {v3, v4}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v1

    .line 45
    .local v1, "result":I
    if-nez v1, :cond_0

    .line 46
    iget-object v3, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->HeadendId:Ljava/lang/String;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProvider(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v0

    .line 47
    .local v0, "left_provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    iget-object v3, p2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;->HeadendId:Ljava/lang/String;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProvider(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v2

    .line 48
    .local v2, "right_provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    .line 49
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getProviderName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getProviderName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    .line 53
    .end local v0    # "left_provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    .end local v2    # "right_provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    :cond_0
    return v1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 38
    check-cast p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;

    check-cast p2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/FutureShowtimesScreenAdapter$AiringComparator;->compare(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;)I

    move-result v0

    return v0
.end method
