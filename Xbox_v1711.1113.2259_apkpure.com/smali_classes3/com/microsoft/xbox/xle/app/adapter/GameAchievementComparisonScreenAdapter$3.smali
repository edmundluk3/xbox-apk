.class Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter$3;
.super Ljava/lang/Object;
.source "GameAchievementComparisonScreenAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 111
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;

    if-eqz v1, :cond_0

    .line 118
    :goto_0
    return-void

    .line 115
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;

    .line 116
    .local v0, "item":Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->navigateToAchievementDetails(Lcom/microsoft/xbox/xle/viewmodel/GameProgressAchievementsCompareItem;)V

    goto :goto_0
.end method
