.class public final Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTagGroup$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_EditorialDataTypes_SystemTagGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTagGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;",
        ">;"
    }
.end annotation


# instance fields
.field private defaultGroupName:Ljava/lang/String;

.field private defaultTags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;",
            ">;"
        }
    .end annotation
.end field

.field private final groupNameAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final tagsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 4
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTagGroup$GsonTypeAdapter;->defaultGroupName:Ljava/lang/String;

    .line 24
    iput-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTagGroup$GsonTypeAdapter;->defaultTags:Ljava/util/List;

    .line 26
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTagGroup$GsonTypeAdapter;->groupNameAdapter:Lcom/google/gson/TypeAdapter;

    .line 27
    const-class v0, Ljava/util/List;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/reflect/Type;

    const/4 v2, 0x0

    const-class v3, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTagGroup$GsonTypeAdapter;->tagsAdapter:Lcom/google/gson/TypeAdapter;

    .line 28
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;
    .locals 5
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v3

    sget-object v4, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v3, v4, :cond_0

    .line 54
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 55
    const/4 v3, 0x0

    .line 81
    :goto_0
    return-object v3

    .line 57
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTagGroup$GsonTypeAdapter;->defaultGroupName:Ljava/lang/String;

    .line 59
    .local v1, "groupName":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTagGroup$GsonTypeAdapter;->defaultTags:Ljava/util/List;

    .line 60
    .local v2, "tags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;>;"
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 61
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 62
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v3

    sget-object v4, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v3, v4, :cond_1

    .line 63
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 66
    :cond_1
    const/4 v3, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v3, :pswitch_data_0

    .line 76
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 66
    :sswitch_0
    const-string v4, "systemTagGroup.name"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x0

    goto :goto_2

    :sswitch_1
    const-string v4, "systemTagGroup.Tags"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x1

    goto :goto_2

    .line 68
    :pswitch_0
    iget-object v3, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTagGroup$GsonTypeAdapter;->groupNameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v3, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "groupName":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 69
    .restart local v1    # "groupName":Ljava/lang/String;
    goto :goto_1

    .line 72
    :pswitch_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTagGroup$GsonTypeAdapter;->tagsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v3, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "tags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;>;"
    check-cast v2, Ljava/util/List;

    .line 73
    .restart local v2    # "tags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;>;"
    goto :goto_1

    .line 80
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 81
    new-instance v3, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTagGroup;

    invoke-direct {v3, v1, v2}, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTagGroup;-><init>(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0

    .line 66
    :sswitch_data_0
    .sparse-switch
        0x33b88793 -> :sswitch_1
        0x33c459e5 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTagGroup$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultGroupName(Ljava/lang/String;)Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTagGroup$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultGroupName"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTagGroup$GsonTypeAdapter;->defaultGroupName:Ljava/lang/String;

    .line 31
    return-object p0
.end method

.method public setDefaultTags(Ljava/util/List;)Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTagGroup$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;",
            ">;)",
            "Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTagGroup$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 34
    .local p1, "defaultTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTag;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTagGroup$GsonTypeAdapter;->defaultTags:Ljava/util/List;

    .line 35
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    if-nez p2, :cond_0

    .line 41
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 50
    :goto_0
    return-void

    .line 44
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 45
    const-string v0, "systemTagGroup.name"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTagGroup$GsonTypeAdapter;->groupNameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;->groupName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 47
    const-string v0, "systemTagGroup.Tags"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTagGroup$GsonTypeAdapter;->tagsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;->tags()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 49
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    check-cast p2, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/socialTags/AutoValue_EditorialDataTypes_SystemTagGroup$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagGroup;)V

    return-void
.end method
