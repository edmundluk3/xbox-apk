.class public Lcom/microsoft/xbox/smartglass/TextConfiguration;
.super Ljava/lang/Object;
.source "TextConfiguration.java"


# instance fields
.field public final inputScope:Lcom/microsoft/xbox/smartglass/TextInputScope;

.field public final locale:Ljava/lang/String;

.field public final maxTextLength:I

.field public final options:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/TextOptions;",
            ">;"
        }
    .end annotation
.end field

.field public final prompt:Ljava/lang/String;


# direct methods
.method constructor <init>(IILjava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "options"    # I
    .param p2, "inputScope"    # I
    .param p3, "locale"    # Ljava/lang/String;
    .param p4, "prompt"    # Ljava/lang/String;
    .param p5, "maxTextLength"    # I

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {p1}, Lcom/microsoft/xbox/smartglass/TextOptions;->fromInt(I)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/TextConfiguration;->options:Ljava/util/EnumSet;

    .line 31
    invoke-static {p2}, Lcom/microsoft/xbox/smartglass/TextInputScope;->fromInt(I)Lcom/microsoft/xbox/smartglass/TextInputScope;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/TextConfiguration;->inputScope:Lcom/microsoft/xbox/smartglass/TextInputScope;

    .line 32
    iput-object p3, p0, Lcom/microsoft/xbox/smartglass/TextConfiguration;->locale:Ljava/lang/String;

    .line 33
    iput-object p4, p0, Lcom/microsoft/xbox/smartglass/TextConfiguration;->prompt:Ljava/lang/String;

    .line 34
    iput p5, p0, Lcom/microsoft/xbox/smartglass/TextConfiguration;->maxTextLength:I

    .line 35
    return-void
.end method
