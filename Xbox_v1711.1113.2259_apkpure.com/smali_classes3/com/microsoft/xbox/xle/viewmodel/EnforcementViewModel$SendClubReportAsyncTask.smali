.class Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "EnforcementViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SendClubReportAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private clubReportItemRequest:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;

.field private reportTargetId:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;Ljava/lang/String;)V
    .locals 0
    .param p2, "clubReportItemRequest"    # Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;
    .param p3, "reportTargetId"    # Ljava/lang/String;

    .prologue
    .line 315
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 316
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;->clubReportItemRequest:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;

    .line 317
    iput-object p3, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;->reportTargetId:Ljava/lang/String;

    .line 318
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 322
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 5

    .prologue
    .line 349
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getClubModerationService()Lcom/microsoft/xbox/service/clubs/IClubModerationService;

    move-result-object v0

    .line 352
    .local v0, "clubModerationService":Lcom/microsoft/xbox/service/clubs/IClubModerationService;
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;->reportTargetId:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;->clubReportItemRequest:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;

    invoke-interface {v0, v2, v3, v4}, Lcom/microsoft/xbox/service/clubs/IClubModerationService;->reportItem(JLcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportItemRequest;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 356
    :goto_0
    return-object v2

    .line 352
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 355
    :catch_0
    move-exception v1

    .line 356
    .local v1, "xex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 311
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 344
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 311
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 327
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->access$902(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;Z)Z

    .line 328
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->access$1000(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 329
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 338
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->access$902(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;Z)Z

    .line 339
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->access$1000(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 340
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 311
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$SendClubReportAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;->access$902(Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel;Z)Z

    .line 334
    return-void
.end method
