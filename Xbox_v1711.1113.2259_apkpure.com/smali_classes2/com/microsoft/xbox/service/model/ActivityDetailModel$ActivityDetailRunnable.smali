.class Lcom/microsoft/xbox/service/model/ActivityDetailModel$ActivityDetailRunnable;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ActivityDetailModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/ActivityDetailModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActivityDetailRunnable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/ActivityDetailModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/ActivityDetailModel;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/ActivityDetailModel$ActivityDetailRunnable;->this$0:Lcom/microsoft/xbox/service/model/ActivityDetailModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/ActivityDetailModel;Lcom/microsoft/xbox/service/model/ActivityDetailModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/ActivityDetailModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/ActivityDetailModel$1;

    .prologue
    .line 117
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/ActivityDetailModel$ActivityDetailRunnable;-><init>(Lcom/microsoft/xbox/service/model/ActivityDetailModel;)V

    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 124
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getEDSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;

    move-result-object v1

    .line 125
    .local v1, "serviceManager":Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/ActivityDetailModel$ActivityDetailRunnable;->this$0:Lcom/microsoft/xbox/service/model/ActivityDetailModel;

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->access$100(Lcom/microsoft/xbox/service/model/ActivityDetailModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v2

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->ID:Ljava/lang/String;

    const-wide/16 v4, -0x1

    const/16 v6, 0x8

    move-object v7, v3

    invoke-interface/range {v1 .. v7}, Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;->getMediaItemDetail(Ljava/lang/String;Ljava/lang/String;JILjava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .line 127
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getParentIDs()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getParentIDs()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 128
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getParentIDs()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/service/network/managers/xblshared/IEDSServiceManager;->getTitleImageFromId(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v8

    .line 129
    .local v8, "parents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    invoke-virtual {v0, v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->setParentItems(Ljava/util/List;)V

    .line 132
    .end local v8    # "parents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;>;"
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/ActivityDetailModel$ActivityDetailRunnable;->buildData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 142
    const-wide/16 v0, 0xfb7

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 137
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/ActivityDetailModel$ActivityDetailRunnable;->this$0:Lcom/microsoft/xbox/service/model/ActivityDetailModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/ActivityDetailModel;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 138
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 120
    return-void
.end method
