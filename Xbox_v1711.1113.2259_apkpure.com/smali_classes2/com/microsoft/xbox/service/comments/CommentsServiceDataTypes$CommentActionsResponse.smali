.class public final Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;
.super Ljava/lang/Object;
.source "CommentsServiceDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CommentActionsResponse"
.end annotation


# instance fields
.field private final actions:Ljava/util/List;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentAction;",
            ">;"
        }
    .end annotation
.end field

.field public final commentCount:J

.field public final continuationToken:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private volatile transient hashCode:I

.field public final lastSeenAlertId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final likeCount:J

.field public final newAlertCount:J

.field public final shareCount:J

.field public final xuid:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;JJJLjava/lang/String;Ljava/util/List;)V
    .locals 1
    .param p1, "commentCount"    # J
    .param p3, "continuationToken"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "lastSeenAlertId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "likeCount"    # J
    .param p7, "newAlertCount"    # J
    .param p9, "shareCount"    # J
    .param p11, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p12    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JJJ",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentAction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    .local p12, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentAction;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-wide p1, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->commentCount:J

    .line 65
    iput-object p3, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->continuationToken:Ljava/lang/String;

    .line 66
    iput-object p4, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->lastSeenAlertId:Ljava/lang/String;

    .line 67
    iput-wide p5, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->likeCount:J

    .line 68
    iput-wide p7, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->newAlertCount:J

    .line 69
    iput-wide p9, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->shareCount:J

    .line 70
    iput-object p11, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->xuid:Ljava/lang/String;

    .line 71
    invoke-static {p12}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->actions:Ljava/util/List;

    .line 72
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 81
    if-ne p1, p0, :cond_1

    .line 87
    :cond_0
    :goto_0
    return v1

    .line 83
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;

    if-nez v3, :cond_2

    move v1, v2

    .line 84
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 86
    check-cast v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;

    .line 87
    .local v0, "other":Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;
    iget-wide v4, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->commentCount:J

    iget-wide v6, v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->commentCount:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->continuationToken:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->continuationToken:Ljava/lang/String;

    .line 88
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->lastSeenAlertId:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->lastSeenAlertId:Ljava/lang/String;

    .line 89
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-wide v4, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->likeCount:J

    iget-wide v6, v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->likeCount:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-wide v4, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->newAlertCount:J

    iget-wide v6, v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->newAlertCount:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-wide v4, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->shareCount:J

    iget-wide v6, v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->shareCount:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->xuid:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->xuid:Ljava/lang/String;

    .line 93
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->actions:Ljava/util/List;

    iget-object v4, v0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->actions:Ljava/util/List;

    .line 94
    invoke-interface {v3, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getActions()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentAction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->actions:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 100
    iget v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->hashCode:I

    if-nez v0, :cond_0

    .line 101
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->hashCode:I

    .line 102
    iget v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->commentCount:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->hashCode:I

    .line 103
    iget v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->continuationToken:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->hashCode:I

    .line 104
    iget v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->lastSeenAlertId:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->hashCode:I

    .line 105
    iget v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->likeCount:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->hashCode:I

    .line 106
    iget v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->newAlertCount:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->hashCode:I

    .line 107
    iget v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->shareCount:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->hashCode:I

    .line 108
    iget v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->xuid:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->hashCode:I

    .line 109
    iget v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->actions:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->hashCode:I

    .line 112
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/comments/CommentsServiceDataTypes$CommentActionsResponse;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
