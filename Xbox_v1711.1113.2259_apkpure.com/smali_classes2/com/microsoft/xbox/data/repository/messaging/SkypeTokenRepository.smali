.class public final enum Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;
.super Ljava/lang/Enum;
.source "SkypeTokenRepository.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

.field public static final enum INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

.field private static final TOKEN_EXPIRATION_BUFFER:I = 0xdbba0


# instance fields
.field private expiration:Ljava/util/Date;

.field private registrationToken:Ljava/lang/String;

.field private rxTokenFetcher:Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Single",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private skypeToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 13
    new-instance v0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    .line 12
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    sget-object v1, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->INSTANCE:Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->$VALUES:[Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->storeResult(Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;)V

    return-void
.end method

.method static synthetic lambda$getToken$0()Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 33
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v0

    invoke-interface {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getSkypeToken()Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized storeResult(Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;)V
    .locals 6
    .param p1, "result"    # Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;

    .prologue
    .line 61
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;->skypetoken()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->skypeToken:Ljava/lang/String;

    .line 62
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->registrationToken:Ljava/lang/String;

    .line 64
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;->expiresIn()I

    move-result v4

    mul-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    add-long/2addr v2, v4

    const-wide/32 v4, 0xdbba0

    sub-long v0, v2, v4

    .line 65
    .local v0, "expirationTime":J
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    iput-object v2, p0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->expiration:Ljava/util/Date;

    .line 67
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->rxTokenFetcher:Lio/reactivex/Single;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    monitor-exit p0

    return-void

    .line 61
    .end local v0    # "expirationTime":J
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->$VALUES:[Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;

    return-object v0
.end method


# virtual methods
.method public getRegistrationToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->registrationToken:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized getToken()Lio/reactivex/Single;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->expiration:Ljava/util/Date;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->expiration:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->reset()V

    .line 30
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->skypeToken:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->rxTokenFetcher:Lio/reactivex/Single;

    if-nez v0, :cond_1

    .line 32
    invoke-static {}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository$$Lambda$1;->lambdaFactory$()Ljava/util/concurrent/Callable;

    move-result-object v0

    .line 33
    invoke-static {v0}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 34
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 35
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, ""

    .line 36
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->onErrorReturnItem(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->rxTokenFetcher:Lio/reactivex/Single;

    .line 39
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->rxTokenFetcher:Lio/reactivex/Single;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 41
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->skypeToken:Ljava/lang/String;

    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 26
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized reset()V
    .locals 1

    .prologue
    .line 54
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->skypeToken:Ljava/lang/String;

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->registrationToken:Ljava/lang/String;

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->expiration:Ljava/util/Date;

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->rxTokenFetcher:Lio/reactivex/Single;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    monitor-exit p0

    return-void

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setRegistrationToken(Ljava/lang/String;)V
    .locals 0
    .param p1, "registrationToken"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/messaging/SkypeTokenRepository;->registrationToken:Ljava/lang/String;

    .line 47
    return-void
.end method
