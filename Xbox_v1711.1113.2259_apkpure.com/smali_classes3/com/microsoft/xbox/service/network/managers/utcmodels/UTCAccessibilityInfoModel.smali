.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAccessibilityInfoModel;
.super Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCJsonBase;
.source "UTCAccessibilityInfoModel.java"


# static fields
.field public static final DEFAULTSERVICES:Ljava/lang/String; = "none"

.field private static accessibilityInfo:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static instance:Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAccessibilityInfoModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAccessibilityInfoModel;->accessibilityInfo:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCJsonBase;-><init>()V

    .line 29
    return-void
.end method

.method private addValue(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 38
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAccessibilityInfoModel;->accessibilityInfo:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAccessibilityInfoModel;->accessibilityInfo:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 39
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAccessibilityInfoModel;->accessibilityInfo:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    :cond_0
    return-void
.end method

.method public static declared-synchronized getAccessibilityInfo()Ljava/lang/String;
    .locals 11

    .prologue
    .line 61
    const-class v4, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAccessibilityInfoModel;

    monitor-enter v4

    :try_start_0
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAccessibilityInfoModel;->instance:Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAccessibilityInfoModel;

    if-nez v3, :cond_3

    .line 64
    new-instance v3, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAccessibilityInfoModel;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAccessibilityInfoModel;-><init>()V

    sput-object v3, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAccessibilityInfoModel;->instance:Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAccessibilityInfoModel;

    .line 67
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAccessibilityInfoModel;->instance:Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAccessibilityInfoModel;

    const-string v5, "isenabled"

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->AccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v6}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-direct {v3, v5, v6}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAccessibilityInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 68
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->AccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    const/4 v5, -0x1

    invoke-virtual {v3, v5}, Landroid/view/accessibility/AccessibilityManager;->getEnabledAccessibilityServiceList(I)Ljava/util/List;

    move-result-object v1

    .line 69
    .local v1, "serviceInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/accessibilityservice/AccessibilityServiceInfo;>;"
    const-string v2, "none"

    .line 70
    .local v2, "services":Ljava/lang/String;
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accessibilityservice/AccessibilityServiceInfo;

    .line 71
    .local v0, "info":Landroid/accessibilityservice/AccessibilityServiceInfo;
    if-eqz v0, :cond_0

    .line 72
    const-string v5, "none"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 73
    invoke-virtual {v0}, Landroid/accessibilityservice/AccessibilityServiceInfo;->getId()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 75
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, ";%s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v0}, Landroid/accessibilityservice/AccessibilityServiceInfo;->getId()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 80
    .end local v0    # "info":Landroid/accessibilityservice/AccessibilityServiceInfo;
    :cond_2
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAccessibilityInfoModel;->instance:Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAccessibilityInfoModel;

    const-string v5, "enabledservices"

    invoke-direct {v3, v5, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAccessibilityInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 84
    :cond_3
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAccessibilityInfoModel;->instance:Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAccessibilityInfoModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAccessibilityInfoModel;->toJson()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    monitor-exit v4

    return-object v3

    .line 61
    .end local v2    # "services":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3
.end method


# virtual methods
.method public toJson()Ljava/lang/String;
    .locals 6

    .prologue
    .line 45
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAccessibilityInfoModel;->accessibilityInfo:Ljava/util/HashMap;

    .line 46
    .local v1, "objects":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v2, ""

    .line 48
    .local v2, "result":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/gson/JsonIOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 52
    :goto_0
    return-object v2

    .line 49
    :catch_0
    move-exception v0

    .line 50
    .local v0, "e":Lcom/google/gson/JsonIOException;
    const-string v3, "UTCJsonSerializer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error in json serialization"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/gson/JsonIOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
