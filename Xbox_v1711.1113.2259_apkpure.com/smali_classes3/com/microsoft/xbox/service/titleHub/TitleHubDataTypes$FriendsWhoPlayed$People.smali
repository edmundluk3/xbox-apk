.class public final Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;
.super Ljava/lang/Object;
.source "TitleHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "People"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;
    }
.end annotation


# instance fields
.field public final displayPicRaw:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final gamertag:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private volatile transient hashCode:I

.field public final isCurrentlyPlaying:Z

.field public final isFavorite:Z

.field public final lastTimePlayed:Ljava/util/Date;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final preferredColor:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final presenceState:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final useAvatar:Z

.field public final xuid:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZZLjava/util/Date;Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;Ljava/lang/String;ZJ)V
    .locals 1
    .param p1, "displayPicRaw"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "gamertag"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "isCurrentlyPlaying"    # Z
    .param p4, "isFavorite"    # Z
    .param p5, "lastTimePlayed"    # Ljava/util/Date;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "preferredColor"    # Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p7, "presenceState"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p8, "useAvatar"    # Z
    .param p9, "xuid"    # J

    .prologue
    .line 512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 513
    iput-object p1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->displayPicRaw:Ljava/lang/String;

    .line 514
    iput-object p2, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->gamertag:Ljava/lang/String;

    .line 515
    iput-boolean p3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->isCurrentlyPlaying:Z

    .line 516
    iput-boolean p4, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->isFavorite:Z

    .line 517
    iput-object p5, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->lastTimePlayed:Ljava/util/Date;

    .line 518
    iput-object p6, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->preferredColor:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;

    .line 519
    iput-object p7, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->presenceState:Ljava/lang/String;

    .line 520
    iput-boolean p8, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->useAvatar:Z

    .line 521
    iput-wide p9, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->xuid:J

    .line 522
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 526
    if-ne p1, p0, :cond_1

    .line 532
    :cond_0
    :goto_0
    return v1

    .line 528
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;

    if-nez v3, :cond_2

    move v1, v2

    .line 529
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 531
    check-cast v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;

    .line 532
    .local v0, "other":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;
    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->displayPicRaw:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->displayPicRaw:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->gamertag:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->gamertag:Ljava/lang/String;

    .line 533
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-boolean v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->isCurrentlyPlaying:Z

    iget-boolean v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->isCurrentlyPlaying:Z

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->isFavorite:Z

    iget-boolean v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->isFavorite:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->lastTimePlayed:Ljava/util/Date;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->lastTimePlayed:Ljava/util/Date;

    .line 536
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->preferredColor:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->preferredColor:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;

    .line 537
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->presenceState:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->presenceState:Ljava/lang/String;

    .line 538
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-boolean v3, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->useAvatar:Z

    iget-boolean v4, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->useAvatar:Z

    if-ne v3, v4, :cond_3

    iget-wide v4, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->xuid:J

    iget-wide v6, v0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->xuid:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 546
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->hashCode:I

    if-nez v0, :cond_0

    .line 547
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->hashCode:I

    .line 548
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->displayPicRaw:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->hashCode:I

    .line 549
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->gamertag:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->hashCode:I

    .line 550
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->isCurrentlyPlaying:Z

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->hashCode:I

    .line 551
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->isFavorite:Z

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->hashCode:I

    .line 552
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->lastTimePlayed:Ljava/util/Date;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->hashCode:I

    .line 553
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->preferredColor:Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People$PreferredColor;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->hashCode:I

    .line 554
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->presenceState:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->hashCode:I

    .line 555
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->useAvatar:Z

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->hashCode:I

    .line 556
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->xuid:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->hashCode:I

    .line 559
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$FriendsWhoPlayed$People;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 564
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
