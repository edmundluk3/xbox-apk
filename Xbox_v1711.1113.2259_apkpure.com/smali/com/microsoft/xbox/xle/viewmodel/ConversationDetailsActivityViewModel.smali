.class public Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;
.source "ConversationDetailsActivityViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SkypeLongPollAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SubscribeSkypeLongPollAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LoadSenderProfileAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LeaveConversationAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$MuteConversationAsyncTask;
    }
.end annotation


# static fields
.field private static final ADD_MEMBER_PRIVACY_CHECK_STATUS:Ljava/lang/String; = "ADD_MEMBER_PRIVACY_CHECK_STATUS"

.field private static final REQUEST_OVERLAY_PERM_CODE:I = 0x4d2

.field private static final SKYPE_LONG_POLL_MAX_RETRY:I = 0x3

.field private static final SKYPE_LONG_POLL_RETRY_DELAY:I = 0x7d0

.field private static final TAG:Ljava/lang/String;

.field private static final TOPIC_CHANGE_PRIVACY_CHECK_STATUS:Ljava/lang/String; = "TOPIC_CHANGE_PRIVACY_CHECK_STATUS"


# instance fields
.field protected addMemberList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;",
            ">;"
        }
    .end annotation
.end field

.field private blockingText:Ljava/lang/String;

.field private errorAddingAtleastOneMemberToConversation:Z

.field private forceRefresh:Z

.field hoverChatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field hoverChatTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private isLeavingConversation:Z

.field public isLoadingSenderProfile:Z

.field private isLoadingUserPrivacyData:Z

.field private isMutingConversation:Z

.field private isUpdatingTopicName:Z

.field private leaveConversationAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LeaveConversationAsyncTask;

.field private loadSenderProfileTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LoadSenderProfileAsyncTask;

.field private mapAddMemberToConversationTask:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;",
            ">;"
        }
    .end annotation
.end field

.field private mapIsAddingMemberToConversation:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private muteConversationAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$MuteConversationAsyncTask;

.field private newTopicName:Ljava/lang/String;

.field private profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private shouldforceUpdateAdapter:Z

.field private skypeLongPollAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SkypeLongPollAsyncTask;

.field private skypeLongPollRetryCount:I

.field private subscribeSkypeLongPollAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SubscribeSkypeLongPollAsyncTask;

.field private updateTopicNameAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 108
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 109
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    .locals 5
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .param p2, "parameters"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    .prologue
    const/4 v2, 0x0

    .line 112
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 80
    new-instance v3, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-direct {v3}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;-><init>()V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->mapIsAddingMemberToConversation:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    .line 81
    new-instance v3, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-direct {v3}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;-><init>()V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->mapAddMemberToConversationTask:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    .line 82
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->forceRefresh:Z

    .line 97
    iput v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->skypeLongPollRetryCount:I

    .line 114
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v3}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v3

    invoke-interface {v3, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)V

    .line 116
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->getInstance()Lcom/microsoft/xbox/service/model/MessageModel;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    .line 117
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 118
    new-instance v3, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 119
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedConversationSummary()Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 120
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-nez v3, :cond_1

    .line 121
    invoke-virtual {p2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSenderXuid()Ljava/lang/String;

    move-result-object v0

    .line 122
    .local v0, "senderXuid":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 123
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/service/model/MessageModel;->getConversationSummaryById(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 124
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-nez v3, :cond_0

    .line 125
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v4, "Creating empty conversation summary"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->synthesizeConversationSummary(Ljava/lang/String;)V

    .line 130
    :cond_0
    invoke-virtual {p2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getConversationTopic()Ljava/lang/String;

    move-result-object v1

    .line 131
    .local v1, "topic":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-eqz v3, :cond_1

    .line 132
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iput-object v1, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->groupTopic:Ljava/lang/String;

    .line 136
    .end local v0    # "senderXuid":Ljava/lang/String;
    .end local v1    # "topic":Ljava/lang/String;
    :cond_1
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070617

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->blockingText:Ljava/lang/String;

    .line 137
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-eqz v3, :cond_2

    const/4 v2, 0x1

    :cond_2
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 138
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-eqz v2, :cond_3

    .line 139
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->isSkypeServiceMessage(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isServiceMessage:Z

    .line 140
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->isSkypeGroupMessage(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isGroupConversation:Z

    .line 146
    :cond_3
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->onMuteConversationCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->onSubscribeOrStartSkypeLongPollFailed()V

    return-void
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->resetSkypeLongPollRetryCount()V

    return-void
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isMutingConversation:Z

    return p1
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->onUpdateTopicNameCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$402(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isUpdatingTopicName:Z

    return p1
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->onLeaveConversationCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$602(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isLeavingConversation:Z

    return p1
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->onAddMemberToConversationCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;)V

    return-void
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->startSkypeLongPoll()V

    return-void
.end method

.method static synthetic access$900()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)V
    .locals 0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->subscribeSkypeLongPoll()V

    return-void
.end method

.method private onAddMemberToConversationCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;)V
    .locals 5
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "item"    # Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 670
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "onAddMemberToConversationCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->xuid:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 673
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->mapIsAddingMemberToConversation:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    iget-object v1, p2, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->xuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 674
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->mapIsAddingMemberToConversation:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    iget-object v1, p2, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->xuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->remove(Ljava/lang/Object;)V

    .line 679
    :cond_0
    :goto_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 694
    :cond_1
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isAddingMembersToConversation()Z

    move-result v0

    if-nez v0, :cond_5

    .line 695
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->errorAddingAtleastOneMemberToConversation:Z

    if-eqz v0, :cond_2

    .line 696
    const v0, 0x7f070655

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->showError(I)V

    .line 697
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->errorAddingAtleastOneMemberToConversation:Z

    .line 700
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->addMemberList:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 701
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->addMemberList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 704
    :cond_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->loadConversationDetailAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;

    if-eqz v0, :cond_4

    .line 705
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->loadConversationDetailAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->cancel()V

    .line 707
    :cond_4
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->loadConversationDetailAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;

    .line 708
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->loadConversationDetailAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->forceRefresh:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->load(Z)V

    .line 711
    :cond_5
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->updateAdapter()V

    .line 712
    return-void

    .line 676
    :cond_6
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Unexpected error"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 686
    :pswitch_1
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->errorAddingAtleastOneMemberToConversation:Z

    .line 687
    if-eqz p2, :cond_1

    .line 688
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to add member %s to group successfully"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->getGamertag()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 679
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onLeaveConversationCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 583
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "onLeaveConversationCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isLeavingConversation:Z

    .line 586
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 598
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->updateAdapter()V

    .line 599
    return-void

    .line 590
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "User removed from conversation"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 594
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to remove user from conversation"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    const v0, 0x7f070656

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->showError(I)V

    goto :goto_0

    .line 586
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onMuteConversationCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v0, 0x0

    .line 602
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v2, "onMuteConversationCompleted"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isMutingConversation:Z

    .line 605
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 616
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->updateAdapter()V

    .line 617
    return-void

    .line 609
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-boolean v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->alerts:Z

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->alerts:Z

    goto :goto_0

    .line 613
    :pswitch_1
    const v0, 0x7f070658

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->showError(I)V

    goto :goto_0

    .line 605
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onSubscribeOrStartSkypeLongPollFailed()V
    .locals 4

    .prologue
    .line 814
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->skypeLongPollRetryCount:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 815
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->skypeLongPollRetryCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->skypeLongPollRetryCount:I

    .line 816
    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)Ljava/lang/Runnable;

    move-result-object v0

    const-wide/16 v2, 0x7d0

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPostDelayed(Ljava/lang/Runnable;J)V

    .line 818
    :cond_0
    return-void
.end method

.method private onUpdateTopicNameCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "topic"    # Ljava/lang/String;

    .prologue
    .line 620
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "onUpdateTopicNameCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 621
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isUpdatingTopicName:Z

    .line 623
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 637
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->updateAdapter()V

    .line 638
    return-void

    .line 627
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-eqz v0, :cond_0

    .line 628
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iput-object p2, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->groupTopic:Ljava/lang/String;

    goto :goto_0

    .line 634
    :pswitch_1
    const v0, 0x7f070657

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->showError(I)V

    goto :goto_0

    .line 623
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private openAsHoverChatInternal()V
    .locals 2

    .prologue
    .line 1272
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->hoverChatTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->conversationOpenedFromDropDown(Ljava/lang/String;)V

    .line 1273
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->addMessageInstance(Landroid/content/Context;Ljava/lang/String;)V

    .line 1274
    return-void
.end method

.method private resetSkypeLongPollRetryCount()V
    .locals 1

    .prologue
    .line 810
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->skypeLongPollRetryCount:I

    .line 811
    return-void
.end method

.method private showErrorDialog(Lcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;)V
    .locals 9
    .param p1, "type"    # Lcom/microsoft/xbox/service/model/UpdateType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "permissionSetting"    # Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v8, 0x7f07064d

    const v7, 0x7f070651

    .line 419
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 420
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 422
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v3, "Update type: %s, Permission setting: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/UpdateType;->name()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07060f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 425
    .local v1, "title":Ljava/lang/String;
    const-string v0, ""

    .line 426
    .local v0, "body":Ljava/lang/String;
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 471
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0707c7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v0, v3, v4}, Lcom/microsoft/xbox/toolkit/DialogManager;->showFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 472
    return-void

    .line 428
    :pswitch_0
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$1;->$SwitchMap$com$microsoft$xbox$service$groupMessaging$SkypeGroupDataTypes$UserPermissionSetting:[I

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 438
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown permission setting: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070655

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 440
    goto :goto_0

    .line 430
    :pswitch_1
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070647

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 431
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 432
    goto :goto_0

    .line 434
    :pswitch_2
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070650

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 435
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07064f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 436
    goto :goto_0

    .line 444
    :pswitch_3
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$1;->$SwitchMap$com$microsoft$xbox$service$groupMessaging$SkypeGroupDataTypes$UserPermissionSetting:[I

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_2

    .line 454
    :pswitch_4
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070657

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 455
    goto :goto_0

    .line 446
    :pswitch_5
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070653

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 447
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 448
    goto/16 :goto_0

    .line 450
    :pswitch_6
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070654

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 451
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07064b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 452
    goto/16 :goto_0

    .line 459
    :pswitch_7
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$1;->$SwitchMap$com$microsoft$xbox$service$groupMessaging$SkypeGroupDataTypes$UserPermissionSetting:[I

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_3

    .line 465
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 466
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07074c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 461
    :pswitch_8
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 462
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 463
    goto/16 :goto_0

    .line 426
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_7
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 428
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 444
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_6
    .end packed-switch

    .line 459
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_8
    .end packed-switch
.end method

.method private startSkypeLongPoll()V
    .locals 2

    .prologue
    .line 800
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startSkypeLongPoll"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 801
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->skypeLongPollAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SkypeLongPollAsyncTask;

    if-eqz v0, :cond_0

    .line 802
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->skypeLongPollAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SkypeLongPollAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SkypeLongPollAsyncTask;->cancel()V

    .line 805
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SkypeLongPollAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SkypeLongPollAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->skypeLongPollAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SkypeLongPollAsyncTask;

    .line 806
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->skypeLongPollAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SkypeLongPollAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SkypeLongPollAsyncTask;->load(Z)V

    .line 807
    return-void
.end method

.method private subscribeSkypeLongPoll()V
    .locals 3

    .prologue
    .line 789
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getInstance()Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->getEDFRegId()Ljava/lang/String;

    move-result-object v0

    .line 791
    .local v0, "edfRegId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 792
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SubscribeSkypeLongPollAsyncTask;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SubscribeSkypeLongPollAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->subscribeSkypeLongPollAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SubscribeSkypeLongPollAsyncTask;

    .line 793
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->subscribeSkypeLongPollAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SubscribeSkypeLongPollAsyncTask;

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->forceRefresh:Z

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SubscribeSkypeLongPollAsyncTask;->load(Z)V

    .line 797
    :goto_0
    return-void

    .line 795
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "subscribeSkypeLongPoll - edfRegId is empty"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public clearForceUpdateAdapter()V
    .locals 1

    .prologue
    .line 1186
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->shouldforceUpdateAdapter:Z

    .line 1187
    return-void
.end method

.method public createGroupConversation()V
    .locals 2

    .prologue
    .line 1190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->hoverChatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->hoverChatIsOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1191
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->hoverChatTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->createGroupConvoSelected(Ljava/lang/String;)V

    .line 1196
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->navigateToFriendsPicker(Z)V

    .line 1197
    return-void

    .line 1193
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedConversationSummary()Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackCreateGroupConversation(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    goto :goto_0
.end method

.method public getBlockingStatusText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->blockingText:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentTopicName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 760
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-static {v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getGroupTopic(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGamerTagTitle()Ljava/lang/String;
    .locals 5

    .prologue
    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-eqz v0, :cond_1

    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->realName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderGamerTag:Ljava/lang/String;

    .line 180
    :goto_0
    return-object v0

    .line 176
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const-string v1, "%s (%s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderGamerTag:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->realName:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 179
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Conversation summary is null!"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const-string v0, ""

    goto :goto_0
.end method

.method public getListState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 1178
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public getSenderGamerTag()Ljava/lang/String;
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderGamerTag:Ljava/lang/String;

    .line 167
    :goto_0
    return-object v0

    .line 166
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Conversation summary is null!"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const-string v0, ""

    goto :goto_0
.end method

.method public getTypingData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MessageModel;->getTypingIndicatorList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public handleDeleteConversation()V
    .locals 7

    .prologue
    .line 1228
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->hoverChatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->hoverChatIsOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1229
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->hoverChatTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->leaveGroupConvoSelected(Ljava/lang/String;)V

    .line 1234
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->forceDismissAll()V

    .line 1235
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070645

    .line 1236
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070644

    .line 1237
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0707c7

    .line 1238
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)Ljava/lang/Runnable;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f07060d

    .line 1240
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    .line 1235
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/toolkit/DialogManager;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 1241
    return-void

    .line 1231
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedConversationSummary()Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackLeaveConversation(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    goto :goto_0
.end method

.method public isAddingMembersToConversation()Z
    .locals 3

    .prologue
    .line 273
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->mapIsAddingMemberToConversation:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->Values()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 274
    .local v0, "val":Ljava/lang/Boolean;
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 275
    const/4 v1, 0x1

    .line 279
    .end local v0    # "val":Ljava/lang/Boolean;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isBlockingBusy()Z
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MessageModel;->getIsDeleting()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MessageModel;->getIsSending()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 268
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isLoadingConversationDetail:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isLoadingSenderProfile:Z

    if-nez v0, :cond_0

    .line 269
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isLoadingAttachment()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isLeavingConversation:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isUpdatingTopicName:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isMutingConversation:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isAddingMembersToConversation()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isLoadingUserPrivacyData:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 268
    :goto_0
    return v0

    .line 269
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isConversationMuted()Z
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->alerts:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public launchUnsharedFeed()V
    .locals 3

    .prologue
    .line 1298
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->hoverChatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v1}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->hoverChatIsOpen()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1299
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->hoverChatTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->shareSelected(Ljava/lang/String;)V

    .line 1302
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;->Message:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;-><init>(Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$LaunchSource;Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    .line 1303
    .local v0, "params":Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel$UnsharedActivityFeedScreenParameters;
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/UnsharedActivityFeedScreen;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 1304
    return-void
.end method

.method public leaveConversation()V
    .locals 3

    .prologue
    .line 721
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isGroupConversation:Z

    if-eqz v1, :cond_1

    .line 722
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->hoverChatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;->with(Ljava/lang/String;)Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->onLeaveGroupMessage(Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$MessageKey;)V

    .line 725
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/activity/ConversationsActivity;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GotoScreenWithPop(Ljava/lang/Class;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 730
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->leaveConversationAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LeaveConversationAsyncTask;

    if-eqz v1, :cond_0

    .line 731
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->leaveConversationAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LeaveConversationAsyncTask;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LeaveConversationAsyncTask;->cancel()V

    .line 733
    :cond_0
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LeaveConversationAsyncTask;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LeaveConversationAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->leaveConversationAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LeaveConversationAsyncTask;

    .line 734
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->leaveConversationAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LeaveConversationAsyncTask;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LeaveConversationAsyncTask;->load(Z)V

    .line 738
    :goto_1
    return-void

    .line 726
    :catch_0
    move-exception v0

    .line 727
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Unable to navigate to Conversations list screen"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 736
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Not group conversation- can only leave a group conversation"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public load(Z)V
    .locals 5
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 490
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->forceRefresh:Z

    .line 492
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-nez v2, :cond_0

    .line 493
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 496
    :cond_0
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isGroupConversation:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->addMemberList:Ljava/util/List;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 498
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 499
    .local v1, "xuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->addMemberList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    .line 500
    .local v0, "member":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    iget-object v3, v0, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->xuid:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 503
    .end local v0    # "member":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->AddMemberPrivacyLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-static {v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->setUpdateTypesToCheck(Ljava/util/EnumSet;)V

    .line 504
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isLoadingUserPrivacyData:Z

    .line 505
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    const-string v3, "ADD_MEMBER_PRIVACY_CHECK_STATUS"

    sget-object v4, Lcom/microsoft/xbox/service/model/UpdateType;->AddMemberPrivacyLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-virtual {v2, v1, v3, v4}, Lcom/microsoft/xbox/service/model/MessageModel;->canContributeToGroupConversationAsync(Ljava/util/List;Ljava/lang/String;Lcom/microsoft/xbox/service/model/UpdateType;)V

    .line 533
    .end local v1    # "xuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->setCurrentConversation(Ljava/lang/String;)V

    .line 534
    :goto_2
    return-void

    .line 510
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-nez v2, :cond_3

    .line 511
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v3, "no item selected "

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 515
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->loadConversationDetailAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;

    if-eqz v2, :cond_4

    .line 516
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->loadConversationDetailAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->cancel()V

    .line 518
    :cond_4
    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-direct {v2, p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->loadConversationDetailAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;

    .line 519
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->loadConversationDetailAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;

    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->load(Z)V

    .line 521
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isGroupConversation:Z

    if-nez v2, :cond_6

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isServiceMessage:Z

    if-nez v2, :cond_6

    .line 522
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->loadSenderProfileTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LoadSenderProfileAsyncTask;

    if-eqz v2, :cond_5

    .line 523
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->loadSenderProfileTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LoadSenderProfileAsyncTask;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LoadSenderProfileAsyncTask;->cancel()V

    .line 525
    :cond_5
    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LoadSenderProfileAsyncTask;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LoadSenderProfileAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Lcom/microsoft/xbox/service/model/ProfileModel;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->loadSenderProfileTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LoadSenderProfileAsyncTask;

    .line 526
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->loadSenderProfileTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LoadSenderProfileAsyncTask;

    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LoadSenderProfileAsyncTask;->load(Z)V

    .line 530
    :cond_6
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->updateConsumptionHorizonAsync(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    goto :goto_1
.end method

.method public navigateToAddPeople()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 839
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->hoverChatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v1}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->hoverChatIsOpen()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 840
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->hoverChatTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->addPeopleSelected(Ljava/lang/String;)V

    .line 845
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 847
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToCommunicateVoiceAndText()Z

    move-result v3

    if-eqz v0, :cond_2

    .line 848
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getCanCommunicateWithTextAndVoice()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f0704a9

    .line 849
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f07061c

    .line 850
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 846
    invoke-static {v3, v1, v4, v5}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showFailedPermissionsDialog(ZZLjava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 851
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->navigateToFriendsPicker(Z)V

    .line 853
    :cond_0
    return-void

    .line 842
    .end local v0    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedConversationSummary()Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackAddPeople(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    goto :goto_0

    .restart local v0    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_2
    move v1, v2

    .line 848
    goto :goto_1
.end method

.method public navigateToConversationMemberProfile()V
    .locals 2

    .prologue
    .line 1254
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->hoverChatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->hoverChatIsOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1255
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->hoverChatTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->viewProfileFromConvoSelected(Ljava/lang/String;)V

    .line 1260
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    .line 1261
    return-void

    .line 1257
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedConversationSummary()Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackViewProfile(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    goto :goto_0
.end method

.method public navigateToEnforcement(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;Z)V
    .locals 13
    .param p1, "message"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "isGroupConversation"    # Z

    .prologue
    .line 541
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 543
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->hoverChatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v3}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->hoverChatIsOpen()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 544
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->hoverChatTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->messageReported(Ljava/lang/String;)V

    .line 549
    :goto_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getSenderXuid()Ljava/lang/String;

    move-result-object v10

    .line 550
    .local v10, "senderXuid":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getSenderSkypeId()Ljava/lang/String;

    move-result-object v9

    .line 551
    .local v9, "senderSkypeId":Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 552
    :cond_0
    iget-object v3, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->content:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getTopicChangedById(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 554
    if-eqz v9, :cond_1

    const-string v3, ":"

    invoke-virtual {v9, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .local v11, "skypeParts":[Ljava/lang/String;
    array-length v3, v11

    const/4 v4, 0x3

    if-lt v3, v4, :cond_1

    .line 555
    const/4 v3, 0x2

    aget-object v10, v11, v3

    .line 559
    .end local v11    # "skypeParts":[Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isSkypeGroupConversation()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getConversationSkypeId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, v9}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getGroupSenderGamertag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 561
    .local v6, "displayName":Ljava/lang/String;
    :goto_1
    iget-object v3, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->clientmessageid:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->id:Ljava/lang/String;

    .line 563
    .local v1, "clientMessageId":Ljava/lang/String;
    :goto_2
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 564
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 565
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 567
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext;

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->id:Ljava/lang/String;

    .line 570
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getConversationSkypeId()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "8:xbox:%s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    .line 571
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v12

    invoke-virtual {v12}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v7, v8

    invoke-static {v4, v5, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    if-eqz p2, :cond_5

    sget-object v5, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;->Group:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;

    :goto_3
    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;)V

    .line 574
    .local v0, "dataContext":Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext;
    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;

    sget-object v3, Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;->Message:Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v5, v10

    invoke-direct/range {v2 .. v8}, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;-><init>(Lcom/microsoft/xbox/service/model/sls/ReportUserData$ReportSource;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 575
    .local v2, "params":Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;
    const-class v3, Lcom/microsoft/xbox/xle/app/activity/EnforcementScreen;

    invoke-virtual {p0, v3, v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 580
    .end local v0    # "dataContext":Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext;
    .end local v2    # "params":Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementParameters;
    :goto_4
    return-void

    .line 546
    .end local v1    # "clientMessageId":Ljava/lang/String;
    .end local v6    # "displayName":Ljava/lang/String;
    .end local v9    # "senderSkypeId":Ljava/lang/String;
    .end local v10    # "senderXuid":Ljava/lang/String;
    :cond_2
    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackReport(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V

    goto/16 :goto_0

    .line 559
    .restart local v9    # "senderSkypeId":Ljava/lang/String;
    .restart local v10    # "senderXuid":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getSenderGamertag()Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    .line 561
    .restart local v6    # "displayName":Ljava/lang/String;
    :cond_4
    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->clientmessageid:Ljava/lang/String;

    goto :goto_2

    .line 571
    .restart local v1    # "clientMessageId":Ljava/lang/String;
    :cond_5
    sget-object v5, Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;->OneToOne:Lcom/microsoft/xbox/xle/viewmodel/EnforcementViewModel$EnforcementConversationDataContext$EnforcementConversationDataMessageType;

    goto :goto_3

    .line 577
    :cond_6
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "Failed to gather enough information to launch enforcement. senderXuid: %s, displayName: %s, clientMessageId: %s, senderSkypeId: %s"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v10, v7, v8

    const/4 v8, 0x1

    aput-object v6, v7, v8

    const/4 v8, 0x2

    aput-object v1, v7, v8

    const/4 v8, 0x3

    aput-object v9, v7, v8

    invoke-static {v4, v5, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    const v3, 0x7f070b6d

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->showError(I)V

    goto :goto_4
.end method

.method protected navigateToFriendProfile(Ljava/lang/String;)V
    .locals 0
    .param p1, "xuid"    # Ljava/lang/String;

    .prologue
    .line 537
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;)V

    .line 538
    return-void
.end method

.method public navigateToFriendsPicker(Z)V
    .locals 3
    .param p1, "returnToConversationDetails"    # Z

    .prologue
    .line 1200
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 1201
    .local v0, "parameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isGroupConversation:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putIsGroupConversation(Z)V

    .line 1202
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getSenderGamerTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putConversationGamerTag(Ljava/lang/String;)V

    .line 1204
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    if-eqz v1, :cond_0

    .line 1205
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/MessageModel;->getConversationMembers(Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putViewAllMembers(Ljava/util/Collection;)V

    .line 1208
    :cond_0
    if-eqz p1, :cond_1

    .line 1209
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putReturnToConversationDetailsScreen(Z)V

    .line 1212
    :cond_1
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/FriendsSelectorScreen;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 1213
    return-void
.end method

.method public navigateToRenameDialog()V
    .locals 2

    .prologue
    .line 1244
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->hoverChatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->hoverChatIsOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1245
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->hoverChatTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->renameGroupConvoSelected(Ljava/lang/String;)V

    .line 1250
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showRenameConversationDialog(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)V

    .line 1251
    return-void

    .line 1247
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedConversationSummary()Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackRenameConversation(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    goto :goto_0
.end method

.method public navigateToViewMemberScreen()V
    .locals 3

    .prologue
    .line 1216
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->hoverChatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v1}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->hoverChatIsOpen()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1217
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->hoverChatTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->viewPeopleSelected(Ljava/lang/String;)V

    .line 1222
    :goto_0
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 1223
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putViewMemberConversationId(Ljava/lang/String;)V

    .line 1224
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/ConversationsViewAllMembersScreen;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 1225
    return-void

    .line 1219
    .end local v0    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedConversationSummary()Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackViewPeople(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 1282
    const/16 v0, 0x4d2

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/PermissionUtil;->canDrawOverlayViews(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1283
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->openAsHoverChatInternal()V

    .line 1287
    :goto_0
    return-void

    .line 1285
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onBackButtonPressed()V
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->onBackPressed()V

    .line 155
    return-void
.end method

.method public onCopyMessageText(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V
    .locals 2
    .param p1, "conversationMessage"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .prologue
    .line 1290
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->hoverChatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->hoverChatIsOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1291
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->hoverChatTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->messageTextCopied(Ljava/lang/String;)V

    .line 1295
    :goto_0
    return-void

    .line 1293
    :cond_0
    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackCopyText(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)V

    goto :goto_0
.end method

.method protected onLoadSkypeConversationMessagesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V
    .locals 1
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "targetXuid"    # Ljava/lang/String;

    .prologue
    .line 716
    invoke-super {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->onLoadSkypeConversationMessagesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V

    .line 717
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->forceRefresh:Z

    .line 718
    return-void
.end method

.method public onLoadUserProfileCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/ProfileModel;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "senderProfileModel"    # Lcom/microsoft/xbox/service/model/ProfileModel;

    .prologue
    .line 641
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "onLoadUserProfile Completed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isLoadingSenderProfile:Z

    .line 644
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 666
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->updateAdapter()V

    .line 667
    return-void

    .line 648
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isGroupConversation:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isServiceMessage:Z

    if-nez v0, :cond_0

    .line 649
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->gamerPicUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 650
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerPicImageUrl()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->gamerPicUrl:Ljava/lang/String;

    .line 653
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderGamerTag:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 654
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerTag()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderGamerTag:Ljava/lang/String;

    .line 655
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getRealName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->realName:Ljava/lang/String;

    goto :goto_0

    .line 662
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 663
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Error in getting sender profile model, won\'t be able to display sender image"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 644
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 159
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 160
    return-void
.end method

.method public onScreenTouchEvent()V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/SkypeConversationDetailsActivityAdapter;->onScreenTouchEvent()V

    .line 150
    return-void
.end method

.method protected onStartOverride()V
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/MessageModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 196
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/MessageModel;->setCurrentConversation(Ljava/lang/String;)V

    .line 200
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedRecipients()Lcom/microsoft/xbox/toolkit/MultiSelection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/MultiSelection;->toArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->addMemberList:Ljava/util/List;

    .line 201
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedRecipients(Lcom/microsoft/xbox/toolkit/MultiSelection;)V

    .line 203
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MessageModel;->startTypingIndicatorManager()V

    .line 205
    sget-boolean v0, Lcom/microsoft/xbox/toolkit/Build;->EnableSkypeLongPoll:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isServiceMessage:Z

    if-nez v0, :cond_1

    .line 206
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->subscribeSkypeLongPoll()V

    .line 208
    :cond_1
    return-void
.end method

.method protected onStopOverride()V
    .locals 3

    .prologue
    .line 217
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/MessageModel;->clearCurrentConversation()V

    .line 218
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/service/model/MessageModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 220
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->loadConversationDetailAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;

    if-eqz v1, :cond_0

    .line 221
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->loadConversationDetailAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;->cancel()V

    .line 224
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->loadSenderProfileTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LoadSenderProfileAsyncTask;

    if-eqz v1, :cond_1

    .line 225
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->loadSenderProfileTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LoadSenderProfileAsyncTask;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$LoadSenderProfileAsyncTask;->cancel()V

    .line 228
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->muteConversationAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$MuteConversationAsyncTask;

    if-eqz v1, :cond_2

    .line 229
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->muteConversationAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$MuteConversationAsyncTask;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$MuteConversationAsyncTask;->cancel()V

    .line 232
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->updateTopicNameAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;

    if-eqz v1, :cond_3

    .line 233
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->updateTopicNameAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;->cancel()V

    .line 236
    :cond_3
    sget-boolean v1, Lcom/microsoft/xbox/toolkit/Build;->EnableSkypeLongPoll:Z

    if-eqz v1, :cond_5

    .line 237
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->subscribeSkypeLongPollAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SubscribeSkypeLongPollAsyncTask;

    if-eqz v1, :cond_4

    .line 238
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->subscribeSkypeLongPollAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SubscribeSkypeLongPollAsyncTask;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SubscribeSkypeLongPollAsyncTask;->cancel()V

    .line 241
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->skypeLongPollAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SkypeLongPollAsyncTask;

    if-eqz v1, :cond_5

    .line 242
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->skypeLongPollAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SkypeLongPollAsyncTask;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$SkypeLongPollAsyncTask;->cancel()V

    .line 246
    :cond_5
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->mapAddMemberToConversationTask:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->Values()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;

    .line 247
    .local v0, "task":Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;
    if-eqz v0, :cond_6

    .line 248
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;->cancel()V

    goto :goto_0

    .line 252
    .end local v0    # "task":Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;
    :cond_7
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->mapAddMemberToConversationTask:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->clear()V

    .line 254
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->addMemberList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 256
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->cancelLoadAttachments()V

    .line 258
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/MessageModel;->stopTypingIndicatorManager()V

    .line 259
    return-void
.end method

.method protected onUpdateFinished()V
    .locals 4

    .prologue
    .line 476
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MessageDelete:Lcom/microsoft/xbox/service/model/UpdateType;

    const-wide/16 v2, 0xbed

    invoke-virtual {p0, v0, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->checkErrorCode(Lcom/microsoft/xbox/service/model/UpdateType;J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 477
    const v0, 0x7f070759

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->showError(I)V

    .line 480
    :cond_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->onUpdateFinished()V

    .line 481
    return-void
.end method

.method public openAsHoverChat()V
    .locals 2

    .prologue
    .line 1264
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/PermissionUtil;->canDrawOverlayViews(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1265
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const/16 v1, 0x4d2

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/PermissionUtil;->requestDrawOverlayViewPermission(Landroid/app/Activity;I)V

    .line 1269
    :goto_0
    return-void

    .line 1267
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->openAsHoverChatInternal()V

    goto :goto_0
.end method

.method public sendSkypeIsTypingMessage()V
    .locals 5

    .prologue
    .line 821
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToCommunicateVoiceAndText()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 822
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    if-eqz v2, :cond_1

    .line 823
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageType:Ljava/lang/String;

    sget-object v3, Lcom/microsoft/xbox/service/model/serialization/XBLSLSMessageType;->User:Lcom/microsoft/xbox/service/model/serialization/XBLSLSMessageType;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/serialization/XBLSLSMessageType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageType:Ljava/lang/String;

    sget-object v3, Lcom/microsoft/xbox/service/model/serialization/XBLSLSMessageType;->ActivityFeedSharedItem:Lcom/microsoft/xbox/service/model/serialization/XBLSLSMessageType;

    .line 824
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/serialization/XBLSLSMessageType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 826
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 827
    .local v0, "model":Lcom/microsoft/xbox/service/model/ProfileModel;
    new-instance v1, Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;

    sget-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->IsTyping:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->getType()Ljava/lang/String;

    move-result-object v3

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerTag()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-direct {v1, v3, v2}, Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 828
    .local v1, "request":Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isGroupConversation:Z

    invoke-virtual {v2, v3, v1, v4}, Lcom/microsoft/xbox/service/model/MessageModel;->sendSkypeIsTypingMessage(Ljava/lang/String;Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;Z)V

    .line 836
    .end local v0    # "model":Lcom/microsoft/xbox/service/model/ProfileModel;
    .end local v1    # "request":Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;
    :cond_1
    :goto_1
    return-void

    .line 827
    .restart local v0    # "model":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_2
    const-string v2, ""

    goto :goto_0

    .line 830
    .end local v0    # "model":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_3
    const-string v2, "ConversationDetailsActivityViewModel"

    const-string v3, "Cannot send typing to system or service messages"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 834
    :cond_4
    const-string v2, "ConversationDetailsActivityViewModel"

    const-string v3, "The user has not privilege to send typing messages"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public setMuteConversation()V
    .locals 3

    .prologue
    .line 741
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->hoverChatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->hoverChatIsOpen()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 742
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->alerts:Z

    if-eqz v0, :cond_1

    .line 743
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->hoverChatTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->muteConversationSelected(Ljava/lang/String;)V

    .line 752
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->muteConversationAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$MuteConversationAsyncTask;

    if-eqz v0, :cond_0

    .line 753
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->muteConversationAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$MuteConversationAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$MuteConversationAsyncTask;->cancel()V

    .line 755
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$MuteConversationAsyncTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-boolean v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->alerts:Z

    invoke-direct {v0, p0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$MuteConversationAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->muteConversationAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$MuteConversationAsyncTask;

    .line 756
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->muteConversationAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$MuteConversationAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$MuteConversationAsyncTask;->load(Z)V

    .line 757
    return-void

    .line 745
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->hoverChatTelemetryService:Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatTelemetryService;->unmuteConversationSelected(Ljava/lang/String;)V

    goto :goto_0

    .line 749
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedConversationSummary()Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackMuteState(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)V

    goto :goto_0
.end method

.method public shouldForceUpdateAdapter()Z
    .locals 1

    .prologue
    .line 1182
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->shouldforceUpdateAdapter:Z

    return v0
.end method

.method public shouldShowHoverChatOption()Z
    .locals 1

    .prologue
    .line 1277
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/PermissionUtil;->canDrawOverlayViews(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/PermissionUtil;->canRequestDrawOverlayViews()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateGroupTopicName(Ljava/lang/String;)V
    .locals 7
    .param p1, "newTopic"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 764
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 766
    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isGroupConversation:Z

    if-eqz v4, :cond_3

    .line 767
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->newTopicName:Ljava/lang/String;

    .line 768
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/model/MessageModel;->getConversationMembers(Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v0

    .line 770
    .local v0, "groupMembers":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 772
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 773
    .local v3, "xuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    .line 774
    .local v1, "member":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getXuidFromSkypeMessageId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 775
    .local v2, "xuid":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 776
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 780
    .end local v1    # "member":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    .end local v2    # "xuid":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isLoadingUserPrivacyData:Z

    .line 781
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    const-string v5, "TOPIC_CHANGE_PRIVACY_CHECK_STATUS"

    sget-object v6, Lcom/microsoft/xbox/service/model/UpdateType;->TopicChangePrivacyLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-virtual {v4, v3, v5, v6}, Lcom/microsoft/xbox/service/model/MessageModel;->canContributeToGroupConversationAsync(Ljava/util/List;Ljava/lang/String;Lcom/microsoft/xbox/service/model/UpdateType;)V

    .line 786
    .end local v0    # "groupMembers":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    .end local v3    # "xuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    :goto_1
    return-void

    .line 784
    :cond_3
    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v5, "Not group conversation- can only rename group topic"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    const/4 v11, 0x0

    const/4 v13, 0x1

    .line 298
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 299
    sget-object v10, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v9

    aget v9, v10, v9

    packed-switch v9, :pswitch_data_0

    .line 415
    :cond_0
    :goto_0
    :pswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->updateAdapter()V

    .line 416
    :goto_1
    return-void

    .line 301
    :pswitch_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v9

    if-nez v9, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->getSkypeMessageList()Ljava/util/List;

    move-result-object v9

    if-nez v9, :cond_1

    .line 303
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->goBack()V

    goto :goto_1

    .line 306
    :cond_1
    iput-boolean v13, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->shouldforceUpdateAdapter:Z

    goto :goto_0

    .line 310
    :pswitch_2
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v9

    if-nez v9, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 312
    iput-boolean v13, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->shouldforceUpdateAdapter:Z

    goto :goto_0

    .line 319
    :pswitch_3
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v9

    if-eqz v9, :cond_5

    .line 320
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/XLEException;->getErrorCode()J

    move-result-wide v2

    .line 321
    .local v2, "errorCode":J
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/XLEException;->getCause()Ljava/lang/Throwable;

    move-result-object v9

    if-eqz v9, :cond_2

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/XLEException;->getCause()Ljava/lang/Throwable;

    move-result-object v9

    instance-of v9, v9, Lcom/microsoft/xbox/toolkit/XLEException;

    if-eqz v9, :cond_2

    .line 322
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/XLEException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/XLEException;

    .line 323
    .local v1, "cause":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/XLEException;->getErrorCode()J

    move-result-wide v2

    .line 326
    .end local v1    # "cause":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_2
    sget-object v9, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "errorCode - "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    const-wide/16 v10, 0x26e8

    cmp-long v9, v2, v10

    if-nez v9, :cond_3

    .line 329
    sget-object v9, Lcom/microsoft/xbox/service/model/UpdateType;->MessageSend:Lcom/microsoft/xbox/service/model/UpdateType;

    sget-object v10, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->NotAllowed:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    invoke-direct {p0, v9, v10}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->showErrorDialog(Lcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;)V

    goto/16 :goto_0

    .line 330
    :cond_3
    const-wide/16 v10, 0x26e9

    cmp-long v9, v2, v10

    if-nez v9, :cond_4

    .line 331
    invoke-static {}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->showFixMessagingDialog()V

    goto/16 :goto_0

    .line 333
    :cond_4
    sget-object v9, Lcom/microsoft/xbox/service/model/UpdateType;->MessageSend:Lcom/microsoft/xbox/service/model/UpdateType;

    sget-object v10, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->Error:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    invoke-direct {p0, v9, v10}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->showErrorDialog(Lcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;)V

    goto/16 :goto_0

    .line 336
    .end local v2    # "errorCode":J
    :cond_5
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->setShouldScrollToBottom()V

    .line 337
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->load()V

    .line 338
    const-string v9, ""

    invoke-virtual {p0, v9}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->setMessageBody(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 342
    :pswitch_4
    iput-boolean v11, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isLoadingUserPrivacyData:Z

    .line 343
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v9

    if-nez v9, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 344
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/UpdateData;->getExtra()Landroid/os/Bundle;

    move-result-object v0

    .line 345
    .local v0, "b":Landroid/os/Bundle;
    if-eqz v0, :cond_9

    .line 347
    const-string v9, "ADD_MEMBER_PRIVACY_CHECK_STATUS"

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 348
    .local v7, "permissionSettingString":Ljava/lang/String;
    if-nez v7, :cond_8

    .line 349
    iput-boolean v11, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->errorAddingAtleastOneMemberToConversation:Z

    .line 351
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->mapAddMemberToConversationTask:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->Values()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;

    .line 352
    .local v4, "loadingTask":Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;->cancel()V

    goto :goto_2

    .line 355
    .end local v4    # "loadingTask":Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;
    :cond_6
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->mapAddMemberToConversationTask:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->clear()V

    .line 356
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->mapIsAddingMemberToConversation:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->clear()V

    .line 358
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->addMemberList:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;

    .line 359
    .local v5, "member":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    new-instance v4, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;

    invoke-direct {v4, p0, v5}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;)V

    .line 360
    .restart local v4    # "loadingTask":Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->mapAddMemberToConversationTask:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    iget-object v11, v5, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->xuid:Ljava/lang/String;

    invoke-virtual {v10, v11, v4}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 361
    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->mapIsAddingMemberToConversation:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    iget-object v11, v5, Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;->xuid:Ljava/lang/String;

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_3

    .line 364
    .end local v4    # "loadingTask":Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;
    .end local v5    # "member":Lcom/microsoft/xbox/xle/viewmodel/FriendSelectorItem;
    :cond_7
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->mapAddMemberToConversationTask:Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/ThreadSafeHashtable;->Values()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;

    .line 365
    .local v8, "task":Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;
    iget-boolean v10, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->forceRefresh:Z

    invoke-virtual {v8, v10}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;->load(Z)V

    goto :goto_4

    .line 368
    .end local v8    # "task":Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$AddMemberToConversationAsyncTask;
    :cond_8
    invoke-static {v7}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    move-result-object v6

    .line 369
    .local v6, "permissionSetting":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;
    sget-object v9, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v10, "None of the members were added since privacy check failed for one or more members"

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    sget-object v9, Lcom/microsoft/xbox/service/model/UpdateType;->AddMemberPrivacyLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {p0, v9, v6}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->showErrorDialog(Lcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;)V

    goto/16 :goto_0

    .line 373
    .end local v6    # "permissionSetting":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;
    .end local v7    # "permissionSettingString":Ljava/lang/String;
    :cond_9
    sget-object v9, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v10, "None of the members were added since privacy check failed - invalid bundle"

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    sget-object v9, Lcom/microsoft/xbox/service/model/UpdateType;->AddMemberPrivacyLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    sget-object v10, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->Error:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    invoke-direct {p0, v9, v10}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->showErrorDialog(Lcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;)V

    goto/16 :goto_0

    .line 380
    .end local v0    # "b":Landroid/os/Bundle;
    :pswitch_5
    iput-boolean v11, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->isLoadingUserPrivacyData:Z

    .line 381
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v9

    if-nez v9, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 382
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v9}, Lcom/microsoft/xbox/service/model/UpdateData;->getExtra()Landroid/os/Bundle;

    move-result-object v0

    .line 383
    .restart local v0    # "b":Landroid/os/Bundle;
    if-eqz v0, :cond_d

    .line 385
    const-string v9, "TOPIC_CHANGE_PRIVACY_CHECK_STATUS"

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 387
    .restart local v7    # "permissionSettingString":Ljava/lang/String;
    if-nez v7, :cond_b

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToCommunicateVoiceAndText()Z

    move-result v9

    if-eqz v9, :cond_b

    .line 388
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->updateTopicNameAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;

    if-eqz v9, :cond_a

    .line 389
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->updateTopicNameAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;

    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;->cancel()V

    .line 391
    :cond_a
    new-instance v9, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;

    iget-object v10, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v10, v10, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    iget-object v11, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->newTopicName:Ljava/lang/String;

    invoke-direct {v9, p0, v10, v11}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->updateTopicNameAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;

    .line 392
    iget-object v9, p0, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->updateTopicNameAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;

    invoke-virtual {v9, v13}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel$UpdateTopicNameAsyncTask;->load(Z)V

    goto/16 :goto_0

    .line 393
    :cond_b
    if-nez v7, :cond_c

    .line 395
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v9

    sget-object v10, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v11, 0x7f07061d

    .line 396
    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    sget-object v11, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v12, 0x7f070110

    .line 397
    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    sget-object v12, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v13, 0x7f0707c7

    .line 398
    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    .line 395
    invoke-interface {v9, v10, v11, v12, v13}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 401
    :cond_c
    invoke-static {v7}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    move-result-object v6

    .line 402
    .restart local v6    # "permissionSetting":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;
    sget-object v9, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v10, "Topic change failed since privacy check failed for one or more members"

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    sget-object v9, Lcom/microsoft/xbox/service/model/UpdateType;->TopicChangePrivacyLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-direct {p0, v9, v6}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->showErrorDialog(Lcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;)V

    goto/16 :goto_0

    .line 406
    .end local v6    # "permissionSetting":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;
    .end local v7    # "permissionSettingString":Ljava/lang/String;
    :cond_d
    sget-object v9, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->TAG:Ljava/lang/String;

    const-string v10, "Topic change failed since privacy check failed - invalid bundle"

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    sget-object v9, Lcom/microsoft/xbox/service/model/UpdateType;->TopicChangePrivacyLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    sget-object v10, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;->Error:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    invoke-direct {p0, v9, v10}, Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;->showErrorDialog(Lcom/microsoft/xbox/service/model/UpdateType;Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;)V

    goto/16 :goto_0

    .line 299
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
