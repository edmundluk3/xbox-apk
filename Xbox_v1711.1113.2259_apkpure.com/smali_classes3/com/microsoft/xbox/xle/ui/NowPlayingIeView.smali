.class public Lcom/microsoft/xbox/xle/ui/NowPlayingIeView;
.super Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;
.source "NowPlayingIeView.java"


# instance fields
.field private ieControl:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "container"    # Landroid/view/ViewGroup;

    .prologue
    .line 13
    const v0, 0x7f030194

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;-><init>(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;Landroid/content/Context;Landroid/view/ViewGroup;I)V

    .line 14
    return-void
.end method


# virtual methods
.method public attachToContainer()V
    .locals 3

    .prologue
    .line 18
    invoke-super {p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->attachToContainer()V

    .line 19
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingIeView;->inflatedView:Landroid/view/View;

    const v1, 0x7f0e07f6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingIeView;->ieControl:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    .line 20
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingIeView;->ieControl:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingIeView;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->initialize(Lcom/microsoft/xbox/xle/viewmodel/NowPlayingBaseViewModel;Z)V

    .line 21
    return-void
.end method

.method public cleanup()V
    .locals 1

    .prologue
    .line 34
    invoke-super {p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->cleanup()V

    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingIeView;->ieControl:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->cleanup()V

    .line 36
    return-void
.end method

.method public update()V
    .locals 1

    .prologue
    .line 28
    invoke-super {p0}, Lcom/microsoft/xbox/xle/ui/NowPlayingTitleView;->update()V

    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/NowPlayingIeView;->ieControl:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->update()V

    .line 30
    return-void
.end method
