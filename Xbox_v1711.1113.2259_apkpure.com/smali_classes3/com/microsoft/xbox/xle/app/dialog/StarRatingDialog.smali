.class public Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "StarRatingDialog.java"


# instance fields
.field private closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private contentTitleTextView:Landroid/widget/TextView;

.field private fiveStarButton:Landroid/widget/LinearLayout;

.field private fourStarButton:Landroid/widget/LinearLayout;

.field private oneStarButton:Landroid/widget/LinearLayout;

.field private softCloseButton:Landroid/view/View;

.field private threeStarButton:Landroid/widget/LinearLayout;

.field private twoStarButton:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentTitle"    # Ljava/lang/String;

    .prologue
    .line 26
    const v0, 0x7f080238

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 27
    const v0, 0x7f03020c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->setContentView(I)V

    .line 28
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->setCanceledOnTouchOutside(Z)V

    .line 30
    const v0, 0x7f0e0a51

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 31
    const v0, 0x7f0e0a52

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->softCloseButton:Landroid/view/View;

    .line 32
    const v0, 0x7f0e0a54

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->oneStarButton:Landroid/widget/LinearLayout;

    .line 33
    const v0, 0x7f0e0a55

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->twoStarButton:Landroid/widget/LinearLayout;

    .line 34
    const v0, 0x7f0e0a56

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->threeStarButton:Landroid/widget/LinearLayout;

    .line 35
    const v0, 0x7f0e0a57

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->fourStarButton:Landroid/widget/LinearLayout;

    .line 36
    const v0, 0x7f0e0a58

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->fiveStarButton:Landroid/widget/LinearLayout;

    .line 37
    const v0, 0x7f0e0a53

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->contentTitleTextView:Landroid/widget/TextView;

    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->contentTitleTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->contentTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->dismissSelf()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;I)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;
    .param p1, "x1"    # I

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->submitRating(I)V

    return-void
.end method

.method private clearRatingSelection()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->oneStarButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->twoStarButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->threeStarButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->fourStarButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->fiveStarButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 87
    return-void
.end method

.method private dismissSelf()V
    .locals 1

    .prologue
    .line 49
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissStarRatingDialog()V

    .line 50
    return-void
.end method

.method private submitRating(I)V
    .locals 0
    .param p1, "rating"    # I

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->dismissSelf()V

    .line 56
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->dismissSelf()V

    .line 46
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog$1;-><init>(Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->softCloseButton:Landroid/view/View;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog$2;-><init>(Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->oneStarButton:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog$3;-><init>(Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->twoStarButton:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog$4;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog$4;-><init>(Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->threeStarButton:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog$5;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog$5;-><init>(Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->fourStarButton:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog$6;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog$6;-><init>(Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->fiveStarButton:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog$7;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog$7;-><init>(Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->softCloseButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->oneStarButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->twoStarButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->threeStarButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->fourStarButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->fiveStarButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    return-void
.end method

.method public setSelectedRating(I)V
    .locals 2
    .param p1, "rating"    # I

    .prologue
    const/4 v1, 0x1

    .line 60
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->clearRatingSelection()V

    .line 62
    packed-switch p1, :pswitch_data_0

    .line 79
    :goto_0
    return-void

    .line 64
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->oneStarButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setSelected(Z)V

    goto :goto_0

    .line 67
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->twoStarButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setSelected(Z)V

    goto :goto_0

    .line 70
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->threeStarButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setSelected(Z)V

    goto :goto_0

    .line 73
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->fourStarButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setSelected(Z)V

    goto :goto_0

    .line 76
    :pswitch_4
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->fiveStarButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setSelected(Z)V

    goto :goto_0

    .line 62
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
