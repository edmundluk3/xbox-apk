.class public Lcom/microsoft/xbox/service/model/serialization/Achievement;
.super Ljava/lang/Object;
.source "Achievement.java"


# annotations
.annotation runtime Lorg/simpleframework/xml/Root;
.end annotation


# instance fields
.field public Description:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
    .end annotation
.end field

.field public DisplayBeforeEarned:Z
    .annotation runtime Lorg/simpleframework/xml/Element;
    .end annotation
.end field

.field public EarnedDateTime:Ljava/util/Date;
    .annotation runtime Lorg/simpleframework/xml/Element;
        required = false
    .end annotation

    .annotation runtime Lorg/simpleframework/xml/convert/Convert;
        value = Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson;
    .end annotation
.end field

.field public EarnedOnline:Z
    .annotation runtime Lorg/simpleframework/xml/Element;
    .end annotation
.end field

.field public GameId:J
    .annotation runtime Lorg/simpleframework/xml/Element;
    .end annotation
.end field

.field public Gamerscore:I
    .annotation runtime Lorg/simpleframework/xml/Element;
    .end annotation
.end field

.field public HowToEarn:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        required = false
    .end annotation
.end field

.field public IsEarned:Z
    .annotation runtime Lorg/simpleframework/xml/Element;
    .end annotation
.end field

.field public Key:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
    .end annotation
.end field

.field public Name:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
    .end annotation
.end field

.field private PictureUrl:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
