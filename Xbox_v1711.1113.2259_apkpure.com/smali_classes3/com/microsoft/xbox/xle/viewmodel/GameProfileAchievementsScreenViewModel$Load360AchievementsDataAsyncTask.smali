.class Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameProfileAchievementsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Load360AchievementsDataAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private filter:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

.field private final model:Lcom/microsoft/xbox/service/model/TitleModel;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;)V
    .locals 2
    .param p2, "filter"    # Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    .prologue
    .line 1001
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 1002
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->filter:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    .line 1003
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 1004
    invoke-static {p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    .line 1005
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 2

    .prologue
    .line 1009
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 1011
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$AchievementsFilter:[I

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->filter:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1018
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1013
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleModel;->shouldRefresh360GameProgressAchievements()Z

    move-result v0

    goto :goto_0

    .line 1015
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleModel;->shouldRefresh360GameProgressEarnedAchievements()Z

    move-result v0

    goto :goto_0

    .line 1011
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 1053
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$AchievementsFilter:[I

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->filter:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1058
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameProgress360EarnedAchievements(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1055
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->loadGameProgress360Achievements(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    goto :goto_0

    .line 1053
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 996
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 1048
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 996
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 3

    .prologue
    .line 1023
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 1024
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->filter:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$1200(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;)V

    .line 1025
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 1043
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->filter:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$1200(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;)V

    .line 1044
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 996
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1029
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 1030
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-static {v0, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$802(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Z)Z

    .line 1031
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$AchievementsFilter:[I

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->filter:Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/AchievementsFilter;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1039
    :goto_0
    return-void

    .line 1033
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-static {v0, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$1302(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Z)Z

    goto :goto_0

    .line 1036
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$Load360AchievementsDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-static {v0, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$1402(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Z)Z

    goto :goto_0

    .line 1031
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
