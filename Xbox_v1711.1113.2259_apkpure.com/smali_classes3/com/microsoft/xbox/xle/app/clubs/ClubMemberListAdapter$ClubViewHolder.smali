.class Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter$ClubViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "ClubMemberListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ClubViewHolder"
.end annotation


# instance fields
.field final clubImageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

.field final clubNameTextView:Landroid/widget/TextView;

.field final clubOwnerTextView:Landroid/widget/TextView;

.field final defaultBackgroundColor:I

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter;Landroid/view/ViewGroup;)V
    .locals 3
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter;
    .param p2, "itemView"    # Landroid/view/ViewGroup;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter$ClubViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter;

    .line 50
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03008d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter$ClubViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e03a4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter$ClubViewHolder;->clubNameTextView:Landroid/widget/TextView;

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter$ClubViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e03a3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter$ClubViewHolder;->clubImageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter$ClubViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e03a5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter$ClubViewHolder;->clubOwnerTextView:Landroid/widget/TextView;

    .line 54
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter$ClubViewHolder;->defaultBackgroundColor:I

    .line 55
    return-void
.end method

.method static synthetic lambda$bindTo$0(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;)I
    .locals 2
    .param p0, "lhs"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;
    .param p1, "rhs"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;->role()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;->role()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    return v0
.end method

.method static synthetic lambda$bindTo$1(Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter$ClubViewHolder;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter$ClubViewHolder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter$ClubViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public bindTo(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V
    .locals 6
    .param p1, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v5, 0x7f0201fa

    .line 58
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 60
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter$ClubViewHolder;->clubNameTextView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->targetRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 63
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->targetRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 65
    .local v2, "targetRoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;>;"
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 66
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter$ClubViewHolder$$Lambda$1;->lambdaFactory$()Ljava/util/Comparator;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 67
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter$ClubViewHolder;->clubOwnerTextView:Landroid/widget/TextView;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;->role()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->getLocalizedName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    .end local v2    # "targetRoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;>;"
    :goto_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter$ClubViewHolder;->clubImageView:Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->displayImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3, v5, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEImageViewFast;->setImageURI2(Ljava/lang/String;II)V

    .line 77
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->preferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v1

    .line 78
    .local v1, "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getSecondaryColor()I

    move-result v0

    .line 79
    .local v0, "color":I
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter$ClubViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 81
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter$ClubViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 82
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter$ClubViewHolder;->itemView:Landroid/view/View;

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter$ClubViewHolder$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter$ClubViewHolder;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    :cond_0
    return-void

    .line 69
    .end local v0    # "color":I
    .end local v1    # "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    .restart local v2    # "targetRoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;>;"
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter$ClubViewHolder;->clubOwnerTextView:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 72
    .end local v2    # "targetRoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;>;"
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter$ClubViewHolder;->clubOwnerTextView:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 78
    .restart local v1    # "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    :cond_3
    iget v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter$ClubViewHolder;->defaultBackgroundColor:I

    goto :goto_1
.end method
