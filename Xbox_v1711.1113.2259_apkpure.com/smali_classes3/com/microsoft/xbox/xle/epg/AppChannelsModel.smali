.class public Lcom/microsoft/xbox/xle/epg/AppChannelsModel;
.super Ljava/lang/Object;
.source "AppChannelsModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;
.implements Lcom/microsoft/xbox/xle/urc/net/BranchSession$IAppChannelsListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;,
        Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;,
        Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;,
        Lcom/microsoft/xbox/xle/epg/AppChannelsModel$IListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AppChannelsModel"

.field private static _default:Lcom/microsoft/xbox/xle/epg/AppChannelsModel;


# instance fields
.field private mChannels:[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

.field private mConnected:Z

.field private mError:Z

.field private mFavChannels:[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

.field private mGetChannelsTask:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;

.field private mInProgressChannels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mInProgressFavChannels:[Lcom/microsoft/xbox/service/model/epg/EPListItem;

.field private mIsStarted:Z

.field private mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/epg/AppChannelsModel$IListener;",
            ">;"
        }
    .end annotation
.end field

.field private mLoading:Z

.field private final moreAppChannelsName:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mChannels:[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    .line 45
    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mFavChannels:[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    .line 46
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mLoading:Z

    .line 47
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mError:Z

    .line 48
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mConnected:Z

    .line 49
    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mGetChannelsTask:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mListeners:Ljava/util/ArrayList;

    .line 53
    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mInProgressFavChannels:[Lcom/microsoft/xbox/service/model/epg/EPListItem;

    .line 54
    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mInProgressChannels:Ljava/util/List;

    .line 56
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mIsStarted:Z

    .line 58
    const-string v0, "OneGuide"

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->moreAppChannelsName:Ljava/lang/String;

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/epg/AppChannelsModel;ZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/AppChannelsModel;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->onRequestError(ZZ)V

    return-void
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/epg/AppChannelsModel;[Lcom/microsoft/xbox/service/model/epg/EPListItem;)[Lcom/microsoft/xbox/service/model/epg/EPListItem;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/AppChannelsModel;
    .param p1, "x1"    # [Lcom/microsoft/xbox/service/model/epg/EPListItem;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mInProgressFavChannels:[Lcom/microsoft/xbox/service/model/epg/EPListItem;

    return-object p1
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/epg/AppChannelsModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/AppChannelsModel;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->onCheckChannelsDone()V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    .prologue
    .line 40
    invoke-static {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->isBranchConnectedState(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;)Z

    move-result v0

    return v0
.end method

.method public static getDefault()Lcom/microsoft/xbox/xle/epg/AppChannelsModel;
    .locals 1

    .prologue
    .line 163
    sget-object v0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->_default:Lcom/microsoft/xbox/xle/epg/AppChannelsModel;

    if-nez v0, :cond_0

    .line 164
    new-instance v0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->_default:Lcom/microsoft/xbox/xle/epg/AppChannelsModel;

    .line 166
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->_default:Lcom/microsoft/xbox/xle/epg/AppChannelsModel;

    return-object v0
.end method

.method private static isBranchConnectedState(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;)Z
    .locals 1
    .param p0, "connectionState"    # Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    .prologue
    .line 150
    sget-object v0, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->DISCONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->ERROR:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-ne p0, v0, :cond_1

    .line 151
    :cond_0
    const/4 v0, 0x0

    .line 153
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private notifyListenersData()V
    .locals 3

    .prologue
    .line 366
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$IListener;

    .line 368
    .local v0, "l":Lcom/microsoft/xbox/xle/epg/AppChannelsModel$IListener;
    invoke-interface {v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$IListener;->onDataChanged()V

    goto :goto_0

    .line 370
    .end local v0    # "l":Lcom/microsoft/xbox/xle/epg/AppChannelsModel$IListener;
    :cond_0
    return-void
.end method

.method private notifyListenersState()V
    .locals 3

    .prologue
    .line 360
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$IListener;

    .line 361
    .local v0, "l":Lcom/microsoft/xbox/xle/epg/AppChannelsModel$IListener;
    invoke-interface {v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$IListener;->onStateChanged()V

    goto :goto_0

    .line 363
    .end local v0    # "l":Lcom/microsoft/xbox/xle/epg/AppChannelsModel$IListener;
    :cond_0
    return-void
.end method

.method private onCheckChannelsDone()V
    .locals 14

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x0

    .line 190
    iget-object v9, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mInProgressFavChannels:[Lcom/microsoft/xbox/service/model/epg/EPListItem;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mInProgressChannels:Ljava/util/List;

    if-nez v9, :cond_1

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 194
    :cond_1
    const-string v9, "AppChannelsModel"

    const-string v10, "Building channel list"

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    iput-object v12, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mChannels:[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    .line 197
    iput-object v12, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mFavChannels:[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    .line 199
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 200
    .local v4, "channels":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 202
    .local v5, "favChannels":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;>;"
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 203
    .local v2, "chanToProv":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/HashSet<Ljava/lang/String;>;>;"
    iget-object v12, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mInProgressFavChannels:[Lcom/microsoft/xbox/service/model/epg/EPListItem;

    array-length v13, v12

    move v10, v11

    :goto_1
    if-ge v10, v13, :cond_4

    aget-object v6, v12, v10

    .line 204
    .local v6, "li":Lcom/microsoft/xbox/service/model/epg/EPListItem;
    if-eqz v6, :cond_3

    iget-object v9, v6, Lcom/microsoft/xbox/service/model/epg/EPListItem;->Item:Lcom/microsoft/xbox/service/model/epg/EPItem;

    if-eqz v9, :cond_3

    .line 205
    iget-object v9, v6, Lcom/microsoft/xbox/service/model/epg/EPListItem;->Item:Lcom/microsoft/xbox/service/model/epg/EPItem;

    iget-object v7, v9, Lcom/microsoft/xbox/service/model/epg/EPItem;->Provider:Ljava/lang/String;

    .line 206
    .local v7, "provId":Ljava/lang/String;
    iget-object v9, v6, Lcom/microsoft/xbox/service/model/epg/EPListItem;->Item:Lcom/microsoft/xbox/service/model/epg/EPItem;

    iget-object v1, v9, Lcom/microsoft/xbox/service/model/epg/EPItem;->ProviderId:Ljava/lang/String;

    .line 208
    .local v1, "chanId":Ljava/lang/String;
    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    if-nez v9, :cond_2

    .line 209
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v2, v1, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    :cond_2
    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/HashSet;

    invoke-virtual {v9, v7}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 203
    .end local v1    # "chanId":Ljava/lang/String;
    .end local v7    # "provId":Ljava/lang/String;
    :cond_3
    add-int/lit8 v9, v10, 0x1

    move v10, v9

    goto :goto_1

    .line 215
    .end local v6    # "li":Lcom/microsoft/xbox/service/model/epg/EPListItem;
    :cond_4
    iget-object v9, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mInProgressChannels:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_5
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;

    .line 216
    .local v0, "c":Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;
    new-instance v3, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    invoke-direct {v3, v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;-><init>(Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;)V

    .line 217
    .local v3, "channel":Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 219
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getChannelId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/HashSet;

    .line 220
    .local v8, "providers":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    if-eqz v8, :cond_5

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->getProviderId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 221
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    const/4 v10, 0x1

    invoke-virtual {v3, v10}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;->copyFavorite(Z)V

    goto :goto_2

    .line 227
    .end local v0    # "c":Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;
    .end local v3    # "channel":Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;
    .end local v8    # "providers":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_6
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->sortChannelsArray(Ljava/util/ArrayList;)V

    .line 228
    invoke-direct {p0, v5}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->sortChannelsArray(Ljava/util/ArrayList;)V

    .line 230
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v9

    new-array v9, v9, [Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    iput-object v9, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mChannels:[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    .line 231
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v9

    new-array v9, v9, [Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    iput-object v9, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mFavChannels:[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    .line 232
    iput-boolean v11, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mLoading:Z

    .line 234
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->notifyListenersState()V

    .line 235
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->notifyListenersData()V

    goto/16 :goto_0
.end method

.method private onRequestError(ZZ)V
    .locals 5
    .param p1, "refresh"    # Z
    .param p2, "error"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 257
    const-string v0, "AppChannelsModel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRequestError(refresh: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    iput-boolean p2, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mError:Z

    .line 260
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mLoading:Z

    .line 261
    iput-object v3, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mGetChannelsTask:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;

    .line 262
    iput-object v3, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mInProgressFavChannels:[Lcom/microsoft/xbox/service/model/epg/EPListItem;

    .line 263
    iput-object v3, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mInProgressChannels:Ljava/util/List;

    .line 265
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mChannels:[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mChannels:[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 266
    :cond_0
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mError:Z

    .line 268
    :cond_1
    if-eqz p1, :cond_2

    .line 269
    iput-object v3, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mChannels:[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    .line 270
    iput-object v3, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mFavChannels:[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    .line 273
    :cond_2
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->notifyListenersState()V

    .line 274
    if-eqz p1, :cond_3

    .line 275
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->notifyListenersData()V

    .line 277
    :cond_3
    return-void
.end method

.method private requestChannels()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 170
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mGetChannelsTask:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;

    if-eqz v0, :cond_0

    .line 187
    :goto_0
    return-void

    .line 174
    :cond_0
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mLoading:Z

    .line 176
    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mInProgressFavChannels:[Lcom/microsoft/xbox/service/model/epg/EPListItem;

    .line 177
    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mInProgressChannels:Ljava/util/List;

    .line 179
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-eq v0, v1, :cond_1

    .line 180
    const/4 v0, 0x0

    invoke-direct {p0, v2, v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->onRequestError(ZZ)V

    goto :goto_0

    .line 182
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/epg/AppChannelsModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mGetChannelsTask:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;

    .line 183
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mGetChannelsTask:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;->execute()V

    .line 185
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->notifyListenersState()V

    goto :goto_0
.end method

.method private sortChannelsArray(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 239
    .local p1, "arrayListToSort":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;>;"
    new-instance v0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$1;-><init>(Lcom/microsoft/xbox/xle/epg/AppChannelsModel;)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 254
    return-void
.end method


# virtual methods
.method public final getChannels(Z)[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;
    .locals 1
    .param p1, "favoritesOnly"    # Z

    .prologue
    .line 142
    if-eqz p1, :cond_0

    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mFavChannels:[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    .line 145
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mChannels:[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    goto :goto_0
.end method

.method public isError()Z
    .locals 1

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mError:Z

    return v0
.end method

.method public isLoading()Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mLoading:Z

    return v0
.end method

.method public onAppChannelDataReceived(Lcom/microsoft/xbox/xle/urc/net/AppChannelData;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "data"    # Lcom/microsoft/xbox/xle/urc/net/AppChannelData;
    .param p2, "error"    # Ljava/lang/Throwable;
    .param p3, "reqProviderID"    # Ljava/lang/String;
    .param p4, "reqChannelID"    # Ljava/lang/String;

    .prologue
    .line 298
    return-void
.end method

.method public onAppChannelLineupsReceived(Ljava/util/List;Ljava/lang/Throwable;)V
    .locals 2
    .param p2, "error"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;",
            ">;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups$ChannelInfo;>;"
    const/4 v1, 0x1

    .line 284
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mGetChannelsTask:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;

    .line 285
    if-nez p1, :cond_0

    .line 286
    invoke-direct {p0, v1, v1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->onRequestError(ZZ)V

    .line 291
    :goto_0
    return-void

    .line 288
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mInProgressChannels:Ljava/util/List;

    .line 289
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->onCheckChannelsDone()V

    goto :goto_0
.end method

.method public onAppChannelProgramDataReceived(Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "data"    # Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;
    .param p2, "error"    # Ljava/lang/Throwable;
    .param p3, "providerID"    # Ljava/lang/String;
    .param p4, "showID"    # Ljava/lang/String;

    .prologue
    .line 305
    return-void
.end method

.method public onConfigChanged([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V
    .locals 0
    .param p1, "devices"    # [Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    .prologue
    .line 331
    return-void
.end method

.method public onConnectionStateChanged(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;Ljava/lang/String;)V
    .locals 3
    .param p1, "state"    # Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;
    .param p2, "connectionError"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 312
    invoke-static {p1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->isBranchConnectedState(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;)Z

    move-result v0

    .line 313
    .local v0, "newConnected":Z
    if-nez v0, :cond_1

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mLoading:Z

    if-eqz v1, :cond_1

    .line 314
    invoke-direct {p0, v2, v2}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->onRequestError(ZZ)V

    .line 322
    :cond_0
    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mConnected:Z

    .line 323
    return-void

    .line 315
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-ne p1, v1, :cond_2

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mIsStarted:Z

    if-eqz v1, :cond_2

    .line 316
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->refresh()V

    goto :goto_0

    .line 317
    :cond_2
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mConnected:Z

    if-eq v0, v1, :cond_0

    .line 318
    if-nez v0, :cond_0

    .line 319
    const/4 v1, 0x0

    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->onRequestError(ZZ)V

    goto :goto_0
.end method

.method public onHeadendChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 0
    .param p1, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 339
    return-void
.end method

.method public onHeadendSettingChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 0
    .param p1, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 347
    return-void
.end method

.method public refresh()V
    .locals 2

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mLoading:Z

    if-eqz v0, :cond_0

    .line 121
    :goto_0
    return-void

    .line 115
    :cond_0
    const-string v0, "AppChannelsModel"

    const-string v1, "Refresh"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mGetChannelsTask:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;

    .line 118
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mError:Z

    .line 120
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->requestChannels()V

    goto :goto_0
.end method

.method public start(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$IListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/epg/AppChannelsModel$IListener;

    .prologue
    .line 69
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    if-nez v0, :cond_0

    .line 70
    const-string v0, "AppChannelsModel"

    const-string v1, "Branch Session not yet initialized"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    :goto_0
    return-void

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->isBranchConnectedState(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mConnected:Z

    .line 78
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->addListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;)V

    .line 79
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->addListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$IAppChannelsListener;)V

    .line 81
    const-string v0, "AppChannelsModel"

    const-string v1, "Subscribed to branch session"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->refresh()V

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mIsStarted:Z

    goto :goto_0
.end method

.method public stop(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$IListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/epg/AppChannelsModel$IListener;

    .prologue
    .line 94
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mIsStarted:Z

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->removeListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;)V

    .line 98
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->removeListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$IAppChannelsListener;)V

    .line 100
    const-string v0, "AppChannelsModel"

    const-string v1, "Unsubscribed from branch session"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->mGetChannelsTask:Lcom/microsoft/xbox/xle/epg/AppChannelsModel$GetChannelsAsyncTask;

    .line 104
    :cond_0
    return-void
.end method
