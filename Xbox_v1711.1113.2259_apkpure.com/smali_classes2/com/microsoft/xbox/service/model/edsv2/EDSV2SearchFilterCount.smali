.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterCount;
.super Ljava/lang/Object;
.source "EDSV2SearchFilterCount.java"


# instance fields
.field private filterType:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

.field private resultCount:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;I)V
    .locals 0
    .param p1, "filterType"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;
    .param p2, "resultCount"    # I

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterCount;->filterType:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .line 12
    iput p2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterCount;->resultCount:I

    .line 13
    return-void
.end method


# virtual methods
.method public getFilterType()Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterCount;->filterType:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    return-object v0
.end method

.method public getResultCount()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterCount;->resultCount:I

    return v0
.end method

.method public setFilterType(Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;)V
    .locals 0
    .param p1, "filterType"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterCount;->filterType:Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterType;

    .line 21
    return-void
.end method

.method public setResultCount(I)V
    .locals 0
    .param p1, "resultCount"    # I

    .prologue
    .line 28
    iput p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2SearchFilterCount;->resultCount:I

    .line 29
    return-void
.end method
