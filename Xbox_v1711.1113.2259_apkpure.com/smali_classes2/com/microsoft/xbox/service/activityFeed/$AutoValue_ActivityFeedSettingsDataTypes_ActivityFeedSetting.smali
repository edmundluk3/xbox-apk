.class abstract Lcom/microsoft/xbox/service/activityFeed/$AutoValue_ActivityFeedSettingsDataTypes_ActivityFeedSetting;
.super Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSetting;
.source "$AutoValue_ActivityFeedSettingsDataTypes_ActivityFeedSetting.java"


# instance fields
.field private final name:Ljava/lang/String;

.field private final value:Z


# direct methods
.method constructor <init>(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSetting;-><init>()V

    .line 15
    if-nez p1, :cond_0

    .line 16
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null name"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 18
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_ActivityFeedSettingsDataTypes_ActivityFeedSetting;->name:Ljava/lang/String;

    .line 19
    iput-boolean p2, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_ActivityFeedSettingsDataTypes_ActivityFeedSetting;->value:Z

    .line 20
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 42
    if-ne p1, p0, :cond_1

    .line 50
    :cond_0
    :goto_0
    return v1

    .line 45
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSetting;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 46
    check-cast v0, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSetting;

    .line 47
    .local v0, "that":Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSetting;
    iget-object v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_ActivityFeedSettingsDataTypes_ActivityFeedSetting;->name:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSetting;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_ActivityFeedSettingsDataTypes_ActivityFeedSetting;->value:Z

    .line 48
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSetting;->value()Z

    move-result v4

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/service/activityFeed/ActivityFeedSettingsDataTypes$ActivityFeedSetting;
    :cond_3
    move v1, v2

    .line 50
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 55
    const/4 v0, 0x1

    .line 56
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 57
    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_ActivityFeedSettingsDataTypes_ActivityFeedSetting;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 58
    mul-int/2addr v0, v2

    .line 59
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_ActivityFeedSettingsDataTypes_ActivityFeedSetting;->value:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x4cf

    :goto_0
    xor-int/2addr v0, v1

    .line 60
    return v0

    .line 59
    :cond_0
    const/16 v1, 0x4d5

    goto :goto_0
.end method

.method public name()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_ActivityFeedSettingsDataTypes_ActivityFeedSetting;->name:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ActivityFeedSetting{name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_ActivityFeedSettingsDataTypes_ActivityFeedSetting;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_ActivityFeedSettingsDataTypes_ActivityFeedSetting;->value:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public value()Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/activityFeed/$AutoValue_ActivityFeedSettingsDataTypes_ActivityFeedSetting;->value:Z

    return v0
.end method
