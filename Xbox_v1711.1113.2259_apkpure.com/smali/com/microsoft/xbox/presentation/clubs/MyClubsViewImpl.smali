.class public Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;
.super Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;
.source "MyClubsViewImpl.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/clubs/MyClubsView;
.implements Lcom/microsoft/xbox/xle/app/activity/HeaderProvider;


# instance fields
.field contentList:Landroid/support/v7/widget/RecyclerView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e07dc
    .end annotation
.end field

.field private listAdapter:Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;

.field presenter:Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field refreshButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0590
    .end annotation
.end field

.field private viewIntents:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;-><init>()V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    return-void
.end method

.method static synthetic lambda$onCreateContentView$0(Ljava/lang/Object;)Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;
    .locals 1
    .param p0, "e"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 82
    sget-object v0, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$RefreshIntent;

    return-object v0
.end method

.method static synthetic lambda$renderClubList$1(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)I
    .locals 4
    .param p0, "c1"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .param p1, "c2"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .prologue
    .line 141
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceCount()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceCount()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private renderClubList(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 138
    .local p1, "clubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 141
    .local v0, "sortedContent":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    invoke-static {}, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl$$Lambda$4;->lambdaFactory$()Ljava/util/Comparator;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 143
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->setListContent(Ljava/util/List;)V

    .line 144
    return-void
.end method

.method private renderContent(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 126
    .local p1, "content":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->renderEmptyContent()V

    .line 131
    :goto_0
    return-void

    .line 129
    :cond_0
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->renderClubList(Ljava/util/List;)V

    goto :goto_0
.end method

.method private renderEmptyContent()V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 135
    return-void
.end method

.method private renderError()V
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 152
    return-void
.end method

.method private renderLoading()V
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 148
    return-void
.end method

.method private setListContent(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 155
    .local p1, "content":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;->getDataCopy()Ljava/util/List;

    move-result-object v1

    .line 157
    .local v1, "oldContent":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;->clear()V

    .line 158
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;

    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;->addAll(Ljava/util/Collection;)V

    .line 160
    new-instance v2, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;

    invoke-direct {v2, v1, p1}, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-static {v2}, Landroid/support/v7/util/DiffUtil;->calculateDiff(Landroid/support/v7/util/DiffUtil$Callback;)Landroid/support/v7/util/DiffUtil$DiffResult;

    move-result-object v0

    .line 161
    .local v0, "diffResult":Landroid/support/v7/util/DiffUtil$DiffResult;
    iget-object v2, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;

    invoke-virtual {v0, v2}, Landroid/support/v7/util/DiffUtil$DiffResult;->dispatchUpdatesTo(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 162
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    const-class v0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getContentScreenId()I
    .locals 1

    .prologue
    .line 87
    const v0, 0x7f030181

    return v0
.end method

.method protected getErrorScreenId()I
    .locals 1

    .prologue
    .line 92
    const v0, 0x7f0300f9

    return v0
.end method

.method public getHeader()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070604

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getLifecycleListener()Lcom/microsoft/xbox/presentation/base/ViewLifecycleListener;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->presenter:Lcom/microsoft/xbox/presentation/clubs/MyClubsPresenter;

    return-object v0
.end method

.method protected getNoContentScreenId()I
    .locals 1

    .prologue
    .line 97
    const v0, 0x7f030182

    return v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 67
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->onCreate()V

    .line 68
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;)V

    .line 69
    return-void
.end method

.method public onCreateContentView()V
    .locals 4

    .prologue
    .line 73
    invoke-super {p0}, Lcom/microsoft/xbox/presentation/base/MviSwitchPanelScreen;->onCreateContentView()V

    .line 75
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    .line 77
    .local v0, "itemClickedRelay":Lcom/jakewharton/rxrelay2/PublishRelay;, "Lcom/jakewharton/rxrelay2/PublishRelay<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    new-instance v1, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v0}, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl$$Lambda$1;->lambdaFactory$(Lcom/jakewharton/rxrelay2/PublishRelay;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;-><init>(Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v1, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;

    .line 78
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->contentList:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->listAdapter:Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 80
    invoke-static {}, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl$$Lambda$2;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 81
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->refreshButton:Landroid/widget/Button;

    .line 82
    invoke-static {v2}, Lcom/jakewharton/rxbinding2/view/RxView;->clicks(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 80
    invoke-static {v1, v2}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->viewIntents:Lio/reactivex/Observable;

    .line 83
    return-void
.end method

.method public render(Lcom/microsoft/xbox/presentation/lce/LceViewState;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/presentation/lce/LceViewState",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 112
    .local p1, "viewState":Lcom/microsoft/xbox/presentation/lce/LceViewState;, "Lcom/microsoft/xbox/presentation/lce/LceViewState<Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->isContent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->content()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->renderContent(Ljava/util/List;)V

    .line 123
    :goto_0
    return-void

    .line 114
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->isLoading()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->renderLoading()V

    goto :goto_0

    .line 116
    :cond_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->isLoadingMore()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 117
    const-string v0, "Not implemented"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0

    .line 118
    :cond_2
    invoke-virtual {p1}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->isError()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 119
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->renderError()V

    goto :goto_0

    .line 121
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown view state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic render(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 36
    check-cast p1, Lcom/microsoft/xbox/presentation/lce/LceViewState;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->render(Lcom/microsoft/xbox/presentation/lce/LceViewState;)V

    return-void
.end method

.method public viewIntents()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsViewImpl;->viewIntents:Lio/reactivex/Observable;

    return-object v0
.end method
