.class public Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepositoryImpl;
.super Ljava/lang/Object;
.source "LanguageSettingsRepositoryImpl.java"

# interfaces
.implements Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final USER_LANGUAGE_KEY:Ljava/lang/String; = "user_language_pref"

.field private static final USER_LOCATION_KEY:Ljava/lang/String; = "user_location_pref"


# instance fields
.field private final languagePrefsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay",
            "<",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;",
            ">;"
        }
    .end annotation
.end field

.field private final sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepositoryImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepositoryImpl;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/SharedPreferences;)V
    .locals 1
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 30
    invoke-direct {p0}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepositoryImpl;->getCurrentPrefsInternal()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepositoryImpl;->languagePrefsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 31
    return-void
.end method

.method private getCurrentPrefsInternal()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepositoryImpl;->getUserDefinedLanguage()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    move-result-object v0

    invoke-direct {p0}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepositoryImpl;->getUserDefinedLocation()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;->with(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;)Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;

    move-result-object v0

    return-object v0
.end method

.method private getUserDefinedLanguage()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "user_language_pref"

    sget-object v2, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;->DefaultLanguage:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    invoke-virtual {v2}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;->valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    move-result-object v0

    return-object v0
.end method

.method private getUserDefinedLocation()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "user_location_pref"

    sget-object v2, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->DefaultMarket:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    invoke-virtual {v2}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCurrentPrefs()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepositoryImpl;->languagePrefsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public getLanguageInUse()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepositoryImpl;->getUserDefinedLanguage()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    move-result-object v0

    .line 83
    .local v0, "language":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;
    sget-object v1, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;->DefaultLanguage:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    if-ne v0, v1, :cond_0

    .line 84
    invoke-static {}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsUtils;->getSystemConfiguredLanguage()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    move-result-object v0

    .line 87
    :cond_0
    return-object v0
.end method

.method public getLocationInUse()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepositoryImpl;->getUserDefinedLocation()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    move-result-object v0

    .line 94
    .local v0, "location":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
    sget-object v1, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->DefaultMarket:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    if-ne v0, v1, :cond_0

    .line 95
    invoke-static {}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsUtils;->getSystemConfiguredLocation()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    move-result-object v0

    .line 98
    :cond_0
    return-object v0
.end method

.method public prefs()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepositoryImpl;->languagePrefsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method public setUserDefinedLanguage(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;)Lio/reactivex/Single;
    .locals 5
    .param p1, "newLanguage"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepositoryImpl;->getCurrentPrefsInternal()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;

    move-result-object v1

    .line 47
    .local v1, "previousPrefs":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;
    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;->currentLanguage()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    move-result-object v2

    if-eq v2, p1, :cond_0

    .line 48
    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;->currentMarket()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;->with(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;)Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;

    move-result-object v0

    .line 50
    .local v0, "newPrefs":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;
    iget-object v2, p0, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 51
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v3, "user_language_pref"

    .line 52
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;->name()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 53
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 55
    iget-object v2, p0, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepositoryImpl;->languagePrefsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v2, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 58
    .end local v0    # "newPrefs":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepositoryImpl;->getCurrentPrefs()Lio/reactivex/Single;

    move-result-object v2

    return-object v2
.end method

.method public setUserDefinedLocation(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;)Lio/reactivex/Single;
    .locals 5
    .param p1, "newLocation"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepositoryImpl;->getCurrentPrefsInternal()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;

    move-result-object v1

    .line 65
    .local v1, "previousPrefs":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;
    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;->currentMarket()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    move-result-object v2

    if-eq v2, p1, :cond_0

    .line 66
    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;->currentLanguage()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;->with(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;)Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;

    move-result-object v0

    .line 68
    .local v0, "newPrefs":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;
    iget-object v2, p0, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepositoryImpl;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 69
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v3, "user_location_pref"

    .line 70
    invoke-virtual {p1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->name()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 71
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 73
    iget-object v2, p0, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepositoryImpl;->languagePrefsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v2, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 76
    .end local v0    # "newPrefs":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepositoryImpl;->getCurrentPrefs()Lio/reactivex/Single;

    move-result-object v2

    return-object v2
.end method
