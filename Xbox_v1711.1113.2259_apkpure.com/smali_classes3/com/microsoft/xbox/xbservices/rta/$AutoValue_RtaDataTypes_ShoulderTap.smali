.class abstract Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;
.super Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$ShoulderTap;
.source "$AutoValue_RtaDataTypes_ShoulderTap.java"


# instance fields
.field private final branch:Ljava/lang/String;

.field private final changeNumber:I

.field private final resource:Ljava/lang/String;

.field private final resourceType:Ljava/lang/String;

.field private final subscription:Ljava/lang/String;

.field private final timestamp:Ljava/util/Date;


# direct methods
.method constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)V
    .locals 2
    .param p1, "branch"    # Ljava/lang/String;
    .param p2, "changeNumber"    # I
    .param p3, "resource"    # Ljava/lang/String;
    .param p4, "resourceType"    # Ljava/lang/String;
    .param p5, "subscription"    # Ljava/lang/String;
    .param p6, "timestamp"    # Ljava/util/Date;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$ShoulderTap;-><init>()V

    .line 25
    if-nez p1, :cond_0

    .line 26
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null branch"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 28
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->branch:Ljava/lang/String;

    .line 29
    iput p2, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->changeNumber:I

    .line 30
    if-nez p3, :cond_1

    .line 31
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null resource"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 33
    :cond_1
    iput-object p3, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->resource:Ljava/lang/String;

    .line 34
    if-nez p4, :cond_2

    .line 35
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null resourceType"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_2
    iput-object p4, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->resourceType:Ljava/lang/String;

    .line 38
    if-nez p5, :cond_3

    .line 39
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null subscription"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_3
    iput-object p5, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->subscription:Ljava/lang/String;

    .line 42
    if-nez p6, :cond_4

    .line 43
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null timestamp"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_4
    iput-object p6, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->timestamp:Ljava/util/Date;

    .line 46
    return-void
.end method


# virtual methods
.method public branch()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->branch:Ljava/lang/String;

    return-object v0
.end method

.method public changeNumber()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->changeNumber:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 97
    if-ne p1, p0, :cond_1

    .line 109
    :cond_0
    :goto_0
    return v1

    .line 100
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$ShoulderTap;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 101
    check-cast v0, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$ShoulderTap;

    .line 102
    .local v0, "that":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$ShoulderTap;
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->branch:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$ShoulderTap;->branch()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->changeNumber:I

    .line 103
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$ShoulderTap;->changeNumber()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->resource:Ljava/lang/String;

    .line 104
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$ShoulderTap;->resource()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->resourceType:Ljava/lang/String;

    .line 105
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$ShoulderTap;->resourceType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->subscription:Ljava/lang/String;

    .line 106
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$ShoulderTap;->subscription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->timestamp:Ljava/util/Date;

    .line 107
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$ShoulderTap;->timestamp()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$ShoulderTap;
    :cond_3
    move v1, v2

    .line 109
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 114
    const/4 v0, 0x1

    .line 115
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 116
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->branch:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 117
    mul-int/2addr v0, v2

    .line 118
    iget v1, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->changeNumber:I

    xor-int/2addr v0, v1

    .line 119
    mul-int/2addr v0, v2

    .line 120
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->resource:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 121
    mul-int/2addr v0, v2

    .line 122
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->resourceType:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 123
    mul-int/2addr v0, v2

    .line 124
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->subscription:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 125
    mul-int/2addr v0, v2

    .line 126
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->timestamp:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 127
    return v0
.end method

.method public resource()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->resource:Ljava/lang/String;

    return-object v0
.end method

.method public resourceType()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->resourceType:Ljava/lang/String;

    return-object v0
.end method

.method public subscription()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->subscription:Ljava/lang/String;

    return-object v0
.end method

.method public timestamp()Ljava/util/Date;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->timestamp:Ljava/util/Date;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ShoulderTap{branch="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->branch:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", changeNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->changeNumber:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", resource="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->resource:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", resourceType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->resourceType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", subscription="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->subscription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/rta/$AutoValue_RtaDataTypes_ShoulderTap;->timestamp:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
