.class public Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;
.super Ljava/util/concurrent/FutureTask;
.source "TransmitterOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/FutureTask",
        "<TV;>;"
    }
.end annotation


# instance fields
.field mWorker:Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker",
            "<TV;>;)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p0, "this":Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationTask<TV;>;"
    .local p1, "worker":Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker<TV;>;"
    invoke-direct {p0, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 21
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;->mWorker:Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;

    .line 22
    return-void
.end method


# virtual methods
.method public addOperationListener(Lcom/microsoft/playready/networkdevice/ITransmitterOperationListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/playready/networkdevice/ITransmitterOperationListener;

    .prologue
    .line 31
    .local p0, "this":Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationTask<TV;>;"
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;->mWorker:Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;

    invoke-virtual {v0, p1}, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->addOperationListener(Lcom/microsoft/playready/networkdevice/ITransmitterOperationListener;)V

    .line 32
    return-void
.end method

.method public cancel(Z)Z
    .locals 1
    .param p1, "mayInterruptIfRunning"    # Z

    .prologue
    .line 47
    .local p0, "this":Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationTask<TV;>;"
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;->mWorker:Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;

    invoke-virtual {v0, p1}, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->cancel(Z)Z

    .line 49
    invoke-super {p0, p1}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    move-result v0

    return v0
.end method

.method public getErrorCode()I
    .locals 1

    .prologue
    .line 59
    .local p0, "this":Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationTask<TV;>;"
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;->mWorker:Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;

    invoke-virtual {v0}, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->getErrorCode()I

    move-result v0

    return v0
.end method

.method public removeOperationListener(Lcom/microsoft/playready/networkdevice/ITransmitterOperationListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/playready/networkdevice/ITransmitterOperationListener;

    .prologue
    .line 41
    .local p0, "this":Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;, "Lcom/microsoft/playready/networkdevice/TransmitterOperationTask<TV;>;"
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/TransmitterOperationTask;->mWorker:Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;

    invoke-virtual {v0, p1}, Lcom/microsoft/playready/networkdevice/TransmitterOperationWorker;->removeOperationListener(Lcom/microsoft/playready/networkdevice/ITransmitterOperationListener;)V

    .line 42
    return-void
.end method
