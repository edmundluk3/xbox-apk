.class Lcom/microsoft/xbox/smartglass/controls/Touch;
.super Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;
.source "Touch.java"


# static fields
.field public static final ComponentName:Ljava/lang/String; = "Touch"

.field private static final SendTouchFrameMethod:Ljava/lang/String; = "SendTouchFrame"


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V
    .locals 1
    .param p1, "canvasState"    # Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    .prologue
    .line 22
    const-string v0, "Touch"

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    .line 24
    const-string v0, "SendTouchFrame"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/Touch;->registerMethod(Ljava/lang/String;)V

    .line 25
    return-void
.end method


# virtual methods
.method public invoke(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "requestId"    # I
    .param p2, "methodName"    # Ljava/lang/String;
    .param p3, "json"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/smartglass/controls/Touch;->validateMethod(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 30
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/controls/Touch;->failRequestUnknownMethod(ILjava/lang/String;)V

    .line 37
    :goto_0
    return-void

    .line 36
    :cond_0
    invoke-virtual {p0, p1, p3}, Lcom/microsoft/xbox/smartglass/controls/Touch;->sendTouchFrame(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public sendTouchFrame(ILjava/lang/String;)V
    .locals 20
    .param p1, "requestId"    # I
    .param p2, "json"    # Ljava/lang/String;

    .prologue
    .line 41
    :try_start_0
    new-instance v4, Lcom/microsoft/xbox/smartglass/controls/JsonTouchFrame;

    move-object/from16 v0, p2

    invoke-direct {v4, v0}, Lcom/microsoft/xbox/smartglass/controls/JsonTouchFrame;-><init>(Ljava/lang/String;)V

    .line 44
    .local v4, "frame":Lcom/microsoft/xbox/smartglass/controls/JsonTouchFrame;
    iget v8, v4, Lcom/microsoft/xbox/smartglass/controls/JsonTouchFrame;->titleId:I

    if-lez v8, :cond_0

    .line 45
    new-instance v6, Lcom/microsoft/xbox/smartglass/MessageTarget;

    iget v8, v4, Lcom/microsoft/xbox/smartglass/controls/JsonTouchFrame;->titleId:I

    invoke-direct {v6, v8}, Lcom/microsoft/xbox/smartglass/MessageTarget;-><init>(I)V

    .line 46
    .local v6, "target":Lcom/microsoft/xbox/smartglass/MessageTarget;
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/microsoft/xbox/smartglass/controls/Touch;->validateSession(Lcom/microsoft/xbox/smartglass/MessageTarget;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 47
    invoke-virtual/range {p0 .. p1}, Lcom/microsoft/xbox/smartglass/controls/Touch;->failRequestValidation(I)V

    .line 74
    .end local v4    # "frame":Lcom/microsoft/xbox/smartglass/controls/JsonTouchFrame;
    .end local v6    # "target":Lcom/microsoft/xbox/smartglass/MessageTarget;
    :goto_0
    return-void

    .line 51
    .restart local v4    # "frame":Lcom/microsoft/xbox/smartglass/controls/JsonTouchFrame;
    :cond_0
    new-instance v6, Lcom/microsoft/xbox/smartglass/MessageTarget;

    sget-object v8, Lcom/microsoft/xbox/smartglass/ServiceChannel;->SystemInput:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    invoke-direct {v6, v8}, Lcom/microsoft/xbox/smartglass/MessageTarget;-><init>(Lcom/microsoft/xbox/smartglass/ServiceChannel;)V

    .line 57
    .restart local v6    # "target":Lcom/microsoft/xbox/smartglass/MessageTarget;
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/smartglass/SessionManager;->getClientResolution()Lcom/microsoft/xbox/smartglass/ClientResolution;

    move-result-object v2

    .line 58
    .local v2, "clientResolution":Lcom/microsoft/xbox/smartglass/ClientResolution;
    new-instance v7, Lcom/microsoft/xbox/smartglass/TouchFrame;

    iget v8, v4, Lcom/microsoft/xbox/smartglass/controls/JsonTouchFrame;->timeStamp:I

    invoke-direct {v7, v8}, Lcom/microsoft/xbox/smartglass/TouchFrame;-><init>(I)V

    .line 59
    .local v7, "touchFrame":Lcom/microsoft/xbox/smartglass/TouchFrame;
    iget-object v9, v4, Lcom/microsoft/xbox/smartglass/controls/JsonTouchFrame;->touchPoints:[Lcom/microsoft/xbox/smartglass/controls/JsonTouchPoint;

    array-length v10, v9

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v10, :cond_3

    aget-object v5, v9, v8

    .line 60
    .local v5, "point":Lcom/microsoft/xbox/smartglass/controls/JsonTouchPoint;
    iget-short v11, v5, Lcom/microsoft/xbox/smartglass/controls/JsonTouchPoint;->id:S

    if-gez v11, :cond_2

    .line 61
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/microsoft/xbox/smartglass/controls/Touch;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v9, "Touch point cannot have a negative ID."

    move/from16 v0, p1

    invoke-interface {v8, v0, v9}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 69
    .end local v2    # "clientResolution":Lcom/microsoft/xbox/smartglass/ClientResolution;
    .end local v4    # "frame":Lcom/microsoft/xbox/smartglass/controls/JsonTouchFrame;
    .end local v5    # "point":Lcom/microsoft/xbox/smartglass/controls/JsonTouchPoint;
    .end local v6    # "target":Lcom/microsoft/xbox/smartglass/MessageTarget;
    .end local v7    # "touchFrame":Lcom/microsoft/xbox/smartglass/TouchFrame;
    :catch_0
    move-exception v3

    .line 70
    .local v3, "ex":Lorg/json/JSONException;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/microsoft/xbox/smartglass/controls/Touch;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Invalid JSON. "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move/from16 v0, p1

    invoke-interface {v8, v0, v9}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0

    .line 64
    .end local v3    # "ex":Lorg/json/JSONException;
    .restart local v2    # "clientResolution":Lcom/microsoft/xbox/smartglass/ClientResolution;
    .restart local v4    # "frame":Lcom/microsoft/xbox/smartglass/controls/JsonTouchFrame;
    .restart local v5    # "point":Lcom/microsoft/xbox/smartglass/controls/JsonTouchPoint;
    .restart local v6    # "target":Lcom/microsoft/xbox/smartglass/MessageTarget;
    .restart local v7    # "touchFrame":Lcom/microsoft/xbox/smartglass/TouchFrame;
    :cond_2
    :try_start_1
    iget-object v11, v7, Lcom/microsoft/xbox/smartglass/TouchFrame;->touchPoints:Ljava/util/List;

    new-instance v12, Lcom/microsoft/xbox/smartglass/TouchPoint;

    iget-short v13, v5, Lcom/microsoft/xbox/smartglass/controls/JsonTouchPoint;->id:S

    iget-object v14, v5, Lcom/microsoft/xbox/smartglass/controls/JsonTouchPoint;->action:Lcom/microsoft/xbox/smartglass/TouchAction;

    iget-short v15, v2, Lcom/microsoft/xbox/smartglass/ClientResolution;->nativeWidth:S

    int-to-double v0, v15

    move-wide/from16 v16, v0

    iget-wide v0, v5, Lcom/microsoft/xbox/smartglass/controls/JsonTouchPoint;->x:D

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-int v15, v0

    iget-short v0, v2, Lcom/microsoft/xbox/smartglass/ClientResolution;->nativeHeight:S

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-double v0, v0

    move-wide/from16 v16, v0

    iget-wide v0, v5, Lcom/microsoft/xbox/smartglass/controls/JsonTouchPoint;->y:D

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-int v0, v0

    move/from16 v16, v0

    invoke-direct/range {v12 .. v16}, Lcom/microsoft/xbox/smartglass/TouchPoint;-><init>(SLcom/microsoft/xbox/smartglass/TouchAction;II)V

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 67
    .end local v5    # "point":Lcom/microsoft/xbox/smartglass/controls/JsonTouchPoint;
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSensorManager()Lcom/microsoft/xbox/smartglass/SensorManager;

    move-result-object v8

    invoke-virtual {v8, v7, v6}, Lcom/microsoft/xbox/smartglass/SensorManager;->sendTouchFrame(Lcom/microsoft/xbox/smartglass/TouchFrame;Lcom/microsoft/xbox/smartglass/MessageTarget;)V

    .line 68
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/microsoft/xbox/smartglass/controls/Touch;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    move/from16 v0, p1

    invoke-interface {v8, v0}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->completeRequest(I)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 71
    .end local v2    # "clientResolution":Lcom/microsoft/xbox/smartglass/ClientResolution;
    .end local v4    # "frame":Lcom/microsoft/xbox/smartglass/controls/JsonTouchFrame;
    .end local v6    # "target":Lcom/microsoft/xbox/smartglass/MessageTarget;
    .end local v7    # "touchFrame":Lcom/microsoft/xbox/smartglass/TouchFrame;
    :catch_1
    move-exception v3

    .line 72
    .local v3, "ex":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/microsoft/xbox/smartglass/controls/Touch;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Failed to send touch frame. "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move/from16 v0, p1

    invoke-interface {v8, v0, v9}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto/16 :goto_0
.end method
