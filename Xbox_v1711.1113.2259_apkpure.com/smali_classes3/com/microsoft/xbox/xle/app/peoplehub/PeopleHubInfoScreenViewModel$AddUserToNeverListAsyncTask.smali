.class Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "PeopleHubInfoScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AddUserToNeverListAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private blockUserXuid:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;Ljava/lang/String;)V
    .locals 0
    .param p2, "blockUserXuid"    # Ljava/lang/String;

    .prologue
    .line 891
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 892
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;->blockUserXuid:Ljava/lang/String;

    .line 893
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 897
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 898
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3

    .prologue
    .line 926
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 927
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    .line 928
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;->forceLoad:Z

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;->blockUserXuid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->addUserToNeverList(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    .line 930
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 887
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 921
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 887
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 3

    .prologue
    .line 903
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 904
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;->blockUserXuid:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V

    .line 905
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 916
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;->blockUserXuid:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V

    .line 917
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 887
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 909
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 910
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->access$602(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;Z)Z

    .line 911
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$AddUserToNeverListAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->access$700(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;)V

    .line 912
    return-void
.end method
