.class Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$TextMessageListener;
.super Lcom/microsoft/xbox/smartglass/TextManagerListener;
.source "TextInputControlViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TextMessageListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;)V
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$TextMessageListener;->this$0:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/TextManagerListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$1;

    .prologue
    .line 157
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$TextMessageListener;-><init>(Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;)V

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 2

    .prologue
    .line 160
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 161
    const-string v0, "textinputvm"

    const-string v1, "onCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getVisibleDialog()Landroid/app/Dialog;

    move-result-object v0

    instance-of v0, v0, Lcom/microsoft/xbox/xle/remote/TextInputControl;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$TextMessageListener;->this$0:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->access$700(Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;)Lcom/microsoft/xbox/xle/remote/TextInputControl;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/remote/TextInputControl;->close(Z)V

    .line 167
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Lcom/microsoft/xbox/smartglass/TextConfiguration;)V
    .locals 2
    .param p1, "configuration"    # Lcom/microsoft/xbox/smartglass/TextConfiguration;

    .prologue
    .line 191
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 192
    const-string v0, "textinputvm"

    const-string v1, "onConfigurationChanged"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$TextMessageListener;->this$0:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->access$700(Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;)Lcom/microsoft/xbox/xle/remote/TextInputControl;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/remote/TextInputControl;->updateConfiguration(Lcom/microsoft/xbox/smartglass/TextConfiguration;)V

    .line 194
    return-void
.end method

.method public onTextChanged(Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 174
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 175
    const-string v0, "textinputvm"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTextChanged: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$TextMessageListener;->this$0:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->onTextChangedMessage(Ljava/lang/String;)V

    .line 177
    return-void
.end method

.method public onTextSelected(II)V
    .locals 6
    .param p1, "start"    # I
    .param p2, "length"    # I

    .prologue
    .line 184
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 185
    const-string v0, "textinputvm"

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "onTextSelected(%d,%d)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel$TextMessageListener;->this$0:Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/xle/remote/TextInputControlViewModel;->onTextSelectionMessage(II)V

    .line 187
    return-void
.end method
