.class public Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;
.source "PeopleHubAchievementsScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadGamerscoreComparisonAsyncTask;,
        Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadTitleDataAsyncTask;,
        Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;,
        Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;
    }
.end annotation


# static fields
.field private static final MAX_RECENT_PROGRESS_AND_ACHIEVEMENT_ITEMS:I = 0x19

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private achievementsFilter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;

.field private compare360Data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            ">;>;"
        }
    .end annotation
.end field

.field private compareData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            ">;>;"
        }
    .end annotation
.end field

.field private compareMergedData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            ">;>;"
        }
    .end annotation
.end field

.field private friendsSelected:Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

.field private final gamerscoreLeaderboardViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

.field private isLoadingMyRecentProgressAndAchievement:Z

.field private isLoadingOtherRecentProgressAndAchievement:Z

.field private loadGamerscoreComparisonAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadGamerscoreComparisonAsyncTask;

.field private loadMyRecentProgressAndAchievementAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;

.field private loadMyRecentProgressAndAchievementsState:Lcom/microsoft/xbox/toolkit/network/ListState;

.field private loadOtherRecentProgressAndAchievementAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;

.field private loadOtherRecentProgressAndAchievementsState:Lcom/microsoft/xbox/toolkit/network/ListState;

.field private loadTitleDataAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadTitleDataAsyncTask;

.field private meGamerscoreChange:J

.field private final model:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private final myMergedRecentData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            ">;"
        }
    .end annotation
.end field

.field private final myRecent360Data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            ">;"
        }
    .end annotation
.end field

.field private final myRecentData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            ">;"
        }
    .end annotation
.end field

.field private otherGamerscoreChange:J

.field private final otherMergedRecentData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            ">;"
        }
    .end annotation
.end field

.field private final otherRecent360Data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            ">;"
        }
    .end annotation
.end field

.field private final otherRecentData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            ">;"
        }
    .end annotation
.end field

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 4
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    const-wide/16 v2, -0x1

    .line 122
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;-><init>()V

    .line 65
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 68
    iput-wide v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->meGamerscoreChange:J

    .line 69
    iput-wide v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->otherGamerscoreChange:J

    .line 73
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->myRecent360Data:Ljava/util/List;

    .line 74
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->myRecentData:Ljava/util/List;

    .line 75
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->myMergedRecentData:Ljava/util/List;

    .line 76
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->otherRecent360Data:Ljava/util/List;

    .line 77
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->otherRecentData:Ljava/util/List;

    .line 78
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->otherMergedRecentData:Ljava/util/List;

    .line 79
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->compare360Data:Ljava/util/List;

    .line 80
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->compareData:Ljava/util/List;

    .line 81
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->compareMergedData:Ljava/util/List;

    .line 88
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadMyRecentProgressAndAchievementsState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 89
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadOtherRecentProgressAndAchievementsState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 123
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->setScreen(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 125
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedProfile()Ljava/lang/String;

    move-result-object v0

    .line 126
    .local v0, "profileXuid":Ljava/lang/String;
    new-instance v1, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    invoke-direct {v1, p1, p0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->gamerscoreLeaderboardViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    .line 127
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->gamerscoreLeaderboardViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->addViewModel(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)Z

    .line 128
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 129
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 133
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getPeopleHubAchievementsAdapter(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 134
    return-void

    .line 131
    :cond_1
    invoke-static {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getProfileModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/ProfileModel;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Lcom/microsoft/xbox/service/model/ProfileModel;
    .param p3, "x3"    # Z

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->onLoadRecentProgressAndAchievementCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/ProfileModel;Z)V

    return-void
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isLoadingMyRecentProgressAndAchievement:Z

    return p1
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isLoadingOtherRecentProgressAndAchievement:Z

    return p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->updateAdapter()V

    return-void
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->onLoadTitleDataCompleted(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->meGamerscoreChange:J

    return-wide v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->otherGamerscoreChange:J

    return-wide v0
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    return-object v0
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;Landroid/util/Pair;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;
    .param p1, "x1"    # Landroid/util/Pair;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->onLoadGamerscoreComparisonCompleted(Landroid/util/Pair;)V

    return-void
.end method

.method private createComparisonList(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            ">;>;"
        }
    .end annotation

    .prologue
    .local p1, "primary":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;>;"
    .local p2, "secondaryInitial":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;>;"
    const/4 v7, 0x0

    .line 462
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 464
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 466
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;>;>;"
    if-nez p2, :cond_0

    .line 468
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    .line 469
    .local v1, "primaryItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    new-instance v6, Landroid/util/Pair;

    invoke-direct {v6, v1, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 472
    .end local v1    # "primaryItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 474
    .local v3, "secondary":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    .line 475
    .restart local v1    # "primaryItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    const/4 v4, 0x0

    .line 477
    .local v4, "secondaryItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-ge v0, v5, :cond_2

    .line 478
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->isSameTitle(Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 479
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "secondaryItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    check-cast v4, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    .line 480
    .restart local v4    # "secondaryItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    invoke-interface {v3, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 477
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 484
    :cond_2
    new-instance v5, Landroid/util/Pair;

    invoke-direct {v5, v4, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 488
    .end local v0    # "i":I
    .end local v1    # "primaryItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    .end local v4    # "secondaryItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    .line 489
    .restart local v4    # "secondaryItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    new-instance v6, Landroid/util/Pair;

    invoke-direct {v6, v4, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 493
    .end local v3    # "secondary":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;>;"
    .end local v4    # "secondaryItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    :cond_4
    return-object v2
.end method

.method private createComparisonLists()V
    .locals 3

    .prologue
    const/16 v2, 0x19

    const/4 v1, 0x0

    .line 441
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isLoadingMyRecentProgressAndAchievement:Z

    if-nez v0, :cond_1

    .line 442
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isMeProfile()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isLoadingOtherRecentProgressAndAchievement:Z

    if-nez v0, :cond_1

    .line 443
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isMeProfile()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 444
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->myRecent360Data:Ljava/util/List;

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->createComparisonList(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->compare360Data:Ljava/util/List;

    .line 445
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->myRecentData:Ljava/util/List;

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->createComparisonList(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->compareData:Ljava/util/List;

    .line 446
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->myMergedRecentData:Ljava/util/List;

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->createComparisonList(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->compareMergedData:Ljava/util/List;

    .line 457
    :cond_1
    :goto_0
    return-void

    .line 448
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->otherRecent360Data:Ljava/util/List;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->myRecent360Data:Ljava/util/List;

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->createComparisonList(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->compare360Data:Ljava/util/List;

    .line 449
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->otherRecentData:Ljava/util/List;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->myRecentData:Ljava/util/List;

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->createComparisonList(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->compareData:Ljava/util/List;

    .line 450
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->otherMergedRecentData:Ljava/util/List;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->myMergedRecentData:Ljava/util/List;

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->createComparisonList(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->compareMergedData:Ljava/util/List;

    .line 452
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->compareMergedData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v2, :cond_1

    .line 453
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->compareMergedData:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->compareMergedData:Ljava/util/List;

    goto :goto_0
.end method

.method private finishLoading()V
    .locals 2

    .prologue
    .line 528
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeProfile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 530
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadMyRecentProgressAndAchievementsState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 542
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->updateAdapter()V

    .line 543
    return-void

    .line 533
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadMyRecentProgressAndAchievementsState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadOtherRecentProgressAndAchievementsState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_2

    .line 534
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->getFilteredResults()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 535
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadMyRecentProgressAndAchievementsState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadOtherRecentProgressAndAchievementsState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_4

    .line 536
    :cond_3
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 538
    :cond_4
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method

.method private loadTitleData(Z)V
    .locals 6
    .param p1, "forceLoad"    # Z

    .prologue
    .line 497
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 499
    .local v1, "titlesToLoad":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->myRecent360Data:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    .line 500
    .local v0, "item":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->getTitleId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 503
    .end local v0    # "item":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->otherRecent360Data:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    .line 504
    .restart local v0    # "item":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->getTitleId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 507
    .end local v0    # "item":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->myRecentData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    .line 508
    .restart local v0    # "item":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->getTitleId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 511
    .end local v0    # "item":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->otherRecentData:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    .line 512
    .restart local v0    # "item":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->getTitleId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 515
    .end local v0    # "item":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    :cond_3
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 516
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadTitleDataAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadTitleDataAsyncTask;

    if-eqz v2, :cond_4

    .line 517
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadTitleDataAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadTitleDataAsyncTask;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadTitleDataAsyncTask;->cancel()V

    .line 520
    :cond_4
    new-instance v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadTitleDataAsyncTask;

    invoke-direct {v2, p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadTitleDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;Ljava/util/Set;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadTitleDataAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadTitleDataAsyncTask;

    .line 521
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadTitleDataAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadTitleDataAsyncTask;

    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadTitleDataAsyncTask;->load(Z)V

    .line 525
    :goto_4
    return-void

    .line 523
    :cond_5
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->finishLoading()V

    goto :goto_4
.end method

.method private onLoadGamerscoreComparisonCompleted(Landroid/util/Pair;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 654
    .local p1, "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Long;Ljava/lang/Long;>;"
    if-eqz p1, :cond_0

    .line 655
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->meGamerscoreChange:J

    .line 656
    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->otherGamerscoreChange:J

    .line 659
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->updateAdapter()V

    .line 660
    return-void
.end method

.method private onLoadRecentProgressAndAchievementCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/ProfileModel;Z)V
    .locals 8
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "profileModel"    # Lcom/microsoft/xbox/service/model/ProfileModel;
    .param p3, "forceLoad"    # Z

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 546
    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onLoadRecentProgressAndAchievementCompleted, isMeProfile == "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeProfile()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeProfile()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 549
    iput-boolean v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isLoadingMyRecentProgressAndAchievement:Z

    .line 554
    :goto_0
    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 613
    :goto_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadMyRecentProgressAndAchievementsState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 614
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeProfile()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadOtherRecentProgressAndAchievementsState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v1, v2, :cond_1

    .line 616
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->createComparisonLists()V

    .line 617
    invoke-direct {p0, p3}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadTitleData(Z)V

    .line 620
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->updateAdapter()V

    .line 625
    return-void

    .line 551
    :cond_2
    iput-boolean v6, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isLoadingOtherRecentProgressAndAchievement:Z

    goto :goto_0

    .line 558
    :pswitch_0
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeProfile()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 559
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->myRecent360Data:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 560
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->myRecentData:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 561
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->myMergedRecentData:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 563
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getRecentProgressAndAchievements()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 564
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getRecentProgressAndAchievements()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    .line 565
    .local v0, "item":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->shouldDisplayAchievements()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 566
    const-string v2, "DGame"

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getMediaItemType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 567
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->myRecent360Data:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 571
    :goto_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->myMergedRecentData:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 569
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->myRecentData:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 573
    :cond_4
    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v3, "Skipped %s"

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->name:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 579
    .end local v0    # "item":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    :cond_5
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadMyRecentProgressAndAchievementsState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto/16 :goto_1

    .line 581
    :cond_6
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->otherRecent360Data:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 582
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->otherRecentData:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 583
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->otherMergedRecentData:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 585
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getRecentProgressAndAchievements()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 586
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getRecentProgressAndAchievements()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    .line 587
    .restart local v0    # "item":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->shouldDisplayAchievements()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 588
    const-string v2, "DGame"

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getMediaItemType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 589
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->otherRecent360Data:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 593
    :goto_5
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->otherMergedRecentData:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 591
    :cond_7
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->otherRecentData:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 595
    :cond_8
    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v3, "Skipped %s"

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, v0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->name:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 601
    .end local v0    # "item":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    :cond_9
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadOtherRecentProgressAndAchievementsState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto/16 :goto_1

    .line 606
    :pswitch_1
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeProfile()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 607
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadMyRecentProgressAndAchievementsState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto/16 :goto_1

    .line 609
    :cond_a
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadOtherRecentProgressAndAchievementsState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto/16 :goto_1

    .line 554
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onLoadTitleDataCompleted(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 628
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;"
    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLoadTitleDataCompleted: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p1, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " titles"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 630
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->myMergedRecentData:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->updateItemList(Ljava/util/List;Ljava/util/List;)V

    .line 631
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->otherMergedRecentData:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->updateItemList(Ljava/util/List;Ljava/util/List;)V

    .line 632
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->myRecentData:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->updateItemList(Ljava/util/List;Ljava/util/List;)V

    .line 633
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->otherRecentData:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->updateItemList(Ljava/util/List;Ljava/util/List;)V

    .line 634
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->myRecent360Data:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->updateItemList(Ljava/util/List;Ljava/util/List;)V

    .line 635
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->otherRecent360Data:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->updateItemList(Ljava/util/List;Ljava/util/List;)V

    .line 638
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->finishLoading()V

    .line 639
    return-void

    .line 628
    :cond_1
    const-string v0, "null"

    goto :goto_0
.end method

.method private updateItemList(Ljava/util/List;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 642
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;>;"
    .local p2, "result":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    .line 643
    .local v0, "item":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->getTitleId()J

    move-result-wide v2

    .line 644
    .local v2, "itemId":J
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;

    .line 645
    .local v1, "title":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    iget-wide v6, v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->titleId:J

    cmp-long v6, v6, v2

    if-eqz v6, :cond_2

    iget-wide v6, v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->modernTitleId:J

    cmp-long v6, v6, v2

    if-nez v6, :cond_1

    .line 646
    :cond_2
    iget-object v5, v1, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;->displayImage:Ljava/lang/String;

    invoke-virtual {v0, v5}, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->setDisplayImage(Ljava/lang/String;)V

    goto :goto_0

    .line 651
    .end local v0    # "item":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    .end local v1    # "title":Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
    .end local v2    # "itemId":J
    :cond_3
    return-void
.end method


# virtual methods
.method public addPersonToSelected(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V
    .locals 0
    .param p1, "p"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .prologue
    .line 402
    return-void
.end method

.method public addPersonToUnSelected(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V
    .locals 0
    .param p1, "p"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .prologue
    .line 412
    return-void
.end method

.method public forceRefresh()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenAdapter;->invalidateData()V

    .line 139
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->forceRefresh()V

    .line 140
    return-void
.end method

.method public getAchievementsFilter()Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->achievementsFilter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->achievementsFilter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;->MOST_RECENT:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;

    goto :goto_0
.end method

.method public getFilteredResults()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->getAchievementsFilter()Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;

    move-result-object v0

    .line 159
    .local v0, "peopleHubAchievementsFilter":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;
    sget-object v1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$xle$app$peoplehub$PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 162
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->compareMergedData:Ljava/util/List;

    .line 166
    :goto_0
    return-object v1

    .line 164
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->compare360Data:Ljava/util/List;

    goto :goto_0

    .line 166
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->compareData:Ljava/util/List;

    goto :goto_0

    .line 159
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getGamerscoreLeaderboardViewModel()Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->gamerscoreLeaderboardViewModel:Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    return-object v0
.end method

.method public getMeGamerscore()Ljava/lang/String;
    .locals 2

    .prologue
    .line 222
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 223
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    .line 224
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerScore()Ljava/lang/String;

    move-result-object v1

    .line 227
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMeGamerscoreChange()J
    .locals 2

    .prologue
    .line 252
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->meGamerscoreChange:J

    return-wide v0
.end method

.method public getMeImageUrl()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 204
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 205
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    .line 206
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerPicImageUrl()Ljava/lang/String;

    move-result-object v1

    .line 209
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getOtherGamerTag()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 195
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerTag()Ljava/lang/String;

    move-result-object v0

    .line 199
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOtherGamerscore()Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerScore()Ljava/lang/String;

    move-result-object v0

    .line 235
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOtherGamerscoreChange()J
    .locals 2

    .prologue
    .line 256
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->otherGamerscoreChange:J

    return-wide v0
.end method

.method public getOtherImageUrl()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 214
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerPicImageUrl()Ljava/lang/String;

    move-result-object v0

    .line 218
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOtherXuid()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 240
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v0

    .line 244
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPeopleSelectedList()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 421
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 422
    .local v3, "peopleSelectorItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;>;"
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    .line 424
    .local v1, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isBusy()Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getFollowingData()Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 425
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getFollowingData()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 426
    .local v0, "f":Lcom/microsoft/xbox/service/model/FollowersData;
    new-instance v2, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;-><init>()V

    .line 427
    .local v2, "p":Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamerName()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;->displayName:Ljava/lang/String;

    .line 428
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamerPicUrl()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;->displayPicRaw:Ljava/lang/String;

    .line 429
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamertag()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;->gamertag:Ljava/lang/String;

    .line 430
    iget-boolean v5, v0, Lcom/microsoft/xbox/service/model/FollowersData;->isFavorite:Z

    iput-boolean v5, v2, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;->isFavorite:Z

    .line 431
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamerRealName()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;->realName:Ljava/lang/String;

    .line 432
    iget-object v5, v0, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    iput-object v5, v2, Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;->xuid:Ljava/lang/String;

    .line 433
    new-instance v5, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    invoke-direct {v5, v2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;-><init>(Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 437
    .end local v0    # "f":Lcom/microsoft/xbox/service/model/FollowersData;
    .end local v2    # "p":Lcom/microsoft/xbox/service/network/managers/FollowingSummaryResult$People;
    :cond_0
    return-object v3
.end method

.method public getProfileColor()I
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v0

    return v0
.end method

.method public getSelectedPerson()Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->friendsSelected:Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 261
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isLoadingMyRecentProgressAndAchievement:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isLoadingOtherRecentProgressAndAchievement:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->isBusy()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMeProfile()Z
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeProfile()Z

    move-result v0

    return v0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 287
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->load(Z)V

    .line 289
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadMyRecentProgressAndAchievementAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadMyRecentProgressAndAchievementAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->cancel()V

    .line 293
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadOtherRecentProgressAndAchievementAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;

    if-eqz v0, :cond_1

    .line 294
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadOtherRecentProgressAndAchievementAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->cancel()V

    .line 297
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadGamerscoreComparisonAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadGamerscoreComparisonAsyncTask;

    if-eqz v0, :cond_2

    .line 298
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadGamerscoreComparisonAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadGamerscoreComparisonAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadGamerscoreComparisonAsyncTask;->cancel()V

    .line 301
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadMyRecentProgressAndAchievementsState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 302
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;

    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;Lcom/microsoft/xbox/service/model/ProfileModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadMyRecentProgressAndAchievementAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;

    .line 303
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadMyRecentProgressAndAchievementAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->load(Z)V

    .line 305
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isMeProfile()Z

    move-result v0

    if-nez v0, :cond_3

    .line 306
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadOtherRecentProgressAndAchievementsState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 307
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;Lcom/microsoft/xbox/service/model/ProfileModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadOtherRecentProgressAndAchievementAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;

    .line 308
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadOtherRecentProgressAndAchievementAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->load(Z)V

    .line 310
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadGamerscoreComparisonAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadGamerscoreComparisonAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadGamerscoreComparisonAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadGamerscoreComparisonAsyncTask;

    .line 311
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadGamerscoreComparisonAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadGamerscoreComparisonAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadGamerscoreComparisonAsyncTask;->load(Z)V

    .line 313
    :cond_3
    return-void
.end method

.method public navigateToComparison(Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;)V
    .locals 4
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    .prologue
    .line 367
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->getTitleId()J

    move-result-wide v0

    .line 368
    .local v0, "titleId":J
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedTitleId(J)V

    .line 369
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setSelectedXuid(Ljava/lang/String;)V

    .line 370
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->trackCompareGameAction(Ljava/lang/String;)V

    .line 372
    const-class v2, Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->NavigateTo(Ljava/lang/Class;)V

    .line 373
    return-void
.end method

.method public navigateToGameProgressPivot(Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;)V
    .locals 7
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    .prologue
    .line 334
    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 335
    .local v2, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v4

    const-class v5, Lcom/microsoft/xbox/xle/app/activity/GameProfilePivotScreen;

    const-class v6, Lcom/microsoft/xbox/xle/app/activity/GameProfileAchievementsScreen;

    invoke-virtual {v4, v5, v6}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setActivePivotPane(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 337
    instance-of v4, p1, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    if-eqz v4, :cond_2

    move-object v3, p1

    .line 338
    check-cast v3, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    .line 339
    .local v3, "rpi":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    const-string v4, "DGame"

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getTitleType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 340
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getEDSV2MediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedMediaItemData(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 355
    .end local v3    # "rpi":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    .end local p1    # "item":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    :cond_0
    :goto_0
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v4

    const-class v5, Lcom/microsoft/xbox/xle/app/activity/GameProfilePivotScreen;

    invoke-virtual {v4, v5, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GotoScreenWithPush(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 359
    :goto_1
    return-void

    .line 342
    .restart local v3    # "rpi":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    .restart local p1    # "item":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    :cond_1
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 343
    .local v1, "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-wide v4, v3, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->titleId:J

    invoke-virtual {v1, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setNowPlayingTitleId(J)V

    .line 344
    iget-object v4, v3, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->name:Ljava/lang/String;

    iput-object v4, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Name:Ljava/lang/String;

    .line 345
    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedMediaItemData(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto :goto_0

    .line 347
    .end local v1    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .end local v3    # "rpi":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    :cond_2
    instance-of v4, p1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;

    if-eqz v4, :cond_0

    .line 348
    new-instance v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 349
    .restart local v1    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->getTitleId()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setNowPlayingTitleId(J)V

    .line 350
    check-cast p1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;

    .end local p1    # "item":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    iget-object v4, p1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->name:Ljava/lang/String;

    iput-object v4, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Name:Ljava/lang/String;

    .line 351
    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedMediaItemData(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    goto :goto_0

    .line 356
    .end local v1    # "mediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :catch_0
    move-exception v0

    .line 357
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v4, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v5, "Could not navigate to game hub: "

    invoke-static {v4, v5, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public navigateToGameProgressPivotOrToComparison(Landroid/util/Pair;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            "Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 322
    .local p1, "itemPair":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;>;"
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    .line 323
    .local v0, "meItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isMeProfile()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 324
    if-eqz v0, :cond_0

    .line 325
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->navigateToGameProgressPivot(Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;)V

    .line 331
    .end local v0    # "meItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    :cond_0
    :goto_0
    return-void

    .line 328
    .restart local v0    # "meItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    :cond_1
    iget-object v1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    .line 329
    .local v1, "otherItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    if-eqz v0, :cond_2

    .end local v0    # "meItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    :goto_1
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->navigateToComparison(Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;)V

    goto :goto_0

    .restart local v0    # "meItem":Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;
    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public navigateToPeopleSelectorActivity()V
    .locals 4

    .prologue
    .line 362
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->friendsSelected:Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .line 363
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    const/4 v1, 0x1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0705bc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p0, v1, v2}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showPeoplePickerDialog(Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;ILjava/lang/String;)V

    .line 364
    return-void
.end method

.method protected onChildViewModelChanged(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
    .locals 0
    .param p1, "child"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 317
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->onChildViewModelChanged(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 318
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->updateAdapter()V

    .line 319
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 144
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->onRehydrate()V

    .line 145
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getPeopleHubAchievementsAdapter(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 146
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 266
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->onStopOverride()V

    .line 268
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadMyRecentProgressAndAchievementAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadMyRecentProgressAndAchievementAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->cancel()V

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadOtherRecentProgressAndAchievementAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;

    if-eqz v0, :cond_1

    .line 273
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadOtherRecentProgressAndAchievementAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadRecentProgressAndAchievementAsyncTask;->cancel()V

    .line 276
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadTitleDataAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadTitleDataAsyncTask;

    if-eqz v0, :cond_2

    .line 277
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadTitleDataAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadTitleDataAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadTitleDataAsyncTask;->cancel()V

    .line 280
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadGamerscoreComparisonAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadGamerscoreComparisonAsyncTask;

    if-eqz v0, :cond_3

    .line 281
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->loadGamerscoreComparisonAsyncTask:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadGamerscoreComparisonAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$LoadGamerscoreComparisonAsyncTask;->cancel()V

    .line 283
    :cond_3
    return-void
.end method

.method public removePersonFromSelected(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V
    .locals 0
    .param p1, "p"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .prologue
    .line 407
    return-void
.end method

.method public removePersonFromUnselected(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V
    .locals 0
    .param p1, "p"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .prologue
    .line 417
    return-void
.end method

.method public selectionActivityCompleted(Z)V
    .locals 5
    .param p1, "accepted"    # Z

    .prologue
    .line 387
    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->friendsSelected:Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    if-eqz v2, :cond_0

    .line 388
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 389
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->friendsSelected:Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    iget-object v2, v2, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->xuid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedProfile(Ljava/lang/String;)V

    .line 390
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    const-class v4, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreen;

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->setActivePivotPane(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 392
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    invoke-virtual {v2, v3, v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->putScreenOnHomeScreen(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 397
    .end local v1    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_0
    :goto_0
    return-void

    .line 393
    .restart local v1    # "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :catch_0
    move-exception v0

    .line 394
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v3, "Failed to navigate to other achievement profile"

    invoke-static {v2, v3, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public setAchievementsFilter(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;)V
    .locals 2
    .param p1, "filter"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;

    .prologue
    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->achievementsFilter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;

    if-ne p1, v0, :cond_0

    .line 187
    :goto_0
    return-void

    .line 179
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->achievementsFilter:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;

    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_2

    .line 183
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->getFilteredResults()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 186
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->updateAdapter()V

    goto :goto_0

    .line 183
    :cond_3
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1
.end method

.method public setSelectedPerson(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V
    .locals 0
    .param p1, "p"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .prologue
    .line 382
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->friendsSelected:Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .line 383
    return-void
.end method
