.class public Lcom/microsoft/xbox/xle/epg/EpgConnector;
.super Ljava/lang/Object;
.source "EpgConnector.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;
.implements Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionStateListener;
.implements Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IChannelEstablishedListener;
.implements Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleMessageListener;


# instance fields
.field private channelListenerAdded:Z

.field private fragmentManager:Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;

.field private listener:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$IConnectionListener;

.field private messageTarget:Lcom/microsoft/xbox/smartglass/MessageTarget;

.field private state:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

.field private titleMessageListenerAdded:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object v3, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->listener:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$IConnectionListener;

    .line 22
    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->DISCONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->state:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    .line 29
    new-instance v1, Lcom/microsoft/xbox/smartglass/MessageTarget;

    sget-object v2, Lcom/microsoft/xbox/smartglass/ServiceChannel;->SystemInputTVRemote:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/smartglass/MessageTarget;-><init>(Lcom/microsoft/xbox/smartglass/ServiceChannel;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->messageTarget:Lcom/microsoft/xbox/smartglass/MessageTarget;

    .line 30
    new-instance v1, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;

    invoke-direct {v1}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->fragmentManager:Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;

    .line 32
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v0

    .line 34
    .local v0, "session":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->addCompanionSessionStateListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionStateListener;)V

    .line 35
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getCurrentSessionState()I

    move-result v1

    invoke-virtual {p0, v1, v3}, Lcom/microsoft/xbox/xle/epg/EpgConnector;->onSessionStateChanged(ILcom/microsoft/xbox/toolkit/XLEException;)V

    .line 36
    return-void
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 52
    return-void
.end method

.method public getLastError()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    return-object v0
.end method

.method public getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->state:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    return-object v0
.end method

.method public onChannelEstablished(Lcom/microsoft/xbox/smartglass/MessageTarget;Lcom/microsoft/xbox/smartglass/SGResult;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/smartglass/MessageTarget;
    .param p2, "result"    # Lcom/microsoft/xbox/smartglass/SGResult;

    .prologue
    .line 117
    const-string v0, "EPGConnector"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received onChannelEstablished: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/microsoft/xbox/smartglass/SGResult;->error:Lcom/microsoft/xbox/smartglass/SGError;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    iget-object v0, p2, Lcom/microsoft/xbox/smartglass/SGResult;->error:Lcom/microsoft/xbox/smartglass/SGError;

    sget-object v1, Lcom/microsoft/xbox/smartglass/SGError;->Success:Lcom/microsoft/xbox/smartglass/SGError;

    if-eq v0, v1, :cond_0

    iget-object v0, p2, Lcom/microsoft/xbox/smartglass/SGResult;->error:Lcom/microsoft/xbox/smartglass/SGError;

    sget-object v1, Lcom/microsoft/xbox/smartglass/SGError;->SuccessFalse:Lcom/microsoft/xbox/smartglass/SGError;

    if-ne v0, v1, :cond_3

    .line 120
    :cond_0
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/microsoft/xbox/smartglass/MessageTarget;->isValid()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/microsoft/xbox/smartglass/MessageTarget;->isService()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/microsoft/xbox/smartglass/MessageTarget;->service:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    sget-object v1, Lcom/microsoft/xbox/smartglass/ServiceChannel;->SystemInputTVRemote:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    if-ne v0, v1, :cond_3

    .line 122
    sget-object v0, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->state:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    .line 123
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->titleMessageListenerAdded:Z

    if-nez v0, :cond_1

    .line 124
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->addTitleMessageListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleMessageListener;)V

    .line 125
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->titleMessageListenerAdded:Z

    .line 127
    :cond_1
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->channelListenerAdded:Z

    if-eqz v0, :cond_2

    .line 128
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->removeChannelEstablishedListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IChannelEstablishedListener;)V

    .line 129
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->channelListenerAdded:Z

    .line 132
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->listener:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$IConnectionListener;

    if-eqz v0, :cond_3

    .line 133
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->listener:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$IConnectionListener;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->state:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$IConnectionListener;->onStateChanged(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;)V

    .line 137
    :cond_3
    return-void
.end method

.method public onSessionStateChanged(ILcom/microsoft/xbox/toolkit/XLEException;)V
    .locals 4
    .param p1, "newSessionState"    # I
    .param p2, "exception"    # Lcom/microsoft/xbox/toolkit/XLEException;

    .prologue
    const/4 v3, 0x0

    .line 77
    const-string v0, "EPGConnector"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received session change: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    packed-switch p1, :pswitch_data_0

    .line 110
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->listener:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$IConnectionListener;

    if-eqz v0, :cond_1

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->listener:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$IConnectionListener;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->state:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$IConnectionListener;->onStateChanged(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;)V

    .line 113
    :cond_1
    return-void

    .line 81
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTING:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->state:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    goto :goto_0

    .line 85
    :pswitch_1
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->channelListenerAdded:Z

    if-nez v0, :cond_0

    .line 86
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->addChannelEstablishedListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IChannelEstablishedListener;)V

    .line 87
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->messageTarget:Lcom/microsoft/xbox/smartglass/MessageTarget;

    invoke-virtual {v0, v1, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->startChannel(Lcom/microsoft/xbox/smartglass/MessageTarget;I)V

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->channelListenerAdded:Z

    goto :goto_0

    .line 94
    :pswitch_2
    sget-object v0, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->DISCONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->state:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    .line 95
    if-nez p1, :cond_0

    .line 96
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->channelListenerAdded:Z

    if-eqz v0, :cond_2

    .line 97
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->removeChannelEstablishedListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IChannelEstablishedListener;)V

    .line 98
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->channelListenerAdded:Z

    .line 100
    :cond_2
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->titleMessageListenerAdded:Z

    if-eqz v0, :cond_0

    .line 101
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->removeTitleMessageListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleMessageListener;)V

    .line 102
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->titleMessageListenerAdded:Z

    goto :goto_0

    .line 79
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public onTitleMessage(Lcom/microsoft/xbox/smartglass/Message;)V
    .locals 8
    .param p1, "message"    # Lcom/microsoft/xbox/smartglass/Message;

    .prologue
    .line 141
    const-string v5, "EPGConnector"

    const-string v6, "Received onTitleMessage"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    iget-object v5, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->listener:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$IConnectionListener;

    if-eqz v5, :cond_0

    iget-object v5, p1, Lcom/microsoft/xbox/smartglass/Message;->type:Lcom/microsoft/xbox/smartglass/MessageType;

    sget-object v6, Lcom/microsoft/xbox/smartglass/MessageType;->Json:Lcom/microsoft/xbox/smartglass/MessageType;

    if-ne v5, v6, :cond_0

    .line 143
    iget-object v5, p1, Lcom/microsoft/xbox/smartglass/Message;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    if-eqz v5, :cond_0

    iget-object v5, p1, Lcom/microsoft/xbox/smartglass/Message;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    invoke-virtual {v5}, Lcom/microsoft/xbox/smartglass/MessageTarget;->isService()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p1, Lcom/microsoft/xbox/smartglass/Message;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    iget-object v5, v5, Lcom/microsoft/xbox/smartglass/MessageTarget;->service:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    sget-object v6, Lcom/microsoft/xbox/smartglass/ServiceChannel;->SystemInputTVRemote:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    if-ne v5, v6, :cond_0

    move-object v3, p1

    .line 144
    check-cast v3, Lcom/microsoft/xbox/smartglass/JsonMessage;

    .line 145
    .local v3, "jsonMessage":Lcom/microsoft/xbox/smartglass/JsonMessage;
    iget-object v4, v3, Lcom/microsoft/xbox/smartglass/JsonMessage;->text:Ljava/lang/String;

    .line 148
    .local v4, "jsonMessageText":Ljava/lang/String;
    const-string v0, "datagram_id"

    .line 149
    .local v0, "DatagramIdKey":Ljava/lang/String;
    const-string v5, "datagram_id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 150
    invoke-static {v4}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;->getFragmentMsgFromJson(Ljava/lang/String;)Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;

    move-result-object v2

    .line 151
    .local v2, "fragmentMessage":Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;
    if-eqz v2, :cond_1

    .line 152
    iget-object v5, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->fragmentManager:Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;

    invoke-virtual {v5, v2}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;->TryGetDatagram(Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;)Ljava/lang/String;

    move-result-object v1

    .line 153
    .local v1, "datagram":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 154
    const-string v5, "EPGConnector"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Message Received: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    iget-object v5, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->listener:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$IConnectionListener;

    invoke-interface {v5, v1}, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$IConnectionListener;->onMessageReceived(Ljava/lang/String;)V

    .line 167
    .end local v0    # "DatagramIdKey":Ljava/lang/String;
    .end local v1    # "datagram":Ljava/lang/String;
    .end local v2    # "fragmentMessage":Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;
    .end local v3    # "jsonMessage":Lcom/microsoft/xbox/smartglass/JsonMessage;
    .end local v4    # "jsonMessageText":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 158
    .restart local v0    # "DatagramIdKey":Ljava/lang/String;
    .restart local v2    # "fragmentMessage":Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;
    .restart local v3    # "jsonMessage":Lcom/microsoft/xbox/smartglass/JsonMessage;
    .restart local v4    # "jsonMessageText":Ljava/lang/String;
    :cond_1
    const-string v5, "EPGConnector"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Message Received: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    iget-object v5, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->listener:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$IConnectionListener;

    invoke-interface {v5, v4}, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$IConnectionListener;->onMessageReceived(Ljava/lang/String;)V

    goto :goto_0

    .line 162
    .end local v2    # "fragmentMessage":Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentMsg;
    :cond_2
    const-string v5, "EPGConnector"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Message Received: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    iget-object v5, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->listener:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$IConnectionListener;

    invoke-interface {v5, v4}, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$IConnectionListener;->onMessageReceived(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public open()Z
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    return v0
.end method

.method public sendMessage(Ljava/lang/String;)Z
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 66
    const-string v0, "EPGConnector"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received sendMessage "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->state:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->state:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-ne v0, v1, :cond_0

    .line 68
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->messageTarget:Lcom/microsoft/xbox/smartglass/MessageTarget;

    invoke-virtual {v0, v1, p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->sendTitleMessage(Lcom/microsoft/xbox/smartglass/MessageTarget;Ljava/lang/String;)V

    .line 69
    const/4 v0, 0x1

    .line 71
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setListener(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$IConnectionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$IConnectionListener;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/EpgConnector;->listener:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$IConnectionListener;

    .line 41
    return-void
.end method
