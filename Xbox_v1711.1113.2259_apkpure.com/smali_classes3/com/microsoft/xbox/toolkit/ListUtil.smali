.class public final Lcom/microsoft/xbox/toolkit/ListUtil;
.super Ljava/lang/Object;
.source "ListUtil.java"


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No instances of ListUtil"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public static batchAction(Ljava/util/List;ILcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 8
    .param p0    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "increment"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p2    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;I",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/List",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 77
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .local p2, "action":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Ljava/util/List<TT;>;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 78
    const-wide/16 v4, 0x1

    int-to-long v6, p1

    invoke-static {v4, v5, v6, v7}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 79
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 81
    const/4 v1, 0x0

    .line 83
    .local v1, "batchStart":I
    const/4 v2, 0x1

    .line 85
    .local v2, "moreToDo":Z
    :goto_0
    if-eqz v2, :cond_1

    .line 86
    add-int v0, v1, p1

    .line 88
    .local v0, "batchEnd":I
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    if-lt v0, v3, :cond_0

    .line 89
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    .line 90
    const/4 v2, 0x0

    .line 93
    :cond_0
    invoke-interface {p0, v1, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    invoke-interface {p2, v3}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 95
    move v1, v0

    goto :goto_0

    .line 97
    .end local v0    # "batchEnd":I
    :cond_1
    return-void
.end method

.method public static countIf(Ljava/util/List;Lcom/microsoft/xbox/toolkit/java8/Predicate;)I
    .locals 4
    .param p0    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Lcom/microsoft/xbox/toolkit/java8/Predicate;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "Lcom/microsoft/xbox/toolkit/java8/Predicate",
            "<TT;>;)I"
        }
    .end annotation

    .prologue
    .line 50
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .local p1, "countIfTrue":Lcom/microsoft/xbox/toolkit/java8/Predicate;, "Lcom/microsoft/xbox/toolkit/java8/Predicate<TT;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 51
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 52
    const/4 v0, 0x0

    .line 53
    .local v0, "count":I
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 54
    .local v1, "element":Ljava/lang/Object;, "TT;"
    invoke-interface {p1, v1}, Lcom/microsoft/xbox/toolkit/java8/Predicate;->test(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 55
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 58
    .end local v1    # "element":Ljava/lang/Object;, "TT;"
    :cond_1
    return v0
.end method

.method public static filter(Ljava/util/List;Lcom/microsoft/xbox/toolkit/java8/Predicate;)Ljava/util/List;
    .locals 4
    .param p0    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Lcom/microsoft/xbox/toolkit/java8/Predicate;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "Lcom/microsoft/xbox/toolkit/java8/Predicate",
            "<TT;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "unfiltered":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .local p1, "retainIfTrue":Lcom/microsoft/xbox/toolkit/java8/Predicate;, "Lcom/microsoft/xbox/toolkit/java8/Predicate<TT;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 32
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 33
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 34
    .local v1, "filtered":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 35
    .local v0, "element":Ljava/lang/Object;, "TT;"
    invoke-interface {p1, v0}, Lcom/microsoft/xbox/toolkit/java8/Predicate;->test(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 36
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 39
    .end local v0    # "element":Ljava/lang/Object;, "TT;"
    :cond_1
    return-object v1
.end method

.method public static moveElement(Ljava/util/List;II)V
    .locals 8
    .param p0    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "from"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .param p2, "to"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;II)V"
        }
    .end annotation

    .prologue
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    const-wide/16 v0, 0x0

    .line 131
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 132
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    int-to-long v2, v2

    int-to-long v4, p1

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRange(JJJ)V

    .line 133
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    int-to-long v2, v2

    int-to-long v4, p2

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRange(JJJ)V

    .line 137
    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    .line 138
    .local v6, "dataFrom":Ljava/lang/Object;, "TT;"
    if-ge p1, p2, :cond_0

    .line 139
    move v7, p1

    .local v7, "i":I
    :goto_0
    if-ge v7, p2, :cond_1

    .line 140
    add-int/lit8 v0, v7, 0x1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p0, v7, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 139
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 143
    .end local v7    # "i":I
    :cond_0
    move v7, p1

    .restart local v7    # "i":I
    :goto_1
    if-le v7, p2, :cond_1

    .line 144
    add-int/lit8 v0, v7, -0x1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p0, v7, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 143
    add-int/lit8 v7, v7, -0x1

    goto :goto_1

    .line 147
    :cond_1
    invoke-interface {p0, p2, v6}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 148
    return-void
.end method

.method public static nullToEmpty(Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 66
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    if-nez p0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p0

    .end local p0    # "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    :cond_0
    return-object p0
.end method

.method public static unsortedEqual(Ljava/util/Collection;Ljava/util/Collection;)Z
    .locals 4
    .param p0    # Ljava/util/Collection;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Ljava/util/Collection;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<TT;>;",
            "Ljava/util/Collection",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 108
    .local p0, "a":Ljava/util/Collection;, "Ljava/util/Collection<TT;>;"
    .local p1, "b":Ljava/util/Collection;, "Ljava/util/Collection<TT;>;"
    if-nez p0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    if-ne p0, p1, :cond_2

    .line 109
    :cond_1
    const/4 v2, 0x1

    .line 117
    :goto_0
    return v2

    .line 110
    :cond_2
    if-eqz p0, :cond_3

    if-eqz p1, :cond_3

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v3

    if-ne v2, v3, :cond_3

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 112
    .local v0, "aCopy":Ljava/util/List;, "Ljava/util/List<TT;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 114
    .local v1, "bCopy":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-interface {v0, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 115
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    goto :goto_0

    .line 117
    .end local v0    # "aCopy":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .end local v1    # "bCopy":Ljava/util/List;, "Ljava/util/List<TT;>;"
    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method
