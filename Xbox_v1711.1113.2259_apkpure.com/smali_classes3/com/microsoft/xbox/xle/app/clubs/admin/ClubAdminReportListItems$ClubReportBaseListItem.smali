.class public abstract Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
.super Ljava/lang/Object;
.source "ClubAdminReportListItems.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ClubReportBaseListItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;",
        ">;"
    }
.end annotation


# instance fields
.field protected creator:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private isCreatorMe:Z

.field private isCreatorMod:Z

.field private isCreatorOwner:Z

.field private isLastReporterMe:Z

.field private isLastReporterMod:Z

.field private isLastReporterOwner:Z

.field private lastReport:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private final reportedItem:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

.field private reporter:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;)V
    .locals 0
    .param p1, "reportedItem"    # Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 50
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->reportedItem:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    .line 51
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)I
    .locals 2
    .param p1, "another"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    .prologue
    .line 268
    if-nez p1, :cond_0

    .line 269
    const/4 v0, -0x1

    .line 271
    :goto_0
    return v0

    :cond_0
    iget-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->reportedItem:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->lastReported()Ljava/util/Date;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->reportedItem:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->lastReported()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 29
    check-cast p1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->compareTo(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 251
    if-ne p1, p0, :cond_0

    .line 252
    const/4 v1, 0x1

    .line 257
    :goto_0
    return v1

    .line 253
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    if-nez v1, :cond_1

    .line 254
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 256
    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    .line 257
    .local v0, "other":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->reportedItem:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->reportedItem:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getAgoText()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->reportedItem:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->lastReported()Ljava/util/Date;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->shortDateUptoMonthToDurationNow(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBanCreatorText()Ljava/lang/String;
    .locals 5
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 215
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->creator:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    if-eqz v0, :cond_0

    .line 216
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070274

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->creator:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v4, v4, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayName:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 218
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getBanReporterText()Ljava/lang/String;
    .locals 5
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 233
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->reporter:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    if-eqz v0, :cond_0

    .line 234
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070274

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->reporter:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v4, v4, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayName:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 236
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getContentId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 178
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->reportedItem:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->contentId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCreator()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->creator:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    return-object v0
.end method

.method public getCreatorGamertag()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->creator:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->creator:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayName:Ljava/lang/String;

    const-string v1, ""

    .line 88
    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 87
    :goto_0
    return-object v0

    .line 88
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public abstract getCreatorXuid()Ljava/lang/Long;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public getFormattedCount()Ljava/lang/String;
    .locals 6
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 206
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->reportedItem:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->reports()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v0

    .line 208
    .local v0, "count":I
    if-eq v0, v3, :cond_0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070271

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 209
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 208
    :goto_0
    return-object v1

    .line 209
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070272

    .line 210
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getIsCreatorMe()Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->isCreatorMe:Z

    return v0
.end method

.method public getIsCreatorModerator()Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->isCreatorMod:Z

    return v0
.end method

.method public getIsCreatorOwner()Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->isCreatorOwner:Z

    return v0
.end method

.method public getIsLastReportedOwner()Z
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->isLastReporterOwner:Z

    return v0
.end method

.method public getIsLastReporterMe()Z
    .locals 1

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->isLastReporterMe:Z

    return v0
.end method

.method public getIsLastReporterModerator()Z
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->isLastReporterMod:Z

    return v0
.end method

.method public getLastReport()Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 152
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->lastReport:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;

    return-object v0
.end method

.method public getMessageCreatorText()Ljava/lang/String;
    .locals 5
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 224
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->creator:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    if-eqz v0, :cond_0

    .line 225
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070279

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->creator:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v4, v4, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayName:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 227
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getMessageReporterText()Ljava/lang/String;
    .locals 5
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 242
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->reporter:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    if-eqz v0, :cond_0

    .line 243
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070279

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->reporter:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v4, v4, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayName:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 245
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getReason()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 199
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->lastReport:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->lastReport:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;

    .line 200
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;->textReason()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 199
    :goto_0
    return-object v0

    .line 200
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getRelatedPeopleXuids()Ljava/util/Set;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 159
    .local v1, "xuids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->getCreatorXuid()Ljava/lang/Long;

    move-result-object v0

    .line 161
    .local v0, "creatorXuid":Ljava/lang/Long;
    if-eqz v0, :cond_0

    .line 162
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 165
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->lastReport:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;

    if-eqz v2, :cond_1

    .line 166
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->lastReport:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;->reportingXuid()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 169
    :cond_1
    return-object v1
.end method

.method public getReportId()J
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->reportedItem:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->reportId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getReporter()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->reporter:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    return-object v0
.end method

.method public getReporterGamertag()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->reporter:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->reporter:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayName:Ljava/lang/String;

    const-string v1, ""

    .line 71
    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 70
    :goto_0
    return-object v0

    .line 71
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getReporterXuid()J
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->lastReport:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->lastReport:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;->reportingXuid()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getReports()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->reportedItem:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;->reports()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 192
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->reporter:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->reporter:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayName:Ljava/lang/String;

    const-string v1, ""

    .line 193
    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 192
    :goto_0
    return-object v0

    .line 193
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public abstract hasValidContent()Z
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->reportedItem:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubReportedItem;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public setCreator(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 0
    .param p1, "creator"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 76
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 77
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->creator:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 78
    return-void
.end method

.method public setIsCreatorMe(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 113
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->isCreatorMe:Z

    .line 114
    return-void
.end method

.method public setIsCreatorMod(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->isCreatorMod:Z

    .line 106
    return-void
.end method

.method public setIsCreatorOwner(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->isCreatorOwner:Z

    .line 98
    return-void
.end method

.method public setIsLastReporterMe(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 137
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->isLastReporterMe:Z

    .line 138
    return-void
.end method

.method public setIsLastReporterMod(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 129
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->isLastReporterMod:Z

    .line 130
    return-void
.end method

.method public setIsLastReporterOwner(Z)V
    .locals 0
    .param p1, "val"    # Z

    .prologue
    .line 121
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->isLastReporterOwner:Z

    .line 122
    return-void
.end method

.method public setLastReport(Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;)V
    .locals 0
    .param p1, "report"    # Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 141
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 142
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->lastReport:Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;

    .line 143
    return-void
.end method

.method public setReporter(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 0
    .param p1, "reporter"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 59
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 60
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;->reporter:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 61
    return-void
.end method
