.class public final Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes;
.super Ljava/lang/Object;
.source "ClubHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSearchResponse;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendationReason;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRecommendations;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTargetRoles;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Deeplink;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$TitleDeeplinks;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubResponse;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRosterFilter;,
        Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "This type shouldn\'t be instantiated."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes;->TAG:Ljava/lang/String;

    return-object v0
.end method
