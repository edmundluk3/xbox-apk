.class final Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildAttachStateChangeEventObservable$Listener;
.super Lio/reactivex/android/MainThreadDisposable;
.source "RecyclerViewChildAttachStateChangeEventObservable.java"

# interfaces
.implements Landroid/support/v7/widget/RecyclerView$OnChildAttachStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildAttachStateChangeEventObservable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "Listener"
.end annotation


# instance fields
.field private final observer:Lio/reactivex/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observer",
            "<-",
            "Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildAttachStateChangeEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerView:Landroid/support/v7/widget/RecyclerView;

.field final synthetic this$0:Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildAttachStateChangeEventObservable;


# direct methods
.method constructor <init>(Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildAttachStateChangeEventObservable;Landroid/support/v7/widget/RecyclerView;Lio/reactivex/Observer;)V
    .locals 0
    .param p1, "this$0"    # Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildAttachStateChangeEventObservable;
    .param p2, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v7/widget/RecyclerView;",
            "Lio/reactivex/Observer",
            "<-",
            "Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildAttachStateChangeEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p3, "observer":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildAttachStateChangeEvent;>;"
    iput-object p1, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildAttachStateChangeEventObservable$Listener;->this$0:Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildAttachStateChangeEventObservable;

    invoke-direct {p0}, Lio/reactivex/android/MainThreadDisposable;-><init>()V

    .line 36
    iput-object p2, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildAttachStateChangeEventObservable$Listener;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 37
    iput-object p3, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildAttachStateChangeEventObservable$Listener;->observer:Lio/reactivex/Observer;

    .line 38
    return-void
.end method


# virtual methods
.method public onChildViewAttachedToWindow(Landroid/view/View;)V
    .locals 2
    .param p1, "childView"    # Landroid/view/View;

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildAttachStateChangeEventObservable$Listener;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildAttachStateChangeEventObservable$Listener;->observer:Lio/reactivex/Observer;

    iget-object v1, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildAttachStateChangeEventObservable$Listener;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v1, p1}, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildAttachEvent;->create(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildAttachEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/reactivex/Observer;->onNext(Ljava/lang/Object;)V

    .line 44
    :cond_0
    return-void
.end method

.method public onChildViewDetachedFromWindow(Landroid/view/View;)V
    .locals 2
    .param p1, "childView"    # Landroid/view/View;

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildAttachStateChangeEventObservable$Listener;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildAttachStateChangeEventObservable$Listener;->observer:Lio/reactivex/Observer;

    iget-object v1, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildAttachStateChangeEventObservable$Listener;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v1, p1}, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildDetachEvent;->create(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildDetachEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/reactivex/Observer;->onNext(Ljava/lang/Object;)V

    .line 50
    :cond_0
    return-void
.end method

.method protected onDispose()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewChildAttachStateChangeEventObservable$Listener;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/RecyclerView;->removeOnChildAttachStateChangeListener(Landroid/support/v7/widget/RecyclerView$OnChildAttachStateChangeListener;)V

    .line 54
    return-void
.end method
