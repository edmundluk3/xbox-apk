.class final Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;
.super Ljava/lang/Object;
.source "PeopleScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "FollowersListBuilder"
.end annotation


# instance fields
.field private final resultList:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)V
    .locals 1

    .prologue
    .line 1131
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1132
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->resultList:Ljava/util/Set;

    .line 1133
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$1;

    .prologue
    .line 1127
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    .prologue
    .line 1127
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->onlineFriends()Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    .prologue
    .line 1127
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->recommendations()Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    .prologue
    .line 1127
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->offlineClubs()Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    .prologue
    .line 1127
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->offlineFavorites()Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    .prologue
    .line 1127
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->offlineFriends()Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1500(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    .prologue
    .line 1127
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->build()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    .prologue
    .line 1127
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->discoverClubs()Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;Lcom/microsoft/xbox/service/model/FollowersData$DummyType;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    .prologue
    .line 1127
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->addHeader(Lcom/microsoft/xbox/service/model/FollowersData$DummyType;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    .prologue
    .line 1127
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->onlineClubs()Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    .prologue
    .line 1127
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->onlineFavorites()Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;Lcom/microsoft/xbox/service/model/FollowersData;Lcom/microsoft/xbox/service/model/FollowersData;)I
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->compareClubs(Lcom/microsoft/xbox/service/model/FollowersData;Lcom/microsoft/xbox/service/model/FollowersData;)I

    move-result v0

    return v0
.end method

.method static synthetic access$lambda$1(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;Lcom/microsoft/xbox/service/model/FollowersData;Lcom/microsoft/xbox/service/model/FollowersData;)I
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->compareClubs(Lcom/microsoft/xbox/service/model/FollowersData;Lcom/microsoft/xbox/service/model/FollowersData;)I

    move-result v0

    return v0
.end method

.method private addHeader(Lcom/microsoft/xbox/service/model/FollowersData$DummyType;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;
    .locals 2
    .param p1, "headerType"    # Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    .prologue
    .line 1189
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->resultList:Ljava/util/Set;

    invoke-static {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->dummy(Lcom/microsoft/xbox/service/model/FollowersData$DummyType;)Lcom/microsoft/xbox/service/model/FollowersData;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1190
    return-object p0
.end method

.method private build()Ljava/util/List;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/FollowersData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1195
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->resultList:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method private compareClubs(Lcom/microsoft/xbox/service/model/FollowersData;Lcom/microsoft/xbox/service/model/FollowersData;)I
    .locals 6
    .param p1, "a"    # Lcom/microsoft/xbox/service/model/FollowersData;
    .param p2, "b"    # Lcom/microsoft/xbox/service/model/FollowersData;

    .prologue
    .line 1199
    iget-object v2, p1, Lcom/microsoft/xbox/service/model/FollowersData;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 1200
    iget-object v2, p2, Lcom/microsoft/xbox/service/model/FollowersData;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 1202
    iget-object v2, p2, Lcom/microsoft/xbox/service/model/FollowersData;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceCount()J

    move-result-wide v2

    iget-object v4, p1, Lcom/microsoft/xbox/service/model/FollowersData;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceCount()J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 1203
    .local v0, "presenceDiff":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 1204
    long-to-int v2, v0

    .line 1207
    :goto_0
    return v2

    :cond_0
    iget-object v2, p1, Lcom/microsoft/xbox/service/model/FollowersData;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p2, Lcom/microsoft/xbox/service/model/FollowersData;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    goto :goto_0
.end method

.method private discoverClubs()Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;
    .locals 2

    .prologue
    .line 1146
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getHideClubDiscoveryBlock()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    .line 1147
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->access$2600(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    .line 1148
    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->access$2800(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1149
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->resultList:Ljava/util/Set;

    sget-object v1, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_DISCOVER_CLUBS:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/FollowersData;->dummy(Lcom/microsoft/xbox/service/model/FollowersData$DummyType;)Lcom/microsoft/xbox/service/model/FollowersData;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1152
    :cond_0
    return-object p0
.end method

.method static synthetic lambda$offlineClubs$1(Lcom/microsoft/xbox/service/model/FollowersData;)Z
    .locals 1
    .param p0, "club"    # Lcom/microsoft/xbox/service/model/FollowersData;

    .prologue
    .line 1182
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/FollowersData;->getIsOnline()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$offlineFriends$0(Lcom/microsoft/xbox/service/model/FollowersData;)Z
    .locals 1
    .param p0, "input"    # Lcom/microsoft/xbox/service/model/FollowersData;

    .prologue
    .line 1177
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/FollowersData;->getIsOnline()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private offlineClubs()Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;
    .locals 3

    .prologue
    .line 1182
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->access$2800(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder$$Lambda$5;->lambdaFactory$()Lcom/microsoft/xbox/toolkit/java8/Predicate;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/ListUtil;->filter(Ljava/util/List;Lcom/microsoft/xbox/toolkit/java8/Predicate;)Ljava/util/List;

    move-result-object v0

    .line 1183
    .local v0, "offlineClubList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Ljava/util/Comparator;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1184
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->resultList:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1185
    return-object p0
.end method

.method private offlineFavorites()Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;
    .locals 2

    .prologue
    .line 1172
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->resultList:Ljava/util/Set;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getOfflineFavorites()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1173
    return-object p0
.end method

.method private offlineFriends()Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;
    .locals 3

    .prologue
    .line 1177
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->resultList:Ljava/util/Set;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->followingData:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder$$Lambda$4;->lambdaFactory$()Lcom/microsoft/xbox/toolkit/java8/Predicate;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/ListUtil;->filter(Ljava/util/List;Lcom/microsoft/xbox/toolkit/java8/Predicate;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1178
    return-object p0
.end method

.method private onlineClubs()Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;
    .locals 3

    .prologue
    .line 1156
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->access$2800(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder$$Lambda$2;->lambdaFactory$()Lcom/microsoft/xbox/toolkit/java8/Predicate;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/ListUtil;->filter(Ljava/util/List;Lcom/microsoft/xbox/toolkit/java8/Predicate;)Ljava/util/List;

    move-result-object v0

    .line 1157
    .local v0, "onlineClubList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;)Ljava/util/Comparator;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1158
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->resultList:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1159
    return-object p0
.end method

.method private onlineFavorites()Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;
    .locals 2

    .prologue
    .line 1136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->resultList:Ljava/util/Set;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->getOnlineFavorites()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1137
    return-object p0
.end method

.method private onlineFriends()Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;
    .locals 3

    .prologue
    .line 1141
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->resultList:Ljava/util/Set;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    iget-object v1, v1, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->followingData:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder$$Lambda$1;->lambdaFactory$()Lcom/microsoft/xbox/toolkit/java8/Predicate;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/ListUtil;->filter(Ljava/util/List;Lcom/microsoft/xbox/toolkit/java8/Predicate;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1142
    return-object p0
.end method

.method private recommendations()Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;
    .locals 2

    .prologue
    .line 1163
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->access$2900(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1164
    sget-object v0, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->DUMMY_VIPS:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->addHeader(Lcom/microsoft/xbox/service/model/FollowersData$DummyType;)Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;

    .line 1165
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->resultList:Ljava/util/Set;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel$FollowersListBuilder;->this$0:Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;->access$2900(Lcom/microsoft/xbox/xle/viewmodel/PeopleScreenViewModel;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1168
    :cond_0
    return-object p0
.end method
