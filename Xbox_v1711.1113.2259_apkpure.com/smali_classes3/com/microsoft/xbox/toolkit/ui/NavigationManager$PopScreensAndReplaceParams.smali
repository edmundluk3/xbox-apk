.class final Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;
.super Ljava/lang/Object;
.source "NavigationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PopScreensAndReplaceParams"
.end annotation


# instance fields
.field private final animate:Z

.field private final goingBack:Z

.field private final isRestart:Z

.field private final newScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

.field private final popCount:I

.field private final screenParameters:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;ILcom/microsoft/xbox/toolkit/ui/ScreenLayout;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    .locals 0
    .param p2, "popCount"    # I
    .param p3, "newScreen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .param p4, "animate"    # Z
    .param p5, "goingBack"    # Z
    .param p6, "isRestart"    # Z
    .param p7, "screenParameters"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    .prologue
    .line 979
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;->this$0:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 980
    iput p2, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;->popCount:I

    .line 981
    iput-object p3, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;->newScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 982
    iput-boolean p4, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;->animate:Z

    .line 983
    iput-boolean p5, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;->goingBack:Z

    .line 984
    iput-boolean p6, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;->isRestart:Z

    .line 985
    iput-object p7, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;->screenParameters:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    .line 986
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;ILcom/microsoft/xbox/toolkit/ui/ScreenLayout;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;Lcom/microsoft/xbox/toolkit/ui/NavigationManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager;
    .param p2, "x1"    # I
    .param p3, "x2"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .param p4, "x3"    # Z
    .param p5, "x4"    # Z
    .param p6, "x5"    # Z
    .param p7, "x6"    # Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .param p8, "x7"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager$1;

    .prologue
    .line 965
    invoke-direct/range {p0 .. p7}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;-><init>(Lcom/microsoft/xbox/toolkit/ui/NavigationManager;ILcom/microsoft/xbox/toolkit/ui/ScreenLayout;ZZZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;

    .prologue
    .line 965
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;->popCount:I

    return v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;

    .prologue
    .line 965
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;->newScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;

    .prologue
    .line 965
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;->animate:Z

    return v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;

    .prologue
    .line 965
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;->goingBack:Z

    return v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;

    .prologue
    .line 965
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;->isRestart:Z

    return v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;)Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;

    .prologue
    .line 965
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$PopScreensAndReplaceParams;->screenParameters:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    return-object v0
.end method
