.class final enum Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;
.super Ljava/lang/Enum;
.source "SuggestionsPeopleListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ViewType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

.field public static final enum LISTVIEW_ITEMS:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

.field public static final enum SPINNER_FILTER:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

.field public static final enum TEXT_ERROR:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

.field public static final enum TEXT_LOADING:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

.field public static final enum TEXT_NO_CONTENT:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    const-string v1, "SPINNER_FILTER"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;->SPINNER_FILTER:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    .line 41
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    const-string v1, "LISTVIEW_ITEMS"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;->LISTVIEW_ITEMS:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    .line 42
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    const-string v1, "TEXT_LOADING"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;->TEXT_LOADING:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    .line 43
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    const-string v1, "TEXT_NO_CONTENT"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;->TEXT_NO_CONTENT:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    .line 44
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    const-string v1, "TEXT_ERROR"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;->TEXT_ERROR:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    .line 39
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;->SPINNER_FILTER:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;->LISTVIEW_ITEMS:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;->TEXT_LOADING:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;->TEXT_NO_CONTENT:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;->TEXT_ERROR:Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;->$VALUES:[Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 39
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;->$VALUES:[Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/adapter/SuggestionsPeopleListAdapter$ViewType;

    return-object v0
.end method
