.class Lcom/microsoft/playready/DomainHandlingWorker;
.super Ljava/lang/Object;
.source "DomainHandlingWorker.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$microsoft$playready$IDomainHandlingPlugin$DomainOperationType:[I

.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mDRPlugin:Lcom/microsoft/playready/IDomainHandlingPlugin;

.field private mDomainInfo:Lcom/microsoft/playready/DomainInfo;

.field private mDomainOpType:Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

.field private mDomainServerUri:Ljava/lang/String;

.field private mFactory:Lcom/microsoft/playready/PlayReadyFactory;


# direct methods
.method static synthetic $SWITCH_TABLE$com$microsoft$playready$IDomainHandlingPlugin$DomainOperationType()[I
    .locals 3

    .prologue
    .line 7
    sget-object v0, Lcom/microsoft/playready/DomainHandlingWorker;->$SWITCH_TABLE$com$microsoft$playready$IDomainHandlingPlugin$DomainOperationType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;->values()[Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;->joinDomain:Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    invoke-virtual {v1}, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    :try_start_1
    sget-object v1, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;->leaveDomain:Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    invoke-virtual {v1}, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    sput-object v0, Lcom/microsoft/playready/DomainHandlingWorker;->$SWITCH_TABLE$com$microsoft$playready$IDomainHandlingPlugin$DomainOperationType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    const-class v0, Lcom/microsoft/playready/DomainHandlingWorker;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/playready/DomainHandlingWorker;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/microsoft/playready/PlayReadyFactory;Lcom/microsoft/playready/IDomainHandlingPlugin;Lcom/microsoft/playready/DomainInfo;Ljava/lang/String;Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;)V
    .locals 1
    .param p1, "factory"    # Lcom/microsoft/playready/PlayReadyFactory;
    .param p2, "drPlugin"    # Lcom/microsoft/playready/IDomainHandlingPlugin;
    .param p3, "domainInfo"    # Lcom/microsoft/playready/DomainInfo;
    .param p4, "domainServerUri"    # Ljava/lang/String;
    .param p5, "opType"    # Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object v0, p0, Lcom/microsoft/playready/DomainHandlingWorker;->mDRPlugin:Lcom/microsoft/playready/IDomainHandlingPlugin;

    .line 10
    iput-object v0, p0, Lcom/microsoft/playready/DomainHandlingWorker;->mFactory:Lcom/microsoft/playready/PlayReadyFactory;

    .line 11
    iput-object v0, p0, Lcom/microsoft/playready/DomainHandlingWorker;->mDomainInfo:Lcom/microsoft/playready/DomainInfo;

    .line 12
    iput-object v0, p0, Lcom/microsoft/playready/DomainHandlingWorker;->mDomainServerUri:Ljava/lang/String;

    .line 13
    iput-object v0, p0, Lcom/microsoft/playready/DomainHandlingWorker;->mDomainOpType:Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    .line 22
    iput-object p1, p0, Lcom/microsoft/playready/DomainHandlingWorker;->mFactory:Lcom/microsoft/playready/PlayReadyFactory;

    .line 23
    iput-object p2, p0, Lcom/microsoft/playready/DomainHandlingWorker;->mDRPlugin:Lcom/microsoft/playready/IDomainHandlingPlugin;

    .line 24
    iput-object p3, p0, Lcom/microsoft/playready/DomainHandlingWorker;->mDomainInfo:Lcom/microsoft/playready/DomainInfo;

    .line 25
    iput-object p4, p0, Lcom/microsoft/playready/DomainHandlingWorker;->mDomainServerUri:Ljava/lang/String;

    .line 26
    iput-object p5, p0, Lcom/microsoft/playready/DomainHandlingWorker;->mDomainOpType:Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    .line 27
    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/microsoft/playready/DomainHandlingWorker;->call()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 33
    const/4 v0, 0x0

    .line 34
    .local v0, "challengeCustomData":Ljava/lang/String;
    const/4 v1, 0x0

    .line 35
    .local v1, "domainChallengeBytes":[B
    const/4 v2, 0x0

    .line 36
    .local v2, "domainResponse":[B
    const/4 v4, 0x0

    .line 38
    .local v4, "responseCustomData":Ljava/lang/String;
    new-instance v3, Lcom/microsoft/playready/DrmProxy;

    iget-object v5, p0, Lcom/microsoft/playready/DomainHandlingWorker;->mFactory:Lcom/microsoft/playready/PlayReadyFactory;

    invoke-direct {v3, v5}, Lcom/microsoft/playready/DrmProxy;-><init>(Lcom/microsoft/playready/PlayReadyFactory;)V

    .line 40
    .local v3, "drmProxy":Lcom/microsoft/playready/DrmProxy;
    iget-object v5, p0, Lcom/microsoft/playready/DomainHandlingWorker;->mDRPlugin:Lcom/microsoft/playready/IDomainHandlingPlugin;

    invoke-interface {v5}, Lcom/microsoft/playready/IDomainHandlingPlugin;->getChallengeCustomData()Ljava/lang/String;

    move-result-object v0

    .line 42
    invoke-static {}, Lcom/microsoft/playready/DomainHandlingWorker;->$SWITCH_TABLE$com$microsoft$playready$IDomainHandlingPlugin$DomainOperationType()[I

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/playready/DomainHandlingWorker;->mDomainOpType:Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    invoke-virtual {v6}, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 51
    sget-boolean v5, Lcom/microsoft/playready/DomainHandlingWorker;->$assertionsDisabled:Z

    if-nez v5, :cond_0

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 45
    :pswitch_0
    iget-object v5, p0, Lcom/microsoft/playready/DomainHandlingWorker;->mDomainInfo:Lcom/microsoft/playready/DomainInfo;

    invoke-virtual {v3, v5, v0}, Lcom/microsoft/playready/DrmProxy;->generateDomainJoinChallenge(Lcom/microsoft/playready/DomainInfo;Ljava/lang/String;)[B

    move-result-object v1

    .line 54
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/microsoft/playready/DomainHandlingWorker;->mDRPlugin:Lcom/microsoft/playready/IDomainHandlingPlugin;

    .line 56
    iget-object v6, p0, Lcom/microsoft/playready/DomainHandlingWorker;->mDomainServerUri:Ljava/lang/String;

    .line 57
    iget-object v7, p0, Lcom/microsoft/playready/DomainHandlingWorker;->mDomainOpType:Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    .line 54
    invoke-interface {v5, v1, v6, v7}, Lcom/microsoft/playready/IDomainHandlingPlugin;->doDomainOperation([BLjava/lang/String;Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;)[B

    move-result-object v2

    .line 59
    invoke-static {}, Lcom/microsoft/playready/DomainHandlingWorker;->$SWITCH_TABLE$com$microsoft$playready$IDomainHandlingPlugin$DomainOperationType()[I

    move-result-object v5

    iget-object v6, p0, Lcom/microsoft/playready/DomainHandlingWorker;->mDomainOpType:Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    invoke-virtual {v6}, Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_1

    .line 68
    sget-boolean v5, Lcom/microsoft/playready/DomainHandlingWorker;->$assertionsDisabled:Z

    if-nez v5, :cond_1

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 48
    :pswitch_1
    iget-object v5, p0, Lcom/microsoft/playready/DomainHandlingWorker;->mDomainInfo:Lcom/microsoft/playready/DomainInfo;

    invoke-virtual {v3, v5, v0}, Lcom/microsoft/playready/DrmProxy;->generateDomainLeaveChallenge(Lcom/microsoft/playready/DomainInfo;Ljava/lang/String;)[B

    move-result-object v1

    .line 49
    goto :goto_0

    .line 62
    :pswitch_2
    invoke-virtual {v3, v2}, Lcom/microsoft/playready/DrmProxy;->processDomainJoinResponse([B)Ljava/lang/String;

    move-result-object v4

    .line 71
    :cond_1
    :goto_1
    return-object v4

    .line 65
    :pswitch_3
    invoke-virtual {v3, v2}, Lcom/microsoft/playready/DrmProxy;->processDomainLeaveResponse([B)Ljava/lang/String;

    move-result-object v4

    .line 66
    goto :goto_1

    .line 42
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 59
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getDomainOperationType()Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/microsoft/playready/DomainHandlingWorker;->mDomainOpType:Lcom/microsoft/playready/IDomainHandlingPlugin$DomainOperationType;

    return-object v0
.end method
