.class final Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;
.super Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;
.source "AutoValue_OOBEInteractor_EnterCodeAction.java"


# instance fields
.field private final code:Ljava/lang/String;

.field private final email:Ljava/lang/String;

.field private final gamerpic:Ljava/lang/String;

.field private final gamertag:Ljava/lang/String;

.field private final isNewUser:Z


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "code"    # Ljava/lang/String;
    .param p2, "email"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "gamertag"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "gamerpic"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "isNewUser"    # Z

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;-><init>()V

    .line 23
    if-nez p1, :cond_0

    .line 24
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null code"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->code:Ljava/lang/String;

    .line 27
    iput-object p2, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->email:Ljava/lang/String;

    .line 28
    iput-object p3, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->gamertag:Ljava/lang/String;

    .line 29
    iput-object p4, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->gamerpic:Ljava/lang/String;

    .line 30
    iput-boolean p5, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->isNewUser:Z

    .line 31
    return-void
.end method


# virtual methods
.method public code()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->code:Ljava/lang/String;

    return-object v0
.end method

.method public email()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->email:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 75
    if-ne p1, p0, :cond_1

    .line 86
    :cond_0
    :goto_0
    return v1

    .line 78
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;

    if-eqz v3, :cond_6

    move-object v0, p1

    .line 79
    check-cast v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;

    .line 80
    .local v0, "that":Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;
    iget-object v3, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->code:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;->code()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->email:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 81
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;->email()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->gamertag:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 82
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;->gamertag()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->gamerpic:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 83
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;->gamerpic()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-boolean v3, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->isNewUser:Z

    .line 84
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;->isNewUser()Z

    move-result v4

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 81
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->email:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;->email()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 82
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->gamertag:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;->gamertag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 83
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->gamerpic:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;->gamerpic()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_3

    .end local v0    # "that":Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;
    :cond_6
    move v1, v2

    .line 86
    goto :goto_0
.end method

.method public gamerpic()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->gamerpic:Ljava/lang/String;

    return-object v0
.end method

.method public gamertag()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->gamertag:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    const v3, 0xf4243

    .line 91
    const/4 v0, 0x1

    .line 92
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 93
    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->code:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 94
    mul-int/2addr v0, v3

    .line 95
    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->email:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 96
    mul-int/2addr v0, v3

    .line 97
    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->gamertag:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 98
    mul-int/2addr v0, v3

    .line 99
    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->gamerpic:Ljava/lang/String;

    if-nez v1, :cond_2

    :goto_2
    xor-int/2addr v0, v2

    .line 100
    mul-int/2addr v0, v3

    .line 101
    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->isNewUser:Z

    if-eqz v1, :cond_3

    const/16 v1, 0x4cf

    :goto_3
    xor-int/2addr v0, v1

    .line 102
    return v0

    .line 95
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->email:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 97
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->gamertag:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 99
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->gamerpic:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    .line 101
    :cond_3
    const/16 v1, 0x4d5

    goto :goto_3
.end method

.method public isNewUser()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->isNewUser:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EnterCodeAction{code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", email="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->email:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", gamertag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->gamertag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", gamerpic="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->gamerpic:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isNewUser="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/domain/oobe/AutoValue_OOBEInteractor_EnterCodeAction;->isNewUser:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
