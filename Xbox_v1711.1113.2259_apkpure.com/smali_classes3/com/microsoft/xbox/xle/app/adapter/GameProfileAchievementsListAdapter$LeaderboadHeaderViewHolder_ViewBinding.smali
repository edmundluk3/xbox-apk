.class public Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder_ViewBinding;
.super Ljava/lang/Object;
.source "GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder;Landroid/view/View;)V
    .locals 2
    .param p1, "target"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder;

    .line 21
    const v0, 0x7f0e0645

    const-string v1, "field \'compareAchievementButton\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder;->compareAchievementButton:Landroid/view/View;

    .line 22
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder;

    .line 28
    .local v0, "target":Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 29
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder;

    .line 31
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder;->compareAchievementButton:Landroid/view/View;

    .line 32
    return-void
.end method
