.class public Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;
.source "GameProgressGameclipsScreenAdapter.java"


# instance fields
.field private filterSpinner:Landroid/widget/Spinner;

.field private filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;",
            ">;"
        }
    .end annotation
.end field

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lcom/microsoft/xbox/service/network/managers/ICapture;",
            ">;"
        }
    .end annotation
.end field

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

.field private noContentText:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

.field private final recyclerViewClickListener:Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;)V
    .locals 5
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;-><init>()V

    .line 46
    const v0, 0x7f0e069d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->screenBody:Landroid/view/View;

    .line 47
    const v0, 0x7f0e069f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 49
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    .line 50
    new-instance v0, Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;)Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener$OnItemClickListener;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener$OnItemClickListener;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->recyclerViewClickListener:Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener;

    .line 59
    const v0, 0x7f0e06a0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->setListView(Landroid/support/v7/widget/RecyclerView;)V

    .line 61
    const v0, 0x7f0e069e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    .line 62
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x1090008

    .line 64
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->values()[Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    const v1, 0x7f03020a

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->setDropDownViewResource(I)V

    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->getCapturesFilter()Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 68
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0301d8

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;-><init>(Landroid/content/Context;IZLcom/microsoft/xbox/xle/viewmodel/ILikeControl;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 70
    const v0, 0x7f0e06a1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->noContentText:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    .line 71
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;)Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    return-object v0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;Landroid/support/v7/widget/RecyclerView;Landroid/view/View;I)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;
    .param p1, "parent"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "adapterPosition"    # I

    .prologue
    .line 51
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->items:Ljava/util/List;

    invoke-interface {v1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    move-object v0, v1

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    .line 52
    .local v0, "capture":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;->getIsScreenshot()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 53
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;

    .end local v0    # "capture":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->navigateToScreenshot(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Screenshot;)V

    .line 57
    :goto_0
    return-void

    .line 55
    .restart local v0    # "capture":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    .end local v0    # "capture":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->navigateToGameclip(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V

    goto :goto_0
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    return-object v0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 75
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onStart()V

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->recyclerViewClickListener:Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->addOnItemTouchListener(Landroid/support/v7/widget/RecyclerView$OnItemTouchListener;)V

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    if-eqz v0, :cond_1

    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 103
    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->recyclerViewClickListener:Lcom/microsoft/xbox/toolkit/listeners/OnRecyclerViewClickListener;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->removeOnItemTouchListener(Landroid/support/v7/widget/RecyclerView$OnItemTouchListener;)V

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    if-eqz v0, :cond_1

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 113
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    if-eqz v0, :cond_2

    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->cleanupLikeClicks()V

    .line 117
    :cond_2
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;->onStop()V

    .line 118
    return-void
.end method

.method public updateViewOverride()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 122
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->isBusy()Z

    move-result v4

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->updateLoadingIndicator(Z)V

    .line 123
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->filterSpinner:Landroid/widget/Spinner;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->getCapturesFilter()Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/Spinner;->setSelection(I)V

    .line 125
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->noContentText:Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->getNoContentText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->setText(Ljava/lang/String;)V

    .line 127
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->getIsFilterChanged()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v4, v5, :cond_1

    .line 128
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->getCapturesFilter()Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    move-result-object v0

    .line 129
    .local v0, "filter":Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    sget-object v4, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter$2;->$SwitchMap$com$microsoft$xbox$xle$viewmodel$GameProgressGameClipsFilter:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 168
    .end local v0    # "filter":Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 169
    return-void

    .line 131
    .restart local v0    # "filter":Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    :pswitch_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    sget-object v5, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsSaved:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v4, v5, v6}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->setGameClipsFilter(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;Z)V

    goto :goto_0

    .line 134
    :pswitch_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    sget-object v5, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->MyGameClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v4, v5, v6}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->setGameClipsFilter(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;Z)V

    goto :goto_0

    .line 137
    :pswitch_2
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    sget-object v5, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;->CommunityClipsRecent:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;

    invoke-virtual {v4, v5, v6}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->setGameClipsFilter(Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;Z)V

    goto :goto_0

    .line 143
    .end local v0    # "filter":Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameClipsFilter;
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->getData()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 144
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->getData()Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->items:Ljava/util/List;

    .line 145
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->clear()V

    .line 147
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->items:Ljava/util/List;

    if-eqz v4, :cond_2

    .line 148
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->items:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 149
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->items:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    invoke-virtual {v5, v4}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->add(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;)V

    .line 148
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 152
    .end local v1    # "i":I
    :cond_2
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/adapter/ProfileShowcaseListAdapter;->notifyDataSetChanged()V

    .line 154
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressGameclipsScreenViewModel;->getLastSelectedCapture()Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;

    move-result-object v2

    .line 155
    .local v2, "lastSelected":Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$Capture;
    if-eqz v2, :cond_0

    .line 156
    const/4 v3, 0x0

    .line 157
    .local v3, "pos":I
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->items:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_3

    .line 158
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->items:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-ne v4, v2, :cond_4

    .line 159
    move v3, v1

    .line 163
    :cond_3
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProgressGameclipsScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v4, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->scrollToPosition(Landroid/support/v7/widget/RecyclerView;I)V

    goto :goto_0

    .line 157
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 129
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
