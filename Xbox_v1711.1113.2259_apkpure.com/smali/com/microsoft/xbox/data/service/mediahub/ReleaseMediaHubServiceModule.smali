.class public Lcom/microsoft/xbox/data/service/mediahub/ReleaseMediaHubServiceModule;
.super Ljava/lang/Object;
.source "ReleaseMediaHubServiceModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/microsoft/xbox/data/service/mediahub/MediaHubServiceModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideMediaHubService(Lretrofit2/Retrofit;)Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;
    .locals 1
    .param p1, "retrofit"    # Lretrofit2/Retrofit;
        .annotation runtime Ljavax/inject/Named;
            value = "MEDIA_HUB_RETROFIT"
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 17
    const-class v0, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    invoke-virtual {p1, v0}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/service/mediahub/MediaHubService;

    return-object v0
.end method
