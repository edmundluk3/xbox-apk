.class Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameProfileAchievementsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadGameProfileAchievementsFriendsWhoPlayAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;)V
    .locals 0

    .prologue
    .line 422
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$1;

    .prologue
    .line 422
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 4

    .prologue
    .line 426
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 427
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGameProfileFriendsWhoPlayModel()Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;->shouldRefresh(J)Z

    move-result v0

    .line 430
    :goto_0
    return v0

    .line 429
    :cond_0
    const-string v0, "GameProfileAchievementsScreenViewModel"

    const-string v1, "Invalid title id"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 458
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGameProfileFriendsWhoPlayModel()Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;->forceLoad:Z

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;->load(ZJ)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 459
    const-string v0, "GameProfileAchievementsScreenViewModel"

    const-string v1, "Failed to get friends who play this title."

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 422
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 448
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 422
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 436
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 437
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 438
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 453
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$300(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 454
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 422
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 442
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 443
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadGameProfileAchievementsFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$402(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Z)Z

    .line 444
    return-void
.end method
