.class public Lcom/microsoft/xbox/xle/ui/ReputationArcView;
.super Landroid/view/View;
.source "ReputationArcView.java"


# static fields
.field public static REP_AVOIDME:I

.field public static REP_GOOD:I

.field public static REP_INDANGER:I

.field public static REP_NEEDSWORK:I

.field public static REP_SUPERSTAR:I


# instance fields
.field private boundingRectf:Landroid/graphics/RectF;

.field private paint:Landroid/graphics/Paint;

.field private reputationColorID:I

.field private reputationPercentage:F

.field private strokeWidth:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/16 v0, 0x19

    sput v0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->REP_AVOIDME:I

    .line 25
    const/16 v0, 0x21

    sput v0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->REP_INDANGER:I

    .line 26
    const/16 v0, 0x32

    sput v0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->REP_NEEDSWORK:I

    .line 27
    const/16 v0, 0x4b

    sput v0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->REP_GOOD:I

    .line 28
    const/16 v0, 0x64

    sput v0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->REP_SUPERSTAR:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->paint:Landroid/graphics/Paint;

    .line 20
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->boundingRectf:Landroid/graphics/RectF;

    .line 32
    sget-object v1, Lcom/microsoft/xboxone/smartglass/R$styleable;->ReputationArcView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 33
    .local v0, "a":Landroid/content/res/TypedArray;
    const/16 v1, 0xb

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->strokeWidth:F

    .line 34
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0115

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->reputationColorID:I

    .line 37
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->paint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 38
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->paint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->strokeWidth:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 39
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->setBackgroundColor(I)V

    .line 40
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    const/4 v6, 0x0

    const/high16 v2, 0x43340000    # 180.0f

    .line 66
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 68
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->strokeWidth:F

    cmpl-float v0, v0, v6

    if-lez v0, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->strokeWidth:F

    div-float v6, v0, v3

    .line 69
    .local v6, "halfStrokeWidth":F
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->boundingRectf:Landroid/graphics/RectF;

    iput v6, v0, Landroid/graphics/RectF;->left:F

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->boundingRectf:Landroid/graphics/RectF;

    iput v6, v0, Landroid/graphics/RectF;->top:F

    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->boundingRectf:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v1, v6

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->boundingRectf:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v3

    sub-float/2addr v1, v6

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->paint:Landroid/graphics/Paint;

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0c00ce

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 75
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->boundingRectf:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    move v3, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->reputationColorID:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 78
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->boundingRectf:Landroid/graphics/RectF;

    iget v3, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->reputationPercentage:F

    iget-object v5, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 79
    return-void
.end method

.method public setPercentage(I)V
    .locals 5
    .param p1, "percentage"    # I

    .prologue
    const v4, 0x7f0c0114

    .line 43
    if-ltz p1, :cond_0

    const/16 v0, 0x64

    if-le p1, v0, :cond_1

    .line 44
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->reputationPercentage:F

    .line 45
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->reputationColorID:I

    .line 61
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->invalidate()V

    .line 62
    return-void

    .line 47
    :cond_1
    int-to-double v0, p1

    const-wide v2, 0x3ffccccccccccccdL    # 1.8

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->reputationPercentage:F

    .line 49
    sget v0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->REP_GOOD:I

    if-le p1, v0, :cond_2

    .line 50
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0c0118

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->reputationColorID:I

    goto :goto_0

    .line 51
    :cond_2
    sget v0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->REP_NEEDSWORK:I

    if-le p1, v0, :cond_3

    .line 52
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0c0115

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->reputationColorID:I

    goto :goto_0

    .line 53
    :cond_3
    sget v0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->REP_INDANGER:I

    if-le p1, v0, :cond_4

    .line 54
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0c0117

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->reputationColorID:I

    goto :goto_0

    .line 55
    :cond_4
    sget v0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->REP_AVOIDME:I

    if-le p1, v0, :cond_5

    .line 56
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0c0116

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->reputationColorID:I

    goto :goto_0

    .line 58
    :cond_5
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/ReputationArcView;->reputationColorID:I

    goto :goto_0
.end method
