.class public Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "MyClubsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ClubViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
        ">;"
    }
.end annotation


# instance fields
.field clubActivityText:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08d5
    .end annotation
.end field

.field clubGlyph:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08d2
    .end annotation
.end field

.field clubImage:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08cf
    .end annotation
.end field

.field clubName:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08d1
    .end annotation
.end field

.field clubType:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08d3
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;Landroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;->this$0:Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;

    .line 72
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 73
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 74
    return-void
.end method

.method static synthetic lambda$onBind$0(Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;->this$0:Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;->access$000(Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;->this$0:Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;->access$000(Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 83
    :cond_0
    return-void
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V
    .locals 7
    .param p1, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v6, 0x7f0201fa

    const v5, 0x7f020125

    .line 79
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;->itemView:Landroid/view/View;

    invoke-static {p0, p1}, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    const-string v1, ""

    .line 86
    .local v1, "imageUrl":Ljava/lang/String;
    const-string v0, ""

    .line 87
    .local v0, "clubName":Ljava/lang/String;
    const-string v2, ""

    .line 89
    .local v2, "typeString":Ljava/lang/String;
    invoke-static {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 90
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->displayImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v3

    const-string v4, ""

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "imageUrl":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 91
    .restart local v1    # "imageUrl":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v3

    const-string v4, ""

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "clubName":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 92
    .restart local v0    # "clubName":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubType()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubTypeContainer;->localizedTitleFamilyName()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "typeString":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 95
    .restart local v2    # "typeString":Ljava/lang/String;
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;->clubImage:Landroid/widget/ImageView;

    invoke-static {v3, v1, v5, v5}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 96
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;->clubName:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->glyphImageUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 98
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;->clubGlyph:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 99
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;->clubGlyph:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->glyphImageUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v6, v6}, Lcom/microsoft/xbox/toolkit/ImageLoader;->load(Landroid/widget/ImageView;Ljava/lang/String;II)V

    .line 104
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;->clubType:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;->clubActivityText:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->getPresenceStringWithMembership()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    return-void

    .line 101
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;->clubGlyph:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 54
    check-cast p1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/clubs/MyClubsListAdapter$ClubViewHolder;->onBind(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    return-void
.end method
