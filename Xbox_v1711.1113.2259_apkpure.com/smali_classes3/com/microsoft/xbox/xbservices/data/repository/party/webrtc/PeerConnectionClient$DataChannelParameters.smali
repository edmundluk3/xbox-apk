.class public Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;
.super Ljava/lang/Object;
.source "PeerConnectionClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DataChannelParameters"
.end annotation


# instance fields
.field public final id:I

.field public final maxRetransmitTimeMs:I

.field public final maxRetransmits:I

.field public final negotiated:Z

.field public final ordered:Z

.field public final protocol:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZIILjava/lang/String;ZI)V
    .locals 0
    .param p1, "ordered"    # Z
    .param p2, "maxRetransmitTimeMs"    # I
    .param p3, "maxRetransmits"    # I
    .param p4, "protocol"    # Ljava/lang/String;
    .param p5, "negotiated"    # Z
    .param p6, "id"    # I

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    iput-boolean p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;->ordered:Z

    .line 136
    iput p2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;->maxRetransmitTimeMs:I

    .line 137
    iput p3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;->maxRetransmits:I

    .line 138
    iput-object p4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;->protocol:Ljava/lang/String;

    .line 139
    iput-boolean p5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;->negotiated:Z

    .line 140
    iput p6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/webrtc/PeerConnectionClient$DataChannelParameters;->id:I

    .line 141
    return-void
.end method
