.class public Lcom/microsoft/playready/HttpLocalServer;
.super Ljava/lang/Object;
.source "HttpLocalServer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;,
        Lcom/microsoft/playready/HttpLocalServer$WorkerThread;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "HttpLocalServer"

.field private static s_InstanceLock:Ljava/lang/Object;

.field private static final s_InstanceMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/microsoft/playready/HttpLocalServer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mRequestThread:Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/microsoft/playready/HttpLocalServer;->s_InstanceMap:Landroid/util/SparseArray;

    .line 76
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/microsoft/playready/HttpLocalServer;->s_InstanceLock:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(I)V
    .locals 2
    .param p1, "serverPort"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227
    new-instance v0, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;

    invoke-direct {v0, p1}, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;-><init>(I)V

    iput-object v0, p0, Lcom/microsoft/playready/HttpLocalServer;->mRequestThread:Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;

    .line 229
    iget-object v0, p0, Lcom/microsoft/playready/HttpLocalServer;->mRequestThread:Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/microsoft/playready/HttpLocalServer;->mRequestThread:Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->setDaemon(Z)V

    .line 231
    iget-object v0, p0, Lcom/microsoft/playready/HttpLocalServer;->mRequestThread:Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;

    invoke-virtual {v0}, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->start()V

    .line 233
    :cond_0
    return-void
.end method

.method public static getInstance(I)Lcom/microsoft/playready/HttpLocalServer;
    .locals 4
    .param p0, "serverPort"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 254
    const/4 v0, 0x0

    .line 256
    .local v0, "instance":Lcom/microsoft/playready/HttpLocalServer;
    sget-object v2, Lcom/microsoft/playready/HttpLocalServer;->s_InstanceMap:Landroid/util/SparseArray;

    invoke-virtual {v2, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    .line 257
    sget-object v3, Lcom/microsoft/playready/HttpLocalServer;->s_InstanceLock:Ljava/lang/Object;

    monitor-enter v3

    .line 258
    :try_start_0
    sget-object v2, Lcom/microsoft/playready/HttpLocalServer;->s_InstanceMap:Landroid/util/SparseArray;

    invoke-virtual {v2, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    .line 259
    new-instance v1, Lcom/microsoft/playready/HttpLocalServer;

    invoke-direct {v1, p0}, Lcom/microsoft/playready/HttpLocalServer;-><init>(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 261
    .end local v0    # "instance":Lcom/microsoft/playready/HttpLocalServer;
    .local v1, "instance":Lcom/microsoft/playready/HttpLocalServer;
    :try_start_1
    sget-object v2, Lcom/microsoft/playready/HttpLocalServer;->s_InstanceMap:Landroid/util/SparseArray;

    invoke-virtual {v2, p0, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v0, v1

    .line 257
    .end local v1    # "instance":Lcom/microsoft/playready/HttpLocalServer;
    .restart local v0    # "instance":Lcom/microsoft/playready/HttpLocalServer;
    :cond_0
    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 266
    :cond_1
    if-nez v0, :cond_2

    .line 267
    sget-object v2, Lcom/microsoft/playready/HttpLocalServer;->s_InstanceMap:Landroid/util/SparseArray;

    invoke-virtual {v2, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "instance":Lcom/microsoft/playready/HttpLocalServer;
    check-cast v0, Lcom/microsoft/playready/HttpLocalServer;

    .line 270
    .restart local v0    # "instance":Lcom/microsoft/playready/HttpLocalServer;
    :cond_2
    return-object v0

    .line 257
    :catchall_0
    move-exception v2

    :goto_0
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    .end local v0    # "instance":Lcom/microsoft/playready/HttpLocalServer;
    .restart local v1    # "instance":Lcom/microsoft/playready/HttpLocalServer;
    :catchall_1
    move-exception v2

    move-object v0, v1

    .end local v1    # "instance":Lcom/microsoft/playready/HttpLocalServer;
    .restart local v0    # "instance":Lcom/microsoft/playready/HttpLocalServer;
    goto :goto_0
.end method

.method public static stopInstance(I)V
    .locals 3
    .param p0, "serverPort"    # I

    .prologue
    .line 274
    sget-object v1, Lcom/microsoft/playready/HttpLocalServer;->s_InstanceMap:Landroid/util/SparseArray;

    invoke-virtual {v1, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/HttpLocalServer;

    .line 275
    .local v0, "instance":Lcom/microsoft/playready/HttpLocalServer;
    iget-object v1, v0, Lcom/microsoft/playready/HttpLocalServer;->mRequestThread:Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;

    invoke-virtual {v1}, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->kill()V

    .line 276
    sget-object v2, Lcom/microsoft/playready/HttpLocalServer;->s_InstanceLock:Ljava/lang/Object;

    monitor-enter v2

    .line 277
    :try_start_0
    sget-object v1, Lcom/microsoft/playready/HttpLocalServer;->s_InstanceMap:Landroid/util/SparseArray;

    invoke-virtual {v1, p0}, Landroid/util/SparseArray;->delete(I)V

    .line 276
    monitor-exit v2

    .line 279
    return-void

    .line 276
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 245
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/playready/HttpLocalServer;->mRequestThread:Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/microsoft/playready/HttpLocalServer;->mRequestThread:Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;

    invoke-virtual {v0}, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->kill()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 251
    return-void

    .line 248
    :catchall_0
    move-exception v0

    .line 249
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 250
    throw v0
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/microsoft/playready/HttpLocalServer;->mRequestThread:Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;

    invoke-virtual {v0}, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->pause()V

    .line 237
    return-void
.end method

.method public registerMediaProxy(Lcom/microsoft/playready/MediaProxyRequestHandler;)V
    .locals 1
    .param p1, "proxyHandler"    # Lcom/microsoft/playready/MediaProxyRequestHandler;

    .prologue
    .line 283
    iget-object v0, p0, Lcom/microsoft/playready/HttpLocalServer;->mRequestThread:Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;

    invoke-virtual {v0, p1}, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->registerMediaProxy(Lcom/microsoft/playready/MediaProxyRequestHandler;)V

    .line 284
    return-void
.end method

.method public unpause()V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/microsoft/playready/HttpLocalServer;->mRequestThread:Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;

    invoke-virtual {v0}, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->unpause()V

    .line 241
    return-void
.end method

.method public unregisterMediaProxy(Lcom/microsoft/playready/MediaProxyRequestHandler;)V
    .locals 1
    .param p1, "proxyHandler"    # Lcom/microsoft/playready/MediaProxyRequestHandler;

    .prologue
    .line 287
    iget-object v0, p0, Lcom/microsoft/playready/HttpLocalServer;->mRequestThread:Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;

    invoke-virtual {v0, p1}, Lcom/microsoft/playready/HttpLocalServer$RequestListenerThread;->unregisterMediaProxy(Lcom/microsoft/playready/MediaProxyRequestHandler;)V

    .line 288
    return-void
.end method
