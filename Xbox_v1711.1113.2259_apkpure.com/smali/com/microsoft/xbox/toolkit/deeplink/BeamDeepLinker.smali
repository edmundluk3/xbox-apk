.class public Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker;
.super Ljava/lang/Object;
.source "BeamDeepLinker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;
    }
.end annotation


# static fields
.field private static final BEAM_SCHEME:Ljava/lang/String; = "beam"

.field private static final BEAM_STREAM_PATH:Ljava/lang/String; = "channel/%s"

.field private static final BEAM_WEB_URI:Ljava/lang/String; = "https://mixer.com/%s"

.field private static final INTERNAL_PACKAGE:Ljava/lang/String; = "com.mcprohosting.beam.internal"

.field private static final MIXER_SCHEME:Ljava/lang/String; = "mixerwatch"

.field private static final PACKAGE_IN_STORE:Ljava/lang/String; = "com.mcprohosting.beam"


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method private static getLaunchStreamInAppIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 6
    .param p0, "scheme"    # Ljava/lang/String;
    .param p1, "channelId"    # Ljava/lang/String;

    .prologue
    .line 67
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 69
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    .line 70
    invoke-virtual {v1, p0}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "channel/%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    .line 71
    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 72
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 69
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 74
    return-object v0
.end method

.method private getLaunchStreamWebIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 5
    .param p1, "channelId"    # Ljava/lang/String;

    .prologue
    .line 78
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "https://mixer.com/%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 79
    .local v0, "webLink":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    return-object v1
.end method

.method private resultFromIntent(Landroid/content/Intent;)Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 52
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 53
    .local v2, "uri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v1

    .line 55
    .local v1, "packageName":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    const-string v4, "beam"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    const-string v4, "mixerwatch"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    const-string v3, "com.mcprohosting.beam"

    .line 56
    invoke-static {v1, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualNonNull(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "com.mcprohosting.beam.internal"

    .line 57
    invoke-static {v1, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualNonNull(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 59
    .local v0, "launchedBeam":Z
    :goto_0
    if-eqz v0, :cond_3

    .line 60
    sget-object v3, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;->App:Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

    .line 62
    :goto_1
    return-object v3

    .line 57
    .end local v0    # "launchedBeam":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 62
    .restart local v0    # "launchedBeam":Z
    :cond_3
    sget-object v3, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;->Web:Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

    goto :goto_1
.end method


# virtual methods
.method public launchStream(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;
    .locals 3
    .param p1, "channelId"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-static {}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->getInstance()Lcom/microsoft/xbox/xle/model/SystemSettingsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/SystemSettingsModel;->beamAppDeeplinkEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 33
    invoke-static {}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->newInstance()Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;

    move-result-object v1

    const-string v2, "mixerwatch"

    .line 34
    invoke-static {v2, p1}, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker;->getLaunchStreamInAppIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->addIntent(Landroid/content/Intent;)Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;

    move-result-object v1

    const-string v2, "beam"

    .line 35
    invoke-static {v2, p1}, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker;->getLaunchStreamInAppIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->addIntent(Landroid/content/Intent;)Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;

    move-result-object v1

    .line 36
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker;->getLaunchStreamWebIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->addIntent(Landroid/content/Intent;)Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;

    move-result-object v1

    .line 37
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->defaultExecute()Landroid/content/Intent;

    move-result-object v0

    .line 44
    .local v0, "launchedIntent":Landroid/content/Intent;
    :goto_0
    if-eqz v0, :cond_1

    .line 45
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker;->resultFromIntent(Landroid/content/Intent;)Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

    move-result-object v1

    .line 47
    :goto_1
    return-object v1

    .line 39
    .end local v0    # "launchedIntent":Landroid/content/Intent;
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->newInstance()Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;

    move-result-object v1

    .line 40
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker;->getLaunchStreamWebIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->addIntent(Landroid/content/Intent;)Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;

    move-result-object v1

    .line 41
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/deeplink/DeepLinkRequest;->defaultExecute()Landroid/content/Intent;

    move-result-object v0

    .restart local v0    # "launchedIntent":Landroid/content/Intent;
    goto :goto_0

    .line 47
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;->Failed:Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

    goto :goto_1
.end method
