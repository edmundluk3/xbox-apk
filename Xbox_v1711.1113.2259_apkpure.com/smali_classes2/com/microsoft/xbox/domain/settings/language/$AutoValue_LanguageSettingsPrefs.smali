.class abstract Lcom/microsoft/xbox/domain/settings/language/$AutoValue_LanguageSettingsPrefs;
.super Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;
.source "$AutoValue_LanguageSettingsPrefs.java"


# instance fields
.field private final currentLanguage:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

.field private final currentMarket:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;)V
    .locals 2
    .param p1, "currentLanguage"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;
    .param p2, "currentMarket"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;-><init>()V

    .line 16
    if-nez p1, :cond_0

    .line 17
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null currentLanguage"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/domain/settings/language/$AutoValue_LanguageSettingsPrefs;->currentLanguage:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    .line 20
    if-nez p2, :cond_1

    .line 21
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null currentMarket"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 23
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/domain/settings/language/$AutoValue_LanguageSettingsPrefs;->currentMarket:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    .line 24
    return-void
.end method


# virtual methods
.method public currentLanguage()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/microsoft/xbox/domain/settings/language/$AutoValue_LanguageSettingsPrefs;->currentLanguage:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    return-object v0
.end method

.method public currentMarket()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/domain/settings/language/$AutoValue_LanguageSettingsPrefs;->currentMarket:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 48
    if-ne p1, p0, :cond_1

    .line 56
    :cond_0
    :goto_0
    return v1

    .line 51
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 52
    check-cast v0, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;

    .line 53
    .local v0, "that":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;
    iget-object v3, p0, Lcom/microsoft/xbox/domain/settings/language/$AutoValue_LanguageSettingsPrefs;->currentLanguage:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;->currentLanguage()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/settings/language/$AutoValue_LanguageSettingsPrefs;->currentMarket:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    .line 54
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;->currentMarket()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;
    :cond_3
    move v1, v2

    .line 56
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 61
    const/4 v0, 0x1

    .line 62
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 63
    iget-object v1, p0, Lcom/microsoft/xbox/domain/settings/language/$AutoValue_LanguageSettingsPrefs;->currentLanguage:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 64
    mul-int/2addr v0, v2

    .line 65
    iget-object v1, p0, Lcom/microsoft/xbox/domain/settings/language/$AutoValue_LanguageSettingsPrefs;->currentMarket:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    invoke-virtual {v1}, Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 66
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LanguageSettingsPrefs{currentLanguage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/settings/language/$AutoValue_LanguageSettingsPrefs;->currentLanguage:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedLanguage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", currentMarket="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/settings/language/$AutoValue_LanguageSettingsPrefs;->currentMarket:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsDataTypes$SupportedMarket;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
