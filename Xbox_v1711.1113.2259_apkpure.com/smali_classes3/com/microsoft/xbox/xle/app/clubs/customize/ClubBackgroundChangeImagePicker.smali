.class public Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeImagePicker;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;
.source "ClubBackgroundChangeImagePicker.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeImagePicker;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 14
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBaseModal;->onCreate()V

    .line 15
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeImagePicker;->onCreateContentView()V

    .line 16
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeImagePicker;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 17
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 26
    const v0, 0x7f03007c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeImagePicker;->setContentView(I)V

    .line 27
    return-void
.end method
