.class public abstract Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;
.super Ljava/lang/Object;
.source "WelcomeCardCompletionStates.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/logging/Loggable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;
    }
.end annotation


# static fields
.field private static final DEFAULTS:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 13
    invoke-static {}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->builder()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;

    move-result-object v0

    .line 14
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;->setupWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;

    move-result-object v0

    .line 15
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;->redeemWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;

    move-result-object v0

    .line 16
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;->shopWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;

    move-result-object v0

    .line 17
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;->facebookWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;

    move-result-object v0

    .line 18
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;->suggestedFriendsWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;

    move-result-object v0

    .line 19
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;->clubWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;

    move-result-object v0

    .line 20
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;->build()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->DEFAULTS:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    .line 12
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static builder()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;
    .locals 1

    .prologue
    .line 96
    new-instance v0, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;

    invoke-direct {v0}, Lcom/microsoft/xbox/domain/tutorial/$AutoValue_WelcomeCardCompletionStates$Builder;-><init>()V

    return-object v0
.end method

.method public static defaultStates()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->DEFAULTS:Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    return-object v0
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    new-instance v0, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/tutorial/AutoValue_WelcomeCardCompletionStates$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static withClubWelcomeCardCompleted(Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;
    .locals 2
    .param p0, "welcomeCardCompletionStates"    # Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    .prologue
    .line 84
    .line 85
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->toBuilder()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 86
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;->clubWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;->build()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v0

    .line 84
    return-object v0
.end method

.method public static withFacebookWelcomeCardCompleted(Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;
    .locals 2
    .param p0, "welcomeCardCompletionStates"    # Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    .prologue
    .line 70
    .line 71
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->toBuilder()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 72
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;->facebookWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;->build()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v0

    .line 70
    return-object v0
.end method

.method public static withRedeemWelcomeCardCompleted(Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;
    .locals 2
    .param p0, "welcomeCardCompletionStates"    # Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    .prologue
    .line 56
    .line 57
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->toBuilder()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 58
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;->redeemWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;->build()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v0

    .line 56
    return-object v0
.end method

.method public static withSetupWelcomeCardCompleted(Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;
    .locals 2
    .param p0, "welcomeCardCompletionStates"    # Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    .prologue
    .line 49
    .line 50
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->toBuilder()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 51
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;->setupWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;->build()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v0

    .line 49
    return-object v0
.end method

.method public static withShopWelcomeCardCompleted(Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;
    .locals 2
    .param p0, "welcomeCardCompletionStates"    # Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    .prologue
    .line 63
    .line 64
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->toBuilder()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 65
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;->shopWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;->build()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v0

    .line 63
    return-object v0
.end method

.method public static withSuggestedFriendsWelcomeCardCompleted(Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;
    .locals 2
    .param p0, "welcomeCardCompletionStates"    # Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    .prologue
    .line 77
    .line 78
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;->toBuilder()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 79
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;->suggestedFriendsWelcomeCardCompleted(Z)Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;->build()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates;

    move-result-object v0

    .line 77
    return-object v0
.end method


# virtual methods
.method public abstract clubWelcomeCardCompleted()Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "clubWelcomeCardCompleted"
    .end annotation
.end method

.method public abstract facebookWelcomeCardCompleted()Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "facebookWelcomeCardCompleted"
    .end annotation
.end method

.method public abstract redeemWelcomeCardCompleted()Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "redeemWelcomeCardCompleted"
    .end annotation
.end method

.method public abstract setupWelcomeCardCompleted()Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "setupWelcomeCardCompleted"
    .end annotation
.end method

.method public abstract shopWelcomeCardCompleted()Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "shopWelcomeCardCompleted"
    .end annotation
.end method

.method public abstract suggestedFriendsWelcomeCardCompleted()Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "suggestedFriendsWelcomeCardCompleted"
    .end annotation
.end method

.method public abstract toBuilder()Lcom/microsoft/xbox/domain/tutorial/WelcomeCardCompletionStates$Builder;
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
