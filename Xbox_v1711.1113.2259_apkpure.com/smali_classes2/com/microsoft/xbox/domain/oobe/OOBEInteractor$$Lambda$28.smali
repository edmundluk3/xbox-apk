.class final synthetic Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$28;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;

.field private final arg$2:Lcom/microsoft/xbox/toolkit/SchedulerProvider;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$28;->arg$1:Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;

    iput-object p2, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$28;->arg$2:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)Lio/reactivex/functions/Function;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$28;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$28;-><init>(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$28;->arg$1:Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;

    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$28;->arg$2:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    check-cast p1, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->lambda$null$22(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;)Lio/reactivex/ObservableSource;

    move-result-object v0

    return-object v0
.end method
