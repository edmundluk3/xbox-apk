.class Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$4;
.super Ljava/lang/Object;
.source "ChannelItemDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;

.field final synthetic val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$4;->this$0:Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$4;->val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 141
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$4;->val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getHeadendID()Ljava/lang/String;

    move-result-object v1

    .line 142
    .local v1, "providerId":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$4;->val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelGuid()Ljava/lang/String;

    move-result-object v0

    .line 144
    .local v0, "channelId":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getInstance()Lcom/microsoft/xbox/xle/epg/TvStreamer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/TvStreamer;->getStreamerState()Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;->IDLE:Lcom/microsoft/xbox/xle/epg/TvStreamer$StreamerState;

    if-ne v2, v3, :cond_0

    .line 145
    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->trackStreamLaunchAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    :cond_0
    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->streamChannel(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$4;->this$0:Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->dismiss()V

    .line 149
    return-void
.end method
