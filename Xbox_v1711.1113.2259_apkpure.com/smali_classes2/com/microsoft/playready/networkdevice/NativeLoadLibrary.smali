.class Lcom/microsoft/playready/networkdevice/NativeLoadLibrary;
.super Ljava/lang/Object;
.source "NativeLoadLibrary.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static loadLibrary()V
    .locals 5

    .prologue
    .line 19
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 22
    .local v1, "sdk":I
    const/16 v2, 0xe

    if-eq v1, v2, :cond_0

    const/16 v2, 0xf

    if-ne v1, v2, :cond_1

    .line 25
    :cond_0
    const-string v0, "media_ics"

    .line 26
    .local v0, "nativeLib":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 88
    :goto_0
    const-string v2, "LibMediaPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "lib"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".so is successfully loaded."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    return-void

    .line 28
    .end local v0    # "nativeLib":Ljava/lang/String;
    :cond_1
    const/16 v2, 0x10

    if-eq v1, v2, :cond_2

    const/16 v2, 0x11

    if-ne v1, v2, :cond_3

    .line 31
    :cond_2
    const-string v0, "media_jb"

    .line 32
    .restart local v0    # "nativeLib":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    goto :goto_0

    .line 34
    .end local v0    # "nativeLib":Ljava/lang/String;
    :cond_3
    const/16 v2, 0x12

    if-ne v1, v2, :cond_4

    .line 36
    const-string v0, "media_jb2"

    .line 37
    .restart local v0    # "nativeLib":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    goto :goto_0

    .line 39
    .end local v0    # "nativeLib":Ljava/lang/String;
    :cond_4
    const/16 v2, 0x13

    if-ne v1, v2, :cond_5

    .line 51
    const-string v0, "media_kk2"

    .line 52
    .restart local v0    # "nativeLib":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    goto :goto_0

    .line 85
    .end local v0    # "nativeLib":Ljava/lang/String;
    :cond_5
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported sdk version: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method
