.class public final enum Lcom/microsoft/xbox/service/clubs/ClubPresenceServiceStub;
.super Ljava/lang/Enum;
.source "ClubPresenceServiceStub.java"

# interfaces
.implements Lcom/microsoft/xbox/service/clubs/IClubPresenceService;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubPresenceServiceStub;",
        ">;",
        "Lcom/microsoft/xbox/service/clubs/IClubPresenceService;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/clubs/ClubPresenceServiceStub;

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubPresenceServiceStub;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 12
    new-instance v0, Lcom/microsoft/xbox/service/clubs/ClubPresenceServiceStub;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubPresenceServiceStub;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubPresenceServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubPresenceServiceStub;

    .line 11
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/clubs/ClubPresenceServiceStub;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubPresenceServiceStub;->INSTANCE:Lcom/microsoft/xbox/service/clubs/ClubPresenceServiceStub;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/clubs/ClubPresenceServiceStub;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubPresenceServiceStub;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubPresenceServiceStub;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubPresenceServiceStub;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubPresenceServiceStub;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/clubs/ClubPresenceServiceStub;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubPresenceServiceStub;->$VALUES:[Lcom/microsoft/xbox/service/clubs/ClubPresenceServiceStub;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/clubs/ClubPresenceServiceStub;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/clubs/ClubPresenceServiceStub;

    return-object v0
.end method


# virtual methods
.method public updateClubPresence(JJLcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)Z
    .locals 3
    .param p1, "clubId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "userId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p5, "clubPresenceState"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    const-wide/16 v0, 0x1

    .line 16
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 17
    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 18
    invoke-static {v0, v1, p3, p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 19
    invoke-static {p5}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 21
    const/4 v0, 0x1

    return v0
.end method
