.class public Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen$TitleLeaderboardScreenAdapterProvider;
.super Ljava/lang/Object;
.source "TitleLeaderboardScreen.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "TitleLeaderboardScreenAdapterProvider"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen;


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen$TitleLeaderboardScreenAdapterProvider;->this$0:Lcom/microsoft/xbox/xle/app/activity/TitleLeaderboardScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    return-void
.end method


# virtual methods
.method public getAdapter(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 62
    move-object v0, p1

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;

    .line 63
    .local v0, "titleLeaderboardVm":Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 64
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getTitleLeaderboardAdapter(Lcom/microsoft/xbox/xle/viewmodel/TitleLeaderboardScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    return-object v1
.end method
