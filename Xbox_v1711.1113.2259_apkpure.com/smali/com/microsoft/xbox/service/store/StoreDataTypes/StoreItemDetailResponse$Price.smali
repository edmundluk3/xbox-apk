.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Price;
.super Ljava/lang/Object;
.source "StoreItemDetailResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Price"
.end annotation


# instance fields
.field public final currencyCode:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "CurrencyCode"
    .end annotation
.end field

.field public final listPrice:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ListPrice"
    .end annotation
.end field

.field public final msrp:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "MSRP"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;FF)V
    .locals 4
    .param p1, "currencyCode"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "listPrice"    # F
        .annotation build Landroid/support/annotation/FloatRange;
            from = 0.0
        .end annotation
    .end param
    .param p3, "msrp"    # F
        .annotation build Landroid/support/annotation/FloatRange;
            from = 0.0
        .end annotation
    .end param

    .prologue
    const-wide/16 v2, 0x0

    .line 566
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 567
    float-to-double v0, p2

    invoke-static {v2, v3, v0, v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->floatRangeFrom(DD)V

    .line 568
    float-to-double v0, p3

    invoke-static {v2, v3, v0, v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->floatRangeFrom(DD)V

    .line 569
    iput-object p1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Price;->currencyCode:Ljava/lang/String;

    .line 570
    iput p2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Price;->listPrice:F

    .line 571
    iput p3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Price;->msrp:F

    .line 572
    return-void
.end method
