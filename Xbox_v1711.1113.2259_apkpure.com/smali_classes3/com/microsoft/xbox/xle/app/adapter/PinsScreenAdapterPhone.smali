.class public Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithGrid;
.source "PinsScreenAdapterPhone.java"


# static fields
.field private static final NUM_OF_COLUMNS:I = 0x3


# instance fields
.field protected final barCount:I

.field private final cellListener:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;

.field protected final cellMargin:I

.field private final dnd:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            ">;"
        }
    .end annotation
.end field

.field private errorDialog:Landroid/app/AlertDialog;

.field private final gridAdpater:Landroid/widget/BaseAdapter;

.field private final gridView:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

.field private final inflater:Landroid/view/LayoutInflater;

.field private pinsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/PinItem;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Landroid/content/res/Resources;

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final tutorialClose:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final tutorialLayout:Landroid/view/ViewGroup;

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V
    .locals 5
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .prologue
    const/4 v3, 0x0

    .line 47
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithGrid;-><init>()V

    .line 201
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$6;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$6;-><init>(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->gridAdpater:Landroid/widget/BaseAdapter;

    .line 48
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;

    .line 49
    const v1, 0x7f0e0972

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->screenBody:Landroid/view/View;

    .line 50
    const v1, 0x7f0e0975

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 51
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->screenBody:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 52
    .local v0, "ctx":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->res:Landroid/content/res/Resources;

    .line 53
    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->inflater:Landroid/view/LayoutInflater;

    .line 55
    const v1, 0x7f0e0973

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->tutorialLayout:Landroid/view/ViewGroup;

    .line 57
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getPinsTutorialIsCleared()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->tutorialLayout:Landroid/view/ViewGroup;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 59
    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->tutorialClose:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 64
    :goto_0
    const v1, 0x7f0e0976

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->gridView:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    .line 65
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->gridView:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->setGridView(Lcom/microsoft/xbox/toolkit/ui/XLEGridView;)V

    .line 67
    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->content:Landroid/view/View;

    .line 68
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->gridView:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->gridAdpater:Landroid/widget/BaseAdapter;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 69
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;

    invoke-direct {v1, p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->cellListener:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;

    .line 71
    const/4 v1, 0x3

    iput v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->barCount:I

    .line 72
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->res:Landroid/content/res/Resources;

    const v2, 0x7f09034b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->cellMargin:I

    .line 73
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->gridView:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    iget v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->barCount:I

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->setNumColumns(I)V

    .line 75
    new-instance v1, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$1;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)V

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->gridAdpater:Landroid/widget/BaseAdapter;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->gridView:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;-><init>(Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper$Config;Landroid/widget/BaseAdapter;Lcom/microsoft/xbox/toolkit/ui/XLEGridView;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->dnd:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    .line 101
    return-void

    .line 61
    :cond_0
    const v1, 0x7f0e0974

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->tutorialClose:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->pinsList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$002(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->pinsList:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->dnd:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->hidePinsTutorial()V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)Landroid/view/LayoutInflater;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->inflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->cellListener:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerPins;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;Landroid/view/View;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->assingNewCellSize(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private assingNewCellSize(Landroid/view/View;)Landroid/view/View;
    .locals 2
    .param p1, "cell"    # Landroid/view/View;

    .prologue
    .line 189
    if-eqz p1, :cond_0

    .line 190
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 191
    .local v0, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->gridView:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getWidth()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->computeCellSize(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 192
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 194
    .end local v0    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    return-object p1
.end method

.method static createErrorDialog(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;)Landroid/app/AlertDialog;
    .locals 7
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;

    .prologue
    .line 159
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 160
    .local v0, "bldr":Landroid/app/AlertDialog$Builder;
    new-instance v3, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$3;

    invoke-direct {v3, p1}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$3;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;)V

    .line 166
    .local v3, "positiveButton":Landroid/content/DialogInterface$OnClickListener;
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$4;

    invoke-direct {v1, p1}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$4;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;)V

    .line 172
    .local v1, "negativeButton":Landroid/content/DialogInterface$OnClickListener;
    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$5;

    invoke-direct {v2, p1}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$5;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;)V

    .line 179
    .local v2, "onCancelListener":Landroid/content/DialogInterface$OnCancelListener;
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070a56

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0704c5

    .line 180
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f070736

    .line 181
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 182
    invoke-virtual {v4, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const/4 v5, 0x1

    .line 183
    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 185
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    return-object v4
.end method

.method private hidePinsTutorial()V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->tutorialLayout:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 105
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setPinsTutorialIsCleared(Z)V

    .line 106
    return-void
.end method


# virtual methods
.method protected computeCellSize(I)I
    .locals 4
    .param p1, "rootSize"    # I

    .prologue
    .line 198
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    int-to-double v2, p1

    mul-double/2addr v0, v2

    iget v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->barCount:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    iget v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->cellMargin:I

    mul-int/lit8 v2, v2, 0x2

    int-to-double v2, v2

    sub-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;

    return-object v0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 110
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithGrid;->onStart()V

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->tutorialClose:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->tutorialClose:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone$2;-><init>(Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 123
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithGrid;->onStop()V

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->tutorialClose:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->tutorialClose:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    :cond_0
    return-void
.end method

.method protected updateViewOverride()V
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->getPins()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->pinsList:Ljava/util/List;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->getPins()Ljava/util/List;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 142
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->dnd:Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/ui/VerticalGridDnDHelper;->isDragging()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->hasPendingMovePinRequests()Z

    move-result v0

    if-nez v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->getPins()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->pinsList:Ljava/util/List;

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->gridAdpater:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->hasMovePinErrors()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->errorDialog:Landroid/app/AlertDialog;

    if-nez v0, :cond_1

    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->gridView:Lcom/microsoft/xbox/toolkit/ui/XLEGridView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEGridView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->createErrorDialog(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;)Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->errorDialog:Landroid/app/AlertDialog;

    .line 153
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->errorDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 155
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;->isBusy()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/PinsScreenAdapterPhone;->updateLoadingIndicator(Z)V

    .line 156
    return-void
.end method
