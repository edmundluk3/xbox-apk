.class public Lcom/flipkart/chatheads/ui/container/WindowManagerContainer$MotionCapturingTouchListener;
.super Ljava/lang/Object;
.source "WindowManagerContainer.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "MotionCapturingTouchListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;


# direct methods
.method protected constructor <init>(Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;)V
    .locals 0
    .param p1, "this$0"    # Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;

    .prologue
    .line 251
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer$MotionCapturingTouchListener;->this$0:Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 254
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer$MotionCapturingTouchListener;->this$0:Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;

    invoke-virtual {v1, p1}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getContainerX(Landroid/view/View;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer$MotionCapturingTouchListener;->this$0:Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;

    invoke-virtual {v2, p1}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getContainerY(Landroid/view/View;)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2, v1, v2}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 255
    iget-object v1, p0, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer$MotionCapturingTouchListener;->this$0:Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;

    invoke-virtual {v1}, Lcom/flipkart/chatheads/ui/container/WindowManagerContainer;->getFrameLayout()Lcom/flipkart/chatheads/ui/HostFrameLayout;

    move-result-object v0

    .line 256
    .local v0, "frameLayout":Lcom/flipkart/chatheads/ui/HostFrameLayout;
    if-eqz v0, :cond_0

    .line 257
    invoke-virtual {v0, p2}, Lcom/flipkart/chatheads/ui/HostFrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 259
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
