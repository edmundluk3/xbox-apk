.class public Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder_ViewBinding;
.super Ljava/lang/Object;
.source "ClubDiscoveryInvitationListAdapter$InvitationViewHolder_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;

    .line 25
    const v0, 0x7f0e0389

    const-string v1, "field \'headerView\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->headerView:Landroid/view/View;

    .line 26
    const v0, 0x7f0e038b

    const-string v1, "field \'clubPic\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->clubPic:Landroid/widget/ImageView;

    .line 27
    const v0, 0x7f0e038a

    const-string v1, "field \'clubBanner\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->clubBanner:Landroid/widget/ImageView;

    .line 28
    const v0, 0x7f0e038c

    const-string v1, "field \'clubName\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->clubName:Landroid/widget/TextView;

    .line 29
    const v0, 0x7f0e038e

    const-string v1, "field \'clubIcon\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->clubIcon:Landroid/widget/TextView;

    .line 30
    const v0, 0x7f0e038f

    const-string v1, "field \'clubGlyph\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->clubGlyph:Landroid/widget/ImageView;

    .line 31
    const v0, 0x7f0e0390

    const-string v1, "field \'clubType\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->clubType:Landroid/widget/TextView;

    .line 32
    const v0, 0x7f0e0392

    const-string v1, "field \'clubOwner\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->clubOwner:Landroid/widget/TextView;

    .line 33
    const v0, 0x7f0e0393

    const-string v1, "field \'inviteReason\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->inviteReason:Landroid/widget/TextView;

    .line 34
    const v0, 0x7f0e0394

    const-string v1, "field \'acceptButton\'"

    const-class v2, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->acceptButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 35
    const v0, 0x7f0e0391

    const-string v1, "field \'ignoreButton\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->ignoreButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 36
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;

    .line 42
    .local v0, "target":Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 43
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;

    .line 45
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->headerView:Landroid/view/View;

    .line 46
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->clubPic:Landroid/widget/ImageView;

    .line 47
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->clubBanner:Landroid/widget/ImageView;

    .line 48
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->clubName:Landroid/widget/TextView;

    .line 49
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->clubIcon:Landroid/widget/TextView;

    .line 50
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->clubGlyph:Landroid/widget/ImageView;

    .line 51
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->clubType:Landroid/widget/TextView;

    .line 52
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->clubOwner:Landroid/widget/TextView;

    .line 53
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->inviteReason:Landroid/widget/TextView;

    .line 54
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->acceptButton:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 55
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InvitationViewHolder;->ignoreButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 56
    return-void
.end method
