.class public final Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;
.super Lcom/facebook/react/bridge/ReactContextBaseJavaModule;
.source "LocaleProviderRnModule.java"


# static fields
.field private static final NAME:Ljava/lang/String; = "LocaleProvider"


# instance fields
.field private callback:Lcom/facebook/react/bridge/Callback;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field languageSettingsRepository:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private languageUpdateDisposable:Lio/reactivex/disposables/Disposable;

.field private pendingLocale:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private pendingMarket:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V
    .locals 2
    .param p1, "reactContext"    # Lcom/facebook/react/bridge/ReactApplicationContext;

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0, p1}, Lcom/facebook/react/bridge/ReactContextBaseJavaModule;-><init>(Lcom/facebook/react/bridge/ReactApplicationContext;)V

    .line 22
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;->pendingLocale:Ljava/lang/String;

    .line 25
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;->pendingMarket:Ljava/lang/String;

    .line 28
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;->callback:Lcom/facebook/react/bridge/Callback;

    .line 38
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;)V

    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;->languageSettingsRepository:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsRepository;->prefs()Lio/reactivex/Observable;

    move-result-object v0

    .line 41
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 42
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;->languageUpdateDisposable:Lio/reactivex/disposables/Disposable;

    .line 43
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;
    .param p1, "ignore"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;->updateLocale()V

    return-void
.end method

.method private updateLocale()V
    .locals 5

    .prologue
    .line 97
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v1

    .line 98
    .local v1, "locale":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v2

    .line 99
    .local v2, "market":Ljava/lang/String;
    const/4 v0, 0x0

    .line 101
    .local v0, "callback":Lcom/facebook/react/bridge/Callback;
    monitor-enter p0

    .line 102
    :try_start_0
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;->callback:Lcom/facebook/react/bridge/Callback;

    if-eqz v3, :cond_1

    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;->callback:Lcom/facebook/react/bridge/Callback;

    .line 104
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;->callback:Lcom/facebook/react/bridge/Callback;

    .line 109
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    if-eqz v0, :cond_0

    .line 113
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v4, 0x1

    aput-object v2, v3, v4

    invoke-interface {v0, v3}, Lcom/facebook/react/bridge/Callback;->invoke([Ljava/lang/Object;)V

    .line 115
    :cond_0
    return-void

    .line 106
    :cond_1
    :try_start_1
    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;->pendingLocale:Ljava/lang/String;

    .line 107
    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;->pendingMarket:Ljava/lang/String;

    goto :goto_0

    .line 109
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method


# virtual methods
.method public getLocale(Lcom/facebook/react/bridge/Promise;)V
    .locals 2
    .param p1, "promise"    # Lcom/facebook/react/bridge/Promise;
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 60
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v0

    .line 61
    .local v0, "locale":Ljava/lang/String;
    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    .line 62
    return-void
.end method

.method public getMarket(Lcom/facebook/react/bridge/Promise;)V
    .locals 2
    .param p1, "promise"    # Lcom/facebook/react/bridge/Promise;
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 66
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v0

    .line 67
    .local v0, "market":Ljava/lang/String;
    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/Promise;->resolve(Ljava/lang/Object;)V

    .line 68
    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    const-string v0, "LocaleProvider"

    return-object v0
.end method

.method public onCatalystInstanceDestroy()V
    .locals 1

    .prologue
    .line 47
    invoke-super {p0}, Lcom/facebook/react/bridge/ReactContextBaseJavaModule;->onCatalystInstanceDestroy()V

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;->languageUpdateDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 50
    return-void
.end method

.method public setLocaleCallback(Lcom/facebook/react/bridge/Callback;)V
    .locals 4
    .param p1, "callback"    # Lcom/facebook/react/bridge/Callback;
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 76
    const/4 v0, 0x0

    .line 77
    .local v0, "locale":Ljava/lang/String;
    const/4 v1, 0x0

    .line 79
    .local v1, "market":Ljava/lang/String;
    monitor-enter p0

    .line 80
    :try_start_0
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;->pendingLocale:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;->pendingMarket:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;->pendingLocale:Ljava/lang/String;

    .line 82
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;->pendingMarket:Ljava/lang/String;

    .line 83
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;->pendingLocale:Ljava/lang/String;

    .line 84
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;->pendingMarket:Ljava/lang/String;

    .line 88
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 92
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v3, 0x1

    aput-object v1, v2, v3

    invoke-interface {p1, v2}, Lcom/facebook/react/bridge/Callback;->invoke([Ljava/lang/Object;)V

    .line 94
    :cond_0
    return-void

    .line 86
    :cond_1
    :try_start_1
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/rn/LocaleProviderRnModule;->callback:Lcom/facebook/react/bridge/Callback;

    goto :goto_0

    .line 88
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method
