.class Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadUserTitleFollowingDataAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameProfileInfoScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadUserTitleFollowingDataAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;)V
    .locals 0

    .prologue
    .line 304
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadUserTitleFollowingDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$1;

    .prologue
    .line 304
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadUserTitleFollowingDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 308
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 309
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getUserTitleFollowingModel()Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;->shouldRefresh()Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 336
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/TitleHubModel;->getUserTitleFollowingModel()Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadUserTitleFollowingDataAsyncTask;->forceLoad:Z

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/TitleHubModel$UserTitleFollowingModel;->load(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 304
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadUserTitleFollowingDataAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 331
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 304
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadUserTitleFollowingDataAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 314
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 315
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadUserTitleFollowingDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->onLoadUserTitleFollowingDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 316
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 326
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadUserTitleFollowingDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->onLoadUserTitleFollowingDataCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 327
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 304
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadUserTitleFollowingDataAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 320
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 321
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel$LoadUserTitleFollowingDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileInfoScreenViewModel;->updateAdapter()V

    .line 322
    return-void
.end method
