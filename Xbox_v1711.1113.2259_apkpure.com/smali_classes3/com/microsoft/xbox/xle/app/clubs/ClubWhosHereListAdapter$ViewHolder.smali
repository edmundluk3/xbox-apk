.class public Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "ClubWhosHereListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ViewHolder"
.end annotation


# instance fields
.field public headerText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field public image:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field public imageFrame:Landroid/widget/RelativeLayout;

.field public primaryText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field public secondaryText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field public tertiaryText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;Landroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;

    .line 79
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 80
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->inflateViews()V

    .line 81
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->bindViewHolderToItem(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;)V

    return-void
.end method

.method private bindViewHolderToItem(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;)V
    .locals 6
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;

    .prologue
    const/4 v5, 0x0

    const v4, 0x7f0201fa

    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 93
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->isHeading()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->headerText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->headingText:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->imageFrame:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->primaryText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->secondaryText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->tertiaryText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    .line 128
    :goto_0
    return-void

    .line 104
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->userData:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    if-eqz v0, :cond_1

    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->imageFrame:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->userData:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayPicRaw:Ljava/lang/String;

    invoke-virtual {v0, v1, v4, v4}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->primaryText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->userData:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->displayName:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 110
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->secondaryText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->userData:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->realName:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->tertiaryText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$WhosHereListItem;->userData:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->presenceText:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextAndVisibilityIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;I)V

    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->headerText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->itemView:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    goto :goto_0

    .line 117
    :cond_1
    const-string v0, "Unexpected WhosHereListItem state"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 118
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Unexpected WhosHereListItem state"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setVisibility(I)V

    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->imageFrame:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->headerText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->primaryText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->secondaryText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->tertiaryText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    goto :goto_0
.end method

.method private inflateViews()V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e03d3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->image:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e03d2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->imageFrame:Landroid/widget/RelativeLayout;

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e03d6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->headerText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e03d7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->primaryText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e03d8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->secondaryText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0e03d9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->tertiaryText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 90
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;->access$300(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;->access$200(Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereListAdapter$ViewHolder;->getAdapterPosition()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    .line 133
    return-void
.end method
