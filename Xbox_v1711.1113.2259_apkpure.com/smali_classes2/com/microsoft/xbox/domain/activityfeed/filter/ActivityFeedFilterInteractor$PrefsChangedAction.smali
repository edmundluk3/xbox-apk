.class public abstract Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$PrefsChangedAction;
.super Ljava/lang/Object;
.source "ActivityFeedFilterInteractor.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "PrefsChangedAction"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$PrefsChangedAction;
    .locals 1
    .param p0, "prefs"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 37
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 38
    new-instance v0, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterInteractor_PrefsChangedAction;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/activityfeed/filter/AutoValue_ActivityFeedFilterInteractor_PrefsChangedAction;-><init>(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)V

    return-object v0
.end method


# virtual methods
.method public abstract prefs()Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
