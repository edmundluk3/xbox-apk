.class public Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "ShareDecisionDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;
    }
.end annotation


# instance fields
.field private final activityFeedButton:Landroid/view/View;

.field private final activityFeedButtonIcon:Landroid/widget/TextView;

.field private final closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final clubsButton:Landroid/view/View;

.field private final clubsButtonIcon:Landroid/widget/TextView;

.field private final contentTitleTextView:Landroid/widget/TextView;

.field private feedEnabledClubs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;"
        }
    .end annotation
.end field

.field private itemRoot:Ljava/lang/String;

.field private itemType:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

.field private meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private final messagesButton:Landroid/view/View;

.field private final messagesButtonIcon:Landroid/widget/TextView;

.field private final softCloseButton:Landroid/view/View;

.field private final specificClubButton:Landroid/view/View;

.field private final specificClubButtonPic:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private final specificClubName:Landroid/widget/TextView;

.field private validShareTargets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;",
            ">;"
        }
    .end annotation
.end field

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 67
    const v0, 0x7f080238

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 68
    const v0, 0x7f030205

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->setContentView(I)V

    .line 69
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->setCanceledOnTouchOutside(Z)V

    .line 71
    const v0, 0x7f0e0a40

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 72
    const v0, 0x7f0e0a3f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->softCloseButton:Landroid/view/View;

    .line 73
    const v0, 0x7f0e0a41

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->contentTitleTextView:Landroid/widget/TextView;

    .line 74
    const v0, 0x7f0e0a43

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->activityFeedButton:Landroid/view/View;

    .line 75
    const v0, 0x7f0e0a45

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->clubsButton:Landroid/view/View;

    .line 76
    const v0, 0x7f0e0a49

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->specificClubButton:Landroid/view/View;

    .line 77
    const v0, 0x7f0e0a47

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->messagesButton:Landroid/view/View;

    .line 78
    const v0, 0x7f0e0a44

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->activityFeedButtonIcon:Landroid/widget/TextView;

    .line 79
    const v0, 0x7f0e0a46

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->clubsButtonIcon:Landroid/widget/TextView;

    .line 80
    const v0, 0x7f0e0a48

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->messagesButtonIcon:Landroid/widget/TextView;

    .line 81
    const v0, 0x7f0e0a4a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->specificClubButtonPic:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 82
    const v0, 0x7f0e0a4b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->specificClubName:Landroid/widget/TextView;

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->feedEnabledClubs:Ljava/util/List;

    .line 85
    return-void
.end method

.method private dismissSelf()V
    .locals 1

    .prologue
    .line 93
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissShareDecisionDialog()V

    .line 94
    return-void
.end method

.method static synthetic lambda$null$4(Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;J)V
    .locals 9
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;
    .param p1, "clubId"    # J

    .prologue
    const/4 v3, 0x1

    .line 162
    const-string v0, "Activity Feed Share to Club"

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->itemRoot:Ljava/lang/String;

    const-string v2, "[()]"

    const-string v4, "0"

    invoke-static {v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeExtract(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->itemType:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackShareFeed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->itemType:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Lfg:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    instance-of v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    if-eqz v0, :cond_0

    .line 164
    const-string v1, "Activity Feed Share to Club"

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    check-cast v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getSessionHandle()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg;->trackShareTo(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->itemRoot:Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v6, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Club:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    move-wide v4, p1

    invoke-static/range {v0 .. v7}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToShareToFeedScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;ZZJLcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)V

    .line 167
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->dismissSelf()V

    .line 168
    return-void
.end method

.method static synthetic lambda$onStart$0(Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Landroid/view/View;)V
    .locals 10
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;
    .param p1, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 143
    const-string v0, "Activity Feed Share to Club"

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->itemRoot:Ljava/lang/String;

    const-string v2, "[()]"

    const-string v4, "0"

    invoke-static {v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeExtract(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->itemType:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackShareFeed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->itemRoot:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v4

    sget-object v6, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Club:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v0 .. v7}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToShareToFeedScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;ZZJLcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)V

    .line 145
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->dismissSelf()V

    .line 146
    return-void
.end method

.method static synthetic lambda$onStart$1(Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 150
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->dismissSelf()V

    return-void
.end method

.method static synthetic lambda$onStart$2(Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;Landroid/view/View;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 151
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->dismissSelf()V

    return-void
.end method

.method static synthetic lambda$onStart$3(Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;Landroid/view/View;)V
    .locals 6
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 153
    const-string v0, "Activity Feed Share to Activity Feed"

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->itemRoot:Ljava/lang/String;

    const-string v2, "[()]"

    const-string v4, "0"

    invoke-static {v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeExtract(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->itemType:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackShareFeed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->itemType:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Lfg:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    instance-of v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    if-eqz v0, :cond_0

    .line 155
    const-string v1, "Activity Feed Share to Activity Feed"

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    check-cast v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getSessionHandle()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg;->trackShareTo(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->itemRoot:Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v4, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->User:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToShareToFeedScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;ZZLcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;Ljava/lang/String;)V

    .line 158
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->dismissSelf()V

    .line 159
    return-void
.end method

.method static synthetic lambda$onStart$5(Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;Landroid/view/View;)V
    .locals 3
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 161
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->feedEnabledClubs:Ljava/util/List;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;)Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showClubPickerDialog(Ljava/util/List;Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;)V

    .line 169
    return-void
.end method

.method static synthetic lambda$onStart$6(Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;Landroid/view/View;)V
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    .line 171
    const-string v0, "Activity Feed Share to Message"

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->itemRoot:Ljava/lang/String;

    const-string v2, "[()]"

    const-string v3, "0"

    invoke-static {v1, v2, v4, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeExtract(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->itemType:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackShareFeed(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->itemType:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Lfg:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    instance-of v0, v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    if-eqz v0, :cond_0

    .line 173
    const-string v1, "Activity Feed Share to Message"

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    check-cast v0, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getSessionHandle()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg;->trackShareTo(Ljava/lang/String;Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->itemRoot:Ljava/lang/String;

    invoke-static {v0, v1, v4}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToShareToMessageScreen(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Z)V

    .line 176
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->dismissSelf()V

    .line 177
    return-void
.end method


# virtual methods
.method public getProfilePicUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerPicImageUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->dismissSelf()V

    .line 90
    return-void
.end method

.method public onStart()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 98
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 99
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v2

    .line 101
    .local v2, "userColor":I
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->activityFeedButtonIcon:Landroid/widget/TextView;

    invoke-static {v3, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setBackgroundColorIfNotNull(Landroid/view/View;I)V

    .line 102
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->clubsButtonIcon:Landroid/widget/TextView;

    invoke-static {v3, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setBackgroundColorIfNotNull(Landroid/view/View;I)V

    .line 103
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->messagesButtonIcon:Landroid/widget/TextView;

    invoke-static {v3, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setBackgroundColorIfNotNull(Landroid/view/View;I)V

    .line 106
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->validShareTargets:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    .line 107
    .local v1, "target":Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;
    sget-object v5, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$1;->$SwitchMap$com$microsoft$xbox$xle$app$dialog$ShareDecisionDialog$ShareTarget:[I

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    goto :goto_1

    .line 109
    :pswitch_0
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->activityFeedButton:Landroid/view/View;

    invoke-static {v5, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    goto :goto_1

    .line 99
    .end local v1    # "target":Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;
    .end local v2    # "userColor":I
    :cond_0
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f0c013c

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    goto :goto_0

    .line 113
    .restart local v1    # "target":Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;
    .restart local v2    # "userColor":I
    :pswitch_1
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->clubsButton:Landroid/view/View;

    invoke-static {v5, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    goto :goto_1

    .line 117
    :pswitch_2
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->messagesButton:Landroid/view/View;

    invoke-static {v5, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    goto :goto_1

    .line 121
    :pswitch_3
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->specificClubButton:Landroid/view/View;

    invoke-static {v5, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    goto :goto_1

    .line 127
    .end local v1    # "target":Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->itemType:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    sget-object v5, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Lfg:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    if-ne v3, v5, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->validShareTargets:Ljava/util/List;

    sget-object v5, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;->SpecificClub:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    invoke-interface {v3, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 128
    const-string v3, "Unexpected: shared an LFG from something other than LfgDetailsViewModel"

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    instance-of v5, v5, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    invoke-static {v3, v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Ljava/lang/String;Z)V

    .line 129
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    check-cast v3, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;->getClub()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v0

    .line 130
    .local v0, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->specificClubName:Landroid/widget/TextView;

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    .line 132
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 133
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 134
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 135
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->displayImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 136
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->displayImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 137
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->specificClubName:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->specificClubButtonPic:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->displayImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 142
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->specificClubButton:Landroid/view/View;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->softCloseButton:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->activityFeedButton:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->clubsButton:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->messagesButton:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->validShareTargets:Ljava/util/List;

    sget-object v5, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;->Clubs:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;

    invoke-interface {v3, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 180
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->feedEnabledClubs:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 181
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-eqz v3, :cond_5

    .line 182
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->meProfile:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getClubs()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 183
    .restart local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 184
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;->post()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->canViewerAct()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 185
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->state()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;->Suspended:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubState;

    if-eq v5, v6, :cond_4

    .line 186
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->feedEnabledClubs:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 190
    .end local v0    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :cond_5
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->clubsButton:Landroid/view/View;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->feedEnabledClubs:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_7

    move v3, v4

    :goto_3
    invoke-static {v5, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showViewIfNotNull(Landroid/view/View;Z)V

    .line 192
    :cond_6
    return-void

    .line 190
    :cond_7
    const/4 v3, 0x0

    goto :goto_3

    .line 107
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 196
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->softCloseButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 198
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->activityFeedButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 199
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->messagesButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    return-void
.end method

.method public setItemRoot(Ljava/lang/String;)V
    .locals 0
    .param p1, "itemRoot"    # Ljava/lang/String;

    .prologue
    .line 207
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->itemRoot:Ljava/lang/String;

    .line 208
    return-void
.end method

.method public setItemType(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)V
    .locals 0
    .param p1, "itemType"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->itemType:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .line 212
    return-void
.end method

.method public setValidShareTargets(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 215
    .local p1, "shareTargets":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->validShareTargets:Ljava/util/List;

    .line 216
    return-void
.end method

.method public setViewModel(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
    .locals 0
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 203
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 204
    return-void
.end method
