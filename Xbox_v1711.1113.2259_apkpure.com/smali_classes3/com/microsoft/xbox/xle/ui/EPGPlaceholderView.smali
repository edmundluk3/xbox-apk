.class public Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;
.super Landroid/widget/LinearLayout;
.source "EPGPlaceholderView.java"


# static fields
.field private static mCache:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mTitleTextView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;->mCache:Ljava/util/LinkedList;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 65
    return-void
.end method

.method public static getInstance(Landroid/view/ViewGroup;)Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;
    .locals 4
    .param p0, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 70
    sget-object v2, Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;->mCache:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 72
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 73
    .local v0, "inflator":Landroid/view/LayoutInflater;
    const v2, 0x7f0300f3

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;

    .line 78
    .end local v0    # "inflator":Landroid/view/LayoutInflater;
    .local v1, "view":Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;
    :goto_0
    return-object v1

    .line 75
    .end local v1    # "view":Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;->mCache:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;

    .restart local v1    # "view":Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;
    goto :goto_0
.end method

.method private static reclaimInstance(Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;)V
    .locals 1
    .param p0, "view"    # Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;

    .prologue
    .line 82
    sget-object v0, Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;->mCache:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 83
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 42
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 44
    const v0, 0x7f0e0576

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;->mTitleTextView:Landroid/widget/TextView;

    .line 45
    return-void
.end method

.method public reclaim()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;->setSelected(Z)V

    .line 53
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;->setPressed(Z)V

    .line 55
    invoke-static {p0}, Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;->reclaimInstance(Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;)V

    .line 56
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/EPGPlaceholderView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 49
    return-void
.end method
