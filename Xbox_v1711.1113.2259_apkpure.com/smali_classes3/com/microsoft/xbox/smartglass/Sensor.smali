.class public abstract Lcom/microsoft/xbox/smartglass/Sensor;
.super Lcom/microsoft/xbox/smartglass/Listenable;
.source "Sensor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T",
        "Listener:Ljava/lang/Object;",
        ">",
        "Lcom/microsoft/xbox/smartglass/Listenable",
        "<TT",
        "Listener;",
        ">;"
    }
.end annotation


# instance fields
.field private final _deviceType:Ljava/lang/String;

.field _isActive:Z

.field final _listener:Landroid/hardware/SensorEventListener;

.field final _manager:Landroid/hardware/SensorManager;

.field final _sensor:Landroid/hardware/Sensor;

.field public target:Lcom/microsoft/xbox/smartglass/MessageTarget;


# direct methods
.method constructor <init>(Ljava/lang/String;Landroid/hardware/SensorManager;Landroid/hardware/Sensor;)V
    .locals 1
    .param p1, "deviceType"    # Ljava/lang/String;
    .param p2, "manager"    # Landroid/hardware/SensorManager;
    .param p3, "sensor"    # Landroid/hardware/Sensor;

    .prologue
    .line 23
    .local p0, "this":Lcom/microsoft/xbox/smartglass/Sensor;, "Lcom/microsoft/xbox/smartglass/Sensor<TTListener;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/Listenable;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/Sensor;->_deviceType:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lcom/microsoft/xbox/smartglass/Sensor;->_manager:Landroid/hardware/SensorManager;

    .line 26
    iput-object p3, p0, Lcom/microsoft/xbox/smartglass/Sensor;->_sensor:Landroid/hardware/Sensor;

    .line 27
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/Sensor;->createListener()Landroid/hardware/SensorEventListener;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/Sensor;->_listener:Landroid/hardware/SensorEventListener;

    .line 28
    return-void
.end method


# virtual methods
.method public bridge synthetic addListener(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 10
    .local p0, "this":Lcom/microsoft/xbox/smartglass/Sensor;, "Lcom/microsoft/xbox/smartglass/Sensor<TTListener;>;"
    invoke-super {p0, p1}, Lcom/microsoft/xbox/smartglass/Listenable;->addListener(Ljava/lang/Object;)V

    return-void
.end method

.method abstract createListener()Landroid/hardware/SensorEventListener;
.end method

.method public isActive()Z
    .locals 1

    .prologue
    .line 35
    .local p0, "this":Lcom/microsoft/xbox/smartglass/Sensor;, "Lcom/microsoft/xbox/smartglass/Sensor<TTListener;>;"
    iget-boolean v0, p0, Lcom/microsoft/xbox/smartglass/Sensor;->_isActive:Z

    return v0
.end method

.method public bridge synthetic removeListener(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 10
    .local p0, "this":Lcom/microsoft/xbox/smartglass/Sensor;, "Lcom/microsoft/xbox/smartglass/Sensor<TTListener;>;"
    invoke-super {p0, p1}, Lcom/microsoft/xbox/smartglass/Listenable;->removeListener(Ljava/lang/Object;)V

    return-void
.end method

.method public start(I)V
    .locals 4
    .param p1, "sampleRateMilliseconds"    # I

    .prologue
    .line 44
    .local p0, "this":Lcom/microsoft/xbox/smartglass/Sensor;, "Lcom/microsoft/xbox/smartglass/Sensor<TTListener;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/Sensor;->_sensor:Landroid/hardware/Sensor;

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Device does not support "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/Sensor;->_deviceType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " input"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/smartglass/Sensor;->_isActive:Z

    if-nez v0, :cond_1

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/Sensor;->_manager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/Sensor;->_listener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/Sensor;->_sensor:Landroid/hardware/Sensor;

    mul-int/lit16 v3, p1, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/smartglass/Sensor;->_isActive:Z

    .line 52
    :cond_1
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 59
    .local p0, "this":Lcom/microsoft/xbox/smartglass/Sensor;, "Lcom/microsoft/xbox/smartglass/Sensor<TTListener;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/Sensor;->_sensor:Landroid/hardware/Sensor;

    if-nez v0, :cond_0

    .line 60
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Device does not support "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/Sensor;->_deviceType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " input"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/Sensor;->target:Lcom/microsoft/xbox/smartglass/MessageTarget;

    .line 65
    iget-boolean v0, p0, Lcom/microsoft/xbox/smartglass/Sensor;->_isActive:Z

    if-eqz v0, :cond_1

    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/Sensor;->_manager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/Sensor;->_listener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/smartglass/Sensor;->_isActive:Z

    .line 69
    :cond_1
    return-void
.end method
