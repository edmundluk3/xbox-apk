.class Lcom/microsoft/xbox/xle/app/module/RelatedListModule$1;
.super Ljava/lang/Object;
.source "RelatedListModule.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/module/RelatedListModule;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/module/RelatedListModule;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/module/RelatedListModule;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/module/RelatedListModule$1;->this$0:Lcom/microsoft/xbox/xle/app/module/RelatedListModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 41
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/module/RelatedListModule$1;->this$0:Lcom/microsoft/xbox/xle/app/module/RelatedListModule;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->access$000(Lcom/microsoft/xbox/xle/app/module/RelatedListModule;)Landroid/widget/ArrayAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 42
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/module/RelatedListModule$1;->this$0:Lcom/microsoft/xbox/xle/app/module/RelatedListModule;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/module/RelatedListModule;->access$100(Lcom/microsoft/xbox/xle/app/module/RelatedListModule;)Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/AbstractRelatedActivityViewModel;->NavigateToRelatedItemDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 43
    return-void
.end method
