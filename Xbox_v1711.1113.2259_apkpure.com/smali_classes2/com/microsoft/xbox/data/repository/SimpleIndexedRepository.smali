.class public abstract Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;
.super Ljava/lang/Object;
.source "SimpleIndexedRepository.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<KEY:",
        "Ljava/lang/Object;",
        "SERVICE_TYPE:",
        "Ljava/lang/Object;",
        "C",
        "LIENT_TYPE:Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final data:Lcom/bumptech/glide/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/bumptech/glide/util/LruCache",
            "<TKEY;TC",
            "LIENT_TYPE;",
            ">;"
        }
    .end annotation
.end field

.field private final dataMapper:Lcom/microsoft/xbox/data/repository/DataMapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/data/repository/DataMapper",
            "<TSERVICE_TYPE;TC",
            "LIENT_TYPE;",
            ">;"
        }
    .end annotation
.end field

.field protected final schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/data/repository/DataMapper;Lcom/microsoft/xbox/toolkit/SchedulerProvider;I)V
    .locals 4
    .param p1    # Lcom/microsoft/xbox/data/repository/DataMapper;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "maxSize"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/repository/DataMapper",
            "<TSERVICE_TYPE;TC",
            "LIENT_TYPE;",
            ">;",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p0, "this":Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;, "Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository<TKEY;TSERVICE_TYPE;TCLIENT_TYPE;>;"
    .local p1, "dataMapper":Lcom/microsoft/xbox/data/repository/DataMapper;, "Lcom/microsoft/xbox/data/repository/DataMapper<TSERVICE_TYPE;TCLIENT_TYPE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 38
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 39
    const-wide/16 v0, 0x1

    int-to-long v2, p3

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 41
    new-instance v0, Lcom/bumptech/glide/util/LruCache;

    invoke-direct {v0, p3}, Lcom/bumptech/glide/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;->data:Lcom/bumptech/glide/util/LruCache;

    .line 42
    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;->dataMapper:Lcom/microsoft/xbox/data/repository/DataMapper;

    .line 43
    iput-object p2, p0, Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 44
    return-void
.end method

.method static synthetic lambda$load$0(Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;Ljava/lang/Object;)V
    .locals 3
    .param p1, "item"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 98
    .local p0, "this":Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;, "Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository<TKEY;TSERVICE_TYPE;TCLIENT_TYPE;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;->data:Lcom/bumptech/glide/util/LruCache;

    monitor-enter v1

    .line 99
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;->data:Lcom/bumptech/glide/util/LruCache;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;->mapKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2, p1}, Lcom/bumptech/glide/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    monitor-exit v1

    .line 101
    return-void

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private loadDataSafe(Ljava/util/Collection;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<TKEY;>;)",
            "Lio/reactivex/Observable",
            "<TSERVICE_TYPE;>;"
        }
    .end annotation

    .prologue
    .line 120
    .local p0, "this":Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;, "Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository<TKEY;TSERVICE_TYPE;TCLIENT_TYPE;>;"
    .local p1, "keys":Ljava/util/Collection;, "Ljava/util/Collection<TKEY;>;"
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;->loadData(Ljava/util/Collection;)Lio/reactivex/Observable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 122
    :goto_0
    return-object v1

    .line 121
    :catch_0
    move-exception v0

    .line 122
    .local v0, "ex":Ljava/lang/Exception;
    invoke-static {v0}, Lio/reactivex/Observable;->error(Ljava/lang/Throwable;)Lio/reactivex/Observable;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public load(Ljava/util/Collection;)Lio/reactivex/Observable;
    .locals 7
    .param p1    # Ljava/util/Collection;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<TKEY;>;)",
            "Lio/reactivex/Observable",
            "<TC",
            "LIENT_TYPE;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    .local p0, "this":Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;, "Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository<TKEY;TSERVICE_TYPE;TCLIENT_TYPE;>;"
    .local p1, "keys":Ljava/util/Collection;, "Ljava/util/Collection<TKEY;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 76
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 77
    .local v1, "cachedResult":Ljava/util/List;, "Ljava/util/List<TCLIENT_TYPE;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 79
    .local v3, "keysToFetch":Ljava/util/List;, "Ljava/util/List<TKEY;>;"
    iget-object v5, p0, Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;->data:Lcom/bumptech/glide/util/LruCache;

    monitor-enter v5

    .line 80
    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 81
    .local v2, "key":Ljava/lang/Object;, "TKEY;"
    iget-object v6, p0, Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;->data:Lcom/bumptech/glide/util/LruCache;

    invoke-virtual {v6, v2}, Lcom/bumptech/glide/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 82
    .local v0, "cachedItem":Ljava/lang/Object;, "TCLIENT_TYPE;"
    if-eqz v0, :cond_0

    .line 83
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 88
    .end local v0    # "cachedItem":Ljava/lang/Object;, "TCLIENT_TYPE;"
    .end local v2    # "key":Ljava/lang/Object;, "TKEY;"
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 85
    .restart local v0    # "cachedItem":Ljava/lang/Object;, "TCLIENT_TYPE;"
    .restart local v2    # "key":Ljava/lang/Object;, "TKEY;"
    :cond_0
    :try_start_1
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 88
    .end local v0    # "cachedItem":Ljava/lang/Object;, "TCLIENT_TYPE;"
    .end local v2    # "key":Ljava/lang/Object;, "TKEY;"
    :cond_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91
    invoke-static {v1}, Lio/reactivex/Observable;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/Observable;

    move-result-object v5

    .line 92
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 93
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v4

    .line 90
    :goto_1
    invoke-static {v5, v4}, Lio/reactivex/Observable;->concat(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v4

    return-object v4

    .line 94
    :cond_2
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;->loadDataSafe(Ljava/util/Collection;)Lio/reactivex/Observable;

    move-result-object v4

    iget-object v6, p0, Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 95
    invoke-interface {v6}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v6

    invoke-virtual {v4, v6}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v4

    iget-object v6, p0, Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;->dataMapper:Lcom/microsoft/xbox/data/repository/DataMapper;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v6}, Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/DataMapper;)Lio/reactivex/functions/Function;

    move-result-object v6

    .line 96
    invoke-virtual {v4, v6}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v4

    invoke-static {p0}, Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v6

    .line 97
    invoke-virtual {v4, v6}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v4

    goto :goto_1
.end method

.method protected abstract loadData(Ljava/util/Collection;)Lio/reactivex/Observable;
    .param p1    # Ljava/util/Collection;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<TKEY;>;)",
            "Lio/reactivex/Observable",
            "<TSERVICE_TYPE;>;"
        }
    .end annotation
.end method

.method protected abstract mapKey(Ljava/lang/Object;)Ljava/lang/Object;
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC",
            "LIENT_TYPE;",
            ")TKEY;"
        }
    .end annotation
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 109
    .local p0, "this":Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;, "Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository<TKEY;TSERVICE_TYPE;TCLIENT_TYPE;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;->data:Lcom/bumptech/glide/util/LruCache;

    monitor-enter v1

    .line 110
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/SimpleIndexedRepository;->data:Lcom/bumptech/glide/util/LruCache;

    invoke-virtual {v0}, Lcom/bumptech/glide/util/LruCache;->clearMemory()V

    .line 111
    monitor-exit v1

    .line 112
    return-void

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
