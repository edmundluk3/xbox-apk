.class public Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;
.super Ljava/lang/Object;
.source "UTCPageView.java"


# static fields
.field private static final PAGEVIEWVERSION:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static track(Ljava/lang/String;)V
    .locals 1
    .param p0, "toPage"    # Ljava/lang/String;

    .prologue
    .line 28
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    invoke-static {p0, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 29
    return-void
.end method

.method public static track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V
    .locals 2
    .param p0, "toPage"    # Ljava/lang/String;
    .param p1, "additionalInfo"    # Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    .prologue
    .line 32
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v0

    .line 33
    .local v0, "fromPage":Ljava/lang/String;
    invoke-static {p0, v0, p1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 34
    return-void
.end method

.method public static track(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V
    .locals 7
    .param p0, "toPage"    # Ljava/lang/String;
    .param p1, "fromPage"    # Ljava/lang/String;
    .param p2, "additionalInfo"    # Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    .prologue
    const/4 v6, 0x1

    .line 37
    new-instance v0, Lxbox/smartglass/PageView;

    invoke-direct {v0}, Lxbox/smartglass/PageView;-><init>()V

    .line 39
    .local v0, "pageView":Lxbox/smartglass/PageView;
    move-object v1, p1

    .line 43
    .local v1, "updatedFromPageName":Ljava/lang/String;
    move-object v2, p0

    .line 44
    .local v2, "updatedToPageName":Ljava/lang/String;
    const-string v3, "People Hub"

    if-ne p0, v3, :cond_3

    .line 45
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->getCurrentTab()Ljava/lang/String;

    move-result-object v2

    .line 47
    const-string v3, "People Hub"

    if-ne p1, v3, :cond_1

    .line 49
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->getPreviousTab()Ljava/lang/String;

    move-result-object v1

    .line 99
    :cond_0
    :goto_0
    invoke-virtual {v0, v2}, Lxbox/smartglass/PageView;->setPageName(Ljava/lang/String;)V

    .line 100
    invoke-virtual {v0, v1}, Lxbox/smartglass/PageView;->setFromPage(Ljava/lang/String;)V

    .line 101
    invoke-static {v6, p2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->getCommonData(ILcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)Lxbox/smartglass/CommonData;

    move-result-object v3

    invoke-virtual {v0, v3}, Lxbox/smartglass/PageView;->setBaseData(Lcom/microsoft/bond/BondSerializable;)V

    .line 102
    const-string v4, "pageView:%s, fromPage:%s, additionalInfo:%s"

    const/4 v3, 0x3

    new-array v5, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v2, v5, v3

    aput-object v1, v5, v6

    const/4 v6, 0x2

    invoke-virtual {v0}, Lxbox/smartglass/PageView;->getBaseData()Lcom/microsoft/bond/BondSerializable;

    move-result-object v3

    check-cast v3, Lxbox/smartglass/CommonData;

    invoke-virtual {v3}, Lxbox/smartglass/CommonData;->getAdditionalInfo()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v6

    invoke-static {v4, v5}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 104
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCTelemetry;->log(LMicrosoft/Telemetry/Base;)V

    .line 105
    return-void

    .line 50
    :cond_1
    const-string v3, "Game Hub"

    if-ne p1, v3, :cond_2

    .line 52
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->getCurrentTab()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 53
    :cond_2
    const-string v3, "Clubs"

    if-ne p1, v3, :cond_0

    .line 55
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->getCurrentTab()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 57
    :cond_3
    const-string v3, "Game Hub"

    if-ne p0, v3, :cond_6

    .line 58
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->getCurrentTab()Ljava/lang/String;

    move-result-object v2

    .line 60
    const-string v3, "People Hub"

    if-ne p1, v3, :cond_4

    .line 62
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->getCurrentTab()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 63
    :cond_4
    const-string v3, "Game Hub"

    if-ne p1, v3, :cond_5

    .line 65
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->getPreviousTab()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 66
    :cond_5
    const-string v3, "Clubs"

    if-ne p1, v3, :cond_0

    .line 68
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->getCurrentTab()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 70
    :cond_6
    const-string v3, "Clubs"

    if-ne p0, v3, :cond_9

    .line 71
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->getCurrentTab()Ljava/lang/String;

    move-result-object v2

    .line 73
    const-string v3, "People Hub"

    if-ne p1, v3, :cond_7

    .line 75
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->getCurrentTab()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 76
    :cond_7
    const-string v3, "Game Hub"

    if-ne p1, v3, :cond_8

    .line 78
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->getCurrentTab()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 79
    :cond_8
    const-string v3, "Clubs"

    if-ne p1, v3, :cond_0

    .line 81
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->getPreviousTab()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 83
    :cond_9
    const-string v3, "People Hub"

    if-ne p1, v3, :cond_a

    .line 85
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->getCurrentTab()Ljava/lang/String;

    move-result-object v1

    .line 86
    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->resetTracking(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 88
    :cond_a
    const-string v3, "Game Hub"

    if-ne p1, v3, :cond_b

    .line 90
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->getCurrentTab()Ljava/lang/String;

    move-result-object v1

    .line 91
    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCGameHub;->resetTracking(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 92
    :cond_b
    const-string v3, "Clubs"

    if-ne p1, v3, :cond_0

    .line 94
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->getCurrentTab()Ljava/lang/String;

    move-result-object v1

    .line 95
    invoke-static {v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->resetTracking(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static trackLegacy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "fromPage"    # Ljava/lang/String;
    .param p1, "toPage"    # Ljava/lang/String;
    .param p2, "contentIdKey"    # Ljava/lang/String;
    .param p3, "contentId"    # Ljava/lang/Object;
    .param p4, "relativeIdKey"    # Ljava/lang/String;
    .param p5, "relativeId"    # Ljava/lang/String;

    .prologue
    .line 114
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 115
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 116
    invoke-virtual {v0, p2, p3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 118
    :cond_0
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 119
    invoke-virtual {v0, p4, p5}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 121
    :cond_1
    invoke-static {p1, p0, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 122
    return-void
.end method

.method public static trackLegacy(Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/ConcurrentHashMap;)V
    .locals 1
    .param p0, "fromPage"    # Ljava/lang/String;
    .param p1, "toPage"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 108
    .local p2, "additionalInfo":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 109
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->setAdditionalInfo(Ljava/util/concurrent/ConcurrentHashMap;)V

    .line 110
    invoke-static {p1, p0, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->track(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 111
    return-void
.end method
