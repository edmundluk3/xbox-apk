.class public interface abstract Lcom/microsoft/playready/IPlayReadyFactory;
.super Ljava/lang/Object;
.source "IPlayReadyFactory.java"


# virtual methods
.method public abstract createDomainHandler()Lcom/microsoft/playready/IDomainHandler;
.end method

.method public abstract createLicenseAcquirer()Lcom/microsoft/playready/ILicenseAcquirer;
.end method

.method public abstract createMeteringReporter()Lcom/microsoft/playready/IMeteringReporter;
.end method

.method public abstract createPRActivator()Lcom/microsoft/playready/IPRActivator;
.end method

.method public abstract createPRMediaPlayer()Lcom/microsoft/playready/PRMediaPlayer;
.end method
