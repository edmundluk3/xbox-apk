.class Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$SnapImageListener;
.super Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ImageListener;
.source "HomeScreenNowPlayingAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SnapImageListener"
.end annotation


# instance fields
.field private final cell:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "cell"    # Landroid/view/View;

    .prologue
    .line 703
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ImageListener;-><init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$1;)V

    .line 704
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$SnapImageListener;->cell:Landroid/view/View;

    .line 705
    return-void
.end method


# virtual methods
.method public onBeforeImageSet(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "img"    # Landroid/widget/ImageView;
    .param p2, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 709
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$SnapImageListener;->cell:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->computeFullSizeImage(Landroid/view/View;FLandroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    .line 710
    return-void
.end method
