.class public Lcom/microsoft/xbox/xle/epg/URCIntegration;
.super Ljava/lang/Object;
.source "URCIntegration.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/urc/net/IBranchIntegration;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getString(I)Ljava/lang/String;
    .locals 4
    .param p1, "id"    # I

    .prologue
    const/4 v2, 0x0

    .line 16
    sparse-switch p1, :sswitch_data_0

    .line 211
    :cond_0
    :goto_0
    return-object v2

    .line 21
    :sswitch_0
    const v0, 0x7f070454

    .line 207
    .local v0, "newID":I
    :goto_1
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 208
    .local v1, "res":Landroid/content/res/Resources;
    if-eqz v1, :cond_0

    .line 209
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 24
    .end local v0    # "newID":I
    .end local v1    # "res":Landroid/content/res/Resources;
    :sswitch_1
    const v0, 0x7f070461

    .line 25
    .restart local v0    # "newID":I
    goto :goto_1

    .line 27
    .end local v0    # "newID":I
    :sswitch_2
    const v0, 0x7f07045e

    .line 28
    .restart local v0    # "newID":I
    goto :goto_1

    .line 30
    .end local v0    # "newID":I
    :sswitch_3
    const v0, 0x7f07045c

    .line 31
    .restart local v0    # "newID":I
    goto :goto_1

    .line 33
    .end local v0    # "newID":I
    :sswitch_4
    const v0, 0x7f070c82

    .line 34
    .restart local v0    # "newID":I
    goto :goto_1

    .line 36
    .end local v0    # "newID":I
    :sswitch_5
    const v0, 0x7f070cdf

    .line 37
    .restart local v0    # "newID":I
    goto :goto_1

    .line 39
    .end local v0    # "newID":I
    :sswitch_6
    const v0, 0x7f070ce0

    .line 40
    .restart local v0    # "newID":I
    goto :goto_1

    .line 42
    .end local v0    # "newID":I
    :sswitch_7
    const v0, 0x7f070ce1

    .line 43
    .restart local v0    # "newID":I
    goto :goto_1

    .line 45
    .end local v0    # "newID":I
    :sswitch_8
    const v0, 0x7f070ce2

    .line 46
    .restart local v0    # "newID":I
    goto :goto_1

    .line 48
    .end local v0    # "newID":I
    :sswitch_9
    const v0, 0x7f070ce3

    .line 49
    .restart local v0    # "newID":I
    goto :goto_1

    .line 51
    .end local v0    # "newID":I
    :sswitch_a
    const v0, 0x7f070ce4

    .line 52
    .restart local v0    # "newID":I
    goto :goto_1

    .line 54
    .end local v0    # "newID":I
    :sswitch_b
    const v0, 0x7f070ce5

    .line 55
    .restart local v0    # "newID":I
    goto :goto_1

    .line 57
    .end local v0    # "newID":I
    :sswitch_c
    const v0, 0x7f070ce6

    .line 58
    .restart local v0    # "newID":I
    goto :goto_1

    .line 60
    .end local v0    # "newID":I
    :sswitch_d
    const v0, 0x7f070ce7

    .line 61
    .restart local v0    # "newID":I
    goto :goto_1

    .line 63
    .end local v0    # "newID":I
    :sswitch_e
    const v0, 0x7f070ce8

    .line 64
    .restart local v0    # "newID":I
    goto :goto_1

    .line 66
    .end local v0    # "newID":I
    :sswitch_f
    const v0, 0x7f070d03

    .line 67
    .restart local v0    # "newID":I
    goto :goto_1

    .line 69
    .end local v0    # "newID":I
    :sswitch_10
    const v0, 0x7f070cff

    .line 70
    .restart local v0    # "newID":I
    goto :goto_1

    .line 73
    .end local v0    # "newID":I
    :sswitch_11
    const v0, 0x7f070cf7

    .line 74
    .restart local v0    # "newID":I
    goto :goto_1

    .line 76
    .end local v0    # "newID":I
    :sswitch_12
    const v0, 0x7f070cfc

    .line 77
    .restart local v0    # "newID":I
    goto :goto_1

    .line 79
    .end local v0    # "newID":I
    :sswitch_13
    const v0, 0x7f070ce9

    .line 80
    .restart local v0    # "newID":I
    goto :goto_1

    .line 83
    .end local v0    # "newID":I
    :sswitch_14
    const v0, 0x7f070cf5

    .line 84
    .restart local v0    # "newID":I
    goto :goto_1

    .line 86
    .end local v0    # "newID":I
    :sswitch_15
    const v0, 0x7f070cf8

    .line 87
    .restart local v0    # "newID":I
    goto :goto_1

    .line 89
    .end local v0    # "newID":I
    :sswitch_16
    const v0, 0x7f070cf3

    .line 90
    .restart local v0    # "newID":I
    goto :goto_1

    .line 92
    .end local v0    # "newID":I
    :sswitch_17
    const v0, 0x7f070d11

    .line 93
    .restart local v0    # "newID":I
    goto :goto_1

    .line 96
    .end local v0    # "newID":I
    :sswitch_18
    const v0, 0x7f070cf6

    .line 97
    .restart local v0    # "newID":I
    goto :goto_1

    .line 99
    .end local v0    # "newID":I
    :sswitch_19
    const v0, 0x7f070ced

    .line 100
    .restart local v0    # "newID":I
    goto :goto_1

    .line 102
    .end local v0    # "newID":I
    :sswitch_1a
    const v0, 0x7f070cf4

    .line 103
    .restart local v0    # "newID":I
    goto :goto_1

    .line 106
    .end local v0    # "newID":I
    :sswitch_1b
    const v0, 0x7f070cee

    .line 107
    .restart local v0    # "newID":I
    goto :goto_1

    .line 109
    .end local v0    # "newID":I
    :sswitch_1c
    const v0, 0x7f070cf1

    .line 110
    .restart local v0    # "newID":I
    goto :goto_1

    .line 112
    .end local v0    # "newID":I
    :sswitch_1d
    const v0, 0x7f070cf0

    .line 113
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 115
    .end local v0    # "newID":I
    :sswitch_1e
    const v0, 0x7f070cfa

    .line 116
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 118
    .end local v0    # "newID":I
    :sswitch_1f
    const v0, 0x7f070cf9

    .line 119
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 121
    .end local v0    # "newID":I
    :sswitch_20
    const v0, 0x7f070cfb

    .line 122
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 125
    .end local v0    # "newID":I
    :sswitch_21
    const v0, 0x7f070795

    .line 126
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 128
    .end local v0    # "newID":I
    :sswitch_22
    const v0, 0x7f07078c

    .line 129
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 131
    .end local v0    # "newID":I
    :sswitch_23
    const v0, 0x7f07078a

    .line 132
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 134
    .end local v0    # "newID":I
    :sswitch_24
    const v0, 0x7f070791

    .line 135
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 137
    .end local v0    # "newID":I
    :sswitch_25
    const v0, 0x7f070790

    .line 138
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 140
    .end local v0    # "newID":I
    :sswitch_26
    const v0, 0x7f070778

    .line 141
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 143
    .end local v0    # "newID":I
    :sswitch_27
    const v0, 0x7f070775

    .line 144
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 146
    .end local v0    # "newID":I
    :sswitch_28
    const v0, 0x7f070776

    .line 147
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 149
    .end local v0    # "newID":I
    :sswitch_29
    const v0, 0x7f070777

    .line 150
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 152
    .end local v0    # "newID":I
    :sswitch_2a
    const v0, 0x7f0707a6

    .line 153
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 155
    .end local v0    # "newID":I
    :sswitch_2b
    const v0, 0x7f0707a4

    .line 156
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 158
    .end local v0    # "newID":I
    :sswitch_2c
    const v0, 0x7f07078d

    .line 159
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 161
    .end local v0    # "newID":I
    :sswitch_2d
    const v0, 0x7f070796

    .line 162
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 164
    .end local v0    # "newID":I
    :sswitch_2e
    const v0, 0x7f07077d

    .line 165
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 167
    .end local v0    # "newID":I
    :sswitch_2f
    const v0, 0x7f07077c

    .line 168
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 170
    .end local v0    # "newID":I
    :sswitch_30
    const v0, 0x7f0707aa

    .line 171
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 173
    .end local v0    # "newID":I
    :sswitch_31
    const v0, 0x7f07077b

    .line 174
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 176
    .end local v0    # "newID":I
    :sswitch_32
    const v0, 0x7f070799

    .line 177
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 179
    .end local v0    # "newID":I
    :sswitch_33
    const v0, 0x7f070786

    .line 180
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 182
    .end local v0    # "newID":I
    :sswitch_34
    const v0, 0x7f070797

    .line 183
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 185
    .end local v0    # "newID":I
    :sswitch_35
    const v0, 0x7f070792

    .line 186
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 188
    .end local v0    # "newID":I
    :sswitch_36
    const v0, 0x7f070783

    .line 189
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 191
    .end local v0    # "newID":I
    :sswitch_37
    const v0, 0x7f070c8a

    .line 192
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 194
    .end local v0    # "newID":I
    :sswitch_38
    const v0, 0x7f070793

    .line 195
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 197
    .end local v0    # "newID":I
    :sswitch_39
    const v0, 0x7f070798

    .line 198
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 200
    .end local v0    # "newID":I
    :sswitch_3a
    const v0, 0x7f07077a

    .line 201
    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 203
    .end local v0    # "newID":I
    :sswitch_3b
    const v0, 0x7f0707a1

    .restart local v0    # "newID":I
    goto/16 :goto_1

    .line 16
    :sswitch_data_0
    .sparse-switch
        0x7f070c82 -> :sswitch_4
        0x7f070e2a -> :sswitch_0
        0x7f070e2b -> :sswitch_3
        0x7f070e2c -> :sswitch_2
        0x7f070e2d -> :sswitch_1
        0x7f070e40 -> :sswitch_5
        0x7f070e41 -> :sswitch_6
        0x7f070e42 -> :sswitch_7
        0x7f070e43 -> :sswitch_8
        0x7f070e44 -> :sswitch_9
        0x7f070e45 -> :sswitch_a
        0x7f070e46 -> :sswitch_b
        0x7f070e47 -> :sswitch_c
        0x7f070e48 -> :sswitch_d
        0x7f070e49 -> :sswitch_e
        0x7f070e4a -> :sswitch_13
        0x7f070e4b -> :sswitch_10
        0x7f070e4c -> :sswitch_f
        0x7f070e4d -> :sswitch_19
        0x7f070e4e -> :sswitch_16
        0x7f070e4f -> :sswitch_1a
        0x7f070e50 -> :sswitch_14
        0x7f070e51 -> :sswitch_17
        0x7f070e52 -> :sswitch_18
        0x7f070e53 -> :sswitch_11
        0x7f070e54 -> :sswitch_15
        0x7f070e55 -> :sswitch_12
        0x7f071068 -> :sswitch_27
        0x7f071069 -> :sswitch_28
        0x7f07106a -> :sswitch_29
        0x7f07106b -> :sswitch_26
        0x7f07106c -> :sswitch_3a
        0x7f07106d -> :sswitch_31
        0x7f07106e -> :sswitch_2f
        0x7f07106f -> :sswitch_2e
        0x7f071070 -> :sswitch_36
        0x7f071071 -> :sswitch_33
        0x7f071072 -> :sswitch_23
        0x7f071073 -> :sswitch_22
        0x7f071074 -> :sswitch_2c
        0x7f071075 -> :sswitch_25
        0x7f071076 -> :sswitch_24
        0x7f071077 -> :sswitch_35
        0x7f071078 -> :sswitch_38
        0x7f071079 -> :sswitch_21
        0x7f07107a -> :sswitch_2d
        0x7f07107b -> :sswitch_39
        0x7f07107c -> :sswitch_32
        0x7f07107d -> :sswitch_34
        0x7f07107e -> :sswitch_3b
        0x7f07107f -> :sswitch_37
        0x7f071080 -> :sswitch_2b
        0x7f071081 -> :sswitch_2a
        0x7f071082 -> :sswitch_30
        0x7f071087 -> :sswitch_1f
        0x7f071088 -> :sswitch_1e
        0x7f071089 -> :sswitch_20
        0x7f07108a -> :sswitch_1d
        0x7f07108b -> :sswitch_1b
        0x7f07108c -> :sswitch_1c
    .end sparse-switch
.end method

.method public reportButton(Ljava/lang/String;)V
    .locals 1
    .param p1, "buttonID"    # Ljava/lang/String;

    .prologue
    .line 216
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackURCPageAction(Ljava/lang/String;)V

    .line 217
    return-void
.end method

.method public reportMalformedHeadend()V
    .locals 3

    .prologue
    .line 221
    const-string v0, "OneGuide Headend"

    const-string v1, "-1"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->track(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    return-void
.end method
