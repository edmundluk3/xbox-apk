.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$24;
.super Ljava/lang/Object;
.source "UTCClubs.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackCreateClubResult(Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;JLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$clubId:J

.field final synthetic val$clubName:Ljava/lang/String;

.field final synthetic val$clubType:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

.field final synthetic val$created:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 519
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$24;->val$clubName:Ljava/lang/String;

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$24;->val$clubType:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    iput-object p3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$24;->val$created:Ljava/lang/String;

    iput-wide p4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$24;->val$clubId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 6

    .prologue
    .line 522
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$24;->val$clubName:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 523
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$24;->val$clubType:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 524
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$24;->val$created:Ljava/lang/String;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 525
    const-wide/16 v2, 0x0

    iget-wide v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$24;->val$clubId:J

    invoke-static {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 526
    const-string v0, "Public"

    .line 527
    .local v0, "clubTypeName":Ljava/lang/String;
    sget-object v2, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$78;->$SwitchMap$com$microsoft$xbox$service$clubs$ClubDataTypes$ClubType:[I

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$24;->val$clubType:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 535
    :goto_0
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 536
    .local v1, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v2, "ClubName"

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$24;->val$clubName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 537
    const-string v2, "ClubType"

    invoke-virtual {v1, v2, v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 538
    const-string v2, "ClubId"

    iget-wide v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$24;->val$clubId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 539
    const-string v2, "Created"

    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs$24;->val$created:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 541
    const-string v2, "Clubs - Create Club Result"

    const-string v3, "Clubs - Create Club Confirmation view"

    invoke-static {v2, v3, v1}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 542
    return-void

    .line 529
    .end local v1    # "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    :pswitch_0
    const-string v0, "Private"

    .line 530
    goto :goto_0

    .line 532
    :pswitch_1
    const-string v0, "Hidden"

    goto :goto_0

    .line 527
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
