.class public final Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule_ProvidePrivacyServiceFactory;
.super Ljava/lang/Object;
.source "PrivacyServiceModule_ProvidePrivacyServiceFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final module:Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;

.field private final retrofitProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lretrofit2/Retrofit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule_ProvidePrivacyServiceFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule_ProvidePrivacyServiceFactory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;Ljavax/inject/Provider;)V
    .locals 1
    .param p1, "module"    # Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;",
            "Ljavax/inject/Provider",
            "<",
            "Lretrofit2/Retrofit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p2, "retrofitProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lretrofit2/Retrofit;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    sget-boolean v0, Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule_ProvidePrivacyServiceFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 23
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule_ProvidePrivacyServiceFactory;->module:Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;

    .line 24
    sget-boolean v0, Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule_ProvidePrivacyServiceFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 25
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule_ProvidePrivacyServiceFactory;->retrofitProvider:Ljavax/inject/Provider;

    .line 26
    return-void
.end method

.method public static create(Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;Ljavax/inject/Provider;)Ldagger/internal/Factory;
    .locals 1
    .param p0, "module"    # Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;",
            "Ljavax/inject/Provider",
            "<",
            "Lretrofit2/Retrofit;",
            ">;)",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    .local p1, "retrofitProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lretrofit2/Retrofit;>;"
    new-instance v0, Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule_ProvidePrivacyServiceFactory;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule_ProvidePrivacyServiceFactory;-><init>(Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static proxyProvidePrivacyService(Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;Lretrofit2/Retrofit;)Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;
    .param p1, "retrofit"    # Lretrofit2/Retrofit;

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;->providePrivacyService(Lretrofit2/Retrofit;)Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;
    .locals 2

    .prologue
    .line 30
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule_ProvidePrivacyServiceFactory;->module:Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;

    iget-object v0, p0, Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule_ProvidePrivacyServiceFactory;->retrofitProvider:Ljavax/inject/Provider;

    .line 31
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lretrofit2/Retrofit;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;->providePrivacyService(Lretrofit2/Retrofit;)Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 30
    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule_ProvidePrivacyServiceFactory;->get()Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyService;

    move-result-object v0

    return-object v0
.end method
