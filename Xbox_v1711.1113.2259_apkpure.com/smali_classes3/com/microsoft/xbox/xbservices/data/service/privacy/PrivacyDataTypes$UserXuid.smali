.class public abstract Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;
.super Ljava/lang/Object;
.source "PrivacyDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "UserXuid"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_UserXuid$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_UserXuid$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;
    .locals 1
    .param p0, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 147
    invoke-static {p0}, Lcom/microsoft/xbox/xbservices/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 148
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_UserXuid;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xbservices/data/service/privacy/AutoValue_PrivacyDataTypes_UserXuid;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public abstract xuid()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
