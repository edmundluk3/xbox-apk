.class Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSuggestionsTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "TitlePickerBaseViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadGameSuggestionsTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private final query:Ljava/lang/String;

.field private results:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "query"    # Ljava/lang/String;

    .prologue
    .line 230
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSuggestionsTask;->this$0:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 231
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 232
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSuggestionsTask;->query:Ljava/lang/String;

    .line 233
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 237
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 3

    .prologue
    .line 252
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->access$000()Lcom/microsoft/xbox/service/store/IStoreService;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSuggestionsTask;->query:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/service/store/IStoreService;->getSearchAutoSuggestions(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSuggestionsTask;->results:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 257
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSuggestionsTask;->results:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;

    if-nez v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    :goto_0
    return-object v1

    .line 253
    :catch_0
    move-exception v0

    .line 254
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0

    .line 257
    .end local v0    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 226
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSuggestionsTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 246
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 226
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSuggestionsTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 242
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 266
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSuggestionsTask;->this$0:Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSuggestionsTask;->results:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;->access$100(Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestResults;)V

    .line 267
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 226
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerBaseViewModel$LoadGameSuggestionsTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 262
    return-void
.end method
