.class final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting;
.super Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;
.source "AutoValue_ClubHubDataTypes_Setting.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting",
        "<TT;>;"
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/Object;Lcom/google/common/collect/ImmutableList;ZZ)V
    .locals 0
    .param p3, "canViewerAct"    # Z
    .param p4, "canViewerChangeSetting"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/google/common/collect/ImmutableList",
            "<TT;>;ZZ)V"
        }
    .end annotation

    .prologue
    .line 20
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting;, "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting<TT;>;"
    .local p1, "value":Ljava/lang/Object;, "TT;"
    .local p2, "allowedValues":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<TT;>;"
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_Setting;-><init>(Ljava/lang/Object;Lcom/google/common/collect/ImmutableList;ZZ)V

    .line 21
    return-void
.end method
