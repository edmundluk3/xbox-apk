.class Lcom/microsoft/xbox/authenticate/DelegateKeyService$2;
.super Lcom/microsoft/xbox/idp/interop/MSATicketCallbacks;
.source "DelegateKeyService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/authenticate/DelegateKeyService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/authenticate/DelegateKeyService;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/authenticate/DelegateKeyService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/authenticate/DelegateKeyService;

    .prologue
    .line 176
    iput-object p1, p0, Lcom/microsoft/xbox/authenticate/DelegateKeyService$2;->this$0:Lcom/microsoft/xbox/authenticate/DelegateKeyService;

    invoke-direct {p0}, Lcom/microsoft/xbox/idp/interop/MSATicketCallbacks;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccountAcquired(Lcom/microsoft/xbox/idp/jobs/MSAJob;Lcom/microsoft/onlineid/UserAccount;)V
    .locals 0
    .param p1, "job"    # Lcom/microsoft/xbox/idp/jobs/MSAJob;
    .param p2, "userAccount"    # Lcom/microsoft/onlineid/UserAccount;

    .prologue
    .line 190
    invoke-super {p0, p1, p2}, Lcom/microsoft/xbox/idp/interop/MSATicketCallbacks;->onAccountAcquired(Lcom/microsoft/xbox/idp/jobs/MSAJob;Lcom/microsoft/onlineid/UserAccount;)V

    .line 191
    return-void
.end method

.method public onFailure(Lcom/microsoft/xbox/idp/jobs/MSAJob;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "job"    # Lcom/microsoft/xbox/idp/jobs/MSAJob;
    .param p2, "e"    # Ljava/lang/Exception;

    .prologue
    .line 184
    invoke-super {p0, p1, p2}, Lcom/microsoft/xbox/idp/interop/MSATicketCallbacks;->onFailure(Lcom/microsoft/xbox/idp/jobs/MSAJob;Ljava/lang/Exception;)V

    .line 185
    const-string v0, "DelegateKeyService"

    const-string v1, "Failed to get Delegated ticket with exception"

    invoke-static {v0, v1, p2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 186
    return-void
.end method

.method public onUiNeeded(Lcom/microsoft/xbox/idp/jobs/MSAJob;)V
    .locals 0
    .param p1, "job"    # Lcom/microsoft/xbox/idp/jobs/MSAJob;

    .prologue
    .line 179
    invoke-super {p0, p1}, Lcom/microsoft/xbox/idp/interop/MSATicketCallbacks;->onUiNeeded(Lcom/microsoft/xbox/idp/jobs/MSAJob;)V

    .line 180
    return-void
.end method
