.class public final enum Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;
.super Ljava/lang/Enum;
.source "NowPlayingTrayViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ContentType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

.field public static final enum App:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

.field public static final enum Game:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

.field public static final enum InternetExplorer:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

.field public static final enum Media:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

.field public static final enum Music:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

.field public static final enum None:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

.field public static final enum Recent:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

.field public static final enum Tv:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 65
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    const-string v1, "Media"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->Media:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    .line 66
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    const-string v1, "Game"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->Game:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    .line 67
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    const-string v1, "Music"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->Music:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    .line 68
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    const-string v1, "App"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->App:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    .line 69
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    const-string v1, "Recent"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->Recent:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    .line 70
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    const-string v1, "InternetExplorer"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->InternetExplorer:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    .line 71
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    const-string v1, "Tv"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->Tv:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    .line 72
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    const-string v1, "None"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->None:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    .line 63
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->Media:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->Game:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->Music:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->App:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->Recent:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->InternetExplorer:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->Tv:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->None:Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 63
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->$VALUES:[Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/viewmodel/NowPlayingTrayViewModel$ContentType;

    return-object v0
.end method
