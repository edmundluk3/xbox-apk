.class final Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_PartyStateResult;
.super Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;
.source "AutoValue_PartyInteractor_PartyStateResult.java"


# instance fields
.field private final partySession:Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

.field private final userSummaries:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xbservices/domain/party/PartySession;Lcom/google/common/collect/ImmutableList;)V
    .locals 2
    .param p1, "partySession"    # Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartySession;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 18
    .local p2, "userSummaries":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;-><init>()V

    .line 19
    if-nez p1, :cond_0

    .line 20
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null partySession"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 22
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_PartyStateResult;->partySession:Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    .line 23
    if-nez p2, :cond_1

    .line 24
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null userSummaries"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_PartyStateResult;->userSummaries:Lcom/google/common/collect/ImmutableList;

    .line 27
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 51
    if-ne p1, p0, :cond_1

    .line 59
    :cond_0
    :goto_0
    return v1

    .line 54
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 55
    check-cast v0, Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;

    .line 56
    .local v0, "that":Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;
    iget-object v3, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_PartyStateResult;->partySession:Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;->partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_PartyStateResult;->userSummaries:Lcom/google/common/collect/ImmutableList;

    .line 57
    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;->userSummaries()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/domain/party/PartyInteractor$PartyStateResult;
    :cond_3
    move v1, v2

    .line 59
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 64
    const/4 v0, 0x1

    .line 65
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 66
    iget-object v1, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_PartyStateResult;->partySession:Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 67
    mul-int/2addr v0, v2

    .line 68
    iget-object v1, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_PartyStateResult;->userSummaries:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 69
    return v0
.end method

.method public partySession()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_PartyStateResult;->partySession:Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PartyStateResult{partySession="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_PartyStateResult;->partySession:Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", userSummaries="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_PartyStateResult;->userSummaries:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public userSummaries()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/domain/party/AutoValue_PartyInteractor_PartyStateResult;->userSummaries:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method
