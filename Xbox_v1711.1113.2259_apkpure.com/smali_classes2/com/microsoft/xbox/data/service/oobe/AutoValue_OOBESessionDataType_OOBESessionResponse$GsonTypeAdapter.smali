.class public final Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_OOBESessionDataType_OOBESessionResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final consoleIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final consoleOSVersionAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final consoleSettingsAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final consoleUpdateProgressAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final consoleUpdateStepAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private defaultConsoleId:Ljava/lang/String;

.field private defaultConsoleOSVersion:Ljava/lang/String;

.field private defaultConsoleSettings:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

.field private defaultConsoleUpdateProgress:Ljava/lang/Integer;

.field private defaultConsoleUpdateStep:Ljava/lang/Integer;

.field private defaultEncryptionKey:Ljava/lang/String;

.field private defaultSessionExpiration:Ljava/util/Date;

.field private defaultSessionId:Ljava/lang/String;

.field private defaultSessionState:I

.field private defaultTransferToken:Ljava/lang/String;

.field private defaultUserName:Ljava/lang/String;

.field private final encryptionKeyAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final sessionExpirationAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private final sessionIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final sessionStateAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final transferTokenAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final userNameAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 2
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 34
    iput-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultSessionId:Ljava/lang/String;

    .line 35
    iput-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultConsoleId:Ljava/lang/String;

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultSessionState:I

    .line 37
    iput-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultSessionExpiration:Ljava/util/Date;

    .line 38
    iput-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultEncryptionKey:Ljava/lang/String;

    .line 39
    iput-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultTransferToken:Ljava/lang/String;

    .line 40
    iput-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultConsoleUpdateProgress:Ljava/lang/Integer;

    .line 41
    iput-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultConsoleUpdateStep:Ljava/lang/Integer;

    .line 42
    iput-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultConsoleOSVersion:Ljava/lang/String;

    .line 43
    iput-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultConsoleSettings:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    .line 44
    iput-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultUserName:Ljava/lang/String;

    .line 46
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->sessionIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 47
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->consoleIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 48
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->sessionStateAdapter:Lcom/google/gson/TypeAdapter;

    .line 49
    const-class v0, Ljava/util/Date;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->sessionExpirationAdapter:Lcom/google/gson/TypeAdapter;

    .line 50
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->encryptionKeyAdapter:Lcom/google/gson/TypeAdapter;

    .line 51
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->transferTokenAdapter:Lcom/google/gson/TypeAdapter;

    .line 52
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->consoleUpdateProgressAdapter:Lcom/google/gson/TypeAdapter;

    .line 53
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->consoleUpdateStepAdapter:Lcom/google/gson/TypeAdapter;

    .line 54
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->consoleOSVersionAdapter:Lcom/google/gson/TypeAdapter;

    .line 55
    const-class v0, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->consoleSettingsAdapter:Lcom/google/gson/TypeAdapter;

    .line 56
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->userNameAdapter:Lcom/google/gson/TypeAdapter;

    .line 57
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;
    .locals 14
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 136
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v13, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v13, :cond_0

    .line 137
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 138
    const/4 v0, 0x0

    .line 209
    :goto_0
    return-object v0

    .line 140
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 141
    iget-object v1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultSessionId:Ljava/lang/String;

    .line 142
    .local v1, "sessionId":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultConsoleId:Ljava/lang/String;

    .line 143
    .local v2, "consoleId":Ljava/lang/String;
    iget v3, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultSessionState:I

    .line 144
    .local v3, "sessionState":I
    iget-object v4, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultSessionExpiration:Ljava/util/Date;

    .line 145
    .local v4, "sessionExpiration":Ljava/util/Date;
    iget-object v5, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultEncryptionKey:Ljava/lang/String;

    .line 146
    .local v5, "encryptionKey":Ljava/lang/String;
    iget-object v6, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultTransferToken:Ljava/lang/String;

    .line 147
    .local v6, "transferToken":Ljava/lang/String;
    iget-object v7, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultConsoleUpdateProgress:Ljava/lang/Integer;

    .line 148
    .local v7, "consoleUpdateProgress":Ljava/lang/Integer;
    iget-object v8, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultConsoleUpdateStep:Ljava/lang/Integer;

    .line 149
    .local v8, "consoleUpdateStep":Ljava/lang/Integer;
    iget-object v9, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultConsoleOSVersion:Ljava/lang/String;

    .line 150
    .local v9, "consoleOSVersion":Ljava/lang/String;
    iget-object v10, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultConsoleSettings:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    .line 151
    .local v10, "consoleSettings":Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;
    iget-object v11, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultUserName:Ljava/lang/String;

    .line 152
    .local v11, "userName":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 153
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v12

    .line 154
    .local v12, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v13, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v13, :cond_1

    .line 155
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 158
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {v12}, Ljava/lang/String;->hashCode()I

    move-result v13

    sparse-switch v13, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 204
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 158
    :sswitch_0
    const-string v13, "SessionId"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :sswitch_1
    const-string v13, "ConsoleId"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :sswitch_2
    const-string v13, "SessionState"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v0, 0x2

    goto :goto_2

    :sswitch_3
    const-string v13, "SessionExpiration"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v0, 0x3

    goto :goto_2

    :sswitch_4
    const-string v13, "EncryptionKey"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v0, 0x4

    goto :goto_2

    :sswitch_5
    const-string v13, "TransferToken"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v0, 0x5

    goto :goto_2

    :sswitch_6
    const-string v13, "ConsoleUpdateProgress"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v0, 0x6

    goto :goto_2

    :sswitch_7
    const-string v13, "ConsoleUpdateStep"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v0, 0x7

    goto :goto_2

    :sswitch_8
    const-string v13, "ConsoleOSVersion"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/16 v0, 0x8

    goto :goto_2

    :sswitch_9
    const-string v13, "ConsoleSettings"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/16 v0, 0x9

    goto :goto_2

    :sswitch_a
    const-string v13, "UserName"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    const/16 v0, 0xa

    goto :goto_2

    .line 160
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->sessionIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "sessionId":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 161
    .restart local v1    # "sessionId":Ljava/lang/String;
    goto/16 :goto_1

    .line 164
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->consoleIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "consoleId":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 165
    .restart local v2    # "consoleId":Ljava/lang/String;
    goto/16 :goto_1

    .line 168
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->sessionStateAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 169
    goto/16 :goto_1

    .line 172
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->sessionExpirationAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "sessionExpiration":Ljava/util/Date;
    check-cast v4, Ljava/util/Date;

    .line 173
    .restart local v4    # "sessionExpiration":Ljava/util/Date;
    goto/16 :goto_1

    .line 176
    :pswitch_4
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->encryptionKeyAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "encryptionKey":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 177
    .restart local v5    # "encryptionKey":Ljava/lang/String;
    goto/16 :goto_1

    .line 180
    :pswitch_5
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->transferTokenAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "transferToken":Ljava/lang/String;
    check-cast v6, Ljava/lang/String;

    .line 181
    .restart local v6    # "transferToken":Ljava/lang/String;
    goto/16 :goto_1

    .line 184
    :pswitch_6
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->consoleUpdateProgressAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "consoleUpdateProgress":Ljava/lang/Integer;
    check-cast v7, Ljava/lang/Integer;

    .line 185
    .restart local v7    # "consoleUpdateProgress":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 188
    :pswitch_7
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->consoleUpdateStepAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "consoleUpdateStep":Ljava/lang/Integer;
    check-cast v8, Ljava/lang/Integer;

    .line 189
    .restart local v8    # "consoleUpdateStep":Ljava/lang/Integer;
    goto/16 :goto_1

    .line 192
    :pswitch_8
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->consoleOSVersionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "consoleOSVersion":Ljava/lang/String;
    check-cast v9, Ljava/lang/String;

    .line 193
    .restart local v9    # "consoleOSVersion":Ljava/lang/String;
    goto/16 :goto_1

    .line 196
    :pswitch_9
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->consoleSettingsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "consoleSettings":Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;
    check-cast v10, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    .line 197
    .restart local v10    # "consoleSettings":Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;
    goto/16 :goto_1

    .line 200
    :pswitch_a
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->userNameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "userName":Ljava/lang/String;
    check-cast v11, Ljava/lang/String;

    .line 201
    .restart local v11    # "userName":Ljava/lang/String;
    goto/16 :goto_1

    .line 208
    .end local v12    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 209
    new-instance v0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse;

    invoke-direct/range {v0 .. v11}, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 158
    :sswitch_data_0
    .sparse-switch
        -0x6453a1cf -> :sswitch_0
        -0x5fbe0d03 -> :sswitch_8
        -0x49088352 -> :sswitch_5
        -0x44d21cd3 -> :sswitch_6
        -0x35385834 -> :sswitch_7
        -0x24cc8986 -> :sswitch_9
        -0x1fc52c45 -> :sswitch_2
        -0xc0a9eea -> :sswitch_a
        0x2d7673bc -> :sswitch_4
        0x5e0514b2 -> :sswitch_1
        0x62c225c5 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultConsoleId(Ljava/lang/String;)Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultConsoleId"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultConsoleId:Ljava/lang/String;

    .line 64
    return-object p0
.end method

.method public setDefaultConsoleOSVersion(Ljava/lang/String;)Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultConsoleOSVersion"    # Ljava/lang/String;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultConsoleOSVersion:Ljava/lang/String;

    .line 92
    return-object p0
.end method

.method public setDefaultConsoleSettings(Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;)Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultConsoleSettings"    # Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultConsoleSettings:Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    .line 96
    return-object p0
.end method

.method public setDefaultConsoleUpdateProgress(Ljava/lang/Integer;)Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultConsoleUpdateProgress"    # Ljava/lang/Integer;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultConsoleUpdateProgress:Ljava/lang/Integer;

    .line 84
    return-object p0
.end method

.method public setDefaultConsoleUpdateStep(Ljava/lang/Integer;)Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultConsoleUpdateStep"    # Ljava/lang/Integer;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultConsoleUpdateStep:Ljava/lang/Integer;

    .line 88
    return-object p0
.end method

.method public setDefaultEncryptionKey(Ljava/lang/String;)Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultEncryptionKey"    # Ljava/lang/String;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultEncryptionKey:Ljava/lang/String;

    .line 76
    return-object p0
.end method

.method public setDefaultSessionExpiration(Ljava/util/Date;)Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultSessionExpiration"    # Ljava/util/Date;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultSessionExpiration:Ljava/util/Date;

    .line 72
    return-object p0
.end method

.method public setDefaultSessionId(Ljava/lang/String;)Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultSessionId"    # Ljava/lang/String;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultSessionId:Ljava/lang/String;

    .line 60
    return-object p0
.end method

.method public setDefaultSessionState(I)Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultSessionState"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultSessionState:I

    .line 68
    return-object p0
.end method

.method public setDefaultTransferToken(Ljava/lang/String;)Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTransferToken"    # Ljava/lang/String;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultTransferToken:Ljava/lang/String;

    .line 80
    return-object p0
.end method

.method public setDefaultUserName(Ljava/lang/String;)Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultUserName"    # Ljava/lang/String;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->defaultUserName:Ljava/lang/String;

    .line 100
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    if-nez p2, :cond_0

    .line 106
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 133
    :goto_0
    return-void

    .line 109
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 110
    const-string v0, "SessionId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->sessionIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->sessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 112
    const-string v0, "ConsoleId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->consoleIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->consoleId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 114
    const-string v0, "SessionState"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->sessionStateAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->sessionState()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 116
    const-string v0, "SessionExpiration"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 117
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->sessionExpirationAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->sessionExpiration()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 118
    const-string v0, "EncryptionKey"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->encryptionKeyAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->encryptionKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 120
    const-string v0, "TransferToken"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->transferTokenAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->transferToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 122
    const-string v0, "ConsoleUpdateProgress"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->consoleUpdateProgressAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->consoleUpdateProgress()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 124
    const-string v0, "ConsoleUpdateStep"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->consoleUpdateStepAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->consoleUpdateStep()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 126
    const-string v0, "ConsoleOSVersion"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->consoleOSVersionAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->consoleOSVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 128
    const-string v0, "ConsoleSettings"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->consoleSettingsAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->consoleSettings()Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 130
    const-string v0, "UserName"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->userNameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;->userName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 132
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto/16 :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    check-cast p2, Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$OOBESessionResponse;)V

    return-void
.end method
