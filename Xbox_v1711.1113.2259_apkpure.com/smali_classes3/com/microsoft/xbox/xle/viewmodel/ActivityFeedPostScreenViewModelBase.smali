.class public abstract Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "ActivityFeedPostScreenViewModelBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field protected checkPinTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;

.field protected errorResId:I

.field private final launchContextTimelineId:Ljava/lang/String;

.field private final launchContextTimelineType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

.field protected pinFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->TAG:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>()V
    .locals 3

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 28
    const/4 v1, 0x0

    iput v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->errorResId:I

    .line 34
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 36
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getTimelineType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->launchContextTimelineType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    .line 37
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->launchContextTimelineType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    sget-object v2, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;->Unknown:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    if-eq v1, v2, :cond_0

    .line 38
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getTimelineId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->launchContextTimelineId:Ljava/lang/String;

    .line 42
    :goto_0
    return-void

    .line 40
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->launchContextTimelineId:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;
    .param p1, "x1"    # Ljava/lang/Boolean;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->onCheckPinStatusCompleted(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;)Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->launchContextTimelineType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->launchContextTimelineId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->onPinFeedItemCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic lambda$onCheckPinStatusCompleted$0(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;

    .prologue
    .line 105
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->postStatus(Z)V

    .line 106
    return-void
.end method

.method private onCheckPinStatusCompleted(Ljava/lang/Boolean;)V
    .locals 7
    .param p1, "hasPin"    # Ljava/lang/Boolean;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 97
    if-eqz p1, :cond_1

    .line 98
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v1, 0x7f0700df

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 101
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v2, 0x7f0700de

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 102
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v3, 0x7f0707c7

    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;)Ljava/lang/Runnable;

    move-result-object v4

    .line 107
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    const v5, 0x7f07060d

    invoke-virtual {v0, v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    move-object v0, p0

    .line 99
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 118
    :goto_0
    return-void

    .line 111
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->postStatus(Z)V

    goto :goto_0

    .line 114
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->TAG:Ljava/lang/String;

    const-string v1, "CheckPinStatus returned null indicating we failed to fetch pins for this feed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const v0, 0x7f070b6d

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->errorResId:I

    .line 116
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->updateAdapter()V

    goto :goto_0
.end method

.method private onPinFeedItemCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v3, 0x0

    .line 121
    const-string v0, ""

    .line 122
    .local v0, "toastMessage":Ljava/lang/String;
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 123
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0700dc

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 128
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v2

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 129
    iput v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->errorResId:I

    .line 130
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->goBack()V

    .line 131
    return-void

    .line 125
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0700dd

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected checkPin()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->checkPinTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->checkPinTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;->cancel()V

    .line 66
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->checkPinTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->checkPinTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;->load(Z)V

    .line 68
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->updateAdapter()V

    .line 69
    return-void
.end method

.method public getLaunchContextTimelineId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->launchContextTimelineId:Ljava/lang/String;

    return-object v0
.end method

.method public getLaunchContextTimelineType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->launchContextTimelineType:Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    return-object v0
.end method

.method public isPinning()Z
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->pinFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->pinFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;->getIsBusy()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->checkPinTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->checkPinTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$CheckPinStatusAsyncTask;->getIsBusy()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected pinFeedItem(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;)Z
    .locals 6
    .param p1, "result"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 72
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 74
    const/4 v1, 0x0

    .line 75
    .local v1, "timelineUri":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;->timelines()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/common/collect/ImmutableList;->iterator()Lcom/google/common/collect/UnmodifiableIterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;

    .line 76
    .local v0, "timeline":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;->timelineType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    move-result-object v4

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->getLaunchContextTimelineType()Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$TimelineType;

    move-result-object v5

    if-ne v4, v5, :cond_0

    .line 77
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;->timelineUri()Ljava/lang/String;

    move-result-object v1

    .line 82
    .end local v0    # "timeline":Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$Timeline;
    :cond_1
    if-eqz v1, :cond_3

    .line 83
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->pinFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;

    if-eqz v3, :cond_2

    .line 84
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->pinFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;->cancel()V

    .line 87
    :cond_2
    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;

    invoke-direct {v3, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->pinFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;

    .line 88
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->pinFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase$PinFeedItemAsyncTask;->load(Z)V

    .line 92
    :goto_0
    return v2

    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected abstract postStatus(Z)V
.end method
