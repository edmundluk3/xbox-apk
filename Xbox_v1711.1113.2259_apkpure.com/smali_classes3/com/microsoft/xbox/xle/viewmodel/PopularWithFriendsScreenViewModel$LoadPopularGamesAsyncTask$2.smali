.class Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask$2;
.super Ljava/lang/Object;
.source "PopularWithFriendsScreenViewModel.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;->processData()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/PopularGame;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask$2;->this$1:Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/microsoft/xbox/service/network/managers/PopularGame;Lcom/microsoft/xbox/service/network/managers/PopularGame;)I
    .locals 4
    .param p1, "arg0"    # Lcom/microsoft/xbox/service/network/managers/PopularGame;
    .param p2, "arg1"    # Lcom/microsoft/xbox/service/network/managers/PopularGame;

    .prologue
    .line 214
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/PopularGame;->getFriendsPlayedCount()I

    move-result v0

    .line 215
    .local v0, "count0":I
    invoke-virtual {p2}, Lcom/microsoft/xbox/service/network/managers/PopularGame;->getFriendsPlayedCount()I

    move-result v1

    .line 216
    .local v1, "count1":I
    if-ge v0, v1, :cond_0

    .line 217
    const/4 v2, 0x1

    .line 224
    :goto_0
    return v2

    .line 218
    :cond_0
    if-ne v0, v1, :cond_3

    .line 219
    if-eqz v0, :cond_1

    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/PopularGame;->date:Ljava/util/Date;

    if-eqz v2, :cond_1

    iget-object v2, p2, Lcom/microsoft/xbox/service/network/managers/PopularGame;->date:Ljava/util/Date;

    if-nez v2, :cond_2

    .line 220
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 222
    :cond_2
    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/PopularGame;->date:Ljava/util/Date;

    iget-object v3, p2, Lcom/microsoft/xbox/service/network/managers/PopularGame;->date:Ljava/util/Date;

    invoke-virtual {v2, v3}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v2

    goto :goto_0

    .line 224
    :cond_3
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 211
    check-cast p1, Lcom/microsoft/xbox/service/network/managers/PopularGame;

    check-cast p2, Lcom/microsoft/xbox/service/network/managers/PopularGame;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/PopularWithFriendsScreenViewModel$LoadPopularGamesAsyncTask$2;->compare(Lcom/microsoft/xbox/service/network/managers/PopularGame;Lcom/microsoft/xbox/service/network/managers/PopularGame;)I

    move-result v0

    return v0
.end method
