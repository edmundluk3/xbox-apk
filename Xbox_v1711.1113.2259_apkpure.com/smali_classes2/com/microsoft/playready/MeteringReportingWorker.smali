.class Lcom/microsoft/playready/MeteringReportingWorker;
.super Ljava/lang/Object;
.source "MeteringReportingWorker.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private mFactory:Lcom/microsoft/playready/PlayReadyFactory;

.field private mMRPlugin:Lcom/microsoft/playready/IMeteringReportingPlugin;

.field private mMeteringCert:[B


# direct methods
.method public constructor <init>(Lcom/microsoft/playready/PlayReadyFactory;Lcom/microsoft/playready/IMeteringReportingPlugin;[B)V
    .locals 1
    .param p1, "factory"    # Lcom/microsoft/playready/PlayReadyFactory;
    .param p2, "plugin"    # Lcom/microsoft/playready/IMeteringReportingPlugin;
    .param p3, "meteringCert"    # [B

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object v0, p0, Lcom/microsoft/playready/MeteringReportingWorker;->mMeteringCert:[B

    .line 20
    iput-object v0, p0, Lcom/microsoft/playready/MeteringReportingWorker;->mMRPlugin:Lcom/microsoft/playready/IMeteringReportingPlugin;

    .line 21
    iput-object v0, p0, Lcom/microsoft/playready/MeteringReportingWorker;->mFactory:Lcom/microsoft/playready/PlayReadyFactory;

    .line 28
    iput-object p1, p0, Lcom/microsoft/playready/MeteringReportingWorker;->mFactory:Lcom/microsoft/playready/PlayReadyFactory;

    .line 29
    iput-object p2, p0, Lcom/microsoft/playready/MeteringReportingWorker;->mMRPlugin:Lcom/microsoft/playready/IMeteringReportingPlugin;

    .line 30
    iput-object p3, p0, Lcom/microsoft/playready/MeteringReportingWorker;->mMeteringCert:[B

    .line 31
    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0}, Lcom/microsoft/playready/MeteringReportingWorker;->call()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 37
    new-instance v1, Lcom/microsoft/playready/DrmProxy;

    iget-object v4, p0, Lcom/microsoft/playready/MeteringReportingWorker;->mFactory:Lcom/microsoft/playready/PlayReadyFactory;

    invoke-direct {v1, v4}, Lcom/microsoft/playready/DrmProxy;-><init>(Lcom/microsoft/playready/PlayReadyFactory;)V

    .line 39
    .local v1, "drmProxy":Lcom/microsoft/playready/DrmProxy;
    const/4 v0, 0x0

    .line 40
    .local v0, "challengeCustomData":Ljava/lang/String;
    const/4 v2, 0x0

    .line 41
    .local v2, "meteringChallenge":Lcom/microsoft/playready/DrmProxy$ChallengeData;
    const/4 v3, 0x0

    .line 43
    .local v3, "meteringResponse":[B
    iget-object v4, p0, Lcom/microsoft/playready/MeteringReportingWorker;->mMRPlugin:Lcom/microsoft/playready/IMeteringReportingPlugin;

    invoke-interface {v4}, Lcom/microsoft/playready/IMeteringReportingPlugin;->getChallengeCustomData()Ljava/lang/String;

    move-result-object v0

    .line 45
    iget-object v4, p0, Lcom/microsoft/playready/MeteringReportingWorker;->mMeteringCert:[B

    invoke-virtual {v1, v4, v0}, Lcom/microsoft/playready/DrmProxy;->generateMeteringChallenge([BLjava/lang/String;)Lcom/microsoft/playready/DrmProxy$ChallengeData;

    move-result-object v2

    .line 47
    iget-object v4, p0, Lcom/microsoft/playready/MeteringReportingWorker;->mMRPlugin:Lcom/microsoft/playready/IMeteringReportingPlugin;

    .line 48
    invoke-virtual {v2}, Lcom/microsoft/playready/DrmProxy$ChallengeData;->getChallengeBytes()[B

    move-result-object v5

    .line 49
    invoke-virtual {v2}, Lcom/microsoft/playready/DrmProxy$ChallengeData;->getEmbeddedServerUri()Ljava/lang/String;

    move-result-object v6

    .line 47
    invoke-interface {v4, v5, v6}, Lcom/microsoft/playready/IMeteringReportingPlugin;->doMeteringReport([BLjava/lang/String;)[B

    move-result-object v3

    .line 51
    invoke-virtual {v1, v3}, Lcom/microsoft/playready/DrmProxy;->processMeteringResponse([B)Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method
