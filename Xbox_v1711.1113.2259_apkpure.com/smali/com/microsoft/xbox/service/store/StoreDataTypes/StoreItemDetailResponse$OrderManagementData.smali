.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;
.super Ljava/lang/Object;
.source "StoreItemDetailResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OrderManagementData"
.end annotation


# instance fields
.field public final price:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Price;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Price"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Price;)V
    .locals 0
    .param p1, "price"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Price;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 550
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 551
    iput-object p1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$OrderManagementData;->price:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Price;

    .line 552
    return-void
.end method
