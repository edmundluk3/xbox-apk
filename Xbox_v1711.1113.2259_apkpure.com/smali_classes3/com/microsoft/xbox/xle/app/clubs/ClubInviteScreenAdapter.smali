.class public Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;
.source "ClubInviteScreenAdapter.java"


# instance fields
.field private final closeButton:Landroid/widget/Button;

.field private final clubViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

.field private followersHashCode:I

.field private final inviteList:Landroid/support/v7/widget/RecyclerView;

.field private final listAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;

.field private final recipientsHeader:Landroid/widget/TextView;

.field private final sendButton:Landroid/widget/Button;

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;)V
    .locals 5
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 37
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->clubViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    .line 39
    const v1, 0x7f0e0a95

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 40
    const v1, 0x7f0e039c

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->closeButton:Landroid/widget/Button;

    .line 41
    const v1, 0x7f0e039f

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->recipientsHeader:Landroid/widget/TextView;

    .line 42
    const v1, 0x7f0e03a2

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->inviteList:Landroid/support/v7/widget/RecyclerView;

    .line 44
    const v1, 0x7f0e06bd

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 45
    .local v0, "noContent":Landroid/widget/TextView;
    const v1, 0x7f070b62

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 47
    const v1, 0x7f0e03a1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->sendButton:Landroid/widget/Button;

    .line 48
    const v1, 0x7f0e039d

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 50
    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->clubViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;)Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$SelectionListener;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$SelectionListener;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;

    .line 51
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->inviteList:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 52
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->inviteList:Landroid/support/v7/widget/RecyclerView;

    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/DividerItemDecoration;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    const v4, 0x7f020163

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/toolkit/ui/DividerItemDecoration;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 53
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->closeButton:Landroid/widget/Button;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->sendButton:Landroid/widget/Button;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->clubViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->close()V

    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->clubViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->send()V

    return-void
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->clubViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    return-object v0
.end method

.method protected updateViewOverride()V
    .locals 7

    .prologue
    .line 69
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->clubViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 71
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->clubViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v4, v5, :cond_0

    .line 72
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->sendButton:Landroid/widget/Button;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->clubViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->getSendText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 73
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->title:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->clubViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->getTitleText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->clubViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->getRecipientGamertags()Ljava/util/List;

    move-result-object v3

    .line 76
    .local v3, "recipients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->clubViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->getFollowers()Ljava/util/List;

    move-result-object v0

    .line 78
    .local v0, "followers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 79
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->recipientsHeader:Landroid/widget/TextView;

    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f07063b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v1

    .line 90
    .local v1, "newHashCode":I
    iget v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->followersHashCode:I

    if-eq v4, v1, :cond_2

    .line 91
    iput v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->followersHashCode:I

    .line 92
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;->clear()V

    .line 93
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->clubViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->getFollowers()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;->addAll(Ljava/util/Collection;)V

    .line 98
    :goto_1
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->sendButton:Landroid/widget/Button;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    const/4 v4, 0x1

    :goto_2
    invoke-virtual {v5, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 100
    .end local v0    # "followers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    .end local v1    # "newHashCode":I
    .end local v3    # "recipients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    return-void

    .line 81
    .restart local v0    # "followers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/FollowersData;>;"
    .restart local v3    # "recipients":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 82
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f07020a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    .line 84
    invoke-static {v5, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 85
    .local v2, "recipentsHeaderText":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->recipientsHeader:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 95
    .end local v2    # "recipentsHeaderText":Ljava/lang/String;
    .restart local v1    # "newHashCode":I
    :cond_2
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenAdapter;->clubViewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteScreenViewModel;->getSelectedFollowerPositions()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;->setSelectedPositions(Ljava/util/List;)V

    goto :goto_1

    .line 98
    :cond_3
    const/4 v4, 0x0

    goto :goto_2
.end method
