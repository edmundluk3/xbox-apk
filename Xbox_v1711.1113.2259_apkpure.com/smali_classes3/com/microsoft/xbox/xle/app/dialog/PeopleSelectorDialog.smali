.class public Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "PeopleSelectorDialog.java"


# instance fields
.field private friendsPickerCancel:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private friendsPickerCancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private friendsPickerConfirm:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private friendsSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

.field private listOrGridView:Landroid/widget/AbsListView;

.field private vmWithPeopleSelectorControl:Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;ILjava/lang/String;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;
    .param p3, "choiceMode"    # I
    .param p4, "title"    # Ljava/lang/String;

    .prologue
    .line 41
    const v5, 0x7f080238

    invoke-direct {p0, p1, v5}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 42
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->vmWithPeopleSelectorControl:Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;

    .line 43
    const v5, 0x7f030109

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->setContentView(I)V

    .line 44
    const v5, 0x7f0e05c6

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 45
    .local v4, "peopleSelectorTitle":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    invoke-static {v4, p4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 46
    const v5, 0x7f0e05d0

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->friendsSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 47
    const v5, 0x7f0e05d1

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/AbsListView;

    iput-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->listOrGridView:Landroid/widget/AbsListView;

    .line 48
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->listOrGridView:Landroid/widget/AbsListView;

    invoke-virtual {v5, p3}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    .line 49
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v3

    .line 50
    .local v3, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    if-eqz v3, :cond_1

    .line 51
    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getFromScreen()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v2

    .line 52
    .local v2, "originatingScreen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-eqz v2, :cond_1

    instance-of v5, v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubActivity;

    if-nez v5, :cond_0

    instance-of v5, v2, Lcom/microsoft/xbox/xle/app/activity/DetailsPivotActivity;

    if-eqz v5, :cond_1

    .line 53
    :cond_0
    const v5, 0x7f0e04d0

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;

    .line 54
    .local v1, "friendsNoContentText":Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;
    const v5, 0x7f0e04d1

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 56
    .local v0, "compareNoContentText":Landroid/widget/LinearLayout;
    const/16 v5, 0x8

    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;->setVisibility(I)V

    .line 57
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 61
    .end local v0    # "compareNoContentText":Landroid/widget/LinearLayout;
    .end local v1    # "friendsNoContentText":Lcom/microsoft/xbox/xle/ui/SwitchPaneWithRefreshView;
    .end local v2    # "originatingScreen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_1
    new-instance v5, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f03010a

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v5, v6, v7, v8}, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

    .line 62
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->listOrGridView:Landroid/widget/AbsListView;

    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 63
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->listOrGridView:Landroid/widget/AbsListView;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/AbsListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 92
    const v5, 0x7f0e05ce

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->friendsPickerConfirm:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 93
    const v5, 0x7f0e05c5

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->friendsPickerCancel:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 94
    const v5, 0x7f0e05cf

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->friendsPickerCancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 95
    return-void
.end method

.method private dismissSelf()V
    .locals 1

    .prologue
    .line 102
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dissmissPeoplePickerDialog()V

    .line 103
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 64
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .line 65
    .local v1, "person":Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->listOrGridView:Landroid/widget/AbsListView;

    invoke-virtual {v2}, Landroid/widget/AbsListView;->getChoiceMode()I

    move-result v2

    if-ne v2, v3, :cond_1

    .line 66
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->vmWithPeopleSelectorControl:Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;

    invoke-interface {v2}, Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;->getSelectedPerson()Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    move-result-object v0

    .line 67
    .local v0, "currentItem":Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;
    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->setSelected(Z)V

    .line 71
    :cond_0
    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->setSelected(Z)V

    .line 72
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->vmWithPeopleSelectorControl:Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;

    invoke-interface {v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;->setSelectedPerson(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V

    .line 73
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->listOrGridView:Landroid/widget/AbsListView;

    invoke-virtual {v2, p3, v3}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    .line 89
    .end local v0    # "currentItem":Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;->notifyDataSetChanged()V

    .line 90
    return-void

    .line 75
    :cond_1
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->getIsSelected()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 77
    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->setSelected(Z)V

    .line 78
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->vmWithPeopleSelectorControl:Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;

    invoke-interface {v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;->removePersonFromSelected(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V

    .line 79
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->vmWithPeopleSelectorControl:Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;

    invoke-interface {v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;->addPersonToUnSelected(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V

    .line 80
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->listOrGridView:Landroid/widget/AbsListView;

    invoke-virtual {v2, p3, v4}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    goto :goto_0

    .line 82
    :cond_2
    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->setSelected(Z)V

    .line 83
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->vmWithPeopleSelectorControl:Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;

    invoke-interface {v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;->removePersonFromSelected(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V

    .line 84
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->vmWithPeopleSelectorControl:Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;

    invoke-interface {v2, v1}, Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;->addPersonToSelected(Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;)V

    .line 85
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->listOrGridView:Landroid/widget/AbsListView;

    invoke-virtual {v2, p3, v3}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    goto :goto_0
.end method

.method static synthetic lambda$onStart$1(Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->vmWithPeopleSelectorControl:Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;->selectionActivityCompleted(Z)V

    .line 123
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->dismissSelf()V

    .line 124
    return-void
.end method

.method static synthetic lambda$onStart$2(Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->vmWithPeopleSelectorControl:Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;->selectionActivityCompleted(Z)V

    .line 130
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->dismissSelf()V

    .line 131
    return-void
.end method

.method static synthetic lambda$onStart$3(Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->vmWithPeopleSelectorControl:Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;->selectionActivityCompleted(Z)V

    .line 137
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->dismissSelf()V

    .line 138
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->vmWithPeopleSelectorControl:Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;->selectionActivityCompleted(Z)V

    .line 157
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->dismissSelf()V

    .line 158
    return-void
.end method

.method public onStart()V
    .locals 6

    .prologue
    .line 107
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->vmWithPeopleSelectorControl:Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;

    invoke-interface {v4}, Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;->getPeopleSelectedList()Ljava/util/ArrayList;

    move-result-object v2

    .line 108
    .local v2, "peopleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;>;"
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;->clear()V

    .line 109
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

    invoke-virtual {v4, v2}, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;->addAll(Ljava/util/Collection;)V

    .line 110
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;->notifyDataSetChanged()V

    .line 112
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;

    .line 113
    .local v3, "person":Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;

    invoke-virtual {v5, v3}, Lcom/microsoft/xbox/xle/app/adapter/PeopleSelectorListAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v0

    .line 114
    .local v0, "index":I
    if-ltz v0, :cond_0

    .line 115
    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;->getIsSelected()Z

    move-result v1

    .line 116
    .local v1, "isSelected":Z
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->listOrGridView:Landroid/widget/AbsListView;

    invoke-virtual {v5, v0, v1}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    goto :goto_0

    .line 120
    .end local v0    # "index":I
    .end local v1    # "isSelected":Z
    .end local v3    # "person":Lcom/microsoft/xbox/xle/viewmodel/PeopleSelectorItem;
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->friendsPickerConfirm:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v4, :cond_2

    .line 121
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->friendsPickerConfirm:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    :cond_2
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->friendsPickerCancel:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v4, :cond_3

    .line 128
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->friendsPickerCancel:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    :cond_3
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->friendsPickerCancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v4, :cond_4

    .line 135
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->friendsPickerCancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    :cond_4
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 142
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->friendsSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v5, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 147
    :goto_1
    return-void

    .line 144
    :cond_5
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->friendsSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v5, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    goto :goto_1
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 151
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->dismissSelf()V

    .line 152
    return-void
.end method

.method public reportAsyncTaskCompleted()V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->vmWithPeopleSelectorControl:Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;->isBusy()Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->dismissSelf()V

    .line 165
    :cond_0
    return-void
.end method

.method public reportAsyncTaskFailed(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->friendsSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 169
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(Ljava/lang/String;I)V

    .line 170
    return-void
.end method

.method public setVmWithPeopleSelectorControl(Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;)V
    .locals 0
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->vmWithPeopleSelectorControl:Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;

    .line 99
    return-void
.end method
