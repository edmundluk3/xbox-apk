.class Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubMembershipViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "GameProfileFriendsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClubMembershipViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;",
        ">;"
    }
.end annotation


# instance fields
.field private clubList:Landroid/support/v7/widget/RecyclerView;

.field private final clubMemberListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter;

.field private headerText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;Landroid/view/View;)V
    .locals 4
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 183
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubMembershipViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    .line 184
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 186
    const v0, 0x7f0e0653

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubMembershipViewHolder;->headerText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 187
    const v0, 0x7f0e0654

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubMembershipViewHolder;->clubList:Landroid/support/v7/widget/RecyclerView;

    .line 189
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubMembershipViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubMembershipViewHolder;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter;-><init>(Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubMembershipViewHolder;->clubMemberListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter;

    .line 194
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubMembershipViewHolder;->clubList:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubMembershipViewHolder;->clubMemberListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 195
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubMembershipViewHolder;->clubList:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-direct {v1, v2, v3, v3}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 196
    return-void
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubMembershipViewHolder;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubMembershipViewHolder;
    .param p1, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .prologue
    .line 191
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubMembershipViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;->getGameTitleId()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackGameHubNavigateToSuggestedClub(JJ)V

    .line 192
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubMembershipViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsScreenViewModel;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToClub(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;J)V

    .line 193
    return-void
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;)V
    .locals 2
    .param p1, "dataObject"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 200
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 201
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubMembershipViewHolder;->headerText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 203
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubMembershipViewHolder;->clubMemberListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter;->clear()V

    .line 204
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubMembershipViewHolder;->clubMemberListAdapter:Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter;

    iget-object v1, p1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;->clubList:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubMemberListAdapter;->addAll(Ljava/util/Collection;)V

    .line 205
    return-void
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 175
    check-cast p1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileFriendsListAdapter$ClubMembershipViewHolder;->onBind(Lcom/microsoft/xbox/xle/viewmodel/GameProfileClubMembershipViewModel$GameProfileClubMembershipListItem;)V

    return-void
.end method
