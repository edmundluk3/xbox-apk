.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$6;
.super Ljava/lang/Object;
.source "UTCPurchase.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase;->trackWebBlendLoadError(ILjava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$description:Ljava/lang/String;

.field final synthetic val$errorCode:I

.field final synthetic val$failingUrl:Ljava/lang/String;

.field final synthetic val$params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$6;->val$params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    iput-object p2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$6;->val$description:Ljava/lang/String;

    iput-object p3, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$6;->val$failingUrl:Ljava/lang/String;

    iput p4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$6;->val$errorCode:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 270
    new-instance v5, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v5}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 272
    .local v5, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v6, "NormalPurchase"

    .line 273
    .local v6, "purchaseType":Ljava/lang/String;
    iget-object v8, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$6;->val$params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->isCodeRedeemable()Ljava/lang/Boolean;

    move-result-object v4

    .line 275
    .local v4, "isCodeRedemption":Ljava/lang/Boolean;
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 276
    const-string v6, "RedeemCode"

    .line 279
    :cond_0
    const-string v8, "PurchaseType"

    invoke-virtual {v5, v8, v6}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 281
    iget-object v8, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$6;->val$params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getPurchaseProductId()Ljava/lang/String;

    move-result-object v1

    .line 282
    .local v1, "catId":Ljava/lang/String;
    const-string v8, "BigCatId"

    invoke-virtual {v5, v8, v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 284
    iget-object v8, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$6;->val$params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getPurchaseSkuId()Ljava/lang/String;

    move-result-object v7

    .line 285
    .local v7, "skuId":Ljava/lang/String;
    const-string v8, "SkuId"

    invoke-virtual {v5, v8, v7}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 287
    iget-object v8, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$6;->val$params:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-virtual {v8}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getPurchaseAvailabilityId()Ljava/lang/String;

    move-result-object v0

    .line 288
    .local v0, "availabilityId":Ljava/lang/String;
    const-string v8, "AvailabilityId"

    invoke-virtual {v5, v8, v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 290
    new-instance v3, Lxbox/smartglass/ClientError;

    invoke-direct {v3}, Lxbox/smartglass/ClientError;-><init>()V

    .line 292
    .local v3, "error":Lxbox/smartglass/ClientError;
    invoke-static {v11, v5}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCCommonData;->getCommonData(ILcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)Lxbox/smartglass/CommonData;

    move-result-object v8

    invoke-virtual {v3, v8}, Lxbox/smartglass/ClientError;->setBaseData(Lcom/microsoft/bond/BondSerializable;)V

    .line 300
    const-string v2, "Unknown Web Blend error."

    .line 301
    .local v2, "descriptionText":Ljava/lang/String;
    iget-object v8, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$6;->val$description:Ljava/lang/String;

    if-eqz v8, :cond_1

    .line 302
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$6;->val$description:Ljava/lang/String;

    .line 303
    iget-object v8, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$6;->val$failingUrl:Ljava/lang/String;

    if-eqz v8, :cond_1

    .line 304
    const-string v8, "%s:%s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$6;->val$description:Ljava/lang/String;

    aput-object v10, v9, v12

    iget-object v10, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$6;->val$failingUrl:Ljava/lang/String;

    aput-object v10, v9, v11

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 308
    :cond_1
    invoke-virtual {v3, v2}, Lxbox/smartglass/ClientError;->setCallStack(Ljava/lang/String;)V

    .line 311
    const-string v8, "%s"

    new-array v9, v11, [Ljava/lang/Object;

    iget v10, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPurchase$6;->val$errorCode:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Lxbox/smartglass/ClientError;->setErrorCode(Ljava/lang/String;)V

    .line 314
    const-string v8, "WebBlend Loading Error"

    invoke-virtual {v3, v8}, Lxbox/smartglass/ClientError;->setErrorText(Ljava/lang/String;)V

    .line 317
    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCTelemetry;->log(LMicrosoft/Telemetry/Base;)V

    .line 318
    return-void
.end method
