.class public Lcom/microsoft/xbox/xle/ui/XLESwipeRefreshLayout;
.super Landroid/support/v4/widget/SwipeRefreshLayout;
.source "XLESwipeRefreshLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;-><init>(Landroid/content/Context;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Landroid/support/v4/widget/SwipeRefreshLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method private requestParentDisallowInterceptTouchEvent(Z)V
    .locals 1
    .param p1, "b"    # Z

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/XLESwipeRefreshLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 86
    .local v0, "p":Landroid/view/ViewParent;
    if-eqz v0, :cond_0

    .line 87
    invoke-interface {v0, p1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 89
    :cond_0
    return-void
.end method


# virtual methods
.method public canChildScrollUp()Z
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 36
    const/4 v4, 0x2

    .line 38
    .local v4, "maxLevesDown":I
    const/4 v0, 0x0

    .line 39
    .local v0, "child":Landroid/view/View;
    move-object v5, p0

    .line 40
    .local v5, "parent":Landroid/view/ViewGroup;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v7, 0x2

    if-ge v2, v7, :cond_2

    if-nez v0, :cond_2

    if-eqz v5, :cond_2

    .line 41
    invoke-virtual {v5, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 42
    instance-of v7, v0, Landroid/view/ViewGroup;

    if-eqz v7, :cond_0

    move-object v5, v0

    .line 43
    check-cast v5, Landroid/view/ViewGroup;

    .line 45
    :cond_0
    instance-of v7, v0, Landroid/widget/AbsListView;

    if-nez v7, :cond_1

    .line 46
    const/4 v0, 0x0

    .line 40
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 49
    :cond_2
    if-eqz v0, :cond_5

    move-object v3, v0

    .line 50
    check-cast v3, Landroid/widget/AbsListView;

    .line 51
    .local v3, "list":Landroid/widget/AbsListView;
    invoke-virtual {v3}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v7

    if-lez v7, :cond_4

    .line 59
    .end local v3    # "list":Landroid/widget/AbsListView;
    :cond_3
    :goto_1
    return v6

    .line 54
    .restart local v3    # "list":Landroid/widget/AbsListView;
    :cond_4
    invoke-virtual {v3, v8}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 55
    .local v1, "firstChild":Landroid/view/View;
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v7

    if-ltz v7, :cond_3

    .line 59
    .end local v1    # "firstChild":Landroid/view/View;
    .end local v3    # "list":Landroid/widget/AbsListView;
    :cond_5
    invoke-super {p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->canChildScrollUp()Z

    move-result v6

    goto :goto_1
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 64
    invoke-super {p0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 65
    .local v0, "ret":Z
    if-eqz v0, :cond_0

    .line 68
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/ui/XLESwipeRefreshLayout;->requestParentDisallowInterceptTouchEvent(Z)V

    .line 70
    :cond_0
    return v0
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 75
    invoke-super {p0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->requestDisallowInterceptTouchEvent(Z)V

    .line 81
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/XLESwipeRefreshLayout;->requestParentDisallowInterceptTouchEvent(Z)V

    .line 82
    return-void
.end method
