.class public final enum Lcom/microsoft/xbox/service/model/StoreBrowseType;
.super Ljava/lang/Enum;
.source "StoreBrowseType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/StoreBrowseType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/StoreBrowseType;

.field public static final enum AddOns:Lcom/microsoft/xbox/service/model/StoreBrowseType;

.field public static final enum Apps:Lcom/microsoft/xbox/service/model/StoreBrowseType;

.field public static final enum Gamepass:Lcom/microsoft/xbox/service/model/StoreBrowseType;

.field public static final enum Games:Lcom/microsoft/xbox/service/model/StoreBrowseType;

.field public static final enum Gold:Lcom/microsoft/xbox/service/model/StoreBrowseType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4
    new-instance v0, Lcom/microsoft/xbox/service/model/StoreBrowseType;

    const-string v1, "Games"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/StoreBrowseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/StoreBrowseType;->Games:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    new-instance v0, Lcom/microsoft/xbox/service/model/StoreBrowseType;

    const-string v1, "AddOns"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/model/StoreBrowseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/StoreBrowseType;->AddOns:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    new-instance v0, Lcom/microsoft/xbox/service/model/StoreBrowseType;

    const-string v1, "Apps"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/model/StoreBrowseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/StoreBrowseType;->Apps:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    new-instance v0, Lcom/microsoft/xbox/service/model/StoreBrowseType;

    const-string v1, "Gold"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/model/StoreBrowseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/StoreBrowseType;->Gold:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    new-instance v0, Lcom/microsoft/xbox/service/model/StoreBrowseType;

    const-string v1, "Gamepass"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/service/model/StoreBrowseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/StoreBrowseType;->Gamepass:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    .line 3
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/StoreBrowseType;

    sget-object v1, Lcom/microsoft/xbox/service/model/StoreBrowseType;->Games:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/model/StoreBrowseType;->AddOns:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/model/StoreBrowseType;->Apps:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/model/StoreBrowseType;->Gold:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/model/StoreBrowseType;->Gamepass:Lcom/microsoft/xbox/service/model/StoreBrowseType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/microsoft/xbox/service/model/StoreBrowseType;->$VALUES:[Lcom/microsoft/xbox/service/model/StoreBrowseType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/StoreBrowseType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lcom/microsoft/xbox/service/model/StoreBrowseType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/StoreBrowseType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/StoreBrowseType;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/microsoft/xbox/service/model/StoreBrowseType;->$VALUES:[Lcom/microsoft/xbox/service/model/StoreBrowseType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/StoreBrowseType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/StoreBrowseType;

    return-object v0
.end method
