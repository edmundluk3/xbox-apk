.class public abstract Lcom/microsoft/xbox/xle/viewmodel/PopupScreenAdapterBase;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
.source "PopupScreenAdapterBase.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;-><init>()V

    return-void
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 15
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getActionMenuDialog()Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->getScreen()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/PopupScreen;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/activity/PopupScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
