.class public Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter$ListItem;
.super Ljava/lang/Object;
.source "PopularWithFriendsScreenListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ListItem"
.end annotation


# instance fields
.field private friendsCountTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private friendsPlayedTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private gameImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field private gameTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "parent"    # Landroid/view/View;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const v0, 0x7f0e097a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter$ListItem;->gameImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 60
    const v0, 0x7f0e097b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter$ListItem;->gameTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 61
    const v0, 0x7f0e097c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter$ListItem;->friendsCountTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 62
    const v0, 0x7f0e097d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter$ListItem;->friendsPlayedTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 63
    return-void
.end method


# virtual methods
.method public updateContent(Lcom/microsoft/xbox/service/network/managers/PopularGame;)V
    .locals 6
    .param p1, "game"    # Lcom/microsoft/xbox/service/network/managers/PopularGame;

    .prologue
    const/4 v4, 0x1

    .line 66
    if-eqz p1, :cond_1

    .line 67
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter$ListItem;->gameImageView:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/PopularGame;->getImageUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 68
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter$ListItem;->gameTitleTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v3, p1, Lcom/microsoft/xbox/service/network/managers/PopularGame;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/PopularGame;->getFriendsPlayedCount()I

    move-result v0

    .line 71
    .local v0, "friendsPlayedCount":I
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/PopularGame;->getIsNowPlaying()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;->access$000()Ljava/lang/String;

    move-result-object v1

    .line 72
    .local v1, "friendsPlayedCountText":Ljava/lang/String;
    :goto_0
    if-ne v0, v4, :cond_0

    .line 73
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/PopularGame;->getIsNowPlaying()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;->access$200()Ljava/lang/String;

    move-result-object v1

    .line 76
    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter$ListItem;->friendsCountTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    new-array v3, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter$ListItem;->friendsPlayedTextView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/PopularGame;->getFriendsPlayedString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    .end local v0    # "friendsPlayedCount":I
    .end local v1    # "friendsPlayedCountText":Ljava/lang/String;
    :cond_1
    return-void

    .line 71
    .restart local v0    # "friendsPlayedCount":I
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;->access$100()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 73
    .restart local v1    # "friendsPlayedCountText":Ljava/lang/String;
    :cond_3
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/PopularWithFriendsScreenListAdapter;->access$300()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
