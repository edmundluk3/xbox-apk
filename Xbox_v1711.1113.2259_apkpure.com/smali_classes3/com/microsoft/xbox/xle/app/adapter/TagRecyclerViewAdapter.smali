.class public Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "TagRecyclerViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagHeaderViewHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;",
        ">;"
    }
.end annotation


# static fields
.field private static final DEFAULT_MARGIN:I = 0xa

.field private static final DEFAULT_PADDING:I = 0x5

.field private static final DEFAULT_RADIUS:F = 40.0f

.field private static final HEADER_LAYOUT:I = 0x7f030225
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field

.field private static final TAG_LAYOUT:I = 0x7f030226
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field


# instance fields
.field private backgroundColor:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private cornerRadius:F

.field private hasCancel:Z

.field private horizontalPadding:I

.field private selectedColor:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private tagLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

.field private tagSelectedListener:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;

.field private textAppearanceId:I
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation
.end field

.field private verticalPadding:I


# direct methods
.method public constructor <init>(IILjava/util/List;)V
    .locals 4
    .param p1, "backgroundColor"    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .param p2, "selectedColor"    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .param p3, "data"    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x5

    const/4 v2, -0x2

    const/16 v1, 0xa

    .line 64
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->textAppearanceId:I

    .line 65
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 67
    iput p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->backgroundColor:I

    .line 68
    iput p2, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->selectedColor:I

    .line 70
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->tagLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->tagLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 73
    const/high16 v0, 0x42200000    # 40.0f

    iput v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->cornerRadius:F

    .line 74
    iput v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->verticalPadding:I

    .line 75
    iput v3, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->horizontalPadding:I

    .line 77
    invoke-virtual {p0, p3}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->addAll(Ljava/util/Collection;)V

    .line 78
    return-void
.end method

.method public constructor <init>(ILjava/util/List;)V
    .locals 1
    .param p1, "backgroundColor"    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 81
    .local p2, "data":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;-><init>(IILjava/util/List;)V

    .line 82
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;)Landroid/widget/LinearLayout$LayoutParams;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->tagLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    .prologue
    .line 35
    iget v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->horizontalPadding:I

    return v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    .prologue
    .line 35
    iget v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->verticalPadding:I

    return v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    .prologue
    .line 35
    iget v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->textAppearanceId:I

    return v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    .prologue
    .line 35
    iget v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->backgroundColor:I

    return v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;)Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->tagSelectedListener:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    .prologue
    .line 35
    iget v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->selectedColor:I

    return v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->hasCancel:Z

    return v0
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;)F
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;

    .prologue
    .line 35
    iget v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->cornerRadius:F

    return v0
.end method


# virtual methods
.method public addAll(Ljava/util/Collection;)V
    .locals 1
    .param p1, "items"    # Ljava/util/Collection;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 120
    return-void
.end method

.method public getItemViewType(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 106
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 107
    .local v0, "item":Ljava/lang/Object;
    instance-of v1, v0, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;

    if-eqz v1, :cond_0

    .line 108
    const v1, 0x7f030226

    .line 115
    :goto_0
    return v1

    .line 111
    :cond_0
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 112
    const v1, 0x7f030225

    goto :goto_0

    .line 115
    :cond_1
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 35
    check-cast p1, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->onBindViewHolder(Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;I)V
    .locals 1
    .param p1, "viewHolderBase"    # Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
    .param p2, "position"    # I

    .prologue
    .line 87
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;->onBind(Ljava/lang/Object;)V

    .line 88
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 92
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 93
    .local v0, "view":Landroid/view/View;
    packed-switch p2, :pswitch_data_0

    .line 99
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown Viewtype: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 101
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 95
    :pswitch_0
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagItemViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 97
    :pswitch_1
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagHeaderViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$TagHeaderViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 93
    :pswitch_data_0
    .packed-switch 0x7f030225
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setBackgroundColor(I)V
    .locals 0
    .param p1, "backgroundColor"    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 144
    iput p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->backgroundColor:I

    .line 145
    return-void
.end method

.method public setCornerRadius(F)V
    .locals 0
    .param p1, "cornerRadius"    # F

    .prologue
    .line 135
    iput p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->cornerRadius:F

    .line 136
    return-void
.end method

.method public setHasCancel(Z)V
    .locals 0
    .param p1, "hasCancel"    # Z

    .prologue
    .line 148
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->hasCancel:Z

    .line 149
    return-void
.end method

.method public setHorizontalPadding(I)V
    .locals 0
    .param p1, "padding"    # I

    .prologue
    .line 156
    iput p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->horizontalPadding:I

    .line 157
    return-void
.end method

.method public setSelectedColor(I)V
    .locals 0
    .param p1, "selectedColor"    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 131
    iput p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->selectedColor:I

    .line 132
    return-void
.end method

.method public setTagLayoutParams(Landroid/widget/LinearLayout$LayoutParams;)V
    .locals 0
    .param p1, "tagLayoutParams"    # Landroid/widget/LinearLayout$LayoutParams;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 139
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 140
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->tagLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

    .line 141
    return-void
.end method

.method public setTagSelectedListener(Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;)V
    .locals 0
    .param p1, "tagSelectedListener"    # Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 127
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->tagSelectedListener:Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter$OnTagSelectedListener;

    .line 128
    return-void
.end method

.method public setTextAppearanceId(I)V
    .locals 0
    .param p1, "textAppearanceId"    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param

    .prologue
    .line 123
    iput p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->textAppearanceId:I

    .line 124
    return-void
.end method

.method public setVerticalPadding(I)V
    .locals 0
    .param p1, "padding"    # I

    .prologue
    .line 152
    iput p1, p0, Lcom/microsoft/xbox/xle/app/adapter/TagRecyclerViewAdapter;->verticalPadding:I

    .line 153
    return-void
.end method
