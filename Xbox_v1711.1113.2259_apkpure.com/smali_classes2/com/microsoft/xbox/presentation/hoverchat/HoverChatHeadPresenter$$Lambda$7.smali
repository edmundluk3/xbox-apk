.class final synthetic Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$7;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/BiFunction;


# static fields
.field private static final instance:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$7;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$7;

    invoke-direct {v0}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$7;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$7;->instance:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$7;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static lambdaFactory$()Lio/reactivex/functions/BiFunction;
    .locals 1

    sget-object v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$7;->instance:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter$$Lambda$7;

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadDetachedEvent;

    check-cast p2, Ljava/lang/Integer;

    invoke-static {p1, p2}, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadPresenter;->lambda$new$5(Lcom/microsoft/xbox/domain/hoverchat/ChatHeadLifecycleEventDataTypes$ChatHeadDetachedEvent;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
