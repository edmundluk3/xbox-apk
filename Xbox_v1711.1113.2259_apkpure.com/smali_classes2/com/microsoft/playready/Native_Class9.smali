.class Lcom/microsoft/playready/Native_Class9;
.super Ljava/lang/Object;
.source "Native_Class9.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/playready/Native_Class9$IHandler_1;,
        Lcom/microsoft/playready/Native_Class9$IHandler_2;,
        Lcom/microsoft/playready/Native_Class9$IHandler_3;,
        Lcom/microsoft/playready/Native_Class9$internal_class_1;,
        Lcom/microsoft/playready/Native_Class9$internal_class_2;,
        Lcom/microsoft/playready/Native_Class9$internal_class_3;,
        Lcom/microsoft/playready/Native_Class9$internal_class_4;
    }
.end annotation


# instance fields
.field protected mField1:J

.field mHandler1:Lcom/microsoft/playready/Native_Class9$IHandler_1;

.field mHandler2:Lcom/microsoft/playready/Native_Class9$IHandler_2;

.field mHandler3:Lcom/microsoft/playready/Native_Class9$IHandler_3;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 97
    invoke-static {}, Lcom/microsoft/playready/NativeLoadLibrary;->loadLibrary()V

    .line 99
    invoke-static {}, Lcom/microsoft/playready/Native_Class9;->_method_1()V

    .line 100
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/playready/Native_Class9;->mField1:J

    .line 152
    iput-object v2, p0, Lcom/microsoft/playready/Native_Class9;->mHandler1:Lcom/microsoft/playready/Native_Class9$IHandler_1;

    .line 154
    iput-object v2, p0, Lcom/microsoft/playready/Native_Class9;->mHandler2:Lcom/microsoft/playready/Native_Class9$IHandler_2;

    .line 156
    iput-object v2, p0, Lcom/microsoft/playready/Native_Class9;->mHandler3:Lcom/microsoft/playready/Native_Class9$IHandler_3;

    .line 135
    invoke-virtual {p0}, Lcom/microsoft/playready/Native_Class9;->_method_2()V

    .line 136
    return-void
.end method

.method protected static final native _method_1()V
.end method


# virtual methods
.method protected native _method_2()V
.end method

.method protected native _method_3()V
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/microsoft/playready/Native_Class9;->_method_3()V

    .line 143
    return-void
.end method

.method public native method_10(II)[B
.end method

.method public native method_11()Lcom/microsoft/playready/Native_Class10;
.end method

.method public native method_12()Lcom/microsoft/playready/Native_Class9$internal_class_4;
.end method

.method public native method_13()V
.end method

.method public native method_14()F
.end method

.method public native method_15()V
.end method

.method public native method_16()V
.end method

.method public native method_4(Ljava/lang/String;J)V
.end method

.method public native method_5(IJ)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/playready/FragmentEOSException;,
            Lcom/microsoft/playready/FragmentBOSException;
        }
    .end annotation
.end method

.method public native method_6(II)Lcom/microsoft/playready/Native_Class9$internal_class_1;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/playready/FragmentEOSException;,
            Lcom/microsoft/playready/FragmentBOSException;
        }
    .end annotation
.end method

.method public native method_7(III)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/playready/FragmentEOSException;,
            Lcom/microsoft/playready/FragmentBOSException;
        }
    .end annotation
.end method

.method public native method_8(ILjava/lang/String;)Lcom/microsoft/playready/Native_Class9$internal_class_2;
.end method

.method public native method_9(II)[B
.end method

.method r_method_1()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 212
    iget-object v0, p0, Lcom/microsoft/playready/Native_Class9;->mHandler1:Lcom/microsoft/playready/Native_Class9$IHandler_1;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/microsoft/playready/Native_Class9;->mHandler1:Lcom/microsoft/playready/Native_Class9$IHandler_1;

    invoke-interface {v0}, Lcom/microsoft/playready/Native_Class9$IHandler_1;->call()V

    .line 217
    return-void

    .line 215
    :cond_0
    new-instance v0, Lcom/microsoft/playready/DrmException;

    sget-object v1, Lcom/microsoft/playready/DrmException$XDRM_ERROR;->XDRM_E_NOTIMPL:Lcom/microsoft/playready/DrmException$XDRM_ERROR;

    invoke-direct {v0, v1}, Lcom/microsoft/playready/DrmException;-><init>(Lcom/microsoft/playready/DrmException$XDRM_ERROR;)V

    throw v0
.end method

.method r_method_2(IIJ)V
    .locals 3
    .param p1, "int1"    # I
    .param p2, "int2"    # I
    .param p3, "long1"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 221
    iget-object v0, p0, Lcom/microsoft/playready/Native_Class9;->mHandler2:Lcom/microsoft/playready/Native_Class9$IHandler_2;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/microsoft/playready/Native_Class9;->mHandler2:Lcom/microsoft/playready/Native_Class9$IHandler_2;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/microsoft/playready/Native_Class9$IHandler_2;->call(IIJ)V

    .line 226
    return-void

    .line 224
    :cond_0
    new-instance v0, Lcom/microsoft/playready/DrmException;

    sget-object v1, Lcom/microsoft/playready/DrmException$XDRM_ERROR;->XDRM_E_NOTIMPL:Lcom/microsoft/playready/DrmException$XDRM_ERROR;

    invoke-direct {v0, v1}, Lcom/microsoft/playready/DrmException;-><init>(Lcom/microsoft/playready/DrmException$XDRM_ERROR;)V

    throw v0
.end method

.method r_method_3(Lcom/microsoft/playready/Native_Class9$internal_class_3;)V
    .locals 2
    .param p1, "clasz"    # Lcom/microsoft/playready/Native_Class9$internal_class_3;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 230
    iget-object v0, p0, Lcom/microsoft/playready/Native_Class9;->mHandler3:Lcom/microsoft/playready/Native_Class9$IHandler_3;

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/microsoft/playready/Native_Class9;->mHandler3:Lcom/microsoft/playready/Native_Class9$IHandler_3;

    invoke-interface {v0, p1}, Lcom/microsoft/playready/Native_Class9$IHandler_3;->call(Lcom/microsoft/playready/Native_Class9$internal_class_3;)V

    .line 235
    return-void

    .line 233
    :cond_0
    new-instance v0, Lcom/microsoft/playready/DrmException;

    sget-object v1, Lcom/microsoft/playready/DrmException$XDRM_ERROR;->XDRM_E_NOTIMPL:Lcom/microsoft/playready/DrmException$XDRM_ERROR;

    invoke-direct {v0, v1}, Lcom/microsoft/playready/DrmException;-><init>(Lcom/microsoft/playready/DrmException$XDRM_ERROR;)V

    throw v0
.end method

.method public setHandler1(Lcom/microsoft/playready/Native_Class9$IHandler_1;)V
    .locals 0
    .param p1, "handler"    # Lcom/microsoft/playready/Native_Class9$IHandler_1;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/microsoft/playready/Native_Class9;->mHandler1:Lcom/microsoft/playready/Native_Class9$IHandler_1;

    .line 105
    return-void
.end method

.method public setHandler2(Lcom/microsoft/playready/Native_Class9$IHandler_2;)V
    .locals 0
    .param p1, "handler"    # Lcom/microsoft/playready/Native_Class9$IHandler_2;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/microsoft/playready/Native_Class9;->mHandler2:Lcom/microsoft/playready/Native_Class9$IHandler_2;

    .line 110
    return-void
.end method

.method public setHandler3(Lcom/microsoft/playready/Native_Class9$IHandler_3;)V
    .locals 0
    .param p1, "handler"    # Lcom/microsoft/playready/Native_Class9$IHandler_3;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/microsoft/playready/Native_Class9;->mHandler3:Lcom/microsoft/playready/Native_Class9$IHandler_3;

    .line 115
    return-void
.end method
