.class public abstract Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;
.super Ljava/lang/Object;
.source "MultiplayerDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract active(Ljava/lang/Boolean;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;
.end method

.method public addConnection(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;
    .locals 2
    .param p1, "connection"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 1359
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1360
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;->connection(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;

    move-result-object v0

    .line 1361
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;->ready(Ljava/lang/Boolean;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;

    move-result-object v0

    .line 1363
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;->connection(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;

    move-result-object v0

    .line 1364
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;->active(Ljava/lang/Boolean;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;

    move-result-object v0

    .line 1365
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystemSubscription;->with(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystemSubscription;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;->subscription(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystemSubscription;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;

    move-result-object v0

    goto :goto_0
.end method

.method public abstract build()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem;
.end method

.method public abstract connection(Ljava/lang/String;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;
.end method

.method public abstract description(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$SessionDescription;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;
.end method

.method public abstract ready(Ljava/lang/Boolean;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;
.end method

.method public abstract serverMeasurements(Ljava/util/Map;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;
.end method

.method public abstract subscription(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystemSubscription;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerMemberPropertiesSystem$Builder;
.end method
