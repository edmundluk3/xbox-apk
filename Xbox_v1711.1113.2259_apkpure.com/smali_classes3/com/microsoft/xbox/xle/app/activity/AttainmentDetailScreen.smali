.class public abstract Lcom/microsoft/xbox/xle/app/activity/AttainmentDetailScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "AttainmentDetailScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    .line 11
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    const-string v0, "this should be override"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 37
    const-string v0, ""

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 15
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 17
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/AttainmentDetailScreen;->onCreateContentView()V

    .line 19
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/AttainmentDetailScreenViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/AttainmentDetailScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 21
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 25
    const v0, 0x7f030043

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/AttainmentDetailScreen;->setContentView(I)V

    .line 26
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    return v0
.end method
