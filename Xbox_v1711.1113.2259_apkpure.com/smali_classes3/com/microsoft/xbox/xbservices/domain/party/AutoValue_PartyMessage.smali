.class final Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMessage;
.super Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;
.source "AutoValue_PartyMessage.java"


# instance fields
.field private final member:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

.field private final message:Ljava/lang/String;

.field private final timeStamp:Ljava/util/Calendar;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;Ljava/lang/String;Ljava/util/Calendar;)V
    .locals 2
    .param p1, "member"    # Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "timeStamp"    # Ljava/util/Calendar;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;-><init>()V

    .line 19
    if-nez p1, :cond_0

    .line 20
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null member"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 22
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMessage;->member:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    .line 23
    if-nez p2, :cond_1

    .line 24
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null message"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMessage;->message:Ljava/lang/String;

    .line 27
    if-nez p3, :cond_2

    .line 28
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null timeStamp"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMessage;->timeStamp:Ljava/util/Calendar;

    .line 31
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 62
    if-ne p1, p0, :cond_1

    .line 71
    :cond_0
    :goto_0
    return v1

    .line 65
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 66
    check-cast v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;

    .line 67
    .local v0, "that":Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMessage;->member:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;->member()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMessage;->message:Ljava/lang/String;

    .line 68
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;->message()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMessage;->timeStamp:Ljava/util/Calendar;

    .line 69
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;->timeStamp()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;
    :cond_3
    move v1, v2

    .line 71
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 76
    const/4 v0, 0x1

    .line 77
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 78
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMessage;->member:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 79
    mul-int/2addr v0, v2

    .line 80
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMessage;->message:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 81
    mul-int/2addr v0, v2

    .line 82
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMessage;->timeStamp:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 83
    return v0
.end method

.method public member()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMessage;->member:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    return-object v0
.end method

.method public message()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMessage;->message:Ljava/lang/String;

    return-object v0
.end method

.method public timeStamp()Ljava/util/Calendar;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMessage;->timeStamp:Ljava/util/Calendar;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PartyMessage{member="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMessage;->member:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMessage;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", timeStamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartyMessage;->timeStamp:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
