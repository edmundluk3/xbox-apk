.class public final enum Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;
.super Ljava/lang/Enum;
.source "ClubWhosHereScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ClubWhosHereFilter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;",
        ">;",
        "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

.field public static final enum ADMINS:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

.field public static final enum EVERYONE:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

.field public static final enum MEMBERS:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

.field public static final enum OWNER:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;


# instance fields
.field private final resId:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field private final telemetryName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 103
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    const-string v1, "EVERYONE"

    const v2, 0x7f070294

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->EVERYONE:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    .line 104
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    const-string v1, "MEMBERS"

    const v2, 0x7f070295

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->MEMBERS:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    .line 105
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    const-string v1, "ADMINS"

    const v2, 0x7f070296

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->ADMINS:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    .line 106
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    const-string v1, "OWNER"

    const v2, 0x7f070298

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->OWNER:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    .line 102
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->EVERYONE:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->MEMBERS:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->ADMINS:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->OWNER:Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    aput-object v1, v0, v7

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->$VALUES:[Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "resId"    # I
    .param p4, "telemetryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 112
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 113
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 115
    iput p3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->resId:I

    .line 116
    iput-object p4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->telemetryName:Ljava/lang/String;

    .line 117
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 102
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->$VALUES:[Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;

    return-object v0
.end method


# virtual methods
.method public getDisplayName()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 128
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    iget v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->resId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTelemetryName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubWhosHereScreenViewModel$ClubWhosHereFilter;->telemetryName:Ljava/lang/String;

    return-object v0
.end method
