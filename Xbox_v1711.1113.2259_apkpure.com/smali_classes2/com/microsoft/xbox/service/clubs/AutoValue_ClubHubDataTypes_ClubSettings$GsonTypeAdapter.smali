.class public final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_ClubHubDataTypes_ClubSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;",
        ">;"
    }
.end annotation


# instance fields
.field private final chatAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;",
            ">;"
        }
    .end annotation
.end field

.field private defaultChat:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

.field private defaultFeed:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

.field private defaultLfg:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

.field private defaultProfile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

.field private defaultRoster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

.field private defaultViewerRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

.field private final feedAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final lfgAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final profileAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final rosterAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final viewerRolesAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 27
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->defaultFeed:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    .line 28
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->defaultChat:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    .line 29
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->defaultLfg:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    .line 30
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->defaultRoster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    .line 31
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->defaultProfile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    .line 32
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->defaultViewerRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    .line 34
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->feedAdapter:Lcom/google/gson/TypeAdapter;

    .line 35
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->chatAdapter:Lcom/google/gson/TypeAdapter;

    .line 36
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->lfgAdapter:Lcom/google/gson/TypeAdapter;

    .line 37
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->rosterAdapter:Lcom/google/gson/TypeAdapter;

    .line 38
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->profileAdapter:Lcom/google/gson/TypeAdapter;

    .line 39
    const-class v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->viewerRolesAdapter:Lcom/google/gson/TypeAdapter;

    .line 40
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    .locals 9
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v8, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v8, :cond_0

    .line 90
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 91
    const/4 v0, 0x0

    .line 137
    :goto_0
    return-object v0

    .line 93
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 94
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->defaultFeed:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    .line 95
    .local v1, "feed":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;
    iget-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->defaultChat:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    .line 96
    .local v2, "chat":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;
    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->defaultLfg:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    .line 97
    .local v3, "lfg":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;
    iget-object v4, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->defaultRoster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    .line 98
    .local v4, "roster":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;
    iget-object v5, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->defaultProfile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    .line 99
    .local v5, "profile":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;
    iget-object v6, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->defaultViewerRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    .line 100
    .local v6, "viewerRoles":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 101
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v7

    .line 102
    .local v7, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v8, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v8, :cond_1

    .line 103
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 106
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {v7}, Ljava/lang/String;->hashCode()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 132
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 106
    :sswitch_0
    const-string v8, "feed"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x0

    goto :goto_2

    :sswitch_1
    const-string v8, "chat"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :sswitch_2
    const-string v8, "lfg"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x2

    goto :goto_2

    :sswitch_3
    const-string v8, "roster"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x3

    goto :goto_2

    :sswitch_4
    const-string v8, "profile"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x4

    goto :goto_2

    :sswitch_5
    const-string v8, "viewerRoles"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v0, 0x5

    goto :goto_2

    .line 108
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->feedAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "feed":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;
    check-cast v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    .line 109
    .restart local v1    # "feed":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;
    goto :goto_1

    .line 112
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->chatAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "chat":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;
    check-cast v2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    .line 113
    .restart local v2    # "chat":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;
    goto :goto_1

    .line 116
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->lfgAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "lfg":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;
    check-cast v3, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    .line 117
    .restart local v3    # "lfg":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;
    goto :goto_1

    .line 120
    :pswitch_3
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->rosterAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "roster":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;
    check-cast v4, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    .line 121
    .restart local v4    # "roster":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;
    goto/16 :goto_1

    .line 124
    :pswitch_4
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->profileAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "profile":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;
    check-cast v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    .line 125
    .restart local v5    # "profile":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;
    goto/16 :goto_1

    .line 128
    :pswitch_5
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->viewerRolesAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "viewerRoles":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;
    check-cast v6, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    .line 129
    .restart local v6    # "viewerRoles":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;
    goto/16 :goto_1

    .line 136
    .end local v7    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 137
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings;

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings;-><init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;)V

    goto/16 :goto_0

    .line 106
    :sswitch_data_0
    .sparse-switch
        -0x37255175 -> :sswitch_3
        -0x12717657 -> :sswitch_4
        0x1a22d -> :sswitch_2
        0x2e9358 -> :sswitch_1
        0x2fe59e -> :sswitch_0
        0x424ae7cb -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultChat(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultChat"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->defaultChat:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    .line 47
    return-object p0
.end method

.method public setDefaultFeed(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultFeed"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->defaultFeed:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    .line 43
    return-object p0
.end method

.method public setDefaultLfg(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultLfg"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->defaultLfg:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    .line 51
    return-object p0
.end method

.method public setDefaultProfile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultProfile"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->defaultProfile:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    .line 59
    return-object p0
.end method

.method public setDefaultRoster(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultRoster"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->defaultRoster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    .line 55
    return-object p0
.end method

.method public setDefaultViewerRoles(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultViewerRoles"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->defaultViewerRoles:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    .line 63
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    if-nez p2, :cond_0

    .line 69
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 86
    :goto_0
    return-void

    .line 72
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 73
    const-string v0, "feed"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->feedAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 75
    const-string v0, "chat"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 76
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->chatAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 77
    const-string v0, "lfg"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->lfgAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 79
    const-string v0, "roster"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->rosterAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 81
    const-string v0, "profile"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->profileAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 83
    const-string v0, "viewerRoles"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->viewerRolesAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 85
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    check-cast p2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)V

    return-void
.end method
