.class public abstract Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "MultiViewModelBase.java"


# instance fields
.field protected viewModels:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZZ)V
    .locals 1
    .param p1, "showNoNetworkPopup"    # Z
    .param p2, "onlyProcessExceptionsAndShowToastsWhenActive"    # Z

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(ZZ)V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;->viewModels:Ljava/util/ArrayList;

    .line 35
    return-void
.end method


# virtual methods
.method protected adapterUpdateView()V
    .locals 3

    .prologue
    .line 201
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;->viewModels:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 202
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->updateAdapter(Z)V

    goto :goto_0

    .line 204
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public forceUpdateViewImmediately()V
    .locals 3

    .prologue
    .line 111
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;->viewModels:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 112
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->forceUpdateViewImmediately()V

    goto :goto_0

    .line 114
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public getTestMenuButtons()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/appbar/AppBarMenuButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    const/4 v0, 0x0

    return-object v0
.end method

.method public final load()V
    .locals 3

    .prologue
    .line 127
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;->viewModels:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 128
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->load()V

    goto :goto_0

    .line 130
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 151
    return-void
.end method

.method public onApplicationPause()V
    .locals 3

    .prologue
    .line 76
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;->viewModels:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 77
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onApplicationPause()V

    goto :goto_0

    .line 79
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public onApplicationResume()V
    .locals 3

    .prologue
    .line 83
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;->viewModels:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 84
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onApplicationResume()V

    goto :goto_0

    .line 86
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 97
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;->viewModels:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 98
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onDestroy()V

    goto :goto_0

    .line 100
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;->cancelLaunchTimeout()V

    .line 69
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;->viewModels:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 70
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onPause()V

    goto :goto_0

    .line 72
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public onRehydrate()V
    .locals 3

    .prologue
    .line 117
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;->viewModels:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 118
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onRehydrate()V

    goto :goto_0

    .line 120
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 90
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;->viewModels:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 91
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onResume()V

    goto :goto_0

    .line 93
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public onSetActive()V
    .locals 3

    .prologue
    .line 170
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;->isActive:Z

    .line 171
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;->viewModels:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 172
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onSetActive()V

    goto :goto_0

    .line 174
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public onSetInactive()V
    .locals 3

    .prologue
    .line 179
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->dismissToast()V

    .line 180
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;->isActive:Z

    .line 181
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;->viewModels:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 182
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onSetInactive()V

    goto :goto_0

    .line 184
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 3

    .prologue
    .line 45
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;->isForeground:Z

    .line 46
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;->viewModels:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 47
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onStart()V

    goto :goto_0

    .line 50
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;->onStartOverride()V

    .line 51
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 136
    return-void
.end method

.method public final onStop()V
    .locals 3

    .prologue
    .line 58
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;->isForeground:Z

    .line 59
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;->viewModels:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 60
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onStop()V

    goto :goto_0

    .line 63
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;->onStopOverride()V

    .line 64
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 142
    return-void
.end method

.method public onTombstone()V
    .locals 3

    .prologue
    .line 104
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;->viewModels:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 105
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onTombstone()V

    goto :goto_0

    .line 107
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method protected onUpdateFinished()V
    .locals 3

    .prologue
    .line 162
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onUpdateFinished()V

    .line 163
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;->viewModels:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 164
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onUpdateFinished()V

    goto :goto_0

    .line 166
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 156
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/MultiViewModelBase;->viewModels:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 157
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    goto :goto_0

    .line 159
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method
