.class Lcom/microsoft/xbox/smartglass/controls/JsonActiveTitle;
.super Lorg/json/JSONObject;
.source "JsonActiveTitle.java"


# direct methods
.method public constructor <init>(ILcom/microsoft/xbox/smartglass/ActiveTitleLocation;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "titleId"    # I
    .param p2, "titleLocation"    # Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .param p3, "hasFocus"    # Z
    .param p4, "productId"    # Ljava/lang/String;
    .param p5, "aumId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 14
    const-string v0, "titleId"

    invoke-static {p1}, Lcom/microsoft/xbox/smartglass/controls/Json;->unsignedInt(I)J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/JsonActiveTitle;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 15
    const-string v0, "titleLocation"

    invoke-virtual {p0, v0, p2}, Lcom/microsoft/xbox/smartglass/controls/JsonActiveTitle;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 16
    const-string v0, "hasFocus"

    invoke-virtual {p0, v0, p3}, Lcom/microsoft/xbox/smartglass/controls/JsonActiveTitle;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 17
    const-string v0, "productId"

    invoke-virtual {p0, v0, p4}, Lcom/microsoft/xbox/smartglass/controls/JsonActiveTitle;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 18
    const-string v0, "aumId"

    invoke-virtual {p0, v0, p5}, Lcom/microsoft/xbox/smartglass/controls/JsonActiveTitle;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19
    return-void
.end method
