.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;
.super Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
.source "EDSV2AppMediaItem.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/gson/PostProcessingEnablerGson$PostProcessableGson;


# static fields
.field private static final ItemType:I = 0x3d


# instance fields
.field public DeveloperName:Ljava/lang/String;

.field public PublisherName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 25
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 1
    .param p1, "source"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 29
    instance-of v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 30
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->DeveloperName:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->DeveloperName:Ljava/lang/String;

    move-object v0, p1

    .line 31
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->PublisherName:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->PublisherName:Ljava/lang/String;

    .line 32
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->MediaItemType:Ljava/lang/String;

    .line 35
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->getMediaType()I

    move-result v0

    if-nez v0, :cond_1

    .line 36
    const/16 v0, 0x2328

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->setMediaItemTypeFromInt(I)V

    .line 38
    :cond_1
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)V
    .locals 4
    .param p1, "provider"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 56
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 58
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->titleId:J

    .line 59
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->ID:Ljava/lang/String;

    .line 60
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->Name:Ljava/lang/String;

    .line 61
    const/16 v1, 0x3d

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->setMediaItemTypeFromInt(I)V

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 64
    .local v0, "providers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;>;"
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->setProviders(Ljava/util/ArrayList;)V

    .line 65
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/sls/Title;)V
    .locals 2
    .param p1, "title"    # Lcom/microsoft/xbox/service/model/sls/Title;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 45
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 46
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/sls/Title;->IsApplication()Z

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 47
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/sls/Title;->getTitleId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->titleId:J

    .line 48
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/sls/Title;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->Name:Ljava/lang/String;

    .line 50
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/service/model/sls/Title;->getImageUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->setImageUrl(Ljava/lang/String;)V

    .line 51
    const/16 v0, 0x3d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->setMediaItemTypeFromInt(I)V

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->setProviders(Ljava/util/ArrayList;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V
    .locals 0
    .param p1, "bigCatProduct"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)V

    .line 42
    return-void
.end method


# virtual methods
.method public getProviders()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->providers:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->providers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 80
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->providers:Ljava/util/ArrayList;

    .line 81
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;-><init>()V

    .line 82
    .local v0, "appProvider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->getTitleId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setTitleId(J)V

    .line 83
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setName(Ljava/lang/String;)V

    .line 84
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setCanonicalId(Ljava/lang/String;)V

    .line 85
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "ms-xbl-%08X://default/"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v6, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->titleId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->DeepLinkInfo:Ljava/lang/String;

    .line 86
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->Images:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setImages(Ljava/util/ArrayList;)V

    .line 88
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->providers:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    .end local v0    # "appProvider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->providers:Ljava/util/ArrayList;

    return-object v1
.end method

.method public getTitleId()J
    .locals 4

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->titleId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 70
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->titleId:J

    .line 74
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->nowPlayingTitleId:J

    goto :goto_0
.end method

.method public postProcess()V
    .locals 8

    .prologue
    .line 98
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->providers:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 99
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->providers:Ljava/util/ArrayList;

    .line 102
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;-><init>()V

    .line 103
    .local v0, "appProvider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->getTitleId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setTitleId(J)V

    .line 104
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setName(Ljava/lang/String;)V

    .line 105
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setCanonicalId(Ljava/lang/String;)V

    .line 106
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "ms-xbl-%08X://default/"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v6, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->titleId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->DeepLinkInfo:Ljava/lang/String;

    .line 107
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->Images:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setImages(Ljava/util/ArrayList;)V

    .line 109
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2AppMediaItem;->providers:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    return-void
.end method
