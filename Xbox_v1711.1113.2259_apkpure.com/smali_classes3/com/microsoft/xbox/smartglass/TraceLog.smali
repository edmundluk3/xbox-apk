.class public Lcom/microsoft/xbox/smartglass/TraceLog;
.super Lcom/microsoft/xbox/smartglass/Listenable;
.source "TraceLog.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/smartglass/Listenable",
        "<",
        "Lcom/microsoft/xbox/smartglass/TraceLogListener;",
        ">;"
    }
.end annotation


# instance fields
.field private _pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;


# direct methods
.method constructor <init>()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/Listenable;-><init>()V

    .line 12
    new-instance v0, Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/TraceLog;->initialize()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/microsoft/xbox/smartglass/RefTPtr;-><init>(J)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/TraceLog;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    .line 13
    return-void
.end method

.method private native getLevel(J)I
.end method

.method private native getRemoteViewer(J)Ljava/lang/String;
.end method

.method private native getRemoteViewerEnabled(J)Z
.end method

.method private native initialize()J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation
.end method

.method private native setLevel(JI)V
.end method

.method private native setRemoteViewer(JLjava/lang/String;)V
.end method

.method private native setRemoteViewerEnabled(JZ)V
.end method

.method private native traceMessage(JILjava/lang/String;)V
.end method


# virtual methods
.method public getLevel()Lcom/microsoft/xbox/smartglass/TraceLogLevel;
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/TraceLog;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/TraceLog;->getLevel(J)I

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/smartglass/TraceLogLevel;->fromInt(I)Lcom/microsoft/xbox/smartglass/TraceLogLevel;

    move-result-object v0

    return-object v0
.end method

.method public getRemoteViewer()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/TraceLog;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/TraceLog;->getRemoteViewer(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRemoteViewerEnabled()Z
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/TraceLog;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/smartglass/TraceLog;->getRemoteViewerEnabled(J)Z

    move-result v0

    return v0
.end method

.method protected onTraceMessageReceived(Ljava/lang/String;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/microsoft/xbox/smartglass/TraceLog;->cloneListeners()Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/TraceLogListener;

    .line 76
    .local v0, "listener":Lcom/microsoft/xbox/smartglass/TraceLogListener;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/smartglass/TraceLogListener;->onTraceMessageReceived(Ljava/lang/String;)V

    goto :goto_0

    .line 78
    .end local v0    # "listener":Lcom/microsoft/xbox/smartglass/TraceLogListener;
    :cond_0
    return-void
.end method

.method public setLevel(Lcom/microsoft/xbox/smartglass/TraceLogLevel;)V
    .locals 3
    .param p1, "level"    # Lcom/microsoft/xbox/smartglass/TraceLogLevel;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/TraceLog;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/smartglass/TraceLogLevel;->getValue()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/microsoft/xbox/smartglass/TraceLog;->setLevel(JI)V

    .line 40
    return-void
.end method

.method public setRemoteViewer(Ljava/lang/String;)V
    .locals 2
    .param p1, "viewer"    # Ljava/lang/String;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/TraceLog;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1}, Lcom/microsoft/xbox/smartglass/TraceLog;->setRemoteViewer(JLjava/lang/String;)V

    .line 68
    return-void
.end method

.method public setRemoteViewerEnabled(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/TraceLog;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1}, Lcom/microsoft/xbox/smartglass/TraceLog;->setRemoteViewerEnabled(JZ)V

    .line 54
    return-void
.end method

.method public write(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/TraceLogLevel;)V
    .locals 6
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "level"    # Lcom/microsoft/xbox/smartglass/TraceLogLevel;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/TraceLog;->_pProxy:Lcom/microsoft/xbox/smartglass/RefTPtr;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/RefTPtr;->get()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/microsoft/xbox/smartglass/TraceLogLevel;->getValue()I

    move-result v2

    const-string v3, "{ \"text\": \"%s\" }"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/microsoft/xbox/smartglass/TraceLog;->traceMessage(JILjava/lang/String;)V

    .line 23
    return-void
.end method
