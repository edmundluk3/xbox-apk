.class public Lcom/microsoft/xbox/service/network/managers/AutoSuggestServiceManager;
.super Ljava/lang/Object;
.source "AutoSuggestServiceManager.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private GetAutoSuggestList(Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$AutoSuggestResult;)Ljava/util/ArrayList;
    .locals 7
    .param p1, "autoSuggestData"    # Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$AutoSuggestResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$AutoSuggestResult;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 73
    if-eqz p1, :cond_0

    iget-object v4, p1, Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$AutoSuggestResult;->AS:Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$AS;

    if-eqz v4, :cond_0

    iget-object v4, p1, Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$AutoSuggestResult;->AS:Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$AS;

    iget-object v4, v4, Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$AS;->Results:Ljava/util/ArrayList;

    if-nez v4, :cond_2

    :cond_0
    move-object v0, v3

    .line 85
    :cond_1
    :goto_0
    return-object v0

    .line 76
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 77
    .local v0, "autoSuggestList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v4, p1, Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$AutoSuggestResult;->AS:Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$AS;

    iget-object v4, v4, Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$AS;->Results:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$Results;

    .line 78
    .local v1, "result":Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$Results;
    iget-object v5, v1, Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$Results;->Suggests:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$Suggests;

    .line 79
    .local v2, "suggestion":Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$Suggests;
    iget-object v6, v2, Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$Suggests;->Txt:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 80
    iget-object v6, v2, Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$Suggests;->Txt:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 85
    .end local v1    # "result":Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$Results;
    .end local v2    # "suggestion":Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$Suggests;
    :cond_5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-gtz v4, :cond_1

    move-object v0, v3

    goto :goto_0
.end method


# virtual methods
.method public getAutoSuggestData(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 10
    .param p1, "searchText"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 25
    const-string v5, "AutoSuggestServiceManager"

    const-string v6, "get all autosuggest data..."

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getAutoSuggestUrlFormat()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    aput-object p1, v7, v8

    const/4 v8, 0x2

    .line 28
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getAutoSuggestdDataSource()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    .line 27
    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 30
    .local v4, "url":Ljava/lang/String;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 31
    .local v2, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v5, Lorg/apache/http/message/BasicHeader;

    const-string v6, "Accept"

    const-string v7, "application/json"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 33
    const/4 v3, 0x0

    .line 35
    .local v3, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    :try_start_0
    invoke-static {v4, v2}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v3

    .line 37
    const/16 v5, 0xc8

    iget v6, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v5, v6, :cond_2

    .line 40
    :try_start_1
    const-string v5, "AutoSuggestServiceManager"

    const-string v6, "Successfully retrieved auto suggest info."

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Completed:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 43
    iget-object v5, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    const-class v6, Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$AutoSuggestResult;

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$AutoSuggestResult;

    .line 45
    .local v0, "autoSuggestData":Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$AutoSuggestResult;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/AutoSuggestServiceManager;->GetAutoSuggestList(Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$AutoSuggestResult;)Ljava/util/ArrayList;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    .line 65
    if-eqz v3, :cond_0

    .line 66
    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    .line 45
    :cond_0
    return-object v5

    .line 46
    .end local v0    # "autoSuggestData":Lcom/microsoft/xbox/service/network/managers/IAutoSuggestServiceManager$AutoSuggestResult;
    :catch_0
    move-exception v1

    .line 47
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xfb9

    invoke-direct {v5, v6, v7, v1}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v5
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 57
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 58
    .restart local v1    # "e":Ljava/lang/Exception;
    :try_start_3
    instance-of v5, v1, Lcom/microsoft/xbox/toolkit/XLEException;

    if-nez v5, :cond_3

    .line 59
    const-string v5, "AutoSuggestServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to get autosuggestions "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xfb8

    invoke-direct {v5, v6, v7}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 65
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    if-eqz v3, :cond_1

    .line 66
    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->close()V

    :cond_1
    throw v5

    .line 51
    :cond_2
    :try_start_4
    const-string v5, "AutoSuggestServiceManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "auto suggest endpoint returned error "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v3, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    sget-object v5, Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;->Error:Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;

    invoke-static {v4, v5}, Lcom/microsoft/xle/test/interop/TestInterop;->onServiceManagerActivity(Ljava/lang/String;Lcom/microsoft/xle/test/interop/TestInterop$ServiceManagerActivityStateChange;)V

    .line 55
    new-instance v5, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v6, 0xfb8

    invoke-direct {v5, v6, v7}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v5
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 62
    .restart local v1    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_5
    check-cast v1, Lcom/microsoft/xbox/toolkit/XLEException;

    .end local v1    # "e":Ljava/lang/Exception;
    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method
