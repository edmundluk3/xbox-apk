.class public Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "ClubSearchScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchAsyncTask;,
        Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchSuggestAsyncTask;
    }
.end annotation


# static fields
.field private static final NUM_RANDOM_TAGS:I = 0x8


# instance fields
.field private currentSearchInput:Ljava/lang/String;

.field private currentSearchParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private currentSuggestParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private volatile isLoadingRecents:Z

.field private volatile isLoadingTags:Z

.field private volatile isSearching:Z

.field private randomTags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
            ">;"
        }
    .end annotation
.end field

.field private final recentsModel:Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;

.field private final searchAction:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Landroid/support/v4/util/Pair",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSearchResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final searchResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;",
            ">;"
        }
    .end annotation
.end field

.field private selectedTags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
            ">;"
        }
    .end annotation
.end field

.field private selectedTitles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

.field private final suggestionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestResult;",
            ">;>;"
        }
    .end annotation
.end field

.field private final suggestionSearchAction:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Landroid/support/v4/util/Pair",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;",
            "Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;",
            ">;>;"
        }
    .end annotation
.end field

.field private final titlesSelectedAction:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 2
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 92
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubSearchScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 93
    invoke-static {}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->getInstance()Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->recentsModel:Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;

    .line 94
    sget-object v0, Lcom/microsoft/xbox/service/model/SocialTagModel;->INSTANCE:Lcom/microsoft/xbox/service/model/SocialTagModel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

    .line 95
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->suggestionMap:Ljava/util/Map;

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->searchResults:Ljava/util/List;

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectedTitles:Ljava/util/List;

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectedTags:Ljava/util/List;

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->randomTags:Ljava/util/List;

    .line 100
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->suggestionSearchAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 101
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->searchAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 102
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->titlesSelectedAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 103
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSearchInput:Ljava/lang/String;

    .line 104
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;Landroid/support/v4/util/Pair;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->onSuggestionSearchCompleted(Landroid/support/v4/util/Pair;)V

    return-void
.end method

.method static synthetic access$lambda$1(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;Landroid/support/v4/util/Pair;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->onSearchCompleted(Landroid/support/v4/util/Pair;)V

    return-void
.end method

.method static synthetic access$lambda$2(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->setSelectedTitles(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$lambda$3(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->setSelectedTags(Ljava/util/List;)V

    return-void
.end method

.method private buildSearchParams()V
    .locals 2

    .prologue
    .line 254
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->isSearchParamsEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 255
    invoke-static {}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;->builder()Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSearchInput:Ljava/lang/String;

    .line 256
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;->query(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;

    move-result-object v0

    .line 257
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->getSelectedTagIds()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;->tags(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;

    move-result-object v0

    .line 258
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->getSelectedTitleIds()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;->titles(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSearchParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    .line 262
    :goto_0
    return-void

    .line 260
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSearchParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    goto :goto_0
.end method

.method private buildSearchSuggestions()V
    .locals 2

    .prologue
    .line 265
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->isSearchParamsEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 266
    invoke-static {}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;->builder()Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSearchInput:Ljava/lang/String;

    .line 267
    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;->query(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;

    move-result-object v0

    .line 268
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->getSelectedTagIds()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;->tags(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;

    move-result-object v0

    .line 269
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->getSelectedTitleIds()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;->titles(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSuggestParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    .line 273
    :goto_0
    return-void

    .line 271
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSuggestParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    goto :goto_0
.end method

.method private declared-synchronized getSelectedTagIds()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 244
    monitor-enter p0

    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectedTags:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 246
    .local v1, "tagIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectedTags:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;

    .line 247
    .local v0, "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
    invoke-interface {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 244
    .end local v0    # "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
    .end local v1    # "tagIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 250
    .restart local v1    # "tagIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    monitor-exit p0

    return-object v1
.end method

.method private declared-synchronized getSelectedTitleIds()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 234
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectedTitles:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 236
    .local v0, "titleIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectedTitles:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;

    .line 237
    .local v1, "titleInfo":Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->getTitleId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 234
    .end local v0    # "titleIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v1    # "titleInfo":Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 240
    .restart local v0    # "titleIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    monitor-exit p0

    return-object v0
.end method

.method private isSearchParamsEmpty()Z
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSearchInput:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectedTitles:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectedTags:Ljava/util/List;

    .line 277
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 276
    :goto_0
    return v0

    .line 277
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSearchSuggestionsEnabled()Z
    .locals 2

    .prologue
    .line 170
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->INSTANCE:Lcom/microsoft/xbox/toolkit/network/NetworkStats;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/NetworkStats;->getCurrentQuality()Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->POOR:Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;->isGreaterThan(Lcom/microsoft/xbox/toolkit/network/NetworkStats$NetworkQuality;)Z

    move-result v0

    return v0
.end method

.method private declared-synchronized onSearchCompleted(Landroid/support/v4/util/Pair;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/util/Pair",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSearchResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 430
    .local p1, "result":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSearchResponse;>;"
    monitor-enter p0

    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->isSearching:Z

    .line 432
    if-eqz p1, :cond_2

    iget-object v2, p1, Landroid/support/v4/util/Pair;->second:Ljava/lang/Object;

    if-eqz v2, :cond_2

    .line 433
    iget-object v2, p1, Landroid/support/v4/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSearchResponse;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSearchResponse;->clubs()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/common/collect/ImmutableList;->iterator()Lcom/google/common/collect/UnmodifiableIterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 434
    .local v0, "searchResult":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 435
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->tags()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;->value()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    if-eqz v3, :cond_1

    new-instance v1, Ljava/util/HashSet;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->tags()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ListSetting;->value()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 436
    .local v1, "tags":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->searchResults:Ljava/util/List;

    sget-object v4, Lcom/microsoft/xbox/service/model/SocialTagModel;->INSTANCE:Lcom/microsoft/xbox/service/model/SocialTagModel;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 439
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/model/SocialTagModel;->getTags(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 437
    invoke-static {v0, v4}, Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;->with(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Ljava/util/List;)Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;

    move-result-object v4

    .line 436
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 430
    .end local v0    # "searchResult":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .end local v1    # "tags":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 435
    .restart local v0    # "searchResult":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :cond_1
    :try_start_1
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    goto :goto_1

    .line 443
    .end local v0    # "searchResult":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    :cond_2
    const v2, 0x7f070b69

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->showError(I)V

    .line 446
    :cond_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->updateAdapter()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 447
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized onSuggestionSearchCompleted(Landroid/support/v4/util/Pair;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/util/Pair",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;",
            "Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 419
    .local p1, "result":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;>;"
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v1, p1, Landroid/support/v4/util/Pair;->second:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 420
    iget-object v1, p1, Landroid/support/v4/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestionResultSet;->results()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 422
    .local v0, "results":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestResult;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 423
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->suggestionMap:Ljava/util/Map;

    iget-object v2, p1, Landroid/support/v4/util/Pair;->first:Ljava/lang/Object;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 424
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->updateAdapter()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 427
    .end local v0    # "results":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestResult;>;"
    :cond_0
    monitor-exit p0

    return-void

    .line 419
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized setSelectedTags(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 214
    .local p1, "selectedTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;>;"
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 216
    .local v0, "newSelectedTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectedTags:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 231
    :goto_0
    monitor-exit p0

    return-void

    .line 219
    :cond_0
    :try_start_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectedTags:Ljava/util/List;

    .line 221
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->buildSearchSuggestions()V

    .line 222
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->buildSearchParams()V

    .line 224
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSearchParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    if-nez v1, :cond_1

    .line 225
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->updateAdapter()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 214
    .end local v0    # "newSelectedTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;>;"
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 227
    .restart local v0    # "newSelectedTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;>;"
    :cond_1
    const/4 v1, 0x1

    :try_start_2
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->isSearching:Z

    .line 228
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->searchResults:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 229
    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchAsyncTask;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSearchParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->searchAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchAsyncTask;-><init>(Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchAsyncTask;->load(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private declared-synchronized setSelectedTitles(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 194
    .local p1, "selectedTitles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;>;"
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 196
    .local v0, "newSelectedTitles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectedTitles:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 211
    :goto_0
    monitor-exit p0

    return-void

    .line 199
    :cond_0
    :try_start_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectedTitles:Ljava/util/List;

    .line 201
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->buildSearchSuggestions()V

    .line 202
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->buildSearchParams()V

    .line 204
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSearchParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    if-nez v1, :cond_1

    .line 205
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->updateAdapter()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 194
    .end local v0    # "newSelectedTitles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;>;"
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 207
    .restart local v0    # "newSelectedTitles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;>;"
    :cond_1
    const/4 v1, 0x1

    :try_start_2
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->isSearching:Z

    .line 208
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->searchResults:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 209
    new-instance v1, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchAsyncTask;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSearchParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->searchAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchAsyncTask;-><init>(Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchAsyncTask;->load(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized addSuggestionTerm(Ljava/lang/String;)V
    .locals 5
    .param p1, "term"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 142
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 144
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->isSearchSuggestionsEnabled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 167
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 150
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSuggestParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    if-nez v2, :cond_2

    .line 151
    invoke-static {}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;->builder()Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;->query(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSuggestParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    .line 152
    const/4 v1, 0x1

    .line 159
    .local v1, "suggestParamsChanged":Z
    :goto_1
    if-eqz v1, :cond_0

    .line 160
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->suggestionMap:Ljava/util/Map;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSuggestParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 161
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->updateAdapter()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 142
    .end local v1    # "suggestParamsChanged":Z
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 154
    :cond_2
    :try_start_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSuggestParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;->query(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    move-result-object v0

    .line 155
    .local v0, "newSuggestParams":Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSuggestParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 156
    .restart local v1    # "suggestParamsChanged":Z
    :goto_2
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSuggestParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    goto :goto_1

    .line 155
    .end local v1    # "suggestParamsChanged":Z
    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    .line 163
    .end local v0    # "newSuggestParams":Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;
    .restart local v1    # "suggestParamsChanged":Z
    :cond_4
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->suggestionMap:Ljava/util/Map;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSuggestParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    new-instance v2, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchSuggestAsyncTask;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSuggestParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->suggestionSearchAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchSuggestAsyncTask;-><init>(Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchSuggestAsyncTask;->load(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public declared-synchronized getCurrentSearchParams()Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 133
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSearchParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getCurrentSearchResults()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/clubs/ClubCardModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 314
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->searchResults:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getCurrentSuggestions()Ljava/util/List;
    .locals 5
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 292
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSuggestParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    if-eqz v3, :cond_2

    .line 293
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->suggestionMap:Ljava/util/Map;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSuggestParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 295
    .local v2, "suggestionResults":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestResult;>;"
    if-nez v2, :cond_1

    .line 296
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 308
    .end local v2    # "suggestionResults":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestResult;>;"
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v1

    .line 299
    .restart local v2    # "suggestionResults":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestResult;>;"
    :cond_1
    :try_start_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 301
    .local v1, "suggestionNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestResult;

    .line 302
    .local v0, "suggestion":Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestResult;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestResult;->result()Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchResult;->name()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 292
    .end local v0    # "suggestion":Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestResult;
    .end local v1    # "suggestionNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "suggestionResults":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSuggestResult;>;"
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 308
    :cond_2
    :try_start_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    goto :goto_0
.end method

.method public getRandomSystemTags()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 287
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->randomTags:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRecentlyPlayedTitles()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 282
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->recentsModel:Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->getRecentTitles()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getSelectedTagNames()Ljava/util/List;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 330
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectedTags:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 332
    .local v0, "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectedTags:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;

    .line 333
    .local v1, "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
    invoke-interface {v1}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;->getDisplayText()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 330
    .end local v0    # "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v1    # "tag":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 336
    .restart local v0    # "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    monitor-exit p0

    return-object v0
.end method

.method public getSelectedTags()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectedTags:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getSelectedTagsCount()I
    .locals 1

    .prologue
    .line 344
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectedTags:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSelectedTitleNames()Ljava/util/List;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 319
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectedTitles:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 321
    .local v0, "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectedTitles:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;

    .line 322
    .local v1, "titleInfo":Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 319
    .end local v0    # "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v1    # "titleInfo":Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 325
    .restart local v0    # "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized getSelectedTitlesCount()I
    .locals 1

    .prologue
    .line 340
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectedTitles:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 2

    .prologue
    .line 379
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->isSearching:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized load(Z)V
    .locals 1
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 384
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 385
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->isLoadingRecents:Z

    .line 386
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->isLoadingTags:Z

    .line 388
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->recentsModel:Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->loadRecentlyPlayed(Z)V

    .line 389
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/SocialTagModel;->loadSystemTagsAsync(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 390
    monitor-exit p0

    return-void

    .line 384
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized navigateToTagPicker()V
    .locals 4

    .prologue
    .line 353
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackDiscoverSearchTags()V

    .line 354
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectedTags:Ljava/util/List;

    .line 355
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopyWritable(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;)Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;

    move-result-object v2

    .line 357
    invoke-static {}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;->systemOnly()Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;

    move-result-object v3

    .line 354
    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showClubTagPickerDialog(Ljava/util/List;Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 358
    monitor-exit p0

    return-void

    .line 353
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized navigateToTitlePicker()V
    .locals 5

    .prologue
    .line 348
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackDiscoverSearchGames()V

    .line 349
    const-class v0, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreen;

    new-instance v1, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectedTitles:Ljava/util/List;

    const/16 v3, 0x19

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->titlesSelectedAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/titlePicker/TitlePickerScreenViewModel$TitlePickerScreenParameters;-><init>(Ljava/util/List;ILcom/microsoft/xbox/toolkit/generics/Action;)V

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 350
    monitor-exit p0

    return-void

    .line 348
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 374
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubSearchScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 375
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->recentsModel:Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 363
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SocialTagModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 364
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->recentsModel:Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/RecentlyPlayedModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 369
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/SocialTagModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 370
    return-void
.end method

.method public selectTag(Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)V
    .locals 2
    .param p1, "socialTag"    # Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 123
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 125
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectedTags:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 126
    .local v0, "newSelectedTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;>;"
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->setSelectedTags(Ljava/util/List;)V

    .line 129
    return-void
.end method

.method public selectTitle(Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;)V
    .locals 3
    .param p1, "titleData"    # Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 112
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectedTitles:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 115
    .local v0, "newSelectedTitles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;>;"
    new-instance v1, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;

    invoke-direct {v1, p1}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;-><init>(Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;)V

    .line 116
    .local v1, "selectedTitle":Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->setSelected(Z)V

    .line 117
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->setSelectedTitles(Ljava/util/List;)V

    .line 120
    return-void
.end method

.method public declared-synchronized setSearchTerm(Ljava/lang/String;)V
    .locals 3
    .param p1, "term"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 174
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSearchInput:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    :goto_0
    monitor-exit p0

    return-void

    .line 179
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSearchInput:Ljava/lang/String;

    .line 181
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->buildSearchParams()V

    .line 183
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSearchParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    if-nez v0, :cond_1

    .line 184
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->updateAdapter()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 174
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 186
    :cond_1
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->isSearching:Z

    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->searchResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 188
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSearchParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;->tags()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSearchParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;->titles()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackDiscoverExecuteSearch(Ljava/util/List;Ljava/util/List;)V

    .line 189
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchAsyncTask;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->currentSearchParams:Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->searchAction:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchAsyncTask;-><init>(Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel$ClubSearchAsyncTask;->load(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    const/4 v2, 0x0

    .line 394
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 396
    if-eqz p1, :cond_1

    .line 397
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 398
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 399
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v0

    .line 401
    .local v0, "updateType":Lcom/microsoft/xbox/service/model/UpdateType;
    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->TitleFeedLoaded:Lcom/microsoft/xbox/service/model/UpdateType;

    if-ne v0, v1, :cond_2

    .line 402
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->isLoadingRecents:Z

    .line 407
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->isLoadingRecents:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->isLoadingTags:Z

    if-nez v1, :cond_1

    .line 408
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 410
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->randomTags:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 411
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->randomTags:Ljava/util/List;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->socialTagModel:Lcom/microsoft/xbox/service/model/SocialTagModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/SocialTagModel;->getSystemTags()Ljava/util/List;

    move-result-object v2

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->selectUniqueRandomN(Ljava/util/List;I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 413
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->updateAdapter()V

    .line 416
    .end local v0    # "updateType":Lcom/microsoft/xbox/service/model/UpdateType;
    :cond_1
    return-void

    .line 403
    .restart local v0    # "updateType":Lcom/microsoft/xbox/service/model/UpdateType;
    :cond_2
    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->GetSystemTags:Lcom/microsoft/xbox/service/model/UpdateType;

    if-ne v0, v1, :cond_0

    .line 404
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->isLoadingTags:Z

    goto :goto_0
.end method
