.class abstract Lcom/microsoft/xbox/smartglass/controls/Media;
.super Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;
.source "Media.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/smartglass/controls/Media$SessionListener;
    }
.end annotation


# static fields
.field public static final ComponentName:Ljava/lang/String; = "Media"

.field private static final GetStateMethod:Ljava/lang/String; = "GetState"

.field private static final SeekMethod:Ljava/lang/String; = "Seek"

.field protected static final SendMethod:Ljava/lang/String; = "Send"


# instance fields
.field private _currentState:Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;

.field private final _listener:Lcom/microsoft/xbox/smartglass/controls/Media$SessionListener;


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V
    .locals 2
    .param p1, "canvasState"    # Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    .prologue
    .line 36
    const-string v0, "Media"

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    .line 38
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/Media$SessionListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/smartglass/controls/Media$SessionListener;-><init>(Lcom/microsoft/xbox/smartglass/controls/Media;Lcom/microsoft/xbox/smartglass/controls/Media$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Media;->_listener:Lcom/microsoft/xbox/smartglass/controls/Media$SessionListener;

    .line 39
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Media;->_listener:Lcom/microsoft/xbox/smartglass/controls/Media$SessionListener;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->addListener(Ljava/lang/Object;)V

    .line 41
    const-string v0, "GetState"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/Media;->registerMethod(Ljava/lang/String;)V

    .line 42
    const-string v0, "Seek"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/Media;->registerMethod(Ljava/lang/String;)V

    .line 43
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/smartglass/controls/Media;)Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/Media;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Media;->_currentState:Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;

    return-object v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/smartglass/controls/Media;Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;)Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/Media;
    .param p1, "x1"    # Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/Media;->_currentState:Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;

    return-object p1
.end method


# virtual methods
.method public dispose()V
    .locals 2

    .prologue
    .line 59
    invoke-super {p0}, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->dispose()V

    .line 60
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/Media;->_listener:Lcom/microsoft/xbox/smartglass/controls/Media$SessionListener;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->removeListener(Ljava/lang/Object;)V

    .line 61
    return-void
.end method

.method public getCurrentState()Lorg/json/JSONObject;
    .locals 1

    .prologue
    .line 48
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Media;->_currentState:Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;

    invoke-direct {v0}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Media;->_currentState:Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/Media;->_currentState:Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;

    return-object v0

    .line 51
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected abstract getState(ILjava/lang/String;)V
.end method

.method protected getState(ILjava/lang/String;Lcom/microsoft/xbox/smartglass/MessageTarget;)V
    .locals 5
    .param p1, "requestId"    # I
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "target"    # Lcom/microsoft/xbox/smartglass/MessageTarget;

    .prologue
    .line 80
    invoke-virtual {p0, p3}, Lcom/microsoft/xbox/smartglass/controls/Media;->validateSession(Lcom/microsoft/xbox/smartglass/MessageTarget;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 81
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/Media;->failRequestValidation(I)V

    .line 101
    :goto_0
    return-void

    .line 85
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v2

    const-string v3, "GetState"

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/smartglass/controls/Media;->getMetricName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->Origin:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/MetricsManager;->recordEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/SessionManager;->getMediaStates()Ljava/util/List;

    move-result-object v1

    .line 88
    .local v1, "mediaStates":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/smartglass/MediaState;>;"
    if-eqz v1, :cond_2

    .line 89
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/MediaState;

    .line 90
    .local v0, "mediaState":Lcom/microsoft/xbox/smartglass/MediaState;
    iget v3, v0, Lcom/microsoft/xbox/smartglass/MediaState;->titleId:I

    iget v4, p3, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    if-ne v3, v4, :cond_1

    .line 92
    :try_start_0
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;

    invoke-direct {v2, v0}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;-><init>(Lcom/microsoft/xbox/smartglass/MediaState;)V

    iput-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/Media;->_currentState:Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    .end local v0    # "mediaState":Lcom/microsoft/xbox/smartglass/MediaState;
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/Media;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/Media;->_currentState:Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;

    invoke-interface {v2, p1, v3}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->completeRequest(ILorg/json/JSONObject;)V

    goto :goto_0

    .line 93
    .restart local v0    # "mediaState":Lcom/microsoft/xbox/smartglass/MediaState;
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method public invoke(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "requestId"    # I
    .param p2, "methodName"    # Ljava/lang/String;
    .param p3, "json"    # Ljava/lang/String;

    .prologue
    .line 65
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/smartglass/controls/Media;->validateMethod(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 66
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/controls/Media;->failRequestUnknownMethod(ILjava/lang/String;)V

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    const-string v0, "GetState"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 71
    invoke-virtual {p0, p1, p3}, Lcom/microsoft/xbox/smartglass/controls/Media;->getState(ILjava/lang/String;)V

    goto :goto_0

    .line 72
    :cond_2
    const-string v0, "Seek"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 73
    invoke-virtual {p0, p1, p3}, Lcom/microsoft/xbox/smartglass/controls/Media;->seek(ILjava/lang/String;)V

    goto :goto_0

    .line 74
    :cond_3
    const-string v0, "Send"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {p0, p1, p3}, Lcom/microsoft/xbox/smartglass/controls/Media;->send(ILjava/lang/String;)V

    goto :goto_0
.end method

.method protected seek(IJLcom/microsoft/xbox/smartglass/MessageTarget;)V
    .locals 8
    .param p1, "requestId"    # I
    .param p2, "seekPositionTicks"    # J
    .param p4, "target"    # Lcom/microsoft/xbox/smartglass/MessageTarget;

    .prologue
    .line 104
    invoke-virtual {p0, p4}, Lcom/microsoft/xbox/smartglass/controls/Media;->validateSession(Lcom/microsoft/xbox/smartglass/MessageTarget;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 105
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/Media;->failRequestValidation(I)V

    .line 127
    :goto_0
    return-void

    .line 109
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v4

    const-string v5, "Seek"

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/smartglass/controls/Media;->getMetricName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->Origin:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lcom/microsoft/xbox/smartglass/MetricsManager;->recordEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/smartglass/SessionManager;->getMediaStates()Ljava/util/List;

    move-result-object v3

    .line 112
    .local v3, "mediaStates":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/smartglass/MediaState;>;"
    if-eqz v3, :cond_2

    .line 113
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/smartglass/MediaState;

    .line 114
    .local v2, "mediaState":Lcom/microsoft/xbox/smartglass/MediaState;
    iget v5, v2, Lcom/microsoft/xbox/smartglass/MediaState;->titleId:I

    iget v6, p4, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    if-ne v5, v6, :cond_1

    .line 115
    new-instance v1, Lcom/microsoft/xbox/smartglass/MediaCommand;

    iget v5, v2, Lcom/microsoft/xbox/smartglass/MediaState;->titleId:I

    invoke-direct {v1, v5, p2, p3}, Lcom/microsoft/xbox/smartglass/MediaCommand;-><init>(IJ)V

    .line 117
    .local v1, "mediaCommand":Lcom/microsoft/xbox/smartglass/MediaCommand;
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->sendMediaCommand(Lcom/microsoft/xbox/smartglass/MediaCommand;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 118
    :catch_0
    move-exception v0

    .line 119
    .local v0, "ex":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/Media;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to seek. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, p1, v5}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0

    .line 126
    .end local v0    # "ex":Ljava/lang/Exception;
    .end local v1    # "mediaCommand":Lcom/microsoft/xbox/smartglass/MediaCommand;
    .end local v2    # "mediaState":Lcom/microsoft/xbox/smartglass/MediaState;
    :cond_2
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/Media;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    invoke-interface {v4, p1}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->completeRequest(I)V

    goto :goto_0
.end method

.method protected abstract seek(ILjava/lang/String;)V
.end method

.method protected send(IILcom/microsoft/xbox/smartglass/MessageTarget;)V
    .locals 5
    .param p1, "requestId"    # I
    .param p2, "command"    # I
    .param p3, "target"    # Lcom/microsoft/xbox/smartglass/MessageTarget;

    .prologue
    .line 130
    invoke-virtual {p0, p3}, Lcom/microsoft/xbox/smartglass/controls/Media;->validateSession(Lcom/microsoft/xbox/smartglass/MessageTarget;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 131
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/smartglass/controls/Media;->failRequestValidation(I)V

    .line 146
    :goto_0
    return-void

    .line 135
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v2

    const-string v3, "Send"

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/smartglass/controls/Media;->getMetricName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->Origin:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/xbox/smartglass/MetricsManager;->recordEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    :try_start_0
    new-instance v1, Lcom/microsoft/xbox/smartglass/MediaCommand;

    iget v2, p3, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    invoke-static {p2}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->enumValueFromInt(I)Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/MediaCommand;-><init>(ILcom/microsoft/xbox/smartglass/MediaControlCommands;)V

    .line 139
    .local v1, "mediaCommand":Lcom/microsoft/xbox/smartglass/MediaCommand;
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->sendMediaCommand(Lcom/microsoft/xbox/smartglass/MediaCommand;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 145
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/Media;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    invoke-interface {v2, p1}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->completeRequest(I)V

    goto :goto_0

    .line 140
    .end local v1    # "mediaCommand":Lcom/microsoft/xbox/smartglass/MediaCommand;
    :catch_0
    move-exception v0

    .line 141
    .local v0, "ex":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/Media;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to send media command. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, p1, v3}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0
.end method

.method protected abstract send(ILjava/lang/String;)V
.end method
