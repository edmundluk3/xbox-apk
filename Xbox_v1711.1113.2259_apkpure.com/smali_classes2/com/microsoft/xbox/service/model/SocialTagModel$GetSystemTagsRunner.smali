.class Lcom/microsoft/xbox/service/model/SocialTagModel$GetSystemTagsRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "SocialTagModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/SocialTagModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetSystemTagsRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagList;",
        ">;"
    }
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final model:Lcom/microsoft/xbox/service/model/SocialTagModel;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/SocialTagModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/SocialTagModel;Lcom/microsoft/xbox/service/model/SocialTagModel;)V
    .locals 1
    .param p1    # Lcom/microsoft/xbox/service/model/SocialTagModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "model"    # Lcom/microsoft/xbox/service/model/SocialTagModel;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/SocialTagModel$GetSystemTagsRunner;->this$0:Lcom/microsoft/xbox/service/model/SocialTagModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 130
    const-class v0, Lcom/microsoft/xbox/service/model/SocialTagModel$GetSystemTagsRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/SocialTagModel$GetSystemTagsRunner;->TAG:Ljava/lang/String;

    .line 134
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 135
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/SocialTagModel$GetSystemTagsRunner;->model:Lcom/microsoft/xbox/service/model/SocialTagModel;

    .line 136
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagList;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SocialTagModel$GetSystemTagsRunner;->this$0:Lcom/microsoft/xbox/service/model/SocialTagModel;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/SocialTagModel;->editorialService:Lcom/microsoft/xbox/data/service/editorial/EditorialService;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/service/editorial/EditorialService;->getSystemTags()Lio/reactivex/Single;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagList;

    return-object v0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/SocialTagModel$GetSystemTagsRunner;->buildData()Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagList;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 154
    const-wide/16 v0, 0x25e4

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagList;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 149
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$SystemTagList;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/SocialTagModel$GetSystemTagsRunner;->model:Lcom/microsoft/xbox/service/model/SocialTagModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/model/SocialTagModel;->access$000(Lcom/microsoft/xbox/service/model/SocialTagModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 150
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 140
    return-void
.end method
