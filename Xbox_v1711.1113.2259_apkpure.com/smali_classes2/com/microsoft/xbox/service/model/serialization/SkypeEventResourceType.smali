.class public final enum Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;
.super Ljava/lang/Enum;
.source "SkypeEventResourceType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;

.field public static final enum Invalid:Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;

.field public static final enum NewMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;

.field public static final enum ThreadUpdate:Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;

.field private static final stringToTypeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 10
    new-instance v2, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;

    const-string v3, "NewMessage"

    const-string v4, "NewMessage"

    invoke-direct {v2, v3, v1, v4}, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;->NewMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;

    .line 11
    new-instance v2, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;

    const-string v3, "ThreadUpdate"

    const-string v4, "ThreadUpdate"

    invoke-direct {v2, v3, v5, v4}, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;->ThreadUpdate:Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;

    .line 12
    new-instance v2, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;

    const-string v3, "Invalid"

    const-string v4, "Invalid"

    invoke-direct {v2, v3, v6, v4}, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;->Invalid:Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;

    .line 9
    const/4 v2, 0x3

    new-array v2, v2, [Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;

    sget-object v3, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;->NewMessage:Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;

    aput-object v3, v2, v1

    sget-object v3, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;->ThreadUpdate:Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;

    aput-object v3, v2, v5

    sget-object v3, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;->Invalid:Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;

    aput-object v3, v2, v6

    sput-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;->$VALUES:[Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;

    .line 15
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;->stringToTypeMap:Ljava/util/Map;

    .line 26
    invoke-static {}, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;->values()[Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;

    move-result-object v2

    array-length v3, v2

    .local v0, "resourceType":Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 27
    sget-object v4, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;->stringToTypeMap:Ljava/util/Map;

    iget-object v5, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;->type:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 29
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 18
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;->type:Ljava/lang/String;

    .line 19
    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;
    .locals 3
    .param p0, "str"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 32
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 33
    sget-object v1, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;->stringToTypeMap:Ljava/util/Map;

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;

    .line 34
    .local v0, "type":Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;
    if-nez v0, :cond_0

    .line 35
    sget-object v0, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;->Invalid:Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;

    .line 41
    .end local v0    # "type":Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;->Invalid:Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 9
    const-class v0, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;->$VALUES:[Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;

    return-object v0
.end method


# virtual methods
.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/serialization/SkypeEventResourceType;->type:Ljava/lang/String;

    return-object v0
.end method
