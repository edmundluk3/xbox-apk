.class public interface abstract Lcom/microsoft/xbox/presentation/base/ReactScreen;
.super Ljava/lang/Object;
.source "ReactScreen.java"


# virtual methods
.method public abstract getLaunchOptions()Landroid/os/Bundle;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getLayoutId()Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getModuleName()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/Size;
        min = 0x1L
    .end annotation
.end method

.method public abstract onCreateFinished()V
.end method
