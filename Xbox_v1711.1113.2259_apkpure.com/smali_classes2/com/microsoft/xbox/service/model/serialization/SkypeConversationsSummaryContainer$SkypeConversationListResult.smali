.class public Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;
.super Ljava/lang/Object;
.source "SkypeConversationsSummaryContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SkypeConversationListResult"
.end annotation


# instance fields
.field public _metadata:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListMetadata;

.field public conversations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationSummary;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deserialize(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;
    .locals 1
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 199
    const-class v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;

    invoke-static {p0, v0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJsonWithDateAdapter(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationListResult;

    return-object v0
.end method
