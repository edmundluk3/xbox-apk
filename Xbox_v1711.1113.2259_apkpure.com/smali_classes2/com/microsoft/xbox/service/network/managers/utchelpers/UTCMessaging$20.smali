.class final Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$20;
.super Ljava/lang/Object;
.source "UTCMessaging.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCEventTracker$UTCEventDelegate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackSendMessageError(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$errorCode:J


# direct methods
.method constructor <init>(J)V
    .locals 1

    .prologue
    .line 479
    iput-wide p1, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$20;->val$errorCode:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()V
    .locals 6

    .prologue
    .line 483
    const-string v0, "Messaging - Failed to send message"

    .line 484
    .local v0, "errorMessage":Ljava/lang/String;
    iget-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$20;->val$errorCode:J

    const-wide/16 v4, 0x26e8

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 485
    const-string v0, "Messaging - Failed to send message due to permissions or settings"

    .line 487
    :cond_0
    const-string v1, "%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging$20;->val$errorCode:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->track(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    return-void
.end method
