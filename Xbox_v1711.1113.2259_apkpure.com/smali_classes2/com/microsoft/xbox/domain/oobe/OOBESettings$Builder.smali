.class public abstract Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;
.super Ljava/lang/Object;
.source "OOBESettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/oobe/OOBESettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract autoDST(Z)Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;
.end method

.method public abstract autoUpdateApps(Z)Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;
.end method

.method public abstract autoUpdateSystem(Z)Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;
.end method

.method public abstract build()Lcom/microsoft/xbox/domain/oobe/OOBESettings;
.end method

.method public abstract instantOn(Ljava/lang/Boolean;)Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;
.end method

.method public abstract timeZone(Ljava/lang/String;)Lcom/microsoft/xbox/domain/oobe/OOBESettings$Builder;
.end method
