.class final synthetic Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$41;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;

.field private final arg$2:Lcom/microsoft/xbox/data/service/oobe/OOBEService;

.field private final arg$3:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

.field private final arg$4:Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$41;->arg$1:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;

    iput-object p2, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$41;->arg$2:Lcom/microsoft/xbox/data/service/oobe/OOBEService;

    iput-object p3, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$41;->arg$3:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    iput-object p4, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$41;->arg$4:Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;)Lio/reactivex/functions/Function;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$41;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$41;-><init>(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$41;->arg$1:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;

    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$41;->arg$2:Lcom/microsoft/xbox/data/service/oobe/OOBEService;

    iget-object v2, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$41;->arg$3:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    iget-object v3, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$41;->arg$4:Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;

    check-cast p1, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;

    invoke-static {v0, v1, v2, v3, p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->lambda$null$8(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;Lcom/microsoft/xbox/data/service/oobe/OOBEService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$EnterCodeAction;)Lio/reactivex/ObservableSource;

    move-result-object v0

    return-object v0
.end method
