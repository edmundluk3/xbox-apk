.class public Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;
.super Ljava/lang/Object;
.source "ExperimentationSettings.java"


# static fields
.field private static final TESTEXP2C:Ljava/lang/String; = "testexp2-c"

.field private static final TESTEXP2D:Ljava/lang/String; = "testexp2-d"


# instance fields
.field public Configs:[Lcom/microsoft/xbox/service/network/managers/ExperimentationConfig;

.field public Features:[Ljava/lang/String;

.field public FlightingVersion:I

.field private data:Ljava/lang/String;

.field private debugTreatments:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private isDebug:Z

.field private treatments:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private xuid:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->debugTreatments:Ljava/util/ArrayList;

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->treatments:Ljava/util/ArrayList;

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->isDebug:Z

    return-void
.end method

.method public static deserialize(JLjava/io/InputStream;)Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;
    .locals 2
    .param p0, "xuid"    # J
    .param p2, "stream"    # Ljava/io/InputStream;

    .prologue
    .line 94
    const-class v1, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;

    invoke-static {p2, v1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;

    .line 95
    .local v0, "settings":Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->setData(Ljava/lang/String;)V

    .line 96
    invoke-virtual {v0, p0, p1}, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->setXuid(J)V

    .line 97
    return-object v0
.end method

.method public static deserialize(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;
    .locals 2
    .param p0, "data"    # Ljava/lang/String;

    .prologue
    .line 106
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;-><init>()V

    .line 107
    .local v0, "settings":Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 108
    const-class v1, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;

    invoke-static {p0, v1}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "settings":Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;
    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;

    .line 109
    .restart local v0    # "settings":Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->setData(Ljava/lang/String;)V

    .line 111
    :cond_0
    return-object v0
.end method


# virtual methods
.method public getData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->data:Ljava/lang/String;

    return-object v0
.end method

.method public getDebugTreatments()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->debugTreatments:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->debugTreatments:Ljava/util/ArrayList;

    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->debugTreatments:Ljava/util/ArrayList;

    const-string v1, "testexp2-c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->debugTreatments:Ljava/util/ArrayList;

    const-string v1, "testexp2-d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->debugTreatments:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getIsDebug()Z
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->isDebug:Z

    return v0
.end method

.method public getTreatments()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->treatments:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 73
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->treatments:Ljava/util/ArrayList;

    .line 76
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->Features:[Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 77
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->Features:[Ljava/lang/String;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v0, v2, v1

    .line 78
    .local v0, "treatment":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->treatments:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 79
    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->treatments:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 84
    .end local v0    # "treatment":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->treatments:Ljava/util/ArrayList;

    return-object v1
.end method

.method public getXuid()J
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->xuid:J

    return-wide v0
.end method

.method public setData(Ljava/lang/String;)V
    .locals 0
    .param p1, "jsonData"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->data:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public setIsDebug(Z)V
    .locals 0
    .param p1, "debug"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->isDebug:Z

    .line 36
    return-void
.end method

.method public setXuid(J)V
    .locals 1
    .param p1, "xuid"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/microsoft/xbox/service/network/managers/ExperimentationSettings;->xuid:J

    .line 44
    return-void
.end method
