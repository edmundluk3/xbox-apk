.class public final enum Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;
.super Ljava/lang/Enum;
.source "BroadcastConnectionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GameStreamState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

.field public static final enum Initializing:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

.field public static final enum Paused:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

.field public static final enum Started:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

.field public static final enum Stopped:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

.field public static final enum Unknown:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 28
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    const-string v1, "Unknown"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;->Unknown:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    new-instance v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    const-string v1, "Initializing"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;->Initializing:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    new-instance v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    const-string v1, "Started"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;->Started:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    new-instance v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    const-string v1, "Stopped"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;->Stopped:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    new-instance v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    const-string v1, "Paused"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;->Paused:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    .line 27
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;->Unknown:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;->Initializing:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;->Started:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;->Stopped:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;->Paused:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 27
    const-class v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    return-object v0
.end method
