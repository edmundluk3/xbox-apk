.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$GameDetail;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GameDetail"
.end annotation


# static fields
.field public static final FriendsWhoPlay:Ljava/lang/String; = "Friends Who Play"

.field public static final GameCaptureFilter:Ljava/lang/String; = "Game Captures Filter"

.field public static final GameDVRFilter:Ljava/lang/String; = "Game DVR Filter"

.field public static final GameDVRLike:Ljava/lang/String; = "Game DVR Like"

.field public static final GameDVRUnlike:Ljava/lang/String; = "Game DVR Un-Like"

.field public static final GameDVRViewCount:Ljava/lang/String; = "Game DVR View Count"

.field public static final PlayDVR:Ljava/lang/String; = "Play Game DVR"

.field public static final PlaySnap:Ljava/lang/String; = "Play in Snap"

.field public static final PlayXbox:Ljava/lang/String; = "Play on Xbox"

.field public static final PopularItem:Ljava/lang/String; = "Game Item Popular"

.field public static final ProfileFriendsWhoPlay:Ljava/lang/String; = "Game Profile Friends Who Play"

.field public static final ProviderPicker:Ljava/lang/String; = "Provider Picker"

.field public static final ScreenshotCount:Ljava/lang/String; = "Screenshot View Count"

.field public static final ScreenshotDisplay:Ljava/lang/String; = "Screenshot Display"

.field public static final ScreenshotLike:Ljava/lang/String; = "Screenshot Like"

.field public static final ScreenshotSave:Ljava/lang/String; = "Screenshot Save"

.field public static final ScreenshotShare:Ljava/lang/String; = "Screenshot Share"

.field public static final ScreenshotUnLike:Ljava/lang/String; = "Screenshot Un-Like"

.field public static final SuggestedFriendsWhoPlay:Ljava/lang/String; = "Game Profile Suggested Friends Who Play"

.field public static final WatchBroadcast:Ljava/lang/String; = "Watch Broadcast"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
