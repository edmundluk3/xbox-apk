.class Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$10;
.super Ljava/lang/Object;
.source "InternetExplorerControl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;->sendIeTitleMessage(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

.field final synthetic val$json:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    .prologue
    .line 244
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$10;->this$0:Lcom/microsoft/xbox/xle/ui/InternetExplorerControl;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$10;->val$json:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 249
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/SessionModel;->getIeChannelReady()Lcom/microsoft/xbox/toolkit/Ready;

    move-result-object v1

    const/16 v2, 0x2710

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/Ready;->waitForReady(I)V

    .line 250
    const-string v1, "InternetExplorerControl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sending IE title message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$10;->val$json:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/smartglass/MessageTarget;

    const v3, 0x3d8b930f

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/smartglass/MessageTarget;-><init>(I)V

    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$10;->val$json:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/model/SessionModel;->sendTitleMessage(Lcom/microsoft/xbox/smartglass/MessageTarget;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 255
    :goto_0
    return-void

    .line 252
    :catch_0
    move-exception v0

    .line 253
    .local v0, "ex":Ljava/lang/Exception;
    const-string v1, "InternetExplorerControl"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "failed to send an IE title message: %s, Error: %s "

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/microsoft/xbox/xle/ui/InternetExplorerControl$10;->val$json:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
