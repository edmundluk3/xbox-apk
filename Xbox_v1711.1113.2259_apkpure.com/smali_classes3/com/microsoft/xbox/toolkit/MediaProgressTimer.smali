.class public Lcom/microsoft/xbox/toolkit/MediaProgressTimer;
.super Ljava/lang/Object;
.source "MediaProgressTimer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/MediaProgressTimer$ProgressTask;,
        Lcom/microsoft/xbox/toolkit/MediaProgressTimer$OnMediaProgressUpdatedListener;
    }
.end annotation


# static fields
.field private static final TIMER_INTERVAL_MS:I = 0x1f4

.field public static final UPDATE_PROGRESS:Ljava/lang/String; = "com.microsoft.xbox.media.PROGRESS"


# instance fields
.field private context:Landroid/content/Context;

.field private currentDuration:J

.field private currentPosition:J

.field private currentRate:J

.field private location:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

.field private onMediaProgressUpdatedListener:Lcom/microsoft/xbox/toolkit/MediaProgressTimer$OnMediaProgressUpdatedListener;

.field private progressTimer:Ljava/util/Timer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "location"    # Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->context:Landroid/content/Context;

    .line 49
    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->location:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    .line 50
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/toolkit/MediaProgressTimer;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/MediaProgressTimer;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->onTimerTick()V

    return-void
.end method

.method private onTimerTick()V
    .locals 6

    .prologue
    .line 107
    sget-object v0, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 108
    iget-wide v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->currentPosition:J

    iget-wide v2, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->currentRate:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->currentPosition:J

    .line 110
    iget-wide v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->currentPosition:J

    iget-wide v2, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->currentDuration:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 111
    iget-wide v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->currentDuration:J

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->currentPosition:J

    .line 115
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->stop()V

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->onMediaProgressUpdatedListener:Lcom/microsoft/xbox/toolkit/MediaProgressTimer$OnMediaProgressUpdatedListener;

    if-eqz v0, :cond_1

    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->onMediaProgressUpdatedListener:Lcom/microsoft/xbox/toolkit/MediaProgressTimer$OnMediaProgressUpdatedListener;

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->getPositionInSeconds()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->getDurationInSeconds()J

    move-result-wide v4

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/MediaProgressTimer$OnMediaProgressUpdatedListener;->onUpdate(JJ)V

    .line 126
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->sendBroadcast()V

    .line 127
    return-void

    .line 107
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private sendBroadcast()V
    .locals 7

    .prologue
    .line 130
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 131
    .local v6, "intent":Landroid/content/Intent;
    const-string v1, "com.microsoft.xbox.media.PROGRESS"

    invoke-virtual {v6, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 132
    new-instance v0, Lcom/microsoft/xbox/toolkit/MediaProgressData;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->location:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->getPositionInSeconds()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->getDurationInSeconds()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/toolkit/MediaProgressData;-><init>(Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;JJ)V

    .line 133
    .local v0, "data":Lcom/microsoft/xbox/toolkit/MediaProgressData;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/MediaProgressData;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 134
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->context:Landroid/content/Context;

    invoke-virtual {v1, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 135
    return-void
.end method


# virtual methods
.method public getDurationInSeconds()J
    .locals 2

    .prologue
    .line 103
    iget-wide v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->currentDuration:J

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XBLSharedUtil;->hundredNanosecondsToSeconds(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getPositionInSeconds()J
    .locals 2

    .prologue
    .line 99
    iget-wide v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->currentPosition:J

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XBLSharedUtil;->hundredNanosecondsToSeconds(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public setOnPositionUpdatedRunnable(Lcom/microsoft/xbox/toolkit/MediaProgressTimer$OnMediaProgressUpdatedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/microsoft/xbox/toolkit/MediaProgressTimer$OnMediaProgressUpdatedListener;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->onMediaProgressUpdatedListener:Lcom/microsoft/xbox/toolkit/MediaProgressTimer$OnMediaProgressUpdatedListener;

    .line 96
    return-void
.end method

.method public start()V
    .locals 8

    .prologue
    const-wide/16 v2, 0x1f4

    const-wide/16 v6, 0x0

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->progressTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->progressTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 68
    :cond_0
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->progressTimer:Ljava/util/Timer;

    .line 72
    iget-wide v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->currentRate:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->progressTimer:Ljava/util/Timer;

    new-instance v1, Lcom/microsoft/xbox/toolkit/MediaProgressTimer$ProgressTask;

    const/4 v4, 0x0

    invoke-direct {v1, p0, v4}, Lcom/microsoft/xbox/toolkit/MediaProgressTimer$ProgressTask;-><init>(Lcom/microsoft/xbox/toolkit/MediaProgressTimer;Lcom/microsoft/xbox/toolkit/MediaProgressTimer$1;)V

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 77
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->onMediaProgressUpdatedListener:Lcom/microsoft/xbox/toolkit/MediaProgressTimer$OnMediaProgressUpdatedListener;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->getDurationInSeconds()J

    move-result-wide v0

    cmp-long v0, v0, v6

    if-lez v0, :cond_2

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->onMediaProgressUpdatedListener:Lcom/microsoft/xbox/toolkit/MediaProgressTimer$OnMediaProgressUpdatedListener;

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->getPositionInSeconds()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->getDurationInSeconds()J

    move-result-wide v4

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/MediaProgressTimer$OnMediaProgressUpdatedListener;->onUpdate(JJ)V

    .line 81
    :cond_2
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->sendBroadcast()V

    .line 82
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->progressTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->progressTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->progressTimer:Ljava/util/Timer;

    .line 92
    :cond_0
    return-void
.end method

.method public update(JJ)V
    .locals 3
    .param p1, "position"    # J
    .param p3, "duration"    # J

    .prologue
    .line 59
    invoke-static {p1, p2, p3, p4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->currentPosition:J

    .line 60
    iput-wide p3, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->currentDuration:J

    .line 61
    return-void
.end method

.method public update(JJF)V
    .locals 3
    .param p1, "position"    # J
    .param p3, "duration"    # J
    .param p5, "rate"    # F

    .prologue
    .line 53
    invoke-static {p1, p2, p3, p4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->currentPosition:J

    .line 54
    iput-wide p3, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->currentDuration:J

    .line 55
    const/high16 v0, 0x43fa0000    # 500.0f

    mul-float/2addr v0, p5

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XBLSharedUtil;->secondsToHundredNanoseconds(F)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/MediaProgressTimer;->currentRate:J

    .line 56
    return-void
.end method
