.class public Lcom/microsoft/xbox/service/model/PagesModel;
.super Ljava/lang/Object;
.source "PagesModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/PagesModel$PageModel;
    }
.end annotation


# static fields
.field private static final MAX_MODEL_COUNT:I = 0xff

.field private static final TAG:Ljava/lang/String;

.field public static final XboxPageId:Ljava/lang/String; = "XboxNews"

.field private static xboxNewsModel:Lcom/microsoft/xbox/service/model/PagesModel;


# instance fields
.field editorialService:Lcom/microsoft/xbox/data/service/editorial/EditorialService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private followingPages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/usertitles/FollowingPages$FollowingPage;",
            ">;"
        }
    .end annotation
.end field

.field private pageModels:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/PagesModel$PageModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/microsoft/xbox/service/model/PagesModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/PagesModel;->TAG:Ljava/lang/String;

    .line 39
    new-instance v0, Lcom/microsoft/xbox/service/model/PagesModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/PagesModel;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/PagesModel;->xboxNewsModel:Lcom/microsoft/xbox/service/model/PagesModel;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/service/model/PagesModel;)V

    .line 46
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0xff

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/PagesModel;->pageModels:Landroid/util/LruCache;

    .line 47
    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/service/model/PagesModel;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/microsoft/xbox/service/model/PagesModel;->xboxNewsModel:Lcom/microsoft/xbox/service/model/PagesModel;

    return-object v0
.end method


# virtual methods
.method public getPageModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/PagesModel$PageModel;
    .locals 2
    .param p1, "pageId"    # Ljava/lang/String;

    .prologue
    .line 54
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/PagesModel;->pageModels:Landroid/util/LruCache;

    invoke-virtual {v1, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;

    .line 55
    .local v0, "model":Lcom/microsoft/xbox/service/model/PagesModel$PageModel;
    if-nez v0, :cond_0

    .line 56
    new-instance v0, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;

    .end local v0    # "model":Lcom/microsoft/xbox/service/model/PagesModel$PageModel;
    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;-><init>(Lcom/microsoft/xbox/service/model/PagesModel;Ljava/lang/String;)V

    .line 57
    .restart local v0    # "model":Lcom/microsoft/xbox/service/model/PagesModel$PageModel;
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/PagesModel;->pageModels:Landroid/util/LruCache;

    invoke-virtual {v1, p1, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    :cond_0
    return-object v0
.end method
