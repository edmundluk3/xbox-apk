.class public Lcom/microsoft/xbox/xle/remote/RemoteControl;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "RemoteControl.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/XLEObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/XLEManagedDialog;",
        "Lcom/microsoft/xbox/toolkit/XLEObserver",
        "<",
        "Lcom/microsoft/xbox/service/model/UpdateData;",
        ">;"
    }
.end annotation


# static fields
.field private static final InAnimation:Ljava/lang/String; = "RemoteControlIn"

.field private static final InAnimationTablet:Ljava/lang/String; = "RemoteControlInTablet"

.field private static final OutAnimation:Ljava/lang/String; = "RemoteControlOut"

.field private static final OutAnimationTablet:Ljava/lang/String; = "RemoteControlOutTablet"

.field public static final STATE_UNIVERSAL_REMOTE:I = 0x1

.field public static final STATE_XBOX_REMOTE:I = 0x0

.field public static final TAG:Ljava/lang/String; = "RemoteControl"

.field private static final UrcOutAnimation:Ljava/lang/String; = "UrcOut"

.field public static final XboxRemotePageName:Ljava/lang/String; = "Remote"


# instance fields
.field private autoConnectionState:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

.field private final blockingDialog:Landroid/widget/RelativeLayout;

.field private final closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private is_dialog_dismissed:Z

.field private final remoteSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final remote_root_view:Landroid/view/View;

.field private startingState:I

.field private final universalRemoteControl:Lcom/microsoft/xbox/xle/remote/UniversalRemoteControl;

.field private final universalRemotebutton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final urc_extra_more:Landroid/view/View;

.field private final urc_extra_power:Landroid/view/View;

.field private final urc_extra_root_view:Landroid/view/View;

.field private urc_extra_visible:Z

.field private final xboxRemoteControl:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

.field private final xboxRemotebutton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 81
    const v5, 0x7f0802b1

    invoke-direct {p0, p1, v5}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 74
    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_visible:Z

    .line 75
    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->is_dialog_dismissed:Z

    .line 83
    const v5, 0x7f0301e8

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->setContentView(I)V

    .line 84
    invoke-virtual {p0, v7}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->setCanceledOnTouchOutside(Z)V

    .line 85
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->makeFullScreen()V

    .line 87
    const v5, 0x7f0e09b4

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->dialogBody:Landroid/view/View;

    .line 89
    const v5, 0x7f0e09b5

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->remote_root_view:Landroid/view/View;

    .line 90
    const v5, 0x7f0e0b99

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_root_view:Landroid/view/View;

    .line 91
    iget-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_root_view:Landroid/view/View;

    invoke-virtual {v5, v8}, Landroid/view/View;->setClickable(Z)V

    .line 93
    const v5, 0x7f0e0b9b

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_more:Landroid/view/View;

    .line 94
    const v5, 0x7f0e0b9c

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_power:Landroid/view/View;

    .line 95
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->hideURCExtra()V

    .line 97
    const v5, 0x7f0e09bb

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->xboxRemotebutton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 98
    const v5, 0x7f0e09bc

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->universalRemotebutton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 99
    const v5, 0x7f0e022a

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 100
    const v5, 0x7f0e09b6

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->remoteSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 101
    const v5, 0x7f0e09b7

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    iput-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->xboxRemoteControl:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    .line 102
    const v5, 0x7f0e09b8

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/xle/remote/UniversalRemoteControl;

    iput-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->universalRemoteControl:Lcom/microsoft/xbox/xle/remote/UniversalRemoteControl;

    .line 103
    const v5, 0x7f0e09b9

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    iput-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->blockingDialog:Landroid/widget/RelativeLayout;

    .line 105
    iget-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->universalRemotebutton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 107
    iget-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->xboxRemotebutton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v6, Lcom/microsoft/xbox/xle/remote/RemoteControl$1;

    invoke-direct {v6, p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl$1;-><init>(Lcom/microsoft/xbox/xle/remote/RemoteControl;)V

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->universalRemotebutton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v6, Lcom/microsoft/xbox/xle/remote/RemoteControl$2;

    invoke-direct {v6, p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl$2;-><init>(Lcom/microsoft/xbox/xle/remote/RemoteControl;)V

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    iget-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v6, Lcom/microsoft/xbox/xle/remote/RemoteControl$3;

    invoke-direct {v6, p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl$3;-><init>(Lcom/microsoft/xbox/xle/remote/RemoteControl;)V

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    iget-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->universalRemoteControl:Lcom/microsoft/xbox/xle/remote/UniversalRemoteControl;

    if-eqz v5, :cond_1

    .line 150
    const v5, 0x7f0e0bb3

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 151
    .local v2, "btnPower":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 152
    new-instance v5, Lcom/microsoft/xbox/xle/remote/RemoteControl$4;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl$4;-><init>(Lcom/microsoft/xbox/xle/remote/RemoteControl;)V

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    :cond_0
    const v5, 0x7f0e0bb7

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 161
    .local v1, "btnMore":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 162
    new-instance v5, Lcom/microsoft/xbox/xle/remote/RemoteControl$5;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl$5;-><init>(Lcom/microsoft/xbox/xle/remote/RemoteControl;)V

    invoke-virtual {v1, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 172
    .end local v1    # "btnMore":Landroid/view/View;
    .end local v2    # "btnPower":Landroid/view/View;
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getInstance()Lcom/microsoft/xbox/toolkit/anim/MAAS;

    move-result-object v5

    invoke-direct {p0, v8}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->getAnimationName(Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getAnimation(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;

    .line 173
    invoke-static {}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getInstance()Lcom/microsoft/xbox/toolkit/anim/MAAS;

    move-result-object v5

    invoke-direct {p0, v7}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->getAnimationName(Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getAnimation(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;

    .line 174
    sget-object v5, Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;->Connected:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    iput-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->autoConnectionState:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    .line 176
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->openTextInputIfNeeded()V

    .line 178
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 179
    .local v4, "r":Landroid/content/res/Resources;
    const v5, 0x7f0c0012

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 180
    .local v0, "activeColorId":I
    const v5, 0x7f0c00ce

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 182
    .local v3, "inactiveColorId":I
    iget-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->remoteSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->getState()I

    move-result v5

    if-nez v5, :cond_2

    .line 183
    iget-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->xboxRemotebutton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v5, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setTextColor(I)V

    .line 184
    iget-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->universalRemotebutton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v5, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setTextColor(I)V

    .line 185
    iget-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->xboxRemotebutton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v5, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setSelected(Z)V

    .line 186
    iget-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->universalRemotebutton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v5, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setSelected(Z)V

    .line 194
    :goto_0
    return-void

    .line 188
    :cond_2
    iget-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->xboxRemotebutton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v5, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setTextColor(I)V

    .line 189
    iget-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->universalRemotebutton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v5, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setTextColor(I)V

    .line 190
    iget-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->xboxRemotebutton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v5, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setSelected(Z)V

    .line 191
    iget-object v5, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->universalRemotebutton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v5, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setSelected(Z)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/remote/RemoteControl;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/RemoteControl;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->showXboxRemote()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/remote/RemoteControl;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/RemoteControl;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->showUniversalRemote()V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/remote/RemoteControl;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/RemoteControl;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->onClosePressed()V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/remote/RemoteControl;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/RemoteControl;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/remote/RemoteControl;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/RemoteControl;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->gotoURCPowerView()V

    return-void
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/remote/RemoteControl;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/RemoteControl;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->gotoURCMoreView()V

    return-void
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/remote/RemoteControl;)Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/RemoteControl;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->remoteSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/remote/RemoteControl;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/remote/RemoteControl;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->hideURCExtra()V

    return-void
.end method

.method private getAnimationName(Z)Ljava/lang/String;
    .locals 1
    .param p1, "in"    # Z

    .prologue
    .line 431
    if-eqz p1, :cond_0

    .line 432
    const-string v0, "RemoteControlIn"

    .line 436
    :goto_0
    return-object v0

    .line 433
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->remoteSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->getState()I

    move-result v0

    if-nez v0, :cond_1

    .line 434
    const-string v0, "RemoteControlOut"

    goto :goto_0

    .line 436
    :cond_1
    const-string v0, "UrcOut"

    goto :goto_0
.end method

.method private gotoURCMoreView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 350
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->remote_root_view:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 351
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_more:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 352
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_root_view:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 353
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_visible:Z

    .line 354
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_root_view:Landroid/view/View;

    const v1, 0x7f0e0b9a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 355
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_power:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 356
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_power:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 357
    return-void
.end method

.method private gotoURCPowerView()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 322
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->remote_root_view:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 324
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_power:Landroid/view/View;

    check-cast v0, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;

    .line 325
    .local v0, "powerLayout":Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;
    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;->setVisibility(I)V

    .line 326
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/ui/URCPowerLayout;->createPowerLineViews()V

    .line 328
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_root_view:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 329
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_visible:Z

    .line 330
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_root_view:Landroid/view/View;

    const v2, 0x7f0e0b9a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/xle/remote/RemoteControl$7;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl$7;-><init>(Lcom/microsoft/xbox/xle/remote/RemoteControl;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 338
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_power:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setClickable(Z)V

    .line 339
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_power:Landroid/view/View;

    new-instance v2, Lcom/microsoft/xbox/xle/remote/RemoteControl$8;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl$8;-><init>(Lcom/microsoft/xbox/xle/remote/RemoteControl;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 346
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    const-string v2, "btn.power"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackURCPageAction(Ljava/lang/String;)V

    .line 347
    return-void
.end method

.method private hideURCExtra()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 312
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_root_view:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 313
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_more:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 314
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_power:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 315
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_visible:Z

    .line 316
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_root_view:Landroid/view/View;

    const v1, 0x7f0e0b9a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 317
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_power:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 318
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_power:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 319
    return-void
.end method

.method private onClosePressed()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 203
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->is_dialog_dismissed:Z

    .line 205
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->remote_root_view:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 206
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->urc_extra_visible:Z

    if-eqz v0, :cond_0

    .line 207
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->hideURCExtra()V

    .line 212
    :goto_0
    return-void

    .line 210
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissRemoteControl()V

    goto :goto_0
.end method

.method private openTextInputIfNeeded()V
    .locals 1

    .prologue
    .line 420
    new-instance v0, Lcom/microsoft/xbox/xle/remote/RemoteControl$9;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl$9;-><init>(Lcom/microsoft/xbox/xle/remote/RemoteControl;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 428
    return-void
.end method

.method private showAutoConnectionScreenIfNeeded()V
    .locals 2

    .prologue
    .line 240
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->autoConnectionState:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    sget-object v1, Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;->Connecting:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    if-ne v0, v1, :cond_1

    .line 241
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->blockingDialog:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 247
    :cond_0
    :goto_0
    return-void

    .line 242
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->autoConnectionState:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    sget-object v1, Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;->Connected:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    if-ne v0, v1, :cond_2

    .line 243
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->blockingDialog:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 244
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->autoConnectionState:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    sget-object v1, Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;->ConnectionFailed:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    if-ne v0, v1, :cond_0

    .line 245
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->onBackPressed()V

    goto :goto_0
.end method

.method private showUniversalRemote()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 261
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->startTrackingURCSession()V

    .line 263
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 264
    .local v1, "r":Landroid/content/res/Resources;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->xboxRemotebutton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const v5, 0x7f0c00ce

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setTextColor(I)V

    .line 265
    iget-object v4, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->universalRemotebutton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const v5, 0x7f0c0012

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setTextColor(I)V

    .line 266
    iget-object v4, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->xboxRemotebutton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setSelected(Z)V

    .line 267
    iget-object v4, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->universalRemotebutton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v4, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setSelected(Z)V

    .line 277
    const v4, 0x7f0e0b98

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;

    .line 278
    .local v3, "urcViewControl":Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getIsInHdmiMode()Z

    move-result v4

    if-nez v4, :cond_0

    .line 281
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v4

    const-string v5, "URC Disabled"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageView(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    :cond_0
    if-eqz v3, :cond_1

    .line 286
    const v4, 0x7f0e0a3c

    :try_start_0
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ScrollView;

    .line 287
    .local v2, "scrollView":Landroid/widget/ScrollView;
    const/high16 v4, 0x3f000000    # 0.5f

    invoke-virtual {v2, v4}, Landroid/widget/ScrollView;->setAlpha(F)V

    .line 288
    new-instance v4, Lcom/microsoft/xbox/xle/remote/RemoteControl$6;

    invoke-direct {v4, p0, v3, v2}, Lcom/microsoft/xbox/xle/remote/RemoteControl$6;-><init>(Lcom/microsoft/xbox/xle/remote/RemoteControl;Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;Landroid/widget/ScrollView;)V

    const-wide/16 v6, 0x1

    invoke-virtual {v2, v4, v6, v7}, Landroid/widget/ScrollView;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 309
    .end local v2    # "scrollView":Landroid/widget/ScrollView;
    :cond_1
    :goto_0
    return-void

    .line 301
    :catch_0
    move-exception v0

    .line 304
    .local v0, "cce":Ljava/lang/ClassCastException;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->remoteSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v4, v8}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    goto :goto_0
.end method

.method private showXboxRemote()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 250
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->stopTrackingSession()V

    .line 252
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->remoteSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 253
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 254
    .local v0, "r":Landroid/content/res/Resources;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->xboxRemotebutton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const v2, 0x7f0c0012

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setTextColor(I)V

    .line 255
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->universalRemotebutton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const v2, 0x7f0c00ce

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setTextColor(I)V

    .line 256
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->xboxRemotebutton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setSelected(Z)V

    .line 257
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->universalRemotebutton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setSelected(Z)V

    .line 258
    return-void
.end method


# virtual methods
.method public getAnimateIn()Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    .locals 4

    .prologue
    .line 442
    const/4 v1, 0x0

    .line 443
    .local v1, "screenBodyAnimation":Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->getDialogBody()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 445
    invoke-static {}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getInstance()Lcom/microsoft/xbox/toolkit/anim/MAAS;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->getAnimationName(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getAnimation(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;

    move-result-object v0

    .line 446
    .local v0, "anim":Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;
    if-eqz v0, :cond_0

    .line 447
    check-cast v0, Lcom/microsoft/xbox/xle/anim/XLEAdapterAnimation;

    .end local v0    # "anim":Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->dialogBody:Landroid/view/View;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/anim/XLEAdapterAnimation;->compile(Landroid/view/View;)Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;

    move-result-object v1

    .line 450
    :cond_0
    return-object v1
.end method

.method public getAnimateOut()Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    .locals 4

    .prologue
    .line 455
    const/4 v1, 0x0

    .line 456
    .local v1, "screenBodyAnimation":Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->getDialogBody()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 458
    invoke-static {}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getInstance()Lcom/microsoft/xbox/toolkit/anim/MAAS;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->getAnimationName(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getAnimation(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;

    move-result-object v0

    .line 459
    .local v0, "anim":Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;
    if-eqz v0, :cond_0

    .line 460
    check-cast v0, Lcom/microsoft/xbox/xle/anim/XLEAdapterAnimation;

    .end local v0    # "anim":Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->dialogBody:Landroid/view/View;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/xle/anim/XLEAdapterAnimation;->compile(Landroid/view/View;)Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;

    move-result-object v1

    .line 463
    :cond_0
    return-object v1
.end method

.method public getCurrentState()I
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->remoteSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->getState()I

    move-result v0

    return v0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 198
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->onClosePressed()V

    .line 199
    return-void
.end method

.method public onStart()V
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 376
    iget-object v3, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->xboxRemoteControl:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->onStart()V

    .line 377
    iget-object v3, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->universalRemoteControl:Lcom/microsoft/xbox/xle/remote/UniversalRemoteControl;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/remote/UniversalRemoteControl;->onStart()V

    .line 378
    invoke-static {}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->getInstance()Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 380
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getPremiumUrcEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 381
    iget-object v3, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->universalRemotebutton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v3, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 382
    const-string v1, "Remote"

    .line 383
    .local v1, "toPage":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v0

    .line 384
    .local v0, "pageUri":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->getInstance()Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->getTitleIdWithFocus()I

    move-result v6

    .line 385
    .local v6, "titleId":I
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s"

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v3, v4, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 386
    .local v5, "relativeId":Ljava/lang/String;
    iget v3, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->startingState:I

    if-ne v3, v10, :cond_0

    .line 387
    const-string v1, "URC"

    .line 388
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->showUniversalRemote()V

    .line 390
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->isPaused()Z

    move-result v3

    if-nez v3, :cond_1

    .line 391
    const-string v4, "TitleId"

    move-object v3, v2

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageView;->trackLegacy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    .end local v0    # "pageUri":Ljava/lang/String;
    .end local v1    # "toPage":Ljava/lang/String;
    .end local v5    # "relativeId":Ljava/lang/String;
    .end local v6    # "titleId":I
    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 398
    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->is_dialog_dismissed:Z

    if-eqz v3, :cond_1

    .line 400
    iget-object v3, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->remoteSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->getState()I

    move-result v3

    if-ne v3, v1, :cond_0

    :goto_0
    invoke-static {v1}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->stopTrackingURCSession(Z)V

    .line 401
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->is_dialog_dismissed:Z

    .line 409
    :goto_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->xboxRemoteControl:Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/remote/XboxRemoteControl;->onStop()V

    .line 410
    iget-object v1, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->universalRemoteControl:Lcom/microsoft/xbox/xle/remote/UniversalRemoteControl;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/remote/UniversalRemoteControl;->onStop()V

    .line 411
    invoke-static {}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->getInstance()Lcom/microsoft/xbox/xle/model/AutoConnectModel;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 413
    const v1, 0x7f0e0b98

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;

    .line 415
    .local v0, "urcViewControl":Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;->cancelFirstRunAnimation()V

    .line 417
    return-void

    .end local v0    # "urcViewControl":Lcom/microsoft/xbox/xle/urc/ui/URCViewControl;
    :cond_0
    move v1, v2

    .line 400
    goto :goto_0

    .line 406
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->stopTrackingSession()V

    goto :goto_1
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasFocus"    # Z

    .prologue
    .line 362
    return-void
.end method

.method public setStartingState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 366
    iput p1, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->startingState:I

    .line 367
    return-void
.end method

.method public update(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 216
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getIsFinal()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 217
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v1

    .line 218
    .local v1, "type":Lcom/microsoft/xbox/service/model/UpdateType;
    iget-object v0, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->autoConnectionState:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    .line 219
    .local v0, "oldState":Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;
    sget-object v2, Lcom/microsoft/xbox/xle/remote/RemoteControl$10;->$SwitchMap$com$microsoft$xbox$service$model$UpdateType:[I

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 229
    const-string v2, "XboxRemoteViewModel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "unhandled update type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/UpdateType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->autoConnectionState:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    if-eq v0, v2, :cond_0

    .line 234
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->showAutoConnectionScreenIfNeeded()V

    .line 237
    .end local v0    # "oldState":Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;
    .end local v1    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :cond_0
    return-void

    .line 221
    .restart local v0    # "oldState":Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;
    .restart local v1    # "type":Lcom/microsoft/xbox/service/model/UpdateType;
    :pswitch_0
    sget-object v2, Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;->Connecting:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->autoConnectionState:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    goto :goto_0

    .line 225
    :pswitch_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/UpdateData;->getExtra()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/xle/model/AutoConnectModel;->getSuccessState(Landroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;->Connected:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    :goto_1
    iput-object v2, p0, Lcom/microsoft/xbox/xle/remote/RemoteControl;->autoConnectionState:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;->ConnectionFailed:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    goto :goto_1

    .line 219
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
