.class final Lcom/microsoft/playready/RepresentationMediaInfo;
.super Ljava/lang/Object;
.source "RepresentationMediaInfo.java"

# interfaces
.implements Lcom/microsoft/playready/MediaInfo;


# instance fields
.field private mNativeClass:Lcom/microsoft/playready/Native_Class10;

.field private mRepIndex:I

.field private mStreamIndex:I


# direct methods
.method constructor <init>(Lcom/microsoft/playready/Native_Class10;II)V
    .locals 0
    .param p1, "nativeClass"    # Lcom/microsoft/playready/Native_Class10;
    .param p2, "streamIndex"    # I
    .param p3, "repIndex"    # I

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/microsoft/playready/RepresentationMediaInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    .line 47
    iput p2, p0, Lcom/microsoft/playready/RepresentationMediaInfo;->mStreamIndex:I

    .line 48
    iput p3, p0, Lcom/microsoft/playready/RepresentationMediaInfo;->mRepIndex:I

    .line 49
    return-void
.end method


# virtual methods
.method public getAudioInfo()Lcom/microsoft/playready/AudioInfo;
    .locals 4

    .prologue
    .line 58
    new-instance v0, Lcom/microsoft/playready/RepresentationAudioInfo;

    iget-object v1, p0, Lcom/microsoft/playready/RepresentationMediaInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v2, p0, Lcom/microsoft/playready/RepresentationMediaInfo;->mStreamIndex:I

    iget v3, p0, Lcom/microsoft/playready/RepresentationMediaInfo;->mRepIndex:I

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/playready/RepresentationAudioInfo;-><init>(Lcom/microsoft/playready/Native_Class10;II)V

    return-object v0
.end method

.method public getFourCC()Ljava/lang/String;
    .locals 5

    .prologue
    .line 28
    iget-object v2, p0, Lcom/microsoft/playready/RepresentationMediaInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v3, p0, Lcom/microsoft/playready/RepresentationMediaInfo;->mStreamIndex:I

    iget v4, p0, Lcom/microsoft/playready/RepresentationMediaInfo;->mRepIndex:I

    invoke-virtual {v2, v3, v4}, Lcom/microsoft/playready/Native_Class10;->method_17(II)I

    move-result v1

    .line 29
    .local v1, "fourccInt":I
    const/4 v2, 0x4

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    .line 30
    .local v0, "fourccByte":[B
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    return-object v2
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 3

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/playready/RepresentationMediaInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/RepresentationMediaInfo;->mStreamIndex:I

    iget v2, p0, Lcom/microsoft/playready/RepresentationMediaInfo;->mRepIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/playready/Native_Class10;->method_19(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMime()Ljava/lang/String;
    .locals 3

    .prologue
    .line 42
    iget-object v0, p0, Lcom/microsoft/playready/RepresentationMediaInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/RepresentationMediaInfo;->mStreamIndex:I

    iget v2, p0, Lcom/microsoft/playready/RepresentationMediaInfo;->mRepIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/playready/Native_Class10;->method_20(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSchemeId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/playready/RepresentationMediaInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v1, p0, Lcom/microsoft/playready/RepresentationMediaInfo;->mStreamIndex:I

    iget v2, p0, Lcom/microsoft/playready/RepresentationMediaInfo;->mRepIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/playready/Native_Class10;->method_18(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType()Lcom/microsoft/playready/MediaInfo$MediaType;
    .locals 4

    .prologue
    .line 24
    invoke-static {}, Lcom/microsoft/playready/MediaInfo$MediaType;->values()[Lcom/microsoft/playready/MediaInfo$MediaType;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/playready/RepresentationMediaInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v2, p0, Lcom/microsoft/playready/RepresentationMediaInfo;->mStreamIndex:I

    iget v3, p0, Lcom/microsoft/playready/RepresentationMediaInfo;->mRepIndex:I

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/playready/Native_Class10;->method_16(II)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getVideoInfo()Lcom/microsoft/playready/VideoInfo;
    .locals 4

    .prologue
    .line 53
    new-instance v0, Lcom/microsoft/playready/RepresentationVideoInfo;

    iget-object v1, p0, Lcom/microsoft/playready/RepresentationMediaInfo;->mNativeClass:Lcom/microsoft/playready/Native_Class10;

    iget v2, p0, Lcom/microsoft/playready/RepresentationMediaInfo;->mStreamIndex:I

    iget v3, p0, Lcom/microsoft/playready/RepresentationMediaInfo;->mRepIndex:I

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/playready/RepresentationVideoInfo;-><init>(Lcom/microsoft/playready/Native_Class10;II)V

    return-object v0
.end method
