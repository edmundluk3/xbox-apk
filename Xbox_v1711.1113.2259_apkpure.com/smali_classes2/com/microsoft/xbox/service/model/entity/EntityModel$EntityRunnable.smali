.class Lcom/microsoft/xbox/service/model/entity/EntityModel$EntityRunnable;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "EntityModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/entity/EntityModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EntityRunnable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/model/entity/Entity;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/service/model/entity/EntityModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/entity/EntityModel;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/entity/EntityModel$EntityRunnable;->this$0:Lcom/microsoft/xbox/service/model/entity/EntityModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/entity/EntityModel;Lcom/microsoft/xbox/service/model/entity/EntityModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/entity/EntityModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/entity/EntityModel$1;

    .prologue
    .line 100
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/entity/EntityModel$EntityRunnable;-><init>(Lcom/microsoft/xbox/service/model/entity/EntityModel;)V

    return-void
.end method

.method private toEntity(Lcom/google/gson/JsonElement;)Lcom/microsoft/xbox/service/model/entity/Entity;
    .locals 7
    .param p1, "elem"    # Lcom/google/gson/JsonElement;

    .prologue
    .line 127
    const/4 v1, 0x0

    .line 128
    .local v1, "entity":Lcom/microsoft/xbox/service/model/entity/Entity;
    invoke-virtual {p1}, Lcom/google/gson/JsonElement;->isJsonObject()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 129
    invoke-virtual {p1}, Lcom/google/gson/JsonElement;->getAsJsonObject()Lcom/google/gson/JsonObject;

    move-result-object v4

    .line 132
    .local v4, "obj":Lcom/google/gson/JsonObject;
    const-string v5, "item"

    invoke-virtual {v4, v5}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 133
    const-string v5, "item"

    invoke-virtual {v4, v5}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v3

    .line 134
    .local v3, "item":Lcom/google/gson/JsonElement;
    invoke-virtual {v3}, Lcom/google/gson/JsonElement;->isJsonObject()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 135
    new-instance v1, Lcom/microsoft/xbox/service/model/entity/Entity;

    .end local v1    # "entity":Lcom/microsoft/xbox/service/model/entity/Entity;
    invoke-virtual {v3}, Lcom/google/gson/JsonElement;->getAsJsonObject()Lcom/google/gson/JsonObject;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->toActivityFeedItem(Lcom/google/gson/JsonObject;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v5

    invoke-direct {v1, v5}, Lcom/microsoft/xbox/service/model/entity/Entity;-><init>(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    .line 155
    .end local v3    # "item":Lcom/google/gson/JsonElement;
    .end local v4    # "obj":Lcom/google/gson/JsonObject;
    .restart local v1    # "entity":Lcom/microsoft/xbox/service/model/entity/Entity;
    :cond_0
    :goto_0
    return-object v1

    .line 137
    .restart local v4    # "obj":Lcom/google/gson/JsonObject;
    :cond_1
    const-string v5, "gameClip"

    invoke-virtual {v4, v5}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 138
    const-string v5, "gameClip"

    invoke-virtual {v4, v5}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v2

    .line 139
    .local v2, "field":Lcom/google/gson/JsonElement;
    invoke-virtual {v2}, Lcom/google/gson/JsonElement;->isJsonObject()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 140
    new-instance v1, Lcom/microsoft/xbox/service/model/entity/Entity;

    .end local v1    # "entity":Lcom/microsoft/xbox/service/model/entity/Entity;
    invoke-virtual {v2}, Lcom/google/gson/JsonElement;->getAsJsonObject()Lcom/google/gson/JsonObject;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->toGameClip(Lcom/google/gson/JsonObject;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    move-result-object v5

    invoke-direct {v1, v5}, Lcom/microsoft/xbox/service/model/entity/Entity;-><init>(Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;)V

    .restart local v1    # "entity":Lcom/microsoft/xbox/service/model/entity/Entity;
    goto :goto_0

    .line 142
    .end local v2    # "field":Lcom/google/gson/JsonElement;
    :cond_2
    const-string v5, "achievements"

    invoke-virtual {v4, v5}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 143
    const-string v5, "achievements"

    invoke-virtual {v4, v5}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v2

    .line 144
    .restart local v2    # "field":Lcom/google/gson/JsonElement;
    invoke-virtual {v2}, Lcom/google/gson/JsonElement;->isJsonArray()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 145
    invoke-virtual {v2}, Lcom/google/gson/JsonElement;->getAsJsonArray()Lcom/google/gson/JsonArray;

    move-result-object v0

    .line 146
    .local v0, "array":Lcom/google/gson/JsonArray;
    invoke-virtual {v0}, Lcom/google/gson/JsonArray;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 147
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Lcom/google/gson/JsonArray;->get(I)Lcom/google/gson/JsonElement;

    move-result-object v3

    .line 148
    .restart local v3    # "item":Lcom/google/gson/JsonElement;
    invoke-virtual {v3}, Lcom/google/gson/JsonElement;->isJsonObject()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 149
    new-instance v1, Lcom/microsoft/xbox/service/model/entity/Entity;

    .end local v1    # "entity":Lcom/microsoft/xbox/service/model/entity/Entity;
    invoke-virtual {v3}, Lcom/google/gson/JsonElement;->getAsJsonObject()Lcom/google/gson/JsonObject;

    move-result-object v5

    invoke-static {v5}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->toAchievement(Lcom/google/gson/JsonObject;)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    move-result-object v5

    invoke-direct {v1, v5}, Lcom/microsoft/xbox/service/model/entity/Entity;-><init>(Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;)V

    .restart local v1    # "entity":Lcom/microsoft/xbox/service/model/entity/Entity;
    goto :goto_0
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/model/entity/Entity;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 107
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v2

    .line 108
    .local v2, "mgr":Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/entity/EntityModel$EntityRunnable;->this$0:Lcom/microsoft/xbox/service/model/entity/EntityModel;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->access$100(Lcom/microsoft/xbox/service/model/entity/EntityModel;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getEntity(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v0

    .line 109
    .local v0, "elem":Lcom/google/gson/JsonElement;
    const/4 v1, 0x0

    .line 110
    .local v1, "entity":Lcom/microsoft/xbox/service/model/entity/Entity;
    if-eqz v0, :cond_0

    .line 111
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/entity/EntityModel$EntityRunnable;->toEntity(Lcom/google/gson/JsonElement;)Lcom/microsoft/xbox/service/model/entity/Entity;

    move-result-object v1

    .line 113
    :cond_0
    return-object v1
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/entity/EntityModel$EntityRunnable;->buildData()Lcom/microsoft/xbox/service/model/entity/Entity;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 123
    const-wide/16 v0, 0xbef

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/entity/Entity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 118
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/entity/Entity;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/entity/EntityModel$EntityRunnable;->this$0:Lcom/microsoft/xbox/service/model/entity/EntityModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->updateWithNewData(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 119
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 103
    return-void
.end method
