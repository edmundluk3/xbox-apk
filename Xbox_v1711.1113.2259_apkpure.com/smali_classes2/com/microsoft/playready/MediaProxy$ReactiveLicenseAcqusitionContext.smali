.class Lcom/microsoft/playready/MediaProxy$ReactiveLicenseAcqusitionContext;
.super Ljava/lang/Object;
.source "MediaProxy.java"

# interfaces
.implements Lcom/microsoft/playready/MediaProxy$IReactiveLicenseAcquisitionContext;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/playready/MediaProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ReactiveLicenseAcqusitionContext"
.end annotation


# instance fields
.field private m_obfus_context:Lcom/microsoft/playready/Native_Class9$internal_class_3;


# direct methods
.method constructor <init>(Lcom/microsoft/playready/Native_Class9$internal_class_3;)V
    .locals 1
    .param p1, "obfus_context"    # Lcom/microsoft/playready/Native_Class9$internal_class_3;

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/playready/MediaProxy$ReactiveLicenseAcqusitionContext;->m_obfus_context:Lcom/microsoft/playready/Native_Class9$internal_class_3;

    .line 53
    iput-object p1, p0, Lcom/microsoft/playready/MediaProxy$ReactiveLicenseAcqusitionContext;->m_obfus_context:Lcom/microsoft/playready/Native_Class9$internal_class_3;

    .line 54
    return-void
.end method


# virtual methods
.method public generateLicenseChallenge(Ljava/lang/String;Lcom/microsoft/playready/DomainInfo;)Lcom/microsoft/playready/DrmProxy$ChallengeData;
    .locals 2
    .param p1, "customData"    # Ljava/lang/String;
    .param p2, "currentDomainInfo"    # Lcom/microsoft/playready/DomainInfo;

    .prologue
    .line 59
    iget-object v1, p0, Lcom/microsoft/playready/MediaProxy$ReactiveLicenseAcqusitionContext;->m_obfus_context:Lcom/microsoft/playready/Native_Class9$internal_class_3;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/microsoft/playready/DomainInfo;->getServiceID()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0, p1}, Lcom/microsoft/playready/Native_Class9$internal_class_3;->method_4(Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/playready/Native_Class8$internal_class_1;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/playready/DrmProxy;->challengeDataFromObfuscatedObj(Lcom/microsoft/playready/Native_Class8$internal_class_1;)Lcom/microsoft/playready/DrmProxy$ChallengeData;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public processLicenseReponse([B)Ljava/lang/String;
    .locals 1
    .param p1, "response"    # [B

    .prologue
    .line 64
    iget-object v0, p0, Lcom/microsoft/playready/MediaProxy$ReactiveLicenseAcqusitionContext;->m_obfus_context:Lcom/microsoft/playready/Native_Class9$internal_class_3;

    invoke-virtual {v0, p1}, Lcom/microsoft/playready/Native_Class9$internal_class_3;->method_5([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
