.class public final Lcom/microsoft/xbox/presentation/lce/DefaultLceViewStateLogger;
.super Ljava/lang/Object;
.source "DefaultLceViewStateLogger.java"


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "No instances"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public static toLogString(Lcom/microsoft/xbox/presentation/lce/LceViewState;)Ljava/lang/String;
    .locals 1
    .param p0, "viewState"    # Lcom/microsoft/xbox/presentation/lce/LceViewState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 18
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 20
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->isContent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 21
    const-string v0, "Content"

    .line 31
    :goto_0
    return-object v0

    .line 22
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->isError()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 23
    const-string v0, "Error"

    goto :goto_0

    .line 24
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->isLoading()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 25
    const-string v0, "Loading"

    goto :goto_0

    .line 26
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->isLoadingMore()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 27
    const-string v0, "Loading more"

    goto :goto_0

    .line 30
    :cond_3
    const-string v0, "Error in LceViewState:toLogString logic"

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 31
    const-string v0, "Unknown State"

    goto :goto_0
.end method
