.class Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;
.super Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;
.source "GcmModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/gcm/GcmModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RegisterChatTask"
.end annotation


# instance fields
.field private allMessagesChannels:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

.field private directMentionChannels:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private isChatRegistered:Z

.field private final meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private noNotificationChannels:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final shouldLoadChannelInfo:Z

.field private systemId:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel;ZLjava/util/Set;Ljava/util/Set;Ljava/util/Set;)V
    .locals 1
    .param p2, "shouldLoadChannelInfo"    # Z
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/Set;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Ljava/util/Set;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1020
    .local p3, "allMessagesChannels":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p4, "directMentionChannels":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p5, "noNotificationChannels":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;-><init>(Lcom/microsoft/xbox/service/model/gcm/GcmModel$1;)V

    .line 1005
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 1021
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1022
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1023
    invoke-static {p5}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1025
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->allMessagesChannels:Ljava/util/Set;

    .line 1026
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->directMentionChannels:Ljava/util/Set;

    .line 1027
    iput-object p5, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->noNotificationChannels:Ljava/util/Set;

    .line 1029
    iput-boolean p2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->shouldLoadChannelInfo:Z

    .line 1030
    return-void
.end method

.method private fromChatChannelSetToStringSet(Ljava/util/Set;)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1243
    .local p1, "chatChannelSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;>;"
    new-instance v1, Ljava/util/HashSet;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(I)V

    .line 1244
    .local v1, "result":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    .line 1245
    .local v0, "channel":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1248
    .end local v0    # "channel":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    :cond_0
    return-object v1
.end method

.method private fromStringSetToChatChannelSet(Ljava/util/Set;)Ljava/util/Set;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1234
    .local p1, "stringSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/HashSet;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(I)V

    .line 1235
    .local v1, "result":Ljava/util/Set;, "Ljava/util/Set<Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;>;"
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1236
    .local v0, "jsonString":Ljava/lang/String;
    const-class v3, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    const-class v4, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    new-instance v5, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelIdJsonAdapter;

    invoke-direct {v5}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelIdJsonAdapter;-><init>()V

    invoke-static {v0, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->deserializeJson(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1239
    .end local v0    # "jsonString":Ljava/lang/String;
    :cond_0
    return-object v1
.end method

.method private refreshChatTicket(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)Z
    .locals 5
    .param p1, "channel"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    .prologue
    const/4 v1, 0x0

    .line 1205
    if-nez p1, :cond_0

    .line 1230
    :goto_0
    return v1

    .line 1209
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-nez v2, :cond_1

    .line 1210
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "RegisterChatNotification - meProfileModel is null"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1214
    :cond_1
    instance-of v2, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;

    if-eqz v2, :cond_3

    .line 1215
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1217
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$2100()Lcom/microsoft/xbox/service/clubs/IClubChatManagementService;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;->getId()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuid()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/microsoft/xbox/service/clubs/IClubChatManagementService;->refreshClubChatTicket(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 1218
    :catch_0
    move-exception v0

    .line 1219
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "RegisterChatNotification - refreshChatTicket throws exception"

    invoke-static {v2, v3, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1223
    .end local v0    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "refreshChatTicket - channelId is empty"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1227
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RegisterChatTask - not supported chat type to refresh ticket:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private registerWithChatBackend(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$IChatNotificationRegistrationResult;
    .locals 6
    .param p1, "regId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;",
            ">;)",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$IChatNotificationRegistrationResult;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1193
    .local p2, "allMessages":Ljava/util/Set;, "Ljava/util/Set<Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;>;"
    .local p3, "directMention":Ljava/util/Set;, "Ljava/util/Set<Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;>;"
    new-instance v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostBody;

    invoke-direct {v0, p1, p2, p3}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostBody;-><init>(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)V

    .line 1194
    .local v0, "notification":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostBody;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostBody;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1196
    .local v1, "postBody":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    if-nez v2, :cond_0

    .line 1197
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "RegisterChatNotification - meProfileModel is null"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1198
    const/4 v2, 0x0

    .line 1201
    :goto_0
    return-object v2

    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$2000()Lcom/microsoft/xbox/service/chat/IChatService;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getXuidLong()J

    move-result-wide v4

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->systemId:Ljava/lang/String;

    invoke-interface {v2, v4, v5, v3, v1}, Lcom/microsoft/xbox/service/chat/IChatService;->registerForNotifications(JLjava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$IChatNotificationRegistrationResult;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1003
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 29
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 1044
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->allMessagesChannels:Ljava/util/Set;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->fromStringSetToChatChannelSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v4

    .line 1045
    .local v4, "allMessagesObjSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->directMentionChannels:Ljava/util/Set;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->fromStringSetToChatChannelSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v10

    .line 1046
    .local v10, "directMentionObjSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->noNotificationChannels:Ljava/util/Set;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->fromStringSetToChatChannelSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v14

    .line 1048
    .local v14, "noNotificationObjSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;>;"
    const/4 v7, 0x0

    .line 1049
    .local v7, "channelUpdated":Z
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->shouldLoadChannelInfo:Z

    move/from16 v19, v0

    if-eqz v19, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    move-object/from16 v19, v0

    if-eqz v19, :cond_8

    .line 1050
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v19

    const-string v20, "RegisterChatTask - load channel data"

    invoke-static/range {v19 .. v20}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1052
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadClubs(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v5

    .line 1053
    .local v5, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 1055
    .local v17, "userChannels":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;>;"
    invoke-virtual {v5}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v19

    if-nez v19, :cond_0

    .line 1056
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/microsoft/xbox/service/model/ProfileModel;->getClubs()Ljava/util/List;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v19

    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask$$Lambda$1;->lambdaFactory$()Lcom/microsoft/xbox/toolkit/java8/Predicate;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/microsoft/xbox/toolkit/ListUtil;->filter(Ljava/util/List;Lcom/microsoft/xbox/toolkit/java8/Predicate;)Ljava/util/List;

    move-result-object v18

    .line 1058
    .local v18, "userClubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_0
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_0

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 1059
    .local v8, "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    new-instance v20, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;

    invoke-virtual {v8}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v22

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ClubChatChannelId;-><init>(J)V

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1064
    .end local v8    # "club":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .end local v18    # "userClubs":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    :cond_0
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_1
    :goto_1
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_2

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    .line 1065
    .local v6, "channel":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    invoke-interface {v4, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_1

    .line 1066
    invoke-interface {v10, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_1

    .line 1067
    invoke-interface {v14, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_1

    .line 1068
    const/4 v7, 0x1

    .line 1069
    invoke-interface {v10, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1074
    .end local v6    # "channel":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    :cond_2
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;>;"
    :cond_3
    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_4

    .line 1075
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    .line 1076
    .restart local v6    # "channel":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_3

    .line 1077
    const/4 v7, 0x1

    .line 1078
    invoke-interface {v13}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    .line 1082
    .end local v6    # "channel":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    :cond_4
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_5
    :goto_3
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_6

    .line 1083
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    .line 1084
    .restart local v6    # "channel":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_5

    .line 1085
    const/4 v7, 0x1

    .line 1086
    invoke-interface {v13}, Ljava/util/Iterator;->remove()V

    goto :goto_3

    .line 1090
    .end local v6    # "channel":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    :cond_6
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_7
    :goto_4
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_8

    .line 1091
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    .line 1092
    .restart local v6    # "channel":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_7

    .line 1093
    invoke-interface {v13}, Ljava/util/Iterator;->remove()V

    goto :goto_4

    .line 1099
    .end local v5    # "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    .end local v6    # "channel":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    .end local v13    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;>;"
    .end local v17    # "userChannels":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;>;"
    :cond_8
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$1800()[I

    move-result-object v20

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v21, v0

    const/16 v19, 0x0

    :goto_5
    move/from16 v0, v19

    move/from16 v1, v21

    if-ge v0, v1, :cond_9

    aget v9, v20, v19

    .line 1100
    .local v9, "delay":I
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->isCancelled()Z

    move-result v22

    if-eqz v22, :cond_a

    .line 1170
    .end local v9    # "delay":I
    :cond_9
    :goto_6
    const/16 v19, 0x0

    return-object v19

    .line 1104
    .restart local v9    # "delay":I
    :cond_a
    if-lez v9, :cond_b

    .line 1105
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v22

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "RegisterChatTask - "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    sget-object v24, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v25, "Waiting for %d seconds"

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-static/range {v24 .. v26}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1106
    mul-int/lit16 v0, v9, 0x3e8

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Thread;->sleep(J)V

    .line 1109
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->isCancelled()Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v22

    if-nez v22, :cond_9

    .line 1114
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->isGCMRegistered()Z

    move-result v22

    if-nez v22, :cond_d

    .line 1115
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v22

    const-string v23, "RegisterChatTask - registration id is not ensured"

    invoke-static/range {v22 .. v23}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_6

    .line 1162
    :catch_0
    move-exception v11

    .line 1163
    .local v11, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :try_start_2
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v22

    const-string v23, "RegisterChatTask Exception"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-static {v0, v1, v11}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1099
    .end local v11    # "e":Lcom/microsoft/xbox/toolkit/XLEException;
    :cond_c
    add-int/lit8 v19, v19, 0x1

    goto :goto_5

    .line 1119
    :cond_d
    :try_start_3
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->isCancelled()Z

    move-result v22

    if-nez v22, :cond_9

    .line 1123
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->systemId:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v22

    if-eqz v22, :cond_e

    .line 1124
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->registrationId:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-static/range {v22 .. v22}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$1300(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->systemId:Ljava/lang/String;

    .line 1127
    :cond_e
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->shouldLoadChannelInfo:Z

    move/from16 v22, v0

    if-eqz v22, :cond_f

    if-eqz v7, :cond_14

    .line 1128
    :cond_f
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v22

    const-string v23, "Registering Chat Notification"

    invoke-static/range {v22 .. v23}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1129
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;->registrationId:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v4, v10}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->registerWithChatBackend(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$IChatNotificationRegistrationResult;

    move-result-object v15

    .line 1134
    .local v15, "registrationResult":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$IChatNotificationRegistrationResult;
    instance-of v0, v15, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostSuccess;

    move/from16 v22, v0

    if-eqz v22, :cond_11

    .line 1135
    const/16 v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->isChatRegistered:Z

    .line 1155
    .end local v15    # "registrationResult":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$IChatNotificationRegistrationResult;
    :cond_10
    :goto_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->isChatRegistered:Z

    move/from16 v22, v0

    if-eqz v22, :cond_c

    .line 1156
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->fromChatChannelSetToStringSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->allMessagesChannels:Ljava/util/Set;

    .line 1157
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->fromChatChannelSetToStringSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->directMentionChannels:Ljava/util/Set;

    .line 1158
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->fromChatChannelSetToStringSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->noNotificationChannels:Ljava/util/Set;
    :try_end_3
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_6

    .line 1166
    .end local v9    # "delay":I
    :catch_1
    move-exception v11

    .line 1167
    .local v11, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v19

    const-string v20, "RegisterChatTask - InterruptedException"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-static {v0, v1, v11}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_6

    .line 1137
    .end local v11    # "e":Ljava/lang/InterruptedException;
    .restart local v9    # "delay":I
    .restart local v15    # "registrationResult":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$IChatNotificationRegistrationResult;
    :cond_11
    :try_start_4
    instance-of v0, v15, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostResponse;

    move/from16 v22, v0

    if-eqz v22, :cond_13

    .line 1138
    move-object v0, v15

    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostResponse;

    move-object/from16 v16, v0

    .line 1139
    .local v16, "response":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostResponse;
    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostResponse;->getUnauthorizedChannelIds()Ljava/util/Set;

    move-result-object v12

    .line 1140
    .local v12, "failedChannels":Ljava/util/Set;, "Ljava/util/Set<Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;>;"
    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :cond_12
    :goto_8
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_10

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    .line 1141
    .restart local v6    # "channel":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->refreshChatTicket(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)Z

    move-result v23

    if-nez v23, :cond_12

    .line 1142
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v23

    const-string v24, "RegisterChatTask refresh ticket failed, remove from request"

    invoke-static/range {v23 .. v24}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1143
    invoke-interface {v4, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1144
    invoke-interface {v10, v6}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_8

    .line 1149
    .end local v6    # "channel":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    .end local v12    # "failedChannels":Ljava/util/Set;, "Ljava/util/Set<Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;>;"
    .end local v16    # "response":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$ChatNotificationPostResponse;
    :cond_13
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v22

    const-string v23, "RegisterChatTask - registration failed, registrationResult is null"

    invoke-static/range {v22 .. v23}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 1152
    .end local v15    # "registrationResult":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatNotificationDataTypes$IChatNotificationRegistrationResult;
    :cond_14
    const/16 v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->isChatRegistered:Z
    :try_end_4
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_7
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1003
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 6
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    const/4 v5, 0x0

    .line 1175
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1177
    invoke-static {}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "RegisterChatTask - onPostExecute"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 1178
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1179
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->isChatRegistered:Z

    if-eqz v1, :cond_0

    .line 1180
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$900(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1181
    .local v0, "edt":Landroid/content/SharedPreferences$Editor;
    const-string v1, "chat.am.channels"

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->allMessagesChannels:Ljava/util/Set;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 1182
    const-string v1, "chat.dm.channels"

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->directMentionChannels:Ljava/util/Set;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 1183
    const-string v1, "chat.no.channels"

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->noNotificationChannels:Ljava/util/Set;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 1184
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1187
    .end local v0    # "edt":Landroid/content/SharedPreferences$Editor;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v1, v5}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$1902(Lcom/microsoft/xbox/service/model/gcm/GcmModel;Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;)Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;

    .line 1188
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    new-instance v2, Lcom/microsoft/xbox/toolkit/AsyncResult;

    sget-object v3, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;->STATE_CHANGED:Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-direct {v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 1190
    :cond_1
    return-void
.end method

.method protected onPreExecute()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1034
    invoke-super {p0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$PushTask;->onPreExecute()V

    .line 1036
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$1200(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->curRegistration:Lcom/microsoft/xbox/service/model/gcm/GcmModel$Registration;

    .line 1037
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->access$900(Lcom/microsoft/xbox/service/model/gcm/GcmModel;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "system.id"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->systemId:Ljava/lang/String;

    .line 1039
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    sget-object v2, Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;->STATE_CHANGED:Lcom/microsoft/xbox/service/model/gcm/GcmModel$GcmEvent;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask;->this$0:Lcom/microsoft/xbox/service/model/gcm/GcmModel;

    invoke-direct {v1, v2, v3, v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/gcm/GcmModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 1040
    return-void
.end method
