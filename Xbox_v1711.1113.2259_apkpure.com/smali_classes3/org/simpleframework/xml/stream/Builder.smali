.class Lorg/simpleframework/xml/stream/Builder;
.super Ljava/lang/Object;
.source "Builder.java"

# interfaces
.implements Lorg/simpleframework/xml/stream/Style;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/simpleframework/xml/stream/Builder$Cache;
    }
.end annotation


# instance fields
.field private final attributes:Lorg/simpleframework/xml/stream/Builder$Cache;

.field private final elements:Lorg/simpleframework/xml/stream/Builder$Cache;

.field private final style:Lorg/simpleframework/xml/stream/Style;


# direct methods
.method public constructor <init>(Lorg/simpleframework/xml/stream/Style;)V
    .locals 1
    .param p1, "style"    # Lorg/simpleframework/xml/stream/Style;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Lorg/simpleframework/xml/stream/Builder$Cache;

    invoke-direct {v0, p0}, Lorg/simpleframework/xml/stream/Builder$Cache;-><init>(Lorg/simpleframework/xml/stream/Builder;)V

    iput-object v0, p0, Lorg/simpleframework/xml/stream/Builder;->attributes:Lorg/simpleframework/xml/stream/Builder$Cache;

    .line 70
    new-instance v0, Lorg/simpleframework/xml/stream/Builder$Cache;

    invoke-direct {v0, p0}, Lorg/simpleframework/xml/stream/Builder$Cache;-><init>(Lorg/simpleframework/xml/stream/Builder;)V

    iput-object v0, p0, Lorg/simpleframework/xml/stream/Builder;->elements:Lorg/simpleframework/xml/stream/Builder$Cache;

    .line 71
    iput-object p1, p0, Lorg/simpleframework/xml/stream/Builder;->style:Lorg/simpleframework/xml/stream/Style;

    .line 72
    return-void
.end method


# virtual methods
.method public getAttribute(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 85
    iget-object v2, p0, Lorg/simpleframework/xml/stream/Builder;->attributes:Lorg/simpleframework/xml/stream/Builder$Cache;

    invoke-virtual {v2, p1}, Lorg/simpleframework/xml/stream/Builder$Cache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 87
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    move-object v1, v0

    .line 95
    .end local v0    # "value":Ljava/lang/String;
    .local v1, "value":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 90
    .end local v1    # "value":Ljava/lang/String;
    .restart local v0    # "value":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lorg/simpleframework/xml/stream/Builder;->style:Lorg/simpleframework/xml/stream/Style;

    invoke-interface {v2, p1}, Lorg/simpleframework/xml/stream/Style;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 92
    if-eqz v0, :cond_1

    .line 93
    iget-object v2, p0, Lorg/simpleframework/xml/stream/Builder;->attributes:Lorg/simpleframework/xml/stream/Builder$Cache;

    invoke-virtual {v2, p1, v0}, Lorg/simpleframework/xml/stream/Builder$Cache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    move-object v1, v0

    .line 95
    .end local v0    # "value":Ljava/lang/String;
    .restart local v1    # "value":Ljava/lang/String;
    goto :goto_0
.end method

.method public getElement(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 109
    iget-object v2, p0, Lorg/simpleframework/xml/stream/Builder;->elements:Lorg/simpleframework/xml/stream/Builder$Cache;

    invoke-virtual {v2, p1}, Lorg/simpleframework/xml/stream/Builder$Cache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 111
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    move-object v1, v0

    .line 119
    .end local v0    # "value":Ljava/lang/String;
    .local v1, "value":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 114
    .end local v1    # "value":Ljava/lang/String;
    .restart local v0    # "value":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lorg/simpleframework/xml/stream/Builder;->style:Lorg/simpleframework/xml/stream/Style;

    invoke-interface {v2, p1}, Lorg/simpleframework/xml/stream/Style;->getElement(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 116
    if-eqz v0, :cond_1

    .line 117
    iget-object v2, p0, Lorg/simpleframework/xml/stream/Builder;->elements:Lorg/simpleframework/xml/stream/Builder$Cache;

    invoke-virtual {v2, p1, v0}, Lorg/simpleframework/xml/stream/Builder$Cache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    move-object v1, v0

    .line 119
    .end local v0    # "value":Ljava/lang/String;
    .restart local v1    # "value":Ljava/lang/String;
    goto :goto_0
.end method

.method public setAttribute(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 132
    iget-object v0, p0, Lorg/simpleframework/xml/stream/Builder;->attributes:Lorg/simpleframework/xml/stream/Builder$Cache;

    invoke-virtual {v0, p1, p2}, Lorg/simpleframework/xml/stream/Builder$Cache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    return-void
.end method

.method public setElement(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 145
    iget-object v0, p0, Lorg/simpleframework/xml/stream/Builder;->elements:Lorg/simpleframework/xml/stream/Builder$Cache;

    invoke-virtual {v0, p1, p2}, Lorg/simpleframework/xml/stream/Builder$Cache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    return-void
.end method
