.class public interface abstract Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;
.super Ljava/lang/Object;
.source "TagPickerDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnTagsSelectedHandler"
.end annotation


# virtual methods
.method public abstract onTagsSelected(Ljava/util/List;)V
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
            ">;)V"
        }
    .end annotation
.end method
