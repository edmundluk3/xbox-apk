.class Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;
.super Ljava/lang/Object;
.source "DefaultChatHeadManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ArrangementChangeRequest"
.end annotation


# instance fields
.field private final animated:Z

.field private final arrangement:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/flipkart/chatheads/ui/ChatHeadArrangement;",
            ">;"
        }
    .end annotation
.end field

.field private chatHeadToBringToFront:Lcom/flipkart/chatheads/ui/ChatHead;

.field private final extras:Landroid/os/Bundle;

.field final synthetic this$0:Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;


# direct methods
.method public constructor <init>(Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;Ljava/lang/Class;Landroid/os/Bundle;Z)V
    .locals 0
    .param p3, "extras"    # Landroid/os/Bundle;
    .param p4, "animated"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/flipkart/chatheads/ui/ChatHeadArrangement;",
            ">;",
            "Landroid/os/Bundle;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 655
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>.ArrangementChangeRequest;"
    .local p2, "arrangement":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/flipkart/chatheads/ui/ChatHeadArrangement;>;"
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;->this$0:Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 656
    iput-object p2, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;->arrangement:Ljava/lang/Class;

    .line 657
    iput-object p3, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;->extras:Landroid/os/Bundle;

    .line 658
    iput-boolean p4, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;->animated:Z

    .line 659
    return-void
.end method

.method static synthetic access$000(Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;)Lcom/flipkart/chatheads/ui/ChatHead;
    .locals 1
    .param p0, "x0"    # Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;

    .prologue
    .line 648
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;->chatHeadToBringToFront:Lcom/flipkart/chatheads/ui/ChatHead;

    return-object v0
.end method

.method static synthetic access$002(Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;Lcom/flipkart/chatheads/ui/ChatHead;)Lcom/flipkart/chatheads/ui/ChatHead;
    .locals 0
    .param p0, "x0"    # Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;
    .param p1, "x1"    # Lcom/flipkart/chatheads/ui/ChatHead;

    .prologue
    .line 648
    iput-object p1, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;->chatHeadToBringToFront:Lcom/flipkart/chatheads/ui/ChatHead;

    return-object p1
.end method


# virtual methods
.method public getArrangement()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/flipkart/chatheads/ui/ChatHeadArrangement;",
            ">;"
        }
    .end annotation

    .prologue
    .line 666
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>.ArrangementChangeRequest;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;->arrangement:Ljava/lang/Class;

    return-object v0
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 662
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>.ArrangementChangeRequest;"
    iget-object v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;->extras:Landroid/os/Bundle;

    return-object v0
.end method

.method public isAnimated()Z
    .locals 1

    .prologue
    .line 670
    .local p0, "this":Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;, "Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager<TT;>.ArrangementChangeRequest;"
    iget-boolean v0, p0, Lcom/flipkart/chatheads/ui/container/DefaultChatHeadManager$ArrangementChangeRequest;->animated:Z

    return v0
.end method
