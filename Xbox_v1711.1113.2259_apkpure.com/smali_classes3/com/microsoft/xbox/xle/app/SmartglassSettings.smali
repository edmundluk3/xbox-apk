.class public Lcom/microsoft/xbox/xle/app/SmartglassSettings;
.super Ljava/lang/Object;
.source "SmartglassSettings.java"


# instance fields
.field public ACTIONABLENON360CONTENTTYPE:Ljava/lang/String;

.field public ANDROID_VERSIONLATEST:I

.field public ANDROID_VERSIONLATEST_BETA:I

.field public ANDROID_VERSIONMIN:I

.field public ANDROID_VERSIONMINOS:I

.field public ANDROID_VERSIONMINOS_BETA:I

.field public ANDROID_VERSIONMIN_BETA:I

.field public ANDROID_VERSIONURL:Ljava/lang/String;

.field public ANDROID_VERSIONURL_BETA:Ljava/lang/String;

.field public ARCHES_CATALOG:Ljava/lang/String;

.field public ARCHES_CATALOG_BETA:Ljava/lang/String;

.field public ARCHES_ENABLED:Ljava/lang/String;

.field public ARCHES_ENABLED_BETA:Ljava/lang/String;

.field public BEAM_APP_DEEPLINK_ENABLED:Ljava/lang/String;

.field public BEAM_APP_DEEPLINK_ENABLED_BETA:Ljava/lang/String;

.field public BEAM_TRENDING_ENABLED:Ljava/lang/String;

.field public BEAM_TRENDING_ENABLED_BETA:Ljava/lang/String;

.field public BLOCK_FEATURED_CHILD:Ljava/lang/String;

.field public CUSTOM_PIC_UPLOADING_ENABLED:Ljava/lang/String;

.field public CUSTOM_PIC_UPLOADING_ENABLED_BETA:Ljava/lang/String;

.field public DEFAULTSANDBOXID:Ljava/lang/String;

.field public HELPCOMPANION_ALLOWEDURLS:Ljava/lang/String;

.field public HIDDEN_MRU_ITEMS:Ljava/lang/String;

.field public MUSIC_BLOCKED:Ljava/lang/String;

.field public PROMOTIONAL_CONTENT_RESTRICTED_REGIONS:Ljava/lang/String;

.field public PURCHASE_BLOCKED:Ljava/lang/String;

.field public REMOTE_CONTROL_SPECIALS:Ljava/lang/String;

.field public SHORSE_ENABLED:Ljava/lang/String;

.field public SHORSE_ENABLED_BETA:Ljava/lang/String;

.field public SNAPPABLE_SYSTEM_APPS:Ljava/lang/String;

.field public TUTORIAL_EXPERIENCE_ENABLED:Ljava/lang/String;

.field public TUTORIAL_EXPERIENCE_ENABLED_BETA:Ljava/lang/String;

.field public VIDEO_BLOCKED:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
