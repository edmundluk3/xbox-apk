.class public Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;
.super Landroid/widget/FrameLayout;
.source "PanoramaBody.java"


# static fields
.field private static final SCREEN_BODY_ANIMATION_NAME:Ljava/lang/String; = "Screen"


# instance fields
.field private childThatHandlesTouch:I

.field private classToChildMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private linearLayout:Landroid/widget/LinearLayout;

.field private linearLayoutWidth:J

.field private nameToChildMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private panes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;"
        }
    .end annotation
.end field

.field private scroll:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaHorizontalScrollView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 69
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 50
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    .line 51
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->nameToChildMap:Ljava/util/HashMap;

    .line 52
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->classToChildMap:Ljava/util/HashMap;

    .line 56
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->linearLayout:Landroid/widget/LinearLayout;

    .line 57
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->scroll:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaHorizontalScrollView;

    .line 58
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->childThatHandlesTouch:I

    .line 59
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->linearLayoutWidth:J

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 80
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    .line 51
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->nameToChildMap:Ljava/util/HashMap;

    .line 52
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->classToChildMap:Ljava/util/HashMap;

    .line 56
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->linearLayout:Landroid/widget/LinearLayout;

    .line 57
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->scroll:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaHorizontalScrollView;

    .line 58
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->childThatHandlesTouch:I

    .line 59
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->linearLayoutWidth:J

    .line 82
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method private getScreenBodyAnimation(Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;
    .locals 3
    .param p1, "animationType"    # Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;
    .param p2, "goingBack"    # Z

    .prologue
    .line 275
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->scroll:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaHorizontalScrollView;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 276
    invoke-static {}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getInstance()Lcom/microsoft/xbox/toolkit/anim/MAAS;

    move-result-object v1

    const-string v2, "Screen"

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/anim/MAAS;->getAnimation(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/anim/MAASAnimation;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimationPackageNavigationManager;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->scroll:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaHorizontalScrollView;

    invoke-virtual {v1, p1, p2, v2}, Lcom/microsoft/xbox/xle/anim/XLEMAASAnimationPackageNavigationManager;->compile(Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;ZLandroid/view/View;)Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;

    move-result-object v0

    .line 277
    .local v0, "screenBodyAnimation":Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;
    return-object v0
.end method


# virtual methods
.method public addPane(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;I)V
    .locals 1
    .param p1, "pane"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .param p2, "index"    # I

    .prologue
    .line 123
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getScreenPercent()I

    move-result v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->addPane(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;II)V

    .line 124
    return-void
.end method

.method public addPane(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;II)V
    .locals 8
    .param p1, "pane"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .param p2, "index"    # I
    .param p3, "screenPercent"    # I

    .prologue
    const/4 v4, -0x2

    const/high16 v7, 0x42c80000    # 100.0f

    const/4 v2, 0x0

    const/4 v6, -0x1

    .line 130
    invoke-virtual {p1, v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setDrawingCacheEnabled(Z)V

    .line 131
    invoke-virtual {p1, v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setVisibility(I)V

    .line 132
    invoke-virtual {p1, v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setIsPivotPane(Z)V

    .line 133
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setIsSubPane(Z)V

    .line 136
    const-wide/16 v0, 0x0

    .line 137
    .local v0, "width":D
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v2, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 138
    if-ne p3, v4, :cond_0

    .line 139
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->linearLayout:Landroid/widget/LinearLayout;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v4, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, p1, p2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 147
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->nameToChildMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->classToChildMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    return-void

    .line 141
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getScreenWidth()I

    move-result v2

    mul-int/2addr v2, p3

    int-to-float v2, v2

    div-float/2addr v2, v7

    float-to-double v0, v2

    .line 142
    iget-wide v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->linearLayoutWidth:J

    int-to-long v4, p3

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->linearLayoutWidth:J

    .line 143
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->linearLayout:Landroid/widget/LinearLayout;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    double-to-int v4, v0

    invoke-direct {v3, v4, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, p1, p2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 144
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->linearLayout:Landroid/widget/LinearLayout;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    iget-wide v4, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->linearLayoutWidth:J

    long-to-float v4, v4

    div-float/2addr v4, v7

    float-to-int v4, v4

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getScreenWidth()I

    move-result v5

    mul-int/2addr v4, v5

    invoke-direct {v3, v4, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public adjustBottomMargin(I)V
    .locals 3
    .param p1, "bottomMargin"    # I

    .prologue
    .line 377
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 378
    .local v0, "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->adjustBottomMargin(I)V

    goto :goto_0

    .line 380
    .end local v0    # "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method public adjustPaneSize(Ljava/lang/Class;I)V
    .locals 3
    .param p2, "percent"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 207
    .local p1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->getIndexOfScreen(Ljava/lang/Class;)I

    move-result v0

    .line 208
    .local v0, "index":I
    if-ltz v0, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 209
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->removePane(I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    .line 210
    .local v1, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v1, p2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setScreenPercent(I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 211
    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->addPane(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;I)V

    .line 213
    .end local v1    # "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method public forceRefresh()V
    .locals 3

    .prologue
    .line 407
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 408
    .local v0, "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->forceRefresh()V

    goto :goto_0

    .line 410
    .end local v0    # "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method public getAnimateIn(Z)Ljava/util/ArrayList;
    .locals 3
    .param p1, "goingBack"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 253
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 255
    .local v0, "animations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;>;"
    sget-object v2, Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;->ANIMATE_IN:Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;

    invoke-direct {p0, v2, p1}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->getScreenBodyAnimation(Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;

    move-result-object v1

    .line 256
    .local v1, "screenBodyAnimation":Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;
    if-eqz v1, :cond_0

    .line 257
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 260
    :cond_0
    return-object v0
.end method

.method public getAnimateOut(Z)Ljava/util/ArrayList;
    .locals 3
    .param p1, "goingBack"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 264
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 266
    .local v0, "animations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;>;"
    sget-object v2, Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;->ANIMATE_OUT:Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;

    invoke-direct {p0, v2, p1}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->getScreenBodyAnimation(Lcom/microsoft/xbox/toolkit/anim/MAAS$MAASAnimationType;Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;

    move-result-object v1

    .line 267
    .local v1, "screenBodyAnimation":Lcom/microsoft/xbox/toolkit/anim/XLEAnimation;
    if-eqz v1, :cond_0

    .line 268
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 271
    :cond_0
    return-object v0
.end method

.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->scroll:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaHorizontalScrollView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaHorizontalScrollView;->getScrollX()I

    move-result v0

    return v0
.end method

.method public getFirstVisibleChild()I
    .locals 5

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->getScrollX()I

    move-result v2

    .line 222
    .local v2, "x":I
    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x1

    if-gt v3, v4, :cond_2

    .line 223
    :cond_0
    const/4 v1, 0x0

    .line 234
    :cond_1
    :goto_0
    return v1

    .line 226
    :cond_2
    const/4 v0, 0x0

    .line 227
    .local v0, "curRightEdge":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_3

    .line 228
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getWidth()I

    move-result v3

    add-int/2addr v0, v3

    .line 229
    if-gt v0, v2, :cond_1

    .line 227
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 234
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v1, v3, -0x1

    goto :goto_0
.end method

.method public getIndexOfScreen(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)I
    .locals 3
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 398
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->nameToChildMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->nameToChildMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 403
    :goto_0
    return v0

    .line 402
    :cond_0
    const-string v0, "PanromaBody"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "can\'t find index for screen class "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getIndexOfScreen(Ljava/lang/Class;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 389
    .local p1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->classToChildMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->classToChildMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 394
    :goto_0
    return v0

    .line 393
    :cond_0
    const-string v0, "PanromaBody"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "can\'t find index for screen class "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getPane(I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 357
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 360
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handleTouch(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 216
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->childThatHandlesTouch:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 217
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->childThatHandlesTouch:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 216
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initialize([Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 7
    .param p1, "panes"    # [Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    const/4 v6, -0x1

    .line 95
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    .line 96
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->nameToChildMap:Ljava/util/HashMap;

    .line 97
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->classToChildMap:Ljava/util/HashMap;

    .line 99
    new-instance v2, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->linearLayout:Landroid/widget/LinearLayout;

    .line 100
    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaHorizontalScrollView;

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaHorizontalScrollView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->scroll:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaHorizontalScrollView;

    .line 101
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->linearLayout:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 105
    if-eqz p1, :cond_0

    .line 106
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 107
    aget-object v1, p1, v0

    .line 108
    .local v1, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->addPane(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;I)V

    .line 106
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 112
    .end local v0    # "i":I
    .end local v1    # "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->scroll:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaHorizontalScrollView;

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->linearLayout:Landroid/widget/LinearLayout;

    iget-wide v4, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->linearLayoutWidth:J

    long-to-float v4, v4

    const/high16 v5, 0x42c80000    # 100.0f

    div-float/2addr v4, v5

    float-to-int v4, v4

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getScreenWidth()I

    move-result v5

    mul-int/2addr v4, v5

    invoke-virtual {v2, v3, v4, v6}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaHorizontalScrollView;->addView(Landroid/view/View;II)V

    .line 113
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->scroll:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaHorizontalScrollView;

    const/4 v3, -0x2

    invoke-virtual {p0, v2, v3, v6}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->addView(Landroid/view/View;II)V

    .line 114
    return-void
.end method

.method public onAnimateInCompleted()V
    .locals 3

    .prologue
    .line 324
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 325
    .local v0, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->forceUpdateViewImmediately()V

    goto :goto_0

    .line 327
    .end local v0    # "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method public onAnimateInStarted()V
    .locals 3

    .prologue
    .line 317
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 318
    .local v0, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->forceUpdateViewImmediately()V

    goto :goto_0

    .line 320
    .end local v0    # "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 348
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->getFirstVisibleChild()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 247
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 248
    .local v0, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onCreate()V

    goto :goto_0

    .line 250
    .end local v0    # "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 352
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->getFirstVisibleChild()I

    move-result v0

    .line 353
    .local v0, "firstChild":I
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v1, p1, p2, p3}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 354
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 330
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 331
    .local v0, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onDestroy()V

    goto :goto_0

    .line 333
    .end local v0    # "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 300
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 302
    .local v0, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onPause()V

    goto :goto_0

    .line 305
    .end local v0    # "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method protected onRehydrate()V
    .locals 3

    .prologue
    .line 342
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 343
    .local v0, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onRehydrate()V

    goto :goto_0

    .line 345
    .end local v0    # "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 308
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 310
    .local v0, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onResume()V

    goto :goto_0

    .line 313
    .end local v0    # "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 2
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 118
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 119
    const-string v0, "PanromaBody"

    const-string v1, "on size changed called "

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 286
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 287
    .local v0, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onStart()V

    goto :goto_0

    .line 289
    .end local v0    # "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 3

    .prologue
    .line 292
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 294
    .local v0, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onStop()V

    goto :goto_0

    .line 297
    .end local v0    # "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method protected onTombstone()V
    .locals 3

    .prologue
    .line 336
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 337
    .local v0, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onTombstone()V

    goto :goto_0

    .line 339
    .end local v0    # "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method public removePane(I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 5
    .param p1, "index"    # I

    .prologue
    .line 152
    if-ltz p1, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge p1, v2, :cond_0

    .line 153
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 154
    .local v1, "removedScreen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->nameToChildMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->classToChildMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->removeViewFromParent(Landroid/view/View;)V

    .line 157
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 159
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->nameToChildMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 160
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->classToChildMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 162
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 163
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->nameToChildMap:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->classToChildMap:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 169
    .end local v0    # "i":I
    .end local v1    # "removedScreen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    const/4 v1, 0x0

    :cond_1
    return-object v1
.end method

.method public resetBottomMargin()V
    .locals 3

    .prologue
    .line 383
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 384
    .local v0, "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->resetBottomMargin()V

    goto :goto_0

    .line 386
    .end local v0    # "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method public scrollToChild(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 196
    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 197
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 198
    .local v0, "child":Landroid/view/View;
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->scroll:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaHorizontalScrollView;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaHorizontalScrollView;->scrollToChild(Landroid/view/View;)V

    .line 200
    .end local v0    # "child":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public setActive()V
    .locals 3

    .prologue
    .line 365
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 366
    .local v0, "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onSetActive()V

    goto :goto_0

    .line 368
    .end local v0    # "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method public setClipChildren(Z)V
    .locals 1
    .param p1, "clipChildren"    # Z

    .prologue
    .line 420
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setClipChildren(Z)V

    .line 422
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->linearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setClipChildren(Z)V

    .line 423
    return-void
.end method

.method public setClipToPadding(Z)V
    .locals 1
    .param p1, "clipToPadding"    # Z

    .prologue
    .line 427
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setClipToPadding(Z)V

    .line 429
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->linearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setClipToPadding(Z)V

    .line 430
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->scroll:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaHorizontalScrollView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaHorizontalScrollView;->setClipToPadding(Z)V

    .line 431
    return-void
.end method

.method public setInactive()V
    .locals 3

    .prologue
    .line 371
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 372
    .local v0, "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->onSetInactive()V

    goto :goto_0

    .line 374
    .end local v0    # "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method public setScreenState(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 413
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 414
    .local v0, "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setScreenState(I)V

    goto :goto_0

    .line 416
    .end local v0    # "screen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-void
.end method

.method public setScrollPadding(II)V
    .locals 2
    .param p1, "left"    # I
    .param p2, "right"    # I

    .prologue
    const/4 v1, 0x0

    .line 189
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->scroll:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaHorizontalScrollView;

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->scroll:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaHorizontalScrollView;

    invoke-virtual {v0, p1, v1, p2, v1}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaHorizontalScrollView;->setPadding(IIII)V

    .line 192
    :cond_0
    return-void
.end method

.method public setScrollX(I)V
    .locals 2
    .param p1, "x"    # I

    .prologue
    .line 179
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->scroll:Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaHorizontalScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaHorizontalScrollView;->scrollTo(II)V

    .line 180
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/panorama/PanoramaBody;->panes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
