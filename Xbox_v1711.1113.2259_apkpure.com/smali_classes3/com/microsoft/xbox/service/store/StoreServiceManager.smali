.class public final enum Lcom/microsoft/xbox/service/store/StoreServiceManager;
.super Ljava/lang/Enum;
.source "StoreServiceManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/store/StoreServiceManager$CacheKey;,
        Lcom/microsoft/xbox/service/store/StoreServiceManager$DesktopSystemRequirements;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/store/StoreServiceManager;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/store/StoreServiceManager;

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/store/StoreServiceManager;

.field private static final MAX_CACHE_ITEMS:I = 0x64

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final backCompatItemsModel:Lcom/microsoft/xbox/service/model/BackCompatItemsModel;

.field private final cachedContentRatingsResponse:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final cachedLocalizedGamePropertyResponse:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedGamePropertyResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final dataProvider:Lcom/microsoft/xbox/toolkit/IProjectSpecificDataProvider;

.field private localizedHardwareRequirements:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final storeService:Lcom/microsoft/xbox/service/store/IStoreService;

.field private valueToHardwareFieldName:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private valueToHardwareFieldValue:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 36
    new-instance v0, Lcom/microsoft/xbox/service/store/StoreServiceManager;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/store/StoreServiceManager;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->INSTANCE:Lcom/microsoft/xbox/service/store/StoreServiceManager;

    .line 35
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/store/StoreServiceManager;

    sget-object v1, Lcom/microsoft/xbox/service/store/StoreServiceManager;->INSTANCE:Lcom/microsoft/xbox/service/store/StoreServiceManager;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->$VALUES:[Lcom/microsoft/xbox/service/store/StoreServiceManager;

    .line 38
    const-class v0, Lcom/microsoft/xbox/service/store/StoreServiceManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->cachedContentRatingsResponse:Ljava/util/Map;

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->cachedLocalizedGamePropertyResponse:Ljava/util/Map;

    .line 43
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->valueToHardwareFieldName:Ljava/util/concurrent/ConcurrentHashMap;

    .line 45
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->valueToHardwareFieldValue:Ljava/util/concurrent/ConcurrentHashMap;

    .line 47
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(I)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->localizedHardwareRequirements:Ljava/util/concurrent/ConcurrentHashMap;

    .line 55
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getStoreService()Lcom/microsoft/xbox/service/store/IStoreService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    .line 56
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->dataProvider:Lcom/microsoft/xbox/toolkit/IProjectSpecificDataProvider;

    .line 57
    invoke-static {}, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->getInstance()Lcom/microsoft/xbox/service/model/BackCompatItemsModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->backCompatItemsModel:Lcom/microsoft/xbox/service/model/BackCompatItemsModel;

    .line 58
    return-void
.end method

.method private initializeLocalizedHardwareRequirements(Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedGamePropertyResponse;)V
    .locals 7
    .param p1, "localizedResponse"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedGamePropertyResponse;

    .prologue
    .line 144
    if-eqz p1, :cond_1

    .line 145
    const-string v3, "hardwarefields"

    invoke-virtual {p1, v3}, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedGamePropertyResponse;->getLocalizedValueOfGroup(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 146
    .local v1, "localizedHWFields":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedValue;>;"
    const-string v3, "hardwarefieldvalues"

    invoke-virtual {p1, v3}, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedGamePropertyResponse;->getLocalizedValueOfGroup(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 148
    .local v0, "localizedHWFieldValues":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedValue;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedValue;

    .line 149
    .local v2, "value":Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedValue;
    iget-object v4, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->valueToHardwareFieldName:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v5, v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedValue;->value:Ljava/lang/String;

    iget-object v6, v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedValue;->localized:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 152
    .end local v2    # "value":Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedValue;
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedValue;

    .line 153
    .restart local v2    # "value":Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedValue;
    iget-object v4, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->valueToHardwareFieldValue:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v5, v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedValue;->value:Ljava/lang/String;

    iget-object v6, v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedValue;->localized:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 156
    .end local v0    # "localizedHWFieldValues":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedValue;>;"
    .end local v1    # "localizedHWFields":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedValue;>;"
    .end local v2    # "value":Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedValue;
    :cond_1
    sget-object v3, Lcom/microsoft/xbox/service/store/StoreServiceManager;->TAG:Ljava/lang/String;

    const-string v4, "Localized properties response is null."

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    :cond_2
    return-void
.end method

.method private resetHardwareRequirementsCache()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->valueToHardwareFieldName:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 139
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->valueToHardwareFieldValue:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->localizedHardwareRequirements:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 141
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreServiceManager;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 35
    const-class v0, Lcom/microsoft/xbox/service/store/StoreServiceManager;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreServiceManager;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/store/StoreServiceManager;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->$VALUES:[Lcom/microsoft/xbox/service/store/StoreServiceManager;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/store/StoreServiceManager;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/store/StoreServiceManager;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized getLocalizedGamePropertyResponse()Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedGamePropertyResponse;
    .locals 5
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 97
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 99
    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->dataProvider:Lcom/microsoft/xbox/toolkit/IProjectSpecificDataProvider;

    invoke-interface {v3}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v0

    .line 100
    .local v0, "currentLocale":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->cachedLocalizedGamePropertyResponse:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedGamePropertyResponse;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    .local v2, "response":Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedGamePropertyResponse;
    if-nez v2, :cond_0

    .line 103
    :try_start_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    invoke-interface {v3}, Lcom/microsoft/xbox/service/store/IStoreService;->getLocalizedGameProperty()Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedGamePropertyResponse;

    move-result-object v2

    .line 104
    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->cachedLocalizedGamePropertyResponse:Ljava/util/Map;

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    invoke-direct {p0}, Lcom/microsoft/xbox/service/store/StoreServiceManager;->resetHardwareRequirementsCache()V

    .line 106
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/service/store/StoreServiceManager;->initializeLocalizedHardwareRequirements(Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedGamePropertyResponse;)V
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 112
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v2

    .line 107
    :catch_0
    move-exception v1

    .line 108
    .local v1, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :try_start_2
    sget-object v3, Lcom/microsoft/xbox/service/store/StoreServiceManager;->TAG:Ljava/lang/String;

    const-string v4, "Failed to get localized genre and hardware property strings from service"

    invoke-static {v3, v4, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 97
    .end local v0    # "currentLocale":Ljava/lang/String;
    .end local v1    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    .end local v2    # "response":Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedGamePropertyResponse;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public getLocalizedSystemRequirementValue(Ljava/lang/String;)Landroid/support/v4/util/Pair;
    .locals 5
    .param p1, "value"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v4, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->localizedHardwareRequirements:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/util/Pair;

    .line 120
    .local v1, "requirement":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v1, :cond_0

    .line 121
    iget-object v4, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->valueToHardwareFieldName:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 122
    .local v2, "requirementName":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->valueToHardwareFieldValue:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 124
    .local v3, "requirementValue":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 125
    new-instance v0, Landroid/support/v4/util/Pair;

    invoke-direct {v0, v2, v3}, Landroid/support/v4/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 126
    .local v0, "newRequirement":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->localizedHardwareRequirements:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "requirement":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    check-cast v1, Landroid/support/v4/util/Pair;

    .line 128
    .restart local v1    # "requirement":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    if-nez v1, :cond_0

    .line 129
    move-object v1, v0

    .line 134
    .end local v0    # "newRequirement":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2    # "requirementName":Ljava/lang/String;
    .end local v3    # "requirementValue":Ljava/lang/String;
    :cond_0
    return-object v1
.end method

.method public declared-synchronized getRatings()Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;
    .locals 5
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 78
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 80
    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->dataProvider:Lcom/microsoft/xbox/toolkit/IProjectSpecificDataProvider;

    invoke-interface {v3}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, "currentLocale":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->cachedContentRatingsResponse:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    .local v2, "response":Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;
    if-nez v2, :cond_0

    .line 84
    :try_start_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    invoke-interface {v3}, Lcom/microsoft/xbox/service/store/IStoreService;->getRatings()Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;

    move-result-object v2

    .line 85
    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->cachedContentRatingsResponse:Ljava/util/Map;

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v2

    .line 86
    :catch_0
    move-exception v1

    .line 87
    .local v1, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    :try_start_2
    sget-object v3, Lcom/microsoft/xbox/service/store/StoreServiceManager;->TAG:Ljava/lang/String;

    const-string v4, "Failed to get ratings from service"

    invoke-static {v3, v4, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 78
    .end local v0    # "currentLocale":Ljava/lang/String;
    .end local v1    # "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    .end local v2    # "response":Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public isBackCompatItem(Ljava/lang/String;)Z
    .locals 2
    .param p1, "legacyId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 62
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 64
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const/4 v0, 0x0

    .line 72
    :goto_0
    return v0

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->backCompatItemsModel:Lcom/microsoft/xbox/service/model/BackCompatItemsModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_1

    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->backCompatItemsModel:Lcom/microsoft/xbox/service/model/BackCompatItemsModel;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->loadBackCompatItemsSync(Z)V

    .line 72
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreServiceManager;->backCompatItemsModel:Lcom/microsoft/xbox/service/model/BackCompatItemsModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/BackCompatItemsModel;->isBackCompatItem(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
