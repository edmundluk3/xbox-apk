.class Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameProfileAchievementsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadStatLeaderboardDataTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V
    .locals 0
    .param p2, "stat"    # Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .prologue
    .line 488
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 489
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .line 490
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 2

    .prologue
    .line 494
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    if-eqz v0, :cond_0

    .line 495
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->shouldRefreshLeaderBoard(Ljava/lang/String;)Z

    move-result v0

    .line 498
    :goto_0
    return v0

    .line 497
    :cond_0
    const-string v0, "GameProfileAchievementsScreenViewModel"

    const-string v1, "Invalid stat"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 536
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    .line 537
    .local v0, "titleModel":Lcom/microsoft/xbox/service/model/TitleModel;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    if-eqz v1, :cond_0

    .line 538
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->forceLoad:Z

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/TitleModel;->loadLeaderBoard(ZLcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 539
    const-string v1, "GameProfileAchievementsScreenViewModel"

    const-string v2, "Failed to get stats."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v1
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 484
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 526
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 484
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 3

    .prologue
    .line 504
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 505
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V

    .line 506
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "asyncActionStatus"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 531
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V

    .line 532
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 484
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 510
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 511
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 512
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 513
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$600(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;)Ljava/util/Hashtable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 514
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$600(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;)Ljava/util/Hashtable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 521
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->updateAdapter()V

    .line 522
    return-void

    .line 516
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$600(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;)Ljava/util/Hashtable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadStatLeaderboardDataTask;->stat:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 517
    .local v0, "isLoadingStatLeaderboard":Ljava/lang/Boolean;
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    goto :goto_0
.end method
