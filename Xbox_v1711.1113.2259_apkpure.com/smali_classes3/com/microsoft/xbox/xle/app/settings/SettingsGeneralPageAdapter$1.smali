.class Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$1;
.super Ljava/lang/Object;
.source "SettingsGeneralPageAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;-><init>(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;

    .prologue
    .line 219
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 222
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    if-ltz p3, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->access$000(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->getCount()I

    move-result v0

    if-ge p3, v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->access$100(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    move-result-object v1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;->access$000(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageAdapter;)Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/settings/HomeScreenSpinnerItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/settings/HomeScreenSpinnerItem;->homeScreenPreference()Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->setHomeScreenPreference(Lcom/microsoft/xbox/data/repository/homescreen/HomeScreenPreference;)V

    .line 225
    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 230
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
