.class public Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;
.super Landroid/widget/ListView;
.source "VirtualGrid.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "HeaderAwareListView"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;Landroid/content/Context;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 827
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    .line 828
    invoke-direct {p0, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 829
    return-void
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 834
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->withinHeader(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 835
    const/4 v0, 0x1

    .line 837
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 843
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid$HeaderAwareListView;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/VirtualGrid;->withinHeader(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 844
    const/4 v0, 0x0

    .line 846
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method
