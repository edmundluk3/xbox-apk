.class public final Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes;
.super Ljava/lang/Object;
.source "ClubProfileDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$PreferredColor;,
        Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateContract;,
        Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequestSerializer;,
        Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileUpdateRequest;,
        Lcom/microsoft/xbox/service/clubs/ClubProfileDataTypes$ClubProfileRole;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Type shouldn\'t be instantiated."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
