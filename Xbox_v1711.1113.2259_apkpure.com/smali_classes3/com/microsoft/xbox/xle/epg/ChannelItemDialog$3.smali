.class Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$3;
.super Ljava/lang/Object;
.source "ChannelItemDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;

.field final synthetic val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$3;->this$0:Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$3;->val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 119
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/SessionModel;->getSessionState()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 120
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$3;->val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$3;->val$channel:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getCurrentProgram()Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/epg/LiveTvUtils;->tuneToProgram(Lcom/microsoft/xbox/service/model/epg/EPGChannel;Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)Z

    move-result v0

    .line 121
    .local v0, "success":Z
    if-nez v0, :cond_0

    .line 122
    const v1, 0x7f070455

    invoke-static {v3, v1, v3}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;->show(IIZ)V

    .line 124
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog$3;->this$0:Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/ChannelItemDialog;->safeDismiss()V

    .line 128
    .end local v0    # "success":Z
    :goto_0
    return-void

    .line 126
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showConnectDialog()V

    goto :goto_0
.end method
