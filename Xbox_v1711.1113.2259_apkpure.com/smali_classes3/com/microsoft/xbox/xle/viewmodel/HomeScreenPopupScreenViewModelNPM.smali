.class public Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM;
.super Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;
.source "HomeScreenPopupScreenViewModelNPM.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent",
        "<",
        "Lcom/microsoft/xbox/service/model/pins/LaunchableItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final headerData:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/LaunchableItem;",
            ">;"
        }
    .end annotation
.end field

.field private final npm:Lcom/microsoft/xbox/xle/model/NowPlayingModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;
    .param p2, "npm"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .prologue
    .line 32
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelContent;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;[Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$Command;)V

    .line 14
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM;->headerData:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;

    .line 33
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM;->npm:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .line 34
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM;)Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM;->npm:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    return-object v0
.end method


# virtual methods
.method public createScreenAdapter()Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/LaunchableItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;->createContentItemScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase;)Lcom/microsoft/xbox/xle/app/adapter/HomeScreenPopupScreenAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getHeaderData()Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData",
            "<",
            "Lcom/microsoft/xbox/service/model/pins/LaunchableItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM;->headerData:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelBase$HeaderData;

    return-object v0
.end method

.method protected getLaunchableItem()Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM;->npm:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    return-object v0
.end method

.method protected getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM;->npm:Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    return-object v0
.end method

.method protected getMediaItemForDetails()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPopupScreenViewModelNPM;->getMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    return-object v0
.end method
