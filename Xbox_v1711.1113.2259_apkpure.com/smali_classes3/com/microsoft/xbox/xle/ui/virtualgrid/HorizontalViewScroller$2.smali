.class Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$2;
.super Ljava/lang/Object;
.source "HorizontalViewScroller.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    .prologue
    .line 828
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$2;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 833
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$2;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->access$400(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;)I

    move-result v1

    if-nez v1, :cond_0

    .line 843
    :goto_0
    return-void

    .line 836
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$2;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getContentOffset()I

    move-result v0

    .line 837
    .local v0, "offset":I
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$2;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$2;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->access$400(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;)I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->setContentOffset(I)V

    .line 838
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$2;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->getContentOffset()I

    move-result v1

    if-ne v1, v0, :cond_1

    .line 839
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$2;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$2;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->access$400(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;)I

    move-result v2

    neg-int v2, v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->access$402(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;I)I

    .line 841
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$2;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$2;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->access$500(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->access$600(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 842
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$2;->this$0:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;

    sget-object v2, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;->FLING:Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;->access$300(Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller;Lcom/microsoft/xbox/xle/ui/virtualgrid/HorizontalViewScroller$ScrollState;)V

    goto :goto_0
.end method
