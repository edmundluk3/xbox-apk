.class final Lcom/microsoft/xbox/presentation/party/AutoValue_PartyListItems_PartyMemberListItem;
.super Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;
.source "AutoValue_PartyListItems_PartyMemberListItem.java"


# instance fields
.field private final canBeKicked:Z

.field private final partyMember:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

.field private final userSummary:Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;Z)V
    .locals 2
    .param p1, "partyMember"    # Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    .param p2, "userSummary"    # Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "canBeKicked"    # Z

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;-><init>()V

    .line 21
    if-nez p1, :cond_0

    .line 22
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null partyMember"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 24
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyListItems_PartyMemberListItem;->partyMember:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    .line 25
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyListItems_PartyMemberListItem;->userSummary:Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    .line 26
    iput-boolean p3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyListItems_PartyMemberListItem;->canBeKicked:Z

    .line 27
    return-void
.end method


# virtual methods
.method public canBeKicked()Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyListItems_PartyMemberListItem;->canBeKicked:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 57
    if-ne p1, p0, :cond_1

    .line 66
    :cond_0
    :goto_0
    return v1

    .line 60
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;

    if-eqz v3, :cond_4

    move-object v0, p1

    .line 61
    check-cast v0, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;

    .line 62
    .local v0, "that":Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyListItems_PartyMemberListItem;->partyMember:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;->partyMember()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyListItems_PartyMemberListItem;->userSummary:Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    if-nez v3, :cond_3

    .line 63
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;->userSummary()Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-boolean v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyListItems_PartyMemberListItem;->canBeKicked:Z

    .line 64
    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;->canBeKicked()Z

    move-result v4

    if-eq v3, v4, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 63
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyListItems_PartyMemberListItem;->userSummary:Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    invoke-virtual {v0}, Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;->userSummary()Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .end local v0    # "that":Lcom/microsoft/xbox/presentation/party/PartyListItems$PartyMemberListItem;
    :cond_4
    move v1, v2

    .line 66
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 71
    const/4 v0, 0x1

    .line 72
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 73
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyListItems_PartyMemberListItem;->partyMember:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 74
    mul-int/2addr v0, v2

    .line 75
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyListItems_PartyMemberListItem;->userSummary:Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    xor-int/2addr v0, v1

    .line 76
    mul-int/2addr v0, v2

    .line 77
    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyListItems_PartyMemberListItem;->canBeKicked:Z

    if-eqz v1, :cond_1

    const/16 v1, 0x4cf

    :goto_1
    xor-int/2addr v0, v1

    .line 78
    return v0

    .line 75
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyListItems_PartyMemberListItem;->userSummary:Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    .line 77
    :cond_1
    const/16 v1, 0x4d5

    goto :goto_1
.end method

.method public partyMember()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyListItems_PartyMemberListItem;->partyMember:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PartyMemberListItem{partyMember="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyListItems_PartyMemberListItem;->partyMember:Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", userSummary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyListItems_PartyMemberListItem;->userSummary:Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", canBeKicked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyListItems_PartyMemberListItem;->canBeKicked:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public userSummary()Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/presentation/party/AutoValue_PartyListItems_PartyMemberListItem;->userSummary:Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    return-object v0
.end method
