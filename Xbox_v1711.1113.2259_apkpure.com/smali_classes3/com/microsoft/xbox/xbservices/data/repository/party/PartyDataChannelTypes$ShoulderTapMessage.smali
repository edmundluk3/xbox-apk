.class public abstract Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ShoulderTapMessage;
.super Ljava/lang/Object;
.source "PartyDataChannelTypes.java"

# interfaces
.implements Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$PartyDataChannelMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ShoulderTapMessage"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ShoulderTapMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/AutoValue_PartyDataChannelTypes_ShoulderTapMessage$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/AutoValue_PartyDataChannelTypes_ShoulderTapMessage$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(I)Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ShoulderTapMessage;
    .locals 2
    .param p0, "changeNumber"    # I

    .prologue
    .line 86
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/AutoValue_PartyDataChannelTypes_ShoulderTapMessage;

    const-string v1, "mpsdShoulderTap"

    invoke-direct {v0, v1, p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/AutoValue_PartyDataChannelTypes_ShoulderTapMessage;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method


# virtual methods
.method public abstract changeNumber()I
.end method

.method public abstract type()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
