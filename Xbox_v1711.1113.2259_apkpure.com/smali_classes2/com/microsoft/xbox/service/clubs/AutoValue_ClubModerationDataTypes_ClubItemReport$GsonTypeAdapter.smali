.class public final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubItemReport$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_ClubModerationDataTypes_ClubItemReport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubItemReport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;",
        ">;"
    }
.end annotation


# instance fields
.field private defaultReportingXuid:J

.field private defaultTextReason:Ljava/lang/String;

.field private final reportingXuidAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final textReasonAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 2
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 21
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubItemReport$GsonTypeAdapter;->defaultReportingXuid:J

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubItemReport$GsonTypeAdapter;->defaultTextReason:Ljava/lang/String;

    .line 24
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubItemReport$GsonTypeAdapter;->reportingXuidAdapter:Lcom/google/gson/TypeAdapter;

    .line 25
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubItemReport$GsonTypeAdapter;->textReasonAdapter:Lcom/google/gson/TypeAdapter;

    .line 26
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;
    .locals 6
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v4

    sget-object v5, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v4, v5, :cond_0

    .line 52
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 53
    const/4 v4, 0x0

    .line 79
    :goto_0
    return-object v4

    .line 55
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 56
    iget-wide v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubItemReport$GsonTypeAdapter;->defaultReportingXuid:J

    .line 57
    .local v2, "reportingXuid":J
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubItemReport$GsonTypeAdapter;->defaultTextReason:Ljava/lang/String;

    .line 58
    .local v1, "textReason":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 59
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 60
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v4

    sget-object v5, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v4, v5, :cond_1

    .line 61
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 64
    :cond_1
    const/4 v4, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v4, :pswitch_data_0

    .line 74
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 64
    :sswitch_0
    const-string v5, "reportingXuid"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x0

    goto :goto_2

    :sswitch_1
    const-string v5, "textReason"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x1

    goto :goto_2

    .line 66
    :pswitch_0
    iget-object v4, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubItemReport$GsonTypeAdapter;->reportingXuidAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v4, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 67
    goto :goto_1

    .line 70
    :pswitch_1
    iget-object v4, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubItemReport$GsonTypeAdapter;->textReasonAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v4, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "textReason":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 71
    .restart local v1    # "textReason":Ljava/lang/String;
    goto :goto_1

    .line 78
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 79
    new-instance v4, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubItemReport;

    invoke-direct {v4, v2, v3, v1}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubItemReport;-><init>(JLjava/lang/String;)V

    goto :goto_0

    .line 64
    :sswitch_data_0
    .sparse-switch
        -0x24303bda -> :sswitch_0
        0x6bd16dd1 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubItemReport$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultReportingXuid(J)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubItemReport$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultReportingXuid"    # J

    .prologue
    .line 28
    iput-wide p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubItemReport$GsonTypeAdapter;->defaultReportingXuid:J

    .line 29
    return-object p0
.end method

.method public setDefaultTextReason(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubItemReport$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTextReason"    # Ljava/lang/String;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubItemReport$GsonTypeAdapter;->defaultTextReason:Ljava/lang/String;

    .line 33
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;)V
    .locals 4
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    if-nez p2, :cond_0

    .line 39
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 48
    :goto_0
    return-void

    .line 42
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 43
    const-string v0, "reportingXuid"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubItemReport$GsonTypeAdapter;->reportingXuidAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;->reportingXuid()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 45
    const-string v0, "textReason"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubItemReport$GsonTypeAdapter;->textReasonAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;->textReason()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 47
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18
    check-cast p2, Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubModerationDataTypes_ClubItemReport$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubModerationDataTypes$ClubItemReport;)V

    return-void
.end method
