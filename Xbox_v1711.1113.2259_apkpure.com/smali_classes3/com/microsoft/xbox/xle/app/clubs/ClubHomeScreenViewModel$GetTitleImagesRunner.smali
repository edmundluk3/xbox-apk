.class Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$GetTitleImagesRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "ClubHomeScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetTitleImagesRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

.field private final titleHubService:Lcom/microsoft/xbox/service/titleHub/ITitleHubService;

.field private final titleIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 927
    .local p2, "titleIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Long;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$GetTitleImagesRunner;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 923
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getTitleHubService()Lcom/microsoft/xbox/service/titleHub/ITitleHubService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$GetTitleImagesRunner;->titleHubService:Lcom/microsoft/xbox/service/titleHub/ITitleHubService;

    .line 928
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$GetTitleImagesRunner;->titleIds:Ljava/util/Set;

    .line 929
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;Ljava/util/Collection;Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;
    .param p2, "x1"    # Ljava/util/Collection;
    .param p3, "x2"    # Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$1;

    .prologue
    .line 922
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$GetTitleImagesRunner;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;Ljava/util/Collection;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 922
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$GetTitleImagesRunner;->buildData()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 938
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$GetTitleImagesRunner;->titleHubService:Lcom/microsoft/xbox/service/titleHub/ITitleHubService;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$GetTitleImagesRunner;->titleIds:Ljava/util/Set;

    invoke-interface {v0, v1}, Lcom/microsoft/xbox/service/titleHub/ITitleHubService;->getTitleSummaries(Ljava/util/Set;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 948
    const-wide/16 v0, 0xbd6

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 943
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$TitleData;>;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel$GetTitleImagesRunner;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;->access$500(Lcom/microsoft/xbox/xle/app/clubs/ClubHomeScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 944
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 934
    return-void
.end method
