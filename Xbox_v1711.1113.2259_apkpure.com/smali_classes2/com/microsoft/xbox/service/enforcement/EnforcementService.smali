.class public final enum Lcom/microsoft/xbox/service/enforcement/EnforcementService;
.super Ljava/lang/Enum;
.source "EnforcementService.java"

# interfaces
.implements Lcom/microsoft/xbox/service/enforcement/IEnforcementService;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/enforcement/EnforcementService;",
        ">;",
        "Lcom/microsoft/xbox/service/enforcement/IEnforcementService;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/enforcement/EnforcementService;

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/enforcement/EnforcementService;

.field public static final REPORT_USER_CONTRACT_VERSION:Ljava/lang/String; = "101"

.field private static final SEND_REPORT_ENDPOINT:Ljava/lang/String; = "https://reputation.xboxlive.com/users/xuid(%1$s)/feedback"

.field private static final STATIC_HEADERS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 21
    new-instance v0, Lcom/microsoft/xbox/service/enforcement/EnforcementService;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/enforcement/EnforcementService;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/enforcement/EnforcementService;->INSTANCE:Lcom/microsoft/xbox/service/enforcement/EnforcementService;

    .line 20
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/enforcement/EnforcementService;

    sget-object v1, Lcom/microsoft/xbox/service/enforcement/EnforcementService;->INSTANCE:Lcom/microsoft/xbox/service/enforcement/EnforcementService;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/enforcement/EnforcementService;->$VALUES:[Lcom/microsoft/xbox/service/enforcement/EnforcementService;

    .line 23
    const-class v0, Lcom/microsoft/xbox/service/enforcement/EnforcementService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/enforcement/EnforcementService;->TAG:Ljava/lang/String;

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/enforcement/EnforcementService;->STATIC_HEADERS:Ljava/util/List;

    .line 32
    sget-object v0, Lcom/microsoft/xbox/service/enforcement/EnforcementService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Content-type"

    const-string v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    sget-object v0, Lcom/microsoft/xbox/service/enforcement/EnforcementService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string/jumbo v2, "x-xbl-contract-version"

    const-string v3, "101"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    sget-object v0, Lcom/microsoft/xbox/service/enforcement/EnforcementService;->STATIC_HEADERS:Ljava/util/List;

    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Accept"

    const-string v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 35
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic lambda$sendReport$0(Ljava/lang/String;Lcom/microsoft/xbox/service/model/sls/ReportUserData;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    .locals 2
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "reportUserData"    # Lcom/microsoft/xbox/service/model/sls/ReportUserData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 48
    sget-object v0, Lcom/microsoft/xbox/service/enforcement/EnforcementService;->STATIC_HEADERS:Ljava/util/List;

    .line 51
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/sls/ReportUserData;->toString()Ljava/lang/String;

    move-result-object v1

    .line 48
    invoke-static {p0, v0, v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->postStringWithStatus(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/enforcement/EnforcementService;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/microsoft/xbox/service/enforcement/EnforcementService;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/enforcement/EnforcementService;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/enforcement/EnforcementService;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/microsoft/xbox/service/enforcement/EnforcementService;->$VALUES:[Lcom/microsoft/xbox/service/enforcement/EnforcementService;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/enforcement/EnforcementService;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/enforcement/EnforcementService;

    return-object v0
.end method


# virtual methods
.method public sendReport(Lcom/microsoft/xbox/service/model/sls/ReportUserData;Ljava/lang/String;)Z
    .locals 5
    .param p1, "reportUserData"    # Lcom/microsoft/xbox/service/model/sls/ReportUserData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "reportTargetId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 39
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 40
    iget-object v1, p1, Lcom/microsoft/xbox/service/model/sls/ReportUserData;->feedbackContext:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 41
    iget-object v1, p1, Lcom/microsoft/xbox/service/model/sls/ReportUserData;->feedbackType:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 42
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 44
    sget-object v1, Lcom/microsoft/xbox/service/enforcement/EnforcementService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendReport - ReportTargetId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ReportUserData : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/sls/ReportUserData;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "https://reputation.xboxlive.com/users/xuid(%1$s)/feedback"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "url":Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/microsoft/xbox/service/enforcement/EnforcementService$$Lambda$1;->lambdaFactory$(Ljava/lang/String;Lcom/microsoft/xbox/service/model/sls/ReportUserData;)Ljava/util/concurrent/Callable;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->requestAccepting2xxs(Ljava/util/concurrent/Callable;)Z

    move-result v1

    return v1
.end method
