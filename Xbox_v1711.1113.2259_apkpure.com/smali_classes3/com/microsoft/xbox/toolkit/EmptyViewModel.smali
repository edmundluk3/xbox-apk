.class public final Lcom/microsoft/xbox/toolkit/EmptyViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "EmptyViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/EmptyViewModel$SingletonHolder;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 21
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/toolkit/EmptyViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/toolkit/EmptyViewModel$1;

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/EmptyViewModel;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/toolkit/EmptyViewModel;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/microsoft/xbox/toolkit/EmptyViewModel$SingletonHolder;->INSTANCE:Lcom/microsoft/xbox/toolkit/EmptyViewModel;

    return-object v0
.end method


# virtual methods
.method public isBusy()Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 42
    return-void
.end method

.method public onRehydrate()V
    .locals 0

    .prologue
    .line 29
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 25
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 33
    return-void
.end method
