.class final Lcom/microsoft/xbox/xle/app/glide/AutoValue_ResizeUrlImageLoader_DimenPoint;
.super Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;
.source "AutoValue_ResizeUrlImageLoader_DimenPoint.java"


# instance fields
.field private final area:I

.field private final aspectRatio:D

.field private final height:I

.field private final width:I


# direct methods
.method constructor <init>(IIID)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "area"    # I
    .param p4, "aspectRatio"    # D

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;-><init>()V

    .line 19
    iput p1, p0, Lcom/microsoft/xbox/xle/app/glide/AutoValue_ResizeUrlImageLoader_DimenPoint;->width:I

    .line 20
    iput p2, p0, Lcom/microsoft/xbox/xle/app/glide/AutoValue_ResizeUrlImageLoader_DimenPoint;->height:I

    .line 21
    iput p3, p0, Lcom/microsoft/xbox/xle/app/glide/AutoValue_ResizeUrlImageLoader_DimenPoint;->area:I

    .line 22
    iput-wide p4, p0, Lcom/microsoft/xbox/xle/app/glide/AutoValue_ResizeUrlImageLoader_DimenPoint;->aspectRatio:D

    .line 23
    return-void
.end method


# virtual methods
.method public area()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/microsoft/xbox/xle/app/glide/AutoValue_ResizeUrlImageLoader_DimenPoint;->area:I

    return v0
.end method

.method public aspectRatio()D
    .locals 2

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/app/glide/AutoValue_ResizeUrlImageLoader_DimenPoint;->aspectRatio:D

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 57
    if-ne p1, p0, :cond_1

    .line 67
    :cond_0
    :goto_0
    return v1

    .line 60
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 61
    check-cast v0, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;

    .line 62
    .local v0, "that":Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;
    iget v3, p0, Lcom/microsoft/xbox/xle/app/glide/AutoValue_ResizeUrlImageLoader_DimenPoint;->width:I

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->width()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget v3, p0, Lcom/microsoft/xbox/xle/app/glide/AutoValue_ResizeUrlImageLoader_DimenPoint;->height:I

    .line 63
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->height()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget v3, p0, Lcom/microsoft/xbox/xle/app/glide/AutoValue_ResizeUrlImageLoader_DimenPoint;->area:I

    .line 64
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->area()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/app/glide/AutoValue_ResizeUrlImageLoader_DimenPoint;->aspectRatio:D

    .line 65
    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;->aspectRatio()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xle/app/glide/ResizeUrlImageLoader$DimenPoint;
    :cond_3
    move v1, v2

    .line 67
    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const v2, 0xf4243

    .line 72
    const/4 v0, 0x1

    .line 73
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 74
    iget v1, p0, Lcom/microsoft/xbox/xle/app/glide/AutoValue_ResizeUrlImageLoader_DimenPoint;->width:I

    xor-int/2addr v0, v1

    .line 75
    mul-int/2addr v0, v2

    .line 76
    iget v1, p0, Lcom/microsoft/xbox/xle/app/glide/AutoValue_ResizeUrlImageLoader_DimenPoint;->height:I

    xor-int/2addr v0, v1

    .line 77
    mul-int/2addr v0, v2

    .line 78
    iget v1, p0, Lcom/microsoft/xbox/xle/app/glide/AutoValue_ResizeUrlImageLoader_DimenPoint;->area:I

    xor-int/2addr v0, v1

    .line 79
    mul-int/2addr v0, v2

    .line 80
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/xle/app/glide/AutoValue_ResizeUrlImageLoader_DimenPoint;->aspectRatio:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    iget-wide v6, p0, Lcom/microsoft/xbox/xle/app/glide/AutoValue_ResizeUrlImageLoader_DimenPoint;->aspectRatio:D

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 81
    return v0
.end method

.method public height()I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/microsoft/xbox/xle/app/glide/AutoValue_ResizeUrlImageLoader_DimenPoint;->height:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DimenPoint{width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xle/app/glide/AutoValue_ResizeUrlImageLoader_DimenPoint;->width:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xle/app/glide/AutoValue_ResizeUrlImageLoader_DimenPoint;->height:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", area="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xle/app/glide/AutoValue_ResizeUrlImageLoader_DimenPoint;->area:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", aspectRatio="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/app/glide/AutoValue_ResizeUrlImageLoader_DimenPoint;->aspectRatio:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public width()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/microsoft/xbox/xle/app/glide/AutoValue_ResizeUrlImageLoader_DimenPoint;->width:I

    return v0
.end method
