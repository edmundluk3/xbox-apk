.class Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenAdapter$SongAdapter;
.super Landroid/widget/ArrayAdapter;
.source "AlbumDetailsScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SongAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;",
        ">;"
    }
.end annotation


# instance fields
.field final inflater:Landroid/view/LayoutInflater;

.field final resourceId:I

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenAdapter;Landroid/content/Context;ILjava/util/List;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "resourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 178
    .local p4, "objects":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenAdapter$SongAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenAdapter;

    .line 179
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 180
    iput p3, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenAdapter$SongAdapter;->resourceId:I

    .line 181
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenAdapter$SongAdapter;->inflater:Landroid/view/LayoutInflater;

    .line 182
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 186
    move-object v0, p2

    .line 187
    .local v0, "view":Landroid/view/View;
    if-nez v0, :cond_0

    .line 188
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenAdapter$SongAdapter;->inflater:Landroid/view/LayoutInflater;

    iget v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenAdapter$SongAdapter;->resourceId:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 190
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenAdapter$SongAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenAdapter$SongAdapter;->this$0:Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenAdapter;->access$100(Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenAdapter;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    invoke-static {v2, v0, v1, p1}, Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenAdapter;->access$200(Lcom/microsoft/xbox/xle/app/adapter/AlbumDetailsScreenAdapter;Landroid/view/View;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;I)V

    .line 191
    return-object v0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 196
    const/4 v0, 0x0

    return v0
.end method
