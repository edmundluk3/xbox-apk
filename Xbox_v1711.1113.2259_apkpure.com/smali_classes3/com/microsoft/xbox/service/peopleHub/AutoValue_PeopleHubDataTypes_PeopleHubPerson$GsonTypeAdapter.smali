.class public final Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_PeopleHubDataTypes_PeopleHubPerson.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;",
        ">;"
    }
.end annotation


# instance fields
.field private final addedDateTimeUtcAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field private defaultAddedDateTimeUtc:Ljava/util/Date;

.field private defaultDisplayName:Ljava/lang/String;

.field private defaultDisplayPicRaw:Ljava/lang/String;

.field private defaultGamerScore:J

.field private defaultGamertag:Ljava/lang/String;

.field private defaultIsBroadcasting:Z

.field private defaultIsFavorite:Z

.field private defaultIsFollowedByCaller:Z

.field private defaultIsFollowingCaller:Z

.field private defaultIsIdentityShared:Z

.field private defaultMultiplayerSummary:Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;

.field private defaultPreferredColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

.field private defaultPresenceState:Lcom/microsoft/xbox/service/model/UserStatus;

.field private defaultPresenceText:Ljava/lang/String;

.field private defaultRealName:Ljava/lang/String;

.field private defaultXuid:J

.field private final displayNameAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final displayPicRawAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final gamerScoreAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final gamertagAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final isBroadcastingAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final isFavoriteAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final isFollowedByCallerAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final isFollowingCallerAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final isIdentitySharedAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final multiplayerSummaryAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;",
            ">;"
        }
    .end annotation
.end field

.field private final preferredColorAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;",
            ">;"
        }
    .end annotation
.end field

.field private final presenceStateAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/model/UserStatus;",
            ">;"
        }
    .end annotation
.end field

.field private final presenceTextAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final realNameAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final xuidAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 4
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 60
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 44
    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultAddedDateTimeUtc:Ljava/util/Date;

    .line 45
    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultDisplayName:Ljava/lang/String;

    .line 46
    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultDisplayPicRaw:Ljava/lang/String;

    .line 47
    iput-wide v2, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultGamerScore:J

    .line 48
    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultGamertag:Ljava/lang/String;

    .line 49
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultIsBroadcasting:Z

    .line 50
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultIsFavorite:Z

    .line 51
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultIsFollowedByCaller:Z

    .line 52
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultIsFollowingCaller:Z

    .line 53
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultIsIdentityShared:Z

    .line 54
    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultMultiplayerSummary:Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;

    .line 55
    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultPreferredColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .line 56
    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultPresenceState:Lcom/microsoft/xbox/service/model/UserStatus;

    .line 57
    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultPresenceText:Ljava/lang/String;

    .line 58
    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultRealName:Ljava/lang/String;

    .line 59
    iput-wide v2, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultXuid:J

    .line 61
    const-class v0, Ljava/util/Date;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->addedDateTimeUtcAdapter:Lcom/google/gson/TypeAdapter;

    .line 62
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->displayNameAdapter:Lcom/google/gson/TypeAdapter;

    .line 63
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->displayPicRawAdapter:Lcom/google/gson/TypeAdapter;

    .line 64
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->gamerScoreAdapter:Lcom/google/gson/TypeAdapter;

    .line 65
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->gamertagAdapter:Lcom/google/gson/TypeAdapter;

    .line 66
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->isBroadcastingAdapter:Lcom/google/gson/TypeAdapter;

    .line 67
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->isFavoriteAdapter:Lcom/google/gson/TypeAdapter;

    .line 68
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->isFollowedByCallerAdapter:Lcom/google/gson/TypeAdapter;

    .line 69
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->isFollowingCallerAdapter:Lcom/google/gson/TypeAdapter;

    .line 70
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->isIdentitySharedAdapter:Lcom/google/gson/TypeAdapter;

    .line 71
    const-class v0, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->multiplayerSummaryAdapter:Lcom/google/gson/TypeAdapter;

    .line 72
    const-class v0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->preferredColorAdapter:Lcom/google/gson/TypeAdapter;

    .line 73
    const-class v0, Lcom/microsoft/xbox/service/model/UserStatus;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->presenceStateAdapter:Lcom/google/gson/TypeAdapter;

    .line 74
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->presenceTextAdapter:Lcom/google/gson/TypeAdapter;

    .line 75
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->realNameAdapter:Lcom/google/gson/TypeAdapter;

    .line 76
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->xuidAdapter:Lcom/google/gson/TypeAdapter;

    .line 77
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;
    .locals 23
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 186
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v2

    sget-object v22, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    move-object/from16 v0, v22

    if-ne v2, v0, :cond_0

    .line 187
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 188
    const/4 v2, 0x0

    .line 284
    :goto_0
    return-object v2

    .line 190
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 191
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultAddedDateTimeUtc:Ljava/util/Date;

    .line 192
    .local v3, "addedDateTimeUtc":Ljava/util/Date;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultDisplayName:Ljava/lang/String;

    .line 193
    .local v4, "displayName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultDisplayPicRaw:Ljava/lang/String;

    .line 194
    .local v5, "displayPicRaw":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultGamerScore:J

    .line 195
    .local v6, "gamerScore":J
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultGamertag:Ljava/lang/String;

    .line 196
    .local v8, "gamertag":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultIsBroadcasting:Z

    .line 197
    .local v9, "isBroadcasting":Z
    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultIsFavorite:Z

    .line 198
    .local v10, "isFavorite":Z
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultIsFollowedByCaller:Z

    .line 199
    .local v11, "isFollowedByCaller":Z
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultIsFollowingCaller:Z

    .line 200
    .local v12, "isFollowingCaller":Z
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultIsIdentityShared:Z

    .line 201
    .local v13, "isIdentityShared":Z
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultMultiplayerSummary:Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;

    .line 202
    .local v14, "multiplayerSummary":Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultPreferredColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .line 203
    .local v15, "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultPresenceState:Lcom/microsoft/xbox/service/model/UserStatus;

    move-object/from16 v16, v0

    .line 204
    .local v16, "presenceState":Lcom/microsoft/xbox/service/model/UserStatus;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultPresenceText:Ljava/lang/String;

    move-object/from16 v17, v0

    .line 205
    .local v17, "presenceText":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultRealName:Ljava/lang/String;

    move-object/from16 v18, v0

    .line 206
    .local v18, "realName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultXuid:J

    move-wide/from16 v19, v0

    .line 207
    .local v19, "xuid":J
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 208
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v21

    .line 209
    .local v21, "_name":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v2

    sget-object v22, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    move-object/from16 v0, v22

    if-ne v2, v0, :cond_1

    .line 210
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 213
    :cond_1
    const/4 v2, -0x1

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->hashCode()I

    move-result v22

    sparse-switch v22, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v2, :pswitch_data_0

    .line 279
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 213
    :sswitch_0
    const-string v22, "addedDateTimeUtc"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    const/4 v2, 0x0

    goto :goto_2

    :sswitch_1
    const-string v22, "displayName"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    const/4 v2, 0x1

    goto :goto_2

    :sswitch_2
    const-string v22, "displayPicRaw"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    const/4 v2, 0x2

    goto :goto_2

    :sswitch_3
    const-string v22, "gamerScore"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    const/4 v2, 0x3

    goto :goto_2

    :sswitch_4
    const-string v22, "gamertag"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    const/4 v2, 0x4

    goto :goto_2

    :sswitch_5
    const-string v22, "isBroadcasting"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    const/4 v2, 0x5

    goto :goto_2

    :sswitch_6
    const-string v22, "isFavorite"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    const/4 v2, 0x6

    goto :goto_2

    :sswitch_7
    const-string v22, "isFollowedByCaller"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    const/4 v2, 0x7

    goto :goto_2

    :sswitch_8
    const-string v22, "isFollowingCaller"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    const/16 v2, 0x8

    goto :goto_2

    :sswitch_9
    const-string v22, "isIdentityShared"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    const/16 v2, 0x9

    goto :goto_2

    :sswitch_a
    const-string v22, "multiplayerSummary"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    const/16 v2, 0xa

    goto :goto_2

    :sswitch_b
    const-string v22, "preferredColor"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    const/16 v2, 0xb

    goto/16 :goto_2

    :sswitch_c
    const-string v22, "presenceState"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    const/16 v2, 0xc

    goto/16 :goto_2

    :sswitch_d
    const-string v22, "presenceText"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    const/16 v2, 0xd

    goto/16 :goto_2

    :sswitch_e
    const-string v22, "realName"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    const/16 v2, 0xe

    goto/16 :goto_2

    :sswitch_f
    const-string v22, "xuid"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    const/16 v2, 0xf

    goto/16 :goto_2

    .line 215
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->addedDateTimeUtcAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "addedDateTimeUtc":Ljava/util/Date;
    check-cast v3, Ljava/util/Date;

    .line 216
    .restart local v3    # "addedDateTimeUtc":Ljava/util/Date;
    goto/16 :goto_1

    .line 219
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->displayNameAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "displayName":Ljava/lang/String;
    check-cast v4, Ljava/lang/String;

    .line 220
    .restart local v4    # "displayName":Ljava/lang/String;
    goto/16 :goto_1

    .line 223
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->displayPicRawAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "displayPicRaw":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 224
    .restart local v5    # "displayPicRaw":Ljava/lang/String;
    goto/16 :goto_1

    .line 227
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->gamerScoreAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 228
    goto/16 :goto_1

    .line 231
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->gamertagAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "gamertag":Ljava/lang/String;
    check-cast v8, Ljava/lang/String;

    .line 232
    .restart local v8    # "gamertag":Ljava/lang/String;
    goto/16 :goto_1

    .line 235
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->isBroadcastingAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    .line 236
    goto/16 :goto_1

    .line 239
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->isFavoriteAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    .line 240
    goto/16 :goto_1

    .line 243
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->isFollowedByCallerAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    .line 244
    goto/16 :goto_1

    .line 247
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->isFollowingCallerAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    .line 248
    goto/16 :goto_1

    .line 251
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->isIdentitySharedAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    .line 252
    goto/16 :goto_1

    .line 255
    :pswitch_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->multiplayerSummaryAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v14

    .end local v14    # "multiplayerSummary":Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;
    check-cast v14, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;

    .line 256
    .restart local v14    # "multiplayerSummary":Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;
    goto/16 :goto_1

    .line 259
    :pswitch_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->preferredColorAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v15

    .end local v15    # "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    check-cast v15, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .line 260
    .restart local v15    # "preferredColor":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    goto/16 :goto_1

    .line 263
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->presenceStateAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "presenceState":Lcom/microsoft/xbox/service/model/UserStatus;
    check-cast v16, Lcom/microsoft/xbox/service/model/UserStatus;

    .line 264
    .restart local v16    # "presenceState":Lcom/microsoft/xbox/service/model/UserStatus;
    goto/16 :goto_1

    .line 267
    :pswitch_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->presenceTextAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v17

    .end local v17    # "presenceText":Ljava/lang/String;
    check-cast v17, Ljava/lang/String;

    .line 268
    .restart local v17    # "presenceText":Ljava/lang/String;
    goto/16 :goto_1

    .line 271
    :pswitch_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->realNameAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v18

    .end local v18    # "realName":Ljava/lang/String;
    check-cast v18, Ljava/lang/String;

    .line 272
    .restart local v18    # "realName":Ljava/lang/String;
    goto/16 :goto_1

    .line 275
    :pswitch_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->xuidAdapter:Lcom/google/gson/TypeAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v19

    .line 276
    goto/16 :goto_1

    .line 283
    .end local v21    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 284
    new-instance v2, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson;

    invoke-direct/range {v2 .. v20}, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson;-><init>(Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ZZZZZLcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;Lcom/microsoft/xbox/service/model/UserStatus;Ljava/lang/String;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 213
    :sswitch_data_0
    .sparse-switch
        -0x7e354bc4 -> :sswitch_7
        -0x766de3aa -> :sswitch_c
        -0x719a6a60 -> :sswitch_2
        -0x69656b26 -> :sswitch_4
        -0x6552249e -> :sswitch_b
        -0x5f167573 -> :sswitch_9
        -0x3347b6b7 -> :sswitch_e
        -0x1eb2a235 -> :sswitch_5
        0x3850d8 -> :sswitch_f
        0xcb260c8 -> :sswitch_d
        0xf5970e6 -> :sswitch_6
        0x1a34c6f2 -> :sswitch_8
        0x44075ae9 -> :sswitch_0
        0x441504ec -> :sswitch_a
        0x5878d3b2 -> :sswitch_3
        0x662bd66d -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultAddedDateTimeUtc(Ljava/util/Date;)Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultAddedDateTimeUtc"    # Ljava/util/Date;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultAddedDateTimeUtc:Ljava/util/Date;

    .line 80
    return-object p0
.end method

.method public setDefaultDisplayName(Ljava/lang/String;)Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultDisplayName"    # Ljava/lang/String;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultDisplayName:Ljava/lang/String;

    .line 84
    return-object p0
.end method

.method public setDefaultDisplayPicRaw(Ljava/lang/String;)Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultDisplayPicRaw"    # Ljava/lang/String;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultDisplayPicRaw:Ljava/lang/String;

    .line 88
    return-object p0
.end method

.method public setDefaultGamerScore(J)Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultGamerScore"    # J

    .prologue
    .line 91
    iput-wide p1, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultGamerScore:J

    .line 92
    return-object p0
.end method

.method public setDefaultGamertag(Ljava/lang/String;)Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultGamertag"    # Ljava/lang/String;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultGamertag:Ljava/lang/String;

    .line 96
    return-object p0
.end method

.method public setDefaultIsBroadcasting(Z)Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultIsBroadcasting"    # Z

    .prologue
    .line 99
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultIsBroadcasting:Z

    .line 100
    return-object p0
.end method

.method public setDefaultIsFavorite(Z)Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultIsFavorite"    # Z

    .prologue
    .line 103
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultIsFavorite:Z

    .line 104
    return-object p0
.end method

.method public setDefaultIsFollowedByCaller(Z)Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultIsFollowedByCaller"    # Z

    .prologue
    .line 107
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultIsFollowedByCaller:Z

    .line 108
    return-object p0
.end method

.method public setDefaultIsFollowingCaller(Z)Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultIsFollowingCaller"    # Z

    .prologue
    .line 111
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultIsFollowingCaller:Z

    .line 112
    return-object p0
.end method

.method public setDefaultIsIdentityShared(Z)Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultIsIdentityShared"    # Z

    .prologue
    .line 115
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultIsIdentityShared:Z

    .line 116
    return-object p0
.end method

.method public setDefaultMultiplayerSummary(Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;)Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultMultiplayerSummary"    # Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultMultiplayerSummary:Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;

    .line 120
    return-object p0
.end method

.method public setDefaultPreferredColor(Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;)Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultPreferredColor"    # Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultPreferredColor:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .line 124
    return-object p0
.end method

.method public setDefaultPresenceState(Lcom/microsoft/xbox/service/model/UserStatus;)Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultPresenceState"    # Lcom/microsoft/xbox/service/model/UserStatus;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultPresenceState:Lcom/microsoft/xbox/service/model/UserStatus;

    .line 128
    return-object p0
.end method

.method public setDefaultPresenceText(Ljava/lang/String;)Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultPresenceText"    # Ljava/lang/String;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultPresenceText:Ljava/lang/String;

    .line 132
    return-object p0
.end method

.method public setDefaultRealName(Ljava/lang/String;)Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultRealName"    # Ljava/lang/String;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultRealName:Ljava/lang/String;

    .line 136
    return-object p0
.end method

.method public setDefaultXuid(J)Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultXuid"    # J

    .prologue
    .line 139
    iput-wide p1, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->defaultXuid:J

    .line 140
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;)V
    .locals 4
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 145
    if-nez p2, :cond_0

    .line 146
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 183
    :goto_0
    return-void

    .line 149
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 150
    const-string v0, "addedDateTimeUtc"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->addedDateTimeUtcAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->addedDateTimeUtc()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 152
    const-string v0, "displayName"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 153
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->displayNameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->displayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 154
    const-string v0, "displayPicRaw"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->displayPicRawAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->displayPicRaw()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 156
    const-string v0, "gamerScore"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->gamerScoreAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->gamerScore()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 158
    const-string v0, "gamertag"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->gamertagAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->gamertag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 160
    const-string v0, "isBroadcasting"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 161
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->isBroadcastingAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->isBroadcasting()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 162
    const-string v0, "isFavorite"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->isFavoriteAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->isFavorite()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 164
    const-string v0, "isFollowedByCaller"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 165
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->isFollowedByCallerAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->isFollowedByCaller()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 166
    const-string v0, "isFollowingCaller"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 167
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->isFollowingCallerAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->isFollowingCaller()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 168
    const-string v0, "isIdentityShared"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->isIdentitySharedAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->isIdentityShared()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 170
    const-string v0, "multiplayerSummary"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 171
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->multiplayerSummaryAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->multiplayerSummary()Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$MultiplayerSummary;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 172
    const-string v0, "preferredColor"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->preferredColorAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->preferredColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 174
    const-string v0, "presenceState"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->presenceStateAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->presenceState()Lcom/microsoft/xbox/service/model/UserStatus;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 176
    const-string v0, "presenceText"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 177
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->presenceTextAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->presenceText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 178
    const-string v0, "realName"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 179
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->realNameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->realName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 180
    const-string v0, "xuid"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 181
    iget-object v0, p0, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->xuidAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;->xuid()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 182
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto/16 :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27
    check-cast p2, Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/peopleHub/AutoValue_PeopleHubDataTypes_PeopleHubPerson$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/peopleHub/PeopleHubDataTypes$PeopleHubPerson;)V

    return-void
.end method
