.class public Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;
.super Landroid/widget/LinearLayout;
.source "VerticalScrollDockLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;
    }
.end annotation


# static fields
.field private static final EPSILON:F = 1.0E-5f

.field private static final HEADER_ANIM_TIMESPAN:I = 0xf0

.field private static final HEADER_MAX_MOMENTUM_ANIM_TIMESPAN:I = 0x258

.field private static final HEADER_VELOCITY_SCALE:F = 0.5f

.field private static final MIN_FLING_VELOCITY_SCALE:I = 0x4

.field private static final PIXELS_PER_SECOND:I = 0x3e8

.field public static final VERTICAL_SCROLLDOCK_SCROLL_CONTAINER_TAG:Ljava/lang/String; = "VerticalScrollDock_ScrollContainer"


# instance fields
.field private activePointerId:I

.field private activeScrollState:I

.field private firstTouchX:I

.field private firstTouchY:I

.field private headerHeight:F

.field private headerLargeView:Landroid/view/View;

.field private headerLargeViewHeight:I

.field private headerMomentumAnim:Landroid/animation/ValueAnimator;

.field private headerSmallView:Landroid/view/View;

.field private headerSmallViewHeight:I

.field private headerState:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

.field private headerVelocityTracker:Landroid/view/VelocityTracker;

.field private headerView:Landroid/view/View;

.field private isInterceptingTouches:Z

.field private minFlingVelocity:I

.field private playHeaderAnimation:Z

.field private prevScrollX:F

.field private prevScrollY:F

.field private scrollViewPrevScrollY:I

.field private scrollableView:Landroid/view/View;

.field private scrollableViewID:I

.field private smallViewSlideUpAnim:Landroid/animation/ObjectAnimator;

.field private touchSlop:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 105
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 66
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerHeight:F

    .line 72
    iput v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableViewID:I

    .line 75
    sget-object v0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;->FULL_OPEN:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerState:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    .line 82
    iput v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerLargeViewHeight:I

    .line 83
    iput v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerSmallViewHeight:I

    .line 94
    iput v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->activePointerId:I

    .line 95
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->isInterceptingTouches:Z

    .line 96
    iput v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->firstTouchY:I

    .line 97
    iput v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->firstTouchX:I

    .line 98
    iput v3, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->prevScrollY:F

    .line 99
    iput v3, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->prevScrollX:F

    .line 100
    iput v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->activeScrollState:I

    .line 101
    iput v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollViewPrevScrollY:I

    .line 102
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->playHeaderAnimation:Z

    .line 106
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 110
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerHeight:F

    .line 72
    iput v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableViewID:I

    .line 75
    sget-object v0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;->FULL_OPEN:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerState:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    .line 82
    iput v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerLargeViewHeight:I

    .line 83
    iput v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerSmallViewHeight:I

    .line 94
    iput v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->activePointerId:I

    .line 95
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->isInterceptingTouches:Z

    .line 96
    iput v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->firstTouchY:I

    .line 97
    iput v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->firstTouchX:I

    .line 98
    iput v3, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->prevScrollY:F

    .line 99
    iput v3, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->prevScrollX:F

    .line 100
    iput v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->activeScrollState:I

    .line 101
    iput v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollViewPrevScrollY:I

    .line 102
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->playHeaderAnimation:Z

    .line 111
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 112
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 115
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerHeight:F

    .line 72
    iput v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableViewID:I

    .line 75
    sget-object v0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;->FULL_OPEN:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerState:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    .line 82
    iput v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerLargeViewHeight:I

    .line 83
    iput v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerSmallViewHeight:I

    .line 94
    iput v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->activePointerId:I

    .line 95
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->isInterceptingTouches:Z

    .line 96
    iput v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->firstTouchY:I

    .line 97
    iput v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->firstTouchX:I

    .line 98
    iput v3, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->prevScrollY:F

    .line 99
    iput v3, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->prevScrollX:F

    .line 100
    iput v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->activeScrollState:I

    .line 101
    iput v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollViewPrevScrollY:I

    .line 102
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->playHeaderAnimation:Z

    .line 116
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 117
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    .prologue
    .line 44
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->activeScrollState:I

    return v0
.end method

.method static synthetic access$002(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;I)I
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->activeScrollState:I

    return p1
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerState:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)F
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    .prologue
    .line 44
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->prevScrollX:F

    return v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)F
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    .prologue
    .line 44
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->prevScrollY:F

    return v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    .prologue
    .line 44
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollViewPrevScrollY:I

    return v0
.end method

.method static synthetic access$402(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;I)I
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollViewPrevScrollY:I

    return p1
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerSmallView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;

    .prologue
    .line 44
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerSmallViewHeight:I

    return v0
.end method

.method static synthetic access$702(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->playHeaderAnimation:Z

    return p1
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;F)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;
    .param p1, "x1"    # F

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->moveViews(F)Z

    move-result v0

    return v0
.end method

.method private getCanAnimateHeader()Z
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 584
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerLargeView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerSmallView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerLargeViewHeight:I

    if-le v0, v1, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerSmallViewHeight:I

    if-le v0, v1, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerLargeViewHeight:I

    iget v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerSmallViewHeight:I

    if-lt v0, v1, :cond_0

    .line 586
    const/4 v0, 0x1

    .line 588
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 121
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->setOrientation(I)V

    .line 123
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    .line 124
    .local v1, "viewConfig":Landroid/view/ViewConfiguration;
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    iput v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->touchSlop:I

    .line 125
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v2

    mul-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->minFlingVelocity:I

    .line 127
    if-eqz p2, :cond_0

    .line 129
    sget-object v2, Lcom/microsoft/xboxone/smartglass/R$styleable;->VerticalScrollDockLayout:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 130
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableViewID:I

    .line 131
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 133
    .end local v0    # "a":Landroid/content/res/TypedArray;
    :cond_0
    return-void
.end method

.method private initializeHeaderCloseAnimations()V
    .locals 5

    .prologue
    .line 595
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->getCanAnimateHeader()Z

    move-result v0

    if-nez v0, :cond_0

    .line 627
    :goto_0
    return-void

    .line 600
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerSmallView:Landroid/view/View;

    const-string v1, "translationY"

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    iget v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerSmallViewHeight:I

    int-to-float v4, v4

    aput v4, v2, v3

    const/4 v3, 0x1

    const/4 v4, 0x0

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->smallViewSlideUpAnim:Landroid/animation/ObjectAnimator;

    .line 601
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->smallViewSlideUpAnim:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0xf0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 602
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->smallViewSlideUpAnim:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 603
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->smallViewSlideUpAnim:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$3;-><init>(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0
.end method

.method private initializeHeaderMomentumAnimation()V
    .locals 4

    .prologue
    .line 633
    const/4 v0, 0x0

    new-array v0, v0, [I

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerMomentumAnim:Landroid/animation/ValueAnimator;

    .line 634
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerMomentumAnim:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 635
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerMomentumAnim:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 636
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerMomentumAnim:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$4;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$4;-><init>(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 648
    return-void
.end method

.method private isListAtTop()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 566
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableView:Landroid/view/View;

    if-nez v2, :cond_1

    .line 580
    :cond_0
    :goto_0
    return v3

    .line 571
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableView:Landroid/view/View;

    instance-of v2, v2, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    if-eqz v2, :cond_3

    .line 572
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableView:Landroid/view/View;

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->getScrollState()Lcom/microsoft/xbox/toolkit/ui/ScrollState;

    move-result-object v1

    .line 573
    .local v1, "state":Lcom/microsoft/xbox/toolkit/ui/ScrollState;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ScrollState;->getOffset()I

    move-result v2

    if-nez v2, :cond_2

    move v2, v3

    :goto_1
    move v3, v2

    goto :goto_0

    :cond_2
    move v2, v4

    goto :goto_1

    .line 575
    .end local v1    # "state":Lcom/microsoft/xbox/toolkit/ui/ScrollState;
    :cond_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableView:Landroid/view/View;

    instance-of v2, v2, Landroid/widget/ScrollView;

    if-eqz v2, :cond_0

    .line 576
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableView:Landroid/view/View;

    check-cast v2, Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v0

    .line 577
    .local v0, "scrollY":I
    if-eqz v0, :cond_0

    move v3, v4

    goto :goto_0
.end method

.method private moveViews(F)Z
    .locals 11
    .param p1, "offsetY"    # F

    .prologue
    .line 492
    const/4 v6, 0x0

    .line 493
    .local v6, "reachedEnd":Z
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->getChildCount()I

    move-result v1

    .line 494
    .local v1, "childCount":I
    if-nez v1, :cond_0

    move v7, v6

    .line 562
    .end local v6    # "reachedEnd":Z
    .local v7, "reachedEnd":I
    :goto_0
    return v7

    .line 500
    .end local v7    # "reachedEnd":I
    .restart local v6    # "reachedEnd":Z
    :cond_0
    const/4 v8, 0x0

    invoke-virtual {p0, v8}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getTranslationY()F

    move-result v2

    .line 504
    .local v2, "curYOffset":F
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->getCanAnimateHeader()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 507
    add-float v8, v2, p1

    const/4 v9, 0x0

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v8

    iget v9, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerLargeViewHeight:I

    iget v10, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerSmallViewHeight:I

    sub-int/2addr v9, v10

    neg-int v9, v9

    int-to-float v9, v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(FF)F

    move-result v5

    .line 508
    .local v5, "newYOffset":F
    const/high16 v8, -0x40800000    # -1.0f

    iget v9, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerLargeViewHeight:I

    iget v10, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerSmallViewHeight:I

    sub-int/2addr v9, v10

    int-to-float v9, v9

    mul-float/2addr v8, v9

    const v9, 0x3727c5ac    # 1.0E-5f

    add-float v4, v8, v9

    .line 517
    .local v4, "nearHeaderHeight":F
    :goto_1
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    if-ge v3, v1, :cond_2

    .line 518
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v5}, Landroid/view/View;->setTranslationY(F)V

    .line 517
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 512
    .end local v3    # "i":I
    .end local v4    # "nearHeaderHeight":F
    .end local v5    # "newYOffset":F
    :cond_1
    add-float v8, v2, p1

    const/4 v9, 0x0

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v8

    iget v9, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerHeight:F

    neg-float v9, v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(FF)F

    move-result v5

    .line 513
    .restart local v5    # "newYOffset":F
    const/high16 v8, -0x40800000    # -1.0f

    iget v9, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerHeight:F

    mul-float/2addr v8, v9

    const v9, 0x3727c5ac    # 1.0E-5f

    add-float v4, v8, v9

    .restart local v4    # "nearHeaderHeight":F
    goto :goto_1

    .line 521
    .restart local v3    # "i":I
    :cond_2
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->getCanAnimateHeader()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 523
    const/high16 v8, -0x40800000    # -1.0f

    iget v9, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerLargeViewHeight:I

    iget v10, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerSmallViewHeight:I

    sub-int/2addr v9, v10

    int-to-float v9, v9

    mul-float/2addr v8, v9

    div-float v0, v5, v8

    .line 524
    .local v0, "animationTime":F
    const/4 v8, 0x0

    invoke-static {v0, v8}, Ljava/lang/Math;->max(FF)F

    move-result v8

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 525
    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerLargeView:Landroid/view/View;

    const/high16 v9, 0x3f800000    # 1.0f

    sub-float/2addr v9, v0

    invoke-virtual {v8, v9}, Landroid/view/View;->setAlpha(F)V

    .line 526
    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerSmallView:Landroid/view/View;

    invoke-virtual {v8, v0}, Landroid/view/View;->setAlpha(F)V

    .line 530
    .end local v0    # "animationTime":F
    :cond_3
    const v8, -0x48d83a54    # -1.0E-5f

    cmpl-float v8, v5, v8

    if-lez v8, :cond_5

    const v8, -0x48d83a54    # -1.0E-5f

    cmpg-float v8, v2, v8

    if-gtz v8, :cond_5

    .line 531
    sget-object v8, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;->FULL_OPEN:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    iput-object v8, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerState:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    .line 532
    const/4 v6, 0x1

    .line 533
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->getCanAnimateHeader()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 534
    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerSmallView:Landroid/view/View;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 535
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->playHeaderAnimation:Z

    :cond_4
    :goto_3
    move v7, v6

    .line 562
    .restart local v7    # "reachedEnd":I
    goto/16 :goto_0

    .line 538
    .end local v7    # "reachedEnd":I
    :cond_5
    const v8, -0x48d83a54    # -1.0E-5f

    cmpg-float v8, v5, v8

    if-gez v8, :cond_6

    const v8, -0x48d83a54    # -1.0E-5f

    cmpl-float v8, v2, v8

    if-ltz v8, :cond_6

    .line 539
    sget-object v8, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;->PARTIAL_OPEN:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    iput-object v8, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerState:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    goto :goto_3

    .line 541
    :cond_6
    cmpg-float v8, v5, v4

    if-gtz v8, :cond_7

    cmpl-float v8, v2, v4

    if-lez v8, :cond_7

    .line 542
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->isInterceptingTouches:Z

    .line 543
    sget-object v8, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;->FULL_CLOSED:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    iput-object v8, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerState:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    .line 544
    const/4 v6, 0x1

    .line 545
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->getCanAnimateHeader()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 546
    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerLargeView:Landroid/view/View;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 547
    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->smallViewSlideUpAnim:Landroid/animation/ObjectAnimator;

    if-eqz v8, :cond_4

    iget-boolean v8, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->playHeaderAnimation:Z

    if-eqz v8, :cond_4

    .line 548
    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->smallViewSlideUpAnim:Landroid/animation/ObjectAnimator;

    invoke-virtual {v8}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_3

    .line 552
    :cond_7
    cmpl-float v8, v5, v4

    if-lez v8, :cond_4

    cmpg-float v8, v2, v4

    if-gtz v8, :cond_4

    .line 553
    sget-object v8, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;->PARTIAL_OPEN:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    iput-object v8, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerState:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    .line 554
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->getCanAnimateHeader()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 555
    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerLargeView:Landroid/view/View;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 556
    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->smallViewSlideUpAnim:Landroid/animation/ObjectAnimator;

    if-eqz v8, :cond_4

    .line 557
    iget-object v8, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->smallViewSlideUpAnim:Landroid/animation/ObjectAnimator;

    invoke-virtual {v8}, Landroid/animation/ObjectAnimator;->cancel()V

    goto :goto_3
.end method

.method private startTrackingVelocity(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 656
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerMomentumAnim:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerMomentumAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 657
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerMomentumAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->cancel()V

    .line 660
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v1, :cond_3

    .line 661
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerVelocityTracker:Landroid/view/VelocityTracker;

    .line 666
    :goto_0
    invoke-static {p1}, Landroid/support/v4/view/MotionEventCompat;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result v0

    .line 667
    .local v0, "action":I
    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    if-nez v0, :cond_2

    .line 668
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v1, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 670
    :cond_2
    return-void

    .line 663
    .end local v0    # "action":I
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_0
.end method

.method private stopInterceptingTouches()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 478
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->isInterceptingTouches:Z

    .line 479
    iput v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->firstTouchY:I

    .line 480
    iput v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->firstTouchX:I

    .line 481
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->activePointerId:I

    .line 482
    return-void
.end method

.method private stopTrackingVelocity(Landroid/view/MotionEvent;Z)V
    .locals 9
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "playAnimation"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 696
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v1, :cond_2

    .line 697
    iget v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->activePointerId:I

    const/4 v3, -0x1

    if-eq v1, v3, :cond_1

    .line 698
    if-eqz p1, :cond_0

    .line 699
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v1, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 701
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v3, 0x3e8

    const v4, 0x7f7fffff    # Float.MAX_VALUE

    invoke-virtual {v1, v3, v4}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 702
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerVelocityTracker:Landroid/view/VelocityTracker;

    iget v3, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->activePointerId:I

    invoke-static {v1, v3}, Landroid/support/v4/view/VelocityTrackerCompat;->getYVelocity(Landroid/view/VelocityTracker;I)F

    move-result v0

    .line 704
    .local v0, "yVelocity":F
    if-eqz p2, :cond_1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v3, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->minFlingVelocity:I

    int-to-float v3, v3

    cmpl-float v1, v1, v3

    if-ltz v1, :cond_1

    .line 705
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerMomentumAnim:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerState:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    sget-object v3, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;->PARTIAL_OPEN:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    if-ne v1, v3, :cond_1

    .line 706
    iget-object v3, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerMomentumAnim:Landroid/animation/ValueAnimator;

    new-array v4, v8, [Landroid/animation/PropertyValuesHolder;

    move-object v1, v2

    check-cast v1, Ljava/lang/String;

    const/4 v5, 0x2

    new-array v5, v5, [I

    const/high16 v6, 0x3f000000    # 0.5f

    mul-float/2addr v6, v0

    float-to-int v6, v6

    aput v6, v5, v7

    aput v7, v5, v8

    invoke-static {v1, v5}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    aput-object v1, v4, v7

    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    .line 707
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerMomentumAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 711
    .end local v0    # "yVelocity":F
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->clear()V

    .line 712
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->recycle()V

    .line 713
    iput-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerVelocityTracker:Landroid/view/VelocityTracker;

    .line 715
    :cond_2
    return-void
.end method

.method private trackVelocity(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 678
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerMomentumAnim:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerMomentumAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 679
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerMomentumAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->cancel()V

    .line 682
    :cond_0
    invoke-static {p1}, Landroid/support/v4/view/MotionEventCompat;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result v0

    .line 683
    .local v0, "action":I
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 684
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v1, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 686
    :cond_1
    return-void
.end method


# virtual methods
.method public addHeaderView(Landroid/view/View;)V
    .locals 1
    .param p1, "headerView"    # Landroid/view/View;

    .prologue
    const/4 v0, 0x0

    .line 160
    if-nez p1, :cond_0

    .line 169
    :goto_0
    return-void

    .line 165
    :cond_0
    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->addView(Landroid/view/View;I)V

    .line 168
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->setHeader(I)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 137
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 140
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    .line 142
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 144
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->setHeader(I)V

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableView:Landroid/view/View;

    if-nez v0, :cond_1

    .line 149
    iget v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableViewID:I

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->setScrollContainerView(I)V

    .line 152
    :cond_1
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 345
    iget-object v9, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerView:Landroid/view/View;

    if-nez v9, :cond_1

    .line 346
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v7

    .line 415
    :cond_0
    :goto_0
    return v7

    .line 349
    :cond_1
    invoke-static {p1}, Landroid/support/v4/view/MotionEventCompat;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result v0

    .line 352
    .local v0, "action":I
    const/4 v9, 0x3

    if-eq v0, v9, :cond_2

    if-ne v0, v7, :cond_3

    .line 353
    :cond_2
    invoke-direct {p0, v12, v8}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->stopTrackingVelocity(Landroid/view/MotionEvent;Z)V

    .line 354
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->stopInterceptingTouches()V

    move v7, v8

    .line 355
    goto :goto_0

    .line 358
    :cond_3
    packed-switch v0, :pswitch_data_0

    :cond_4
    :goto_1
    :pswitch_0
    move v7, v8

    .line 415
    goto :goto_0

    .line 360
    :pswitch_1
    iput-boolean v8, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->isInterceptingTouches:Z

    .line 361
    invoke-static {p1}, Landroid/support/v4/view/MotionEventCompat;->getActionIndex(Landroid/view/MotionEvent;)I

    move-result v2

    .line 364
    .local v2, "pointerIndex":I
    invoke-static {p1, v2}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v7

    iput v7, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->prevScrollX:F

    .line 365
    invoke-static {p1, v2}, Landroid/support/v4/view/MotionEventCompat;->getY(Landroid/view/MotionEvent;I)F

    move-result v7

    iput v7, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->prevScrollY:F

    .line 366
    iget v7, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->prevScrollY:F

    float-to-int v7, v7

    iput v7, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->firstTouchY:I

    .line 367
    iget v7, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->prevScrollX:F

    float-to-int v7, v7

    iput v7, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->firstTouchX:I

    .line 368
    invoke-static {p1, v8}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v7

    iput v7, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->activePointerId:I

    goto :goto_1

    .line 372
    .end local v2    # "pointerIndex":I
    :pswitch_2
    iget-boolean v9, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->isInterceptingTouches:Z

    if-nez v9, :cond_0

    .line 376
    iget v9, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->activePointerId:I

    if-ltz v9, :cond_5

    invoke-static {p1}, Landroid/support/v4/view/MotionEventCompat;->getPointerCount(Landroid/view/MotionEvent;)I

    move-result v9

    iget v10, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->activePointerId:I

    if-gt v9, v10, :cond_6

    .line 382
    :cond_5
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->stopInterceptingTouches()V

    .line 383
    invoke-direct {p0, v12, v8}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->stopTrackingVelocity(Landroid/view/MotionEvent;Z)V

    move v7, v8

    .line 384
    goto :goto_0

    .line 387
    :cond_6
    iget v9, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->activePointerId:I

    invoke-static {p1, v9}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v1

    .line 388
    .local v1, "dragPointerIndex":I
    if-ltz v1, :cond_9

    .line 389
    invoke-static {p1, v1}, Landroid/support/v4/view/MotionEventCompat;->getY(Landroid/view/MotionEvent;I)F

    move-result v5

    .line 390
    .local v5, "y":F
    invoke-static {p1, v1}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v3

    .line 391
    .local v3, "x":F
    iget v9, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->firstTouchY:I

    int-to-float v9, v9

    sub-float v6, v5, v9

    .line 392
    .local v6, "yDiff":F
    iget v9, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->firstTouchX:I

    int-to-float v9, v9

    sub-float v4, v3, v9

    .line 396
    .local v4, "xDiff":F
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v9

    iget v10, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->touchSlop:I

    int-to-float v10, v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_4

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v9

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v10

    cmpl-float v9, v9, v10

    if-lez v9, :cond_4

    .line 398
    iget-object v9, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerState:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    sget-object v10, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;->PARTIAL_OPEN:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    if-eq v9, v10, :cond_8

    iget-object v9, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerState:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    sget-object v10, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;->FULL_OPEN:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    if-ne v9, v10, :cond_7

    cmpg-float v9, v6, v11

    if-ltz v9, :cond_8

    :cond_7
    iget-object v9, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerState:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    sget-object v10, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;->FULL_CLOSED:Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$HeaderState;

    if-ne v9, v10, :cond_4

    cmpl-float v9, v6, v11

    if-lez v9, :cond_4

    .line 399
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->isListAtTop()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 401
    :cond_8
    iput-boolean v7, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->isInterceptingTouches:Z

    .line 402
    float-to-int v8, v5

    int-to-float v8, v8

    iput v8, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->prevScrollY:F

    .line 403
    invoke-static {p1, v1}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v8

    iput v8, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->prevScrollX:F

    .line 404
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->startTrackingVelocity(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 409
    .end local v3    # "x":F
    .end local v4    # "xDiff":F
    .end local v5    # "y":F
    .end local v6    # "yDiff":F
    :cond_9
    const-string v7, "VerticalScrollDockLayout"

    const-string v9, "drag pointer index < 0"

    invoke-static {v7, v9}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 358
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 10
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 305
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 308
    iget v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerLargeViewHeight:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerLargeView:Landroid/view/View;

    if-eqz v4, :cond_0

    .line 309
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerLargeView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    iput v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerLargeViewHeight:I

    .line 311
    :cond_0
    iget v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerSmallViewHeight:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerSmallView:Landroid/view/View;

    if-eqz v4, :cond_1

    .line 312
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerSmallView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    iput v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerSmallViewHeight:I

    .line 316
    :cond_1
    iget v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerHeight:F

    const/high16 v5, -0x40800000    # -1.0f

    cmpl-float v4, v4, v5

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerView:Landroid/view/View;

    if-eqz v4, :cond_3

    .line 317
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    if-lez v4, :cond_3

    .line 319
    iget-object v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    iput v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerHeight:F

    .line 321
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->getMeasuredHeight()I

    move-result v4

    iget v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerHeight:F

    float-to-int v5, v5

    add-int v2, v4, v5

    .line 322
    .local v2, "newHeight":I
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->getCanAnimateHeader()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 323
    iget v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerSmallViewHeight:I

    sub-int/2addr v2, v4

    .line 327
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 330
    .local v1, "layoutParamType":Ljava/lang/Class;, "Ljava/lang/Class<+Landroid/view/ViewGroup$LayoutParams;>;"
    const/4 v4, 0x2

    :try_start_0
    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v1, v4}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, -0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$LayoutParams;

    .line 331
    .local v3, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 337
    .end local v3    # "params":Landroid/view/ViewGroup$LayoutParams;
    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->initializeHeaderCloseAnimations()V

    .line 340
    .end local v1    # "layoutParamType":Ljava/lang/Class;, "Ljava/lang/Class<+Landroid/view/ViewGroup$LayoutParams;>;"
    .end local v2    # "newHeight":I
    :cond_3
    return-void

    .line 332
    .restart local v1    # "layoutParamType":Ljava/lang/Class;, "Ljava/lang/Class<+Landroid/view/ViewGroup$LayoutParams;>;"
    .restart local v2    # "newHeight":I
    :catch_0
    move-exception v0

    .line 333
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "VerticalScrollDockLayout"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "Failed to create LayoutParams of type \'%s\' with error: %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 422
    iget-object v7, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerView:Landroid/view/View;

    if-nez v7, :cond_1

    .line 424
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    .line 473
    :cond_0
    :goto_0
    return v5

    .line 427
    :cond_1
    invoke-static {p1}, Landroid/support/v4/view/MotionEventCompat;->getActionMasked(Landroid/view/MotionEvent;)I

    move-result v0

    .line 428
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 464
    :pswitch_0
    invoke-direct {p0, p1, v6}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->stopTrackingVelocity(Landroid/view/MotionEvent;Z)V

    .line 465
    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->isInterceptingTouches:Z

    goto :goto_0

    .line 430
    :pswitch_1
    iget v7, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->activePointerId:I

    if-ltz v7, :cond_0

    .line 434
    iget v7, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->activePointerId:I

    invoke-static {p1, v7}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v2

    .line 435
    .local v2, "dragPointerIndex":I
    if-ltz v2, :cond_0

    .line 436
    invoke-static {p1, v2}, Landroid/support/v4/view/MotionEventCompat;->getY(Landroid/view/MotionEvent;I)F

    move-result v4

    .line 437
    .local v4, "y":F
    invoke-static {p1, v2}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v7

    iput v7, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->prevScrollX:F

    .line 440
    iget v7, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->prevScrollY:F

    sub-float v7, v4, v7

    invoke-direct {p0, v7}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->moveViews(F)Z

    .line 443
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->trackVelocity(Landroid/view/MotionEvent;)V

    .line 446
    iget-boolean v7, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->isInterceptingTouches:Z

    if-nez v7, :cond_2

    .line 448
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    .line 449
    .local v1, "cancelTouch":Landroid/view/MotionEvent;
    const/4 v7, 0x3

    invoke-virtual {v1, v7}, Landroid/view/MotionEvent;->setAction(I)V

    .line 450
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 451
    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    .line 452
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v3

    .line 453
    .local v3, "newTouch":Landroid/view/MotionEvent;
    invoke-virtual {v3, v5}, Landroid/view/MotionEvent;->setAction(I)V

    .line 454
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 455
    invoke-virtual {v3}, Landroid/view/MotionEvent;->recycle()V

    .line 458
    .end local v1    # "cancelTouch":Landroid/view/MotionEvent;
    .end local v3    # "newTouch":Landroid/view/MotionEvent;
    :cond_2
    iput v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->prevScrollY:F

    move v5, v6

    .line 459
    goto :goto_0

    .line 468
    .end local v2    # "dragPointerIndex":I
    .end local v4    # "y":F
    :pswitch_2
    const/4 v6, 0x0

    invoke-direct {p0, v6, v5}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->stopTrackingVelocity(Landroid/view/MotionEvent;Z)V

    .line 469
    iput-boolean v5, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->isInterceptingTouches:Z

    goto :goto_0

    .line 428
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public resetScrollContainer()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 287
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableView:Landroid/view/View;

    if-eqz v2, :cond_1

    .line 288
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableView:Landroid/view/View;

    instance-of v2, v2, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    if-eqz v2, :cond_2

    .line 289
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableView:Landroid/view/View;

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    .line 290
    .local v0, "listView":Lcom/microsoft/xbox/toolkit/ui/XLEListView;
    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 295
    .end local v0    # "listView":Lcom/microsoft/xbox/toolkit/ui/XLEListView;
    :cond_0
    :goto_0
    iput-object v3, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableView:Landroid/view/View;

    .line 298
    :cond_1
    const/4 v2, -0x1

    iput v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableViewID:I

    .line 299
    iput v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->activeScrollState:I

    .line 300
    iput v4, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollViewPrevScrollY:I

    .line 301
    return-void

    .line 291
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableView:Landroid/view/View;

    instance-of v2, v2, Landroid/widget/ScrollView;

    if-eqz v2, :cond_0

    .line 292
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableView:Landroid/view/View;

    check-cast v1, Landroid/widget/ScrollView;

    .line 293
    .local v1, "scrollView":Landroid/widget/ScrollView;
    invoke-virtual {v1, v3}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0
.end method

.method public setHeader(I)V
    .locals 2
    .param p1, "childViewIndex"    # I

    .prologue
    .line 178
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerView:Landroid/view/View;

    .line 181
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerView:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerView:Landroid/view/View;

    const v1, 0x7f0e01a6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerLargeView:Landroid/view/View;

    .line 183
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerView:Landroid/view/View;

    const v1, 0x7f0e01ac

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerSmallView:Landroid/view/View;

    .line 184
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerSmallView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->headerSmallView:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 190
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->initializeHeaderMomentumAnimation()V

    .line 192
    :cond_1
    return-void
.end method

.method public setScrollContainerView(I)V
    .locals 1
    .param p1, "scrollContainerID"    # I

    .prologue
    .line 200
    if-gez p1, :cond_0

    .line 201
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->resetScrollContainer()V

    .line 208
    :goto_0
    return-void

    .line 206
    :cond_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->setScrollContainerView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public setScrollContainerView(Landroid/view/View;)V
    .locals 3
    .param p1, "scrollingView"    # Landroid/view/View;

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->resetScrollContainer()V

    .line 219
    if-nez p1, :cond_1

    .line 281
    :cond_0
    :goto_0
    return-void

    .line 223
    :cond_1
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableView:Landroid/view/View;

    .line 224
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    iput v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableViewID:I

    .line 226
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableView:Landroid/view/View;

    instance-of v2, v2, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    if-eqz v2, :cond_2

    .line 227
    iget-object v0, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableView:Landroid/view/View;

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    .line 228
    .local v0, "listView":Lcom/microsoft/xbox/toolkit/ui/XLEListView;
    new-instance v2, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$1;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$1;-><init>(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)V

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    goto :goto_0

    .line 254
    .end local v0    # "listView":Lcom/microsoft/xbox/toolkit/ui/XLEListView;
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableView:Landroid/view/View;

    instance-of v2, v2, Landroid/widget/ScrollView;

    if-eqz v2, :cond_0

    .line 255
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;->scrollableView:Landroid/view/View;

    check-cast v1, Landroid/widget/ScrollView;

    .line 256
    .local v1, "scrollView":Landroid/widget/ScrollView;
    new-instance v2, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$2;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout$2;-><init>(Lcom/microsoft/xbox/xle/ui/VerticalScrollDockLayout;)V

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0
.end method
