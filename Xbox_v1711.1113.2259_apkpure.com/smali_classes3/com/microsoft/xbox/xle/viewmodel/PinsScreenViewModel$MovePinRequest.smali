.class Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;
.super Ljava/lang/Object;
.source "PinsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MovePinRequest"
.end annotation


# instance fields
.field private final beforeDstItem:Z

.field private final dstItem:Lcom/microsoft/xbox/service/model/pins/PinItem;

.field private lastResult:Lcom/microsoft/xbox/toolkit/AsyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final srcItem:Lcom/microsoft/xbox/service/model/pins/PinItem;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/pins/PinItem;Lcom/microsoft/xbox/service/model/pins/PinItem;Z)V
    .locals 0
    .param p1, "srcItem"    # Lcom/microsoft/xbox/service/model/pins/PinItem;
    .param p2, "dstItem"    # Lcom/microsoft/xbox/service/model/pins/PinItem;
    .param p3, "beforeDstItem"    # Z

    .prologue
    .line 252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 253
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;->srcItem:Lcom/microsoft/xbox/service/model/pins/PinItem;

    .line 254
    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;->dstItem:Lcom/microsoft/xbox/service/model/pins/PinItem;

    .line 255
    iput-boolean p3, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;->beforeDstItem:Z

    .line 256
    return-void
.end method


# virtual methods
.method public getDstItem()Lcom/microsoft/xbox/service/model/pins/PinItem;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;->dstItem:Lcom/microsoft/xbox/service/model/pins/PinItem;

    return-object v0
.end method

.method public getLastResult()Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 271
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;->lastResult:Lcom/microsoft/xbox/toolkit/AsyncResult;

    return-object v0
.end method

.method public getSrcItem()Lcom/microsoft/xbox/service/model/pins/PinItem;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;->srcItem:Lcom/microsoft/xbox/service/model/pins/PinItem;

    return-object v0
.end method

.method public isBeforeDstItem()Z
    .locals 1

    .prologue
    .line 267
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;->beforeDstItem:Z

    return v0
.end method

.method public setLastResult(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 275
    .local p1, "lastResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PinsScreenViewModel$MovePinRequest;->lastResult:Lcom/microsoft/xbox/toolkit/AsyncResult;

    .line 276
    return-void
.end method
