.class public final Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;
.super Ljava/lang/Object;
.source "ChatHistoryDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BatchMessageResponse"
.end annotation


# instance fields
.field private volatile transient hashCode:I

.field private final messages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;",
            ">;"
        }
    .end annotation
.end field

.field private final notFound:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 240
    .local p1, "messages":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;>;"
    .local p2, "notFound":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 241
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;->messages:Ljava/util/List;

    .line 242
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;->notFound:Ljava/util/List;

    .line 243
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 257
    if-ne p0, p1, :cond_1

    .line 263
    :cond_0
    :goto_0
    return v1

    .line 259
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;

    if-nez v3, :cond_2

    move v1, v2

    .line 260
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 262
    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;

    .line 263
    .local v0, "other":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;
    iget-object v3, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;->messages:Ljava/util/List;

    iget-object v4, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;->messages:Ljava/util/List;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;->notFound:Ljava/util/List;

    iget-object v4, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;->notFound:Ljava/util/List;

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getMessages()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 247
    iget-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;->messages:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getNotFound()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 252
    iget-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;->notFound:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 269
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;->hashCode:I

    if-nez v0, :cond_0

    .line 270
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;->hashCode:I

    .line 271
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;->messages:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;->hashCode:I

    .line 272
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;->notFound:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;->hashCode:I

    .line 275
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$BatchMessageResponse;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 280
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
