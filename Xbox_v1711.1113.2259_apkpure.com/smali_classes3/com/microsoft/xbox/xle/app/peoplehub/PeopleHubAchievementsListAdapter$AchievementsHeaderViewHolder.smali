.class public Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
.source "PeopleHubAchievementsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AchievementsHeaderViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;",
        ">;"
    }
.end annotation


# instance fields
.field achievementMeLayout:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e03fc
    .end annotation
.end field

.field achievementYouLayout:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0407
    .end annotation
.end field

.field compareAchievementButton:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08fb
    .end annotation
.end field

.field comparisonHeader:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08fc
    .end annotation
.end field

.field private final dateFormat:Ljava/text/SimpleDateFormat;

.field filterSpinner:Landroid/widget/Spinner;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08fd
    .end annotation
.end field

.field private filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter",
            "<",
            "Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;",
            ">;"
        }
    .end annotation
.end field

.field meGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e03f9
    .end annotation
.end field

.field meGamerscoreChange:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e03fa
    .end annotation
.end field

.field meGamerscoreMonth:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e03fb
    .end annotation
.end field

.field meProfileImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e03f7
    .end annotation
.end field

.field otherGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0404
    .end annotation
.end field

.field otherGamerscoreChange:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0405
    .end annotation
.end field

.field otherGamerscoreMonth:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0406
    .end annotation
.end field

.field otherGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0401
    .end annotation
.end field

.field otherProfileImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0402
    .end annotation
.end field

.field rareAchievementMeLayout:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e03fe
    .end annotation
.end field

.field rareAchievementYouLayout:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e0409
    .end annotation
.end field

.field shareAchievementButton:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0e08fa
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;Landroid/view/View;)V
    .locals 13
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    const v12, 0x7f0705ff

    const v10, 0x1090008

    const/16 v9, 0x8

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 194
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    .line 195
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;-><init>(Landroid/view/View;)V

    .line 156
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v7, "MMMM"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-direct {v4, v7, v8}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->dateFormat:Ljava/text/SimpleDateFormat;

    .line 196
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 198
    new-instance v4, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v7

    .line 200
    invoke-static {}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;->values()[Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;

    move-result-object v8

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-direct {v4, v7, v10, v8}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    .line 202
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    move-result-object v2

    .line 204
    .local v2, "peopleHubAchievementsScreenViewModel":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->meGamerscoreChange:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isMeProfile()Z

    move-result v4

    if-nez v4, :cond_2

    move v4, v5

    :goto_0
    invoke-static {v7, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 205
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->otherGamerscoreChange:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isMeProfile()Z

    move-result v4

    if-nez v4, :cond_3

    move v4, v5

    :goto_1
    invoke-static {v7, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 206
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->meGamerscoreMonth:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isMeProfile()Z

    move-result v4

    if-nez v4, :cond_4

    move v4, v5

    :goto_2
    invoke-static {v7, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 207
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->otherGamerscoreMonth:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isMeProfile()Z

    move-result v4

    if-nez v4, :cond_5

    move v4, v5

    :goto_3
    invoke-static {v7, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 209
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->achievementMeLayout:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 210
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->achievementYouLayout:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 211
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->rareAchievementMeLayout:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 212
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->rareAchievementYouLayout:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 214
    new-instance v4, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v7

    .line 216
    invoke-static {}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;->values()[Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;

    move-result-object v8

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-direct {v4, v7, v10, v8}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    .line 217
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    const v7, 0x7f03020a

    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->setDropDownViewResource(I)V

    .line 218
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->filterSpinner:Landroid/widget/Spinner;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->filterSpinnerAdapter:Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    invoke-virtual {v4, v7}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 219
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->filterSpinner:Landroid/widget/Spinner;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->getAchievementsFilter()Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel$PeopleHubAchievementsFilter;->ordinal()I

    move-result v7

    invoke-virtual {v4, v7}, Landroid/widget/Spinner;->setSelection(I)V

    .line 220
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->filterSpinner:Landroid/widget/Spinner;

    new-instance v7, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder$1;

    invoke-direct {v7, p0, p1, v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder$1;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;)V

    invoke-virtual {v4, v7}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 250
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->compareAchievementButton:Landroid/view/View;

    if-eqz v4, :cond_0

    .line 252
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->compareAchievementButton:Landroid/view/View;

    invoke-direct {p0, v4, v5}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->setCompareView(Landroid/view/View;Z)V

    .line 253
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->shareAchievementButton:Landroid/view/View;

    invoke-direct {p0, v4, v5}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->toggleShareButton(Landroid/view/View;Z)V

    .line 256
    :cond_0
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isMeProfile()Z

    move-result v4

    if-nez v4, :cond_1

    .line 258
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->compareAchievementButton:Landroid/view/View;

    invoke-direct {p0, v4, v6}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->setCompareView(Landroid/view/View;Z)V

    .line 259
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->shareAchievementButton:Landroid/view/View;

    invoke-direct {p0, v4, v6}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->toggleShareButton(Landroid/view/View;Z)V

    .line 261
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->comparisonHeader:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 262
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->otherGamertag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->getOtherGamerTag()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 264
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->getMeImageUrl()Ljava/lang/String;

    move-result-object v0

    .line 265
    .local v0, "meImageUrl":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->getOtherImageUrl()Ljava/lang/String;

    move-result-object v1

    .line 267
    .local v1, "otherImageUrl":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->meProfileImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v4, v0}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 268
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->otherProfileImage:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;)V

    .line 271
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->meGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->getMeGamerscore()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 272
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->otherGamerscore:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->getOtherGamerscore()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 274
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->meGamerscoreChange:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    new-array v8, v5, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->getMeGamerscoreChange()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v8, v6

    invoke-virtual {v7, v12, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 275
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->otherGamerscoreChange:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->getOtherGamerscoreChange()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v5, v6

    invoke-virtual {v7, v12, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 277
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->dateFormat:Ljava/text/SimpleDateFormat;

    invoke-static {}, Lcom/microsoft/xbox/service/model/leaderboards/GamerscoreLeaderboardModel;->getCurrentLeaderboardMonth()Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    .line 278
    .local v3, "thisMonth":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->meGamerscoreMonth:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v4, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 279
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->otherGamerscoreMonth:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v4, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 283
    .end local v0    # "meImageUrl":Ljava/lang/String;
    .end local v1    # "otherImageUrl":Ljava/lang/String;
    .end local v3    # "thisMonth":Ljava/lang/String;
    :cond_1
    return-void

    :cond_2
    move v4, v6

    .line 204
    goto/16 :goto_0

    :cond_3
    move v4, v6

    .line 205
    goto/16 :goto_1

    :cond_4
    move v4, v6

    .line 206
    goto/16 :goto_2

    :cond_5
    move v4, v6

    .line 207
    goto/16 :goto_3
.end method

.method static synthetic lambda$setCompareView$1(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;Landroid/view/View;)V
    .locals 6
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 318
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 321
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToViewProfile()Z

    move-result v2

    if-eqz v0, :cond_1

    .line 322
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->canViewOtherProfiles()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0704b5

    .line 323
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f07061c

    .line 324
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 320
    invoke-static {v2, v1, v3, v4}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showFailedPermissionsDialog(ZZLjava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 326
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->trackCompareWithFriendsAction()V

    .line 327
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->navigateToPeopleSelectorActivity()V

    .line 329
    :cond_0
    return-void

    .line 322
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$setCompareView$2(Landroid/view/View;)V
    .locals 3
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 336
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->GoBack()V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 340
    :goto_0
    return-void

    .line 337
    :catch_0
    move-exception v0

    .line 338
    .local v0, "e":Lcom/microsoft/xbox/toolkit/XLEException;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$200()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error attempting to goBack"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic lambda$toggleShareButton$0(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;Landroid/view/View;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 289
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->isMeProfile()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 290
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->shareMyLeaderboard()V

    .line 291
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->trackShareMyLeaderboard()V

    .line 297
    :goto_0
    return-void

    .line 293
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->getOtherXuid()Ljava/lang/String;

    move-result-object v0

    .line 294
    .local v0, "otherXuid":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;->access$000(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter;)Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/GamerscoreLeaderboardInsertViewModel;->shareLeaderboardCompare(Ljava/lang/String;)V

    .line 295
    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCPeopleHub;->trackShareLeaderboard(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setCompareView(Landroid/view/View;Z)V
    .locals 4
    .param p1, "compareControlView"    # Landroid/view/View;
    .param p2, "showCompare"    # Z

    .prologue
    .line 307
    const v2, 0x7f0e0115

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 308
    .local v0, "compareIcon":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    const v2, 0x7f0e0116

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 310
    .local v1, "compareLabel":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    if-eqz p2, :cond_0

    .line 311
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070f48

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 312
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0705bc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 314
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->compareAchievementButton:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 342
    :goto_0
    return-void

    .line 331
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070ef7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 332
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0705b2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 334
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->compareAchievementButton:Landroid/view/View;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder$$Lambda$3;->lambdaFactory$()Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private toggleShareButton(Landroid/view/View;Z)V
    .locals 3
    .param p1, "shareView"    # Landroid/view/View;
    .param p2, "showShareProgress"    # Z

    .prologue
    .line 286
    const v1, 0x7f0e0118

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 288
    .local v0, "shareLabel":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->shareAchievementButton:Landroid/view/View;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 299
    if-eqz p2, :cond_0

    .line 300
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0706a4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 304
    :goto_0
    return-void

    .line 302
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0706a3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public onBind(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;)V
    .locals 0
    .param p1, "dataObject"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 347
    return-void
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 155
    check-cast p1, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsListAdapter$AchievementsHeaderViewHolder;->onBind(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsCompositeDataItem;)V

    return-void
.end method
