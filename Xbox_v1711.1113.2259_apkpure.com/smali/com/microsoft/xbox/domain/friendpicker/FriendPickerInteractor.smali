.class public final Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;
.super Ljava/lang/Object;
.source "FriendPickerInteractor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;,
        Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final cancelTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final initialLoadTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final okTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V
    .locals 1
    .param p1, "peopleHubService"    # Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;
    .param p2, "userSummaryDataMapper"    # Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;
    .param p3, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-static {p1, p3, p2}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;->initialLoadTransformer:Lio/reactivex/ObservableTransformer;

    .line 73
    invoke-static {}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$$Lambda$2;->lambdaFactory$()Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;->okTransformer:Lio/reactivex/ObservableTransformer;

    .line 80
    invoke-static {}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$$Lambda$3;->lambdaFactory$()Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;->cancelTransformer:Lio/reactivex/ObservableTransformer;

    .line 86
    return-void
.end method

.method static synthetic lambda$new$11(Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 80
    invoke-static {}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$$Lambda$4;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v0

    .line 81
    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 80
    return-object v0
.end method

.method static synthetic lambda$new$3(Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "peopleHubService"    # Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "userSummaryDataMapper"    # Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;
    .param p3, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 58
    invoke-static {p0, p1, p2}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$$Lambda$10;->lambdaFactory$(Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 59
    invoke-virtual {p3, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 58
    return-object v0
.end method

.method static synthetic lambda$new$7(Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 73
    invoke-static {}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$$Lambda$7;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v0

    .line 74
    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 73
    return-object v0
.end method

.method static synthetic lambda$null$0(Ljava/util/ArrayList;Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;)V
    .locals 0
    .param p0, "collection"    # Ljava/util/ArrayList;
    .param p1, "item"    # Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 70
    sget-object v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$10(Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "action"    # Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 82
    invoke-static {p0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$$Lambda$5;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 83
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 84
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 82
    return-object v0
.end method

.method static synthetic lambda$null$2(Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;)Lio/reactivex/ObservableSource;
    .locals 3
    .param p0, "peopleHubService"    # Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;
    .param p1, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .param p2, "userSummaryDataMapper"    # Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;
    .param p3, "action"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 60
    invoke-interface {p0}, Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubService;->getFriends()Lio/reactivex/Single;

    move-result-object v0

    .line 61
    invoke-interface {p1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$$Lambda$11;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 62
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$$Lambda$12;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 64
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p2}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$$Lambda$13;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromPeopleHubPersonDataMapper;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 65
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$$Lambda$14;->lambdaFactory$()Lio/reactivex/functions/BiConsumer;

    move-result-object v2

    .line 66
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->collectInto(Ljava/lang/Object;Lio/reactivex/functions/BiConsumer;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$$Lambda$15;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 67
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$$Lambda$16;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 68
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p3}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$$Lambda$17;->lambdaFactory$(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 70
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 60
    return-object v0
.end method

.method static synthetic lambda$null$4(Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;
    .locals 1
    .param p0, "act"    # Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 76
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;->with(Ljava/lang/Object;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$5(Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 77
    sget-object v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$6(Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "action"    # Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 75
    invoke-static {p0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$$Lambda$8;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 76
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$$Lambda$9;->lambdaFactory$(Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 77
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 75
    return-object v0
.end method

.method static synthetic lambda$null$8(Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;
    .locals 1
    .param p0, "act"    # Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 83
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;->with(Ljava/lang/Object;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$9(Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 84
    sget-object v0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method


# virtual methods
.method public getCancelTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$CancelAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;->cancelTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public getInitialLoadTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;->initialLoadTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method

.method public getOkTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor$OkAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BasicResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/domain/friendpicker/FriendPickerInteractor;->okTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method
