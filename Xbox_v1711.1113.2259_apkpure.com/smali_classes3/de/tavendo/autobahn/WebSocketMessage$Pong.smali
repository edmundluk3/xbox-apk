.class public Lde/tavendo/autobahn/WebSocketMessage$Pong;
.super Lde/tavendo/autobahn/WebSocketMessage$Message;
.source "WebSocketMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lde/tavendo/autobahn/WebSocketMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Pong"
.end annotation


# instance fields
.field public mPayload:[B


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 210
    invoke-direct {p0}, Lde/tavendo/autobahn/WebSocketMessage$Message;-><init>()V

    .line 211
    const/4 v0, 0x0

    iput-object v0, p0, Lde/tavendo/autobahn/WebSocketMessage$Pong;->mPayload:[B

    .line 212
    return-void
.end method

.method constructor <init>([B)V
    .locals 0
    .param p1, "payload"    # [B

    .prologue
    .line 214
    invoke-direct {p0}, Lde/tavendo/autobahn/WebSocketMessage$Message;-><init>()V

    .line 215
    iput-object p1, p0, Lde/tavendo/autobahn/WebSocketMessage$Pong;->mPayload:[B

    .line 216
    return-void
.end method
