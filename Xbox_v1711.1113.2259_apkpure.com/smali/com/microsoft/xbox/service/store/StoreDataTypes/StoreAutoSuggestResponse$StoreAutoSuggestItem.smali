.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;
.super Ljava/lang/Object;
.source "StoreAutoSuggestResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StoreAutoSuggestItem"
.end annotation


# instance fields
.field private volatile transient hashCode:I

.field public final height:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Height"
    .end annotation
.end field

.field private final icon:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Icon"
    .end annotation
.end field

.field public final imageType:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ImageType"
    .end annotation
.end field

.field public final productId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ProductId"
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Title"
    .end annotation
.end field

.field public final type:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Type"
    .end annotation
.end field

.field public final width:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Width"
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "height"    # I
    .param p2, "icon"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "imageType"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "productId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "title"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "type"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p7, "width"    # I

    .prologue
    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    iput p1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->height:I

    .line 159
    iput-object p2, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->icon:Ljava/lang/String;

    .line 160
    iput-object p3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->imageType:Ljava/lang/String;

    .line 161
    iput-object p4, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->productId:Ljava/lang/String;

    .line 162
    iput-object p5, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->title:Ljava/lang/String;

    .line 163
    iput-object p6, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->type:Ljava/lang/String;

    .line 164
    iput p7, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->width:I

    .line 165
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 183
    if-ne p1, p0, :cond_1

    .line 189
    :cond_0
    :goto_0
    return v1

    .line 185
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestList;

    if-nez v3, :cond_2

    move v1, v2

    .line 186
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 188
    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;

    .line 189
    .local v0, "other":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;
    iget v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->height:I

    iget v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->height:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->icon:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->icon:Ljava/lang/String;

    .line 190
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->imageType:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->imageType:Ljava/lang/String;

    .line 191
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->productId:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->productId:Ljava/lang/String;

    .line 192
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->title:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->title:Ljava/lang/String;

    .line 193
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->type:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->type:Ljava/lang/String;

    .line 194
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->width:I

    iget v4, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->width:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getProductType()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 169
    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->type:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string v0, ""

    .line 170
    .local v0, "typeString":Ljava/lang/String;
    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;->fromBigCatTypeString(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    move-result-object v1

    return-object v1

    .line 169
    .end local v0    # "typeString":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->type:Ljava/lang/String;

    goto :goto_0
.end method

.method public getValidIconString()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->icon:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->icon:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->ensureHttpsPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 178
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 201
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->hashCode:I

    if-nez v0, :cond_0

    .line 202
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->hashCode:I

    .line 203
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->height:I

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->hashCode:I

    .line 204
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->icon:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCodeLowercase(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->hashCode:I

    .line 205
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->imageType:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCodeLowercase(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->hashCode:I

    .line 206
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->productId:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCodeLowercase(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->hashCode:I

    .line 207
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->title:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCodeLowercase(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->hashCode:I

    .line 208
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->type:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCodeLowercase(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->hashCode:I

    .line 209
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->width:I

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->hashCode:I

    .line 211
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreAutoSuggestResponse$StoreAutoSuggestItem;->hashCode:I

    return v0
.end method
