.class final Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;
.super Ljava/util/concurrent/atomic/AtomicReference;
.source "ReplayRelay.java"

# interfaces
.implements Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jakewharton/rxrelay2/ReplayRelay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "SizeAndTimeBoundReplayBuffer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicReference",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayBuffer",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x6fcd9699e42b76b5L


# instance fields
.field volatile head:Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode",
            "<TT;>;"
        }
    .end annotation
.end field

.field final maxAge:J

.field final maxSize:I

.field final scheduler:Lio/reactivex/Scheduler;

.field size:I

.field tail:Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode",
            "<TT;>;"
        }
    .end annotation
.end field

.field final unit:Ljava/util/concurrent/TimeUnit;


# direct methods
.method constructor <init>(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)V
    .locals 4
    .param p1, "maxSize"    # I
    .param p2, "maxAge"    # J
    .param p4, "unit"    # Ljava/util/concurrent/TimeUnit;
    .param p5, "scheduler"    # Lio/reactivex/Scheduler;

    .prologue
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;, "Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer<TT;>;"
    const-wide/16 v2, 0x0

    .line 688
    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 689
    if-gtz p1, :cond_0

    .line 690
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "maxSize > 0 required but it was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 692
    :cond_0
    cmp-long v1, p2, v2

    if-gtz v1, :cond_1

    .line 693
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "maxAge > 0 required but it was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 695
    :cond_1
    if-nez p4, :cond_2

    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "unit == null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 696
    :cond_2
    if-nez p5, :cond_3

    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "scheduler == null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 697
    :cond_3
    iput p1, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->maxSize:I

    .line 698
    iput-wide p2, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->maxAge:J

    .line 699
    iput-object p4, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->unit:Ljava/util/concurrent/TimeUnit;

    .line 700
    iput-object p5, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->scheduler:Lio/reactivex/Scheduler;

    .line 701
    new-instance v0, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;-><init>(Ljava/lang/Object;J)V

    .line 702
    .local v0, "h":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    iput-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->tail:Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;

    .line 703
    iput-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->head:Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;

    .line 704
    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 735
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;, "Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer<TT;>;"
    .local p1, "value":Ljava/lang/Object;, "TT;"
    new-instance v0, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;

    iget-object v2, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->scheduler:Lio/reactivex/Scheduler;

    iget-object v3, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->unit:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v3}, Lio/reactivex/Scheduler;->now(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    invoke-direct {v0, p1, v2, v3}, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;-><init>(Ljava/lang/Object;J)V

    .line 736
    .local v0, "n":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    iget-object v1, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->tail:Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;

    .line 738
    .local v1, "t":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    iput-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->tail:Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;

    .line 739
    iget v2, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->size:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->size:I

    .line 740
    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;->set(Ljava/lang/Object;)V

    .line 742
    invoke-virtual {p0}, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->trim()V

    .line 743
    return-void
.end method

.method public getValue()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 748
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;, "Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer<TT;>;"
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->head:Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;

    .line 751
    .local v0, "h":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    :goto_0
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;

    .line 752
    .local v1, "next":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    if-nez v1, :cond_0

    .line 758
    iget-object v2, v0, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;->value:Ljava/lang/Object;

    return-object v2

    .line 755
    :cond_0
    move-object v0, v1

    .line 756
    goto :goto_0
.end method

.method public getValues([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)[TT;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;, "Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer<TT;>;"
    .local p1, "array":[Ljava/lang/Object;, "[TT;"
    const/4 v5, 0x0

    .line 764
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->head:Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;

    .line 765
    .local v0, "h":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    invoke-virtual {p0}, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->size()I

    move-result v3

    .line 767
    .local v3, "s":I
    if-nez v3, :cond_1

    .line 768
    array-length v4, p1

    if-eqz v4, :cond_0

    .line 769
    const/4 v4, 0x0

    aput-object v5, p1, v4

    .line 788
    :cond_0
    :goto_0
    return-object p1

    .line 772
    :cond_1
    array-length v4, p1

    if-ge v4, v3, :cond_2

    .line 773
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v4

    invoke-static {v4, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    move-object p1, v4

    check-cast p1, [Ljava/lang/Object;

    .line 776
    :cond_2
    const/4 v1, 0x0

    .line 777
    .local v1, "i":I
    :goto_1
    if-eq v1, v3, :cond_3

    .line 778
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;

    .line 779
    .local v2, "next":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    iget-object v4, v2, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;->value:Ljava/lang/Object;

    aput-object v4, p1, v1

    .line 780
    add-int/lit8 v1, v1, 0x1

    .line 781
    move-object v0, v2

    .line 782
    goto :goto_1

    .line 783
    .end local v2    # "next":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    :cond_3
    array-length v4, p1

    if-le v4, v3, :cond_0

    .line 784
    aput-object v5, p1, v3

    goto :goto_0
.end method

.method public replay(Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 794
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;, "Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer<TT;>;"
    .local p1, "rs":Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;, "Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable<TT;>;"
    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->getAndIncrement()I

    move-result v10

    if-eqz v10, :cond_0

    .line 854
    :goto_0
    return-void

    .line 798
    :cond_0
    const/4 v4, 0x1

    .line 799
    .local v4, "missed":I
    iget-object v0, p1, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->actual:Lio/reactivex/Observer;

    .line 801
    .local v0, "a":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-TT;>;"
    iget-object v1, p1, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->index:Ljava/lang/Object;

    check-cast v1, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;

    .line 802
    .local v1, "index":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    if-nez v1, :cond_1

    .line 803
    iget-object v1, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->head:Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;

    .line 805
    iget-object v10, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->scheduler:Lio/reactivex/Scheduler;

    iget-object v11, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->unit:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v10, v11}, Lio/reactivex/Scheduler;->now(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v10

    iget-wide v12, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->maxAge:J

    sub-long v2, v10, v12

    .line 806
    .local v2, "limit":J
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;

    .line 807
    .local v6, "next":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    :goto_1
    if-eqz v6, :cond_1

    .line 808
    iget-wide v8, v6, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;->time:J

    .line 809
    .local v8, "ts":J
    cmp-long v10, v8, v2

    if-lez v10, :cond_2

    .line 819
    .end local v2    # "limit":J
    .end local v6    # "next":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    .end local v8    # "ts":J
    :cond_1
    iget-boolean v10, p1, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->cancelled:Z

    if-eqz v10, :cond_4

    .line 820
    const/4 v10, 0x0

    iput-object v10, p1, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->index:Ljava/lang/Object;

    goto :goto_0

    .line 812
    .restart local v2    # "limit":J
    .restart local v6    # "next":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    .restart local v8    # "ts":J
    :cond_2
    move-object v1, v6

    .line 813
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;->get()Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "next":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    check-cast v6, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;

    .line 814
    .restart local v6    # "next":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    goto :goto_1

    .line 836
    .end local v2    # "limit":J
    .end local v6    # "next":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    .end local v8    # "ts":J
    .local v5, "n":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    :cond_3
    iget-object v7, v5, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;->value:Ljava/lang/Object;

    .line 838
    .local v7, "o":Ljava/lang/Object;, "TT;"
    invoke-interface {v0, v7}, Lio/reactivex/Observer;->onNext(Ljava/lang/Object;)V

    .line 840
    move-object v1, v5

    .line 825
    .end local v5    # "n":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    .end local v7    # "o":Ljava/lang/Object;, "TT;"
    :cond_4
    iget-boolean v10, p1, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->cancelled:Z

    if-eqz v10, :cond_5

    .line 826
    const/4 v10, 0x0

    iput-object v10, p1, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->index:Ljava/lang/Object;

    goto :goto_0

    .line 830
    :cond_5
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;

    .line 832
    .restart local v5    # "n":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    if-nez v5, :cond_3

    .line 843
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;->get()Ljava/lang/Object;

    move-result-object v10

    if-nez v10, :cond_1

    .line 847
    iput-object v1, p1, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->index:Ljava/lang/Object;

    .line 849
    neg-int v10, v4

    invoke-virtual {p1, v10}, Lcom/jakewharton/rxrelay2/ReplayRelay$ReplayDisposable;->addAndGet(I)I

    move-result v4

    .line 850
    if-nez v4, :cond_1

    goto :goto_0
.end method

.method public size()I
    .locals 4

    .prologue
    .line 858
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;, "Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer<TT;>;"
    const/4 v2, 0x0

    .line 859
    .local v2, "s":I
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->head:Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;

    .line 860
    .local v0, "h":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    :goto_0
    const v3, 0x7fffffff

    if-eq v2, v3, :cond_0

    .line 861
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;

    .line 862
    .local v1, "next":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    if-nez v1, :cond_1

    .line 869
    .end local v1    # "next":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    :cond_0
    return v2

    .line 865
    .restart local v1    # "next":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 866
    move-object v0, v1

    .line 867
    goto :goto_0
.end method

.method trim()V
    .locals 8

    .prologue
    .line 707
    .local p0, "this":Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;, "Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer<TT;>;"
    iget v4, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->size:I

    iget v5, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->maxSize:I

    if-le v4, v5, :cond_0

    .line 708
    iget v4, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->size:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->size:I

    .line 709
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->head:Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;

    .line 710
    .local v0, "h":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;

    iput-object v4, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->head:Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;

    .line 712
    .end local v0    # "h":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    :cond_0
    iget-object v4, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->scheduler:Lio/reactivex/Scheduler;

    iget-object v5, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->unit:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v5}, Lio/reactivex/Scheduler;->now(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    iget-wide v6, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->maxAge:J

    sub-long v2, v4, v6

    .line 714
    .local v2, "limit":J
    iget-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->head:Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;

    .line 717
    .restart local v0    # "h":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    :goto_0
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;

    .line 718
    .local v1, "next":Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;, "Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode<TT;>;"
    if-nez v1, :cond_1

    .line 719
    iput-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->head:Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;

    .line 731
    :goto_1
    return-void

    .line 723
    :cond_1
    iget-wide v4, v1, Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;->time:J

    cmp-long v4, v4, v2

    if-lez v4, :cond_2

    .line 724
    iput-object v0, p0, Lcom/jakewharton/rxrelay2/ReplayRelay$SizeAndTimeBoundReplayBuffer;->head:Lcom/jakewharton/rxrelay2/ReplayRelay$TimedNode;

    goto :goto_1

    .line 728
    :cond_2
    move-object v0, v1

    .line 729
    goto :goto_0
.end method
