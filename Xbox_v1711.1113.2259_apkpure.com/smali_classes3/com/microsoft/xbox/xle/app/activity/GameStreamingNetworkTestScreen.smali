.class public Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "GameStreamingNetworkTestScreen.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;
.implements Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionStateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;,
        Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;
    }
.end annotation


# static fields
.field private static final BI_NETWORK_TEST:Ljava/lang/String; = "Network Test"

.field private static final CONNECT_TO_TEST_ID:I = 0x7f0705f9

.field private static final MINIMUM_BUILD_TO_TEST:I = 0x319f

.field private static final SIGNIN_TO_TEST_ID:I = 0x7f0705fa

.field private static final TAG:Ljava/lang/String; = "GameStreamingNetworkTestScreen"

.field private static final TEST_COMPLETE_ID:I = 0x7f0705fb

.field private static final TEST_IN_PROGRESS_ID:I = 0x7f0705fc


# instance fields
.field private mBroadcastConnectionManager:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;

.field private mCurrentError:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

.field private mCurrentTestState:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;

.field private mSpinner:Landroid/widget/ProgressBar;

.field private mStartButton:Landroid/widget/Button;

.field private mTestMessage:Landroid/widget/TextView;

.field private mTestTimeout:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->showTestStarted()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->showTestFinished()V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->showError(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;)V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;)Ljava/util/Timer;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mTestTimeout:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;Ljava/util/Timer;)Ljava/util/Timer;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;
    .param p1, "x1"    # Ljava/util/Timer;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mTestTimeout:Ljava/util/Timer;

    return-object p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;)Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mBroadcastConnectionManager:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;
    .param p1, "x1"    # Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->showErrorInternal(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;)V

    return-void
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->showTestFinishedInternal()V

    return-void
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->showTestStartedInternal()V

    return-void
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->resetInternal()V

    return-void
.end method

.method private reset()V
    .locals 2

    .prologue
    .line 238
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v0, v1, :cond_0

    .line 239
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$5;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$5;-><init>(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 249
    :goto_0
    return-void

    .line 247
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->resetInternal()V

    goto :goto_0
.end method

.method private resetInternal()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 252
    sget-object v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;->NO_ERROR:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mCurrentError:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    .line 253
    sget-object v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;->NOT_STARTED:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mCurrentTestState:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;

    .line 254
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mStartButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 255
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mTestMessage:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 256
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mTestMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 257
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mSpinner:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 258
    return-void
.end method

.method private showError(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;)V
    .locals 2
    .param p1, "error"    # Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    .prologue
    .line 152
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v0, v1, :cond_0

    .line 153
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$2;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$2;-><init>(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 163
    :goto_0
    return-void

    .line 161
    :cond_0
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->showErrorInternal(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;)V

    goto :goto_0
.end method

.method private showErrorInternal(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;)V
    .locals 3
    .param p1, "error"    # Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    .prologue
    const/16 v1, 0x8

    .line 166
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mTestTimeout:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mTestTimeout:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mStartButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 170
    sget-object v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;->ERROR:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mCurrentTestState:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;

    .line 171
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mCurrentError:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mSpinner:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mTestMessage:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 175
    sget-object v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$6;->$SwitchMap$com$microsoft$xbox$xle$app$activity$GameStreamingNetworkTestScreen$NetworkTestError:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 188
    :goto_0
    return-void

    .line 177
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mTestMessage:Landroid/widget/TextView;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0705f9

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 180
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mTestMessage:Landroid/widget/TextView;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0705fa

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 183
    :pswitch_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mTestMessage:Landroid/widget/TextView;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0705fb

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 175
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private showTestFinished()V
    .locals 2

    .prologue
    .line 191
    const-string v0, "GameStreamingNetworkTestScreen"

    const-string v1, "show Network Test Completed."

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v0, v1, :cond_0

    .line 193
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$3;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$3;-><init>(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 203
    :goto_0
    return-void

    .line 201
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->showTestFinishedInternal()V

    goto :goto_0
.end method

.method private showTestFinishedInternal()V
    .locals 3

    .prologue
    .line 206
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mTestTimeout:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mTestTimeout:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 209
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;->COMPLETED:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mCurrentTestState:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;

    .line 210
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mSpinner:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 211
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mTestMessage:Landroid/widget/TextView;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0705fb

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 212
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mTestMessage:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 213
    return-void
.end method

.method private showTestStarted()V
    .locals 2

    .prologue
    .line 216
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-eq v0, v1, :cond_0

    .line 217
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$4;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$4;-><init>(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 227
    :goto_0
    return-void

    .line 225
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->showTestStartedInternal()V

    goto :goto_0
.end method

.method private showTestStartedInternal()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 230
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mStartButton:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 231
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mTestMessage:Landroid/widget/TextView;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0705fc

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mTestMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 233
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mSpinner:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 234
    sget-object v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;->IN_PROGRESS:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mCurrentTestState:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;

    .line 235
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    const-string v0, "GameStreamingNetworkTest"

    return-object v0
.end method

.method public onBroadcastChannelConnectionStateChanged(Z)V
    .locals 0
    .param p1, "isConnected"    # Z

    .prologue
    .line 280
    return-void
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 76
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 77
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->onCreateContentView()V

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mBroadcastConnectionManager:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->addGameStreamStateListener(Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;)V

    .line 80
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->addCompanionSessionStateListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionStateListener;)V

    .line 82
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->reset()V

    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mStartButton:Landroid/widget/Button;

    new-instance v1, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$1;-><init>(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 67
    const v0, 0x7f030117

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->setContentView(I)V

    .line 68
    const v0, 0x7f0e063c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mStartButton:Landroid/widget/Button;

    .line 69
    const v0, 0x7f0e063d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mTestMessage:Landroid/widget/TextView;

    .line 70
    const v0, 0x7f0e063e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mSpinner:Landroid/widget/ProgressBar;

    .line 71
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mBroadcastConnectionManager:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;

    .line 72
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mTestTimeout:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mTestTimeout:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mBroadcastConnectionManager:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->getIsGameStreaming()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mBroadcastConnectionManager:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->stopGameStream()Z

    .line 146
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mBroadcastConnectionManager:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->removeGameStreamStateListener(Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$IGameStreamStateListener;)V

    .line 147
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->removeCompanionSessionStateListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionStateListener;)V

    .line 148
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onDestroy()V

    .line 149
    return-void
.end method

.method public onGameStreamEnabledChanged(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 275
    return-void
.end method

.method public onGameStreamError(Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;)V
    .locals 2
    .param p1, "error"    # Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamError;

    .prologue
    .line 334
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mCurrentTestState:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;

    sget-object v1, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;->IN_PROGRESS:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;

    if-ne v0, v1, :cond_0

    .line 335
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->showTestFinished()V

    .line 337
    :cond_0
    return-void
.end method

.method public onGameStreamStateChanged(Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;)V
    .locals 4
    .param p1, "state"    # Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    .prologue
    .line 262
    const-string v1, "GameStreamingNetworkTestScreen"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onGameStreamStateChanged: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " stream enabled: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->getIsGameStreamEnabled()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;->Started:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager$GameStreamState;

    if-ne p1, v1, :cond_0

    .line 264
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mBroadcastConnectionManager:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;

    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/SessionModel;->getCurrentConsole()Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/model/ConsoleData;->getIpAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->connectToStream(Ljava/lang/String;)Z

    move-result v0

    .line 265
    .local v0, "success":Z
    if-nez v0, :cond_0

    .line 266
    const-string v1, "GameStreamingNetworkTestScreen"

    const-string v2, "Failed to connect to stream"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->showTestFinished()V

    .line 270
    .end local v0    # "success":Z
    :cond_0
    return-void
.end method

.method public onNetworkTestCompleted(ZLjava/lang/String;)V
    .locals 3
    .param p1, "succeeded"    # Z
    .param p2, "jsonResult"    # Ljava/lang/String;

    .prologue
    .line 306
    const-string v0, "GameStreamingNetworkTestScreen"

    invoke-static {v0, p2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mBroadcastConnectionManager:Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->stopGameStream()Z

    .line 308
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->showTestFinished()V

    .line 309
    if-eqz p1, :cond_0

    .line 310
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Network Test"

    invoke-virtual {v0, v1, p2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->endTrackPerformance(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    :goto_0
    return-void

    .line 312
    :cond_0
    const-string v0, "Network Test"

    const-string v1, "failed"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->track(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onPairedIdentityStateChanged(Lcom/microsoft/xbox/smartglass/PairedIdentityState;Lcom/microsoft/xbox/smartglass/SGResult;)V
    .locals 2
    .param p1, "state"    # Lcom/microsoft/xbox/smartglass/PairedIdentityState;
    .param p2, "result"    # Lcom/microsoft/xbox/smartglass/SGResult;

    .prologue
    .line 297
    sget-object v0, Lcom/microsoft/xbox/smartglass/PairedIdentityState;->Paired:Lcom/microsoft/xbox/smartglass/PairedIdentityState;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mCurrentError:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    sget-object v1, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;->USER_NOT_PAIRED:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    if-ne v0, v1, :cond_1

    .line 298
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->reset()V

    .line 302
    :cond_0
    :goto_0
    return-void

    .line 299
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/smartglass/PairedIdentityState;->NotPaired:Lcom/microsoft/xbox/smartglass/PairedIdentityState;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mCurrentTestState:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;

    sget-object v1, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;->NOT_STARTED:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;

    if-ne v0, v1, :cond_0

    .line 300
    sget-object v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;->USER_NOT_PAIRED:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->showError(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;)V

    goto :goto_0
.end method

.method public onSessionStateChanged(ILcom/microsoft/xbox/toolkit/XLEException;)V
    .locals 2
    .param p1, "newSessionState"    # I
    .param p2, "exception"    # Lcom/microsoft/xbox/toolkit/XLEException;

    .prologue
    .line 284
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 285
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mCurrentError:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    sget-object v1, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;->CONSOLE_DISCONNECTED:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    if-ne v0, v1, :cond_0

    .line 286
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->reset()V

    .line 293
    :cond_0
    :goto_0
    return-void

    .line 289
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mCurrentTestState:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;

    sget-object v1, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;->IN_PROGRESS:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;

    if-ne v0, v1, :cond_0

    .line 290
    sget-object v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;->CONSOLE_DISCONNECTED:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->showError(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;)V

    goto :goto_0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 129
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onStart()V

    .line 131
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getCurrentSessionState()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 132
    sget-object v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;->CONSOLE_DISCONNECTED:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->showError(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;)V

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getCompanionSession()Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getPairedIdentityState()Lcom/microsoft/xbox/smartglass/PairedIdentityState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/smartglass/PairedIdentityState;->NotPaired:Lcom/microsoft/xbox/smartglass/PairedIdentityState;

    if-ne v0, v1, :cond_0

    .line 134
    sget-object v0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;->USER_NOT_PAIRED:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->showError(Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestError;)V

    goto :goto_0
.end method

.method public onStreamConnectionStateChanged(Z)V
    .locals 4
    .param p1, "isConnected"    # Z

    .prologue
    .line 318
    const-string v1, "GameStreamingNetworkTestScreen"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onStreamConnectionStateChanged: isConnected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    if-eqz p1, :cond_1

    .line 320
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/BroadcastConnectionManager;->startNetworkTest()Z

    move-result v0

    .line 321
    .local v0, "success":Z
    if-nez v0, :cond_0

    .line 322
    const-string v1, "GameStreamingNetworkTestScreen"

    const-string v2, "Failed to start network test"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->showTestFinished()V

    .line 330
    .end local v0    # "success":Z
    :cond_0
    :goto_0
    return-void

    .line 326
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->mCurrentTestState:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;

    sget-object v2, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;->IN_PROGRESS:Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen$NetworkTestState;

    if-ne v1, v2, :cond_0

    .line 327
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/GameStreamingNetworkTestScreen;->showTestFinished()V

    goto :goto_0
.end method
