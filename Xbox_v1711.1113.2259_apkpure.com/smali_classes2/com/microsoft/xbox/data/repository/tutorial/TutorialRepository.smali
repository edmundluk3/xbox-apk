.class public interface abstract Lcom/microsoft/xbox/data/repository/tutorial/TutorialRepository;
.super Ljava/lang/Object;
.source "TutorialRepository.java"


# virtual methods
.method public abstract getIsFirstRun()Z
.end method

.method public abstract isTutorialExperienceAllowed()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isTutorialExperienceEnabled()Z
.end method

.method public abstract isTutorialExperienceEnabledObservable()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setAppToNormalState()V
.end method

.method public abstract temporarilyForceDisableTutorialExperience()V
.end method
