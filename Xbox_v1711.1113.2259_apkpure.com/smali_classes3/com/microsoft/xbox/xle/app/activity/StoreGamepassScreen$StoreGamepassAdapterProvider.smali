.class public Lcom/microsoft/xbox/xle/app/activity/StoreGamepassScreen$StoreGamepassAdapterProvider;
.super Ljava/lang/Object;
.source "StoreGamepassScreen.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/activity/StoreGamepassScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "StoreGamepassAdapterProvider"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/activity/StoreGamepassScreen;


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/xle/app/activity/StoreGamepassScreen;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/activity/StoreGamepassScreen;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/StoreGamepassScreen$StoreGamepassAdapterProvider;->this$0:Lcom/microsoft/xbox/xle/app/activity/StoreGamepassScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAdapter(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 2
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 55
    move-object v0, p1

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;

    .line 56
    .local v0, "viewModel":Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 57
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getStoreItemsAdapter(Lcom/microsoft/xbox/xle/viewmodel/StoreItemsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    return-object v1
.end method
