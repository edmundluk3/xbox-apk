.class Lorg/simpleframework/xml/core/ScannerFactory;
.super Ljava/lang/Object;
.source "ScannerFactory.java"


# instance fields
.field private final cache:Lorg/simpleframework/xml/core/ScannerCache;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Lorg/simpleframework/xml/core/ScannerCache;

    invoke-direct {v0}, Lorg/simpleframework/xml/core/ScannerCache;-><init>()V

    iput-object v0, p0, Lorg/simpleframework/xml/core/ScannerFactory;->cache:Lorg/simpleframework/xml/core/ScannerCache;

    .line 47
    return-void
.end method


# virtual methods
.method public getInstance(Ljava/lang/Class;)Lorg/simpleframework/xml/core/Scanner;
    .locals 2
    .param p1, "type"    # Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 63
    iget-object v1, p0, Lorg/simpleframework/xml/core/ScannerFactory;->cache:Lorg/simpleframework/xml/core/ScannerCache;

    invoke-virtual {v1, p1}, Lorg/simpleframework/xml/core/ScannerCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/simpleframework/xml/core/Scanner;

    .line 65
    .local v0, "schema":Lorg/simpleframework/xml/core/Scanner;
    if-nez v0, :cond_0

    .line 66
    new-instance v0, Lorg/simpleframework/xml/core/Scanner;

    .end local v0    # "schema":Lorg/simpleframework/xml/core/Scanner;
    invoke-direct {v0, p1}, Lorg/simpleframework/xml/core/Scanner;-><init>(Ljava/lang/Class;)V

    .line 67
    .restart local v0    # "schema":Lorg/simpleframework/xml/core/Scanner;
    iget-object v1, p0, Lorg/simpleframework/xml/core/ScannerFactory;->cache:Lorg/simpleframework/xml/core/ScannerCache;

    invoke-virtual {v1, p1, v0}, Lorg/simpleframework/xml/core/ScannerCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    :cond_0
    return-object v0
.end method
