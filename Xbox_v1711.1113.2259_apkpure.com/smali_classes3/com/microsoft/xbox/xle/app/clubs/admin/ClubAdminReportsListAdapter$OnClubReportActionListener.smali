.class public interface abstract Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;
.super Ljava/lang/Object;
.source "ClubAdminReportsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnClubReportActionListener"
.end annotation


# virtual methods
.method public abstract banUser(JLjava/lang/String;)V
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
.end method

.method public abstract deleteItem(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V
.end method

.method public abstract ignoreReport(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V
.end method

.method public abstract navigateToItem(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportCommentListItem;)V
.end method

.method public abstract navigateToItem(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;)V
.end method

.method public abstract sendUserMessage(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
.end method
