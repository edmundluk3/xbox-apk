.class public Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;
.super Lcom/microsoft/xbox/toolkit/DialogManagerBase;
.source "SGProjectSpecificDialogManager.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;


# instance fields
.field private actionMenuDialog:Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

.field private changeFriendshipDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeFriendshipDialog;

.field private changeGamertagDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

.field private chooseGamerpicDialog:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

.field private chooseProfileColorDialog:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

.field private clubChatEditReportDialog:Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;

.field private clubChatTopicDialog:Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;

.field private clubModeratorPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;

.field private clubPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;

.field private connectDialog:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

.field private editRealNameDialog:Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

.field private editTextDialog:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

.field private enterIpAddressDialog:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;

.field private feedbackDialog:Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

.field private friendFinderCompletedAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

.field private friendFinderConfirmDialog:Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;

.field private friendFinderIsTaskCompleted:Z

.field private friendFinderNextStatus:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

.field private launchUrcRemote:Z

.field private lfgViewAllDetailDialog:Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;

.field private final navigationManager:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

.field private needToRestoreFriendFinderConfirmDialog:Z

.field private needToRestoreRemoteControl:Z

.field private peopleSelectorDialog:Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;

.field private prevStreamView:Landroid/view/SurfaceView;

.field private purchaseResultDialog:Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;

.field private remoteControl:Lcom/microsoft/xbox/xle/remote/RemoteControl;

.field private remoteControlStateToRestore:I

.field private renameConversationDialog:Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;

.field private shareDecisionDialog:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;

.field private starRatingDialog:Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;

.field private tagPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

.field private volumeDialog:Lcom/microsoft/xbox/xle/remote/VolumeDialog;

.field private webViewDialog:Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    const-class v0, Lcom/microsoft/xbox/toolkit/DialogManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->TAG:Ljava/lang/String;

    .line 97
    new-instance v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->instance:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/DialogManagerBase;-><init>()V

    .line 98
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->navigationManager:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    .line 126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->launchUrcRemote:Z

    .line 146
    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;
    .locals 1

    .prologue
    .line 134
    sget-object v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->instance:Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    return-object v0
.end method

.method public static getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;
    .locals 1

    .prologue
    .line 142
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    return-object v0
.end method

.method static synthetic lambda$showClubChatEditReportDialog$1(Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 801
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubChatEditReportDialog:Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;

    if-eqz v0, :cond_0

    .line 802
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubChatEditReportDialog:Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->setViewModel(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V

    .line 803
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubChatEditReportDialog:Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->updateDialogUI()V

    .line 804
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubChatEditReportDialog:Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 805
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubChatEditReportDialog:Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 810
    :goto_0
    return-void

    .line 807
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubChatEditReportDialog:Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;

    .line 808
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubChatEditReportDialog:Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0
.end method

.method static synthetic lambda$showClubChatTopicDialog$0(Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 766
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubChatTopicDialog:Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;

    if-eqz v0, :cond_0

    .line 767
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubChatTopicDialog:Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->setViewModel(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V

    .line 768
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubChatTopicDialog:Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->updateDialog()V

    .line 769
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubChatTopicDialog:Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 770
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubChatTopicDialog:Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 775
    :goto_0
    return-void

    .line 772
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubChatTopicDialog:Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;

    .line 773
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubChatTopicDialog:Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0
.end method

.method private showClubPicker(Ljava/util/List;ZLcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "showInviteSubtitle"    # Z
    .param p3, "doneHandler"    # Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;Z",
            "Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 705
    .local p1, "clubList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 706
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 708
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;

    if-eqz v0, :cond_0

    .line 709
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;->setClubList(Ljava/util/List;)V

    .line 710
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;->setDoneHandler(Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;)V

    .line 711
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;->setShowInviteSubtitle(Z)V

    .line 712
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 713
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 719
    :goto_0
    return-void

    .line 715
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1, p3}, Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;

    .line 716
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;->setShowInviteSubtitle(Z)V

    .line 717
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0
.end method

.method public static showFailedPermissionsDialog(ZZLjava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p0, "hasPrivilege"    # Z
    .param p1, "hasPrivacySetting"    # Z
    .param p2, "suspendString"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "disabledString"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    const v6, 0x7f0707c7

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 868
    if-nez p0, :cond_1

    .line 869
    if-nez p1, :cond_0

    .line 870
    sget-object v1, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->TAG:Ljava/lang/String;

    const-string v2, "Showing missing privilege dialog"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 871
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f07061d

    .line 872
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 874
    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 871
    invoke-interface {v1, v2, p3, v3, v7}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 889
    :goto_0
    return v0

    .line 877
    :cond_0
    sget-object v2, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->TAG:Ljava/lang/String;

    const-string v3, "Showing enforcement dialog"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 878
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0704a4

    .line 879
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f0704a3

    .line 881
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v0, [Ljava/lang/Object;

    aput-object p2, v5, v1

    .line 880
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    .line 883
    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 878
    invoke-interface {v2, v3, v1, v4, v7}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 889
    goto :goto_0
.end method

.method private showTagPickerDialog(Ljava/util/List;ILcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "maxTags"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3, "tagsSelectedHandler"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4, "pivotState"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
            ">;I",
            "Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;",
            "Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;",
            ")V"
        }
    .end annotation

    .prologue
    .line 583
    .local p1, "selectedTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 584
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 585
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 587
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->tagPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->tagPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->getPivotState()Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->tagPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->setMaxTags(I)V

    .line 589
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->tagPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 590
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->tagPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 596
    :goto_0
    return-void

    .line 592
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1, p3, p4}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->tagPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    .line 593
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->tagPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;->setMaxTags(I)V

    .line 594
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->tagPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0
.end method

.method private trackPageView(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "pageName"    # Ljava/lang/String;
    .param p2, "content"    # Ljava/lang/String;

    .prologue
    .line 963
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->navigationManager:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 964
    .local v0, "currentScreen":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    if-nez v0, :cond_0

    const-string v1, ""

    .line 965
    .local v1, "fromPageName":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v2

    invoke-virtual {v2, v1, p1, p2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageView(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 966
    return-void

    .line 964
    .end local v1    # "fromPageName":Ljava/lang/String;
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public dismissChangeFriendshipDialog()V
    .locals 1

    .prologue
    .line 505
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeFriendshipDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeFriendshipDialog;

    if-eqz v0, :cond_0

    .line 506
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeFriendshipDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeFriendshipDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 507
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeFriendshipDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeFriendshipDialog;

    .line 509
    :cond_0
    return-void
.end method

.method public dismissChangeGamertagDialog()V
    .locals 1

    .prologue
    .line 672
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeGamertagDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    if-eqz v0, :cond_0

    .line 673
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeGamertagDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 674
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeGamertagDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    .line 676
    :cond_0
    return-void
.end method

.method public dismissChooseColorProfileDialog()V
    .locals 1

    .prologue
    .line 558
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->chooseProfileColorDialog:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    if-eqz v0, :cond_0

    .line 559
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->chooseProfileColorDialog:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 560
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->chooseProfileColorDialog:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    .line 562
    :cond_0
    return-void
.end method

.method public dismissChooseGamerpicDialog()V
    .locals 1

    .prologue
    .line 648
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->chooseGamerpicDialog:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    if-eqz v0, :cond_0

    .line 649
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->chooseGamerpicDialog:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 650
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->chooseGamerpicDialog:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    .line 652
    :cond_0
    return-void
.end method

.method public dismissClubChatEditReportDialog()V
    .locals 1

    .prologue
    .line 814
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubChatEditReportDialog:Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;

    if-eqz v0, :cond_0

    .line 815
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubChatEditReportDialog:Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 816
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubChatEditReportDialog:Lcom/microsoft/xbox/xle/app/clubs/ClubChatEditReportDialog;

    .line 818
    :cond_0
    return-void
.end method

.method public dismissClubChatTopicDialog()V
    .locals 1

    .prologue
    .line 779
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubChatTopicDialog:Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;

    if-eqz v0, :cond_0

    .line 780
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubChatTopicDialog:Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 781
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubChatTopicDialog:Lcom/microsoft/xbox/xle/app/clubs/ClubChatTopicDialog;

    .line 783
    :cond_0
    return-void
.end method

.method public dismissClubModeratorPickerDialog()V
    .locals 1

    .prologue
    .line 744
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubModeratorPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;

    if-eqz v0, :cond_0

    .line 745
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubModeratorPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 746
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubModeratorPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;

    .line 748
    :cond_0
    return-void
.end method

.method public dismissClubPickerDialog()V
    .locals 1

    .prologue
    .line 722
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;

    if-eqz v0, :cond_0

    .line 723
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 724
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog;

    .line 726
    :cond_0
    return-void
.end method

.method public dismissConnectDialog()V
    .locals 2

    .prologue
    .line 168
    const-string v0, "DialogManager"

    const-string v1, "Dismiss connect dialog"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->connectDialog:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->connectDialog:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 171
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->connectDialog:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    .line 173
    :cond_0
    return-void
.end method

.method public dismissEditDialog()V
    .locals 1

    .prologue
    .line 630
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editTextDialog:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    if-eqz v0, :cond_0

    .line 631
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editTextDialog:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 632
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editTextDialog:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    .line 634
    :cond_0
    return-void
.end method

.method public dismissEditRealNameDialog()V
    .locals 1

    .prologue
    .line 498
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editRealNameDialog:Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    if-eqz v0, :cond_0

    .line 499
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editRealNameDialog:Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 500
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editRealNameDialog:Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    .line 502
    :cond_0
    return-void
.end method

.method public dismissEnterIpAddressDialog()V
    .locals 2

    .prologue
    .line 188
    const-string v0, "DialogManager"

    const-string v1, "Dismiss EnterIpAddress dialog"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->enterIpAddressDialog:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->enterIpAddressDialog:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 191
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->enterIpAddressDialog:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;

    .line 193
    :cond_0
    return-void
.end method

.method public dismissFeedbackDialog()V
    .locals 2

    .prologue
    .line 341
    const-string v0, "DialogManager"

    const-string v1, "Dismiss feedback dialog"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->feedbackDialog:Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->feedbackDialog:Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 344
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->feedbackDialog:Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    .line 346
    :cond_0
    return-void
.end method

.method public dismissFriendFinderConfirmDialog()V
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->friendFinderConfirmDialog:Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;

    if-eqz v0, :cond_0

    .line 529
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->friendFinderConfirmDialog:Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 530
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->friendFinderConfirmDialog:Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;

    .line 532
    :cond_0
    return-void
.end method

.method public dismissLfgViewAllDetailDialog()V
    .locals 1

    .prologue
    .line 835
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->lfgViewAllDetailDialog:Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;

    if-eqz v0, :cond_0

    .line 836
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->lfgViewAllDetailDialog:Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 837
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->lfgViewAllDetailDialog:Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;

    .line 839
    :cond_0
    return-void
.end method

.method public dismissPurchaseResultDialog()V
    .locals 2

    .prologue
    .line 320
    const-string v0, "DialogManager"

    const-string v1, "dismiss purchase result dialog"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->purchaseResultDialog:Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->purchaseResultDialog:Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 323
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->purchaseResultDialog:Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;

    .line 325
    :cond_0
    return-void
.end method

.method public dismissRemoteControl()V
    .locals 2

    .prologue
    .line 269
    const-string v0, "DialogManager"

    const-string v1, "dismiss remote control"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getStayAwakeSetting()Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;->OnlyRemote:Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

    if-ne v0, v1, :cond_0

    .line 271
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setKeepScreenOn(Z)V

    .line 273
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->remoteControl:Lcom/microsoft/xbox/xle/remote/RemoteControl;

    if-eqz v0, :cond_1

    .line 274
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->remoteControl:Lcom/microsoft/xbox/xle/remote/RemoteControl;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 275
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->remoteControl:Lcom/microsoft/xbox/xle/remote/RemoteControl;

    .line 280
    :cond_1
    return-void
.end method

.method public dismissRenameConversationDialog()V
    .locals 1

    .prologue
    .line 429
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->renameConversationDialog:Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;

    if-eqz v0, :cond_0

    .line 430
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->renameConversationDialog:Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 431
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->renameConversationDialog:Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;

    .line 433
    :cond_0
    return-void
.end method

.method public dismissShareDecisionDialog()V
    .locals 2

    .prologue
    .line 482
    const-string v0, "DialogManager"

    const-string v1, "Dismiss share decision dialog"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->shareDecisionDialog:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;

    if-eqz v0, :cond_0

    .line 484
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->shareDecisionDialog:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 485
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->shareDecisionDialog:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;

    .line 487
    :cond_0
    return-void
.end method

.method public dismissStarRatingDialog()V
    .locals 2

    .prologue
    .line 297
    const-string v0, "DialogManager"

    const-string v1, "dismiss star rating dialog"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->starRatingDialog:Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->starRatingDialog:Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 300
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->starRatingDialog:Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;

    .line 302
    :cond_0
    return-void
.end method

.method public dismissTagPickerDialog()V
    .locals 1

    .prologue
    .line 599
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->tagPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    if-eqz v0, :cond_0

    .line 600
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->tagPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 601
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->tagPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog;

    .line 603
    :cond_0
    return-void
.end method

.method public dismissVolumeDialog()V
    .locals 2

    .prologue
    .line 213
    const-string v0, "DialogManager"

    const-string v1, "dismiss dialog"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->volumeDialog:Lcom/microsoft/xbox/xle/remote/VolumeDialog;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->volumeDialog:Lcom/microsoft/xbox/xle/remote/VolumeDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 216
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->volumeDialog:Lcom/microsoft/xbox/xle/remote/VolumeDialog;

    .line 221
    :cond_0
    return-void
.end method

.method public dismissWebViewDialog()V
    .locals 1

    .prologue
    .line 690
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->webViewDialog:Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;

    if-eqz v0, :cond_0

    .line 691
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->webViewDialog:Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 692
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->webViewDialog:Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;

    .line 694
    :cond_0
    return-void
.end method

.method public dissmissPeoplePickerDialog()V
    .locals 2

    .prologue
    .line 490
    const-string v0, "DialogManager"

    const-string v1, "People picker dialog dismissed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->peopleSelectorDialog:Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;

    if-eqz v0, :cond_0

    .line 492
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->peopleSelectorDialog:Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 493
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->peopleSelectorDialog:Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;

    .line 495
    :cond_0
    return-void
.end method

.method public forceDismissAll()V
    .locals 1

    .prologue
    .line 896
    invoke-super {p0}, Lcom/microsoft/xbox/toolkit/DialogManagerBase;->forceDismissAll()V

    .line 899
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->actionMenuDialog:Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    if-eqz v0, :cond_0

    .line 900
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->actionMenuDialog:Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->dismiss()V

    .line 901
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->actionMenuDialog:Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    .line 904
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissConnectDialog()V

    .line 905
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissEnterIpAddressDialog()V

    .line 906
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissVolumeDialog()V

    .line 907
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissRemoteControl()V

    .line 908
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissStarRatingDialog()V

    .line 909
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissPurchaseResultDialog()V

    .line 910
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissFeedbackDialog()V

    .line 911
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissShareDecisionDialog()V

    .line 912
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissChooseColorProfileDialog()V

    .line 913
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissChooseGamerpicDialog()V

    .line 914
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissChangeGamertagDialog()V

    .line 915
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissTagPickerDialog()V

    .line 916
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissWebViewDialog()V

    .line 917
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissClubPickerDialog()V

    .line 918
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissClubModeratorPickerDialog()V

    .line 919
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissClubChatTopicDialog()V

    .line 920
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissLfgViewAllDetailDialog()V

    .line 921
    return-void
.end method

.method public getActionMenuDialog()Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;
    .locals 2

    .prologue
    .line 846
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->actionMenuDialog:Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    if-nez v0, :cond_0

    .line 847
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->actionMenuDialog:Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    .line 849
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->actionMenuDialog:Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    return-object v0
.end method

.method public notifyChangeFriendshipDialogAsyncTaskCompleted()V
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeFriendshipDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeFriendshipDialog;

    if-eqz v0, :cond_0

    .line 447
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeFriendshipDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeFriendshipDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChangeFriendshipDialog;->reportAsyncTaskCompleted()V

    .line 449
    :cond_0
    return-void
.end method

.method public notifyChangeFriendshipDialogAsyncTaskFailed(Ljava/lang/String;)V
    .locals 1
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 452
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeFriendshipDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeFriendshipDialog;

    if-eqz v0, :cond_0

    .line 453
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeFriendshipDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeFriendshipDialog;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ChangeFriendshipDialog;->reportAsyncTaskFailed(Ljava/lang/String;)V

    .line 455
    :cond_0
    return-void
.end method

.method public notifyChangeFriendshipDialogUpdateView()V
    .locals 3

    .prologue
    .line 438
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeFriendshipDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeFriendshipDialog;

    if-eqz v0, :cond_0

    .line 439
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeFriendshipDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeFriendshipDialog;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/dialog/ChangeFriendshipDialog;->updateView(ZLjava/lang/String;)V

    .line 441
    :cond_0
    return-void
.end method

.method public notifyEditRealNameDialogAsyncTaskCompleted()V
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editRealNameDialog:Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    if-eqz v0, :cond_0

    .line 459
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editRealNameDialog:Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->reportAsyncTaskCompleted()V

    .line 461
    :cond_0
    return-void
.end method

.method public notifyEditRealNameDialogAsyncTaskFailed()V
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editRealNameDialog:Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    if-eqz v0, :cond_0

    .line 465
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editRealNameDialog:Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->reportAsyncTaskFailed()V

    .line 467
    :cond_0
    return-void
.end method

.method public notifyFriendFinderConfirmDialogAsyncTaskCompleted(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;)V
    .locals 1
    .param p1, "taskType"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    .prologue
    .line 535
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->friendFinderConfirmDialog:Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;

    if-eqz v0, :cond_0

    .line 536
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->friendFinderConfirmDialog:Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->reportAsyncTaskCompleted(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;)V

    .line 538
    :cond_0
    return-void
.end method

.method public notifyFriendFinderConfirmDialogAsyncTaskFailed(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;)V
    .locals 1
    .param p1, "taskType"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    .prologue
    .line 541
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->friendFinderConfirmDialog:Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->friendFinderConfirmDialog:Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->reportAsyncTaskFailed(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;)V

    .line 544
    :cond_0
    return-void
.end method

.method public notifyPeoplePickerDialogAsyncTaskCompleted()V
    .locals 1

    .prologue
    .line 470
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->peopleSelectorDialog:Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;

    if-eqz v0, :cond_0

    .line 471
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->peopleSelectorDialog:Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->reportAsyncTaskCompleted()V

    .line 473
    :cond_0
    return-void
.end method

.method public notifyPeoplePickerDialogAsyncTaskFailed(Ljava/lang/String;)V
    .locals 1
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 476
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->peopleSelectorDialog:Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;

    if-eqz v0, :cond_0

    .line 477
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->peopleSelectorDialog:Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->reportAsyncTaskFailed(Ljava/lang/String;)V

    .line 479
    :cond_0
    return-void
.end method

.method public onApplicationPause()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 935
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->remoteControl:Lcom/microsoft/xbox/xle/remote/RemoteControl;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->needToRestoreRemoteControl:Z

    .line 936
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->remoteControl:Lcom/microsoft/xbox/xle/remote/RemoteControl;

    if-eqz v0, :cond_0

    .line 937
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->remoteControl:Lcom/microsoft/xbox/xle/remote/RemoteControl;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->getCurrentState()I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->remoteControlStateToRestore:I

    .line 940
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->friendFinderConfirmDialog:Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;

    if-eqz v0, :cond_3

    :goto_1
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->needToRestoreFriendFinderConfirmDialog:Z

    .line 941
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->friendFinderConfirmDialog:Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;

    if-eqz v0, :cond_1

    .line 942
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->setShouldNavigateToSuggestionList(Z)V

    .line 943
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->friendFinderConfirmDialog:Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->getOptInStatus()Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->friendFinderNextStatus:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    .line 944
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->friendFinderConfirmDialog:Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->getAsyncTaskType()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->friendFinderCompletedAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    .line 945
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->friendFinderConfirmDialog:Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->getIsTaskCompleted()Z

    move-result v0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->friendFinderIsTaskCompleted:Z

    .line 947
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->forceDismissAll()V

    .line 948
    return-void

    :cond_2
    move v0, v2

    .line 935
    goto :goto_0

    :cond_3
    move v1, v2

    .line 940
    goto :goto_1
.end method

.method public onApplicationResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 952
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->needToRestoreRemoteControl:Z

    if-eqz v0, :cond_0

    .line 953
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showRemoteControl()V

    .line 954
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->needToRestoreRemoteControl:Z

    .line 956
    :cond_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->needToRestoreFriendFinderConfirmDialog:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->isPostingToFacebook()Z

    move-result v0

    if-nez v0, :cond_1

    .line 957
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->friendFinderNextStatus:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->friendFinderCompletedAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->friendFinderIsTaskCompleted:Z

    invoke-virtual {p0, v0, v1, v2}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showFriendFinderConfirmDialog(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;Z)V

    .line 958
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->needToRestoreFriendFinderConfirmDialog:Z

    .line 960
    :cond_1
    return-void
.end method

.method public setShouldLaunchUrcRemote(Z)V
    .locals 0
    .param p1, "urcLaunch"    # Z

    .prologue
    .line 225
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->launchUrcRemote:Z

    .line 226
    return-void
.end method

.method protected shouldDismissAllBeforeOpeningADialog()Z
    .locals 1

    .prologue
    .line 930
    const/4 v0, 0x0

    return v0
.end method

.method public showChangeFriendshipDialog(Lcom/microsoft/xbox/xle/viewmodel/ChangeFriendshipDialogViewModel;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
    .locals 2
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ChangeFriendshipDialogViewModel;
    .param p2, "previousVM"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 395
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeFriendshipDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeFriendshipDialog;

    if-eqz v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeFriendshipDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeFriendshipDialog;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ChangeFriendshipDialog;->setVm(Lcom/microsoft/xbox/xle/viewmodel/ChangeFriendshipDialogViewModel;)V

    .line 397
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeFriendshipDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeFriendshipDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChangeFriendshipDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 398
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeFriendshipDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeFriendshipDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 403
    :goto_0
    return-void

    .line 400
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/ChangeFriendshipDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2}, Lcom/microsoft/xbox/xle/app/dialog/ChangeFriendshipDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/ChangeFriendshipDialogViewModel;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeFriendshipDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeFriendshipDialog;

    .line 401
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeFriendshipDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeFriendshipDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0
.end method

.method public showChangeGamertagDialog(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)V
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    .prologue
    .line 655
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeGamertagDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    if-eqz v0, :cond_0

    .line 656
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeGamertagDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->setViewModel(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)V

    .line 657
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeGamertagDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 658
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeGamertagDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 663
    :goto_0
    return-void

    .line 660
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeGamertagDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    .line 661
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeGamertagDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0
.end method

.method public showChooseColorProfileDialog(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)V
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    .prologue
    .line 547
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->chooseProfileColorDialog:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    if-eqz v0, :cond_0

    .line 548
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->chooseProfileColorDialog:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->setProfileColorContainer(Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;)V

    .line 549
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->chooseProfileColorDialog:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 550
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->chooseProfileColorDialog:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 555
    :goto_0
    return-void

    .line 552
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog$ProfileColorContainer;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->chooseProfileColorDialog:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    .line 553
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->chooseProfileColorDialog:Lcom/microsoft/xbox/xle/app/dialog/ChooseProfileColorDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0
.end method

.method public showChooseGamerpicDialog(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)V
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;

    .prologue
    .line 637
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->chooseGamerpicDialog:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    if-eqz v0, :cond_0

    .line 638
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->chooseGamerpicDialog:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->setViewModel(Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)V

    .line 639
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->chooseGamerpicDialog:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 640
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->chooseGamerpicDialog:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 645
    :goto_0
    return-void

    .line 642
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/CustomizeProfileScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->chooseGamerpicDialog:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    .line 643
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->chooseGamerpicDialog:Lcom/microsoft/xbox/xle/app/dialog/ChooseGamerpicDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0
.end method

.method public showClubChatEditReportDialog(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V
    .locals 5
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 786
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 792
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->navigationManager:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->bringAppToForeground()Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

    move-result-object v0

    .line 794
    .local v0, "result":Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;
    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;->AlreadyInForeground:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

    if-ne v0, v1, :cond_0

    const-wide/16 v2, 0x0

    .line 798
    .local v2, "showDelayMs":J
    :goto_0
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v2, v3, v1}, Lio/reactivex/Completable;->timer(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Completable;

    move-result-object v1

    .line 799
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v4

    invoke-virtual {v1, v4}, Lio/reactivex/Completable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object v1

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)Lio/reactivex/functions/Action;

    move-result-object v4

    .line 800
    invoke-virtual {v1, v4}, Lio/reactivex/Completable;->subscribe(Lio/reactivex/functions/Action;)Lio/reactivex/disposables/Disposable;

    .line 811
    return-void

    .line 794
    .end local v2    # "showDelayMs":J
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;->BroughtToForeground:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

    if-ne v0, v1, :cond_1

    const-wide/16 v2, 0x1f4

    goto :goto_0

    :cond_1
    const-wide/16 v2, 0x7d0

    goto :goto_0
.end method

.method public showClubChatTopicDialog(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)V
    .locals 5
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 751
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 757
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->navigationManager:Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->bringAppToForeground()Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

    move-result-object v0

    .line 759
    .local v0, "result":Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;
    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;->AlreadyInForeground:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

    if-ne v0, v1, :cond_0

    const-wide/16 v2, 0x0

    .line 763
    .local v2, "showDelayMs":J
    :goto_0
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v2, v3, v1}, Lio/reactivex/Completable;->timer(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Completable;

    move-result-object v1

    .line 764
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v4

    invoke-virtual {v1, v4}, Lio/reactivex/Completable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Completable;

    move-result-object v1

    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;)Lio/reactivex/functions/Action;

    move-result-object v4

    .line 765
    invoke-virtual {v1, v4}, Lio/reactivex/Completable;->subscribe(Lio/reactivex/functions/Action;)Lio/reactivex/disposables/Disposable;

    .line 776
    return-void

    .line 759
    .end local v2    # "showDelayMs":J
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;->BroughtToForeground:Lcom/microsoft/xbox/toolkit/ui/NavigationManager$AppToForegroundResult;

    if-ne v0, v1, :cond_1

    const-wide/16 v2, 0x1f4

    goto :goto_0

    :cond_1
    const-wide/16 v2, 0x7d0

    goto :goto_0
.end method

.method public showClubModeratorPickerDialog(Ljava/util/List;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2    # Lcom/microsoft/xbox/toolkit/generics/Action;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 729
    .local p1, "moderators":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    .local p2, "doneHandler":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 730
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 732
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubModeratorPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;

    if-eqz v0, :cond_0

    .line 733
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubModeratorPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;->setModerators(Ljava/util/List;)V

    .line 734
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubModeratorPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;->setDoneHandler(Lcom/microsoft/xbox/toolkit/generics/Action;)V

    .line 735
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubModeratorPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 736
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubModeratorPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 741
    :goto_0
    return-void

    .line 738
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2}, Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubModeratorPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;

    .line 739
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->clubModeratorPickerDialog:Lcom/microsoft/xbox/xle/app/dialog/ClubModeratorPickerDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0
.end method

.method public showClubPickerDialog(Ljava/util/List;Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "doneHandler"    # Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;",
            "Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 701
    .local p1, "clubList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showClubPicker(Ljava/util/List;ZLcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;)V

    .line 702
    return-void
.end method

.method public showClubTagPickerDialog(Ljava/util/List;Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "tagsSelectedHandler"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "pivotState"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
            ">;",
            "Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;",
            "Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;",
            ")V"
        }
    .end annotation

    .prologue
    .line 575
    .local p1, "selectedTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;>;"
    const/16 v0, 0xa

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showTagPickerDialog(Ljava/util/List;ILcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;)V

    .line 576
    return-void
.end method

.method public showConnectDialog()V
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showConnectDialog(Ljava/lang/Runnable;)V

    .line 150
    return-void
.end method

.method public showConnectDialog(Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "onConnectHandler"    # Ljava/lang/Runnable;

    .prologue
    const/4 v2, 0x0

    .line 153
    const-string v0, "Connection - Connection View"

    invoke-direct {p0, v0, v2}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->trackPageView(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Connection"

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->connectDialog:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    if-eqz v0, :cond_0

    .line 156
    const-string v0, "DialogManager"

    const-string v1, "connect dialog was not dismissed correctly !"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->connectDialog:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->setOnConnectHandler(Ljava/lang/Runnable;)V

    .line 158
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->connectDialog:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->connectDialog:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 165
    :goto_0
    return-void

    .line 161
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->connectDialog:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    .line 162
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->connectDialog:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->setOnConnectHandler(Ljava/lang/Runnable;)V

    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->connectDialog:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0
.end method

.method public showEditRealNameDialog(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)V
    .locals 2
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    .prologue
    .line 406
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editRealNameDialog:Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    if-eqz v0, :cond_0

    .line 407
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editRealNameDialog:Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->setVm(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)V

    .line 408
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editRealNameDialog:Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 409
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editRealNameDialog:Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 415
    :goto_0
    return-void

    .line 411
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editRealNameDialog:Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    .line 412
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editRealNameDialog:Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 413
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editRealNameDialog:Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0
.end method

.method public showEditTextDialog(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ILcom/microsoft/xbox/xle/app/dialog/EditTextDialog$OnEditTextCompletedHandler;)V
    .locals 8
    .param p1, "headerText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "initialText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "maxCharacterLength"    # I
        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p4, "hintText"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "color"    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param
    .param p6, "editTextCompletedHandler"    # Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$OnEditTextCompletedHandler;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 611
    invoke-static {p6}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 612
    const-wide/16 v0, 0x1

    int-to-long v2, p3

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 614
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editTextDialog:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    if-eqz v0, :cond_0

    .line 615
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editTextDialog:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->setHeaderText(Ljava/lang/String;)V

    .line 616
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editTextDialog:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->setInitialText(Ljava/lang/String;)V

    .line 617
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editTextDialog:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->setMaxCharacterLength(I)V

    .line 618
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editTextDialog:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    invoke-virtual {v0, p4}, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->setHintText(Ljava/lang/String;)V

    .line 619
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editTextDialog:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    invoke-virtual {v0, p5}, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->setColor(I)V

    .line 620
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editTextDialog:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    invoke-virtual {v0, p6}, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->setEditCompletionHander(Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog$OnEditTextCompletedHandler;)V

    .line 621
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editTextDialog:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 622
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editTextDialog:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 627
    :goto_0
    return-void

    .line 624
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ILcom/microsoft/xbox/xle/app/dialog/EditTextDialog$OnEditTextCompletedHandler;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editTextDialog:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    .line 625
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->editTextDialog:Lcom/microsoft/xbox/xle/app/dialog/EditTextDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0
.end method

.method public showEnterIpAddressDialog()V
    .locals 3

    .prologue
    .line 176
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Manual IP"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->enterIpAddressDialog:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;

    if-eqz v0, :cond_0

    .line 178
    const-string v0, "DialogManager"

    const-string v1, "EnterIpAddress dialog was not dismissed correctly !"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->enterIpAddressDialog:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 180
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->enterIpAddressDialog:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 185
    :goto_0
    return-void

    .line 182
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->enterIpAddressDialog:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;

    .line 183
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->enterIpAddressDialog:Lcom/microsoft/xbox/xle/app/dialog/EnterIPAddressDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0
.end method

.method public showFeedbackDialog(Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;)V
    .locals 2
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;

    .prologue
    .line 330
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->feedbackDialog:Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    if-eqz v0, :cond_0

    .line 331
    const-string v0, "DialogManager"

    const-string v1, "feedback dialog was not dismissed correctly !"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->feedbackDialog:Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 333
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->feedbackDialog:Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 338
    :goto_0
    return-void

    .line 335
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/FeedbackViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->feedbackDialog:Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    .line 336
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->feedbackDialog:Lcom/microsoft/xbox/xle/app/dialog/FeedbackDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0
.end method

.method public showFriendFinderConfirmDialog(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V
    .locals 2
    .param p1, "nextStatus"    # Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    .prologue
    .line 512
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;->NONE:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showFriendFinderConfirmDialog(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;Z)V

    .line 513
    return-void
.end method

.method public showFriendFinderConfirmDialog(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;Z)V
    .locals 2
    .param p1, "nextStatus"    # Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;
    .param p2, "taskType"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;
    .param p3, "isCompleted"    # Z

    .prologue
    .line 516
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->friendFinderConfirmDialog:Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;

    if-eqz v0, :cond_0

    .line 517
    const-string v0, "DialogManager"

    const-string v1, "friend finder dialog was not dismissed correctly !"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->friendFinderConfirmDialog:Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;

    invoke-virtual {v0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->reset(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;Z)V

    .line 519
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->friendFinderConfirmDialog:Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 520
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->friendFinderConfirmDialog:Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 525
    :goto_0
    return-void

    .line 522
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;Z)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->friendFinderConfirmDialog:Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;

    .line 523
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->friendFinderConfirmDialog:Lcom/microsoft/xbox/xle/app/dialog/FriendFinderConfirmDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0
.end method

.method public showInviteClubPickerDialog(Ljava/util/List;Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p2, "doneHandler"    # Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;",
            "Lcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 697
    .local p1, "clubList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showClubPicker(Ljava/util/List;ZLcom/microsoft/xbox/xle/app/dialog/ClubPickerDialog$OnClubsSelectedHandler;)V

    .line 698
    return-void
.end method

.method public showLfgViewAllDetailDialog(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)V
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 821
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 823
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->lfgViewAllDetailDialog:Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;

    if-eqz v0, :cond_0

    .line 824
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->lfgViewAllDetailDialog:Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->setViewModel(Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)V

    .line 825
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->lfgViewAllDetailDialog:Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->updateDialog()V

    .line 826
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->lfgViewAllDetailDialog:Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 827
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->lfgViewAllDetailDialog:Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 832
    :goto_0
    return-void

    .line 829
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->lfgViewAllDetailDialog:Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;

    .line 830
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->lfgViewAllDetailDialog:Lcom/microsoft/xbox/xle/app/lfg/LfgViewAllDetailDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0
.end method

.method public showPeoplePickerDialog(Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;I)V
    .locals 1
    .param p1, "vmWithPeopleSelectorControl"    # Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;
    .param p2, "choiceMode"    # I

    .prologue
    .line 376
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showPeoplePickerDialog(Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;ILjava/lang/String;)V

    .line 377
    return-void
.end method

.method public showPeoplePickerDialog(Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;ILjava/lang/String;)V
    .locals 2
    .param p1, "vmWithPeopleSelectorControl"    # Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;
    .param p2, "choiceMode"    # I
    .param p3, "title"    # Ljava/lang/String;

    .prologue
    .line 382
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCComparePeoplePicker;->trackComparePeoplePickerView()V

    .line 384
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->peopleSelectorDialog:Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->peopleSelectorDialog:Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->setVmWithPeopleSelectorControl(Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;)V

    .line 386
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->peopleSelectorDialog:Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 387
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->peopleSelectorDialog:Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 392
    :goto_0
    return-void

    .line 389
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->peopleSelectorDialog:Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;

    .line 390
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->peopleSelectorDialog:Lcom/microsoft/xbox/xle/app/dialog/PeopleSelectorDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0
.end method

.method public showPeoplePickerDialog(Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;Ljava/lang/String;)V
    .locals 1
    .param p1, "vmWithPeopleSelectorControl"    # Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 372
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, p2}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showPeoplePickerDialog(Lcom/microsoft/xbox/xle/viewmodel/IPeopleSelectorControl;ILjava/lang/String;)V

    .line 373
    return-void
.end method

.method public showProviderPickerDialogScreen(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 6
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    const/4 v3, -0x1

    .line 856
    const/4 v2, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showProviderPickerDialogScreen(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;IIZ)V

    .line 857
    return-void
.end method

.method public showProviderPickerDialogScreen(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;IIZ)V
    .locals 8
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .param p2, "onClickListener"    # Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;
    .param p3, "msgRid"    # I
    .param p4, "errorRid"    # I
    .param p5, "displayAirings"    # Z

    .prologue
    const/4 v7, 0x1

    .line 860
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getActionMenuDialog()Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v6

    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/activity/ProviderPickerDialogScreen;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;IIZ)V

    invoke-virtual {v6, v0}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->setScreen(Landroid/view/View;)V

    .line 861
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getActionMenuDialog()Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->makeFullScreen(Z)Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->makeTransparent(Z)Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->show()V

    .line 862
    return-void
.end method

.method public showPurchaseResultDialog(Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)V
    .locals 5
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;

    .prologue
    .line 305
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v1

    .line 306
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getPurchaseOriginatingPage()Ljava/lang/String;

    move-result-object v0

    .line 307
    .local v0, "originatingPage":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v2

    const-string v3, "PurchaseResult"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v0, v4}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    const-string v2, "DialogManager"

    const-string/jumbo v3, "show purchase result dialog"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->purchaseResultDialog:Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;

    if-eqz v2, :cond_0

    .line 310
    const-string v2, "DialogManager"

    const-string/jumbo v3, "the purchase result dialog was not dismissed correctly, investgate!"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->purchaseResultDialog:Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    .line 312
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->purchaseResultDialog:Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 317
    :goto_0
    return-void

    .line 314
    :cond_0
    new-instance v2, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-direct {v2, v3, p1}, Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/PurchaseWebViewActivityViewModel;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->purchaseResultDialog:Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;

    .line 315
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->purchaseResultDialog:Lcom/microsoft/xbox/xle/app/dialog/PurchaseResultDialog;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0
.end method

.method public showRemoteControl()V
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 230
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivityName()Ljava/lang/String;

    move-result-object v2

    .line 231
    .local v2, "pageUri":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->getInstance()Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/XBoxOneCompanionSession;->getTitleIdWithFocus()I

    move-result v7

    .line 232
    .local v7, "titleId":I
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v3, v5

    invoke-static {v0, v1, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 233
    .local v4, "relativeId":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/MainActivity;->isPaused()Z

    move-result v0

    if-nez v0, :cond_0

    .line 234
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Remote"

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 240
    .end local v2    # "pageUri":Ljava/lang/String;
    .end local v4    # "relativeId":Ljava/lang/String;
    .end local v7    # "titleId":I
    :cond_0
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getStayAwakeSetting()Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;->OnlyRemote:Lcom/microsoft/xbox/xle/viewmodel/StayAwakeSettings;

    if-ne v0, v1, :cond_1

    .line 241
    invoke-static {v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setKeepScreenOn(Z)V

    .line 244
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->remoteControl:Lcom/microsoft/xbox/xle/remote/RemoteControl;

    if-eqz v0, :cond_3

    .line 245
    const-string v0, "DialogManager"

    const-string v1, "remote control already exist"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->remoteControl:Lcom/microsoft/xbox/xle/remote/RemoteControl;

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->launchUrcRemote:Z

    if-eqz v0, :cond_2

    move v0, v8

    :goto_1
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->setStartingState(I)V

    .line 247
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->remoteControl:Lcom/microsoft/xbox/xle/remote/RemoteControl;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 266
    :goto_2
    return-void

    :cond_2
    move v0, v9

    .line 246
    goto :goto_1

    .line 251
    :cond_3
    new-instance v0, Lcom/microsoft/xbox/xle/remote/RemoteControl;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/remote/RemoteControl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->remoteControl:Lcom/microsoft/xbox/xle/remote/RemoteControl;

    .line 252
    const/4 v6, 0x0

    .line 253
    .local v6, "newState":I
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->needToRestoreRemoteControl:Z

    if-eqz v0, :cond_4

    .line 254
    iget v6, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->remoteControlStateToRestore:I

    .line 255
    iput-boolean v9, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->needToRestoreRemoteControl:Z

    .line 256
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->remoteControlStateToRestore:I

    .line 260
    :goto_3
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->remoteControl:Lcom/microsoft/xbox/xle/remote/RemoteControl;

    invoke-virtual {v0, v6}, Lcom/microsoft/xbox/xle/remote/RemoteControl;->setStartingState(I)V

    .line 261
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->remoteControl:Lcom/microsoft/xbox/xle/remote/RemoteControl;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_2

    .line 258
    :cond_4
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->launchUrcRemote:Z

    if-eqz v0, :cond_5

    move v6, v8

    :goto_4
    goto :goto_3

    :cond_5
    move v6, v9

    goto :goto_4

    .line 236
    .end local v6    # "newState":I
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public showRenameConversationDialog(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)V
    .locals 2
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;

    .prologue
    .line 418
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->renameConversationDialog:Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;

    if-eqz v0, :cond_0

    .line 419
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->renameConversationDialog:Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->setVm(Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)V

    .line 420
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->renameConversationDialog:Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 421
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->renameConversationDialog:Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 426
    :goto_0
    return-void

    .line 423
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/viewmodel/ConversationDetailsActivityViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->renameConversationDialog:Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;

    .line 424
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->renameConversationDialog:Lcom/microsoft/xbox/xle/app/dialog/RenameConversationDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0
.end method

.method public showShareDecisionDialog(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)V
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p2, "itemRoot"    # Ljava/lang/String;
    .param p3, "itemType"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .prologue
    .line 349
    invoke-static {}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;->allTargets()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showShareDecisionDialog(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;Ljava/util/List;)V

    .line 350
    return-void
.end method

.method public showShareDecisionDialog(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;Ljava/util/List;)V
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p2, "itemRoot"    # Ljava/lang/String;
    .param p3, "itemType"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 353
    .local p4, "shareTargets":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog$ShareTarget;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->shareDecisionDialog:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;

    if-eqz v0, :cond_0

    .line 354
    const-string v0, "DialogManager"

    const-string/jumbo v1, "share decision dialog was not dismissed correctly !"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->shareDecisionDialog:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->setViewModel(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 356
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->shareDecisionDialog:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->setItemRoot(Ljava/lang/String;)V

    .line 357
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->shareDecisionDialog:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->setItemType(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)V

    .line 358
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->shareDecisionDialog:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;

    invoke-virtual {v0, p4}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->setValidShareTargets(Ljava/util/List;)V

    .line 359
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->shareDecisionDialog:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 360
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->shareDecisionDialog:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 369
    :goto_0
    return-void

    .line 362
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->shareDecisionDialog:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;

    .line 363
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->shareDecisionDialog:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->setViewModel(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 364
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->shareDecisionDialog:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->setItemRoot(Ljava/lang/String;)V

    .line 365
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->shareDecisionDialog:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->setItemType(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)V

    .line 366
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->shareDecisionDialog:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;

    invoke-virtual {v0, p4}, Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;->setValidShareTargets(Ljava/util/List;)V

    .line 367
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->shareDecisionDialog:Lcom/microsoft/xbox/xle/app/dialog/ShareDecisionDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0
.end method

.method public showStarRatingDialog(Ljava/lang/String;I)V
    .locals 3
    .param p1, "contentTitle"    # Ljava/lang/String;
    .param p2, "previousRating"    # I

    .prologue
    .line 283
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "StarRating"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    const-string v0, "DialogManager"

    const-string/jumbo v1, "show star rating dialog"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->starRatingDialog:Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;

    if-eqz v0, :cond_0

    .line 286
    const-string v0, "DialogManager"

    const-string/jumbo v1, "the star rating dialog was not dismissed correctly, investgate!"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->starRatingDialog:Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 288
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->starRatingDialog:Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 294
    :goto_0
    return-void

    .line 290
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->starRatingDialog:Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;

    .line 291
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->starRatingDialog:Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;

    invoke-virtual {v0, p2}, Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;->setSelectedRating(I)V

    .line 292
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->starRatingDialog:Lcom/microsoft/xbox/xle/app/dialog/StarRatingDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0
.end method

.method public showTagPickerDialog(Ljava/util/List;Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "tagsSelectedHandler"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "pivotState"    # Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;",
            ">;",
            "Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;",
            "Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;",
            ")V"
        }
    .end annotation

    .prologue
    .line 568
    .local p1, "selectedTags":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;>;"
    const/16 v0, 0xf

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showTagPickerDialog(Ljava/util/List;ILcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$OnTagsSelectedHandler;Lcom/microsoft/xbox/xle/app/dialog/TagPickerDialog$PivotState;)V

    .line 569
    return-void
.end method

.method public showVolumeDialog()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 196
    const-string v0, "Volume"

    invoke-direct {p0, v0, v2}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->trackPageView(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Volume"

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    const-string v0, "DialogManager"

    const-string/jumbo v1, "show volume dialog"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->volumeDialog:Lcom/microsoft/xbox/xle/remote/VolumeDialog;

    if-eqz v0, :cond_0

    .line 200
    const-string v0, "DialogManager"

    const-string/jumbo v1, "the volume dialog was not dismissed correctly, investgate!"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->volumeDialog:Lcom/microsoft/xbox/xle/remote/VolumeDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/remote/VolumeDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 202
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->volumeDialog:Lcom/microsoft/xbox/xle/remote/VolumeDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 210
    :goto_0
    return-void

    .line 204
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/remote/VolumeDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/remote/VolumeDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->volumeDialog:Lcom/microsoft/xbox/xle/remote/VolumeDialog;

    .line 205
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->volumeDialog:Lcom/microsoft/xbox/xle/remote/VolumeDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0
.end method

.method public showWebViewDialog(Ljava/lang/String;)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 679
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->webViewDialog:Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;

    if-eqz v0, :cond_0

    .line 680
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->webViewDialog:Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;->setUrl(Ljava/lang/String;)V

    .line 681
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->webViewDialog:Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 682
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->webViewDialog:Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyDialogListeners(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    .line 687
    :goto_0
    return-void

    .line 684
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->webViewDialog:Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;

    .line 685
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->webViewDialog:Lcom/microsoft/xbox/xle/app/dialog/WebViewDialog;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->addManagedDialog(Lcom/microsoft/xbox/toolkit/IXLEManagedDialog;)V

    goto :goto_0
.end method

.method public updateChangeGamertagDialog()V
    .locals 1

    .prologue
    .line 666
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeGamertagDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    if-eqz v0, :cond_0

    .line 667
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->changeGamertagDialog:Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/ChangeGamertagDialog;->updateDialog()V

    .line 669
    :cond_0
    return-void
.end method
