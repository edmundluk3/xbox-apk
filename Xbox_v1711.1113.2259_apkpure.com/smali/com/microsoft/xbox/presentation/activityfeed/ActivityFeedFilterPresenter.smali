.class public Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;
.super Lcom/microsoft/xbox/presentation/base/MviPresenter;
.source "ActivityFeedFilterPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/presentation/base/MviPresenter",
        "<",
        "Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterView;",
        "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
        "Lcom/microsoft/xbox/presentation/lce/LceViewState",
        "<",
        "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final intentToStateTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            "Lcom/microsoft/xbox/presentation/lce/LceViewState",
            "<",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;",
            ">;>;"
        }
    .end annotation
.end field

.field private final schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V
    .locals 1
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;
    .param p2, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/microsoft/xbox/presentation/base/MviPresenter;-><init>()V

    .line 36
    iput-object p2, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 38
    invoke-static {p0, p1}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;->intentToStateTransformer:Lio/reactivex/ObservableTransformer;

    .line 58
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)Lcom/microsoft/xbox/presentation/lce/LceViewState;
    .locals 1

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;->stateReducer(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)Lcom/microsoft/xbox/presentation/lce/LceViewState;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$initialSetup$8(Lcom/microsoft/xbox/presentation/lce/LceViewState;)V
    .locals 2
    .param p0, "state"    # Lcom/microsoft/xbox/presentation/lce/LceViewState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 67
    sget-object v0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;->TAG:Ljava/lang/String;

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/presentation/lce/LceViewState;)Lcom/microsoft/xbox/toolkit/logging/Loggable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logRender(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/logging/Loggable;)V

    return-void
.end method

.method static synthetic lambda$new$6(Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;
    .param p2, "intents"    # Lio/reactivex/Observable;

    .prologue
    .line 39
    invoke-static {p0, p1}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;)Lio/reactivex/functions/Function;

    move-result-object v0

    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 57
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 39
    return-object v0
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .locals 1
    .param p0, "initialLoadIntent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 43
    sget-object v0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;->INSTANCE:Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;

    return-object v0
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;)Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    .locals 1
    .param p0, "initialLoadResult"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->content:Ljava/lang/Object;

    check-cast v0, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    return-object v0
.end method

.method static synthetic lambda$null$2(Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$PrefsChangedIntent;)Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$PrefsChangedAction;
    .locals 1
    .param p0, "prefsChangedIntent"    # Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$PrefsChangedIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$PrefsChangedIntent;->prefs()Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$PrefsChangedAction;->with(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$PrefsChangedAction;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$3(Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$ApplyIntent;)Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$ApplyAction;
    .locals 1
    .param p0, "applyIntent"    # Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$ApplyIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$ApplyIntent;->prefs()Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$ApplyAction;->with(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor$ApplyAction;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$4(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 4
    .param p0, "interactor"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;
    .param p1, "shared"    # Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 41
    const-class v0, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;

    .line 42
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter$$Lambda$9;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 43
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 44
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;->getInitialLoadTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter$$Lambda$10;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 45
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$PrefsChangedIntent;

    .line 47
    invoke-virtual {p1, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter$$Lambda$11;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v2

    .line 48
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 49
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;->getPrefsChangedTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter$$Lambda$12;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v2

    .line 50
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterViewIntents$ApplyIntent;

    .line 52
    invoke-virtual {p1, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter$$Lambda$13;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 53
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 54
    invoke-virtual {p0}, Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;->getApplyActionTransformer()Lio/reactivex/ObservableTransformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-static {}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter$$Lambda$14;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v3

    .line 55
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 41
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$5(Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;
    .param p1, "interactor"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;
    .param p2, "intent"    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 39
    invoke-static {p2}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 40
    invoke-interface {v1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->computation()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p1}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter$$Lambda$8;->lambdaFactory$(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterInteractor;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 41
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->publish(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 39
    return-object v0
.end method

.method static synthetic lambda$null$7(Lcom/microsoft/xbox/presentation/lce/LceViewState;)Ljava/lang/String;
    .locals 1
    .param p0, "state"    # Lcom/microsoft/xbox/presentation/lce/LceViewState;

    .prologue
    .line 67
    invoke-static {p0}, Lcom/microsoft/xbox/presentation/lce/DefaultLceViewStateLogger;->toLogString(Lcom/microsoft/xbox/presentation/lce/LceViewState;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private stateReducer(Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;)Lcom/microsoft/xbox/presentation/lce/LceViewState;
    .locals 1
    .param p1, "prefs"    # Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;",
            ")",
            "Lcom/microsoft/xbox/presentation/lce/LceViewState",
            "<",
            "Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    invoke-static {p1}, Lcom/microsoft/xbox/presentation/lce/LceViewState;->withContent(Ljava/lang/Object;)Lcom/microsoft/xbox/presentation/lce/LceViewState;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected initialSetup()V
    .locals 3

    .prologue
    .line 62
    invoke-static {}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter$$Lambda$2;->lambdaFactory$()Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;

    move-result-object v1

    .line 63
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;->bindViewIntents(Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewIntentBinder;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;->INSTANCE:Lcom/microsoft/xbox/presentation/common/CommonViewIntents$InitialLoadIntent;

    .line 64
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;->intentToStateTransformer:Lio/reactivex/ObservableTransformer;

    .line 65
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v1

    .line 66
    invoke-virtual {v1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 67
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 68
    invoke-interface {v2}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->main()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 70
    .local v0, "viewStates":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lcom/microsoft/xbox/presentation/lce/LceViewState<Lcom/microsoft/xbox/domain/activityfeed/filter/ActivityFeedFilterPrefs;>;>;"
    invoke-static {}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter$$Lambda$4;->lambdaFactory$()Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/presentation/activityfeed/ActivityFeedFilterPresenter;->bindViewState(Lio/reactivex/Observable;Lcom/microsoft/xbox/presentation/base/MviPresenter$ViewStateConsumer;)V

    .line 71
    return-void
.end method
