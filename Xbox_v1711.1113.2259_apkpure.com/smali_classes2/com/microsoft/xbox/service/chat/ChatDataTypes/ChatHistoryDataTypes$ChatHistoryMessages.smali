.class public final Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;
.super Ljava/lang/Object;
.source "ChatHistoryDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ChatHistoryMessages"
.end annotation


# instance fields
.field public final continuationToken:J

.field private volatile transient hashCode:I

.field private final messages:Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(JLjava/util/List;)V
    .locals 1
    .param p1, "continuationToken"    # J
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p3, "messages":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 38
    iput-wide p1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;->continuationToken:J

    .line 39
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;->messages:Ljava/util/List;

    .line 40
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 49
    if-ne p1, p0, :cond_1

    .line 55
    :cond_0
    :goto_0
    return v1

    .line 51
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;

    if-nez v3, :cond_2

    move v1, v2

    .line 52
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 54
    check-cast v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;

    .line 55
    .local v0, "other":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;
    iget-wide v4, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;->continuationToken:J

    iget-wide v6, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;->continuationToken:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;->messages:Ljava/util/List;

    iget-object v4, v0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;->messages:Ljava/util/List;

    .line 56
    invoke-interface {v3, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getMessages()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$HistoryMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;->messages:Ljava/util/List;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 62
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;->hashCode:I

    if-nez v0, :cond_0

    .line 63
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;->hashCode:I

    .line 64
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;->continuationToken:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;->hashCode:I

    .line 65
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;->messages:Ljava/util/List;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;->hashCode:I

    .line 68
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHistoryDataTypes$ChatHistoryMessages;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
