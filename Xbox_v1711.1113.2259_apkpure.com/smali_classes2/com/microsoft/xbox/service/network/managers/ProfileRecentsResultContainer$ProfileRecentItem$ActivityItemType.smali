.class public final enum Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;
.super Ljava/lang/Enum;
.source "ProfileRecentsResultContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ActivityItemType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

.field public static final enum Achievement:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

.field public static final enum BroadcastEnd:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

.field public static final enum BroadcastStart:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

.field public static final enum Container:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

.field public static final enum Followed:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

.field public static final enum GameDVR:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

.field public static final enum GamertagChanged:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

.field public static final enum Leaderboard:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

.field public static final enum LegacyAchievement:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

.field public static final enum Lfg:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

.field public static final enum Listened:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

.field public static final enum Played:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

.field public static final enum Screenshot:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

.field public static final enum SocialRecommendation:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

.field public static final enum TextPost:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

.field public static final enum Tournament:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

.field public static final enum Unknown:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

.field public static final enum UserPost:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

.field public static final enum Watched:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 38
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    const-string v1, "Achievement"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Achievement:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .line 39
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    const-string v1, "Played"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Played:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .line 40
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    const-string v1, "Watched"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Watched:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .line 41
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    const-string v1, "Listened"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Listened:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .line 42
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    const-string v1, "GameDVR"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->GameDVR:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .line 43
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    const-string v1, "Followed"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Followed:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .line 44
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    const-string v1, "BroadcastStart"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->BroadcastStart:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .line 45
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    const-string v1, "BroadcastEnd"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->BroadcastEnd:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .line 46
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    const-string v1, "TextPost"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->TextPost:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .line 47
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    const-string v1, "GamertagChanged"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->GamertagChanged:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .line 48
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    const-string v1, "Screenshot"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Screenshot:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .line 49
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    const-string v1, "SocialRecommendation"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->SocialRecommendation:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .line 50
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    const-string v1, "Container"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Container:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .line 51
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    const-string v1, "UserPost"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->UserPost:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .line 52
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    const-string v1, "LegacyAchievement"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->LegacyAchievement:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .line 53
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    const-string v1, "Leaderboard"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Leaderboard:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .line 54
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    const-string v1, "Lfg"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Lfg:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .line 55
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    const-string v1, "Tournament"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Tournament:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .line 56
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    const-string v1, "Unknown"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Unknown:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .line 37
    const/16 v0, 0x13

    new-array v0, v0, [Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Achievement:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Played:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Watched:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Listened:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->GameDVR:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Followed:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->BroadcastStart:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->BroadcastEnd:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->TextPost:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->GamertagChanged:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Screenshot:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->SocialRecommendation:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Container:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->UserPost:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->LegacyAchievement:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Leaderboard:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Lfg:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Tournament:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Unknown:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    const-class v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    return-object v0
.end method
