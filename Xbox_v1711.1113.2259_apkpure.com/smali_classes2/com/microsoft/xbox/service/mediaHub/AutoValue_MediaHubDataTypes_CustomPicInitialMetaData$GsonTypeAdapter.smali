.class public final Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicInitialMetaData$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_MediaHubDataTypes_CustomPicInitialMetaData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicInitialMetaData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;",
        ">;"
    }
.end annotation


# instance fields
.field private final associationIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final customPicTypeAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;",
            ">;"
        }
    .end annotation
.end field

.field private defaultAssociationId:J

.field private defaultCustomPicType:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 2
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicInitialMetaData$GsonTypeAdapter;->defaultCustomPicType:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    .line 23
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicInitialMetaData$GsonTypeAdapter;->defaultAssociationId:J

    .line 25
    const-class v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicInitialMetaData$GsonTypeAdapter;->customPicTypeAdapter:Lcom/google/gson/TypeAdapter;

    .line 26
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicInitialMetaData$GsonTypeAdapter;->associationIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 27
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;
    .locals 6
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v4

    sget-object v5, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v4, v5, :cond_0

    .line 54
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 55
    const/4 v4, 0x0

    .line 81
    :goto_0
    return-object v4

    .line 57
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 58
    iget-object v1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicInitialMetaData$GsonTypeAdapter;->defaultCustomPicType:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    .line 59
    .local v1, "customPicType":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;
    iget-wide v2, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicInitialMetaData$GsonTypeAdapter;->defaultAssociationId:J

    .line 60
    .local v2, "associationId":J
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 61
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 62
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v4

    sget-object v5, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v4, v5, :cond_1

    .line 63
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 66
    :cond_1
    const/4 v4, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v4, :pswitch_data_0

    .line 76
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 66
    :sswitch_0
    const-string v5, "customPicType"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x0

    goto :goto_2

    :sswitch_1
    const-string v5, "associationId"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x1

    goto :goto_2

    .line 68
    :pswitch_0
    iget-object v4, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicInitialMetaData$GsonTypeAdapter;->customPicTypeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v4, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "customPicType":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;
    check-cast v1, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    .line 69
    .restart local v1    # "customPicType":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;
    goto :goto_1

    .line 72
    :pswitch_1
    iget-object v4, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicInitialMetaData$GsonTypeAdapter;->associationIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v4, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 73
    goto :goto_1

    .line 80
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 81
    new-instance v4, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicInitialMetaData;

    invoke-direct {v4, v1, v2, v3}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicInitialMetaData;-><init>(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;J)V

    goto :goto_0

    .line 66
    :sswitch_data_0
    .sparse-switch
        0x1a265b33 -> :sswitch_0
        0x6c03c97c -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicInitialMetaData$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultAssociationId(J)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicInitialMetaData$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultAssociationId"    # J

    .prologue
    .line 33
    iput-wide p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicInitialMetaData$GsonTypeAdapter;->defaultAssociationId:J

    .line 34
    return-object p0
.end method

.method public setDefaultCustomPicType(Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;)Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicInitialMetaData$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultCustomPicType"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicInitialMetaData$GsonTypeAdapter;->defaultCustomPicType:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    .line 30
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;)V
    .locals 4
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39
    if-nez p2, :cond_0

    .line 40
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 49
    :goto_0
    return-void

    .line 43
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 44
    const-string v0, "customPicType"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicInitialMetaData$GsonTypeAdapter;->customPicTypeAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;->customPicType()Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicType;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 46
    const-string v0, "associationId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicInitialMetaData$GsonTypeAdapter;->associationIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;->associationId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 48
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    check-cast p2, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_CustomPicInitialMetaData$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$CustomPicInitialMetaData;)V

    return-void
.end method
