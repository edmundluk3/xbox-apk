.class public Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "XboxAuthActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(I)V

    .line 24
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity;->actualSetActive()V

    return-void
.end method

.method private actualSetActive()V
    .locals 1

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity;->getIsActive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onSetActive()V

    .line 94
    :cond_0
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    const-string v0, "Splash"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 28
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 30
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity;->onCreateContentView()V

    .line 32
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/XboxAuthActivityViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 34
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity$1;-><init>(Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity;->setOnLayoutChangedListener(Ljava/lang/Runnable;)V

    .line 41
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity;->setDesiredDrawerLockState(I)V

    .line 42
    return-void
.end method

.method public onCreateContentView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 52
    const v0, 0x7f030163

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity;->setContentView(I)V

    .line 55
    const/4 v0, -0x1

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity;->setAppBarLayout(IZZ)V

    .line 56
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity;->setShowUtilityBar(Z)V

    .line 57
    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity;->setShowRightPane(Z)V

    .line 58
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/XboxAuthActivity;->setOnLayoutChangedListener(Ljava/lang/Runnable;)V

    .line 47
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onDestroy()V

    .line 48
    return-void
.end method

.method public onRehydrateOverride()V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 63
    return-void
.end method

.method public onSetActive()V
    .locals 0

    .prologue
    .line 88
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 68
    invoke-static {}, Lcom/microsoft/xbox/xle/test/automator/Automator;->getInstance()Lcom/microsoft/xbox/xle/test/automator/IAutomator;

    move-result-object v1

    invoke-interface {v1}, Lcom/microsoft/xbox/xle/test/automator/IAutomator;->logOut()V

    .line 70
    :try_start_0
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/CookieManager;->removeExpiredCookie()V

    .line 71
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/webkit/CookieManager;->setAcceptCookie(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    :goto_0
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onStart()V

    .line 76
    return-void

    .line 72
    :catch_0
    move-exception v0

    .line 73
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "XboxAuthActivity"

    if-nez v0, :cond_0

    const-string v1, "CookieManager error"

    :goto_1
    invoke-static {v2, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
