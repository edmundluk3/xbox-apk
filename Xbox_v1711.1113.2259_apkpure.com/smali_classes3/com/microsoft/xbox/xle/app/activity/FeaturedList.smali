.class public Lcom/microsoft/xbox/xle/app/activity/FeaturedList;
.super Landroid/widget/AdapterView;
.source "FeaturedList.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;,
        Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;,
        Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;,
        Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/AdapterView",
        "<",
        "Landroid/widget/ListAdapter;",
        ">;"
    }
.end annotation


# static fields
.field public static numberOfColumns:I

.field private static padding:I


# instance fields
.field private columnWidth:I

.field private final columns:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;",
            ">;"
        }
    .end annotation
.end field

.field private dataSetObserver:Landroid/database/DataSetObserver;

.field private flingDamping:F

.field private final itemViewCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation
.end field

.field private listAdapter:Landroid/widget/ListAdapter;

.field private listTopAtTouchStart:I

.field private overscroll:Z

.field private reloadViews:Z

.field private rubberbandFactor:F

.field private final setPressedRunnable:Ljava/lang/Runnable;

.field private snapDamping:F

.field private snapSpring:F

.field private touchDownX:I

.field private touchDownY:I

.field private final touchSlop:I

.field private touchState:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

.field private touchedItem:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

.field private velocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    const/16 v0, 0xa

    sput v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->padding:I

    .line 109
    const/4 v0, 0x2

    sput v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->numberOfColumns:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 130
    invoke-direct {p0, p1, p2}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 73
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columns:Ljava/util/ArrayList;

    .line 79
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->itemViewCache:Ljava/util/HashMap;

    .line 88
    sget-object v2, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;->RESTING:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchState:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    .line 112
    const/high16 v2, 0x3fc00000    # 1.5f

    iput v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->flingDamping:F

    .line 493
    new-instance v2, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$3;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$3;-><init>(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)V

    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->setPressedRunnable:Ljava/lang/Runnable;

    .line 132
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 134
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    iput v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchSlop:I

    .line 135
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    sget v2, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->numberOfColumns:I

    if-ge v1, v2, :cond_0

    .line 136
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;-><init>(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$1;)V

    .line 137
    .local v0, "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columns:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 139
    .end local v0    # "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    :cond_0
    return-void
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->overscroll:Z

    return v0
.end method

.method static synthetic access$102(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList;
    .param p1, "x1"    # Z

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->reloadViews:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;I)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList;
    .param p1, "x1"    # I

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->scrollListTo(I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)F
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    .prologue
    .line 28
    iget v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->snapDamping:F

    return v0
.end method

.method static synthetic access$1300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)F
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    .prologue
    .line 28
    iget v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->flingDamping:F

    return v0
.end method

.method static synthetic access$1400(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)F
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    .prologue
    .line 28
    iget v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->snapSpring:F

    return v0
.end method

.method static synthetic access$1500(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->isFirstItemShowing()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1600(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getTopSnapPos()I

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->isLastItemShowing()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1800(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getBottomSnapPos()I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->clearAllData()V

    return-void
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchedItem:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    return-object v0
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchState:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    return-object v0
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)I
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getListTop()I

    move-result v0

    return v0
.end method

.method private addItemToColumnDown(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)V
    .locals 4
    .param p1, "column"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    .param p2, "item"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    .prologue
    .line 386
    invoke-static {p2}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->addViewToLayout(Landroid/view/View;)V

    .line 387
    invoke-static {p2}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->measureView(Landroid/view/View;)V

    .line 388
    invoke-static {p2}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 389
    .local v0, "height":I
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 391
    iget v2, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->bottom:I

    sget v3, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->padding:I

    add-int v1, v2, v3

    .line 392
    .local v1, "top":I
    invoke-direct {p0, p1, p2, v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->layoutItem(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;I)V

    .line 394
    iget v2, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->bottom:I

    sget v3, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->padding:I

    add-int/2addr v3, v0

    add-int/2addr v2, v3

    iput v2, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->bottom:I

    .line 395
    return-void
.end method

.method private addItemToColumnUp(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)V
    .locals 5
    .param p1, "column"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    .param p2, "item"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    .prologue
    const/4 v4, 0x0

    .line 398
    invoke-static {p2}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->addViewToLayout(Landroid/view/View;)V

    .line 399
    invoke-static {p2}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->measureView(Landroid/view/View;)V

    .line 400
    invoke-static {p2}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 401
    .local v0, "height":I
    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-virtual {v2, v4, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 403
    iget v2, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->top:I

    sget v3, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->padding:I

    add-int/2addr v3, v0

    sub-int/2addr v2, v3

    iput v2, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->top:I

    .line 404
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columns:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 405
    iget v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->listTopAtTouchStart:I

    sget v3, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->padding:I

    add-int/2addr v3, v0

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->listTopAtTouchStart:I

    .line 407
    :cond_0
    iget v2, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->top:I

    sget v3, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->padding:I

    add-int v1, v2, v3

    .line 408
    .local v1, "top":I
    invoke-direct {p0, p1, p2, v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->layoutItem(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;I)V

    .line 409
    return-void
.end method

.method private addItemViewToCache(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)V
    .locals 4
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    .prologue
    .line 716
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->listAdapter:Landroid/widget/ListAdapter;

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$400(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    .line 717
    .local v0, "itemViewType":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->itemViewCache:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 718
    .local v1, "viewCacheForType":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    if-nez v1, :cond_0

    .line 719
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "viewCacheForType":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 720
    .restart local v1    # "viewCacheForType":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->itemViewCache:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 722
    :cond_0
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 723
    return-void
.end method

.method private addViewToLayout(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, -0x1

    .line 416
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 417
    .local v0, "params":Landroid/view/ViewGroup$LayoutParams;
    if-nez v0, :cond_0

    .line 418
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    .end local v0    # "params":Landroid/view/ViewGroup$LayoutParams;
    const/4 v1, -0x2

    invoke-direct {v0, v2, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 420
    .restart local v0    # "params":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v2, v0, v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    .line 421
    return-void
.end method

.method private applyRubberBand(I)I
    .locals 5
    .param p1, "pos"    # I

    .prologue
    .line 551
    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->overscroll:Z

    if-eqz v3, :cond_1

    iget v1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->rubberbandFactor:F

    .line 552
    .local v1, "rubberbandFactor":F
    :goto_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->isFirstItemShowing()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 553
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getTopSnapPos()I

    move-result v2

    .line 554
    .local v2, "topRubberbandPos":I
    if-le p1, v2, :cond_2

    .line 555
    int-to-float v3, v2

    sub-int v4, p1, v2

    int-to-float v4, v4

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    float-to-int p1, v3

    .line 566
    .end local v2    # "topRubberbandPos":I
    .end local p1    # "pos":I
    :cond_0
    :goto_1
    return p1

    .line 551
    .end local v1    # "rubberbandFactor":F
    .restart local p1    # "pos":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 559
    .restart local v1    # "rubberbandFactor":F
    :cond_2
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->isLastItemShowing()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 560
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getBottomSnapPos()I

    move-result v0

    .line 561
    .local v0, "bottomRubberbandPos":I
    if-ge p1, v0, :cond_0

    .line 562
    int-to-float v3, v0

    sub-int v4, p1, v0

    int-to-float v4, v4

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    float-to-int p1, v3

    goto :goto_1
.end method

.method private clearAllData()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 194
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->clearAllViews()V

    .line 195
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columns:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    .line 196
    .local v0, "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 197
    iput v3, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->top:I

    .line 198
    iput v3, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->bottom:I

    .line 199
    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->previousItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 201
    .end local v0    # "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->itemViewCache:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 202
    return-void
.end method

.method private clearAllViews()V
    .locals 5

    .prologue
    .line 205
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columns:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    .line 206
    .local v0, "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    .line 207
    .local v1, "item":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->removeItemView(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)V

    goto :goto_0

    .line 210
    .end local v0    # "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    .end local v1    # "item":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->removeAllViewsInLayout()V

    .line 211
    return-void
.end method

.method private endTouch()Z
    .locals 3

    .prologue
    .line 700
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->setPressedRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 701
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->velocityTracker:Landroid/view/VelocityTracker;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 702
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->velocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    .line 703
    .local v0, "velocity":F
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchState:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    sget-object v2, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;->LONG_PRESS:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    if-ne v1, v2, :cond_0

    .line 704
    const/4 v0, 0x0

    .line 706
    :cond_0
    new-instance v1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;-><init>(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;F)V

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$FlingRunnable;->start()V

    .line 708
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->velocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->recycle()V

    .line 709
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->velocityTracker:Landroid/view/VelocityTracker;

    .line 711
    sget-object v1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;->RESTING:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchState:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    .line 712
    const/4 v1, 0x1

    return v1
.end method

.method private ensureDataSetObserverIsCreated()V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->dataSetObserver:Landroid/database/DataSetObserver;

    if-nez v0, :cond_0

    .line 177
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$1;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$1;-><init>(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->dataSetObserver:Landroid/database/DataSetObserver;

    .line 191
    :cond_0
    return-void
.end method

.method private fillList()V
    .locals 0

    .prologue
    .line 284
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->fillListDown()V

    .line 285
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->fillListUp()V

    .line 286
    return-void
.end method

.method private fillListDown()V
    .locals 4

    .prologue
    .line 289
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getLastVisiblePosition()I

    move-result v3

    add-int/lit8 v2, v3, 0x1

    .line 291
    .local v2, "nextPosition":I
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getNextColumnDown()Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    move-result-object v0

    .line 292
    .local v0, "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    :goto_0
    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->listAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 293
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getItemFrolistAdapter(I)Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    move-result-object v1

    .line 294
    .local v1, "item":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->addItemToColumnDown(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)V

    .line 295
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getNextColumnDown()Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    move-result-object v0

    .line 296
    add-int/lit8 v2, v2, 0x1

    .line 297
    goto :goto_0

    .line 298
    .end local v1    # "item":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    :cond_0
    return-void
.end method

.method private fillListUp()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 301
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getNextColumnUp()Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    move-result-object v0

    .line 302
    .local v0, "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    const/4 v2, -0x1

    .line 303
    .local v2, "nextPosition":I
    if-eqz v0, :cond_0

    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->previousItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 304
    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->previousItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 306
    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    if-ltz v2, :cond_2

    .line 307
    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getItemFrolistAdapter(I)Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    move-result-object v1

    .line 308
    .local v1, "item":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->addItemToColumnUp(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)V

    .line 309
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getNextColumnUp()Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    move-result-object v0

    .line 311
    if-eqz v0, :cond_1

    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->previousItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 312
    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->previousItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_0

    .line 314
    :cond_1
    const/4 v2, -0x1

    goto :goto_0

    .line 317
    .end local v1    # "item":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    :cond_2
    return-void
.end method

.method private getAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 142
    sget-object v1, Lcom/microsoft/xboxone/smartglass/R$styleable;->FeaturedList:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 143
    .local v0, "array":Landroid/content/res/TypedArray;
    if-eqz v0, :cond_0

    .line 144
    const/4 v1, 0x0

    sget v2, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->padding:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    sput v1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->padding:I

    .line 145
    const/4 v1, 0x1

    sget v2, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->numberOfColumns:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    sput v1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->numberOfColumns:I

    .line 146
    const/4 v1, 0x2

    iget v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->flingDamping:F

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->flingDamping:F

    .line 147
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 149
    :cond_0
    return-void
.end method

.method private getBottomSnapPos()I
    .locals 3

    .prologue
    .line 739
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getListHeight()I

    move-result v0

    .line 740
    .local v0, "listHeight":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    if-ge v0, v1, :cond_0

    .line 741
    sget v1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->padding:I

    .line 743
    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getListHeight()I

    move-result v2

    sub-int/2addr v1, v2

    goto :goto_0
.end method

.method private getItemFrolistAdapter(I)Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 372
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;-><init>(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$1;)V

    .line 373
    .local v0, "item":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getView(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$302(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;Landroid/view/View;)Landroid/view/View;

    .line 374
    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$402(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;I)I

    .line 375
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->listAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v1, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$602(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;J)J

    .line 376
    return-object v0
.end method

.method private getListHeight()I
    .locals 6

    .prologue
    .line 748
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getListTop()I

    move-result v3

    .line 749
    .local v3, "listTop":I
    const/4 v2, 0x0

    .line 750
    .local v2, "listHeight":I
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columns:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    .line 751
    .local v0, "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    iget v5, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->bottom:I

    sub-int v1, v5, v3

    .line 752
    .local v1, "columnHeight":I
    if-le v1, v2, :cond_0

    .line 753
    move v2, v1

    goto :goto_0

    .line 756
    .end local v0    # "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    .end local v1    # "columnHeight":I
    :cond_1
    sget v4, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->padding:I

    add-int/2addr v4, v2

    return v4
.end method

.method private getListTop()I
    .locals 2

    .prologue
    .line 760
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columns:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    iget v0, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->top:I

    return v0
.end method

.method private getNextColumnDown()Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    .locals 5

    .prologue
    .line 320
    const/4 v2, 0x0

    .line 321
    .local v2, "nextColumn":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getHeight()I

    move-result v1

    .line 322
    .local v1, "highestBottom":I
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columns:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    .line 323
    .local v0, "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    iget v4, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->bottom:I

    if-ge v4, v1, :cond_0

    .line 324
    iget v1, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->bottom:I

    .line 325
    move-object v2, v0

    goto :goto_0

    .line 328
    .end local v0    # "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    :cond_1
    return-object v2
.end method

.method private getNextColumnUp()Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    .locals 5

    .prologue
    .line 332
    const/4 v2, 0x0

    .line 333
    .local v2, "nextColumn":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    const/4 v1, 0x0

    .line 334
    .local v1, "lowestTop":I
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columns:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    .line 335
    .local v0, "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    iget v4, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->top:I

    if-le v4, v1, :cond_0

    .line 336
    iget v1, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->bottom:I

    .line 337
    move-object v2, v0

    goto :goto_0

    .line 340
    .end local v0    # "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    :cond_1
    return-object v2
.end method

.method private getTopSnapPos()I
    .locals 1

    .prologue
    .line 735
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getPaddingTop()I

    move-result v0

    return v0
.end method

.method private getTouchedItem(II)Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    .locals 6
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 503
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columns:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    .line 504
    .local v0, "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    iget v4, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->left:I

    if-le p1, v4, :cond_0

    iget v4, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->left:I

    iget v5, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columnWidth:I

    add-int/2addr v4, v5

    if-ge p1, v4, :cond_0

    .line 505
    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    .line 506
    .local v1, "item":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v2

    .line 507
    .local v2, "view":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v5

    if-ge v5, p2, :cond_1

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v5

    if-le v5, p2, :cond_1

    .line 513
    .end local v0    # "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    .end local v1    # "item":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    .end local v2    # "view":Landroid/view/View;
    :goto_0
    return-object v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getView(I)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 380
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->listAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2, p1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v1

    .line 381
    .local v1, "viewType":I
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getViewFromCache(I)Landroid/view/View;

    move-result-object v0

    .line 382
    .local v0, "cachedView":Landroid/view/View;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->listAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2, p1, v0, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    return-object v2
.end method

.method private getViewFromCache(I)Landroid/view/View;
    .locals 3
    .param p1, "itemViewType"    # I

    .prologue
    .line 726
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->itemViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 727
    .local v0, "viewCacheForType":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 728
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 730
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private handleItemClick(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)V
    .locals 7
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    .prologue
    .line 671
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getOnItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v0

    .line 672
    .local v0, "onItemClickListener":Landroid/widget/AdapterView$OnItemClickListener;
    if-eqz v0, :cond_0

    .line 673
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v2

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$400(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)I

    move-result v3

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$600(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)J

    move-result-wide v4

    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 677
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->setPressedRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 679
    if-eqz p1, :cond_1

    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 680
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->isPressed()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 682
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setPressed(Z)V

    .line 697
    :cond_1
    :goto_0
    return-void

    .line 687
    :cond_2
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setPressed(Z)V

    .line 688
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v6

    .line 689
    .local v6, "view":Landroid/view/View;
    new-instance v1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$4;

    invoke-direct {v1, p0, v6}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$4;-><init>(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;Landroid/view/View;)V

    .line 694
    invoke-static {}, Landroid/view/ViewConfiguration;->getPressedStateDuration()I

    move-result v2

    int-to-long v2, v2

    .line 689
    invoke-virtual {p0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private handleTouchMove(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 517
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->velocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 518
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchState:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    sget-object v1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;->PRESSED:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    if-ne v0, v1, :cond_1

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->hasMovedFarEnoughForScroll(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 519
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->startScrolling(Landroid/view/MotionEvent;)V

    .line 523
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 520
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchState:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    sget-object v1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;->SCROLLING:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    if-ne v0, v1, :cond_0

    .line 521
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->handleTouchScroll(Landroid/view/MotionEvent;)V

    goto :goto_0
.end method

.method private handleTouchScroll(Landroid/view/MotionEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 546
    iget v1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->listTopAtTouchStart:I

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget v3, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchDownY:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-int v0, v1

    .line 547
    .local v0, "listTop":I
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->applyRubberBand(I)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->scrollListTo(I)V

    .line 548
    return-void
.end method

.method private handleTouchUp(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 663
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchState:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    sget-object v1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;->PRESSED:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchedItem:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    if-eqz v0, :cond_0

    .line 664
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchedItem:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->handleItemClick(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)V

    .line 666
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->endTouch()Z

    .line 667
    const/4 v0, 0x1

    return v0
.end method

.method private hasMovedFarEnoughForScroll(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 527
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v0, v2

    .line 528
    .local v0, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v1, v2

    .line 529
    .local v1, "y":I
    iget v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchDownX:I

    iget v3, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchSlop:I

    sub-int/2addr v2, v3

    if-ge v2, v0, :cond_0

    iget v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchDownX:I

    iget v3, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchSlop:I

    add-int/2addr v2, v3

    if-ge v0, v2, :cond_0

    iget v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchDownY:I

    iget v3, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchSlop:I

    sub-int/2addr v2, v3

    if-ge v2, v1, :cond_0

    iget v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchDownY:I

    iget v3, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchSlop:I

    add-int/2addr v2, v3

    if-ge v1, v2, :cond_0

    .line 530
    const/4 v2, 0x0

    .line 532
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private isBottomItemVisible(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;)Z
    .locals 4
    .param p1, "column"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    .prologue
    const/4 v1, 0x0

    .line 608
    iget-object v0, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 609
    iget-object v0, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    if-gt v0, v2, :cond_0

    const/4 v0, 0x1

    .line 611
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 609
    goto :goto_0

    :cond_1
    move v0, v1

    .line 611
    goto :goto_0
.end method

.method private isFirstItemShowing()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 652
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columns:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    .line 653
    .local v0, "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    iget-object v1, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 654
    iget-object v1, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$400(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)I

    move-result v1

    if-nez v1, :cond_0

    .line 655
    const/4 v1, 0x1

    .line 659
    .end local v0    # "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    :goto_0
    return v1

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method private isLastItemShowing()Z
    .locals 4

    .prologue
    .line 641
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columns:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    .line 642
    .local v0, "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    iget-object v1, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 643
    iget-object v1, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$400(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)I

    move-result v1

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->listAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_0

    .line 644
    const/4 v1, 0x1

    .line 648
    .end local v0    # "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isTopItemVisible(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;)Z
    .locals 2
    .param p1, "column"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    .prologue
    const/4 v1, 0x0

    .line 600
    iget-object v0, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 601
    iget-object v0, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    .line 603
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 601
    goto :goto_0

    :cond_1
    move v0, v1

    .line 603
    goto :goto_0
.end method

.method private layoutItem(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;I)V
    .locals 4
    .param p1, "column"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    .param p2, "item"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    .param p3, "top"    # I

    .prologue
    .line 412
    invoke-static {p2}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v0

    iget v1, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->left:I

    iget v2, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->left:I

    iget v3, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columnWidth:I

    add-int/2addr v2, v3

    invoke-static {p2}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, p3

    invoke-virtual {v0, v1, p3, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 413
    return-void
.end method

.method private measureView(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, 0x0

    .line 424
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 426
    .local v2, "params":Landroid/view/ViewGroup$LayoutParams;
    iget v3, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columnWidth:I

    .line 427
    .local v3, "width":I
    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 429
    .local v4, "widthMeasureSpec":I
    iget v0, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 431
    .local v0, "height":I
    if-lez v0, :cond_0

    .line 432
    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 437
    .local v1, "heightMeasureSpec":I
    :goto_0
    invoke-virtual {p1, v4, v1}, Landroid/view/View;->measure(II)V

    .line 438
    return-void

    .line 434
    .end local v1    # "heightMeasureSpec":I
    :cond_0
    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .restart local v1    # "heightMeasureSpec":I
    goto :goto_0
.end method

.method private offsetListTo(I)V
    .locals 6
    .param p1, "pos"    # I

    .prologue
    .line 577
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columns:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    iget v3, v3, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->top:I

    sub-int v1, p1, v3

    .line 578
    .local v1, "delta":I
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columns:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    .line 579
    .local v0, "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    iget v4, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->top:I

    add-int/2addr v4, v1

    iput v4, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->top:I

    .line 580
    iget v4, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->bottom:I

    add-int/2addr v4, v1

    iput v4, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->bottom:I

    .line 581
    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    .line 582
    .local v2, "item":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/view/View;->offsetTopAndBottom(I)V

    goto :goto_0

    .line 585
    .end local v0    # "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    .end local v2    # "item":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    :cond_1
    return-void
.end method

.method private reloadViews()V
    .locals 7

    .prologue
    .line 264
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columns:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    .line 265
    .local v0, "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    iget v4, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->top:I

    sget v5, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->padding:I

    add-int v2, v4, v5

    .line 266
    .local v2, "top":I
    iget-object v4, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    .line 268
    .local v1, "item":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->removeItemView(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)V

    .line 271
    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$400(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)I

    move-result v5

    invoke-direct {p0, v5}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getView(I)Landroid/view/View;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$302(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;Landroid/view/View;)Landroid/view/View;

    .line 274
    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->addViewToLayout(Landroid/view/View;)V

    .line 275
    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->measureView(Landroid/view/View;)V

    .line 276
    invoke-direct {p0, v0, v1, v2}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->layoutItem(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;I)V

    .line 278
    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    sget v6, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->padding:I

    add-int/2addr v5, v6

    add-int/2addr v2, v5

    .line 279
    goto :goto_0

    .line 281
    .end local v0    # "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    .end local v1    # "item":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    .end local v2    # "top":I
    :cond_1
    return-void
.end method

.method private removeBottomItem(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;)V
    .locals 4
    .param p1, "column"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    .prologue
    .line 627
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 628
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    iget-object v2, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    .line 629
    .local v0, "item":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    iget v1, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->bottom:I

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    sget v3, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->padding:I

    add-int/2addr v2, v3

    sub-int/2addr v1, v2

    iput v1, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->bottom:I

    .line 630
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->removeItemView(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)V

    .line 632
    .end local v0    # "item":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    :cond_0
    return-void
.end method

.method private removeItemView(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)V
    .locals 1
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    .prologue
    .line 635
    invoke-static {p1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->removeViewInLayout(Landroid/view/View;)V

    .line 636
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->addItemViewToCache(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)V

    .line 637
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$302(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;Landroid/view/View;)Landroid/view/View;

    .line 638
    return-void
.end method

.method private removeNonVisibleViews()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 588
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columns:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    .line 589
    .local v0, "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    :goto_0
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->isTopItemVisible(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->isLastItemShowing()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, v3, :cond_1

    .line 590
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->removeTopItem(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;)V

    goto :goto_0

    .line 593
    :cond_1
    :goto_1
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->isBottomItemVisible(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->isFirstItemShowing()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, v3, :cond_0

    .line 594
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->removeBottomItem(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;)V

    goto :goto_1

    .line 597
    .end local v0    # "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    :cond_2
    return-void
.end method

.method private removeTopItem(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;)V
    .locals 5
    .param p1, "column"    # Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    .prologue
    const/4 v4, 0x0

    .line 615
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 616
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    .line 617
    .local v0, "item":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    iget v1, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->top:I

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    sget v3, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->padding:I

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->top:I

    .line 618
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columns:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 619
    iget v1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->listTopAtTouchStart:I

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    sget v3, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->padding:I

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->listTopAtTouchStart:I

    .line 621
    :cond_0
    iget-object v1, p1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->previousItems:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$400(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 622
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->removeItemView(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)V

    .line 624
    .end local v0    # "item":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    :cond_1
    return-void
.end method

.method private scrollListTo(I)V
    .locals 0
    .param p1, "listTop"    # I

    .prologue
    .line 570
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->offsetListTo(I)V

    .line 571
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->removeNonVisibleViews()V

    .line 572
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->fillList()V

    .line 573
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->invalidate()V

    .line 574
    return-void
.end method

.method private startScrolling(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 536
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->setPressedRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 537
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchedItem:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchedItem:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 538
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchedItem:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    .line 540
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchDownX:I

    .line 541
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchDownY:I

    .line 542
    sget-object v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;->SCROLLING:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchState:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    .line 543
    return-void
.end method

.method private startTouch(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 470
    sget-object v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;->PRESSED:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchState:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    .line 471
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchDownX:I

    .line 472
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchDownY:I

    .line 473
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getListTop()I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->listTopAtTouchStart:I

    .line 474
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->velocityTracker:Landroid/view/VelocityTracker;

    .line 475
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->velocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 476
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getTouchedItem(II)Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->touchedItem:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    .line 480
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$2;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$2;-><init>(Lcom/microsoft/xbox/xle/app/activity/FeaturedList;)V

    .line 487
    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollDefaultDelay()I

    move-result v1

    int-to-long v2, v1

    .line 480
    invoke-virtual {p0, v0, v2, v3}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 489
    const/4 v0, 0x1

    return v0
.end method

.method private updateColumnDimensions(I)V
    .locals 5
    .param p1, "width"    # I

    .prologue
    .line 229
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    sub-int/2addr p1, v2

    .line 230
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columns:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    sget v3, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->padding:I

    mul-int/2addr v2, v3

    sub-int v2, p1, v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columns:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    div-int/2addr v2, v3

    iput v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columnWidth:I

    .line 231
    const/4 v1, 0x0

    .line 232
    .local v1, "columnLeft":I
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columns:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    .line 233
    .local v0, "col":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getPaddingLeft()I

    move-result v3

    add-int/2addr v3, v1

    iput v3, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->left:I

    .line 234
    iget v3, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columnWidth:I

    sget v4, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->padding:I

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    .line 235
    goto :goto_0

    .line 236
    .end local v0    # "col":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->listAdapter:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public getFirstVisiblePosition()I
    .locals 6

    .prologue
    .line 359
    const v1, 0x7fffffff

    .line 360
    .local v1, "firstPosition":I
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columns:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    .line 361
    .local v0, "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 362
    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$400(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)I

    move-result v2

    .line 363
    .local v2, "firstPositionInColumn":I
    if-ge v2, v1, :cond_0

    .line 364
    move v1, v2

    goto :goto_0

    .line 368
    .end local v0    # "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    .end local v2    # "firstPositionInColumn":I
    :cond_1
    return v1
.end method

.method public getLastVisiblePosition()I
    .locals 6

    .prologue
    .line 345
    const/4 v1, -0x1

    .line 346
    .local v1, "lastPosition":I
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columns:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    .line 347
    .local v0, "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 348
    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    iget-object v5, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$400(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)I

    move-result v2

    .line 349
    .local v2, "lastPositionInColumn":I
    if-le v2, v1, :cond_0

    .line 350
    move v1, v2

    goto :goto_0

    .line 354
    .end local v0    # "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    .end local v2    # "lastPositionInColumn":I
    :cond_1
    return v1
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 1

    .prologue
    .line 215
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 253
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->listAdapter:Landroid/widget/ListAdapter;

    if-nez v0, :cond_0

    .line 261
    :goto_0
    return-void

    .line 256
    :cond_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->reloadViews:Z

    if-eqz v0, :cond_1

    .line 257
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->reloadViews:Z

    .line 258
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->reloadViews()V

    .line 260
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->fillList()V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 240
    invoke-super {p0, p1, p2}, Landroid/widget/AdapterView;->onMeasure(II)V

    .line 241
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->getMeasuredWidth()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->updateColumnDimensions(I)V

    .line 242
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->columns:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;

    .line 243
    .local v0, "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    iget-object v3, v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;->items:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;

    .line 244
    .local v1, "item":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 245
    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;->access$300(Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;)Landroid/view/View;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->measureView(Landroid/view/View;)V

    goto :goto_0

    .line 249
    .end local v0    # "column":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Column;
    .end local v1    # "item":Lcom/microsoft/xbox/xle/app/activity/FeaturedList$Item;
    :cond_2
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 224
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/AdapterView;->onSizeChanged(IIII)V

    .line 225
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->updateColumnDimensions(I)V

    .line 226
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 442
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v1

    if-lez v1, :cond_0

    .line 443
    const/4 v0, 0x0

    .line 466
    :goto_0
    return v0

    .line 448
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 463
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->endTouch()Z

    move-result v0

    .local v0, "handled":Z
    goto :goto_0

    .line 450
    .end local v0    # "handled":Z
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->startTouch(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 451
    .restart local v0    # "handled":Z
    goto :goto_0

    .line 453
    .end local v0    # "handled":Z
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->handleTouchMove(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 454
    .restart local v0    # "handled":Z
    goto :goto_0

    .line 456
    .end local v0    # "handled":Z
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->handleTouchUp(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 457
    .restart local v0    # "handled":Z
    goto :goto_0

    .line 460
    .end local v0    # "handled":Z
    :pswitch_4
    const/4 v0, 0x0

    .line 461
    .restart local v0    # "handled":Z
    goto :goto_0

    .line 448
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    .line 158
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->listAdapter:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->listAdapter:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->dataSetObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 163
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->clearAllData()V

    .line 165
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->listAdapter:Landroid/widget/ListAdapter;

    .line 167
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->listAdapter:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_1

    .line 168
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->ensureDataSetObserverIsCreated()V

    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->listAdapter:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->dataSetObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 171
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList;->requestLayout()V

    .line 173
    return-void
.end method

.method public setSelection(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 220
    return-void
.end method
