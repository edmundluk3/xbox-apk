.class Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "MessageModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/MessageModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SendSkypeGroupMessageRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final caller:Lcom/microsoft/xbox/service/model/MessageModel;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private conversationId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private newGroup:Z

.field private final request:Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/MessageModel;

.field private final topic:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private xuids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;)V
    .locals 1
    .param p1    # Lcom/microsoft/xbox/service/model/MessageModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/MessageModel;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "topic"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p6, "request"    # Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/MessageModel;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1053
    .local p4, "xuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->this$0:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 1050
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->newGroup:Z

    .line 1054
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1055
    invoke-static {p6}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 1057
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->caller:Lcom/microsoft/xbox/service/model/MessageModel;

    .line 1058
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->conversationId:Ljava/lang/String;

    .line 1059
    iput-object p6, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->request:Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;

    .line 1060
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p4, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->xuids:Ljava/util/List;

    .line 1061
    iput-object p5, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->topic:Ljava/lang/String;

    .line 1062
    return-void
.end method


# virtual methods
.method public buildData()Ljava/lang/Boolean;
    .locals 22
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1067
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v7

    .line 1068
    .local v7, "meXuid":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->conversationId:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 1069
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1070
    .local v10, "members":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    new-instance v6, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    const-string v18, "8:xbox:%s"

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    aput-object v7, v19, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    sget-object v19, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;->User:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v6, v0, v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;)V

    .line 1071
    .local v6, "me":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1073
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->xuids:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_0

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 1074
    .local v13, "recipientXuid":Ljava/lang/String;
    new-instance v8, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    const-string v19, "8:xbox:%s"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v13, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    sget-object v20, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;->User:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v8, v0, v1}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;)V

    .line 1075
    .local v8, "member":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1078
    .end local v8    # "member":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    .end local v13    # "recipientXuid":Ljava/lang/String;
    :cond_0
    invoke-static {v10}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v18

    if-nez v18, :cond_3

    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->access$500()Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->topic:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-interface {v0, v10, v1}, Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;->createGroup(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    :goto_1
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->conversationId:Ljava/lang/String;

    .line 1079
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->newGroup:Z

    .line 1082
    .end local v6    # "me":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    .end local v10    # "members":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->newGroup:Z

    move/from16 v18, v0

    if-nez v18, :cond_4

    .line 1083
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->xuids:Ljava/util/List;

    .line 1084
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->this$0:Lcom/microsoft/xbox/service/model/MessageModel;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->conversationId:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lcom/microsoft/xbox/service/model/MessageModel;->getSkypeConversationMessages(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    move-result-object v4

    .line 1085
    .local v4, "conversation":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    if-eqz v4, :cond_4

    iget-object v0, v4, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForGroupMembers:Ljava/util/Map;

    move-object/from16 v18, v0

    if-eqz v18, :cond_4

    .line 1086
    iget-object v0, v4, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForGroupMembers:Ljava/util/Map;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_2
    :goto_2
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_4

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    .line 1087
    .local v5, "gm":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->getId()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/microsoft/xbox/service/groupMessaging/SkypeUtil;->getXuidFromSkypeMessageId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1088
    .local v9, "memberXuid":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_2

    .line 1089
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->xuids:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1078
    .end local v4    # "conversation":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    .end local v5    # "gm":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    .end local v9    # "memberXuid":Ljava/lang/String;
    .restart local v6    # "me":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    .restart local v10    # "members":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    :cond_3
    const/16 v18, 0x0

    goto :goto_1

    .line 1095
    .end local v6    # "me":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    .end local v10    # "members":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    :cond_4
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 1096
    .local v16, "users":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$User;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->xuids:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_3
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_5

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 1097
    .local v17, "xuid":Ljava/lang/String;
    new-instance v15, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$User;

    move-object/from16 v0, v17

    invoke-direct {v15, v0}, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$User;-><init>(Ljava/lang/String;)V

    .line 1098
    .local v15, "u":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$User;
    move-object/from16 v0, v16

    invoke-interface {v0, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1101
    .end local v15    # "u":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$User;
    .end local v17    # "xuid":Ljava/lang/String;
    :cond_5
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 1102
    .local v12, "permissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v18, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;->CommunicateUsingText:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;

    invoke-virtual/range {v18 .. v18}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PermissionSetting;->name()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1103
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->access$500()Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-interface {v0, v7, v12, v1}, Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;->getUserPermissions(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponse;

    move-result-object v14

    .line 1104
    .local v14, "response":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponse;
    invoke-static {v14}, Lcom/microsoft/xbox/service/model/MessageModel;->access$600(Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$PermissionResponse;)Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;

    move-result-object v11

    .line 1108
    .local v11, "permissionSetting":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$UserPermissionSetting;
    if-eqz v11, :cond_6

    .line 1109
    new-instance v18, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v20, 0x26e8

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v18

    .line 1112
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->conversationId:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_7

    .line 1113
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->conversationId:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->request:Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;->getSendSkypeMessageRequestBody(Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;)Ljava/lang/String;

    move-result-object v20

    invoke-interface/range {v18 .. v20}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->sendSkypeMessage(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_7

    const/16 v18, 0x1

    .line 1112
    :goto_4
    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v18

    return-object v18

    .line 1113
    :cond_7
    const/16 v18, 0x0

    goto :goto_4
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1039
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->buildData()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 1151
    const-wide/16 v0, 0xbec

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 6
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1122
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v2, :cond_0

    .line 1123
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/XLEException;->getErrorCode()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackSendMessageError(J)V

    .line 1126
    :cond_0
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->newGroup:Z

    if-eqz v1, :cond_1

    .line 1127
    iput-boolean v4, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->newGroup:Z

    .line 1128
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->caller:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->conversationId:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->updateFromNotification(Ljava/lang/String;Z)V

    .line 1131
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->request:Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;->messagetype:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1132
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->request:Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;->messagetype:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    move-result-object v0

    .line 1133
    .local v0, "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    sget-object v1, Lcom/microsoft/xbox/service/model/MessageModel$4;->$SwitchMap$com$microsoft$xbox$service$model$serialization$SkypeMessageType:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1143
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->access$900()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SendSkypeGroupMessageRunner onPostExecute - unsupported request type:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 1147
    .end local v0    # "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    :cond_2
    :goto_0
    :pswitch_0
    return-void

    .line 1135
    .restart local v0    # "type":Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;
    :pswitch_1
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->caller:Lcom/microsoft/xbox/service/model/MessageModel;

    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v1, v3, p0, v5}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-static {p1, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->conversationId:Ljava/lang/String;

    invoke-static {v2, v1, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->access$700(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V

    goto :goto_0

    .line 1138
    :pswitch_2
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->caller:Lcom/microsoft/xbox/service/model/MessageModel;

    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v1, v3, p0, v5}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-static {p1, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/MessageModel$SendSkypeGroupMessageRunner;->conversationId:Ljava/lang/String;

    invoke-static {v2, v1, v3}, Lcom/microsoft/xbox/service/model/MessageModel;->access$800(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V

    goto :goto_0

    .line 1133
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 1118
    return-void
.end method
