.class final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams$Builder;
.super Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;
.source "AutoValue_ClubSearchDataTypes_ClubSearchParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# instance fields
.field private query:Ljava/lang/String;

.field private tags:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private titles:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;-><init>()V

    .line 88
    return-void
.end method

.method constructor <init>(Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;)V
    .locals 1
    .param p1, "source"    # Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;-><init>()V

    .line 90
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;->query()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams$Builder;->query:Ljava/lang/String;

    .line 91
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;->tags()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams$Builder;->tags:Lcom/google/common/collect/ImmutableList;

    .line 92
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;->titles()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams$Builder;->titles:Lcom/google/common/collect/ImmutableList;

    .line 93
    return-void
.end method


# virtual methods
.method public build()Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams;
    .locals 5

    .prologue
    .line 111
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;

    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams$Builder;->query:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams$Builder;->tags:Lcom/google/common/collect/ImmutableList;

    iget-object v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams$Builder;->titles:Lcom/google/common/collect/ImmutableList;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams;-><init>(Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams$1;)V

    return-object v0
.end method

.method public query(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;
    .locals 0
    .param p1, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 96
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams$Builder;->query:Ljava/lang/String;

    .line 97
    return-object p0
.end method

.method public tags(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;"
        }
    .end annotation

    .prologue
    .line 101
    .local p1, "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams$Builder;->tags:Lcom/google/common/collect/ImmutableList;

    .line 102
    return-object p0

    .line 101
    :cond_0
    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    goto :goto_0
.end method

.method public titles(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubSearchDataTypes$ClubSearchParams$Builder;"
        }
    .end annotation

    .prologue
    .line 106
    .local p1, "titles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubSearchDataTypes_ClubSearchParams$Builder;->titles:Lcom/google/common/collect/ImmutableList;

    .line 107
    return-object p0

    .line 106
    :cond_0
    invoke-static {p1}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    goto :goto_0
.end method
