.class abstract Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_LfgLanguageList;
.super Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;
.source "$AutoValue_EditorialDataTypes_LfgLanguageList.java"


# instance fields
.field private final languageList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 15
    .local p1, "languageList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;-><init>()V

    .line 16
    if-nez p1, :cond_0

    .line 17
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null languageList"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_LfgLanguageList;->languageList:Ljava/util/List;

    .line 20
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 38
    if-ne p1, p0, :cond_0

    .line 39
    const/4 v1, 0x1

    .line 45
    :goto_0
    return v1

    .line 41
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 42
    check-cast v0, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;

    .line 43
    .local v0, "that":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;
    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_LfgLanguageList;->languageList:Ljava/util/List;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;->languageList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 45
    .end local v0    # "that":Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguageList;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 50
    const/4 v0, 0x1

    .line 51
    .local v0, "h":I
    const v1, 0xf4243

    mul-int/2addr v0, v1

    .line 52
    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_LfgLanguageList;->languageList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 53
    return v0
.end method

.method public languageList()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "lfgLanguageList"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$LfgLanguage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    iget-object v0, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_LfgLanguageList;->languageList:Ljava/util/List;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LfgLanguageList{languageList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/socialTags/$AutoValue_EditorialDataTypes_LfgLanguageList;->languageList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
