.class public Lcom/microsoft/xbox/xle/app/activity/FutureShowtimesScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "FutureShowtimesScreen.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    const-string v0, "FutureShowtimes"

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 10
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 12
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/FutureShowtimesScreen;->onCreateContentView()V

    .line 13
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/viewmodel/FutureShowtimesScreenViewModel;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/FutureShowtimesScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 14
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 18
    const v0, 0x7f03016b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/FutureShowtimesScreen;->setContentView(I)V

    .line 19
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    return v0
.end method
