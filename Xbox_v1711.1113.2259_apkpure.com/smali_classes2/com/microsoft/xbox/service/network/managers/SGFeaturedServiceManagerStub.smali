.class public Lcom/microsoft/xbox/service/network/managers/SGFeaturedServiceManagerStub;
.super Ljava/lang/Object;
.source "SGFeaturedServiceManagerStub.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/ISGFeaturedServiceManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getProgrammingContentManifest()Lcom/microsoft/xbox/service/model/serialization/ProgrammingContentManifest;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 18
    const-string v2, "ProgrammingServiceManagerStub"

    const-string v3, "getting programming override data from mock xml file"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    :try_start_0
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->AssetManager:Landroid/content/res/AssetManager;

    const-string v3, "stubdata/ProgrammingOverride.xml"

    invoke-virtual {v2, v3}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    .line 23
    .local v1, "stream":Ljava/io/InputStream;
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XMLHelper;->instance()Lcom/microsoft/xbox/toolkit/XMLHelper;

    move-result-object v2

    const-class v3, Lcom/microsoft/xbox/service/model/serialization/ProgrammingContentManifest;

    invoke-virtual {v2, v1, v3}, Lcom/microsoft/xbox/toolkit/XMLHelper;->load(Ljava/io/InputStream;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/serialization/ProgrammingContentManifest;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    .line 25
    .end local v1    # "stream":Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 26
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v4, 0xfa1

    const-string v3, "failed to get programming override data"

    invoke-direct {v2, v4, v5, v3, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method
