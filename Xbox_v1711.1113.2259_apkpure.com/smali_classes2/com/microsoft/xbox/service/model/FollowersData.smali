.class public Lcom/microsoft/xbox/service/model/FollowersData;
.super Ljava/lang/Object;
.source "FollowersData.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/FollowersData$DummyType;,
        Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x5d301434c32a65e1L


# instance fields
.field public broadcast:Ljava/util/List;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Broadcast;",
            ">;"
        }
    .end annotation
.end field

.field public club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private followerText:Ljava/lang/String;

.field public isBroadcasting:Z

.field public isCurrentlyPlaying:Z

.field protected isDummy:Z

.field public isFavorite:Z

.field public isInParty:Z

.field public transient isNew:Z

.field protected itemDummyType:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

.field private lastPlayedWithDateTime:Ljava/util/Date;

.field private personSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

.field public presenceString:Ljava/lang/String;

.field private recentPlayerText:Ljava/lang/String;

.field private searchResultPerson:Lcom/microsoft/xbox/service/model/SearchResultPerson;

.field public status:Lcom/microsoft/xbox/service/model/UserStatus;

.field private timeStamp:Ljava/util/Date;

.field public titleId:J

.field public userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

.field public xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object v1, p0, Lcom/microsoft/xbox/service/model/FollowersData;->personSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 54
    iput-object v1, p0, Lcom/microsoft/xbox/service/model/FollowersData;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 63
    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isCurrentlyPlaying:Z

    .line 79
    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isDummy:Z

    .line 82
    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isNew:Z

    .line 90
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V
    .locals 6
    .param p1, "club"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object v3, p0, Lcom/microsoft/xbox/service/model/FollowersData;->personSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 54
    iput-object v3, p0, Lcom/microsoft/xbox/service/model/FollowersData;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 63
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isCurrentlyPlaying:Z

    .line 79
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isDummy:Z

    .line 82
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isNew:Z

    .line 152
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/FollowersData;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 154
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->id()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    .line 155
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isFavorite:Z

    .line 156
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresenceCount()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/service/model/UserStatus;->Online:Lcom/microsoft/xbox/service/model/UserStatus;

    :goto_0
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    .line 157
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->getPresenceStringWithMembership()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->presenceString:Ljava/lang/String;

    .line 158
    iput-wide v4, p0, Lcom/microsoft/xbox/service/model/FollowersData;->titleId:J

    .line 159
    iput-object v3, p0, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    .line 160
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isCurrentlyPlaying:Z

    .line 161
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->timeStamp:Ljava/util/Date;

    .line 162
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isDummy:Z

    .line 163
    return-void

    .line 156
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/model/UserStatus;->Offline:Lcom/microsoft/xbox/service/model/UserStatus;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/model/FollowersData;)V
    .locals 2
    .param p1, "follower"    # Lcom/microsoft/xbox/service/model/FollowersData;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object v1, p0, Lcom/microsoft/xbox/service/model/FollowersData;->personSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 54
    iput-object v1, p0, Lcom/microsoft/xbox/service/model/FollowersData;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 63
    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isCurrentlyPlaying:Z

    .line 79
    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isDummy:Z

    .line 82
    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isNew:Z

    .line 137
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    .line 138
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/model/FollowersData;->isFavorite:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isFavorite:Z

    .line 139
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/model/FollowersData;->isBroadcasting:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isBroadcasting:Z

    .line 140
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/model/FollowersData;->isInParty:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isInParty:Z

    .line 141
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/FollowersData;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    .line 142
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/FollowersData;->presenceString:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->presenceString:Ljava/lang/String;

    .line 143
    iget-wide v0, p1, Lcom/microsoft/xbox/service/model/FollowersData;->titleId:J

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->titleId:J

    .line 144
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    .line 145
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/model/FollowersData;->isCurrentlyPlaying:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isCurrentlyPlaying:Z

    .line 146
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/FollowersData;->timeStamp:Ljava/util/Date;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->timeStamp:Ljava/util/Date;

    .line 147
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/model/FollowersData;->isDummy:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isDummy:Z

    .line 148
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/FollowersData;->broadcast:Ljava/util/List;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->broadcast:Ljava/util/List;

    .line 149
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V
    .locals 4
    .param p1, "person"    # Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->personSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 54
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 63
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isCurrentlyPlaying:Z

    .line 79
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isDummy:Z

    .line 82
    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isNew:Z

    .line 102
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 103
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/FollowersData;->personSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 104
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    .line 105
    new-instance v0, Lcom/microsoft/xbox/service/model/UserProfileData;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/service/model/UserProfileData;-><init>(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    .line 106
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->isFavorite:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isFavorite:Z

    .line 107
    iget-boolean v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->isBroadcasting:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isBroadcasting:Z

    .line 108
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->multiplayerSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$MultiplayerSummary;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->multiplayerSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$MultiplayerSummary;

    iget v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$MultiplayerSummary;->InParty:I

    if-lez v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isInParty:Z

    .line 109
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->presenceState:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/UserStatus;->getStatusFromString(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/UserStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    .line 110
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->presenceText:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->presenceString:Ljava/lang/String;

    .line 111
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->titleHistory:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubTitleHistory;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->titleHistory:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubTitleHistory;

    iget-wide v2, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubTitleHistory;->TitleId:J

    iput-wide v2, p0, Lcom/microsoft/xbox/service/model/FollowersData;->titleId:J

    .line 113
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->titleHistory:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubTitleHistory;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubTitleHistory;->LastTimePlayed:Ljava/util/Date;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->timeStamp:Ljava/util/Date;

    .line 116
    :cond_0
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->recentPlayer:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecentPlayer;

    if-eqz v0, :cond_1

    .line 117
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->recentPlayer:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecentPlayer;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecentPlayer;->text:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->recentPlayerText:Ljava/lang/String;

    .line 119
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->recentPlayer:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecentPlayer;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecentPlayer;->titles:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 120
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->recentPlayer:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecentPlayer;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecentPlayer;->titles:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Title;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Title;->lastPlayedWithDateTime:Ljava/util/Date;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->lastPlayedWithDateTime:Ljava/util/Date;

    .line 124
    :cond_1
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->follower:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Follower;

    if-eqz v0, :cond_2

    .line 125
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->follower:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Follower;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$Follower;->text:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->followerText:Ljava/lang/String;

    .line 128
    :cond_2
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->titlePresence:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubTitlePresence;

    if-eqz v0, :cond_3

    .line 129
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->titlePresence:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubTitlePresence;

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubTitlePresence;->IsCurrentlyPlaying:Z

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isCurrentlyPlaying:Z

    .line 130
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->titlePresence:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubTitlePresence;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubTitlePresence;->PresenceText:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->presenceString:Ljava/lang/String;

    .line 133
    :cond_3
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->broadcast:Ljava/util/List;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->broadcast:Ljava/util/List;

    .line 134
    return-void

    :cond_4
    move v0, v1

    .line 108
    goto :goto_0
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "isDummy"    # Z

    .prologue
    .line 93
    sget-object v0, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->NOT_SET:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>(ZLcom/microsoft/xbox/service/model/FollowersData$DummyType;)V

    .line 94
    return-void
.end method

.method public constructor <init>(ZLcom/microsoft/xbox/service/model/FollowersData$DummyType;)V
    .locals 2
    .param p1, "isDummy"    # Z
    .param p2, "type"    # Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object v1, p0, Lcom/microsoft/xbox/service/model/FollowersData;->personSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 54
    iput-object v1, p0, Lcom/microsoft/xbox/service/model/FollowersData;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    .line 63
    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isCurrentlyPlaying:Z

    .line 79
    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isDummy:Z

    .line 82
    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isNew:Z

    .line 97
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isDummy:Z

    .line 98
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/FollowersData;->itemDummyType:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    .line 99
    return-void
.end method

.method public static dummy(Lcom/microsoft/xbox/service/model/FollowersData$DummyType;)Lcom/microsoft/xbox/service/model/FollowersData;
    .locals 2
    .param p0, "type"    # Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    .prologue
    .line 85
    new-instance v0, Lcom/microsoft/xbox/service/model/FollowersData;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p0}, Lcom/microsoft/xbox/service/model/FollowersData;-><init>(ZLcom/microsoft/xbox/service/model/FollowersData$DummyType;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 262
    if-ne p1, p0, :cond_1

    .line 271
    :cond_0
    :goto_0
    return v1

    .line 264
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/service/model/FollowersData;

    if-nez v3, :cond_2

    move v1, v2

    .line 265
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 267
    check-cast v0, Lcom/microsoft/xbox/service/model/FollowersData;

    .line 268
    .local v0, "other":Lcom/microsoft/xbox/service/model/FollowersData;
    iget-boolean v3, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isDummy:Z

    if-eqz v3, :cond_3

    iget-boolean v3, v0, Lcom/microsoft/xbox/service/model/FollowersData;->isDummy:Z

    if-eqz v3, :cond_3

    .line 269
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/FollowersData;->itemDummyType:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/FollowersData;->itemDummyType:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0

    .line 271
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    iget-object v2, v0, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public getFollowersTitleText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->followerText:Ljava/lang/String;

    return-object v0
.end method

.method public getGamerName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 213
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/UserProfileData;->appDisplayName:Ljava/lang/String;

    .line 217
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getGamerPicUrl()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 202
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/UserProfileData;->profileImageUrl:Ljava/lang/String;

    .line 208
    :goto_0
    return-object v0

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 205
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->displayImageUrl()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 208
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGamerRealName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/UserProfileData;->gamerRealName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getGamertag()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 191
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/UserProfileData;->gamerTag:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 196
    :goto_0
    return-object v0

    .line 193
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 194
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->name()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 196
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public getIsDummy()Z
    .locals 1

    .prologue
    .line 237
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isDummy:Z

    return v0
.end method

.method public getIsOnline()Z
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    sget-object v1, Lcom/microsoft/xbox/service/model/UserStatus;->Online:Lcom/microsoft/xbox/service/model/UserStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemDummyType()Lcom/microsoft/xbox/service/model/FollowersData$DummyType;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->itemDummyType:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    return-object v0
.end method

.method public getLastPlayedWithDateTime()Ljava/util/Date;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->lastPlayedWithDateTime:Ljava/util/Date;

    return-object v0
.end method

.method public getPersonSummary()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->personSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    return-object v0
.end method

.method public getRecentPlayerTitleText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->recentPlayerText:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchResultPerson()Lcom/microsoft/xbox/service/model/SearchResultPerson;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->searchResultPerson:Lcom/microsoft/xbox/service/model/SearchResultPerson;

    return-object v0
.end method

.method public getTimeStamp()Ljava/util/Date;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->timeStamp:Ljava/util/Date;

    return-object v0
.end method

.method public getType()Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->userProfileData:Lcom/microsoft/xbox/service/model/UserProfileData;

    if-eqz v0, :cond_0

    .line 168
    sget-object v0, Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;->Person:Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;

    .line 172
    :goto_0
    return-object v0

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->club:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    if-eqz v0, :cond_1

    .line 170
    sget-object v0, Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;->Club:Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;

    goto :goto_0

    .line 172
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;->Dummy:Lcom/microsoft/xbox/service/model/FollowersData$FollowerDataType;

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 277
    iget-boolean v1, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isDummy:Z

    if-eqz v1, :cond_0

    .line 278
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/FollowersData;->itemDummyType:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/FollowersData$DummyType;->hashCode()I

    move-result v0

    .line 283
    :goto_0
    return v0

    .line 280
    :cond_0
    const/16 v0, 0x11

    .line 281
    .local v0, "hashCode":I
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/FollowersData;->xuid:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCodeLowercase(Ljava/lang/String;)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 283
    goto :goto_0
.end method

.method public setItemDummyType(Lcom/microsoft/xbox/service/model/FollowersData$DummyType;)V
    .locals 1
    .param p1, "type"    # Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    .prologue
    .line 177
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/FollowersData;->isDummy:Z

    .line 178
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/FollowersData;->itemDummyType:Lcom/microsoft/xbox/service/model/FollowersData$DummyType;

    .line 179
    return-void
.end method

.method public setSearchResultPerson(Lcom/microsoft/xbox/service/model/SearchResultPerson;)V
    .locals 0
    .param p1, "srp"    # Lcom/microsoft/xbox/service/model/SearchResultPerson;

    .prologue
    .line 257
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/FollowersData;->searchResultPerson:Lcom/microsoft/xbox/service/model/SearchResultPerson;

    .line 258
    return-void
.end method

.method public setTimeStamp(Ljava/util/Date;)V
    .locals 0
    .param p1, "timeStamp"    # Ljava/util/Date;

    .prologue
    .line 233
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/FollowersData;->timeStamp:Ljava/util/Date;

    .line 234
    return-void
.end method
