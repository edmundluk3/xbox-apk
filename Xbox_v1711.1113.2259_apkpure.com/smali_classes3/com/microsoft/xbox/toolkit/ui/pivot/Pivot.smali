.class public Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;
.super Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;
.source "Pivot.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;,
        Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;
    }
.end annotation


# static fields
.field private static final ANIMATION_MS:I = 0x96

.field private static final DISTANCE_TO_MAKE_DIRECTION_DECISION:I

.field private static final MIN_PANE_DISPLACEMENT_TO_ROTATE:F = 0.14285715f

.field private static final MIN_VEL_TO_FLICK:F = 500.0f

.field private static final NO_TOUCH_ID:I = -0x1

.field private static final TOUCH_BLOCK_TIMEOUT_MS:I = 0x7530


# instance fields
.field private animIn:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;

.field private animating:Z

.field private body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

.field private currentPaneIndex:I

.field private currentTouchId:I

.field private flickDebt:I

.field private flickState:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;

.field private interceptingDecision:Z

.field private onCurrentPivotPaneChanged:Ljava/lang/Runnable;

.field private prevTime:J

.field private prevX:F

.field private prevY:F

.field private scrolling:Z

.field private scrollingRunner:Ljava/lang/Runnable;

.field private startPaneIndex:I

.field touchX0:F

.field touchY0:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const/high16 v0, 0x40a00000    # 5.0f

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->DIPtoPixels(F)I

    move-result v0

    sput v0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->DISTANCE_TO_MAKE_DIRECTION_DECISION:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/high16 v1, -0x40800000    # -1.0f

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 97
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;-><init>(Landroid/content/Context;)V

    .line 68
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    .line 69
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animIn:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;

    .line 70
    iput-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->scrolling:Z

    .line 71
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->scrollingRunner:Ljava/lang/Runnable;

    .line 72
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->onCurrentPivotPaneChanged:Ljava/lang/Runnable;

    .line 75
    iput-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->interceptingDecision:Z

    .line 78
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->touchX0:F

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->touchY0:F

    .line 80
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentTouchId:I

    .line 81
    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->prevX:F

    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->prevY:F

    .line 82
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->prevTime:J

    .line 83
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;->PIVOT_FLICK_NONE:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->flickState:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;

    .line 86
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    .line 87
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->startPaneIndex:I

    .line 88
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->flickDebt:I

    .line 89
    iput-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animating:Z

    .line 100
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    const/high16 v1, -0x40800000    # -1.0f

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 110
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 68
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    .line 69
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animIn:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;

    .line 70
    iput-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->scrolling:Z

    .line 71
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->scrollingRunner:Ljava/lang/Runnable;

    .line 72
    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->onCurrentPivotPaneChanged:Ljava/lang/Runnable;

    .line 75
    iput-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->interceptingDecision:Z

    .line 78
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->touchX0:F

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->touchY0:F

    .line 80
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentTouchId:I

    .line 81
    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->prevX:F

    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->prevY:F

    .line 82
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->prevTime:J

    .line 83
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;->PIVOT_FLICK_NONE:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->flickState:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;

    .line 86
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    .line 87
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->startPaneIndex:I

    .line 88
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->flickDebt:I

    .line 89
    iput-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animating:Z

    .line 111
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;
    .param p1, "x1"    # Z

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->onPivotAnimationEnd(Z)V

    return-void
.end method

.method private animateCancelIfNecessary()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 416
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animating:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 418
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->getPanningDelta()I

    move-result v0

    if-eqz v0, :cond_1

    .line 419
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animateCancelInternal()V

    .line 423
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 416
    goto :goto_0

    .line 421
    :cond_1
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->onPivotAnimationEnd(Z)V

    goto :goto_1
.end method

.method private animateCancelInternal()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 541
    iget-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animating:Z

    if-nez v2, :cond_2

    move v2, v3

    :goto_0
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 542
    iput-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animating:Z

    .line 546
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->getScrollX()I

    move-result v2

    int-to-float v1, v2

    .line 547
    .local v1, "startX":F
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/MainActivity;->getScreenWidth()I

    move-result v5

    mul-int/2addr v2, v5

    int-to-float v0, v2

    .line 549
    .local v0, "endX":F
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animIn:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animIn:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;->hasEnded()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v4, v3

    :cond_1
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 551
    const-string v2, "PIVOT"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "create cancel animation from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-direct {v2, v3, v1, v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;-><init>(Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;FF)V

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animIn:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;

    .line 554
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animIn:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;

    const-wide/16 v4, 0x96

    invoke-virtual {v2, v4, v5}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;->setDuration(J)V

    .line 555
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animIn:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;

    new-instance v3, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 556
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animIn:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$2;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$2;-><init>(Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;)V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;->setAnimationEndPostRunnable(Ljava/lang/Runnable;)V

    .line 562
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animIn:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->startAnimation(Landroid/view/animation/Animation;)V

    .line 563
    return-void

    .end local v0    # "endX":F
    .end local v1    # "startX":F
    :cond_2
    move v2, v4

    .line 541
    goto :goto_0
.end method

.method private animateFlickIfNecessary()V
    .locals 1

    .prologue
    .line 393
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;->LINEAR:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animateFlickIfNecessary(Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;)V

    .line 394
    return-void
.end method

.method private animateFlickIfNecessary(Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;)V
    .locals 4
    .param p1, "animationType"    # Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 397
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animating:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 399
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->flickDebt:I

    if-eqz v0, :cond_0

    .line 401
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->flickDebt:I

    add-int/2addr v0, v3

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    .line 404
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    if-ltz v0, :cond_2

    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    :goto_1
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 406
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->flickDebt:I

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animateOutInternal(ILcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;)V

    .line 408
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->flickDebt:I

    .line 410
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 397
    goto :goto_0

    :cond_2
    move v1, v2

    .line 404
    goto :goto_1
.end method

.method private animateOutInternal(I)V
    .locals 1
    .param p1, "animateDirection"    # I

    .prologue
    .line 534
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;->LINEAR:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animateOutInternal(ILcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;)V

    .line 535
    return-void
.end method

.method private animateOutInternal(ILcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;)V
    .locals 6
    .param p1, "animateDirection"    # I
    .param p2, "animationType"    # Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;

    .prologue
    const/4 v3, 0x1

    .line 496
    iget-boolean v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animating:Z

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 497
    iput-boolean v3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animating:Z

    .line 501
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->getScrollX()I

    move-result v2

    int-to-float v1, v2

    .line 502
    .local v1, "startX":F
    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getScreenWidth()I

    move-result v3

    mul-int/2addr v2, v3

    int-to-float v0, v2

    .line 503
    .local v0, "endX":F
    const-string v2, "PIVOT"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "create out animation from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-direct {v2, v3, v1, v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;-><init>(Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;FF)V

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animIn:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;

    .line 507
    sget-object v2, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$4;->$SwitchMap$com$microsoft$xbox$toolkit$ui$pivot$Pivot$PaneAnimationType:[I

    invoke-virtual {p2}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 520
    :goto_1
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animIn:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;

    new-instance v3, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 521
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animIn:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$1;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$1;-><init>(Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;)V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;->setAnimationEndPostRunnable(Ljava/lang/Runnable;)V

    .line 528
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animIn:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;

    const-wide/16 v4, 0x96

    invoke-virtual {v2, v4, v5}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;->setDuration(J)V

    .line 530
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animIn:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->startAnimation(Landroid/view/animation/Animation;)V

    .line 531
    return-void

    .line 496
    .end local v0    # "endX":F
    .end local v1    # "startX":F
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 509
    .restart local v0    # "endX":F
    .restart local v1    # "startX":F
    :pswitch_0
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animIn:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;

    new-instance v3, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    goto :goto_1

    .line 513
    :pswitch_1
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animIn:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;

    new-instance v3, Lcom/microsoft/xbox/toolkit/anim/ExponentialInterpolator;

    const/high16 v4, 0x40e00000    # 7.0f

    sget-object v5, Lcom/microsoft/xbox/toolkit/anim/EasingMode;->EaseOut:Lcom/microsoft/xbox/toolkit/anim/EasingMode;

    invoke-direct {v3, v4, v5}, Lcom/microsoft/xbox/toolkit/anim/ExponentialInterpolator;-><init>(FLcom/microsoft/xbox/toolkit/anim/EasingMode;)V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    goto :goto_1

    .line 507
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getPanningDelta()I
    .locals 4

    .prologue
    .line 474
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->getScrollX()I

    move-result v1

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getScreenWidth()I

    move-result v2

    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    mul-int/2addr v2, v3

    sub-int v0, v1, v2

    .line 475
    .local v0, "panningDelta":I
    return v0
.end method

.method private handleTouch(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, -0x1

    .line 166
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->prevTime:J

    sub-long/2addr v4, v6

    long-to-float v1, v4

    .line 167
    .local v1, "dt":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->prevTime:J

    .line 169
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 171
    .local v0, "action":I
    and-int/lit16 v4, v0, 0xff

    packed-switch v4, :pswitch_data_0

    .line 204
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v4, 0x1

    return v4

    .line 173
    :pswitch_1
    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentTouchId:I

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v3

    .line 174
    .local v3, "index":I
    invoke-direct {p0, p1, v3, v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->onTouchMove(Landroid/view/MotionEvent;IF)V

    goto :goto_0

    .line 180
    .end local v3    # "index":I
    :pswitch_2
    iput v8, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentTouchId:I

    .line 182
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->onTouchRelease()V

    goto :goto_0

    .line 187
    :pswitch_3
    const v4, 0xff00

    and-int/2addr v4, v0

    shr-int/lit8 v3, v4, 0x8

    .line 188
    .restart local v3    # "index":I
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    .line 192
    .local v2, "id":I
    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentTouchId:I

    if-ne v2, v4, :cond_0

    .line 193
    iput v8, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentTouchId:I

    .line 196
    invoke-direct {p0, p1, v3, v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->onTouchMove(Landroid/view/MotionEvent;IF)V

    .line 198
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->onTouchRelease()V

    goto :goto_0

    .line 171
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private onPivotAnimationEnd(Z)V
    .locals 2
    .param p1, "indexChanged"    # Z

    .prologue
    const/4 v1, 0x0

    .line 479
    iput-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animating:Z

    .line 480
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->flickDebt:I

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 482
    if-eqz p1, :cond_0

    .line 483
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->setCurrentPaneIndex(I)V

    .line 486
    :cond_0
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->setScrolling(Z)V

    .line 487
    return-void

    :cond_1
    move v0, v1

    .line 480
    goto :goto_0
.end method

.method private onTouchDown(F)V
    .locals 1
    .param p1, "dt"    # F

    .prologue
    .line 286
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->updateFlickState(FF)V

    .line 287
    return-void
.end method

.method private onTouchMove(Landroid/view/MotionEvent;IF)V
    .locals 5
    .param p1, "ev"    # Landroid/view/MotionEvent;
    .param p2, "index"    # I
    .param p3, "dt"    # F

    .prologue
    .line 298
    const/4 v4, -0x1

    if-eq p2, v4, :cond_0

    .line 299
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    .line 300
    .local v2, "x":F
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    .line 302
    .local v3, "y":F
    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->prevX:F

    sub-float v0, v2, v4

    .line 303
    .local v0, "dx":F
    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->prevY:F

    sub-float v1, v3, v4

    .line 306
    .local v1, "dy":F
    neg-float v4, v0

    invoke-direct {p0, v4, v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->pivotTranslate(FF)V

    .line 310
    invoke-direct {p0, v0, p3}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->updateFlickState(FF)V

    .line 312
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->prevX:F

    .line 313
    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->prevY:F

    .line 315
    .end local v0    # "dx":F
    .end local v1    # "dy":F
    .end local v2    # "x":F
    .end local v3    # "y":F
    :cond_0
    return-void
.end method

.method private onTouchRelease()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 323
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animating:Z

    if-nez v0, :cond_0

    .line 325
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$4;->$SwitchMap$com$microsoft$xbox$toolkit$ui$pivot$Pivot$PivotFlickState:[I

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->flickState:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 350
    :cond_0
    :goto_0
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->flickDebt:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    :cond_1
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->flickDebt:I

    if-gez v0, :cond_3

    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    if-gtz v0, :cond_3

    .line 351
    :cond_2
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->flickDebt:I

    .line 355
    :cond_3
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animating:Z

    if-nez v0, :cond_4

    .line 356
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animateFlickIfNecessary()V

    .line 359
    :cond_4
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animating:Z

    if-nez v0, :cond_5

    .line 360
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animateCancelIfNecessary()V

    .line 362
    :cond_5
    return-void

    .line 327
    :pswitch_0
    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->flickDebt:I

    .line 328
    const-string v0, "PIVOT"

    const-string v1, "FLICK left"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 333
    :pswitch_1
    const-string v0, "PIVOT"

    const-string v1, "FLICK right"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    iput v4, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->flickDebt:I

    goto :goto_0

    .line 339
    :pswitch_2
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->getPanningDelta()I

    move-result v0

    int-to-float v0, v0

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getScreenWidth()I

    move-result v1

    int-to-float v1, v1

    const v2, -0x41edb6db

    mul-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_6

    .line 340
    iput v3, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->flickDebt:I

    goto :goto_0

    .line 341
    :cond_6
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->getPanningDelta()I

    move-result v0

    int-to-float v0, v0

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getScreenWidth()I

    move-result v1

    int-to-float v1, v1

    const v2, 0x3e124925

    mul-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 342
    iput v4, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->flickDebt:I

    goto :goto_0

    .line 325
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private pivotTranslate(FF)V
    .locals 2
    .param p1, "dx"    # F
    .param p2, "dy"    # F

    .prologue
    .line 459
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->setScrolling(Z)V

    .line 462
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animating:Z

    if-nez v0, :cond_0

    .line 464
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->getScrollX()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->setScrollX(I)V

    .line 466
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->invalidate()V

    .line 468
    :cond_0
    return-void
.end method

.method private resetAnimation()V
    .locals 1

    .prologue
    .line 676
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animIn:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;

    if-eqz v0, :cond_0

    .line 677
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animIn:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;->cancel()V

    .line 678
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animIn:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBodyAnimation;

    .line 680
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->onPivotAnimationEnd(Z)V

    .line 681
    return-void
.end method

.method private setScrolling(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 373
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->scrolling:Z

    if-eq v0, p1, :cond_0

    .line 374
    iput-boolean p1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->scrolling:Z

    .line 376
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->scrolling:Z

    if-eqz v0, :cond_1

    .line 377
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;->PivotScroll:Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;

    const/16 v2, 0x7530

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->setBlocking(Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;I)V

    .line 383
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->scrollingRunner:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->scrollingRunner:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 387
    :cond_0
    return-void

    .line 379
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;->PivotScroll:Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->clearBlocking(Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor$WaitType;)V

    goto :goto_0
.end method

.method private updateFlickState(FF)V
    .locals 3
    .param p1, "dx"    # F
    .param p2, "dt"    # F

    .prologue
    const/4 v2, 0x0

    .line 432
    cmpg-float v1, p1, v2

    if-gez v1, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->getPanningDelta()I

    move-result v1

    if-gtz v1, :cond_1

    :cond_0
    cmpl-float v1, p1, v2

    if-lez v1, :cond_2

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->getPanningDelta()I

    move-result v1

    if-gez v1, :cond_2

    .line 434
    :cond_1
    const/4 p1, 0x0

    .line 438
    :cond_2
    const/high16 v1, 0x447a0000    # 1000.0f

    div-float v1, p2, v1

    div-float v0, p1, v1

    .line 441
    .local v0, "pixelsPerSecond":F
    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;->PIVOT_FLICK_NONE:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->flickState:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;

    .line 443
    const/high16 v1, 0x43fa0000    # 500.0f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_4

    .line 445
    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;->PIVOT_FLICK_LEFT:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->flickState:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;

    .line 449
    :cond_3
    :goto_0
    return-void

    .line 446
    :cond_4
    const/high16 v1, -0x3c060000    # -500.0f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_3

    .line 447
    sget-object v1, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;->PIVOT_FLICK_RIGHT:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->flickState:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PivotFlickState;

    goto :goto_0
.end method


# virtual methods
.method public addPane(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;I)V
    .locals 1
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .param p2, "index"    # I

    .prologue
    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->addPivotPane(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;I)V

    .line 152
    return-void
.end method

.method public adjustBottomMargin(I)V
    .locals 1
    .param p1, "bottomMargin"    # I

    .prologue
    .line 717
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->adjustBottomMargin(I)V

    .line 718
    return-void
.end method

.method public adjustPaneSize(Ljava/lang/Class;I)V
    .locals 0
    .param p2, "percent"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 756
    .local p1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    return-void
.end method

.method public animateToCurrentPaneIndex(I)V
    .locals 1
    .param p1, "newPane"    # I

    .prologue
    .line 593
    sget-object v0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;->LINEAR:Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animateToCurrentPaneIndex(ILcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;)V

    .line 594
    return-void
.end method

.method public animateToCurrentPaneIndex(ILcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;)V
    .locals 2
    .param p1, "newPane"    # I
    .param p2, "animationType"    # Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;

    .prologue
    .line 597
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animating:Z

    if-nez v0, :cond_3

    .line 598
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    sub-int v0, p1, v0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->flickDebt:I

    .line 601
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->flickDebt:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->flickDebt:I

    if-gez v0, :cond_2

    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    if-gtz v0, :cond_2

    .line 602
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->flickDebt:I

    .line 606
    :cond_2
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animateFlickIfNecessary(Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$PaneAnimationType;)V

    .line 608
    :cond_3
    return-void
.end method

.method public forceRefresh()V
    .locals 1

    .prologue
    .line 760
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->getCurrentPane()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->forceRefresh()V

    .line 761
    return-void
.end method

.method public getAnimateIn(Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    .locals 2
    .param p1, "goingBack"    # Z

    .prologue
    .line 709
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->getPivotPane(I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getAnimateIn(Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;

    move-result-object v0

    return-object v0
.end method

.method public getAnimateOut(Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;
    .locals 2
    .param p1, "goingBack"    # Z

    .prologue
    .line 713
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->getPivotPane(I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->getAnimateOut(Z)Lcom/microsoft/xbox/toolkit/anim/XLEAnimationPackage;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentPane()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 2

    .prologue
    .line 620
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    if-eqz v0, :cond_0

    .line 621
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->getPivotPane(I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    .line 623
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentPaneIndex()I
    .locals 1

    .prologue
    .line 616
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    return v0
.end method

.method public getIndexOfScreen(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)I
    .locals 1
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 749
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->getIndexOfScreen(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)I

    move-result v0

    return v0
.end method

.method public getIndexOfScreen(Ljava/lang/Class;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 725
    .local p1, "screenClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->getIndexOfScreen(Ljava/lang/Class;)I

    move-result v0

    return v0
.end method

.method protected getInitialPanes()[Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 4

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->getChildCount()I

    move-result v3

    new-array v2, v3, [Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 142
    .local v2, "panes":[Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 143
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 144
    .local v1, "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    aput-object v1, v2, v0

    .line 142
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 146
    .end local v1    # "pane":Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    :cond_0
    return-object v2
.end method

.method public getIsScrolling()Z
    .locals 1

    .prologue
    .line 365
    iget-boolean v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->scrolling:Z

    return v0
.end method

.method public getTotalPaneCount()I
    .locals 1

    .prologue
    .line 628
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->size()I

    move-result v0

    return v0
.end method

.method protected initialize()V
    .locals 4

    .prologue
    .line 119
    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    .line 122
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->getInitialPanes()[Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v1

    .line 125
    .local v1, "panes":[Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->removeAllViews()V

    .line 128
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->initialize([Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 131
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 132
    .local v0, "bodyParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 133
    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 134
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {p0, v2, v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 136
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->setScrolling(Z)V

    .line 137
    return-void
.end method

.method public onAnimateInCompleted()V
    .locals 4

    .prologue
    .line 662
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-direct {v0, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 663
    .local v0, "bodyWeakPtr":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;>;"
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    .line 664
    .local v1, "current":I
    invoke-static {}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->getInstance()Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;

    move-result-object v2

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$3;

    invoke-direct {v3, p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot$3;-><init>(Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;Ljava/lang/ref/WeakReference;I)V

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/BackgroundThreadWaitor;->postRunnableAfterReady(Ljava/lang/Runnable;)V

    .line 673
    return-void
.end method

.method public onAnimateInStarted()V
    .locals 1

    .prologue
    .line 658
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->onAnimateInStarted()V

    .line 659
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 740
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 636
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->onCreate()V

    .line 637
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 744
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v0, p1, p2, p3}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 745
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 705
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->onDestroy()V

    .line 706
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 214
    const/4 v2, 0x0

    .line 216
    .local v2, "intercepting":Z
    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v9

    iput v9, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentTouchId:I

    .line 217
    iget v9, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentTouchId:I

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v1

    .line 219
    .local v1, "index":I
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    .line 220
    .local v5, "x":F
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    .line 222
    .local v6, "y":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    if-nez v9, :cond_0

    .line 223
    iput v5, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->touchX0:F

    .line 224
    iput v6, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->touchY0:F

    .line 226
    iput-boolean v8, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->interceptingDecision:Z

    .line 228
    const/high16 v9, 0x3f800000    # 1.0f

    invoke-direct {p0, v9}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->onTouchDown(F)V

    .line 231
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    const/4 v10, 0x2

    if-eq v9, v10, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    if-nez v9, :cond_2

    .line 232
    :cond_1
    iput v5, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->prevX:F

    .line 233
    iput v6, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->prevY:F

    .line 238
    :cond_2
    iget-boolean v9, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->interceptingDecision:Z

    if-nez v9, :cond_3

    .line 239
    iget v9, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->touchX0:F

    sub-float v3, v5, v9

    .line 240
    .local v3, "totalX":F
    iget v9, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->touchY0:F

    sub-float v4, v6, v9

    .line 242
    .local v4, "totalY":F
    mul-float v9, v3, v3

    mul-float v10, v4, v4

    add-float/2addr v9, v10

    float-to-int v0, v9

    .line 244
    .local v0, "distSq":I
    sget v9, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->DISTANCE_TO_MAKE_DIRECTION_DECISION:I

    sget v10, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->DISTANCE_TO_MAKE_DIRECTION_DECISION:I

    mul-int/2addr v9, v10

    if-le v0, v9, :cond_3

    .line 245
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v9

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v10

    cmpg-float v9, v9, v10

    if-gez v9, :cond_4

    move v2, v7

    .line 246
    :goto_0
    iput-boolean v7, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->interceptingDecision:Z

    .line 250
    .end local v0    # "distSq":I
    .end local v3    # "totalX":F
    .end local v4    # "totalY":F
    :cond_3
    return v2

    .restart local v0    # "distSq":I
    .restart local v3    # "totalX":F
    .restart local v4    # "totalY":F
    :cond_4
    move v2, v8

    .line 245
    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 648
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->resetAnimation()V

    .line 649
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->onPause()V

    .line 650
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 688
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->onRehydrate()V

    .line 689
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 653
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->resetAnimation()V

    .line 654
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->onResume()V

    .line 655
    return-void
.end method

.method public onSetActive()V
    .locals 1

    .prologue
    .line 697
    iget v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->setCurrentPaneIndex(I)V

    .line 698
    return-void
.end method

.method public onSetActive(I)V
    .locals 0
    .param p1, "pivotPaneIndex"    # I

    .prologue
    .line 692
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    .line 693
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->onSetActive()V

    .line 694
    return-void
.end method

.method public onSetInactive()V
    .locals 1

    .prologue
    .line 701
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->setInactive()V

    .line 702
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 640
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->startPaneIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->onStart(I)V

    .line 641
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 644
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->onStop()V

    .line 645
    return-void
.end method

.method public onTombstone()V
    .locals 1

    .prologue
    .line 684
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->onTombstone()V

    .line 685
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 261
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 263
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->handleTouch(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 265
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected overrideTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 271
    invoke-super {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/MultiPaneScreen/MultiPaneScreen;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public removePane(I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 156
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->removePivotPane(I)Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    return-object v0
.end method

.method public resetBottomMargin()V
    .locals 1

    .prologue
    .line 721
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->resetBottomMargin()V

    .line 722
    return-void
.end method

.method public setActiveOnAdd(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 0
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 771
    return-void
.end method

.method public setCurrentPaneIndex(I)V
    .locals 3
    .param p1, "newPane"    # I

    .prologue
    .line 571
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    .line 572
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->setActivePivotPane(I)V

    .line 573
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getScreenWidth()I

    move-result v1

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    mul-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->setScrollX(I)V

    .line 578
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->onCurrentPivotPaneChanged:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 579
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->onCurrentPivotPaneChanged:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 581
    :cond_0
    return-void
.end method

.method public setOnCurrentPaneChangedRunnable(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 632
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->onCurrentPivotPaneChanged:Ljava/lang/Runnable;

    .line 633
    return-void
.end method

.method public setOnScrollingChangedRunnable(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "runner"    # Ljava/lang/Runnable;

    .prologue
    .line 369
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->scrollingRunner:Ljava/lang/Runnable;

    .line 370
    return-void
.end method

.method public setScreenState(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 765
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->getCurrentPane()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->setScreenState(I)V

    .line 766
    return-void
.end method

.method public setStartPaneIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 589
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->startPaneIndex:I

    .line 590
    return-void
.end method

.method public switchToPane(I)V
    .locals 3
    .param p1, "newIndex"    # I

    .prologue
    .line 729
    iget-boolean v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->animating:Z

    if-nez v1, :cond_0

    if-ltz p1, :cond_0

    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    if-eq p1, v1, :cond_0

    .line 730
    iput p1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    .line 731
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getScreenWidth()I

    move-result v2

    mul-int/2addr v1, v2

    int-to-float v0, v1

    .line 733
    .local v0, "endX":F
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->body:Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;

    float-to-int v2, v0

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/pivot/PivotBody;->setScrollX(I)V

    .line 735
    iget v1, p0, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->currentPaneIndex:I

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;->setCurrentPaneIndex(I)V

    .line 737
    .end local v0    # "endX":F
    :cond_0
    return-void
.end method
