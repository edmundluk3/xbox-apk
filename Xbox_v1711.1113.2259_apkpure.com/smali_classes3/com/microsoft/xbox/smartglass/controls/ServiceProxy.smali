.class Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;
.super Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;
.source "ServiceProxy.java"


# static fields
.field public static final ComponentName:Ljava/lang/String; = "ServiceProxy"

.field private static final SendRequestMethod:Ljava/lang/String; = "SendRequest"


# instance fields
.field private final _tracker:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V
    .locals 1
    .param p1, "canvasState"    # Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    .prologue
    .line 24
    const-string v0, "ServiceProxy"

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    .line 26
    new-instance v0, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;

    invoke-direct {v0}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->_tracker:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;

    .line 28
    const-string v0, "SendRequest"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->registerMethod(Ljava/lang/String;)V

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;)Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->_tracker:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;

    return-object v0
.end method

.method private isAllowedUrl(Landroid/net/Uri;)Z
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 121
    sget-boolean v4, Lcom/microsoft/xbox/smartglass/controls/CanvasFragment;->IsSmartGlassStudioRunning:Z

    if-eqz v4, :cond_1

    .line 134
    :cond_0
    :goto_0
    return v2

    .line 125
    :cond_1
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->_canvasState:Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    iget-object v5, v4, Lcom/microsoft/xbox/smartglass/controls/CanvasState;->allowedUrlPrefixes:[Ljava/lang/String;

    array-length v6, v5

    move v4, v3

    :goto_1
    if-ge v4, v6, :cond_3

    aget-object v1, v5, v4

    .line 126
    .local v1, "allowedUrlPrefix":Ljava/lang/String;
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 129
    .local v0, "allowedUri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v8

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v8, v9}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 125
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .end local v0    # "allowedUri":Landroid/net/Uri;
    .end local v1    # "allowedUrlPrefix":Ljava/lang/String;
    :cond_3
    move v2, v3

    .line 134
    goto :goto_0
.end method

.method private sendRequest(ILcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;)V
    .locals 7
    .param p1, "requestId"    # I
    .param p2, "jsonRequest"    # Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;

    .prologue
    .line 55
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v4

    const-string v5, "SendRequest"

    invoke-virtual {p0, v5}, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->getMetricName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, p1}, Lcom/microsoft/xbox/smartglass/MetricsManager;->start(Ljava/lang/String;I)V

    .line 57
    move v0, p1

    .line 61
    .local v0, "currentRequestId":I
    :try_start_0
    iget-object v4, p2, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->url:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 62
    .local v3, "uri":Landroid/net/Uri;
    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    const-string v5, "http"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    const-string v5, "https"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 63
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v5, "Only http or https are supported."

    invoke-interface {v4, p1, v5}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    .line 118
    .end local v3    # "uri":Landroid/net/Uri;
    :goto_0
    return-void

    .line 68
    .restart local v3    # "uri":Landroid/net/Uri;
    :cond_0
    invoke-direct {p0, v3}, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->isAllowedUrl(Landroid/net/Uri;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 70
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTraceLog()Lcom/microsoft/xbox/smartglass/TraceLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p2, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->url:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " was not found in the allowed URL list."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/smartglass/TraceLogLevel;->Error:Lcom/microsoft/xbox/smartglass/TraceLogLevel;

    invoke-virtual {v4, v5, v6}, Lcom/microsoft/xbox/smartglass/TraceLog;->write(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/TraceLogLevel;)V

    .line 72
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v5, "URL is not a valid form or does not have a corresponding entry in the allowed URL list."

    invoke-interface {v4, p1, v5}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 80
    .end local v3    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v1

    .line 81
    .local v1, "e":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v5, "Invalid URL."

    invoke-interface {v4, p1, v5}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0

    .line 76
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v3    # "uri":Landroid/net/Uri;
    :cond_1
    :try_start_1
    iget-object v4, p2, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->sendUserToken:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    const-string v5, "https"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 77
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v5, "https is required when sendUserToken is true."

    invoke-interface {v4, p1, v5}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 85
    :cond_2
    iget-object v4, p2, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->sendUserToken:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p2, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;->audienceUri:Ljava/lang/String;

    if-nez v4, :cond_3

    .line 86
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v5, "audienceUri is required when sendUserToken is true."

    invoke-interface {v4, p1, v5}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0

    .line 90
    :cond_3
    new-instance v2, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;

    new-instance v4, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy$1;

    invoke-direct {v4, p0, v0}, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy$1;-><init>(Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;I)V

    invoke-direct {v2, p2, v4}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;-><init>(Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;Lcom/microsoft/xbox/smartglass/controls/ServiceRequestListener;)V

    .line 116
    .local v2, "request":Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;
    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->_tracker:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;

    invoke-virtual {v4, v2}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;->add(Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;)V

    .line 117
    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequest;->runAsync()V

    goto :goto_0
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .prologue
    .line 33
    invoke-super {p0}, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->dispose()V

    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->_tracker:Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/controls/ServiceRequestTracker;->cancelAll()V

    .line 35
    return-void
.end method

.method public invoke(ILjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "requestId"    # I
    .param p2, "methodName"    # Ljava/lang/String;
    .param p3, "json"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->validateMethod(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 40
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->failRequestUnknownMethod(ILjava/lang/String;)V

    .line 52
    :goto_0
    return-void

    .line 47
    :cond_0
    :try_start_0
    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;

    invoke-direct {v1, p3}, Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;-><init>(Ljava/lang/String;)V

    .line 48
    .local v1, "request":Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;
    invoke-direct {p0, p1, v1}, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->sendRequest(ILcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 49
    .end local v1    # "request":Lcom/microsoft/xbox/smartglass/controls/JsonServiceRequest;
    :catch_0
    move-exception v0

    .line 50
    .local v0, "ex":Lorg/json/JSONException;
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/ServiceProxy;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid JSON. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, p1, v3}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0
.end method
