.class public Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;
.super Lcom/microsoft/xbox/xle/app/XLEUtil$BaseHolder;
.source "GameAchievementComparisonListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StatisticsComparisonViewHolder"
.end annotation


# instance fields
.field private heroStatMeValue:Landroid/widget/TextView;

.field private heroStatProgressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

.field private heroStatTitle:Landroid/widget/TextView;

.field private heroStatYouValue:Landroid/widget/TextView;

.field private narratorContent:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/XLEUtil$BaseHolder;-><init>()V

    return-void
.end method

.method private getHeroStatValue(Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)J
    .locals 6
    .param p1, "stats"    # Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .prologue
    const-wide/16 v2, 0x0

    .line 202
    if-nez p1, :cond_1

    move-wide v0, v2

    .line 216
    :cond_0
    :goto_0
    return-wide v0

    .line 206
    :cond_1
    const-wide/16 v0, 0x0

    .line 208
    .local v0, "value":J
    iget-object v4, p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->type:Ljava/lang/String;

    const-string v5, "Integer"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 209
    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseInteger(Ljava/lang/String;I)I

    move-result v2

    int-to-long v0, v2

    goto :goto_0

    .line 210
    :cond_2
    iget-object v4, p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->type:Ljava/lang/String;

    const-string v5, "Double"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 211
    iget-object v2, p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    const-wide/16 v4, 0x0

    invoke-static {v2, v4, v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseDouble(Ljava/lang/String;D)D

    move-result-wide v2

    double-to-long v0, v2

    goto :goto_0

    .line 212
    :cond_3
    iget-object v4, p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->type:Ljava/lang/String;

    const-string v5, "Long"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 213
    iget-object v4, p1, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    invoke-static {v4, v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseLong(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method private getProgressPercentage(Ljava/lang/String;)I
    .locals 8
    .param p1, "percentage"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x64

    .line 188
    const-wide/16 v4, 0x0

    invoke-static {p1, v4, v5}, Lcom/microsoft/xbox/toolkit/JavaUtil;->tryParseDouble(Ljava/lang/String;D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    .line 190
    .local v1, "percent":Ljava/lang/Double;
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpl-double v3, v4, v6

    if-lez v3, :cond_1

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v0, v4

    .line 191
    .local v0, "finalPercent":I
    :goto_0
    if-ltz v0, :cond_2

    .line 192
    if-le v0, v2, :cond_0

    move v0, v2

    .line 198
    .end local v0    # "finalPercent":I
    :cond_0
    :goto_1
    return v0

    .line 190
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    goto :goto_0

    .line 198
    .restart local v0    # "finalPercent":I
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static inflateView(Landroid/content/Context;)Landroid/view/View;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x0

    .line 110
    const-string v4, "layout_inflater"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 111
    .local v3, "vi":Landroid/view/LayoutInflater;
    const v4, 0x7f03010d

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 112
    .local v2, "v":Landroid/view/View;
    invoke-virtual {v2, v6, v6, v6, v6}, Landroid/view/View;->setPadding(IIII)V

    .line 114
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;

    invoke-direct {v1}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;-><init>()V

    .line 115
    .local v1, "holder":Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;
    const v4, 0x7f0e05e5

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->heroStatTitle:Landroid/widget/TextView;

    .line 116
    const v4, 0x7f0e05e7

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->heroStatYouValue:Landroid/widget/TextView;

    .line 117
    const v4, 0x7f0e05e6

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->heroStatMeValue:Landroid/widget/TextView;

    .line 118
    const v4, 0x7f0e05e8

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    iput-object v4, v1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->heroStatProgressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    .line 119
    const v4, 0x7f0e05e4

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 120
    .local v0, "compareStatisticsItemContainer":Landroid/widget/LinearLayout;
    invoke-static {v0, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 122
    invoke-virtual {v2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 124
    return-object v2
.end method

.method private setHeroStatisticsTextView(Landroid/widget/TextView;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V
    .locals 11
    .param p1, "heroStatTextView"    # Landroid/widget/TextView;
    .param p2, "meHeroStat"    # Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;
    .param p3, "youHeroStat"    # Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    .prologue
    const/16 v6, 0x8

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 152
    if-eqz p2, :cond_3

    iget-object v4, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    sget-object v5, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->GameProgress:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 153
    iget-object v4, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->getProgressPercentage(Ljava/lang/String;)I

    move-result v1

    .line 155
    .local v1, "percentageProgress":I
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v4}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f07034b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 156
    invoke-static {p1, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 157
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->heroStatMeValue:Landroid/widget/TextView;

    if-lez v1, :cond_1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    const-string v6, "%d%%"

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v4, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    :goto_0
    invoke-static {v5, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 159
    if-eqz p3, :cond_0

    .line 160
    iget-object v4, p3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->getProgressPercentage(Ljava/lang/String;)I

    move-result v3

    .line 161
    .local v3, "youPercentageProgress":I
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->heroStatYouValue:Landroid/widget/TextView;

    if-lez v3, :cond_2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    const-string v6, "%d%%"

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v4, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-static {v5, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 185
    .end local v1    # "percentageProgress":I
    .end local v3    # "youPercentageProgress":I
    :cond_0
    :goto_2
    return-void

    .line 157
    .restart local v1    # "percentageProgress":I
    :cond_1
    const-string v4, "--"

    goto :goto_0

    .line 161
    .restart local v3    # "youPercentageProgress":I
    :cond_2
    const-string v4, "--"

    goto :goto_1

    .line 163
    .end local v1    # "percentageProgress":I
    .end local v3    # "youPercentageProgress":I
    :cond_3
    if-eqz p2, :cond_6

    iget-object v4, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->name:Ljava/lang/String;

    sget-object v5, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->MinutesPlayed:Lcom/microsoft/xbox/service/model/sls/StatisticsType;

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/model/sls/StatisticsType;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 164
    iget-object v4, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->convertFromMinuteToHours(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 165
    .local v0, "minutesPlayed":Ljava/lang/String;
    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v4}, Lcom/microsoft/xbox/XLEApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f07034c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 166
    invoke-static {p1, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 167
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->heroStatMeValue:Landroid/widget/TextView;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .end local v0    # "minutesPlayed":Ljava/lang/String;
    :goto_3
    invoke-static {v4, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 169
    if-eqz p3, :cond_0

    .line 170
    iget-object v4, p3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    invoke-static {v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->convertFromMinuteToHours(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 171
    .local v2, "youMinutesPlayed":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->heroStatYouValue:Landroid/widget/TextView;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    .end local v2    # "youMinutesPlayed":Ljava/lang/String;
    :goto_4
    invoke-static {v4, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 167
    .restart local v0    # "minutesPlayed":Ljava/lang/String;
    :cond_4
    const-string v0, "--"

    goto :goto_3

    .line 171
    .end local v0    # "minutesPlayed":Ljava/lang/String;
    .restart local v2    # "youMinutesPlayed":Ljava/lang/String;
    :cond_5
    const-string v2, "--"

    goto :goto_4

    .line 173
    .end local v2    # "youMinutesPlayed":Ljava/lang/String;
    :cond_6
    if-eqz p2, :cond_9

    iget-object v4, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->groupproperties:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;

    if-eqz v4, :cond_9

    iget-object v4, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->groupproperties:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;

    iget-object v4, v4, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;->DisplayName:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 174
    iget-object v4, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->groupproperties:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;

    iget-object v4, v4, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$GroupProperties;->DisplayName:Ljava/lang/String;

    invoke-static {p1, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 175
    invoke-static {p1, v9}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 176
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->heroStatMeValue:Landroid/widget/TextView;

    iget-object v4, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    iget-object v4, p2, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    :goto_5
    invoke-static {v5, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 178
    if-eqz p3, :cond_0

    .line 179
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->heroStatYouValue:Landroid/widget/TextView;

    iget-object v4, p3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    iget-object v4, p3, Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;->value:Ljava/lang/String;

    :goto_6
    invoke-static {v5, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateAndShowTextViewUnlessEmpty(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 176
    :cond_7
    const-string v4, "--"

    goto :goto_5

    .line 179
    :cond_8
    const-string v4, "--"

    goto :goto_6

    .line 182
    :cond_9
    invoke-static {p1, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 183
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->heroStatMeValue:Landroid/widget/TextView;

    invoke-static {v4, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto/16 :goto_2
.end method


# virtual methods
.method public getNarratorContent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->narratorContent:Ljava/lang/String;

    return-object v0
.end method

.method public updateUI(Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;)V
    .locals 10
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;
    .param p2, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    .prologue
    const v9, 0x7f070d48

    .line 128
    if-eqz p1, :cond_0

    iget-object v3, p1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;->meSatistics:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    if-eqz v3, :cond_0

    iget-object v3, p1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;->youSatistics:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    if-eqz v3, :cond_0

    .line 130
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->heroStatTitle:Landroid/widget/TextView;

    iget-object v4, p1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;->meSatistics:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    iget-object v5, p1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;->youSatistics:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    invoke-direct {p0, v3, v4, v5}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->setHeroStatisticsTextView(Landroid/widget/TextView;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)V

    .line 133
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v2

    .line 135
    .local v2, "preferedColor":I
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->heroStatProgressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    if-eqz v3, :cond_0

    .line 136
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->heroStatProgressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setLeftBarColor(I)V

    .line 137
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->heroStatProgressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getYouPreferredColor()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setRightBarColor(I)V

    .line 138
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->heroStatProgressBar:Lcom/microsoft/xbox/xle/ui/ComparisonBar;

    iget-object v4, p1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;->meSatistics:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->getHeroStatValue(Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)J

    move-result-wide v4

    iget-object v6, p1, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$TitleStatItem;->youSatistics:Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;

    invoke-direct {p0, v6}, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->getHeroStatValue(Lcom/microsoft/xbox/service/network/managers/ProfileStatisticsResultContainer$Statistics;)J

    move-result-wide v6

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/microsoft/xbox/xle/ui/ComparisonBar;->setValues(JJ)V

    .line 142
    .end local v2    # "preferedColor":I
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->heroStatMeValue:Landroid/widget/TextView;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNull(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 143
    .local v0, "heroStatMeContent":Ljava/lang/CharSequence;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->heroStatYouValue:Landroid/widget/TextView;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->retrieveTextIfNotNull(Landroid/widget/TextView;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 144
    .local v1, "heroStatYouContent":Ljava/lang/CharSequence;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    const-string v4, "%1$s, %2$s %3$s, %4$s %5$s"

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->heroStatTitle:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v8, 0x7f07034d

    .line 145
    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string v7, "--"

    .line 146
    invoke-virtual {v0, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .end local v0    # "heroStatMeContent":Ljava/lang/CharSequence;
    :goto_0
    aput-object v0, v5, v6

    const/4 v6, 0x3

    .line 147
    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getYouGamerTag()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const-string v7, "--"

    .line 148
    invoke-virtual {v1, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .end local v1    # "heroStatYouContent":Ljava/lang/CharSequence;
    :goto_1
    aput-object v1, v5, v6

    .line 144
    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/GameAchievementComparisonListAdapter$StatisticsComparisonViewHolder;->narratorContent:Ljava/lang/String;

    .line 149
    return-void

    .line 146
    .restart local v0    # "heroStatMeContent":Ljava/lang/CharSequence;
    .restart local v1    # "heroStatYouContent":Ljava/lang/CharSequence;
    :cond_1
    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 148
    .end local v0    # "heroStatMeContent":Ljava/lang/CharSequence;
    :cond_2
    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
