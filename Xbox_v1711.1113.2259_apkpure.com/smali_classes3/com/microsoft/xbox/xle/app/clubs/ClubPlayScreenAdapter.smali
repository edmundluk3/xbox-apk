.class public Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;
.source "ClubPlayScreenAdapter.java"


# instance fields
.field private final createLfg:Lcom/microsoft/xbox/xle/ui/IconFontButton;

.field private final errorScreenText:Landroid/widget/TextView;

.field private final invalidScreenReason:Landroid/widget/TextView;

.field private final listAdapter:Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;

.field private final switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;)V
    .locals 4
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithRecyclerView;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 42
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 43
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;

    .line 45
    const v0, 0x7f0e0a95

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 46
    const v0, 0x7f0e03b7

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->setListView(Landroid/support/v7/widget/RecyclerView;)V

    .line 47
    new-instance v0, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;

    .line 48
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter$$Lambda$1;->lambdaFactory$()Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;-><init>(Landroid/content/Context;Lcom/microsoft/xbox/toolkit/generics/Action;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;

    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 61
    const v0, 0x7f0e03b8

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->createLfg:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->createLfg:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->createLfg:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->getLfgCreateRestriction()Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$ClubLfgCreateRestriction;

    move-result-object v0

    sget-object v2, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$ClubLfgCreateRestriction;->None:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$ClubLfgCreateRestriction;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setVisibility(I)V

    .line 64
    const v0, 0x7f0e02b5

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->invalidScreenReason:Landroid/widget/TextView;

    .line 65
    const v0, 0x7f0e03b6

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->errorScreenText:Landroid/widget/TextView;

    .line 66
    return-void

    .line 63
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V
    .locals 4
    .param p0, "handle"    # Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;

    .prologue
    .line 51
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->id()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 52
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 53
    .local v0, "activityParameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->activityInfo()Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$ActivityInfo;->titleId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putTitleId(J)V

    .line 54
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;->id()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putMultiplayerHandleId(Ljava/lang/String;)V

    .line 55
    invoke-static {p0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCLfg;->trackSelectLFG(Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$MultiplayerHandle;)V

    .line 56
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xle/app/lfg/LfgDetailsScreen;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3, v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 58
    .end local v0    # "activityParameters":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    :cond_0
    return-void
.end method

.method static synthetic lambda$new$1(Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->navigateToLfgCreate()V

    return-void
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;

    return-object v0
.end method

.method protected updateViewOverride()V
    .locals 6

    .prologue
    .line 70
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->isBusy()Z

    move-result v3

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->updateLoadingIndicator(Z)V

    .line 71
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 73
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->isBusy()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v3, v4, :cond_0

    .line 74
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->invalidScreenReason:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->getInvalidReasonText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->getData()Ljava/util/List;

    move-result-object v0

    .line 77
    .local v0, "allItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->getDataCopy()Ljava/util/List;

    move-result-object v2

    .line 79
    .local v2, "oldItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->clear()V

    .line 80
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;->addAll(Ljava/util/Collection;)V

    .line 82
    new-instance v3, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;

    invoke-direct {v3, v2, v0}, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-static {v3}, Landroid/support/v7/util/DiffUtil;->calculateDiff(Landroid/support/v7/util/DiffUtil$Callback;)Landroid/support/v7/util/DiffUtil$DiffResult;

    move-result-object v1

    .line 83
    .local v1, "diffResult":Landroid/support/v7/util/DiffUtil$DiffResult;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter;

    invoke-virtual {v1, v3}, Landroid/support/v7/util/DiffUtil$DiffResult;->dispatchUpdatesTo(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 86
    .end local v0    # "allItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;>;"
    .end local v1    # "diffResult":Landroid/support/v7/util/DiffUtil$DiffResult;
    .end local v2    # "oldItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/lfg/PartyAndLfgListViewAdapter$IPartyAndLfgListItem;>;"
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v3, v4, :cond_1

    .line 87
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel;->getLfgCreateRestriction()Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$ClubLfgCreateRestriction;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$ClubLfgCreateRestriction;->Privacy:Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenViewModel$ClubLfgCreateRestriction;

    if-ne v3, v4, :cond_2

    .line 88
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->errorScreenText:Landroid/widget/TextView;

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f0706ba

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 93
    :cond_1
    :goto_0
    return-void

    .line 90
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubPlayScreenAdapter;->errorScreenText:Landroid/widget/TextView;

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070257

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
