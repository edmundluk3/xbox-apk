.class public interface abstract Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponse;
.super Ljava/lang/Object;
.source "RtaDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/rta/RtaDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "RtaResponse"
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const-class v0, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponse;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaResponse;->TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public abstract payload()Lcom/microsoft/xbox/service/rta/RtaDataTypes$RtaPayload;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract subscriptionId()Ljava/lang/Integer;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
