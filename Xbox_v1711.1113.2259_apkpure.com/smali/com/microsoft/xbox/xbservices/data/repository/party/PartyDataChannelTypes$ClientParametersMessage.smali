.class public abstract Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ClientParametersMessage;
.super Ljava/lang/Object;
.source "PartyDataChannelTypes.java"

# interfaces
.implements Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$PartyDataChannelMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ClientParametersMessage"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ClientParametersMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/AutoValue_PartyDataChannelTypes_ClientParametersMessage$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/AutoValue_PartyDataChannelTypes_ClientParametersMessage$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method

.method public static with(JI)Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$ClientParametersMessage;
    .locals 2
    .param p0, "ssrc"    # J
    .param p2, "payloadType"    # I

    .prologue
    .line 32
    new-instance v0, Lcom/microsoft/xbox/xbservices/data/repository/party/AutoValue_PartyDataChannelTypes_ClientParametersMessage;

    const-string v1, "announceClientParameters"

    invoke-direct {v0, v1, p0, p1, p2}, Lcom/microsoft/xbox/xbservices/data/repository/party/AutoValue_PartyDataChannelTypes_ClientParametersMessage;-><init>(Ljava/lang/String;JI)V

    return-object v0
.end method


# virtual methods
.method public abstract payloadType()I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "payload_type"
    .end annotation
.end method

.method public abstract ssrc()J
.end method

.method public abstract type()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
