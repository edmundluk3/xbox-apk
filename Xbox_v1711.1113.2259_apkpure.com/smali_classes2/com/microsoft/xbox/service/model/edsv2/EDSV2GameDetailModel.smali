.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;
.super Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;
.source "EDSV2GameDetailModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel$BigCatAddonsLookUpRunner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel",
        "<",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final COOP_SUPPORT_LOCAL_ATTRIBUTE_KEY:Ljava/lang/String; = "CoopSupportLocal"

.field private static final COOP_SUPPORT_ONLINE_ATTRIBUTE_KEY:Ljava/lang/String; = "CoopSupportOnline"

.field private static final LOCAL_MULTIPLAYER_ATTRIBUTE_KEY:Ljava/lang/String; = "LocalMultiplayer"

.field private static final ONLINE_MULTIPLAYER_WITH_GOLD_ATTRIBUTE_KEY:Ljava/lang/String; = "OnlineMultiplayerWithGold"


# instance fields
.field private final addonsLookUpRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel$BigCatAddonsLookUpRunner;


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 2
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 33
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel$BigCatAddonsLookUpRunner;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel$BigCatAddonsLookUpRunner;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->addonsLookUpRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel$BigCatAddonsLookUpRunner;

    .line 31
    return-void
.end method


# virtual methods
.method protected createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;
    .locals 1
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 37
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    invoke-direct {v0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    return-object v0
.end method

.method protected bridge synthetic createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    move-result-object v0

    return-object v0
.end method

.method public getAverageUserRating()F
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getAverageUserRating()F

    move-result v0

    return v0
.end method

.method public getBundlePrimaryItemDetailModel()Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getIsBundle()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->BundlePrimaryItemId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 200
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;-><init>()V

    .line 201
    .local v0, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getBundlePrimaryItemId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->setBigCatProductId(Ljava/lang/String;)V

    .line 202
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getRelatedMediaType()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->setMediaItemTypeFromInt(I)V

    .line 203
    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;

    .line 205
    .end local v0    # "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCoop()Ljava/lang/String;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 148
    const-string v0, ""

    .line 150
    .local v0, "coopCapabilityText":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->hasAttribute()Z

    move-result v2

    if-nez v2, :cond_0

    move-object v1, v0

    .line 170
    .end local v0    # "coopCapabilityText":Ljava/lang/String;
    .local v1, "coopCapabilityText":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 153
    .end local v1    # "coopCapabilityText":Ljava/lang/String;
    .restart local v0    # "coopCapabilityText":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    const-string v3, "CoopSupportLocal"

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getAttribute(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    const-string v3, "CoopSupportOnline"

    .line 154
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getAttribute(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;

    move-result-object v2

    if-nez v2, :cond_1

    .line 155
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0703c5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object v1, v0

    .line 170
    .end local v0    # "coopCapabilityText":Ljava/lang/String;
    .restart local v1    # "coopCapabilityText":Ljava/lang/String;
    goto :goto_0

    .line 157
    .end local v1    # "coopCapabilityText":Ljava/lang/String;
    .restart local v0    # "coopCapabilityText":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    const-string v3, "CoopSupportOnline"

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getAttribute(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    const-string v3, "CoopSupportLocal"

    .line 158
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getAttribute(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;

    move-result-object v2

    if-nez v2, :cond_2

    .line 159
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0703c6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 161
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    const-string v3, "CoopSupportOnline"

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getAttribute(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    const-string v3, "CoopSupportLocal"

    .line 162
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getAttribute(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 163
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0703c7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 166
    :cond_3
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0703c4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public getDefaultParentalRating()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getParentalRatings()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getParentalRatings()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getParentalRatings()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;

    .line 104
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getDesiredMediaItemType()I
    .locals 1

    .prologue
    .line 194
    const/4 v0, 0x0

    return v0
.end method

.method protected getDesiredMediaItemTypeString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    const-string v0, "DDurable.DConsumable"

    return-object v0
.end method

.method public getDeveloper()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getDeveloper()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastPlayedDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x0

    return-object v0
.end method

.method public getLaunchType()Lcom/microsoft/xbox/service/model/LaunchType;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/microsoft/xbox/service/model/LaunchType;->GameLaunchType:Lcom/microsoft/xbox/service/model/LaunchType;

    return-object v0
.end method

.method public getMediaGroup()I
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x1

    return v0
.end method

.method public getMultiplayer()Ljava/lang/String;
    .locals 5
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 119
    const-string v0, ""

    .line 121
    .local v0, "multiplayerCapabilityText":Ljava/lang/String;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->hasAttribute()Z

    move-result v2

    if-nez v2, :cond_0

    move-object v1, v0

    .line 143
    .end local v0    # "multiplayerCapabilityText":Ljava/lang/String;
    .local v1, "multiplayerCapabilityText":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 124
    .end local v1    # "multiplayerCapabilityText":Ljava/lang/String;
    .restart local v0    # "multiplayerCapabilityText":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    const-string v3, "OnlineMultiplayerWithGold"

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getAttribute(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;

    move-result-object v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    const-string v3, "LocalMultiplayer"

    .line 125
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getAttribute(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    const-string v3, "LocalMultiplayer"

    .line 126
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getAttribute(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;

    move-result-object v2

    iget v2, v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;->maximum:I

    if-ne v2, v4, :cond_1

    .line 127
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0703c4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object v1, v0

    .line 143
    .end local v0    # "multiplayerCapabilityText":Ljava/lang/String;
    .restart local v1    # "multiplayerCapabilityText":Ljava/lang/String;
    goto :goto_0

    .line 129
    .end local v1    # "multiplayerCapabilityText":Ljava/lang/String;
    .restart local v0    # "multiplayerCapabilityText":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    const-string v3, "LocalMultiplayer"

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getAttribute(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    const-string v3, "LocalMultiplayer"

    .line 130
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getAttribute(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;

    move-result-object v2

    iget v2, v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;->maximum:I

    if-ne v2, v4, :cond_2

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    const-string v3, "OnlineMultiplayerWithGold"

    .line 131
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getAttribute(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 132
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0703c6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 134
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    const-string v3, "LocalMultiplayer"

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getAttribute(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    const-string v3, "LocalMultiplayer"

    .line 135
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getAttribute(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;

    move-result-object v2

    iget v2, v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;->maximum:I

    if-le v2, v4, :cond_3

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    const-string v3, "OnlineMultiplayerWithGold"

    .line 136
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getAttribute(Ljava/lang/String;)Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$Attribute;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 137
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0703c7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 140
    :cond_3
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0703c5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public getPublisher()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getPublisher()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRatingDescriptors()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2RatingDescriptor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getRatingDescriptors()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getRatingId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getRatingId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getRelatedMediaType()I
    .locals 1

    .prologue
    .line 179
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getIsForXboxOne()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    const/16 v0, 0x2329

    .line 182
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getSlideShows()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getSlideShow()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTitleType()Lcom/microsoft/xbox/service/model/JTitleType;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/microsoft/xbox/service/model/JTitleType;->Standard:Lcom/microsoft/xbox/service/model/JTitleType;

    return-object v0
.end method

.method public getUserRatingCount()I
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;->getUserRatingCount()I

    move-result v0

    return v0
.end method

.method public loadAddOns(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 53
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 54
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 56
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 58
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v0, v1, :cond_1

    .line 59
    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->lifetime:J

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->lastRefreshChildTime:Ljava/util/Date;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->loadingChildStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->addonsLookUpRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel$BigCatAddonsLookUpRunner;

    move v1, p1

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    .line 61
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v2, Lcom/microsoft/xbox/toolkit/XLEException;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->addonsLookUpRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel$BigCatAddonsLookUpRunner;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel$BigCatAddonsLookUpRunner;->getDefaultErrorCode()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    invoke-direct {v1, v6, v6, v2, v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    goto :goto_0
.end method

.method public loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 5
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 42
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 43
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->loadBigCatItemDetailFromProductId(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    .line 49
    :goto_0
    return-object v0

    .line 44
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getLegacyId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 45
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getLegacyId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->loadBigCatItemDetailFromLegacyId(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    goto :goto_0

    .line 46
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getTitleId()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 47
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->getTitleId()J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->loadBigCatItemDetailFromTitleId(ZJ)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    goto :goto_0

    .line 49
    :cond_2
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0xfae

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    invoke-direct {v0, v4, v4, v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    goto :goto_0
.end method

.method protected onGetMediaItemDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 212
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;>;"
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameDetailModel;->isLoading:Z

    .line 213
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_1

    .line 214
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2GameMediaItem;

    if-eqz v0, :cond_0

    .line 215
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->onGetMediaItemDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 220
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    invoke-super {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaListBrowseModel;->onGetMediaItemDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    goto :goto_0
.end method
