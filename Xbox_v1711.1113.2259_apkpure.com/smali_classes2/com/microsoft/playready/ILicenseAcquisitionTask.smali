.class public interface abstract Lcom/microsoft/playready/ILicenseAcquisitionTask;
.super Ljava/lang/Object;
.source "ILicenseAcquisitionTask.java"

# interfaces
.implements Ljava/util/concurrent/Future;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Future",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract addLicenseAcquirerListener(Lcom/microsoft/playready/ILicenseAcquirerListener;)V
.end method

.method public abstract getResponseCustomData()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public abstract removeLicenseAcquirerListener(Lcom/microsoft/playready/ILicenseAcquirerListener;)V
.end method
