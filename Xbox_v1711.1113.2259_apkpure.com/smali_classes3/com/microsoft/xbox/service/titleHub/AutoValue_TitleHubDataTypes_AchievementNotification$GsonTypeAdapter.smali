.class public final Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_TitleHubDataTypes_AchievementNotification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;",
        ">;"
    }
.end annotation


# instance fields
.field private final achievementIconUrlAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final achievementNameAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private defaultAchievementIconUrl:Ljava/lang/String;

.field private defaultAchievementName:Ljava/lang/String;

.field private defaultGamerscore:J

.field private defaultRarityCategory:Ljava/lang/String;

.field private defaultRarityPercentage:F

.field private defaultTitleId:J

.field private defaultTitleName:Ljava/lang/String;

.field private final gamerscoreAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final rarityCategoryAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final rarityPercentageAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final titleIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final titleNameAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 4
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 29
    iput-object v1, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->defaultTitleName:Ljava/lang/String;

    .line 30
    iput-object v1, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->defaultAchievementIconUrl:Ljava/lang/String;

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->defaultRarityPercentage:F

    .line 32
    iput-object v1, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->defaultRarityCategory:Ljava/lang/String;

    .line 33
    iput-wide v2, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->defaultTitleId:J

    .line 34
    iput-wide v2, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->defaultGamerscore:J

    .line 35
    iput-object v1, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->defaultAchievementName:Ljava/lang/String;

    .line 37
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->titleNameAdapter:Lcom/google/gson/TypeAdapter;

    .line 38
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->achievementIconUrlAdapter:Lcom/google/gson/TypeAdapter;

    .line 39
    const-class v0, Ljava/lang/Float;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->rarityPercentageAdapter:Lcom/google/gson/TypeAdapter;

    .line 40
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->rarityCategoryAdapter:Lcom/google/gson/TypeAdapter;

    .line 41
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->titleIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 42
    const-class v0, Ljava/lang/Long;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->gamerscoreAdapter:Lcom/google/gson/TypeAdapter;

    .line 43
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->achievementNameAdapter:Lcom/google/gson/TypeAdapter;

    .line 44
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;
    .locals 12
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v1

    sget-object v11, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v1, v11, :cond_0

    .line 101
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 102
    const/4 v1, 0x0

    .line 153
    :goto_0
    return-object v1

    .line 104
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 105
    iget-object v2, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->defaultTitleName:Ljava/lang/String;

    .line 106
    .local v2, "titleName":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->defaultAchievementIconUrl:Ljava/lang/String;

    .line 107
    .local v3, "achievementIconUrl":Ljava/lang/String;
    iget v4, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->defaultRarityPercentage:F

    .line 108
    .local v4, "rarityPercentage":F
    iget-object v5, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->defaultRarityCategory:Ljava/lang/String;

    .line 109
    .local v5, "rarityCategory":Ljava/lang/String;
    iget-wide v6, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->defaultTitleId:J

    .line 110
    .local v6, "titleId":J
    iget-wide v8, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->defaultGamerscore:J

    .line 111
    .local v8, "gamerscore":J
    iget-object v10, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->defaultAchievementName:Ljava/lang/String;

    .line 112
    .local v10, "achievementName":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 113
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 114
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v1

    sget-object v11, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v1, v11, :cond_1

    .line 115
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 118
    :cond_1
    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v11

    sparse-switch v11, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v1, :pswitch_data_0

    .line 148
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 118
    :sswitch_0
    const-string v11, "titleName"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    const/4 v1, 0x0

    goto :goto_2

    :sswitch_1
    const-string v11, "achievementIconUrl"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :sswitch_2
    const-string v11, "rarityPercentage"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    const/4 v1, 0x2

    goto :goto_2

    :sswitch_3
    const-string v11, "rarityCategory"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    const/4 v1, 0x3

    goto :goto_2

    :sswitch_4
    const-string v11, "titleId"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    const/4 v1, 0x4

    goto :goto_2

    :sswitch_5
    const-string v11, "gamerscore"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    const/4 v1, 0x5

    goto :goto_2

    :sswitch_6
    const-string v11, "achievementName"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    const/4 v1, 0x6

    goto :goto_2

    .line 120
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->titleNameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "titleName":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 121
    .restart local v2    # "titleName":Ljava/lang/String;
    goto :goto_1

    .line 124
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->achievementIconUrlAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "achievementIconUrl":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 125
    .restart local v3    # "achievementIconUrl":Ljava/lang/String;
    goto :goto_1

    .line 128
    :pswitch_2
    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->rarityPercentageAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v4

    .line 129
    goto/16 :goto_1

    .line 132
    :pswitch_3
    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->rarityCategoryAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "rarityCategory":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 133
    .restart local v5    # "rarityCategory":Ljava/lang/String;
    goto/16 :goto_1

    .line 136
    :pswitch_4
    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->titleIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 137
    goto/16 :goto_1

    .line 140
    :pswitch_5
    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->gamerscoreAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 141
    goto/16 :goto_1

    .line 144
    :pswitch_6
    iget-object v1, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->achievementNameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "achievementName":Ljava/lang/String;
    check-cast v10, Ljava/lang/String;

    .line 145
    .restart local v10    # "achievementName":Ljava/lang/String;
    goto/16 :goto_1

    .line 152
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 153
    new-instance v1, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification;

    invoke-direct/range {v1 .. v10}, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification;-><init>(Ljava/lang/String;Ljava/lang/String;FLjava/lang/String;JJLjava/lang/String;)V

    goto/16 :goto_0

    .line 118
    nop

    :sswitch_data_0
    .sparse-switch
        -0x7f4fdafd -> :sswitch_0
        -0x6c1c07f9 -> :sswitch_1
        -0x4deb0a6d -> :sswitch_4
        0x410bea5 -> :sswitch_2
        0x25064e5a -> :sswitch_6
        0x3fcf0fc9 -> :sswitch_3
        0x5a3bc3d2 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultAchievementIconUrl(Ljava/lang/String;)Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultAchievementIconUrl"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->defaultAchievementIconUrl:Ljava/lang/String;

    .line 51
    return-object p0
.end method

.method public setDefaultAchievementName(Ljava/lang/String;)Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultAchievementName"    # Ljava/lang/String;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->defaultAchievementName:Ljava/lang/String;

    .line 71
    return-object p0
.end method

.method public setDefaultGamerscore(J)Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultGamerscore"    # J

    .prologue
    .line 66
    iput-wide p1, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->defaultGamerscore:J

    .line 67
    return-object p0
.end method

.method public setDefaultRarityCategory(Ljava/lang/String;)Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultRarityCategory"    # Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->defaultRarityCategory:Ljava/lang/String;

    .line 59
    return-object p0
.end method

.method public setDefaultRarityPercentage(F)Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultRarityPercentage"    # F

    .prologue
    .line 54
    iput p1, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->defaultRarityPercentage:F

    .line 55
    return-object p0
.end method

.method public setDefaultTitleId(J)Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;
    .locals 1
    .param p1, "defaultTitleId"    # J

    .prologue
    .line 62
    iput-wide p1, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->defaultTitleId:J

    .line 63
    return-object p0
.end method

.method public setDefaultTitleName(Ljava/lang/String;)Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultTitleName"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->defaultTitleName:Ljava/lang/String;

    .line 47
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;)V
    .locals 4
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    if-nez p2, :cond_0

    .line 77
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 96
    :goto_0
    return-void

    .line 80
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 81
    const-string v0, "titleName"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 82
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->titleNameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;->titleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 83
    const-string v0, "achievementIconUrl"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->achievementIconUrlAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;->achievementIconUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 85
    const-string v0, "rarityPercentage"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 86
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->rarityPercentageAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;->rarityPercentage()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 87
    const-string v0, "rarityCategory"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 88
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->rarityCategoryAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;->rarityCategory()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 89
    const-string v0, "titleId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->titleIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;->titleId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 91
    const-string v0, "gamerscore"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->gamerscoreAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;->gamerscore()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 93
    const-string v0, "achievementName"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->achievementNameAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;->achievementName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 95
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    check-cast p2, Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;)V

    return-void
.end method
