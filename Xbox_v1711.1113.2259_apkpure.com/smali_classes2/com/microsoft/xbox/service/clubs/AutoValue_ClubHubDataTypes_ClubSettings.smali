.class final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings;
.super Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;
.source "AutoValue_ClubHubDataTypes_ClubSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubSettings$GsonTypeAdapter;
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;)V
    .locals 0
    .param p1, "feed"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;
    .param p2, "chat"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;
    .param p3, "lfg"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;
    .param p4, "roster"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;
    .param p5, "profile"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;
    .param p6, "viewerRoles"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    .prologue
    .line 17
    invoke-direct/range {p0 .. p6}, Lcom/microsoft/xbox/service/clubs/$AutoValue_ClubHubDataTypes_ClubSettings;-><init>(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;)V

    .line 18
    return-void
.end method
