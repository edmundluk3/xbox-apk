.class public Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;
.super Ljava/lang/Object;
.source "StumpProtocol.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;
    }
.end annotation


# static fields
.field private static final ATTR_BUTTON_ID:Ljava/lang/String; = "button_id"

.field private static final ATTR_CHANNEL_ID:Ljava/lang/String; = "channelId"

.field private static final ATTR_CHANNEL_NAME:Ljava/lang/String; = "channel_name"

.field private static final ATTR_COUNT:Ljava/lang/String; = "count"

.field private static final ATTR_CURRENT_CHANNEL_ID:Ljava/lang/String; = "currentChannelId"

.field private static final ATTR_DATA:Ljava/lang/String; = "params"

.field private static final ATTR_DEVICE_ID:Ljava/lang/String; = "device_id"

.field private static final ATTR_ERROR_TYPE:Ljava/lang/String; = "errorType"

.field private static final ATTR_ID:Ljava/lang/String; = "msgid"

.field private static final ATTR_LINEUP_ID:Ljava/lang/String; = "lineupInstanceId"

.field private static final ATTR_NEW_CHANNEL:Ljava/lang/String; = "newChannel"

.field private static final ATTR_NOTIFY:Ljava/lang/String; = "notification"

.field private static final ATTR_PLAYBACK_STATE:Ljava/lang/String; = "playbackState"

.field private static final ATTR_PROVIDER_ID:Ljava/lang/String; = "providerId"

.field private static final ATTR_REQUEST:Ljava/lang/String; = "request"

.field private static final ATTR_RESPONSE:Ljava/lang/String; = "response"

.field private static final ATTR_SHOW_ID:Ljava/lang/String; = "programId"

.field private static final ATTR_SOURCE:Ljava/lang/String; = "source"

.field private static final ATTR_START_INDEX:Ljava/lang/String; = "startindex"

.field private static final ATTR_TUNER_CHANNEL_TYPE:Ljava/lang/String; = "tunerChannelType"

.field private static final ATTR_USER_CAN_VIEW_CHANNEL:Ljava/lang/String; = "userCanViewChannel"

.field private static final CMD_ENSURE_STREAMING_STARTED:Ljava/lang/String; = "EnsureStreamingStarted"

.field private static final CMD_ERROR:Ljava/lang/String; = "Error"

.field private static final CMD_GET_APP_CHANNEL_DATA:Ljava/lang/String; = "GetAppChannelData"

.field private static final CMD_GET_APP_CHANNEL_LINEUPS:Ljava/lang/String; = "GetAppChannelLineups"

.field private static final CMD_GET_APP_CHANNEL_PROGRAM_DATA:Ljava/lang/String; = "GetAppChannelProgramData"

.field private static final CMD_GET_CONFIG:Ljava/lang/String; = "GetConfiguration"

.field private static final CMD_GET_HEADEND:Ljava/lang/String; = "GetHeadendInfo"

.field private static final CMD_GET_LIVETV_INFO:Ljava/lang/String; = "GetLiveTVInfo"

.field private static final CMD_GET_PROGRAMM:Ljava/lang/String; = "GetProgrammInfo"

.field private static final CMD_GET_RECENTS:Ljava/lang/String; = "GetRecentChannels"

.field private static final CMD_GET_TUNER:Ljava/lang/String; = "GetTunerLineups"

.field private static final CMD_SEND_KEY:Ljava/lang/String; = "SendKey"

.field private static final CMD_SET_CHANNEL:Ljava/lang/String; = "SetChannel"

.field private static final NOTIFY_CHANNEL_CHANGED:Ljava/lang/String; = "ChannelChanged"

.field private static final NOTIFY_CHANNEL_TYPE_CHANGED:Ljava/lang/String; = "ChannelTypeChanged"

.field private static final NOTIFY_CONFIG_CHANGED:Ljava/lang/String; = "ConfigurationChanged"

.field private static final NOTIFY_DEVICE_UI_CHANGED:Ljava/lang/String; = "DeviceUIChanged"

.field private static final NOTIFY_HEADEND_CHANGED:Ljava/lang/String; = "HeadendChanged"

.field private static final NOTIFY_STREAM_ERROR:Ljava/lang/String; = "StreamingError"

.field private static final NOTIFY_STREAM_FORMAT_CHANGED:Ljava/lang/String; = "VideoFormatChanged"

.field private static final NOTIFY_TUNED_CHANGED:Ljava/lang/String; = "ProgrammChanged"

.field private static final NOTIFY_TUNER_STATE_CHANGED:Ljava/lang/String; = "TunerStateChanged"

.field private static final TAG:Ljava/lang/String; = "StumpProtocol"


# instance fields
.field private final connection:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;

.field private idIndex:I

.field private idStart:Ljava/lang/String;

.field private final listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

.field private final pendingAttributes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation
.end field

.field private final pendingRequests:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;)V
    .locals 1
    .param p1, "connection"    # Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;
    .param p2, "listener"    # Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->pendingRequests:Ljava/util/HashMap;

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->pendingAttributes:Ljava/util/HashMap;

    .line 246
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->idStart:Ljava/lang/String;

    .line 247
    const/4 v0, 0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->idIndex:I

    .line 62
    iput-object p1, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->connection:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;

    .line 63
    iput-object p2, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    .line 64
    return-void
.end method

.method private declared-synchronized generateMessageID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 250
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->idStart:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 251
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Ljava/security/SecureRandom;->nextInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->idStart:Ljava/lang/String;

    .line 252
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->idStart:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->idStart:Ljava/lang/String;

    .line 255
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->idIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->idIndex:I

    .line 256
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->idStart:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->idIndex:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 250
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private getAppChannelData(Lorg/json/JSONObject;)Ljava/lang/Object;
    .locals 2
    .param p1, "json"    # Lorg/json/JSONObject;

    .prologue
    .line 402
    :try_start_0
    new-instance v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData;

    const-string v1, "params"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/urc/net/AppChannelData;-><init>(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 404
    :goto_0
    return-object v0

    .line 403
    :catch_0
    move-exception v0

    .line 404
    .local v0, "ex":Ljava/lang/Throwable;
    goto :goto_0
.end method

.method private getAppChannelLineups(Lorg/json/JSONObject;)Ljava/lang/Object;
    .locals 2
    .param p1, "json"    # Lorg/json/JSONObject;

    .prologue
    .line 394
    :try_start_0
    const-string v1, "params"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/urc/net/AppChannelLineups;->parseJSON(Lorg/json/JSONArray;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 396
    :goto_0
    return-object v0

    .line 395
    :catch_0
    move-exception v0

    .line 396
    .local v0, "ex":Ljava/lang/Throwable;
    goto :goto_0
.end method

.method private getAppChannelProgramData(Lorg/json/JSONObject;)Ljava/lang/Object;
    .locals 2
    .param p1, "json"    # Lorg/json/JSONObject;

    .prologue
    .line 410
    :try_start_0
    new-instance v0, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;

    const-string v1, "params"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/urc/net/AppChannelData$ShowExtra;-><init>(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 412
    :goto_0
    return-object v0

    .line 411
    :catch_0
    move-exception v0

    .line 412
    .local v0, "ex":Ljava/lang/Throwable;
    goto :goto_0
.end method

.method private getHeadend(Lorg/json/JSONObject;)Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;
    .locals 3
    .param p1, "json"    # Lorg/json/JSONObject;

    .prologue
    .line 380
    const/4 v0, 0x0

    .line 382
    .local v0, "headend":Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;
    const-string v2, "params"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 383
    .local v1, "oHeadend":Lorg/json/JSONObject;
    if-eqz v1, :cond_0

    .line 384
    invoke-static {v1}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->fromJSON(Lorg/json/JSONObject;)Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    move-result-object v0

    .line 388
    :goto_0
    return-object v0

    .line 386
    :cond_0
    const-string v2, "params"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;->fromString(Ljava/lang/String;)Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    move-result-object v0

    goto :goto_0
.end method

.method private getRecentChannelsProgramData(Lorg/json/JSONObject;)Ljava/lang/Object;
    .locals 2
    .param p1, "json"    # Lorg/json/JSONObject;

    .prologue
    .line 419
    :try_start_0
    const-string v1, "params"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;->parseJSON(Lorg/json/JSONArray;)Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 421
    :goto_0
    return-object v1

    .line 420
    :catch_0
    move-exception v0

    .line 421
    .local v0, "ex":Ljava/lang/Throwable;
    invoke-static {v0}, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;->instantiateWithException(Ljava/lang/Throwable;)Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;

    move-result-object v1

    goto :goto_0
.end method

.method private parseNotification(Lorg/json/JSONObject;Ljava/lang/String;)V
    .locals 5
    .param p1, "json"    # Lorg/json/JSONObject;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 353
    const-string v2, "ConfigurationChanged"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 354
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    invoke-interface {v1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onNotifyConfigChanged()V

    .line 377
    :cond_0
    :goto_0
    return-void

    .line 355
    :cond_1
    const-string v2, "ProgrammChanged"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 356
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    invoke-interface {v1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onNotifyProgramInfoChanged()V

    goto :goto_0

    .line 357
    :cond_2
    const-string v2, "DeviceUIChanged"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 358
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    const-string v2, "params"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onNotifyDeviceUIChanged(Ljava/lang/String;)V

    goto :goto_0

    .line 359
    :cond_3
    const-string v2, "HeadendChanged"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 360
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->getHeadend(Lorg/json/JSONObject;)Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onNotifyHeadendChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V

    goto :goto_0

    .line 361
    :cond_4
    const-string v2, "VideoFormatChanged"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 362
    iget-object v1, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    invoke-interface {v1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onNotifyStreamFormatChanged()V

    goto :goto_0

    .line 363
    :cond_5
    const-string v2, "ChannelChanged"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 364
    const-string v2, "params"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 365
    .local v0, "oData":Lorg/json/JSONObject;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    if-eqz v0, :cond_7

    const-string v2, "source"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    :goto_1
    if-eqz v0, :cond_8

    const-string v2, "newChannel"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    if-eqz v0, :cond_6

    const-string v1, "userCanViewChannel"

    .line 366
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 365
    :cond_6
    invoke-interface {v4, v3, v2, v1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onNotifyChannelChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_7
    move-object v3, v1

    goto :goto_1

    :cond_8
    move-object v2, v1

    goto :goto_2

    .line 367
    .end local v0    # "oData":Lorg/json/JSONObject;
    :cond_9
    const-string v2, "ChannelTypeChanged"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 368
    const-string v2, "params"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 369
    .restart local v0    # "oData":Lorg/json/JSONObject;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    if-eqz v0, :cond_b

    const-string v2, "tunerChannelType"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_3
    if-eqz v0, :cond_a

    invoke-static {v0}, Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;->parse(Lorg/json/JSONObject;)Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;

    move-result-object v1

    :cond_a
    invoke-interface {v3, v2, v1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onNotifyChannelTypeChanged(Ljava/lang/String;Lcom/microsoft/xbox/xle/urc/net/BlockExplicitContentPerShowInfo;)V

    goto/16 :goto_0

    :cond_b
    move-object v2, v1

    goto :goto_3

    .line 370
    .end local v0    # "oData":Lorg/json/JSONObject;
    :cond_c
    const-string v1, "StreamingError"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 371
    const-string v1, "params"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 372
    .restart local v0    # "oData":Lorg/json/JSONObject;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    if-eqz v0, :cond_d

    const-string v1, "errorType"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_4
    invoke-interface {v2, v1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onNotifyStreamError(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    const-string v1, ""

    goto :goto_4

    .line 373
    .end local v0    # "oData":Lorg/json/JSONObject;
    :cond_e
    const-string v1, "TunerStateChanged"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 374
    const-string v1, "params"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 375
    .restart local v0    # "oData":Lorg/json/JSONObject;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    if-eqz v0, :cond_f

    const-string v1, "playbackState"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_5
    invoke-interface {v2, v1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onNotifyTunerStateChange(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_f
    const-string v1, ""

    goto :goto_5
.end method

.method private parseResponse(Lorg/json/JSONObject;Ljava/lang/String;)V
    .locals 15
    .param p1, "json"    # Lorg/json/JSONObject;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 260
    const-string v11, "msgid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 262
    .local v7, "msgid":Ljava/lang/String;
    iget-object v11, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->pendingAttributes:Ljava/util/HashMap;

    invoke-virtual {v11, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/json/JSONObject;

    .line 263
    .local v9, "requestAttr":Lorg/json/JSONObject;
    iget-object v11, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->pendingRequests:Ljava/util/HashMap;

    invoke-virtual {v11, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 264
    .local v10, "requestType":Ljava/lang/String;
    if-eqz v7, :cond_0

    if-nez v10, :cond_1

    .line 265
    :cond_0
    iget-object v11, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->pendingAttributes:Ljava/util/HashMap;

    invoke-virtual {v11, v7}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    :goto_0
    return-void

    .line 269
    :cond_1
    const-string v11, "Error"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 270
    const-string v11, "StumpProtocol"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "parseResponse received error to request \'"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\':\n"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    const-string v11, "GetConfiguration"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 272
    iget-object v11, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    const/4 v12, 0x0

    invoke-interface {v11, v12}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onConfigReceived([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V

    .line 348
    :cond_2
    :goto_1
    iget-object v11, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->pendingRequests:Ljava/util/HashMap;

    invoke-virtual {v11, v7}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    iget-object v11, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->pendingAttributes:Ljava/util/HashMap;

    invoke-virtual {v11, v7}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 273
    :cond_3
    const-string v11, "GetProgrammInfo"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 275
    const-string v11, "GetHeadendInfo"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 276
    iget-object v11, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    const/4 v12, 0x0

    invoke-interface {v11, v12}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onNotifyHeadendChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V

    goto :goto_1

    .line 277
    :cond_4
    const-string v11, "GetAppChannelLineups"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 278
    iget-object v11, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    const/4 v12, 0x0

    invoke-interface {v11, v12}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onAppChannelLineupsReceived(Ljava/lang/Object;)V

    goto :goto_1

    .line 279
    :cond_5
    const-string v11, "GetAppChannelData"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 280
    if-eqz v9, :cond_2

    .line 281
    iget-object v11, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    const/4 v12, 0x0

    const-string v13, "providerId"

    invoke-virtual {v9, v13}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "channelId"

    invoke-virtual {v9, v14}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v11, v12, v13, v14}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onAppChannelDataReceived(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 283
    :cond_6
    const-string v11, "GetAppChannelProgramData"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 284
    if-eqz v9, :cond_2

    .line 285
    iget-object v11, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    const/4 v12, 0x0

    const-string v13, "providerId"

    invoke-virtual {v9, v13}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "programId"

    invoke-virtual {v9, v14}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v11, v12, v13, v14}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onAppChannelProgramDataReceived(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 287
    :cond_7
    const-string v11, "GetTunerLineups"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 288
    iget-object v11, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    const/4 v12, 0x0

    invoke-interface {v11, v12}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onTunerChannelsReceived([Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;)V

    goto :goto_1

    .line 289
    :cond_8
    const-string v11, "GetLiveTVInfo"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 290
    iget-object v11, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    const/4 v12, 0x0

    invoke-interface {v11, v12}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onLiveTvInfoReceived(Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;)V

    goto/16 :goto_1

    .line 291
    :cond_9
    const-string v11, "EnsureStreamingStarted"

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 292
    iget-object v11, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    const/4 v12, 0x0

    invoke-interface {v11, v12}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onStreamingStartedReceived(Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;)V

    goto/16 :goto_1

    .line 295
    :cond_a
    const-string v11, "SendKey"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    const-string v11, "SetChannel"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 297
    const-string v11, "GetConfiguration"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_e

    .line 299
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    .line 300
    .local v2, "devices":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;>;"
    const-string v11, "params"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    .line 302
    .local v8, "o":Ljava/lang/Object;
    if-eqz v8, :cond_d

    .line 303
    instance-of v11, v8, Lorg/json/JSONArray;

    if-eqz v11, :cond_c

    move-object v5, v8

    .line 304
    check-cast v5, Lorg/json/JSONArray;

    .line 305
    .local v5, "jsonData":Lorg/json/JSONArray;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v11

    if-ge v4, v11, :cond_d

    .line 307
    :try_start_0
    invoke-virtual {v5, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 308
    .local v6, "jsonDevice":Lorg/json/JSONObject;
    if-eqz v6, :cond_b

    .line 309
    invoke-static {v6}, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->fromJSON(Lorg/json/JSONObject;)Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 305
    .end local v6    # "jsonDevice":Lorg/json/JSONObject;
    :cond_b
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 311
    :catch_0
    move-exception v3

    .line 312
    .local v3, "e":Lorg/json/JSONException;
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_3

    .line 315
    .end local v3    # "e":Lorg/json/JSONException;
    .end local v4    # "i":I
    .end local v5    # "jsonData":Lorg/json/JSONArray;
    :cond_c
    instance-of v11, v8, Lorg/json/JSONObject;

    if-eqz v11, :cond_d

    move-object v5, v8

    .line 316
    check-cast v5, Lorg/json/JSONObject;

    .line 317
    .local v5, "jsonData":Lorg/json/JSONObject;
    invoke-static {v5}, Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;->fromJSON(Lorg/json/JSONObject;)Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 321
    .end local v5    # "jsonData":Lorg/json/JSONObject;
    :cond_d
    iget-object v12, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    const/4 v11, 0x0

    new-array v11, v11, [Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    invoke-virtual {v2, v11}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    invoke-interface {v12, v11}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onConfigReceived([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V

    goto/16 :goto_1

    .line 323
    .end local v2    # "devices":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;>;"
    .end local v8    # "o":Ljava/lang/Object;
    :cond_e
    const-string v11, "GetProgrammInfo"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 325
    const-string v11, "GetHeadendInfo"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_f

    .line 326
    iget-object v11, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    invoke-direct/range {p0 .. p1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->getHeadend(Lorg/json/JSONObject;)Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    move-result-object v12

    invoke-interface {v11, v12}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onNotifyHeadendChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V

    goto/16 :goto_1

    .line 327
    :cond_f
    const-string v11, "GetAppChannelLineups"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_10

    .line 328
    iget-object v11, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    invoke-direct/range {p0 .. p1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->getAppChannelLineups(Lorg/json/JSONObject;)Ljava/lang/Object;

    move-result-object v12

    invoke-interface {v11, v12}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onAppChannelLineupsReceived(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 329
    :cond_10
    const-string v11, "GetAppChannelData"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_11

    .line 330
    if-eqz v9, :cond_2

    .line 331
    iget-object v11, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    invoke-direct/range {p0 .. p1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->getAppChannelData(Lorg/json/JSONObject;)Ljava/lang/Object;

    move-result-object v12

    const-string v13, "providerId"

    invoke-virtual {v9, v13}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "channelId"

    invoke-virtual {v9, v14}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v11, v12, v13, v14}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onAppChannelDataReceived(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 333
    :cond_11
    const-string v11, "GetAppChannelProgramData"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_12

    .line 334
    if-eqz v9, :cond_2

    .line 335
    iget-object v11, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    invoke-direct/range {p0 .. p1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->getAppChannelProgramData(Lorg/json/JSONObject;)Ljava/lang/Object;

    move-result-object v12

    const-string v13, "providerId"

    invoke-virtual {v9, v13}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "programId"

    invoke-virtual {v9, v14}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v11, v12, v13, v14}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onAppChannelProgramDataReceived(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 337
    :cond_12
    const-string v11, "GetRecentChannels"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_13

    .line 338
    invoke-direct/range {p0 .. p1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->getRecentChannelsProgramData(Lorg/json/JSONObject;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;

    .line 339
    .local v1, "data":Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;
    iget-object v11, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    invoke-interface {v11, v1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onTvRecentsReceived(Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;)V

    goto/16 :goto_1

    .line 340
    .end local v1    # "data":Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;
    :cond_13
    const-string v11, "GetTunerLineups"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_14

    .line 341
    iget-object v11, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    invoke-static/range {p1 .. p1}, Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;->parse(Lorg/json/JSONObject;)[Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;

    move-result-object v12

    invoke-interface {v11, v12}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onTunerChannelsReceived([Lcom/microsoft/xbox/xle/urc/net/TunerChannelsData;)V

    goto/16 :goto_1

    .line 342
    :cond_14
    const-string v11, "GetLiveTVInfo"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_15

    .line 343
    iget-object v11, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    invoke-static/range {p1 .. p1}, Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;->parse(Lorg/json/JSONObject;)Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;

    move-result-object v12

    invoke-interface {v11, v12}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onLiveTvInfoReceived(Lcom/microsoft/xbox/xle/urc/net/LiveTvInfo;)V

    goto/16 :goto_1

    .line 344
    :cond_15
    const-string v11, "EnsureStreamingStarted"

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 345
    iget-object v11, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->listener:Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;

    invoke-static/range {p1 .. p1}, Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;->parse(Lorg/json/JSONObject;)Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;

    move-result-object v12

    invoke-interface {v11, v12}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol$IProtocolListener;->onStreamingStartedReceived(Lcom/microsoft/xbox/xle/urc/net/StreamStartupInfo;)V

    goto/16 :goto_1
.end method

.method private sendRequest(Ljava/lang/String;Lorg/json/JSONObject;)Z
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "data"    # Lorg/json/JSONObject;

    .prologue
    .line 223
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->generateMessageID()Ljava/lang/String;

    move-result-object v1

    .line 224
    .local v1, "id":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->pendingRequests:Ljava/util/HashMap;

    invoke-virtual {v3, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->pendingAttributes:Ljava/util/HashMap;

    invoke-virtual {v3, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 229
    .local v2, "json":Lorg/json/JSONObject;
    const-string v3, "request"

    invoke-virtual {v2, v3, p1}, Lorg/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 230
    const-string v3, "msgid"

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 231
    if-eqz p2, :cond_0

    .line 232
    const-string v3, "params"

    invoke-virtual {v2, v3, p2}, Lorg/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 235
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->connection:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection;->sendMessage(Ljava/lang/String;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_1

    .line 236
    const/4 v3, 0x1

    .line 243
    .end local v2    # "json":Lorg/json/JSONObject;
    :goto_0
    return v3

    .line 238
    :catch_0
    move-exception v0

    .line 239
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 241
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->pendingRequests:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    iget-object v3, p0, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->pendingAttributes:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public parseMessage(Ljava/lang/String;)V
    .locals 6
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 71
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 73
    .local v1, "json":Lorg/json/JSONObject;
    const-string v4, "response"

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 74
    .local v3, "strResponse":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 75
    invoke-direct {p0, v1, v3}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->parseResponse(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 87
    .end local v1    # "json":Lorg/json/JSONObject;
    .end local v3    # "strResponse":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 79
    .restart local v1    # "json":Lorg/json/JSONObject;
    .restart local v3    # "strResponse":Ljava/lang/String;
    :cond_1
    const-string v4, "notification"

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 80
    .local v2, "strNotify":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 81
    invoke-direct {p0, v1, v2}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->parseNotification(Lorg/json/JSONObject;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 84
    .end local v1    # "json":Lorg/json/JSONObject;
    .end local v2    # "strNotify":Ljava/lang/String;
    .end local v3    # "strResponse":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 85
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public requestAppChannelData(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "providerID"    # Ljava/lang/String;
    .param p2, "channelID"    # Ljava/lang/String;

    .prologue
    .line 192
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 193
    .local v0, "data":Lorg/json/JSONObject;
    const-string v2, "id"

    invoke-virtual {v0, v2, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 194
    const-string v2, "providerId"

    invoke-virtual {v0, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 195
    const-string v2, "channelId"

    invoke-virtual {v0, v2, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 197
    const-string v2, "GetAppChannelData"

    invoke-direct {p0, v2, v0}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->sendRequest(Ljava/lang/String;Lorg/json/JSONObject;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 202
    .end local v0    # "data":Lorg/json/JSONObject;
    :goto_0
    return v2

    .line 199
    :catch_0
    move-exception v1

    .line 200
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 202
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public requestAppChannelLineups()Z
    .locals 2

    .prologue
    .line 186
    const-string v0, "GetAppChannelLineups"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->sendRequest(Ljava/lang/String;Lorg/json/JSONObject;)Z

    move-result v0

    return v0
.end method

.method public requestAppChannelProgramData(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "providerID"    # Ljava/lang/String;
    .param p2, "showID"    # Ljava/lang/String;

    .prologue
    .line 209
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 210
    .local v0, "data":Lorg/json/JSONObject;
    const-string v2, "providerId"

    invoke-virtual {v0, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 211
    const-string v2, "programId"

    invoke-virtual {v0, v2, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 213
    const-string v2, "GetAppChannelProgramData"

    invoke-direct {p0, v2, v0}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->sendRequest(Ljava/lang/String;Lorg/json/JSONObject;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 218
    .end local v0    # "data":Lorg/json/JSONObject;
    :goto_0
    return v2

    .line 215
    :catch_0
    move-exception v1

    .line 216
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 218
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public requestConfig()Z
    .locals 2

    .prologue
    .line 160
    const-string v0, "GetConfiguration"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->sendRequest(Ljava/lang/String;Lorg/json/JSONObject;)Z

    move-result v0

    return v0
.end method

.method public requestHeadend()Z
    .locals 2

    .prologue
    .line 139
    const-string v0, "GetHeadendInfo"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->sendRequest(Ljava/lang/String;Lorg/json/JSONObject;)Z

    move-result v0

    return v0
.end method

.method public requestLiveTvInfo()Z
    .locals 2

    .prologue
    .line 164
    const-string v0, "GetLiveTVInfo"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->sendRequest(Ljava/lang/String;Lorg/json/JSONObject;)Z

    move-result v0

    return v0
.end method

.method public requestProgramInfo()Z
    .locals 2

    .prologue
    .line 182
    const-string v0, "GetProgrammInfo"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->sendRequest(Ljava/lang/String;Lorg/json/JSONObject;)Z

    move-result v0

    return v0
.end method

.method public requestRecentChannels(II)Z
    .locals 3
    .param p1, "first"    # I
    .param p2, "count"    # I

    .prologue
    .line 144
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 146
    .local v1, "param":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "startindex"

    invoke-virtual {v1, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 147
    const-string v2, "count"

    invoke-virtual {v1, v2, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    :goto_0
    const-string v2, "GetRecentChannels"

    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->sendRequest(Ljava/lang/String;Lorg/json/JSONObject;)Z

    move-result v2

    return v2

    .line 148
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method

.method public requestStreamingStarted(Ljava/lang/String;)Z
    .locals 4
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    .line 168
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 169
    const-string v2, "EnsureStreamingStarted"

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->sendRequest(Ljava/lang/String;Lorg/json/JSONObject;)Z

    move-result v2

    .line 177
    :goto_0
    return v2

    .line 171
    :cond_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 173
    .local v1, "param":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "source"

    invoke-virtual {v1, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 174
    const-string v2, "EnsureStreamingStarted"

    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->sendRequest(Ljava/lang/String;Lorg/json/JSONObject;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 175
    :catch_0
    move-exception v0

    .line 176
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 177
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public requestTunerChannels()Z
    .locals 2

    .prologue
    .line 156
    const-string v0, "GetTunerLineups"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->sendRequest(Ljava/lang/String;Lorg/json/JSONObject;)Z

    move-result v0

    return v0
.end method

.method public sendButton(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "device"    # Ljava/lang/String;
    .param p2, "button"    # Ljava/lang/String;

    .prologue
    .line 90
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->sendButton(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Z

    move-result v0

    return v0
.end method

.method public sendButton(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Z
    .locals 5
    .param p1, "device"    # Ljava/lang/String;
    .param p2, "button"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 95
    .local p3, "extras":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 96
    .local v1, "json":Lorg/json/JSONObject;
    const-string v3, "button_id"

    invoke-virtual {v1, v3, p2}, Lorg/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 97
    if-eqz p1, :cond_0

    .line 98
    const-string v3, "device_id"

    invoke-virtual {v1, v3, p1}, Lorg/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 100
    :cond_0
    if-eqz p3, :cond_1

    .line 101
    invoke-interface {p3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 102
    .local v2, "key":Ljava/lang/String;
    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Lorg/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 107
    .end local v1    # "json":Lorg/json/JSONObject;
    .end local v2    # "key":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 108
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 110
    const/4 v3, 0x0

    .end local v0    # "e":Lorg/json/JSONException;
    :goto_1
    return v3

    .line 106
    .restart local v1    # "json":Lorg/json/JSONObject;
    :cond_1
    :try_start_1
    const-string v3, "SendKey"

    invoke-direct {p0, v3, v1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->sendRequest(Ljava/lang/String;Lorg/json/JSONObject;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v3

    goto :goto_1
.end method

.method public setChannel(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "channelId"    # Ljava/lang/String;
    .param p2, "lineupId"    # Ljava/lang/String;

    .prologue
    .line 115
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 116
    .local v1, "json":Lorg/json/JSONObject;
    const-string v2, "channelId"

    invoke-virtual {v1, v2, p1}, Lorg/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 117
    const-string v2, "lineupInstanceId"

    invoke-virtual {v1, v2, p2}, Lorg/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 119
    const-string v2, "SetChannel"

    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->sendRequest(Ljava/lang/String;Lorg/json/JSONObject;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 123
    .end local v1    # "json":Lorg/json/JSONObject;
    :goto_0
    return v2

    .line 120
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 123
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setChannelName(Ljava/lang/String;)Z
    .locals 3
    .param p1, "channelName"    # Ljava/lang/String;

    .prologue
    .line 128
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 129
    .local v1, "json":Lorg/json/JSONObject;
    const-string v2, "channel_name"

    invoke-virtual {v1, v2, p1}, Lorg/json/JSONObject;->accumulate(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 131
    const-string v2, "SetChannel"

    invoke-direct {p0, v2, v1}, Lcom/microsoft/xbox/xle/urc/net/StumpProtocol;->sendRequest(Ljava/lang/String;Lorg/json/JSONObject;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 135
    .end local v1    # "json":Lorg/json/JSONObject;
    :goto_0
    return v2

    .line 132
    :catch_0
    move-exception v0

    .line 133
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 135
    const/4 v2, 0x0

    goto :goto_0
.end method
