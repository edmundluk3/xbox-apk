.class final synthetic Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$38;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$38;->arg$1:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;)Lio/reactivex/functions/Consumer;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$38;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$38;-><init>(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;)V

    return-object v0
.end method


# virtual methods
.method public accept(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$38;->arg$1:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;

    check-cast p1, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->lambda$null$13(Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$TimeZoneChangedAction;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;)V

    return-void
.end method
