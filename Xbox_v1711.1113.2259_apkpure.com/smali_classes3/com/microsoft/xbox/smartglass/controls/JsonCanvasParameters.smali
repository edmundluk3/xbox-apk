.class Lcom/microsoft/xbox/smartglass/controls/JsonCanvasParameters;
.super Lorg/json/JSONObject;
.source "JsonCanvasParameters.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;[Ljava/lang/String;[I)V
    .locals 6
    .param p1, "activityUrl"    # Ljava/lang/String;
    .param p2, "allowedUrlPrefixes"    # [Ljava/lang/String;
    .param p3, "allowedTitleIds"    # [I

    .prologue
    .line 16
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 18
    :try_start_0
    const-string v2, "activityUrl"

    invoke-virtual {p0, v2, p1}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasParameters;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19
    const-string v2, "allowedUrlPrefixes"

    new-instance v3, Lorg/json/JSONArray;

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasParameters;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 22
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 23
    .local v1, "titleIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    array-length v3, p3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget v0, p3, v2

    .line 24
    .local v0, "i":I
    invoke-static {v0}, Lcom/microsoft/xbox/smartglass/controls/Json;->unsignedInt(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 23
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 26
    .end local v0    # "i":I
    :cond_0
    const-string v2, "allowedTitleIds"

    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, v1}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v2, v3}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasParameters;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 29
    .end local v1    # "titleIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    :goto_1
    return-void

    .line 27
    :catch_0
    move-exception v2

    goto :goto_1
.end method
