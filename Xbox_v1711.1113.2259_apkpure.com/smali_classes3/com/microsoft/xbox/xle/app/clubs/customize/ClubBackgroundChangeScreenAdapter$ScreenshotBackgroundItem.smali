.class Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$ScreenshotBackgroundItem;
.super Ljava/lang/Object;
.source "ClubBackgroundChangeScreenAdapter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeAdapter$BackgroundImageItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScreenshotBackgroundItem"
.end annotation


# instance fields
.field private imageUri:Ljava/lang/String;

.field private final screenshot:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;

.field private thumbnailUri:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;)V
    .locals 0
    .param p2, "screenshot"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;

    .prologue
    .line 200
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$ScreenshotBackgroundItem;->this$0:Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 201
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$ScreenshotBackgroundItem;->screenshot:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;

    .line 202
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;
    .param p3, "x2"    # Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$1;

    .prologue
    .line 194
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$ScreenshotBackgroundItem;-><init>(Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter;Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;)V

    return-void
.end method


# virtual methods
.method public getDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$ScreenshotBackgroundItem;->screenshot:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->dateUploaded()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getGamerScore()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 267
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getGamerScore on screenshot"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getImageURI()Ljava/lang/String;
    .locals 5
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 231
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$ScreenshotBackgroundItem;->imageUri:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 232
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$ScreenshotBackgroundItem;->screenshot:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->contentLocators()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 234
    .local v1, "locators":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;

    .line 235
    .local v0, "locator":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;->locatorType()Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    move-result-object v2

    .line 237
    .local v2, "type":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;
    if-eqz v2, :cond_0

    sget-object v4, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;->Download:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    if-ne v2, v4, :cond_0

    .line 238
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;->uri()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$ScreenshotBackgroundItem;->imageUri:Ljava/lang/String;

    .line 244
    .end local v0    # "locator":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;
    .end local v1    # "locators":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;>;"
    .end local v2    # "type":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$ScreenshotBackgroundItem;->imageUri:Ljava/lang/String;

    return-object v3
.end method

.method public getItemType()I
    .locals 1

    .prologue
    .line 206
    const/4 v0, 0x0

    return v0
.end method

.method public getSubTitle()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 256
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$ScreenshotBackgroundItem;->screenshot:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->titleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getThumbnailImageURI()Ljava/lang/String;
    .locals 5
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 212
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$ScreenshotBackgroundItem;->thumbnailUri:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 213
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$ScreenshotBackgroundItem;->screenshot:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->contentLocators()Lcom/google/common/collect/ImmutableList;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 215
    .local v1, "locators":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;

    .line 216
    .local v0, "locator":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;->locatorType()Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;

    move-result-object v2

    .line 218
    .local v2, "type":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;->isThumbnail()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 219
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;->uri()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$ScreenshotBackgroundItem;->thumbnailUri:Ljava/lang/String;

    .line 225
    .end local v0    # "locator":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;
    .end local v1    # "locators":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;>;"
    .end local v2    # "type":Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$LocatorType;
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$ScreenshotBackgroundItem;->thumbnailUri:Ljava/lang/String;

    return-object v3
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 250
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/customize/ClubBackgroundChangeScreenAdapter$ScreenshotBackgroundItem;->screenshot:Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Screenshot;->userCaption()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
