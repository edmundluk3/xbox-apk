.class public Lcom/microsoft/xbox/data/service/ServiceModule;
.super Ljava/lang/Object;
.source "ServiceModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/microsoft/xbox/data/service/beam/BeamServiceModule;,
        Lcom/microsoft/xbox/data/service/editorial/EditorialServiceModule;,
        Lcom/microsoft/xbox/data/service/multiplayer/GameServerServiceModule;,
        Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;,
        Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerServiceModule;,
        Lcom/microsoft/xbox/data/service/oobe/OOBEServiceModule;,
        Lcom/microsoft/xbox/data/service/peoplehub/PeopleHubServiceModule;,
        Lcom/microsoft/xbox/data/service/privacy/PrivacyServiceModule;,
        Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorServiceModule;
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/data/service/ServiceModule$Names;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideDefaultOkHttpClient(Lokhttp3/logging/HttpLoggingInterceptor;Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;)Lokhttp3/OkHttpClient;
    .locals 1
    .param p1, "loggingInterceptor"    # Lokhttp3/logging/HttpLoggingInterceptor;
    .param p2, "localeHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "DEFAULT_OK_HTTP"
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 69
    new-instance v0, Lokhttp3/OkHttpClient$Builder;

    invoke-direct {v0}, Lokhttp3/OkHttpClient$Builder;-><init>()V

    .line 70
    invoke-virtual {v0, p1}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 71
    invoke-virtual {v0, p2}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    .line 69
    return-object v0
.end method

.method provideGsonConvertorFactory()Lretrofit2/converter/gson/GsonConverterFactory;
    .locals 5
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 97
    new-instance v1, Lcom/google/gson/GsonBuilder;

    invoke-direct {v1}, Lcom/google/gson/GsonBuilder;-><init>()V

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v3, 0x0

    const/16 v4, 0x80

    aput v4, v2, v3

    .line 98
    invoke-virtual {v1, v2}, Lcom/google/gson/GsonBuilder;->excludeFieldsWithModifiers([I)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    .line 99
    invoke-static {}, Lcom/microsoft/xbox/toolkit/gson/AutoValueGsonAdapterFactory;->create()Lcom/google/gson/TypeAdapterFactory;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/gson/GsonBuilder;->registerTypeAdapterFactory(Lcom/google/gson/TypeAdapterFactory;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    .line 100
    invoke-static {}, Lcom/microsoft/xbox/xbservices/toolkit/gson/AutoValueGsonAdapterFactory;->create()Lcom/google/gson/TypeAdapterFactory;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/gson/GsonBuilder;->registerTypeAdapterFactory(Lcom/google/gson/TypeAdapterFactory;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/toolkit/gson/PostProcessingEnablerGson;

    invoke-direct {v2}, Lcom/microsoft/xbox/toolkit/gson/PostProcessingEnablerGson;-><init>()V

    .line 101
    invoke-virtual {v1, v2}, Lcom/google/gson/GsonBuilder;->registerTypeAdapterFactory(Lcom/google/gson/TypeAdapterFactory;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    const-class v2, Lcom/google/common/collect/ImmutableList;

    new-instance v3, Lcom/microsoft/xbox/toolkit/gson/ImmutableListDeserializer;

    invoke-direct {v3}, Lcom/microsoft/xbox/toolkit/gson/ImmutableListDeserializer;-><init>()V

    .line 102
    invoke-virtual {v1, v2, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/service/multiplayer/MultiplayerSessionDataTypes$RemoveMultiplayerMemberRequest;

    new-instance v3, Lcom/microsoft/xbox/toolkit/gson/RemoveMultiplayerMemberTypeAdapter;

    invoke-direct {v3}, Lcom/microsoft/xbox/toolkit/gson/RemoveMultiplayerMemberTypeAdapter;-><init>()V

    .line 103
    invoke-virtual {v1, v2, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    const-class v2, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$RemoveMultiplayerMemberRequest;

    new-instance v3, Lcom/microsoft/xbox/xbservices/toolkit/gson/RemoveMultiplayerMemberTypeAdapter;

    invoke-direct {v3}, Lcom/microsoft/xbox/xbservices/toolkit/gson/RemoveMultiplayerMemberTypeAdapter;-><init>()V

    .line 104
    invoke-virtual {v1, v2, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    const-class v2, Ljava/util/Date;

    new-instance v3, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterIso8601;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson$UTCDateConverterIso8601;-><init>()V

    .line 105
    invoke-virtual {v1, v2, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    .line 106
    invoke-virtual {v1}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    .line 108
    .local v0, "gson":Lcom/google/gson/Gson;
    invoke-static {v0}, Lretrofit2/converter/gson/GsonConverterFactory;->create(Lcom/google/gson/Gson;)Lretrofit2/converter/gson/GsonConverterFactory;

    move-result-object v1

    return-object v1
.end method

.method provideXTokenOkHttpClient(Lokhttp3/logging/HttpLoggingInterceptor;Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;)Lokhttp3/OkHttpClient;
    .locals 1
    .param p1, "loggingInterceptor"    # Lokhttp3/logging/HttpLoggingInterceptor;
    .param p2, "localeHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/LocaleHeaderInterceptor;
    .param p3, "xTokenAuthenticator"    # Lcom/microsoft/xbox/service/retrofit/XTokenAuthenticator;
    .param p4, "xTokenHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/XTokenHeaderInterceptor;
    .param p5, "xUserAgentHeaderInterceptor"    # Lcom/microsoft/xbox/service/retrofit/XUserAgentHeaderInterceptor;
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "XTOKEN_OK_HTTP"
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 85
    new-instance v0, Lokhttp3/OkHttpClient$Builder;

    invoke-direct {v0}, Lokhttp3/OkHttpClient$Builder;-><init>()V

    .line 86
    invoke-virtual {v0, p3}, Lokhttp3/OkHttpClient$Builder;->authenticator(Lokhttp3/Authenticator;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 87
    invoke-virtual {v0, p4}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 88
    invoke-virtual {v0, p5}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 89
    invoke-virtual {v0, p1}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 90
    invoke-virtual {v0, p2}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v0

    .line 85
    return-object v0
.end method
