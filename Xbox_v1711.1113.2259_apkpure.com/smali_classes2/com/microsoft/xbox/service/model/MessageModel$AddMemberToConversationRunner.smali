.class Lcom/microsoft/xbox/service/model/MessageModel$AddMemberToConversationRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "MessageModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/MessageModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AddMemberToConversationRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final caller:Lcom/microsoft/xbox/service/model/MessageModel;

.field private final conversationId:Ljava/lang/String;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/MessageModel;

.field private final xuid:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/service/model/MessageModel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "caller"    # Lcom/microsoft/xbox/service/model/MessageModel;
    .param p3, "xuid"    # Ljava/lang/String;
    .param p4, "conversationId"    # Ljava/lang/String;

    .prologue
    .line 2109
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/MessageModel$AddMemberToConversationRunner;->this$0:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 2110
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/MessageModel$AddMemberToConversationRunner;->caller:Lcom/microsoft/xbox/service/model/MessageModel;

    .line 2111
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/MessageModel$AddMemberToConversationRunner;->xuid:Ljava/lang/String;

    .line 2112
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/MessageModel$AddMemberToConversationRunner;->conversationId:Ljava/lang/String;

    .line 2113
    return-void
.end method


# virtual methods
.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2102
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/MessageModel$AddMemberToConversationRunner;->buildData()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public buildData()Ljava/lang/Void;
    .locals 7
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 2118
    invoke-static {}, Lcom/microsoft/xbox/service/model/MessageModel;->access$500()Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel$AddMemberToConversationRunner;->conversationId:Ljava/lang/String;

    const-string v3, "8:xbox:%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/MessageModel$AddMemberToConversationRunner;->xuid:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;->User:Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;

    invoke-interface {v1, v2, v3, v4}, Lcom/microsoft/xbox/service/groupMessaging/ISkypeGroupService;->joinGroupConversation(Ljava/lang/String;Ljava/lang/String;Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMemberRole;)Z

    move-result v0

    .line 2119
    .local v0, "result":Z
    if-nez v0, :cond_0

    .line 2120
    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0x2648

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v1

    .line 2123
    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 2137
    const-wide/16 v0, 0x2648

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 3
    .param p1    # Lcom/microsoft/xbox/toolkit/AsyncResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2132
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Void;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/MessageModel$AddMemberToConversationRunner;->caller:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/MessageModel$AddMemberToConversationRunner;->xuid:Ljava/lang/String;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/MessageModel$AddMemberToConversationRunner;->conversationId:Ljava/lang/String;

    invoke-static {v0, p1, v1, v2}, Lcom/microsoft/xbox/service/model/MessageModel;->access$1700(Lcom/microsoft/xbox/service/model/MessageModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;Ljava/lang/String;)V

    .line 2133
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 2128
    return-void
.end method
