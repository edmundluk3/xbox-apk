.class final synthetic Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$29;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;

.field private final arg$2:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$29;->arg$1:Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;

    iput-object p2, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$29;->arg$2:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;)Ljava/util/concurrent/Callable;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$29;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$29;-><init>(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;)V

    return-object v0
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$29;->arg$1:Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;

    iget-object v1, p0, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$$Lambda$29;->arg$2:Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/domain/oobe/OOBEInteractor;->lambda$null$20(Lcom/microsoft/xbox/data/repository/oobe/OOBESettingsRepository;Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$InstantOnChangedAction;)Lcom/microsoft/xbox/domain/oobe/OOBEInteractor$UpdateSettingsResult;

    move-result-object v0

    return-object v0
.end method
