.class final synthetic Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog$$Lambda$4;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog$$Lambda$4;->arg$1:Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;)Lio/reactivex/functions/Consumer;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog$$Lambda$4;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog$$Lambda$4;-><init>(Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;)V

    return-object v0
.end method


# virtual methods
.method public accept(Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog$$Lambda$4;->arg$1:Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/dialog/ConsoleConnectionManagementDialog;->setToggleConnectionButtonState(I)V

    return-void
.end method
