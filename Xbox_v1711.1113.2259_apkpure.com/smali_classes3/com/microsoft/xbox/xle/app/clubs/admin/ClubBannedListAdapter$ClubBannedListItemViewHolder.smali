.class public final Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "ClubBannedListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "ClubBannedListItemViewHolder"
.end annotation


# instance fields
.field private gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private gamerTag:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private presence:Landroid/widget/TextView;

.field private realName:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;

.field private unban:Lcom/microsoft/xbox/xle/ui/IconFontButton;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;Landroid/view/View;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;

    .line 59
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 61
    const v0, 0x7f0e025e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;->gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 62
    const v0, 0x7f0e025f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;->gamerTag:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 63
    const v0, 0x7f0e0260

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;->realName:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 64
    const v0, 0x7f0e0261

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;->presence:Landroid/widget/TextView;

    .line 65
    const v0, 0x7f0e0262

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/ui/IconFontButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;->unban:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    .line 66
    return-void
.end method

.method static synthetic lambda$bindTo$0(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$BannedItemClickListener;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;Landroid/view/View;)V
    .locals 2
    .param p0, "clickListener"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$BannedItemClickListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 77
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;->getXuid()J

    move-result-wide v0

    invoke-interface {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$BannedItemClickListener;->onProfileClick(J)V

    return-void
.end method

.method static synthetic lambda$bindTo$2(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$BannedItemClickListener;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "clickListener"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$BannedItemClickListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;
    .param p3, "v"    # Landroid/view/View;

    .prologue
    .line 78
    invoke-static {p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$BannedItemClickListener;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;->removeSelfDecorator(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$BannedItemClickListener;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;)V
    .locals 2
    .param p0, "clickListener"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$BannedItemClickListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 78
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;->getXuid()J

    move-result-wide v0

    invoke-interface {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$BannedItemClickListener;->onUnbanClick(J)V

    return-void
.end method

.method private removeSelfDecorator(Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 82
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;->getAdapterPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;->remove(I)V

    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;->getAdapterPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;->notifyItemRemoved(I)V

    .line 85
    return-void
.end method


# virtual methods
.method public bindTo(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$BannedItemClickListener;)V
    .locals 3
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "clickListener"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$BannedItemClickListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v2, 0x7f0201fa

    .line 69
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 70
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 72
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;->gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;->getGamerpic()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;->gamerTag:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;->getGamertag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    .line 74
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;->realName:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;->getRealName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setText(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;->presence:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;->getPresenceText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;->itemView:Landroid/view/View;

    invoke-static {p2, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$BannedItemClickListener;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;->unban:Lcom/microsoft/xbox/xle/ui/IconFontButton;

    invoke-static {p0, p2, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$ClubBannedListItemViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$BannedItemClickListener;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/ui/IconFontButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    return-void
.end method
