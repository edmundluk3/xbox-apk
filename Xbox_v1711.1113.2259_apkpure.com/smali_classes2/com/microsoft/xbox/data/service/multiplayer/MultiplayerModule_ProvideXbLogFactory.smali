.class public final Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideXbLogFactory;
.super Ljava/lang/Object;
.source "MultiplayerModule_ProvideXbLogFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory",
        "<",
        "Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final module:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideXbLogFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideXbLogFactory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;)V
    .locals 1
    .param p1, "module"    # Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    sget-boolean v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideXbLogFactory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 17
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideXbLogFactory;->module:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;

    .line 18
    return-void
.end method

.method public static create(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;)Ldagger/internal/Factory;
    .locals 1
    .param p0, "module"    # Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;",
            ")",
            "Ldagger/internal/Factory",
            "<",
            "Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    new-instance v0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideXbLogFactory;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideXbLogFactory;-><init>(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;)V

    return-object v0
.end method

.method public static proxyProvideXbLog(Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;)Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;->provideXbLog()Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public get()Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;
    .locals 2

    .prologue
    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideXbLogFactory;->module:Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;

    .line 23
    invoke-virtual {v0}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule;->provideXbLog()Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    .line 22
    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0}, Lcom/microsoft/xbox/data/service/multiplayer/MultiplayerModule_ProvideXbLogFactory;->get()Lcom/microsoft/xbox/xbservices/toolkit/IXBLog;

    move-result-object v0

    return-object v0
.end method
