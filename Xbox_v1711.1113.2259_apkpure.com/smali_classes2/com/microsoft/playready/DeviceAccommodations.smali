.class Lcom/microsoft/playready/DeviceAccommodations;
.super Ljava/lang/Object;
.source "DeviceAccommodations.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$microsoft$playready$DrmConfig$HLSPlaylistFormattingOption:[I


# direct methods
.method static synthetic $SWITCH_TABLE$com$microsoft$playready$DrmConfig$HLSPlaylistFormattingOption()[I
    .locals 3

    .prologue
    .line 21
    sget-object v0, Lcom/microsoft/playready/DeviceAccommodations;->$SWITCH_TABLE$com$microsoft$playready$DrmConfig$HLSPlaylistFormattingOption:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;->values()[Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;->DEFAULT:Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    invoke-virtual {v1}, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;->NONE:Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    invoke-virtual {v1}, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;->STRICT:Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    invoke-virtual {v1}, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;->VARIANT1:Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    invoke-virtual {v1}, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lcom/microsoft/playready/DeviceAccommodations;->$SWITCH_TABLE$com$microsoft$playready$DrmConfig$HLSPlaylistFormattingOption:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static adjustMasterPlaylist(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "masterPlaylistBody"    # Ljava/lang/String;

    .prologue
    .line 95
    move-object v0, p0

    .line 97
    .local v0, "adjustedPlaylist":Ljava/lang/String;
    invoke-static {v0}, Lcom/microsoft/playready/DeviceAccommodations;->adjustPlaylistStartingNewline(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 99
    return-object v0
.end method

.method public static adjustPlaylist(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "playlist"    # Ljava/lang/String;

    .prologue
    .line 86
    move-object v0, p0

    .line 88
    .local v0, "adjustedPlaylist":Ljava/lang/String;
    invoke-static {v0}, Lcom/microsoft/playready/DeviceAccommodations;->adjustPlaylistStartingNewline(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 90
    return-object v0
.end method

.method private static adjustPlaylistStartingNewline(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "playlist"    # Ljava/lang/String;

    .prologue
    .line 25
    move-object v0, p0

    .line 30
    .local v0, "adjustedPlaylist":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/playready/DrmConfig;->getPlaylistFormattingOverride()Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    move-result-object v1

    .line 31
    .local v1, "playlistFormat":Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;
    sget-object v2, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;->NONE:Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    if-ne v1, v2, :cond_0

    .line 33
    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v3, "LGE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 46
    sget-object v1, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;->VARIANT1:Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    .line 57
    :cond_0
    :goto_0
    invoke-static {}, Lcom/microsoft/playready/DeviceAccommodations;->$SWITCH_TABLE$com$microsoft$playready$DrmConfig$HLSPlaylistFormattingOption()[I

    move-result-object v2

    invoke-virtual {v1}, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 78
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Unknown HLS playlist format specified."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 50
    :cond_1
    sget-object v1, Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;->DEFAULT:Lcom/microsoft/playready/DrmConfig$HLSPlaylistFormattingOption;

    goto :goto_0

    .line 63
    :pswitch_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\r\n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 81
    :goto_1
    :pswitch_1
    return-object v0

    .line 73
    :pswitch_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "#EXTM3U\r\n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 74
    goto :goto_1

    .line 57
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
