.class public final Lcom/microsoft/xbox/service/model/clubs/ClubModelFactory;
.super Ljava/lang/Object;
.source "ClubModelFactory.java"


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Factory shouldn\'t be instantiated"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method private static addToRoster(Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;J)Ljava/util/List;
    .locals 6
    .param p1, "member"    # Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .param p2, "actorXuid"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;",
            ">;",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            "J)",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 261
    .local p0, "roster":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    invoke-static {p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModelFactory;->memberExistsInRoster(Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 264
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->userId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 265
    invoke-virtual {v0}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-wide v0, p2

    .line 262
    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->with(JJLjava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 269
    :cond_0
    return-object p0
.end method

.method private static calcCanViewerViewFeed(Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)Z
    .locals 1
    .param p1, "clubType"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;",
            "Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 614
    .local p0, "viewerRoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Banned:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {p0, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {p0, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Open:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static calcCanViewerViewRoster(Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)Z
    .locals 1
    .param p1, "clubType"    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;",
            "Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 618
    .local p0, "viewerRoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Banned:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {p0, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    invoke-interface {p0, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Open:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getClosedClubDefaultSettings(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .locals 8
    .param p0, "oldClub"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 414
    invoke-static {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 416
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 418
    .local v0, "viewerRoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v4

    .line 419
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v1

    .line 420
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;

    move-result-object v5

    .line 421
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;->post()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 422
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 423
    invoke-interface {v0, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 424
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    .line 421
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;->post(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;

    move-result-object v5

    .line 425
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Nonmember:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 426
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Closed:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 427
    invoke-static {v0, v7}, Lcom/microsoft/xbox/service/model/clubs/ClubModelFactory;->calcCanViewerViewFeed(Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 428
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    .line 425
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;->view(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;

    move-result-object v5

    .line 429
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v5

    .line 420
    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->feed(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v1

    .line 430
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;

    move-result-object v5

    .line 431
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->write()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 432
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 433
    invoke-interface {v0, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 434
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    .line 431
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;->write(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;

    move-result-object v5

    .line 435
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->setChatTopic()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 436
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 437
    invoke-interface {v0, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 438
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    .line 435
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;->setChatTopic(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;

    move-result-object v5

    .line 439
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 440
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 441
    invoke-interface {v0, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 442
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    .line 439
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;->view(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;

    move-result-object v5

    .line 443
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v5

    .line 430
    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->chat(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v5

    .line 444
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;

    move-result-object v6

    .line 445
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->join()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v1

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Nonmember:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 446
    invoke-virtual {v1, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v7

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Banned:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 447
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v7, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v1

    .line 448
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    .line 445
    invoke-virtual {v6, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;->join(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;

    move-result-object v1

    .line 449
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->create()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 450
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 451
    invoke-interface {v0, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 452
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    .line 449
    invoke-virtual {v1, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;->create(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;

    move-result-object v1

    .line 453
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v1

    .line 444
    invoke-virtual {v5, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->lfg(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v1

    .line 454
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;

    move-result-object v5

    .line 455
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->inviteOrAccept()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 456
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 457
    invoke-interface {v0, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 458
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    .line 455
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;->inviteOrAccept(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;

    move-result-object v5

    .line 459
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->kickOrBan()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 460
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 461
    invoke-interface {v0, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 462
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    .line 459
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;->kickOrBan(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;

    move-result-object v5

    .line 463
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Nonmember:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 464
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Closed:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 465
    invoke-static {v0, v7}, Lcom/microsoft/xbox/service/model/clubs/ClubModelFactory;->calcCanViewerViewRoster(Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 466
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    .line 463
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;->view(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;

    move-result-object v5

    .line 467
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v5

    .line 454
    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->roster(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v1

    .line 468
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;

    move-result-object v5

    .line 469
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;->update()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 470
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 471
    invoke-interface {v0, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 472
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    .line 469
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;->update(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;

    move-result-object v5

    .line 473
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;->delete()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 474
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 475
    invoke-interface {v0, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 476
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    .line 473
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;->delete(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;

    move-result-object v5

    .line 477
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Nonmember:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 478
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 479
    invoke-virtual {v6, v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 480
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    .line 477
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;->view(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;

    move-result-object v5

    .line 481
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v5

    .line 468
    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->profile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v1

    .line 482
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    .line 419
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->settings(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v1

    .line 483
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v4

    .line 484
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->matureContentEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 485
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 486
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    .line 484
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->matureContentEnabled(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v4

    .line 487
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->watchClubTitlesOnly()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 488
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v5, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v3

    .line 489
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    .line 487
    invoke-virtual {v4, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->watchClubTitlesOnly(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v3

    .line 490
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->leaveEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 491
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 492
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    .line 490
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->leaveEnabled(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v3

    .line 493
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->requestToJoinEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 494
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 495
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    .line 493
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->requestToJoinEnabled(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v3

    .line 496
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->transferOwnershipEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 497
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 498
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    .line 496
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->transferOwnershipEnabled(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v3

    .line 499
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->isRecommendable()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 500
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 501
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    .line 499
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->isRecommendable(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v3

    .line 502
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->isSearchable()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 503
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v2

    .line 504
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v2

    .line 502
    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->isSearchable(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v2

    .line 505
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v2

    .line 483
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->profile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v1

    .line 506
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    .line 418
    return-object v1

    :cond_0
    move v1, v3

    .line 447
    goto/16 :goto_0
.end method

.method public static getOpenClubDefaultSettings(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .locals 8
    .param p0, "oldClub"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 310
    invoke-static {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 312
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 314
    .local v0, "viewerRoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v4

    .line 315
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v1

    .line 316
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;

    move-result-object v5

    .line 317
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;->post()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 318
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 319
    invoke-interface {v0, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 320
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    .line 317
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;->post(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;

    move-result-object v5

    .line 321
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Nonmember:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 322
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Open:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 323
    invoke-static {v0, v7}, Lcom/microsoft/xbox/service/model/clubs/ClubModelFactory;->calcCanViewerViewFeed(Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 324
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    .line 321
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;->view(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;

    move-result-object v5

    .line 325
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v5

    .line 316
    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->feed(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v1

    .line 326
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;

    move-result-object v5

    .line 327
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->write()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 328
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 329
    invoke-interface {v0, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 330
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    .line 327
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;->write(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;

    move-result-object v5

    .line 331
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->setChatTopic()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 332
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 333
    invoke-interface {v0, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 334
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    .line 331
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;->setChatTopic(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;

    move-result-object v5

    .line 335
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 336
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 337
    invoke-interface {v0, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 338
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    .line 335
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;->view(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;

    move-result-object v5

    .line 339
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v5

    .line 326
    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->chat(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v5

    .line 340
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;

    move-result-object v6

    .line 341
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->join()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v1

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Nonmember:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 342
    invoke-virtual {v1, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v7

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Banned:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 343
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v7, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v1

    .line 344
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v1

    .line 341
    invoke-virtual {v6, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;->join(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;

    move-result-object v1

    .line 345
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->create()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 346
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 347
    invoke-interface {v0, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 348
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    .line 345
    invoke-virtual {v1, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;->create(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;

    move-result-object v1

    .line 349
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v1

    .line 340
    invoke-virtual {v5, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->lfg(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v1

    .line 350
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;

    move-result-object v5

    .line 351
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->inviteOrAccept()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 352
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 353
    invoke-interface {v0, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 354
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    .line 351
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;->inviteOrAccept(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;

    move-result-object v5

    .line 355
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->kickOrBan()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 356
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 357
    invoke-interface {v0, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 358
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    .line 355
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;->kickOrBan(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;

    move-result-object v5

    .line 359
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Nonmember:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 360
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Open:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 361
    invoke-static {v0, v7}, Lcom/microsoft/xbox/service/model/clubs/ClubModelFactory;->calcCanViewerViewRoster(Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 362
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    .line 359
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;->view(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;

    move-result-object v5

    .line 363
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v5

    .line 350
    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->roster(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v1

    .line 364
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;

    move-result-object v5

    .line 365
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;->update()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 366
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 367
    invoke-interface {v0, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 368
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    .line 365
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;->update(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;

    move-result-object v5

    .line 369
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;->delete()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 370
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 371
    invoke-interface {v0, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 372
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    .line 369
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;->delete(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;

    move-result-object v5

    .line 373
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Nonmember:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 374
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 375
    invoke-virtual {v6, v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v6

    .line 376
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v6

    .line 373
    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;->view(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;

    move-result-object v5

    .line 377
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v5

    .line 364
    invoke-virtual {v1, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->profile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v1

    .line 378
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    .line 315
    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->settings(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v1

    .line 379
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v4

    .line 380
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->matureContentEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 381
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 382
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    .line 380
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->matureContentEnabled(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v4

    .line 383
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->watchClubTitlesOnly()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v5

    .line 384
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v5, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v3

    .line 385
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    .line 383
    invoke-virtual {v4, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->watchClubTitlesOnly(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v3

    .line 386
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->leaveEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 387
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 388
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    .line 386
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->leaveEnabled(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v3

    .line 389
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->requestToJoinEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 390
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 391
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    .line 389
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->requestToJoinEnabled(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v3

    .line 392
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->transferOwnershipEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 393
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 394
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    .line 392
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->transferOwnershipEnabled(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v3

    .line 395
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->isRecommendable()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 396
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 397
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    .line 395
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->isRecommendable(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v3

    .line 398
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->isSearchable()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 399
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v2

    .line 400
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v2

    .line 398
    invoke-virtual {v3, v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->isSearchable(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v2

    .line 401
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v2

    .line 379
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->profile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v1

    .line 402
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    .line 314
    return-object v1

    :cond_0
    move v1, v3

    .line 343
    goto/16 :goto_0
.end method

.method public static getSecretClubDefaultSettings(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .locals 8
    .param p0, "oldClub"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 518
    invoke-static {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 520
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    .line 522
    .local v0, "viewerRoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v1

    .line 523
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v2

    .line 524
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;

    move-result-object v3

    .line 525
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;->post()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 526
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 527
    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 528
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    .line 525
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;->post(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;

    move-result-object v3

    .line 529
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 530
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Secret:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 531
    invoke-static {v0, v5}, Lcom/microsoft/xbox/service/model/clubs/ClubModelFactory;->calcCanViewerViewFeed(Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 532
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    .line 529
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;->view(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;

    move-result-object v3

    .line 533
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v3

    .line 524
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->feed(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v2

    .line 534
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;

    move-result-object v3

    .line 535
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->write()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 536
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 537
    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 538
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    .line 535
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;->write(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;

    move-result-object v3

    .line 539
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->setChatTopic()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 540
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 541
    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 542
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    .line 539
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;->setChatTopic(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;

    move-result-object v3

    .line 543
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 544
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 545
    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 546
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    .line 543
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;->view(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;

    move-result-object v3

    .line 547
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v3

    .line 534
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->chat(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v2

    .line 548
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;

    move-result-object v3

    .line 549
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->join()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 550
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 551
    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 552
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    .line 549
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;->join(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;

    move-result-object v3

    .line 553
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->create()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 554
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 555
    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 556
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    .line 553
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;->create(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;

    move-result-object v3

    .line 557
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v3

    .line 548
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->lfg(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v2

    .line 558
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;

    move-result-object v3

    .line 559
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->inviteOrAccept()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 560
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 561
    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 562
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    .line 559
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;->inviteOrAccept(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;

    move-result-object v3

    .line 563
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->kickOrBan()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 564
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 565
    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 566
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    .line 563
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;->kickOrBan(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;

    move-result-object v3

    .line 567
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 568
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;->Secret:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;

    .line 569
    invoke-static {v0, v5}, Lcom/microsoft/xbox/service/model/clubs/ClubModelFactory;->calcCanViewerViewRoster(Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 570
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    .line 567
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;->view(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;

    move-result-object v3

    .line 571
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v3

    .line 558
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->roster(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v2

    .line 572
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;

    move-result-object v3

    .line 573
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;->update()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 574
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 575
    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 576
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    .line 573
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;->update(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;

    move-result-object v3

    .line 577
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;->delete()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 578
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 579
    invoke-interface {v0, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 580
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    .line 577
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;->delete(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;

    move-result-object v3

    .line 581
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    sget-object v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 582
    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 583
    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v4

    .line 584
    invoke-virtual {v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v4

    .line 581
    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;->view(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;

    move-result-object v3

    .line 585
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v3

    .line 572
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->profile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v2

    .line 586
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v2

    .line 523
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->settings(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v1

    .line 587
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v2

    .line 588
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->matureContentEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v3

    .line 589
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v3

    .line 590
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    .line 588
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->matureContentEnabled(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v2

    .line 591
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->watchClubTitlesOnly()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v3

    .line 592
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v3

    .line 593
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    .line 591
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->watchClubTitlesOnly(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v2

    .line 594
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->leaveEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v3

    .line 595
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v3

    .line 596
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    .line 594
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->leaveEnabled(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v2

    .line 597
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->requestToJoinEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v3

    .line 598
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v3

    .line 599
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    .line 597
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->requestToJoinEnabled(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v2

    .line 600
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->transferOwnershipEnabled()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v3

    .line 601
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v3

    .line 602
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    .line 600
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->transferOwnershipEnabled(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v2

    .line 603
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->isRecommendable()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v3

    .line 604
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v3

    .line 605
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    .line 603
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->isRecommendable(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v2

    .line 606
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;->isSearchable()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v3

    .line 607
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->value(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v3

    .line 608
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v3

    .line 606
    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->isSearchable(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;

    move-result-object v2

    .line 609
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;

    move-result-object v2

    .line 587
    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->profile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfile;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v1

    .line 610
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    .line 522
    return-object v1
.end method

.method private static memberExistsInRoster(Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;)Z
    .locals 6
    .param p1, "member"    # Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;",
            ">;",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 276
    .local p0, "roster":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;

    .line 277
    .local v0, "item":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->xuid()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->userId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 278
    const/4 v1, 0x1

    .line 282
    .end local v0    # "item":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static modifyWithClubMembers(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
    .locals 42
    .param p0, "oldClub"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;"
        }
    .end annotation

    .prologue
    .line 63
    .local p1, "clubMembers":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    invoke-static/range {p0 .. p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)Z

    move-result v36

    invoke-static/range {v36 .. v36}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 65
    invoke-static/range {p1 .. p1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    .line 66
    .local v7, "clubMembersCopy":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 68
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->settings()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v34

    .line 69
    .local v34, "oldSettings":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-result-object v33

    .line 71
    .local v33, "oldRoster":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    const/16 v28, 0x0

    .line 72
    .local v28, "newViewerRoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    if-eqz v33, :cond_3

    invoke-virtual/range {v33 .. v33}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->moderator()Lcom/google/common/collect/ImmutableList;

    move-result-object v36

    invoke-static/range {v36 .. v36}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopyWritable(Ljava/util/List;)Ljava/util/List;

    move-result-object v23

    .line 73
    .local v23, "newModerators":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    :goto_0
    if-eqz v33, :cond_4

    invoke-virtual/range {v33 .. v33}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->requestedToJoin()Lcom/google/common/collect/ImmutableList;

    move-result-object v36

    invoke-static/range {v36 .. v36}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopyWritable(Ljava/util/List;)Ljava/util/List;

    move-result-object v27

    .line 74
    .local v27, "newRequestedToJoin":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    :goto_1
    if-eqz v33, :cond_5

    invoke-virtual/range {v33 .. v33}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->recommended()Lcom/google/common/collect/ImmutableList;

    move-result-object v36

    invoke-static/range {v36 .. v36}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopyWritable(Ljava/util/List;)Ljava/util/List;

    move-result-object v26

    .line 76
    .local v26, "newRecommended":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    :goto_2
    if-eqz v33, :cond_6

    invoke-virtual/range {v33 .. v33}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->banned()Lcom/google/common/collect/ImmutableList;

    move-result-object v36

    invoke-static/range {v36 .. v36}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopy(Ljava/util/List;)Ljava/util/List;

    move-result-object v31

    .line 77
    .local v31, "oldBanned":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    :goto_3
    new-instance v21, Ljava/util/ArrayList;

    move-object/from16 v0, v21

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 79
    .local v21, "newBanned":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresence()Lcom/google/common/collect/ImmutableList;

    move-result-object v36

    invoke-static/range {v36 .. v36}, Lcom/microsoft/xbox/toolkit/JavaUtil;->safeCopyWritable(Ljava/util/List;)Ljava/util/List;

    move-result-object v22

    .line 80
    .local v22, "newClubPresence":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->clubPresence()Lcom/google/common/collect/ImmutableList;

    move-result-object v36

    invoke-static/range {v36 .. v36}, Lcom/microsoft/xbox/toolkit/ListUtil;->nullToEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v32

    .line 81
    .local v32, "oldClubPresence":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;>;"
    new-instance v20, Ljava/util/ArrayList;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v36

    move-object/from16 v0, v20

    move/from16 v1, v36

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 82
    .local v20, "membersInResponse":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    new-instance v29, Ljava/util/ArrayList;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v36

    move-object/from16 v0, v29

    move/from16 v1, v36

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 84
    .local v29, "nonMembersInResponse":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v36

    invoke-static/range {v36 .. v36}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    .line 86
    .local v14, "meXuid":J
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v36

    :cond_0
    :goto_4
    invoke-interface/range {v36 .. v36}, Ljava/util/Iterator;->hasNext()Z

    move-result v37

    if-eqz v37, :cond_12

    invoke-interface/range {v36 .. v36}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    .line 87
    .local v16, "member":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->getError()Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMemberError;

    move-result-object v37

    if-nez v37, :cond_0

    .line 92
    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->userId()Ljava/lang/String;

    move-result-object v37

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v38

    invoke-virtual/range {v37 .. v38}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    .line 93
    .local v12, "isMe":Z
    if-eqz v12, :cond_1

    .line 94
    new-instance v28, Ljava/util/ArrayList;

    .end local v28    # "newViewerRoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v37

    move-object/from16 v0, v28

    move-object/from16 v1, v37

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 97
    .restart local v28    # "newViewerRoles":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;>;"
    :cond_1
    const/4 v9, 0x0

    .line 98
    .local v9, "hasModeratorRole":Z
    const/4 v11, 0x0

    .line 99
    .local v11, "hasRequestedToJoinRole":Z
    const/4 v10, 0x0

    .line 100
    .local v10, "hasRecommendedRole":Z
    const/4 v8, 0x0

    .line 101
    .local v8, "hasBannedRole":Z
    const/4 v13, 0x0

    .line 103
    .local v13, "isMember":Z
    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->roles()Lcom/google/common/collect/ImmutableList;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Lcom/google/common/collect/ImmutableList;->iterator()Lcom/google/common/collect/UnmodifiableIterator;

    move-result-object v37

    :cond_2
    :goto_5
    invoke-interface/range {v37 .. v37}, Ljava/util/Iterator;->hasNext()Z

    move-result v38

    if-eqz v38, :cond_c

    invoke-interface/range {v37 .. v37}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    .line 104
    .local v35, "role":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;
    sget-object v38, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    move-object/from16 v0, v35

    move-object/from16 v1, v38

    if-ne v0, v1, :cond_7

    .line 105
    const/4 v9, 0x1

    goto :goto_5

    .line 72
    .end local v8    # "hasBannedRole":Z
    .end local v9    # "hasModeratorRole":Z
    .end local v10    # "hasRecommendedRole":Z
    .end local v11    # "hasRequestedToJoinRole":Z
    .end local v12    # "isMe":Z
    .end local v13    # "isMember":Z
    .end local v14    # "meXuid":J
    .end local v16    # "member":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .end local v20    # "membersInResponse":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    .end local v21    # "newBanned":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    .end local v22    # "newClubPresence":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;>;"
    .end local v23    # "newModerators":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    .end local v26    # "newRecommended":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    .end local v27    # "newRequestedToJoin":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    .end local v29    # "nonMembersInResponse":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    .end local v31    # "oldBanned":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    .end local v32    # "oldClubPresence":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;>;"
    .end local v35    # "role":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;
    :cond_3
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    goto/16 :goto_0

    .line 73
    .restart local v23    # "newModerators":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    :cond_4
    new-instance v27, Ljava/util/ArrayList;

    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    goto/16 :goto_1

    .line 74
    .restart local v27    # "newRequestedToJoin":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    :cond_5
    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    goto/16 :goto_2

    .line 76
    .restart local v26    # "newRecommended":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    :cond_6
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v31

    goto/16 :goto_3

    .line 106
    .restart local v8    # "hasBannedRole":Z
    .restart local v9    # "hasModeratorRole":Z
    .restart local v10    # "hasRecommendedRole":Z
    .restart local v11    # "hasRequestedToJoinRole":Z
    .restart local v12    # "isMe":Z
    .restart local v13    # "isMember":Z
    .restart local v14    # "meXuid":J
    .restart local v16    # "member":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .restart local v20    # "membersInResponse":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    .restart local v21    # "newBanned":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    .restart local v22    # "newClubPresence":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;>;"
    .restart local v29    # "nonMembersInResponse":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;>;"
    .restart local v31    # "oldBanned":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    .restart local v32    # "oldClubPresence":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;>;"
    .restart local v35    # "role":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;
    :cond_7
    sget-object v38, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->RequestedToJoin:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    move-object/from16 v0, v35

    move-object/from16 v1, v38

    if-ne v0, v1, :cond_8

    .line 107
    const/4 v11, 0x1

    goto :goto_5

    .line 108
    :cond_8
    sget-object v38, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Recommended:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    move-object/from16 v0, v35

    move-object/from16 v1, v38

    if-ne v0, v1, :cond_9

    .line 109
    const/4 v10, 0x1

    goto :goto_5

    .line 110
    :cond_9
    sget-object v38, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Banned:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    move-object/from16 v0, v35

    move-object/from16 v1, v38

    if-ne v0, v1, :cond_a

    .line 111
    const/4 v8, 0x1

    goto :goto_5

    .line 112
    :cond_a
    sget-object v38, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Member:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    move-object/from16 v0, v35

    move-object/from16 v1, v38

    if-eq v0, v1, :cond_b

    sget-object v38, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Moderator:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    move-object/from16 v0, v35

    move-object/from16 v1, v38

    if-eq v0, v1, :cond_b

    sget-object v38, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;->Owner:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;

    move-object/from16 v0, v35

    move-object/from16 v1, v38

    if-ne v0, v1, :cond_2

    .line 113
    :cond_b
    const/4 v13, 0x1

    goto :goto_5

    .line 117
    .end local v35    # "role":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;
    :cond_c
    if-eqz v13, :cond_d

    .line 118
    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    :goto_6
    if-eqz v9, :cond_e

    .line 124
    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-static {v0, v1, v14, v15}, Lcom/microsoft/xbox/service/model/clubs/ClubModelFactory;->addToRoster(Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;J)Ljava/util/List;

    move-result-object v23

    .line 127
    :goto_7
    if-eqz v11, :cond_f

    .line 128
    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->userId()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v37 .. v37}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v38

    move-object/from16 v0, v27

    move-object/from16 v1, v16

    move-wide/from16 v2, v38

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/clubs/ClubModelFactory;->addToRoster(Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;J)Ljava/util/List;

    move-result-object v27

    .line 131
    :goto_8
    if-eqz v10, :cond_10

    .line 132
    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-static {v0, v1, v14, v15}, Lcom/microsoft/xbox/service/model/clubs/ClubModelFactory;->addToRoster(Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;J)Ljava/util/List;

    move-result-object v26

    .line 135
    :goto_9
    if-eqz v8, :cond_11

    .line 136
    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-static {v0, v1, v14, v15}, Lcom/microsoft/xbox/service/model/clubs/ClubModelFactory;->addToRoster(Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;J)Ljava/util/List;

    move-result-object v21

    .line 138
    :goto_a
    goto/16 :goto_4

    .line 120
    :cond_d
    move-object/from16 v0, v29

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 125
    :cond_e
    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModelFactory;->removeFromRoster(Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;)Ljava/util/List;

    move-result-object v23

    goto :goto_7

    .line 129
    :cond_f
    move-object/from16 v0, v27

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModelFactory;->removeFromRoster(Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;)Ljava/util/List;

    move-result-object v27

    goto :goto_8

    .line 133
    :cond_10
    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModelFactory;->removeFromRoster(Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;)Ljava/util/List;

    move-result-object v26

    goto :goto_9

    .line 137
    :cond_11
    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModelFactory;->removeFromRoster(Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;)Ljava/util/List;

    move-result-object v21

    goto :goto_a

    .line 140
    .end local v8    # "hasBannedRole":Z
    .end local v9    # "hasModeratorRole":Z
    .end local v10    # "hasRecommendedRole":Z
    .end local v11    # "hasRequestedToJoinRole":Z
    .end local v12    # "isMe":Z
    .end local v13    # "isMember":Z
    .end local v16    # "member":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    :cond_12
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->hashCode()I

    move-result v36

    invoke-interface/range {v31 .. v31}, Ljava/util/List;->hashCode()I

    move-result v37

    move/from16 v0, v36

    move/from16 v1, v37

    if-eq v0, v1, :cond_15

    .line 141
    invoke-interface/range {v32 .. v32}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v36

    :cond_13
    :goto_b
    invoke-interface/range {v36 .. v36}, Ljava/util/Iterator;->hasNext()Z

    move-result v37

    if-eqz v37, :cond_15

    invoke-interface/range {v36 .. v36}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    .line 142
    .local v17, "memberPresence":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v37

    :cond_14
    invoke-interface/range {v37 .. v37}, Ljava/util/Iterator;->hasNext()Z

    move-result v38

    if-eqz v38, :cond_13

    invoke-interface/range {v37 .. v37}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;

    .line 143
    .local v5, "bannedUser":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->xuid()J

    move-result-wide v38

    invoke-virtual {v5}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->xuid()J

    move-result-wide v40

    cmp-long v38, v38, v40

    if-nez v38, :cond_14

    .line 144
    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_b

    .line 151
    .end local v5    # "bannedUser":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    .end local v17    # "memberPresence":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
    :cond_15
    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v36

    new-instance v37, Ljava/util/Date;

    invoke-direct/range {v37 .. v37}, Ljava/util/Date;-><init>()V

    invoke-virtual/range {v36 .. v37}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v30

    .line 153
    .local v30, "nowDateString":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->membersCount()J

    move-result-wide v24

    .line 155
    .local v24, "newMembersCount":J
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v36

    :cond_16
    :goto_c
    invoke-interface/range {v36 .. v36}, Ljava/util/Iterator;->hasNext()Z

    move-result v37

    if-eqz v37, :cond_19

    invoke-interface/range {v36 .. v36}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    .line 156
    .restart local v16    # "member":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    const/4 v4, 0x0

    .line 157
    .local v4, "alreadyInPresence":Z
    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->userId()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v37 .. v37}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v18

    .line 159
    .local v18, "memberXuid":J
    invoke-interface/range {v32 .. v32}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v37

    :cond_17
    invoke-interface/range {v37 .. v37}, Ljava/util/Iterator;->hasNext()Z

    move-result v38

    if-eqz v38, :cond_18

    invoke-interface/range {v37 .. v37}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    .line 160
    .restart local v17    # "memberPresence":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->xuid()J

    move-result-wide v38

    cmp-long v38, v38, v18

    if-nez v38, :cond_17

    .line 161
    const/4 v4, 0x1

    .line 166
    .end local v17    # "memberPresence":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
    :cond_18
    if-nez v4, :cond_16

    .line 167
    const-wide/16 v38, 0x1

    add-long v24, v24, v38

    .line 168
    sget-object v37, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->Unknown:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    move-wide/from16 v0, v18

    move-object/from16 v2, v30

    move-object/from16 v3, v37

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->with(JLjava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    move-result-object v37

    move-object/from16 v0, v22

    move-object/from16 v1, v37

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 172
    .end local v4    # "alreadyInPresence":Z
    .end local v16    # "member":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .end local v18    # "memberXuid":J
    :cond_19
    invoke-interface/range {v29 .. v29}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v36

    :cond_1a
    :goto_d
    invoke-interface/range {v36 .. v36}, Ljava/util/Iterator;->hasNext()Z

    move-result v37

    if-eqz v37, :cond_1c

    invoke-interface/range {v36 .. v36}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;

    .line 173
    .restart local v16    # "member":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    invoke-interface/range {v32 .. v32}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v37

    :cond_1b
    invoke-interface/range {v37 .. v37}, Ljava/util/Iterator;->hasNext()Z

    move-result v38

    if-eqz v38, :cond_1a

    invoke-interface/range {v37 .. v37}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;

    .line 174
    .restart local v17    # "memberPresence":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
    invoke-virtual/range {v17 .. v17}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;->xuid()J

    move-result-wide v38

    invoke-virtual/range {v16 .. v16}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->userId()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v40 .. v40}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v40

    cmp-long v38, v38, v40

    if-nez v38, :cond_1b

    .line 175
    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 176
    const-wide/16 v38, 0x1

    sub-long v24, v24, v38

    .line 177
    goto :goto_d

    .line 182
    .end local v16    # "member":Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .end local v17    # "memberPresence":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubMemberPresence;
    :cond_1c
    invoke-virtual/range {p0 .. p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v36

    .line 183
    move-object/from16 v0, v23

    move-object/from16 v1, v27

    move-object/from16 v2, v26

    move-object/from16 v3, v21

    invoke-static {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->with(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->roster(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v36

    .line 188
    move-object/from16 v0, v36

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->membersCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v36

    .line 189
    invoke-interface/range {v23 .. v23}, Ljava/util/List;->size()I

    move-result v37

    move/from16 v0, v37

    int-to-long v0, v0

    move-wide/from16 v38, v0

    move-object/from16 v0, v36

    move-wide/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->moderatorsCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v36

    .line 190
    invoke-interface/range {v26 .. v26}, Ljava/util/List;->size()I

    move-result v37

    move/from16 v0, v37

    int-to-long v0, v0

    move-wide/from16 v38, v0

    move-object/from16 v0, v36

    move-wide/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->recommendedCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v36

    .line 191
    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v37

    move/from16 v0, v37

    int-to-long v0, v0

    move-wide/from16 v38, v0

    move-object/from16 v0, v36

    move-wide/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->requestedToJoinCount(J)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v36

    .line 192
    move-object/from16 v0, v36

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->clubPresence(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    move-result-object v6

    .line 194
    .local v6, "clubBuilder":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;
    invoke-static/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Z

    move-result v36

    if-eqz v36, :cond_1d

    if-eqz v28, :cond_1d

    .line 196
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v36

    .line 197
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;

    move-result-object v37

    .line 198
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;->post()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 199
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;->post()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v39

    move-object/from16 v0, v28

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v39

    invoke-virtual/range {v38 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 200
    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    .line 198
    invoke-virtual/range {v37 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;->post(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;

    move-result-object v37

    .line 201
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 202
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->feed()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v39

    move-object/from16 v0, v28

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v39

    invoke-virtual/range {v38 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 203
    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    .line 201
    invoke-virtual/range {v37 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;->view(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;

    move-result-object v37

    .line 204
    invoke-virtual/range {v37 .. v37}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;

    move-result-object v37

    .line 197
    invoke-virtual/range {v36 .. v37}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->feed(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubFeedSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v36

    .line 205
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;

    move-result-object v37

    .line 206
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->write()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 207
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->write()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v39

    move-object/from16 v0, v28

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v39

    invoke-virtual/range {v38 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 208
    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    .line 206
    invoke-virtual/range {v37 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;->write(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;

    move-result-object v37

    .line 209
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->setChatTopic()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 210
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->setChatTopic()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v39

    move-object/from16 v0, v28

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v39

    invoke-virtual/range {v38 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 211
    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    .line 209
    invoke-virtual/range {v37 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;->setChatTopic(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;

    move-result-object v37

    .line 212
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 213
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->chat()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v39

    move-object/from16 v0, v28

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v39

    invoke-virtual/range {v38 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 214
    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    .line 212
    invoke-virtual/range {v37 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;->view(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;

    move-result-object v37

    .line 215
    invoke-virtual/range {v37 .. v37}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;

    move-result-object v37

    .line 205
    invoke-virtual/range {v36 .. v37}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->chat(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubChatSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v36

    .line 216
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;

    move-result-object v37

    .line 217
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->join()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 218
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->join()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v39

    move-object/from16 v0, v28

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v39

    invoke-virtual/range {v38 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 219
    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    .line 217
    invoke-virtual/range {v37 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;->join(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;

    move-result-object v37

    .line 220
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->create()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 221
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->lfg()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;->create()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v39

    move-object/from16 v0, v28

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v39

    invoke-virtual/range {v38 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 222
    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    .line 220
    invoke-virtual/range {v37 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;->create(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;

    move-result-object v37

    .line 223
    invoke-virtual/range {v37 .. v37}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;

    move-result-object v37

    .line 216
    invoke-virtual/range {v36 .. v37}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->lfg(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubLfgSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v36

    .line 224
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;

    move-result-object v37

    .line 225
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->inviteOrAccept()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 226
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->inviteOrAccept()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v39

    move-object/from16 v0, v28

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v39

    invoke-virtual/range {v38 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 227
    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    .line 225
    invoke-virtual/range {v37 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;->inviteOrAccept(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;

    move-result-object v37

    .line 228
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->kickOrBan()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 229
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->kickOrBan()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v39

    move-object/from16 v0, v28

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v39

    invoke-virtual/range {v38 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 230
    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    .line 228
    invoke-virtual/range {v37 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;->kickOrBan(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;

    move-result-object v37

    .line 231
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 232
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v39

    move-object/from16 v0, v28

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v39

    invoke-virtual/range {v38 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 233
    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    .line 231
    invoke-virtual/range {v37 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;->view(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;

    move-result-object v37

    .line 234
    invoke-virtual/range {v37 .. v37}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;

    move-result-object v37

    .line 224
    invoke-virtual/range {v36 .. v37}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->roster(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v36

    .line 235
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;

    move-result-object v37

    .line 236
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;->update()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 237
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;->update()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v39

    move-object/from16 v0, v28

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v39

    invoke-virtual/range {v38 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 238
    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    .line 236
    invoke-virtual/range {v37 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;->update(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;

    move-result-object v37

    .line 239
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;->delete()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 240
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;->delete()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v39

    move-object/from16 v0, v28

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v39

    invoke-virtual/range {v38 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 241
    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    .line 239
    invoke-virtual/range {v37 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;->delete(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;

    move-result-object v37

    .line 242
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 243
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->profile()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;->view()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v39

    move-object/from16 v0, v28

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v39

    invoke-virtual/range {v38 .. v39}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->canViewerAct(Z)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;

    move-result-object v38

    .line 244
    invoke-virtual/range {v38 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v38

    .line 242
    invoke-virtual/range {v37 .. v38}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;->view(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;

    move-result-object v37

    .line 245
    invoke-virtual/range {v37 .. v37}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;

    move-result-object v37

    .line 235
    invoke-virtual/range {v36 .. v37}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->profile(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v36

    .line 246
    invoke-virtual/range {v34 .. v34}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;->viewerRoles()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;->toBuilder()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles$Builder;

    move-result-object v37

    move-object/from16 v0, v37

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles$Builder;->roles(Ljava/util/List;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles$Builder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->viewerRoles(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettingsViewerRoles;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;

    move-result-object v36

    .line 247
    invoke-virtual/range {v36 .. v36}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;

    move-result-object v36

    .line 195
    move-object/from16 v0, v36

    invoke-virtual {v6, v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->settings(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubSettings;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;

    .line 250
    :cond_1d
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club$Builder;->build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v36

    return-object v36
.end method

.method private static removeFromRoster(Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;)Ljava/util/List;
    .locals 6
    .param p1, "member"    # Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;",
            ">;",
            "Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 291
    .local p0, "roster":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;

    .line 292
    .local v0, "item":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->xuid()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubRosterDataTypes$ClubMember;->userId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 293
    invoke-interface {p0, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 298
    .end local v0    # "item":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    :cond_1
    return-object p0
.end method
