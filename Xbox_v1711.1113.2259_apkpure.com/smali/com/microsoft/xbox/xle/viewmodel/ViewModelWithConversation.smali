.class public abstract Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;
.source "ViewModelWithConversation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;
    }
.end annotation


# static fields
.field private static final PRESENCE_UPDATE_INTERVAL:J = 0x3a98L


# instance fields
.field private final attachmentErrorResId:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field chatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field protected conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

.field private conversationSummarySynthesized:Z

.field protected isGroupConversation:Z

.field private final isLoadingAttachment:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field protected isLoadingConversationDetail:Z

.field protected isServiceMessage:Z

.field private final loadAttachmentTasks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;",
            ">;"
        }
    .end annotation
.end field

.field protected loadConversationDetailAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadConversationDetailsAsyncTask;

.field private messageBody:Ljava/lang/String;

.field protected messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

.field protected presenceUpdateHandler:Ljava/lang/Runnable;

.field private shouldScrollToBottom:Z

.field private skypeConversationMessages:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

.field protected viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 83
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;-><init>()V

    .line 66
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummarySynthesized:Z

    .line 67
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 68
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->isGroupConversation:Z

    .line 69
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->isServiceMessage:Z

    .line 71
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->isLoadingAttachment:Ljava/util/HashMap;

    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->loadAttachmentTasks:Ljava/util/HashMap;

    .line 73
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->attachmentErrorResId:Ljava/util/HashMap;

    .line 74
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->shouldScrollToBottom:Z

    .line 76
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->messageBody:Ljava/lang/String;

    .line 84
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;)V

    .line 85
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 2
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithEntity;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 66
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummarySynthesized:Z

    .line 67
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 68
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->isGroupConversation:Z

    .line 69
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->isServiceMessage:Z

    .line 71
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->isLoadingAttachment:Ljava/util/HashMap;

    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->loadAttachmentTasks:Ljava/util/HashMap;

    .line 73
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->attachmentErrorResId:Ljava/util/HashMap;

    .line 74
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->shouldScrollToBottom:Z

    .line 76
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->messageBody:Ljava/lang/String;

    .line 89
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;)V

    .line 90
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "x2"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->onLoadAttachmentCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->isLoadingAttachment:Ljava/util/HashMap;

    return-object v0
.end method

.method private deleteSkypeCurrentMessageInternal(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "targetXuid"    # Ljava/lang/String;
    .param p2, "messageId"    # Ljava/lang/String;
    .param p3, "clientMessageId"    # Ljava/lang/String;

    .prologue
    .line 541
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 542
    sget-object v0, Lcom/microsoft/xbox/service/model/UpdateType;->MessageDelete:Lcom/microsoft/xbox/service/model/UpdateType;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->setUpdateTypesToCheck(Ljava/util/EnumSet;)V

    .line 544
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v0, p2, p1, p3}, Lcom/microsoft/xbox/service/model/MessageModel;->deleteSkypeMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->updateAdapter()V

    .line 551
    :goto_0
    return-void

    .line 547
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not delete message: targetXuid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " messageId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    const-string v0, "Could not delete message."

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 549
    const v0, 0x7f070b6d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->showError(I)V

    goto :goto_0
.end method

.method static synthetic lambda$deleteSkypeMessage$0(Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;
    .param p1, "targetXuid"    # Ljava/lang/String;
    .param p2, "messageId"    # Ljava/lang/String;
    .param p3, "clientMessageId"    # Ljava/lang/String;

    .prologue
    .line 564
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->deleteSkypeCurrentMessageInternal(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private onLoadAttachmentCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "message"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    .prologue
    .line 483
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLoadAttachmentCompleted status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->isLoadingAttachment:Ljava/util/HashMap;

    iget-object v1, p2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->activityfeedItemLocator:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 501
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->updateAdapter()V

    .line 502
    return-void

    .line 490
    :pswitch_0
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->TAG:Ljava/lang/String;

    const-string v1, "Attachment loaded successfully"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->attachmentErrorResId:Ljava/util/HashMap;

    iget-object v1, p2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->activityfeedItemLocator:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 492
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->getEntityModel(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Lcom/microsoft/xbox/service/model/entity/EntityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->getEntity()Lcom/microsoft/xbox/service/model/entity/Entity;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->setAttachment(Lcom/microsoft/xbox/service/model/entity/Entity;)V

    goto :goto_0

    .line 496
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->TAG:Ljava/lang/String;

    const-string v1, "Error in getting message attachment, won\'t be able to display it"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->attachmentErrorResId:Ljava/util/HashMap;

    iget-object v1, p2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->activityfeedItemLocator:Ljava/lang/String;

    const v2, 0x7f070752

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 486
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public cancelLoadAttachments()V
    .locals 4

    .prologue
    .line 512
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->loadAttachmentTasks:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    .line 513
    .local v1, "taskList":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;>;"
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;

    .line 514
    .local v0, "task":Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;
    if-eqz v0, :cond_0

    .line 515
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;->cancel()V

    goto :goto_0

    .line 518
    .end local v0    # "task":Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;
    :cond_1
    return-void
.end method

.method public clearShouldScrollToBottom()V
    .locals 1

    .prologue
    .line 529
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->shouldScrollToBottom:Z

    .line 530
    return-void
.end method

.method public deleteSkypeMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "targetXuid"    # Ljava/lang/String;
    .param p2, "messageId"    # Ljava/lang/String;
    .param p3, "clientMessageId"    # Ljava/lang/String;

    .prologue
    .line 554
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 556
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->chatHeadRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;->hoverChatIsOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 558
    invoke-direct {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->deleteSkypeCurrentMessageInternal(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    :goto_0
    return-void

    .line 560
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f07073e

    .line 561
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f07074f

    .line 562
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070738

    .line 563
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Runnable;

    move-result-object v4

    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070736

    .line 565
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/toolkit/JavaUtil;->NO_OP:Ljava/lang/Runnable;

    move-object v0, p0

    .line 560
    invoke-virtual/range {v0 .. v6}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public getActivityFeedItemLocatorFromSkypeMessage(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeAttachmentContent;
    .locals 1
    .param p1, "message"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .prologue
    .line 309
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->hasSkypeAttachment(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->content:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->getSkypeAttachmentMessageContent(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeAttachmentContent;

    move-result-object v0

    .line 313
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAttachment(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Lcom/microsoft/xbox/service/model/entity/Entity;
    .locals 1
    .param p1, "message"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    .prologue
    .line 397
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->getAttachment()Lcom/microsoft/xbox/service/model/entity/Entity;

    move-result-object v0

    return-object v0
.end method

.method public getAttachmentErrorResId(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)I
    .locals 2
    .param p1, "message"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    .prologue
    .line 393
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->hasAttachmentLoadingError(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->attachmentErrorResId:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->activityfeedItemLocator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getEntityModel(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Lcom/microsoft/xbox/service/model/entity/EntityModel;
    .locals 1
    .param p1, "message"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    .prologue
    .line 505
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->hasAttachment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 506
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->activityfeedItemLocator:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->getInstance(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/entity/EntityModel;

    move-result-object v0

    .line 508
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGroupMemberPic(Ljava/lang/String;I)Ljava/lang/String;
    .locals 6
    .param p1, "conversationId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "index"    # I
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 197
    const/4 v4, -0x1

    if-le p2, v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/Preconditions;->isTrue(Z)V

    .line 198
    const/4 v1, 0x0

    .line 200
    .local v1, "count":I
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 201
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v4, p1}, Lcom/microsoft/xbox/service/model/MessageModel;->getSkypeConversationMessages(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    move-result-object v0

    .line 202
    .local v0, "conversationMessages":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    if-eqz v0, :cond_2

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForGroupMembers:Ljava/util/Map;

    if-eqz v4, :cond_2

    .line 203
    iget-object v4, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForGroupMembers:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    .line 204
    .local v3, "gms":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    .line 205
    .local v2, "gm":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    if-ge v1, p2, :cond_1

    .line 206
    add-int/lit8 v1, v1, 0x1

    .line 207
    goto :goto_1

    .line 197
    .end local v0    # "conversationMessages":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    .end local v1    # "count":I
    .end local v2    # "gm":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    .end local v3    # "gms":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    .line 210
    .restart local v0    # "conversationMessages":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    .restart local v1    # "count":I
    .restart local v2    # "gm":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    .restart local v3    # "gms":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    :cond_1
    iget-object v4, v2, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->gamerPicUrl:Ljava/lang/String;

    .line 215
    .end local v0    # "conversationMessages":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    .end local v2    # "gm":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    .end local v3    # "gms":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    :goto_2
    return-object v4

    :cond_2
    const-string v4, ""

    goto :goto_2
.end method

.method public getGroupSenderGamertag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "conversationId"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 150
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 156
    :goto_0
    return-object v1

    .line 154
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 155
    .local v0, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    invoke-virtual {p0, p1, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->getGroupSenderGamertag(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getGroupSenderGamertag(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "conversationId"    # Ljava/lang/String;
    .param p3, "gamertagToIgnore"    # Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p2, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 161
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 192
    :cond_0
    :goto_0
    return-object v3

    .line 165
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 166
    .local v0, "gamertags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 167
    .local v2, "id":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 168
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    invoke-virtual {v5, p1}, Lcom/microsoft/xbox/service/model/MessageModel;->getSkypeConversationMessages(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    move-result-object v5

    iput-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->skypeConversationMessages:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    .line 169
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->skypeConversationMessages:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    if-eqz v5, :cond_2

    .line 170
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->skypeConversationMessages:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForGroupMembers:Ljava/util/Map;

    if-eqz v5, :cond_3

    .line 171
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->skypeConversationMessages:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForGroupMembers:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    .line 172
    .local v1, "gm":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    if-eqz v1, :cond_3

    .line 173
    iget-object v5, v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->senderGamerTag:Ljava/lang/String;

    invoke-static {v5, p3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualNonNull(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 174
    iget-object v5, v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->senderGamerTag:Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 180
    .end local v1    # "gm":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    :cond_3
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->skypeConversationMessages:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForNonGroupMembers:Ljava/util/Map;

    if-eqz v5, :cond_2

    .line 181
    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->skypeConversationMessages:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    iget-object v5, v5, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForNonGroupMembers:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    .line 182
    .restart local v1    # "gm":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    if-eqz v1, :cond_2

    .line 183
    iget-object v5, v1, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->senderGamerTag:Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 189
    .end local v1    # "gm":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    .end local v2    # "id":Ljava/lang/String;
    :cond_4
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 190
    const-string v3, ", "

    invoke-static {v3, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public getIsRecipientNonEmpty()Z
    .locals 1

    .prologue
    .line 578
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderGamerTag:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->realName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMemberOnlineCount()I
    .locals 6

    .prologue
    .line 238
    const/4 v2, 0x0

    .line 240
    .local v2, "onlineCount":I
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->skypeConversationMessages:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->skypeConversationMessages:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForGroupMembers:Ljava/util/Map;

    if-eqz v3, :cond_1

    .line 241
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->skypeConversationMessages:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForGroupMembers:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    .line 243
    .local v1, "members":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 244
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    .line 245
    .local v0, "gm":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    iget-object v4, v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    sget-object v5, Lcom/microsoft/xbox/service/model/UserStatus;->Online:Lcom/microsoft/xbox/service/model/UserStatus;

    if-ne v4, v5, :cond_0

    .line 246
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 252
    .end local v0    # "gm":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    .end local v1    # "members":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    :cond_1
    return v2
.end method

.method public getMemberPics()Ljava/util/List;
    .locals 5
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->skypeConversationMessages:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->skypeConversationMessages:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForGroupMembers:Ljava/util/Map;

    if-eqz v3, :cond_0

    .line 222
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->skypeConversationMessages:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->lookupDetailsForGroupMembers:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    .line 224
    .local v2, "members":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 225
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 226
    .local v1, "memberPicUrls":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;

    .line 227
    .local v0, "gm":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    iget-object v4, v0, Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;->gamerPicUrl:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 234
    .end local v0    # "gm":Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;
    .end local v1    # "memberPicUrls":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "members":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/groupMessaging/SkypeGroupDataTypes$GroupMember;>;"
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    :cond_1
    return-object v1
.end method

.method public getMessageBody()Ljava/lang/String;
    .locals 1

    .prologue
    .line 586
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->messageBody:Ljava/lang/String;

    return-object v0
.end method

.method public getOwnGameClip(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;
    .locals 2
    .param p1, "message"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    .prologue
    .line 401
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->getAttachment()Lcom/microsoft/xbox/service/model/entity/Entity;

    move-result-object v0

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->senderXuid:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getGameClipForOwnedActivityFeeedItemContent(Lcom/microsoft/xbox/service/model/entity/Entity;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IProfileShowcaseResult$GameClip;

    move-result-object v0

    return-object v0
.end method

.method public getSLSMessageList()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-eqz v1, :cond_0

    .line 122
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/MessageModel;->getConversationDetails(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$Conversation;

    move-result-object v0

    .line 123
    .local v0, "conversation":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$Conversation;
    if-eqz v0, :cond_0

    .line 124
    iget-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$Conversation;->messages:Ljava/util/ArrayList;

    .line 127
    .end local v0    # "conversation":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$Conversation;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSelectedSummary()Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    return-object v0
.end method

.method public getSenderGamerPicUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->gamerPicUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getSenderGamertag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderGamerTag:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSenderXuid(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Ljava/lang/String;
    .locals 1
    .param p1, "message"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    .prologue
    .line 405
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->senderXuid:Ljava/lang/String;

    return-object v0
.end method

.method public getSkypeMessageList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-eqz v1, :cond_0

    .line 132
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/MessageModel;->getSkypeConversationMessages(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    move-result-object v0

    .line 133
    .local v0, "conversation":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    if-eqz v0, :cond_0

    .line 134
    iget-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;->messages:Ljava/util/List;

    .line 137
    .end local v0    # "conversation":Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSkypeMessageSentTimeString(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)Ljava/lang/String;
    .locals 9
    .param p1, "message"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 285
    if-eqz p1, :cond_0

    iget-object v3, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    if-nez v3, :cond_1

    .line 286
    :cond_0
    const-string v3, ""

    .line 304
    :goto_0
    return-object v3

    .line 287
    :cond_1
    iget-object v3, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 288
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070640

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 289
    .local v1, "format":Ljava/lang/String;
    invoke-static {}, Ljava/text/DateFormat;->getTimeInstance()Ljava/text/DateFormat;

    move-result-object v3

    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 290
    .local v2, "timeValue":Ljava/lang/String;
    new-array v3, v7, [Ljava/lang/Object;

    aput-object v2, v3, v6

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 291
    .end local v1    # "format":Ljava/lang/String;
    .end local v2    # "timeValue":Ljava/lang/String;
    :cond_2
    iget-object v3, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isDateWithinYesterday(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 292
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070642

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 293
    .restart local v1    # "format":Ljava/lang/String;
    invoke-static {}, Ljava/text/DateFormat;->getTimeInstance()Ljava/text/DateFormat;

    move-result-object v3

    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 294
    .restart local v2    # "timeValue":Ljava/lang/String;
    new-array v3, v7, [Ljava/lang/Object;

    aput-object v2, v3, v6

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 295
    .end local v1    # "format":Ljava/lang/String;
    .end local v2    # "timeValue":Ljava/lang/String;
    :cond_3
    iget-object v3, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isDateWithinLastWeek(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 296
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070641

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 297
    .restart local v1    # "format":Ljava/lang/String;
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "EEEE"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 298
    .local v0, "dayValue":Ljava/lang/String;
    invoke-static {}, Ljava/text/DateFormat;->getTimeInstance()Ljava/text/DateFormat;

    move-result-object v3

    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 299
    .restart local v2    # "timeValue":Ljava/lang/String;
    new-array v3, v8, [Ljava/lang/Object;

    aput-object v0, v3, v6

    aput-object v2, v3, v7

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 301
    .end local v0    # "dayValue":Ljava/lang/String;
    .end local v1    # "format":Ljava/lang/String;
    .end local v2    # "timeValue":Ljava/lang/String;
    :cond_4
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f07063f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 302
    .restart local v1    # "format":Ljava/lang/String;
    invoke-static {}, Ljava/text/DateFormat;->getDateInstance()Ljava/text/DateFormat;

    move-result-object v3

    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 303
    .restart local v0    # "dayValue":Ljava/lang/String;
    invoke-static {}, Ljava/text/DateFormat;->getTimeInstance()Ljava/text/DateFormat;

    move-result-object v3

    iget-object v4, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->originalarrivaltime:Ljava/util/Date;

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 304
    .restart local v2    # "timeValue":Ljava/lang/String;
    new-array v3, v8, [Ljava/lang/Object;

    aput-object v0, v3, v6

    aput-object v2, v3, v7

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0
.end method

.method public hasAttachment(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Z
    .locals 1
    .param p1, "message"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    .prologue
    .line 359
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->hasAttachment()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasAttachmentLoadingError(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Z
    .locals 2
    .param p1, "message"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    .prologue
    .line 388
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->activityfeedItemLocator:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->attachmentErrorResId:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->activityfeedItemLocator:Ljava/lang/String;

    .line 389
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 388
    :goto_0
    return v0

    .line 389
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSkypeAttachment(Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;)Z
    .locals 2
    .param p1, "message"    # Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;

    .prologue
    .line 363
    if-eqz p1, :cond_0

    sget-object v0, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Activity:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->getType()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessage;->messagetype:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasXbox360Attachment(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Z
    .locals 1
    .param p1, "message"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    .prologue
    .line 367
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->hasXbox360Attachment()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLoadingAttachment()Z
    .locals 4

    .prologue
    .line 378
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->isLoadingAttachment:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    .line 379
    .local v1, "statusList":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Boolean;>;"
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 380
    .local v0, "status":Ljava/lang/Boolean;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 381
    const/4 v2, 0x1

    .line 384
    .end local v0    # "status":Ljava/lang/Boolean;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isLoadingAttachment(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Z
    .locals 2
    .param p1, "message"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    .prologue
    .line 371
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->activityfeedItemLocator:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->isLoadingAttachment:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->activityfeedItemLocator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->isLoadingAttachment:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->activityfeedItemLocator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 374
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSelectedSenderService()Z
    .locals 1

    .prologue
    .line 537
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->getSelectedSummary()Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->isSkypeSenderService(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Z

    move-result v0

    return v0
.end method

.method public isSkypeGroupConversation()Z
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->isGroupConversation:Z

    return v0
.end method

.method public isSkypeSenderService(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Z
    .locals 1
    .param p1, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .prologue
    .line 533
    if-eqz p1, :cond_0

    iget-boolean v0, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isServiceMessage:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSkypeServiceConversation()Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->isServiceMessage:Z

    return v0
.end method

.method public isUserOnline(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;)Z
    .locals 2
    .param p1, "summary"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .prologue
    .line 117
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->status:Lcom/microsoft/xbox/service/model/UserStatus;

    sget-object v1, Lcom/microsoft/xbox/service/model/UserStatus;->Online:Lcom/microsoft/xbox/service/model/UserStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadAttachment(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)V
    .locals 3
    .param p1, "message"    # Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    .prologue
    .line 97
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->getEntityModel(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Lcom/microsoft/xbox/service/model/entity/EntityModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->getEntityModel(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Lcom/microsoft/xbox/service/model/entity/EntityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->hasNoContent()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->getEntityModel(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Lcom/microsoft/xbox/service/model/entity/EntityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->loadFailed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 98
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->getEntityModel(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Lcom/microsoft/xbox/service/model/entity/EntityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->getEntity()Lcom/microsoft/xbox/service/model/entity/Entity;

    move-result-object v0

    if-nez v0, :cond_2

    .line 99
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->isLoadingAttachment(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->loadAttachmentTasks:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->activityfeedItemLocator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->loadAttachmentTasks:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->activityfeedItemLocator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;->cancel()V

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->loadAttachmentTasks:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->activityfeedItemLocator:Ljava/lang/String;

    new-instance v2, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;

    invoke-direct {v2, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->loadAttachmentTasks:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->activityfeedItemLocator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$LoadAttachmentAsyncTask;->load(Z)V

    .line 110
    :cond_1
    :goto_0
    return-void

    .line 107
    :cond_2
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->getEntityModel(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;)Lcom/microsoft/xbox/service/model/entity/EntityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->getEntity()Lcom/microsoft/xbox/service/model/entity/Entity;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->setAttachment(Lcom/microsoft/xbox/service/model/entity/Entity;)V

    goto :goto_0
.end method

.method public navigateToSpecificFriendProfile(Ljava/lang/String;)V
    .locals 3
    .param p1, "xuid"    # Ljava/lang/String;

    .prologue
    .line 269
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    if-eqz v1, :cond_1

    .line 270
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->lastMessage:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;

    iget-object v0, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationMessage;->messageType:Ljava/lang/String;

    .line 271
    .local v0, "messageType":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/service/model/serialization/XBLSLSMessageType;->User:Lcom/microsoft/xbox/service/model/serialization/XBLSLSMessageType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/serialization/XBLSLSMessageType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/service/model/serialization/XBLSLSMessageType;->ActivityFeedSharedItem:Lcom/microsoft/xbox/service/model/serialization/XBLSLSMessageType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/serialization/XBLSLSMessageType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 272
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-static {v1, p1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCMessaging;->trackViewSpecificProfile(Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;Ljava/lang/String;)V

    .line 273
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->navigateToFriendProfile(Ljava/lang/String;)V

    .line 278
    .end local v0    # "messageType":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 275
    .restart local v0    # "messageType":Ljava/lang/String;
    :cond_2
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->TAG:Ljava/lang/String;

    const-string v2, "Cannot navigate to system or service user profile"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onLoadSkypeConversationMessagesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Ljava/lang/String;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "targetXuid"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 409
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->TAG:Ljava/lang/String;

    const-string v2, "onLoadSkypeConversationMessagesCompleted"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->isLoadingConversationDetail:Z

    .line 412
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 436
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->updateAdapter()V

    .line 437
    return-void

    .line 417
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 418
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/MessageModel;->getSkypeConversationMessages(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->skypeConversationMessages:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    .line 419
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/MessageModel;->getConversationSummaryById(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    move-result-object v0

    .line 420
    .local v0, "skypeConversationDetails":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummarySynthesized:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 421
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->TAG:Ljava/lang/String;

    const-string v2, "Initializing conversation summary"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 423
    iput-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummarySynthesized:Z

    .line 427
    .end local v0    # "skypeConversationDetails":Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->skypeConversationMessages:Lcom/microsoft/xbox/service/model/serialization/SkypeConversationsSummaryContainer$SkypeConversationMessageListResult;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 431
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v1, v2, :cond_0

    .line 432
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 412
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public sendSkypeReply()V
    .locals 8

    .prologue
    .line 590
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 593
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToCommunicateVoiceAndText()Z

    move-result v4

    if-eqz v0, :cond_1

    .line 594
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getCanCommunicateWithTextAndVoice()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    :goto_0
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v6, 0x7f0704a9

    .line 595
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f07061c

    .line 596
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 592
    invoke-static {v4, v3, v5, v6}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showFailedPermissionsDialog(ZZLjava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 598
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "start sending skype message"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-boolean v3, v3, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->isServiceMessage:Z

    if-nez v3, :cond_4

    .line 601
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    .line 602
    .local v1, "model":Lcom/microsoft/xbox/service/model/ProfileModel;
    new-instance v2, Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;

    sget-object v3, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->Text:Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/serialization/SkypeMessageType;->name()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->messageBody:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerTag()Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-direct {v2, v4, v5, v3}, Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    .local v2, "request":Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;
    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->isGroupConversation:Z

    if-eqz v3, :cond_3

    .line 604
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, v2}, Lcom/microsoft/xbox/service/model/MessageModel;->sendSkypeGroupMessage(Ljava/lang/String;Ljava/util/List;Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;)V

    .line 609
    :goto_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->updateAdapter()V

    .line 618
    .end local v1    # "model":Lcom/microsoft/xbox/service/model/ProfileModel;
    .end local v2    # "request":Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;
    :cond_0
    :goto_3
    return-void

    .line 594
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 602
    .restart local v1    # "model":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_2
    const-string v3, ""

    goto :goto_1

    .line 606
    .restart local v2    # "request":Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->messageModel:Lcom/microsoft/xbox/service/model/MessageModel;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iget-object v4, v4, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, Lcom/microsoft/xbox/service/model/MessageModel;->sendSkypeMessage(Ljava/lang/String;Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;)V

    goto :goto_2

    .line 611
    .end local v1    # "model":Lcom/microsoft/xbox/service/model/ProfileModel;
    .end local v2    # "request":Lcom/microsoft/xbox/service/model/sls/SendSkypeMessageRequest;
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    if-nez v3, :cond_5

    .line 612
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->TAG:Ljava/lang/String;

    const-string v4, "Conversation summary is null!"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 614
    :cond_5
    sget-object v3, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->TAG:Ljava/lang/String;

    const-string v4, "Cannot reply to system or service messages"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public setMessageBody(Ljava/lang/String;)V
    .locals 0
    .param p1, "body"    # Ljava/lang/String;

    .prologue
    .line 582
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->messageBody:Ljava/lang/String;

    .line 583
    return-void
.end method

.method public setShouldScrollToBottom()V
    .locals 1

    .prologue
    .line 521
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->shouldScrollToBottom:Z

    .line 522
    return-void
.end method

.method public shouldScrollToBottom()Z
    .locals 1

    .prologue
    .line 525
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->shouldScrollToBottom:Z

    return v0
.end method

.method public showKeyboard()V
    .locals 3

    .prologue
    .line 621
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    .line 622
    .local v0, "activity":Landroid/app/Activity;
    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->isActivityAlive(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 623
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 625
    :cond_0
    return-void
.end method

.method protected synthesizeConversationSummary(Ljava/lang/String;)V
    .locals 2
    .param p1, "senderXuid"    # Ljava/lang/String;

    .prologue
    .line 571
    new-instance v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    .line 572
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    iput-object p1, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderXuid:Ljava/lang/String;

    .line 573
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummary:Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;

    const-string v1, ""

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/SLSConversationsSummaryContainer$ConversationSummary;->senderGamerTag:Ljava/lang/String;

    .line 574
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->conversationSummarySynthesized:Z

    .line 575
    return-void
.end method

.method protected updatePresenceDataForConversations(Z)V
    .locals 4
    .param p1, "noDelay"    # Z

    .prologue
    .line 256
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->presenceUpdateHandler:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 258
    sget-object v0, Lcom/microsoft/xbox/toolkit/ThreadManager;->Handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->presenceUpdateHandler:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 260
    if-eqz p1, :cond_1

    .line 261
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->presenceUpdateHandler:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 266
    :cond_0
    :goto_0
    return-void

    .line 263
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelWithConversation;->presenceUpdateHandler:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3a98

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPostDelayed(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method
