.class final Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse;
.super Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;
.source "AutoValue_OOBESessionDataType_OOBESessionResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/data/service/oobe/AutoValue_OOBESessionDataType_OOBESessionResponse$GsonTypeAdapter;
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;Ljava/lang/String;)V
    .locals 0
    .param p1, "sessionId"    # Ljava/lang/String;
    .param p2, "consoleId"    # Ljava/lang/String;
    .param p3, "sessionState"    # I
    .param p4, "sessionExpiration"    # Ljava/util/Date;
    .param p5, "encryptionKey"    # Ljava/lang/String;
    .param p6, "transferToken"    # Ljava/lang/String;
    .param p7, "consoleUpdateProgress"    # Ljava/lang/Integer;
    .param p8, "consoleUpdateStep"    # Ljava/lang/Integer;
    .param p9, "consoleOSVersion"    # Ljava/lang/String;
    .param p10, "consoleSettings"    # Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;
    .param p11, "userName"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct/range {p0 .. p11}, Lcom/microsoft/xbox/data/service/oobe/$AutoValue_OOBESessionDataType_OOBESessionResponse;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Lcom/microsoft/xbox/data/service/oobe/OOBESessionDataType$ConsoleSettings;Ljava/lang/String;)V

    .line 20
    return-void
.end method
