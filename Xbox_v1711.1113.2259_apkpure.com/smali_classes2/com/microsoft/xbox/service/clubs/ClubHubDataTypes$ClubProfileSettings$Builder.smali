.class public abstract Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;
.super Ljava/lang/Object;
.source "ClubHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Builder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 933
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract build()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings;
.end method

.method public abstract delete(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;"
        }
    .end annotation
.end method

.method public abstract update(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;"
        }
    .end annotation
.end method

.method public abstract view(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;",
            ">;)",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubProfileSettings$Builder;"
        }
    .end annotation
.end method
