.class Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabIndicator;
.super Ljava/lang/Object;
.source "ConsoleBarView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/ui/ConsoleBarView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TabIndicator"
.end annotation


# instance fields
.field public container:Landroid/view/View;

.field public tabIcon:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

.field public tabTitle:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;Ljava/lang/String;)V
    .locals 4
    .param p2, "tab"    # Lcom/microsoft/xbox/xle/ui/ConsoleBarView$Tab;
    .param p3, "location"    # Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;
    .param p4, "title"    # Ljava/lang/String;

    .prologue
    .line 666
    iput-object p1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabIndicator;->this$0:Lcom/microsoft/xbox/xle/ui/ConsoleBarView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 667
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 668
    .local v0, "vi":Landroid/view/LayoutInflater;
    const v1, 0x7f030224

    invoke-static {p1}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->access$600(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;)Landroid/widget/TabHost;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabIndicator;->container:Landroid/view/View;

    .line 669
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabIndicator;->container:Landroid/view/View;

    const v2, 0x7f0e0a9b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabIndicator;->tabIcon:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 670
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabIndicator;->tabIcon:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {p1, p3}, Lcom/microsoft/xbox/xle/ui/ConsoleBarView;->access$700(Lcom/microsoft/xbox/xle/ui/ConsoleBarView;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 671
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabIndicator;->container:Landroid/view/View;

    const v2, 0x7f0e0a9c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabIndicator;->tabTitle:Landroid/widget/TextView;

    .line 672
    iget-object v1, p0, Lcom/microsoft/xbox/xle/ui/ConsoleBarView$TabIndicator;->tabTitle:Landroid/widget/TextView;

    invoke-virtual {v1, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 673
    return-void
.end method
