.class Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "PeopleHubInfoScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadUserProfileAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;)V
    .locals 0

    .prologue
    .line 833
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$1;

    .prologue
    .line 833
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 836
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 837
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefresh()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 838
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefreshProfileSummary()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 839
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->shouldRefreshPeopleHubSummary()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 837
    :goto_0
    return v0

    .line 839
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 871
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    iget-object v2, v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 873
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    iget-object v2, v2, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;->forceLoad:Z

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    .line 874
    .local v1, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    new-array v2, v6, [Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    iget-object v3, v3, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;->forceLoad:Z

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadProfileSummary(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->merge(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;[Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    .line 875
    new-array v2, v6, [Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    iget-object v3, v3, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->model:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-boolean v4, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;->forceLoad:Z

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadPeopleHubPersonData(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->merge(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;[Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    .line 877
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 878
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_0

    .line 880
    invoke-virtual {v0, v5}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadClubs(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 883
    :cond_0
    return-object v1
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 833
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 866
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 833
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 844
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 845
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->onLoadUserProfileCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 846
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 861
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->onLoadUserProfileCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 862
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 833
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 850
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 851
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->access$302(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;Z)Z

    .line 856
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel$LoadUserProfileAsyncTask;->this$0:Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;->access$400(Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubInfoScreenViewModel;)V

    .line 857
    return-void
.end method
