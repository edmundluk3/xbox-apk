.class public final Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;
.super Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;
.source "ClubAdminReportsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "FeedViewHolder"
.end annotation


# instance fields
.field private final root:Landroid/widget/FrameLayout;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;Landroid/view/View;)V
    .locals 1
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;
    .param p2, "itemView"    # Landroid/view/View;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongViewCast"
        }
    .end annotation

    .prologue
    .line 258
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    .line 259
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;Landroid/view/View;)V

    .line 260
    const v0, 0x7f0e027d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;->root:Landroid/widget/FrameLayout;

    .line 261
    return-void
.end method

.method static synthetic lambda$null$1(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;
    .param p1, "feedListItem"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;

    .prologue
    .line 289
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->access$200(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;->deleteItem(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V

    return-void
.end method

.method static synthetic lambda$onBind$0(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;
    .param p1, "feedListItem"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 287
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;->access$200(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$OnClubReportActionListener;->navigateToItem(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;)V

    return-void
.end method

.method static synthetic lambda$onBind$2(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;
    .param p1, "feedListItem"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 289
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;->removeDecorator(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method protected getInnerViewId()I
    .locals 1
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .prologue
    .line 266
    const v0, 0x7f03005e

    return v0
.end method

.method public onBind(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V
    .locals 5
    .param p1, "item"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 271
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$BaseViewHolder;->onBind(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V

    .line 273
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;->root:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->removeAllViews()V

    move-object v1, p1

    .line 275
    check-cast v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;

    .line 277
    .local v1, "feedListItem":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;->hasValidContent()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 278
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;->hideErrorView()V

    .line 280
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;->getProfileRecentItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->getItemViewType(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;

    move-result-object v2

    .line 282
    .local v2, "viewType":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;->root:Landroid/widget/FrameLayout;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;->root:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    invoke-static {v3, v2, v4}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->createView(Landroid/view/ViewGroup;Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    .line 284
    .local v0, "feedItemView":Landroid/view/View;
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;->getProfileRecentItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil;->bindView(Landroid/view/View;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    .line 286
    const v3, 0x7f0e0191

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 287
    invoke-static {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 289
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;->delete:Landroid/view/View;

    invoke-static {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportFeedListItem;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 290
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;->root:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 294
    .end local v0    # "feedItemView":Landroid/view/View;
    .end local v2    # "viewType":Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewType;
    :goto_0
    return-void

    .line 292
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;->showErrorView()V

    goto :goto_0
.end method

.method public bridge synthetic onBind(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 253
    check-cast p1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportsListAdapter$FeedViewHolder;->onBind(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminReportListItems$ClubReportBaseListItem;)V

    return-void
.end method
