.class public final Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$TransferOwnershipRequest;
.super Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$BaseClubChangeRequest;
.source "ClubAccountsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TransferOwnershipRequest"
.end annotation


# instance fields
.field private final user:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "xuid"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 284
    sget-object v0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;->TransferOwnership:Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$BaseClubChangeRequest;-><init>(Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubChangeRequestMethod;)V

    .line 285
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 286
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$TransferOwnershipRequest;->user:Ljava/lang/String;

    .line 287
    return-void
.end method
