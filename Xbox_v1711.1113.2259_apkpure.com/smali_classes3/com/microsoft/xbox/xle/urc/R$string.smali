.class public final Lcom/microsoft/xbox/xle/urc/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/urc/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Streaming_Remote_Cannot_Control_Source:I = 0x7f070c82

.field public static final URC_ConnectionError_Remote:I = 0x7f070e2a

.field public static final URC_Error_Console_Setup_Directions_STB:I = 0x7f070e2b

.field public static final URC_Error_Remote_Console_Setup:I = 0x7f070e2c

.field public static final URC_Error_TvListings_Settings:I = 0x7f070e2d

.field public static final app_name:I = 0x7f070e3d

.field public static final btn_0:I = 0x7f070e40

.field public static final btn_1:I = 0x7f070e41

.field public static final btn_2:I = 0x7f070e42

.field public static final btn_3:I = 0x7f070e43

.field public static final btn_4:I = 0x7f070e44

.field public static final btn_5:I = 0x7f070e45

.field public static final btn_6:I = 0x7f070e46

.field public static final btn_7:I = 0x7f070e47

.field public static final btn_8:I = 0x7f070e48

.field public static final btn_9:I = 0x7f070e49

.field public static final btn_ch:I = 0x7f070e4a

.field public static final btn_ch_enter:I = 0x7f070e4b

.field public static final btn_delimiter:I = 0x7f070e4c

.field public static final btn_dvr:I = 0x7f070e4d

.field public static final btn_exit:I = 0x7f070e4e

.field public static final btn_guide:I = 0x7f070e4f

.field public static final btn_info:I = 0x7f070e50

.field public static final btn_live:I = 0x7f070e51

.field public static final btn_menu:I = 0x7f070e52

.field public static final btn_page:I = 0x7f070e53

.field public static final btn_select:I = 0x7f070e54

.field public static final btn_vol:I = 0x7f070e55

.field public static final more_actions:I = 0x7f071067

.field public static final narrator_btn_arrow_down:I = 0x7f071068

.field public static final narrator_btn_arrow_left:I = 0x7f071069

.field public static final narrator_btn_arrow_right:I = 0x7f07106a

.field public static final narrator_btn_arrow_up:I = 0x7f07106b

.field public static final narrator_btn_back:I = 0x7f07106c

.field public static final narrator_btn_blue:I = 0x7f07106d

.field public static final narrator_btn_channel_down:I = 0x7f07106e

.field public static final narrator_btn_channel_up:I = 0x7f07106f

.field public static final narrator_btn_fast_forward:I = 0x7f071070

.field public static final narrator_btn_green:I = 0x7f071071

.field public static final narrator_btn_input:I = 0x7f071072

.field public static final narrator_btn_more:I = 0x7f071073

.field public static final narrator_btn_mute:I = 0x7f071074

.field public static final narrator_btn_page_down:I = 0x7f071075

.field public static final narrator_btn_page_up:I = 0x7f071076

.field public static final narrator_btn_pause:I = 0x7f071077

.field public static final narrator_btn_play:I = 0x7f071078

.field public static final narrator_btn_power:I = 0x7f071079

.field public static final narrator_btn_previous_channel:I = 0x7f07107a

.field public static final narrator_btn_record:I = 0x7f07107b

.field public static final narrator_btn_red:I = 0x7f07107c

.field public static final narrator_btn_rewind:I = 0x7f07107d

.field public static final narrator_btn_skip_forward:I = 0x7f07107e

.field public static final narrator_btn_stop:I = 0x7f07107f

.field public static final narrator_btn_volume_down:I = 0x7f071080

.field public static final narrator_btn_volume_up:I = 0x7f071081

.field public static final narrator_btn_yellow:I = 0x7f071082

.field public static final power_off:I = 0x7f071087

.field public static final power_on:I = 0x7f071088

.field public static final power_on_off:I = 0x7f071089

.field public static final power_stb:I = 0x7f07108a

.field public static final power_system:I = 0x7f07108b

.field public static final power_tv:I = 0x7f07108c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
