.class public Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCTrending;
.super Ljava/lang/Object;
.source "UTCTrending.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static trackTrendingFilterAction(Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;)V
    .locals 3
    .param p0, "filter"    # Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;

    .prologue
    .line 15
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 16
    .local v0, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v1, "Filter"

    invoke-interface {p0}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;->getTelemetryName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 18
    const-string v1, "Trending - Trending List Filter"

    invoke-static {v1, v0}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 19
    return-void
.end method
