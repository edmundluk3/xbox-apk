.class public Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;
.super Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;
.source "ClubAdminBannedScreenAdapter.java"


# instance fields
.field private final bannedList:Landroid/support/v7/widget/RecyclerView;

.field private final countAppearance:Landroid/text/style/TextAppearanceSpan;

.field private final listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;

.field private final title:Landroid/widget/TextView;

.field private final viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;)V
    .locals 6
    .param p1, "clubViewModel"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;)V

    .line 42
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 43
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;

    .line 45
    const v3, 0x7f0e0263

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->title:Landroid/widget/TextView;

    .line 46
    new-instance v3, Landroid/text/style/TextAppearanceSpan;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->title:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f08021e

    invoke-direct {v3, v4, v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->countAppearance:Landroid/text/style/TextAppearanceSpan;

    .line 48
    const v3, 0x7f0e06bd

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 49
    .local v1, "noContent":Landroid/widget/TextView;
    const v3, 0x7f070b62

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 51
    const v3, 0x7f0e06bc

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 52
    .local v2, "noContentIcon":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 53
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070ecb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    new-instance v3, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;

    new-instance v4, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter$1;

    invoke-direct {v4, p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;)V

    invoke-direct {v3, v4}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter$BannedItemClickListener;)V

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;

    .line 67
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 69
    .local v0, "layoutManager":Landroid/support/v7/widget/LinearLayoutManager;
    const v3, 0x7f0e0264

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/RecyclerView;

    iput-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->bannedList:Landroid/support/v7/widget/RecyclerView;

    .line 70
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->bannedList:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 71
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->bannedList:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;

    return-object v0
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->scrollExceedsLoadMoreThreshold(Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic lambda$onStart$0(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;)Z
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;
    .param p1, "e"    # Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->isBusy()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->hasMoreItemsToLoad()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic lambda$onStart$1(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;
    .param p1, "e"    # Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->loadMoreItems()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method private scrollExceedsLoadMoreThreshold(Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;)Z
    .locals 6
    .param p1, "recyclerViewScrollEvent"    # Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;

    .prologue
    .line 102
    invoke-virtual {p1}, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;->view()Landroid/support/v7/widget/RecyclerView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/LinearLayoutManager;

    .line 103
    .local v0, "layoutManager":Landroid/support/v7/widget/LinearLayoutManager;
    invoke-virtual {p1}, Lcom/jakewharton/rxbinding2/support/v7/widget/RecyclerViewScrollEvent;->view()Landroid/support/v7/widget/RecyclerView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v1

    .line 104
    .local v1, "realizedItemCount":I
    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    move-result v4

    add-int/2addr v4, v1

    int-to-float v2, v4

    .line 105
    .local v2, "realizedWindowPos":F
    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager;->getItemCount()I

    move-result v3

    .line 107
    .local v3, "totalCount":I
    if-eqz v3, :cond_0

    int-to-float v4, v3

    div-float v4, v2, v4

    const/high16 v5, 0x3f400000    # 0.75f

    cmpl-float v4, v4, v5

    if-ltz v4, :cond_1

    :cond_0
    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private setTitle()V
    .locals 7

    .prologue
    .line 121
    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f070167

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 122
    .local v1, "titleBase":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->getNumOfBannedUsers()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 124
    .local v2, "titleCount":Ljava/lang/String;
    new-instance v0, Landroid/text/SpannableString;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 125
    .local v0, "styledTitle":Landroid/text/SpannableString;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->countAppearance:Landroid/text/style/TextAppearanceSpan;

    .line 127
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    .line 128
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    const/16 v6, 0x21

    .line 125
    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 131
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->title:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    return-void
.end method

.method private updateMissingListItems()V
    .locals 4

    .prologue
    .line 135
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->getLoadedBannedModels()Ljava/util/List;

    move-result-object v0

    .line 136
    .local v0, "allItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;->getDataCopy()Ljava/util/List;

    move-result-object v2

    .line 138
    .local v2, "oldItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel$ClubBannedListItem;>;"
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;->clear()V

    .line 139
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;->addAll(Ljava/util/Collection;)V

    .line 141
    new-instance v3, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;

    invoke-direct {v3, v2, v0}, Lcom/microsoft/xbox/toolkit/generics/ListOfImmutablesDiffCallback;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-static {v3}, Landroid/support/v7/util/DiffUtil;->calculateDiff(Landroid/support/v7/util/DiffUtil$Callback;)Landroid/support/v7/util/DiffUtil$DiffResult;

    move-result-object v1

    .line 142
    .local v1, "diffResult":Landroid/support/v7/util/DiffUtil$DiffResult;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubBannedListAdapter;

    invoke-virtual {v1, v3}, Landroid/support/v7/util/DiffUtil$DiffResult;->dispatchUpdatesTo(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 143
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 3

    .prologue
    .line 76
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->onStart()V

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->rxDisposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->bannedList:Landroid/support/v7/widget/RecyclerView;

    .line 79
    invoke-static {v1}, Lcom/jakewharton/rxbinding2/support/v7/widget/RxRecyclerView;->scrollEvents(Landroid/support/v7/widget/RecyclerView;)Lio/reactivex/Observable;

    move-result-object v1

    .line 80
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;)Lio/reactivex/functions/Predicate;

    move-result-object v2

    .line 83
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;)Lio/reactivex/functions/Predicate;

    move-result-object v2

    .line 86
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v1

    .line 89
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;)Lio/reactivex/functions/Function;

    move-result-object v2

    .line 93
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 97
    invoke-virtual {v1}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 78
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 99
    return-void
.end method

.method protected updateViewOverride()V
    .locals 2

    .prologue
    .line 112
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBaseScreenAdapter;->updateViewOverride()V

    .line 114
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->isBusy()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_0

    .line 115
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->setTitle()V

    .line 116
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminBannedScreenAdapter;->updateMissingListItems()V

    .line 118
    :cond_0
    return-void
.end method
