.class Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$JsonCommand;
.super Ljava/lang/Object;
.source "JavaScriptBridge.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "JsonCommand"
.end annotation


# instance fields
.field public final args:Ljava/lang/String;

.field public final id:I

.field final synthetic this$0:Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;Ljava/lang/String;)V
    .locals 2
    .param p2, "json"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 159
    iput-object p1, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$JsonCommand;->this$0:Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 162
    .local v0, "jsonObject":Lorg/json/JSONObject;
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$JsonCommand;->id:I

    .line 166
    const-string v1, "args"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 167
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$JsonCommand;->args:Ljava/lang/String;

    .line 171
    :goto_0
    return-void

    .line 169
    :cond_0
    const-string v1, "args"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/JavaScriptBridge$JsonCommand;->args:Ljava/lang/String;

    goto :goto_0
.end method
