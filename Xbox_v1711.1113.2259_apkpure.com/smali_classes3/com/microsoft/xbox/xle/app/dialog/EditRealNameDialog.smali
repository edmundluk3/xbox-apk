.class public Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "EditRealNameDialog.java"


# instance fields
.field private cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private contentStatusSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private edit_first_name:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

.field private edit_last_name:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

.field private saveButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private vm:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "vm"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    .prologue
    .line 32
    const v0, 0x7f080238

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 33
    const v0, 0x7f0300e7

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->setContentView(I)V

    .line 34
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->vm:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    .line 35
    const v0, 0x7f0e055a

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->contentStatusSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 36
    const v0, 0x7f0e055c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->edit_first_name:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .line 37
    const v0, 0x7f0e055d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->edit_last_name:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    .line 38
    const v0, 0x7f0e055f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->saveButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 39
    const v0, 0x7f0e055e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 40
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->hideSoftKeyBoard()V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->edit_first_name:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->edit_last_name:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;)Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->vm:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;)Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->contentStatusSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->dismissSelf()V

    return-void
.end method

.method private dismissSelf()V
    .locals 1

    .prologue
    .line 119
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissEditRealNameDialog()V

    .line 120
    return-void
.end method

.method private hideSoftKeyBoard()V
    .locals 4

    .prologue
    .line 141
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 142
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 144
    .local v0, "currentFocus":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->isAcceptingText()Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 145
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 147
    :cond_0
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 113
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing;->trackCancelEdit()V

    .line 115
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->dismissSelf()V

    .line 116
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->saveButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->saveButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog$1;-><init>(Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog$2;-><init>(Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->updateEditNameForm()V

    .line 73
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCRealNameSharing;->trackRealNameSharingEditNameView()V

    .line 74
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 107
    return-void
.end method

.method public reportAsyncTaskCompleted()V
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->updateEditNameForm()V

    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->vm:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getIsEditingFirstNameStatus()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->vm:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getIsEditingLastNameStatus()Z

    move-result v0

    if-nez v0, :cond_0

    .line 125
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->dismissSelf()V

    .line 127
    :cond_0
    return-void
.end method

.method public reportAsyncTaskFailed()V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->contentStatusSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->getState()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->contentStatusSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 132
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    const v1, 0x7f070b21

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(I)V

    .line 135
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->dismissSelf()V

    .line 136
    return-void
.end method

.method public setVm(Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;)V
    .locals 0
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->vm:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    .line 103
    return-void
.end method

.method public updateEditNameForm()V
    .locals 4

    .prologue
    .line 77
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->vm:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->isBusy()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->contentStatusSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 79
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog$3;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog$3;-><init>(Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;)V

    const-wide/16 v2, 0xc8

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPostDelayed(Ljava/lang/Runnable;J)V

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->contentStatusSwitchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 92
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->edit_last_name:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    if-eqz v0, :cond_2

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->edit_last_name:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->vm:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getLastName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setText(Ljava/lang/CharSequence;)V

    .line 96
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->edit_first_name:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->edit_first_name:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/EditRealNameDialog;->vm:Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/settings/SettingsGeneralPageViewModel;->getFirstName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
