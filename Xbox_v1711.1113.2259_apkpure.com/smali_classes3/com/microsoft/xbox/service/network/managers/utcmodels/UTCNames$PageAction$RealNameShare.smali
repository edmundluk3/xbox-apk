.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$RealNameShare;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RealNameShare"
.end annotation


# static fields
.field public static final CancelEditName:Ljava/lang/String; = "Real Name Share - Cancel Edit Name"

.field public static final CancelFriends:Ljava/lang/String; = "Real Name Share - Cancel Select Friends"

.field public static final EditFriends:Ljava/lang/String; = "Real Name Share - Edit Friends"

.field public static final EditName:Ljava/lang/String; = "Real Name Share - Edit Name"

.field public static final RealNameSharingOff:Ljava/lang/String; = "Real Name Share - Turn off Real Name Sharing"

.field public static final RealNameSharingOn:Ljava/lang/String; = "Real Name Share - Turn on Real Name Sharing"

.field public static final RealNameSharingUnknown:Ljava/lang/String; = "Real Name Share - Unknown"

.field public static final SaveFriends:Ljava/lang/String; = "Real Name Share - Save Friends"

.field public static final SaveName:Ljava/lang/String; = "Real Name Share - Save Name"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1050
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
