.class public Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder_ViewBinding;
.super Ljava/lang/Object;
.source "PeopleScreenListAdapter$DummyViewHolder_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;

    .line 24
    const v0, 0x7f0e08e8

    const-string v1, "field \'listStateText\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->listStateText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 25
    const v0, 0x7f0e08e7

    const-string v1, "field \'friendsHeader\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->friendsHeader:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 26
    const v0, 0x7f0e08db

    const-string v1, "field \'recommendationsHeader\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->recommendationsHeader:Landroid/view/View;

    .line 27
    const v0, 0x7f0e08de

    const-string v1, "field \'linkToFacebookItem\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->linkToFacebookItem:Landroid/view/View;

    .line 28
    const v0, 0x7f0e08e2

    const-string v1, "field \'linkToPhoneContactItem\'"

    invoke-static {p2, v0, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->linkToPhoneContactItem:Landroid/view/View;

    .line 29
    const v0, 0x7f0e08df

    const-string v1, "field \'facebookImageMD\'"

    const-class v2, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->facebookImageMD:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 30
    const v0, 0x7f0e08dd

    const-string v1, "field \'seeAllRecommendationsButton\'"

    const-class v2, Landroid/widget/RelativeLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p1, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->seeAllRecommendationsButton:Landroid/widget/RelativeLayout;

    .line 31
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;

    .line 37
    .local v0, "target":Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 38
    :cond_0
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder_ViewBinding;->target:Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;

    .line 40
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->listStateText:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 41
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->friendsHeader:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 42
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->recommendationsHeader:Landroid/view/View;

    .line 43
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->linkToFacebookItem:Landroid/view/View;

    .line 44
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->linkToPhoneContactItem:Landroid/view/View;

    .line 45
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->facebookImageMD:Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 46
    iput-object v1, v0, Lcom/microsoft/xbox/xle/app/adapter/PeopleScreenListAdapter$DummyViewHolder;->seeAllRecommendationsButton:Landroid/widget/RelativeLayout;

    .line 47
    return-void
.end method
