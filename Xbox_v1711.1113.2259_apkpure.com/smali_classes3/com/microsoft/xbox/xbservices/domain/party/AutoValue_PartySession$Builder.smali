.class final Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession$Builder;
.super Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;
.source "AutoValue_PartySession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Builder"
.end annotation


# instance fields
.field private multiplayerSession:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

.field private muted:Ljava/lang/Boolean;

.field private roster:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;-><init>()V

    .line 86
    return-void
.end method

.method private constructor <init>(Lcom/microsoft/xbox/xbservices/domain/party/PartySession;)V
    .locals 1
    .param p1, "source"    # Lcom/microsoft/xbox/xbservices/domain/party/PartySession;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;-><init>()V

    .line 88
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->isMuted()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession$Builder;->muted:Ljava/lang/Boolean;

    .line 89
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->getRoster()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession$Builder;->roster:Lcom/google/common/collect/ImmutableList;

    .line 90
    invoke-virtual {p1}, Lcom/microsoft/xbox/xbservices/domain/party/PartySession;->getMultiplayerSession()Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession$Builder;->multiplayerSession:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 91
    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xbservices/domain/party/PartySession;Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
    .param p2, "x1"    # Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession$1;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession$Builder;-><init>(Lcom/microsoft/xbox/xbservices/domain/party/PartySession;)V

    return-void
.end method


# virtual methods
.method public build()Lcom/microsoft/xbox/xbservices/domain/party/PartySession;
    .locals 6

    .prologue
    .line 115
    const-string v0, ""

    .line 116
    .local v0, "missing":Ljava/lang/String;
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession$Builder;->muted:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 117
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " muted"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 119
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession$Builder;->roster:Lcom/google/common/collect/ImmutableList;

    if-nez v1, :cond_1

    .line 120
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " roster"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 122
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession$Builder;->multiplayerSession:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    if-nez v1, :cond_2

    .line 123
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " multiplayerSession"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 125
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 126
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Missing required properties:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 128
    :cond_3
    new-instance v1, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession;

    iget-object v2, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession$Builder;->muted:Ljava/lang/Boolean;

    .line 129
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession$Builder;->roster:Lcom/google/common/collect/ImmutableList;

    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession$Builder;->multiplayerSession:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession;-><init>(ZLcom/google/common/collect/ImmutableList;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession$1;)V

    .line 128
    return-object v1
.end method

.method public setMultiplayerSession(Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;)Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;
    .locals 2
    .param p1, "multiplayerSession"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .prologue
    .line 107
    if-nez p1, :cond_0

    .line 108
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null multiplayerSession"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession$Builder;->multiplayerSession:Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$MultiplayerSession;

    .line 111
    return-object p0
.end method

.method public setMuted(Z)Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;
    .locals 1
    .param p1, "muted"    # Z

    .prologue
    .line 94
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession$Builder;->muted:Ljava/lang/Boolean;

    .line 95
    return-object p0
.end method

.method public setRoster(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;",
            ">;)",
            "Lcom/microsoft/xbox/xbservices/domain/party/PartySession$Builder;"
        }
    .end annotation

    .prologue
    .line 99
    .local p1, "roster":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;>;"
    if-nez p1, :cond_0

    .line 100
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null roster"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 102
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/domain/party/AutoValue_PartySession$Builder;->roster:Lcom/google/common/collect/ImmutableList;

    .line 103
    return-object p0
.end method
