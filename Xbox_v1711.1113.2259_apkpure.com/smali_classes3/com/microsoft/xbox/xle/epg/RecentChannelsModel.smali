.class public Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;
.super Ljava/lang/Object;
.source "RecentChannelsModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/urc/net/BranchSession$ITVRecentsListener;
.implements Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;
.implements Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$GetChannelsAsyncTask;,
        Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$IListener;
    }
.end annotation


# static fields
.field public static final ChannelTuneFormat:Ljava/lang/String; = "ms-xbox-livetv://?ChannelNum=%s&channelId=%s&providerId=%s"

.field public static final TAG:Ljava/lang/String; = "RecentChannelsModel"

.field private static _default:Lcom/microsoft/xbox/xle/epg/RecentChannelsModel; = null

.field private static final sMaxRecents:I = 0x32


# instance fields
.field private mChannels:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/epg/EPGChannel;",
            ">;"
        }
    .end annotation
.end field

.field private mError:Z

.field private mGetChannelsTask:Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$GetChannelsAsyncTask;

.field private mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$IListener;",
            ">;"
        }
    .end annotation
.end field

.field private mLoading:Z

.field private mRawData:Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mLoading:Z

    .line 43
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mError:Z

    .line 44
    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mGetChannelsTask:Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$GetChannelsAsyncTask;

    .line 45
    iput-object v1, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mRawData:Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mListeners:Ljava/util/ArrayList;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mChannels:Ljava/util/ArrayList;

    .line 52
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;ZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->onRequestError(ZZ)V

    return-void
.end method

.method public static getDefault()Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->_default:Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->_default:Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;

    .line 37
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->_default:Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;

    return-object v0
.end method

.method private notifyListenersData()V
    .locals 3

    .prologue
    .line 232
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$IListener;

    .line 234
    .local v0, "l":Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$IListener;
    invoke-interface {v0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$IListener;->onDataChanged()V

    goto :goto_0

    .line 236
    .end local v0    # "l":Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$IListener;
    :cond_0
    return-void
.end method

.method private notifyListenersState()V
    .locals 3

    .prologue
    .line 226
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$IListener;

    .line 227
    .local v0, "l":Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$IListener;
    invoke-interface {v0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$IListener;->onStateChanged()V

    goto :goto_0

    .line 229
    .end local v0    # "l":Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$IListener;
    :cond_0
    return-void
.end method

.method private onRequestError(ZZ)V
    .locals 2
    .param p1, "cleanContent"    # Z
    .param p2, "error"    # Z

    .prologue
    const/4 v1, 0x0

    .line 200
    iput-boolean p2, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mError:Z

    .line 201
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mLoading:Z

    .line 203
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mGetChannelsTask:Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$GetChannelsAsyncTask;

    .line 205
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mChannels:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mError:Z

    .line 209
    :cond_0
    if-eqz p1, :cond_1

    .line 210
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mChannels:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 213
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->notifyListenersState()V

    .line 214
    if-eqz p1, :cond_2

    .line 215
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->notifyListenersData()V

    .line 217
    :cond_2
    return-void
.end method

.method private requestChannels()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 183
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mGetChannelsTask:Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$GetChannelsAsyncTask;

    if-eqz v0, :cond_0

    .line 197
    :goto_0
    return-void

    .line 187
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mRawData:Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;

    .line 188
    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mLoading:Z

    .line 190
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getState()Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->CONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-eq v0, v1, :cond_1

    .line 191
    const/4 v0, 0x0

    invoke-direct {p0, v2, v0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->onRequestError(ZZ)V

    goto :goto_0

    .line 193
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$GetChannelsAsyncTask;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$GetChannelsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mGetChannelsTask:Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$GetChannelsAsyncTask;

    .line 194
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mGetChannelsTask:Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$GetChannelsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$GetChannelsAsyncTask;->execute()V

    .line 195
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->notifyListenersState()V

    goto :goto_0
.end method


# virtual methods
.method public getChannels()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/epg/EPGChannel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mChannels:Ljava/util/ArrayList;

    return-object v0
.end method

.method public isError()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mError:Z

    return v0
.end method

.method public isLoading()Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mLoading:Z

    return v0
.end method

.method public onActiveProviderChanged(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)V
    .locals 0
    .param p1, "newProvider"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .prologue
    .line 308
    return-void
.end method

.method public onConfigChanged([Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;)V
    .locals 0
    .param p1, "devices"    # [Lcom/microsoft/xbox/xle/urc/net/DeviceInfo;

    .prologue
    .line 288
    return-void
.end method

.method public onConnectionStateChanged(Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;Ljava/lang/String;)V
    .locals 1
    .param p1, "state"    # Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;
    .param p2, "connectionError"    # Ljava/lang/String;

    .prologue
    .line 280
    sget-object v0, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->DISCONNECTED:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;->ERROR:Lcom/microsoft/xbox/xle/urc/net/IBranchConnection$ConnectionState;

    if-ne p1, v0, :cond_1

    .line 281
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->stopLoading()V

    .line 283
    :cond_1
    return-void
.end method

.method public onDataChanged(Lcom/microsoft/xbox/service/model/epg/EPGProvider;II)V
    .locals 0
    .param p1, "sourceProvider"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    .param p2, "startChannel"    # I
    .param p3, "endChannel"    # I

    .prologue
    .line 325
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->notifyListenersData()V

    .line 326
    return-void
.end method

.method public onFavoritesError(Lcom/microsoft/xbox/service/model/epg/EPGProvider;Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;)V
    .locals 2
    .param p1, "sourceProvider"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    .param p2, "result"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    .prologue
    .line 331
    const-string v0, "RecentChannelsModel"

    const-string v1, "Received the onFavoritesError event on the recent channels screen."

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    return-void
.end method

.method public onFetchingStatusChanged(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)V
    .locals 0
    .param p1, "sourceProvider"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .prologue
    .line 319
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->processChannelData()V

    .line 320
    return-void
.end method

.method public onHeadendChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 0
    .param p1, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 292
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->stopLoading()V

    .line 293
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->refresh()V

    .line 294
    return-void
.end method

.method public onHeadendSettingChanged(Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;)V
    .locals 0
    .param p1, "headend"    # Lcom/microsoft/xbox/xle/urc/net/HeadendInfo;

    .prologue
    .line 299
    return-void
.end method

.method public onProviderListChanged()V
    .locals 0

    .prologue
    .line 312
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->refresh()V

    .line 313
    return-void
.end method

.method public onTVRecentsReceived(Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;)V
    .locals 4
    .param p1, "data"    # Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mRawData:Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;

    .line 111
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;->getChannelCount()I

    move-result v0

    .line 112
    .local v0, "dataChannelCount":I
    const-string v1, "RecentChannelsModel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onTVRecentsReceived: Channels received:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->processChannelData()V

    .line 116
    return-void
.end method

.method public processChannelData()V
    .locals 13

    .prologue
    .line 121
    iget-object v10, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mRawData:Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;

    if-nez v10, :cond_0

    .line 180
    :goto_0
    return-void

    .line 125
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProviders()Ljava/util/HashMap;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v9

    .line 128
    .local v9, "providers":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/service/model/epg/EPGProvider;>;"
    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .line 129
    .local v8, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getIteratorFactory()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->isFetching()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 133
    const-string v10, "RecentChannelsModel"

    const-string v11, "processChannelData: EPG still fetching provider channels"

    invoke-static {v10, v11}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 139
    .end local v8    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    :cond_2
    iget-object v10, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mRawData:Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;->getChannelInfoList()Ljava/util/List;

    move-result-object v7

    .line 140
    .local v7, "list":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;>;"
    iget-object v10, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mRawData:Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;->getChannelCount()I

    move-result v1

    .line 141
    .local v1, "dataChannelCount":I
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 144
    .local v4, "foundChannelsHash":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    iget-object v10, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mChannels:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lez v10, :cond_3

    .line 145
    iget-object v10, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mChannels:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 148
    :cond_3
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    if-ge v5, v1, :cond_7

    iget-object v10, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mChannels:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    const/16 v11, 0x32

    if-ge v10, v11, :cond_7

    .line 149
    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;

    .line 151
    .local v2, "dataChannelInfo":Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;
    iget-object v10, v2, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;->providerId:Ljava/lang/String;

    invoke-static {v10}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProvider(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v8

    .line 153
    .restart local v8    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    if-nez v8, :cond_5

    .line 148
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 157
    :cond_5
    invoke-virtual {v8}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getIteratorFactory()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->getChannels()[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    move-result-object v0

    .line 159
    .local v0, "channelInfo":[Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    if-eqz v0, :cond_4

    .line 163
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_2
    array-length v10, v0

    if-ge v6, v10, :cond_4

    .line 164
    aget-object v3, v0, v6

    .line 165
    .local v3, "epgChannelInfo":Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    iget-object v10, v2, Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;->channelId:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelGuid()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelGuid()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_6

    .line 166
    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelGuid()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 167
    iget-object v10, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mChannels:Ljava/util/ArrayList;

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    :cond_6
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 172
    .end local v0    # "channelInfo":[Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    .end local v2    # "dataChannelInfo":Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData$ChannelInfo;
    .end local v3    # "epgChannelInfo":Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    .end local v6    # "j":I
    .end local v8    # "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    :cond_7
    const-string v10, "RecentChannelsModel"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "processChannelData: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mChannels:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " channels added to model"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    const/4 v10, 0x0

    iput-object v10, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mRawData:Lcom/microsoft/xbox/xle/urc/net/RecentChannelsData;

    .line 177
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mLoading:Z

    .line 178
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->notifyListenersState()V

    .line 179
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->notifyListenersData()V

    goto/16 :goto_0
.end method

.method public refresh()V
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mLoading:Z

    if-eqz v0, :cond_0

    .line 86
    :goto_0
    return-void

    .line 84
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->stopLoading()V

    .line 85
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->requestChannels()V

    goto :goto_0
.end method

.method public start(Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$IListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$IListener;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->addListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ITVRecentsListener;)V

    .line 61
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->addListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;)V

    .line 62
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->addListener(Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;)V

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 67
    :cond_1
    return-void
.end method

.method public stop(Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$IListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$IListener;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->removeListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ITVRecentsListener;)V

    .line 73
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->removeListener(Lcom/microsoft/xbox/xle/urc/net/BranchSession$ISessionListener;)V

    .line 74
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->removeListener(Lcom/microsoft/xbox/service/model/epg/EPGModel$IAllProvidersListener;)V

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mGetChannelsTask:Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$GetChannelsAsyncTask;

    .line 78
    :cond_0
    return-void
.end method

.method public stopLoading()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 89
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mLoading:Z

    .line 90
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mGetChannelsTask:Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$GetChannelsAsyncTask;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mGetChannelsTask:Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$GetChannelsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$GetChannelsAsyncTask;->cancel()V

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mGetChannelsTask:Lcom/microsoft/xbox/xle/epg/RecentChannelsModel$GetChannelsAsyncTask;

    .line 94
    :cond_0
    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mError:Z

    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/xle/epg/RecentChannelsModel;->mChannels:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 96
    return-void
.end method
