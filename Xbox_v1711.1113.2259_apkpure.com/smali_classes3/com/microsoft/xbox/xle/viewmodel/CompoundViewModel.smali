.class public Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "CompoundViewModel.java"


# static fields
.field private static final EMPTY_SET:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private viewModels:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->EMPTY_SET:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 25
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->EMPTY_SET:Ljava/util/Set;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p1, "viewModels":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 30
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0, p1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    .line 31
    return-void
.end method

.method public varargs constructor <init>([Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
    .locals 4
    .param p1, "viewModels"    # [Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 34
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    .line 35
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v0, p1, v1

    .line 36
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 37
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->setParent(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 35
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 40
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_1
    return-void
.end method

.method public static activate(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
    .locals 0
    .param p0, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 338
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onStart()V

    .line 339
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onResume()V

    .line 340
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onSetActive()V

    .line 341
    return-void
.end method

.method public static deactivate(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
    .locals 0
    .param p0, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 344
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onSetInactive()V

    .line 345
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onPause()V

    .line 346
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onStop()V

    .line 347
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onDestroy()V

    .line 348
    return-void
.end method

.method private ensureWritable()V
    .locals 2

    .prologue
    .line 351
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->EMPTY_SET:Ljava/util/Set;

    if-ne v0, v1, :cond_0

    .line 352
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    .line 354
    :cond_0
    return-void
.end method


# virtual methods
.method public addViewModel(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)Z
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->addViewModel(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Z)Z

    move-result v0

    return v0
.end method

.method public addViewModel(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Z)Z
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p2, "activate"    # Z

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->ensureWritable()V

    .line 52
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 53
    .local v0, "added":Z
    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {p1, p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->setParent(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 55
    if-eqz p2, :cond_0

    .line 56
    invoke-static {p1}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->activate(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 59
    :cond_0
    return v0
.end method

.method public findFirstChildViewModel(Ljava/lang/Class;)Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 84
    .local p1, "vmType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->getChildViewModels()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 85
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {p1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 89
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public findViewById(I)Landroid/view/View;
    .locals 5
    .param p1, "id"    # I

    .prologue
    .line 366
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 367
    .local v1, "view":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 376
    .end local v1    # "view":Landroid/view/View;
    :goto_0
    return-object v1

    .line 370
    .restart local v1    # "view":Landroid/view/View;
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 371
    .local v2, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 372
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_1

    move-object v1, v0

    .line 373
    goto :goto_0

    .line 376
    .end local v0    # "v":Landroid/view/View;
    .end local v2    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public forceRefresh()V
    .locals 3

    .prologue
    .line 127
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 128
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->forceRefresh()V

    goto :goto_0

    .line 130
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public forceUpdateViewImmediately()V
    .locals 3

    .prologue
    .line 134
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->forceUpdateViewImmediately()V

    .line 135
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 136
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->forceUpdateViewImmediately()V

    goto :goto_0

    .line 138
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public getChildViewModels()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    return-object v0
.end method

.method public getIsActive()Z
    .locals 4

    .prologue
    .line 142
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->getIsActive()Z

    move-result v0

    .line 143
    .local v0, "active":Z
    if-nez v0, :cond_1

    .line 144
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 145
    .local v1, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->getIsActive()Z

    move-result v0

    .line 146
    if-eqz v0, :cond_0

    .line 151
    .end local v1    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_1
    return v0
.end method

.method public getShowNoNetworkPopup()Z
    .locals 4

    .prologue
    .line 156
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->getShowNoNetworkPopup()Z

    move-result v0

    .line 157
    .local v0, "show":Z
    if-nez v0, :cond_1

    .line 158
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 159
    .local v1, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->getShowNoNetworkPopup()Z

    move-result v0

    .line 160
    if-eqz v0, :cond_0

    .line 165
    .end local v1    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_1
    return v0
.end method

.method public getTestMenuButtons()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/toolkit/ui/appbar/AppBarMenuButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 170
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 171
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/toolkit/ui/appbar/AppBarMenuButton;>;"
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->getTestMenuButtons()Ljava/util/List;

    move-result-object v0

    .line 172
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/toolkit/ui/appbar/AppBarMenuButton;>;"
    if-eqz v0, :cond_0

    .line 173
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 175
    :cond_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 176
    .local v2, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->getTestMenuButtons()Ljava/util/List;

    move-result-object v0

    .line 177
    if-eqz v0, :cond_1

    .line 178
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 181
    .end local v2    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_2
    return-object v1
.end method

.method public isBlockingBusy()Z
    .locals 4

    .prologue
    .line 106
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->isBlockingBusy()Z

    move-result v0

    .line 107
    .local v0, "busy":Z
    if-nez v0, :cond_1

    .line 108
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 109
    .local v1, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->isBlockingBusy()Z

    move-result v0

    .line 110
    if-eqz v0, :cond_0

    .line 115
    .end local v1    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_1
    return v0
.end method

.method public isBusy()Z
    .locals 4

    .prologue
    .line 94
    const/4 v0, 0x0

    .line 95
    .local v0, "busy":Z
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 96
    .local v1, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->isBusy()Z

    move-result v0

    .line 97
    if-eqz v0, :cond_0

    .line 101
    .end local v1    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_1
    return v0
.end method

.method public load(Z)V
    .locals 3
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 120
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 121
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->load(Z)V

    goto :goto_0

    .line 123
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 187
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 188
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0, p1, p2, p3}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    .line 190
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public onAnimateInCompleted()V
    .locals 3

    .prologue
    .line 289
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onAnimateInCompleted()V

    .line 290
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 291
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onAnimateInCompleted()V

    goto :goto_0

    .line 293
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public onApplicationPause()V
    .locals 3

    .prologue
    .line 218
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onApplicationPause()V

    .line 219
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 220
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onApplicationPause()V

    goto :goto_0

    .line 222
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public onApplicationResume()V
    .locals 3

    .prologue
    .line 226
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onApplicationResume()V

    .line 227
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 228
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onApplicationResume()V

    goto :goto_0

    .line 230
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 265
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 266
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 267
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    goto :goto_0

    .line 269
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 312
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 313
    .local v0, "ret":Z
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 314
    .local v1, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 315
    :goto_1
    goto :goto_0

    .line 314
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 316
    .end local v1    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_2
    return v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 242
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onDestroy()V

    .line 243
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 244
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onDestroy()V

    goto :goto_0

    .line 246
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 330
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 331
    .local v0, "ret":Z
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 332
    .local v1, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 333
    :goto_1
    goto :goto_0

    .line 332
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 334
    .end local v1    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_2
    return v0
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 210
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onPause()V

    .line 211
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 212
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onPause()V

    goto :goto_0

    .line 214
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 321
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    .line 322
    .local v0, "ret":Z
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 323
    .local v1, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 324
    :goto_1
    goto :goto_0

    .line 323
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 325
    .end local v1    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_2
    return v0
.end method

.method public onRehydrate()V
    .locals 3

    .prologue
    .line 258
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 259
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onRehydrate()V

    goto :goto_0

    .line 261
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 281
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 282
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 283
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onRestoreInstanceState(Landroid/os/Bundle;)V

    goto :goto_0

    .line 285
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 234
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onResume()V

    .line 235
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 236
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onResume()V

    goto :goto_0

    .line 238
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 273
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 274
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 275
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onSaveInstanceState(Landroid/os/Bundle;)V

    goto :goto_0

    .line 277
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public onSetActive()V
    .locals 3

    .prologue
    .line 297
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 298
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onSetActive()V

    goto :goto_0

    .line 300
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public onSetInactive()V
    .locals 3

    .prologue
    .line 304
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onSetInactive()V

    .line 305
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 306
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onSetInactive()V

    goto :goto_0

    .line 308
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 194
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onStart()V

    .line 195
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 196
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onStart()V

    goto :goto_0

    .line 198
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 358
    return-void
.end method

.method public onStop()V
    .locals 3

    .prologue
    .line 202
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onStop()V

    .line 203
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 204
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onStop()V

    goto :goto_0

    .line 206
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 362
    return-void
.end method

.method public onTombstone()V
    .locals 3

    .prologue
    .line 250
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onTombstone()V

    .line 251
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 252
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->onTombstone()V

    goto :goto_0

    .line 254
    .end local v0    # "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    :cond_0
    return-void
.end method

.method public removeViewModel(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)Z
    .locals 1
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->removeViewModel(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Z)Z

    move-result v0

    return v0
.end method

.method public removeViewModel(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Z)Z
    .locals 2
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .param p2, "deactivate"    # Z

    .prologue
    .line 67
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->viewModels:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 68
    .local v0, "removed":Z
    if-eqz v0, :cond_1

    .line 69
    if-eqz p2, :cond_0

    .line 70
    invoke-static {p1}, Lcom/microsoft/xbox/xle/viewmodel/CompoundViewModel;->deactivate(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 72
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->setParent(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 74
    :cond_1
    return v0
.end method
