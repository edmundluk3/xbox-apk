.class Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "EPGChannel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/epg/EPGChannel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ToggleFavoriteAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private listFull:Z

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/epg/EPGChannel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V
    .locals 1

    .prologue
    .line 174
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    .line 176
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;->listFull:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/service/model/epg/EPGChannel;Lcom/microsoft/xbox/service/model/epg/EPGChannel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    .param p2, "x1"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel$1;

    .prologue
    .line 174
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;-><init>(Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x1

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 219
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->access$400(Lcom/microsoft/xbox/service/model/epg/EPGChannel;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getProvider(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v0

    .line 220
    .local v0, "provider":Lcom/microsoft/xbox/service/model/epg/EPGProvider;
    if-nez v0, :cond_0

    .line 221
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 226
    :goto_0
    return-object v2

    .line 224
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-static {v2}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->access$500(Lcom/microsoft/xbox/service/model/epg/EPGChannel;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->access$100(Lcom/microsoft/xbox/service/model/epg/EPGChannel;)Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->setFavoriteForChannel(Ljava/lang/String;Z)Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    move-result-object v1

    .line 225
    .local v1, "ret":Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;
    sget-object v2, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;->errorFullList:Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    if-ne v1, v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;->listFull:Z

    .line 226
    sget-object v2, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;->resultNoError:Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    if-ne v1, v2, :cond_2

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0

    .line 225
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 226
    :cond_2
    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 210
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 174
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 0

    .prologue
    .line 215
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 7
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 189
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 190
    iget-object v6, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-static {v3}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->access$100(Lcom/microsoft/xbox/service/model/epg/EPGChannel;)Z

    move-result v3

    if-nez v3, :cond_2

    move v3, v4

    :goto_0
    invoke-static {v6, v3}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->access$102(Lcom/microsoft/xbox/service/model/epg/EPGChannel;Z)Z

    .line 191
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    iget-object v3, v3, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->propNotificaton:Lcom/microsoft/xbox/service/model/epg/PropertyNotification;

    const-string v6, "favorite"

    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/service/model/epg/PropertyNotification;->notify(Ljava/lang/String;)V

    .line 194
    :cond_0
    iget-boolean v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;->listFull:Z

    if-eqz v3, :cond_1

    .line 195
    const v3, 0x7f0300f8

    invoke-static {v3, v4, v4}, Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;->show(IZZ)Lcom/microsoft/xbox/xle/epg/EPGDialogFromLayout;

    .line 196
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/DialogManager;->getVisibleDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 197
    .local v0, "d":Landroid/app/Dialog;
    const v3, 0x7f0e058f

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 198
    .local v2, "textView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    if-eqz v2, :cond_1

    .line 199
    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 200
    .local v1, "text":Ljava/lang/String;
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    invoke-static {v6}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->access$200(Lcom/microsoft/xbox/service/model/epg/EPGChannel;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v1, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 201
    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 205
    .end local v0    # "d":Landroid/app/Dialog;
    .end local v1    # "text":Ljava/lang/String;
    .end local v2    # "textView":Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;->this$0:Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->access$302(Lcom/microsoft/xbox/service/model/epg/EPGChannel;Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;)Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;

    .line 206
    return-void

    :cond_2
    move v3, v5

    .line 190
    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 174
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/epg/EPGChannel$ToggleFavoriteAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 185
    return-void
.end method
