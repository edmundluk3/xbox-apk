.class public final Lcom/microsoft/playready/InbandEventInfo;
.super Ljava/lang/Object;
.source "InbandEventInfo.java"


# instance fields
.field private mEventValue:Ljava/lang/String;

.field private mSchemeId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "schemeId"    # Ljava/lang/String;
    .param p2, "eventValue"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/microsoft/playready/InbandEventInfo;->mSchemeId:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Lcom/microsoft/playready/InbandEventInfo;->mEventValue:Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method public getEventValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/microsoft/playready/InbandEventInfo;->mEventValue:Ljava/lang/String;

    return-object v0
.end method

.method public getSchemeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/microsoft/playready/InbandEventInfo;->mSchemeId:Ljava/lang/String;

    return-object v0
.end method
