.class final enum Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;
.super Ljava/lang/Enum;
.source "CanvasWebViewActivityAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "SplashBodyState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;

.field public static final enum Error:Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;

.field public static final enum Normal:Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 60
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;

    const-string v1, "Normal"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;->Normal:Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;

    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;

    const-string v1, "Error"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;->Error:Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;

    .line 59
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;->Normal:Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;->Error:Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;->$VALUES:[Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 59
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;->$VALUES:[Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/adapter/CanvasWebViewActivityAdapter$SplashBodyState;

    return-object v0
.end method
