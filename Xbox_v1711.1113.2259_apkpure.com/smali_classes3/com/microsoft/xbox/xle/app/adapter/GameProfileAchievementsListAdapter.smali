.class public Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "GameProfileAchievementsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementsEmptyViewHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;,
        Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;",
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
        "<",
        "Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final ACHIEVEMENT_ERROR_LAYOUT:I = 0x7f0301bb

.field private static final ACHIEVEMENT_HEADER_LAYOUT:I = 0x7f030119

.field private static final ACHIEVEMENT_ITEMS_EMPTY_LAYOUT:I = 0x7f03012e

.field private static final ACHIEVEMENT_ITEM_LOCKED_LAYOUT:I = 0x7f03012a

.field private static final ACHIEVEMENT_ITEM_UNLOCKED_LAYOUT:I = 0x7f03012b

.field private static final LEADERBOARD_HEADER_LAYOUT:I = 0x7f03011a

.field private static final LEADERBOARD_ITEM_LAYOUT:I = 0x7f030129

.field public static final TAG:Ljava/lang/String;


# instance fields
.field private gameProfileAchievementsScreenViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;)V
    .locals 0
    .param p1, "gameProfileAchievementsScreenViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->gameProfileAchievementsScreenViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    .line 56
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;)Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->gameProfileAchievementsScreenViewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    return-object v0
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 91
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->data:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;

    .line 92
    .local v0, "compositeItem":Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->isLeaderboardHeader()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 93
    const v1, 0x7f03011a

    .line 110
    :goto_0
    return v1

    .line 94
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->isLeaderboardItem()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 95
    const v1, 0x7f030129

    goto :goto_0

    .line 96
    :cond_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->isAchievementHeader()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 97
    const v1, 0x7f030119

    goto :goto_0

    .line 98
    :cond_2
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->isAchievementItem()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 101
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->achievementItem()Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->getProgressState()Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;->Achieved:Lcom/microsoft/xbox/xle/app/adapter/ChallengeProgressState;

    if-eq v1, v2, :cond_3

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->achievementItem()Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/GameProgressAchievementsItemBase;->isExpired()Z

    move-result v1

    if-nez v1, :cond_3

    .line 102
    const v1, 0x7f03012a

    goto :goto_0

    .line 104
    :cond_3
    const v1, 0x7f03012b

    goto :goto_0

    .line 106
    :cond_4
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;->isEmptyAchievementItems()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 107
    const v1, 0x7f03012e

    goto :goto_0

    .line 109
    :cond_5
    const-string v1, "Attempted to getItemViewType for an unsupported GameProfileAchivementsCompositeDataItem"

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 110
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 41
    check-cast p1, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->onBindViewHolder(Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;I)V
    .locals 1
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
            "<",
            "Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 83
    .local p1, "holder":Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;, "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase<Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;>;"
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;

    .line 84
    .local v0, "item":Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;
    if-eqz v0, :cond_0

    .line 85
    invoke-virtual {p1, v0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;->onBind(Ljava/lang/Object;)V

    .line 87
    :cond_0
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewTypeLayout"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase",
            "<",
            "Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsCompositeItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 61
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const/4 v2, 0x0

    invoke-virtual {v0, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 63
    .local v1, "rootView":Landroid/view/View;
    sparse-switch p2, :sswitch_data_0

    .line 76
    const-string v2, "Attempted to create an invalid view holder"

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 77
    const/4 v2, 0x0

    :goto_0
    return-object v2

    .line 65
    :sswitch_0
    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder;

    invoke-direct {v2, p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboadHeaderViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 67
    :sswitch_1
    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;

    invoke-direct {v2, p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$LeaderboardItemViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 69
    :sswitch_2
    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementsEmptyViewHolder;

    invoke-direct {v2, p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementsEmptyViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 72
    :sswitch_3
    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;

    invoke-direct {v2, p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementItemViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 74
    :sswitch_4
    new-instance v2, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;

    invoke-direct {v2, p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter$AchievementHeaderViewHolder;-><init>(Lcom/microsoft/xbox/xle/app/adapter/GameProfileAchievementsListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 63
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f030119 -> :sswitch_4
        0x7f03011a -> :sswitch_0
        0x7f030129 -> :sswitch_1
        0x7f03012a -> :sswitch_3
        0x7f03012b -> :sswitch_3
        0x7f03012e -> :sswitch_2
    .end sparse-switch
.end method
