.class public final Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_CloudComputePackage$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_MultiplayerDataTypes_CloudComputePackage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_CloudComputePackage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputePackage;",
        ">;"
    }
.end annotation


# instance fields
.field private final crossSandboxAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final gsiSetAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final titleIdAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 29
    const-class v0, Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_CloudComputePackage$GsonTypeAdapter;->crossSandboxAdapter:Lcom/google/gson/TypeAdapter;

    .line 30
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_CloudComputePackage$GsonTypeAdapter;->gsiSetAdapter:Lcom/google/gson/TypeAdapter;

    .line 31
    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_CloudComputePackage$GsonTypeAdapter;->titleIdAdapter:Lcom/google/gson/TypeAdapter;

    .line 32
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputePackage;
    .locals 6
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v4

    sget-object v5, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v4, v5, :cond_0

    .line 52
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 53
    const/4 v4, 0x0

    .line 84
    :goto_0
    return-object v4

    .line 55
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 56
    const/4 v1, 0x0

    .line 57
    .local v1, "crossSandbox":Ljava/lang/Boolean;
    const/4 v2, 0x0

    .line 58
    .local v2, "gsiSet":Ljava/lang/String;
    const/4 v3, 0x0

    .line 59
    .local v3, "titleId":Ljava/lang/String;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 60
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 61
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v4

    sget-object v5, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v4, v5, :cond_1

    .line 62
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 65
    :cond_1
    const/4 v4, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v4, :pswitch_data_0

    .line 79
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 65
    :sswitch_0
    const-string v5, "crossSandbox"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x0

    goto :goto_2

    :sswitch_1
    const-string v5, "gsiSet"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x1

    goto :goto_2

    :sswitch_2
    const-string v5, "titleId"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x2

    goto :goto_2

    .line 67
    :pswitch_0
    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_CloudComputePackage$GsonTypeAdapter;->crossSandboxAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v4, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "crossSandbox":Ljava/lang/Boolean;
    check-cast v1, Ljava/lang/Boolean;

    .line 68
    .restart local v1    # "crossSandbox":Ljava/lang/Boolean;
    goto :goto_1

    .line 71
    :pswitch_1
    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_CloudComputePackage$GsonTypeAdapter;->gsiSetAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v4, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "gsiSet":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 72
    .restart local v2    # "gsiSet":Ljava/lang/String;
    goto :goto_1

    .line 75
    :pswitch_2
    iget-object v4, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_CloudComputePackage$GsonTypeAdapter;->titleIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v4, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "titleId":Ljava/lang/String;
    check-cast v3, Ljava/lang/String;

    .line 76
    .restart local v3    # "titleId":Ljava/lang/String;
    goto :goto_1

    .line 83
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 84
    new-instance v4, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_CloudComputePackage;

    invoke-direct {v4, v1, v2, v3}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_CloudComputePackage;-><init>(Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 65
    :sswitch_data_0
    .sparse-switch
        -0x4deb0a6d -> :sswitch_2
        -0x49b749db -> :sswitch_1
        0x4aaff667 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_CloudComputePackage$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputePackage;

    move-result-object v0

    return-object v0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputePackage;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputePackage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    if-nez p2, :cond_0

    .line 37
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 48
    :goto_0
    return-void

    .line 40
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 41
    const-string v0, "crossSandbox"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 42
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_CloudComputePackage$GsonTypeAdapter;->crossSandboxAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputePackage;->crossSandbox()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 43
    const-string v0, "gsiSet"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_CloudComputePackage$GsonTypeAdapter;->gsiSetAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputePackage;->gsiSet()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 45
    const-string v0, "titleId"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_CloudComputePackage$GsonTypeAdapter;->titleIdAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputePackage;->titleId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 47
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    check-cast p2, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputePackage;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xbservices/data/service/multiplayer/AutoValue_MultiplayerDataTypes_CloudComputePackage$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/xbservices/data/service/multiplayer/MultiplayerDataTypes$CloudComputePackage;)V

    return-void
.end method
