.class public abstract Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;
.super Ljava/lang/Object;
.source "ClubHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ClubRoleItem"
.end annotation


# instance fields
.field private volatile transient createdDateObject:Ljava/util/Date;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1195
    new-instance v0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_ClubRoleItem$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract actorXuid()J
.end method

.method public abstract createdDate()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public getCreatedDate()Ljava/util/Date;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1183
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;->createdDateObject:Ljava/util/Date;

    if-nez v0, :cond_0

    .line 1184
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;->createdDate()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/serialization/UTCDateConverterGson;->convert(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;->createdDateObject:Ljava/util/Date;

    .line 1185
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;->createdDateObject:Ljava/util/Date;

    if-nez v0, :cond_0

    .line 1186
    invoke-static {}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not creation date: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;->createdDate()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 1187
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;->createdDateObject:Ljava/util/Date;

    .line 1191
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoleItem;->createdDateObject:Ljava/util/Date;

    return-object v0
.end method

.method public abstract role()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubSettingsRole;
.end method
