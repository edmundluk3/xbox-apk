.class public Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;
.source "GameProgressChallengesScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;
    }
.end annotation


# instance fields
.field private isLoadingLimitedTimeChallenge:Z

.field private item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

.field protected limitedTimeChallenges:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;",
            ">;"
        }
    .end annotation
.end field

.field private loadLimitedTimeChallengeTask:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;

.field protected model:Lcom/microsoft/xbox/service/model/TitleModel;

.field private profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

.field protected titleId:J

.field protected viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V
    .locals 2
    .param p1, "adapterProvider"    # Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/MultiPurposeViewModelBase;-><init>()V

    .line 29
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 41
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->setAdapterProvider(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 42
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getRecentProgressAndAchievementItem()Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    .line 49
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getInstance()Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/XLEGlobalData;->getSelectedTitleId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->titleId:J

    .line 50
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;)Lcom/microsoft/xbox/service/model/ProfileModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    return-object v0
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->isLoadingLimitedTimeChallenge:Z

    return p1
.end method

.method private getCurrentContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    instance-of v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentType:Ljava/lang/String;

    .line 195
    :goto_0
    return-object v0

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    if-eqz v0, :cond_1

    .line 192
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getTitleType()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 195
    :cond_1
    const-string v0, "dgame"

    goto :goto_0
.end method

.method private getItemTitleId()J
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    instance-of v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->titleId:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    .line 185
    :goto_0
    return-wide v0

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    instance-of v0, v0, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;

    if-eqz v0, :cond_1

    .line 180
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;

    iget v0, v0, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->titleId:I

    int-to-long v0, v0

    goto :goto_0

    .line 181
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    instance-of v0, v0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    if-eqz v0, :cond_2

    .line 182
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    iget-wide v0, v0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->titleId:J

    goto :goto_0

    .line 185
    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private updateViewModelState()V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->limitedTimeChallenges:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->limitedTimeChallenges:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 151
    :cond_0
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->isLoadingLimitedTimeChallenge:Z

    if-eqz v0, :cond_1

    .line 152
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 159
    :goto_0
    return-void

    .line 154
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 157
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method


# virtual methods
.method public getBackgroundImageUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 78
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    if-eqz v1, :cond_0

    .line 79
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleImageDetailsData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    .line 80
    .local v0, "titleImageDetail":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Images:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 81
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v1

    .line 84
    .end local v0    # "titleImageDetail":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->limitedTimeChallenges:Ljava/util/List;

    return-object v0
.end method

.method public getGameTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 62
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    if-nez v1, :cond_0

    .line 63
    const/4 v1, 0x0

    .line 73
    :goto_0
    return-object v1

    .line 66
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    instance-of v1, v1, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;

    if-eqz v1, :cond_1

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;

    .line 68
    .local v0, "i":Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;
    iget-object v1, v0, Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;->name:Ljava/lang/String;

    goto :goto_0

    .line 69
    .end local v0    # "i":Lcom/microsoft/xbox/service/network/managers/IRecent360ProgressAndAchievementResult$Recent360ProgressAndAchievementItem;
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    instance-of v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-eqz v1, :cond_2

    .line 70
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentTitle:Ljava/lang/String;

    goto :goto_0

    .line 72
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    .line 73
    .local v0, "i":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getTitleName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->isLoadingLimitedTimeChallenge:Z

    return v0
.end method

.method public isGame()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 163
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->getCurrentContentType()Ljava/lang/String;

    move-result-object v0

    .line 165
    .local v0, "titleType":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 166
    const-string v2, "game"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "dgame"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 173
    :cond_0
    :goto_0
    return v1

    .line 169
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 200
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->loadLimitedTimeChallengeTask:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->loadLimitedTimeChallengeTask:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;->cancel()V

    .line 203
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->loadLimitedTimeChallengeTask:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;

    .line 204
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->loadLimitedTimeChallengeTask:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;->load(Z)V

    .line 205
    return-void
.end method

.method public navigateToChallengeDetails(Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;)V
    .locals 5
    .param p1, "item"    # Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;

    .prologue
    .line 246
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 247
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    instance-of v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-eqz v1, :cond_0

    .line 248
    new-instance v2, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->id:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iget-object v4, p1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->serviceConfigId:Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->contentType:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v1}, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putTitleDetailInfo(Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;)V

    .line 252
    :goto_0
    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->titleId:J

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putTitleId(J)V

    .line 253
    const-class v1, Lcom/microsoft/xbox/xle/app/activity/ChallengeDetailsScreen;

    invoke-virtual {p0, v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->NavigateTo(Ljava/lang/Class;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 254
    return-void

    .line 250
    :cond_0
    new-instance v2, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;

    iget-object v1, p1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->id:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iget-object v4, p1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsItem;->serviceConfigId:Ljava/lang/String;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getTitleType()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v4, v1}, Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putTitleDetailInfo(Lcom/microsoft/xbox/service/network/managers/TitleDetailInfo;)V

    goto :goto_0
.end method

.method protected onLoadLimitedTimeChallengesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 112
    const-string v1, "GameProgressChallengesScreenViewModel"

    const-string v2, "onLoadLimitedTimeChallengesCompleted Completed"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->isLoadingLimitedTimeChallenge:Z

    .line 115
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 145
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->updateAdapter()V

    .line 146
    return-void

    .line 119
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    if-eqz v1, :cond_1

    .line 120
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/TitleModel;->getGameProgressLimitedTimeChallenge()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->limitedTimeChallenges:Ljava/util/List;

    .line 121
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->titleId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getTitleProgressData(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    move-result-object v0

    .line 122
    .local v0, "titleProgress":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    if-eqz v0, :cond_1

    .line 123
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    iget-object v2, v0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;->setTitleName(Ljava/lang/String;)V

    .line 124
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    instance-of v1, v1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    if-nez v1, :cond_1

    .line 125
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->getTitleType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;->setTitleType(Ljava/lang/String;)V

    .line 130
    .end local v0    # "titleProgress":Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->updateViewModelState()V

    .line 132
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v1, v2, :cond_0

    .line 133
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->setShouldHideScreen(Z)V

    goto :goto_0

    .line 139
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->limitedTimeChallenges:Ljava/util/List;

    if-nez v1, :cond_0

    .line 140
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 115
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected onStartOverride()V
    .locals 4

    .prologue
    .line 95
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 96
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->titleId:J

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->getTitleModel(J)Lcom/microsoft/xbox/service/model/TitleModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->model:Lcom/microsoft/xbox/service/model/TitleModel;

    .line 98
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->getItemTitleId()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->titleId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 99
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/IRecentProgressAndAchievementResult$RecentProgressAndAchievementItem;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->item:Lcom/microsoft/xbox/service/network/managers/RecentProgressAndAchievementItemBase;

    .line 101
    :cond_0
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->limitedTimeChallenges:Ljava/util/List;

    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->loadLimitedTimeChallengeTask:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel;->loadLimitedTimeChallengeTask:Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProgressChallengesScreenViewModel$LoadLimitedTimeChallengesAsyncTask;->cancel()V

    .line 109
    :cond_0
    return-void
.end method
