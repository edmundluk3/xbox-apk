.class public abstract Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;
.super Ljava/lang/Object;
.source "CompanionSession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IPrimaryDeviceStateChangedListener;,
        Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IChannelEstablishedListener;,
        Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionRequestCompleteListener;,
        Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleMessageListener;,
        Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IMediaStateListener;,
        Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangeCompletedListener;,
        Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangedListener;,
        Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionStateListener;
    }
.end annotation


# static fields
.field public static final LRCERROR_DUPLICATEREQ:I = 0x8

.field public static final LRCERROR_EXCLUSIVE_CONNECTED:I = 0xf

.field public static final LRCERROR_EXPIRED_COMMAND:I = 0xd

.field public static final LRCERROR_FAILURE:I = 0x3

.field public static final LRCERROR_FAIL_TO_CONNECT_TO_SESSION:I = 0xb

.field public static final LRCERROR_FAIL_TO_GET_SESSION:I = 0xa

.field public static final LRCERROR_INTERNAL:I = 0x5

.field public static final LRCERROR_INVALIDARGS:I = 0x2

.field public static final LRCERROR_INVALIDCONTENT:I = 0x7

.field public static final LRCERROR_IN_PROGRESS:I = 0x10

.field public static final LRCERROR_NOSESSION:I = 0x4

.field public static final LRCERROR_NO_EXCLUSIVE:I = 0xe

.field public static final LRCERROR_OUTOFMEMORY:I = 0x1

.field public static final LRCERROR_REQUEST_TIMEOUT:I = 0x6

.field public static final LRCERROR_SUCCESS:I = 0x0

.field public static final LRCERROR_TITLECHANNEL_EXISTS:I = 0x11

.field public static final LRCERROR_TMF_SIGNIN_FAIL:I = 0x9

.field public static final LRCERROR_TOO_MANY_CLIENTS:I = 0xc

.field public static final LRC_MESSAGE_TYPE_GET_ACTIVE_TITLEID:I = 0x1

.field public static final LRC_MESSAGE_TYPE_GET_MEDIA_TITLE_STATE:I = 0x4

.field public static final LRC_MESSAGE_TYPE_JOIN_SESSION:I = -0x7fffffff

.field public static final LRC_MESSAGE_TYPE_LAUNCH_TITLE:I = 0x2

.field public static final LRC_MESSAGE_TYPE_LEAVE_SESSION:I = -0x7ffffffe

.field public static final LRC_MESSAGE_TYPE_MEDIA_TITLE_STATE_NOTIFICATION:I = 0x6

.field public static final LRC_MESSAGE_TYPE_NONE:I = 0x0

.field public static final LRC_MESSAGE_TYPE_NON_MEDIA_TITLE_STATE_NOTIFICATION:I = 0x5

.field public static final LRC_MESSAGE_TYPE_SEND_INPUT:I = 0x3

.field public static final SESSION_CAPABILITY_DEFAULT:I = 0x1

.field public static final SESSION_CAPABILITY_LOCALTCP:I = 0x3

.field public static final SESSION_CAPABILITY_NOTCONNECTED:I = 0x0

.field public static final SESSION_STATE_CONNECTED:I = 0x2

.field public static final SESSION_STATE_CONNECTING:I = 0x1

.field public static final SESSION_STATE_CONNECTION_FAILED:I = 0x3

.field public static final SESSION_STATE_DISCONNECTED:I = 0x0

.field public static final SESSION_STATE_DISCONNECTING:I = 0x4

.field private static TAG:Ljava/lang/String;


# instance fields
.field public final NO_TITLE_ID_WITH_FOCUS:I

.field protected TEST_lastSentKeys:Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList",
            "<",
            "Lcom/microsoft/xbox/service/model/LRCControlKey;",
            ">;"
        }
    .end annotation
.end field

.field protected activeTitleStates:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;",
            "Lcom/microsoft/xbox/smartglass/ActiveTitleState;",
            ">;"
        }
    .end annotation
.end field

.field protected final channelEstablishedListenerList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IChannelEstablishedListener;",
            ">;"
        }
    .end annotation
.end field

.field protected clientResolution:Lcom/microsoft/xbox/smartglass/ClientResolution;

.field protected currentMediaStates:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/microsoft/xbox/smartglass/MediaState;",
            ">;"
        }
    .end annotation
.end field

.field protected final datalock:Ljava/lang/Object;

.field protected lastErrorCode:J

.field protected final mediaTitleStateListenerList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IMediaStateListener;",
            ">;"
        }
    .end annotation
.end field

.field protected final primaryDeviceStateChangedListenerList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IPrimaryDeviceStateChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final requestCompleteListenerList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionRequestCompleteListener;",
            ">;"
        }
    .end annotation
.end field

.field protected sessionId:Ljava/util/UUID;

.field protected sessionState:I

.field protected final sessionStateListenerList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionStateListener;",
            ">;"
        }
    .end annotation
.end field

.field protected final titleChangeCompletedListenerList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangeCompletedListener;",
            ">;"
        }
    .end annotation
.end field

.field protected final titleChangedListenerList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field protected final titleMessageListenerList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleMessageListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->datalock:Ljava/lang/Object;

    .line 150
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->sessionId:Ljava/util/UUID;

    .line 155
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->activeTitleStates:Ljava/util/Hashtable;

    .line 157
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->currentMediaStates:Ljava/util/Hashtable;

    .line 164
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->sessionStateListenerList:Ljava/util/LinkedList;

    .line 165
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->mediaTitleStateListenerList:Ljava/util/LinkedList;

    .line 166
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleChangedListenerList:Ljava/util/LinkedList;

    .line 167
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleChangeCompletedListenerList:Ljava/util/LinkedList;

    .line 168
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleMessageListenerList:Ljava/util/LinkedList;

    .line 169
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->requestCompleteListenerList:Ljava/util/LinkedList;

    .line 170
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->channelEstablishedListenerList:Ljava/util/LinkedList;

    .line 171
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->primaryDeviceStateChangedListenerList:Ljava/util/LinkedList;

    .line 172
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->clientResolution:Lcom/microsoft/xbox/smartglass/ClientResolution;

    .line 328
    const/4 v0, -0x1

    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->NO_TITLE_ID_WITH_FOCUS:I

    .line 175
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getTag()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->TAG:Ljava/lang/String;

    .line 176
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->lastErrorCode:J

    .line 177
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->sessionState:I

    .line 178
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->activeTitleStates:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    .line 179
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private getClientResolution()Lcom/microsoft/xbox/smartglass/ClientResolution;
    .locals 6

    .prologue
    .line 642
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->clientResolution:Lcom/microsoft/xbox/smartglass/ClientResolution;

    if-nez v1, :cond_0

    .line 643
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 646
    .local v0, "dm":Landroid/util/DisplayMetrics;
    new-instance v1, Lcom/microsoft/xbox/smartglass/ClientResolution;

    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-short v2, v2

    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-short v3, v3

    iget v4, v0, Landroid/util/DisplayMetrics;->xdpi:F

    float-to-int v4, v4

    int-to-short v4, v4

    iget v5, v0, Landroid/util/DisplayMetrics;->ydpi:F

    float-to-int v5, v5

    int-to-short v5, v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/ClientResolution;-><init>(SSSS)V

    iput-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->clientResolution:Lcom/microsoft/xbox/smartglass/ClientResolution;

    .line 649
    .end local v0    # "dm":Landroid/util/DisplayMetrics;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->clientResolution:Lcom/microsoft/xbox/smartglass/ClientResolution;

    return-object v1
.end method

.method private getMSATicketForConsolePairing()Ljava/lang/String;
    .locals 10

    .prologue
    .line 653
    const-string v9, ""

    .line 657
    .local v9, "ticket":Ljava/lang/String;
    :try_start_0
    new-instance v7, Lcom/microsoft/xbox/idp/interop/LocalConfig;

    invoke-direct {v7}, Lcom/microsoft/xbox/idp/interop/LocalConfig;-><init>()V

    .line 658
    .local v7, "cfg":Lcom/microsoft/xbox/idp/interop/LocalConfig;
    new-instance v3, Lcom/microsoft/xbox/idp/interop/MSATicketCallbacks;

    invoke-direct {v3}, Lcom/microsoft/xbox/idp/interop/MSATicketCallbacks;-><init>()V

    .line 659
    .local v3, "callbacks":Lcom/microsoft/xbox/idp/interop/MSATicketCallbacks;
    new-instance v0, Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->TAG:Ljava/lang/String;

    .line 662
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getXboxLiveScopeForAccessToken()Ljava/lang/String;

    move-result-object v4

    const-string v5, "MBI_SSL"

    .line 664
    invoke-virtual {v7}, Lcom/microsoft/xbox/idp/interop/LocalConfig;->getCid()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;Lcom/microsoft/xbox/idp/jobs/MSAJob$Callbacks;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    .local v0, "job":Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;
    monitor-enter v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 667
    :try_start_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;->start()Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;

    .line 668
    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    .line 669
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 671
    :try_start_2
    invoke-virtual {v3}, Lcom/microsoft/xbox/idp/interop/MSATicketCallbacks;->getTicket()Ljava/lang/String;

    move-result-object v9

    .line 673
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 674
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->TAG:Ljava/lang/String;

    const-string v2, "Unexpected empty ticket for console pairing"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 680
    .end local v0    # "job":Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;
    .end local v3    # "callbacks":Lcom/microsoft/xbox/idp/interop/MSATicketCallbacks;
    .end local v7    # "cfg":Lcom/microsoft/xbox/idp/interop/LocalConfig;
    :cond_0
    :goto_0
    return-object v9

    .line 669
    .restart local v0    # "job":Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;
    .restart local v3    # "callbacks":Lcom/microsoft/xbox/idp/interop/MSATicketCallbacks;
    .restart local v7    # "cfg":Lcom/microsoft/xbox/idp/interop/LocalConfig;
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 676
    .end local v0    # "job":Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;
    .end local v3    # "callbacks":Lcom/microsoft/xbox/idp/interop/MSATicketCallbacks;
    .end local v7    # "cfg":Lcom/microsoft/xbox/idp/interop/LocalConfig;
    :catch_0
    move-exception v8

    .line 677
    .local v8, "exception":Ljava/lang/Exception;
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->TAG:Ljava/lang/String;

    const-string v2, "Unexpected exception retrieving ticket for console pairing:"

    invoke-static {v1, v2, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic lambda$completeTextEntry$1(Lcom/microsoft/xbox/smartglass/TextAction;)V
    .locals 4
    .param p0, "action"    # Lcom/microsoft/xbox/smartglass/TextAction;

    .prologue
    .line 488
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTextManager()Lcom/microsoft/xbox/smartglass/TextManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/smartglass/TextManager;->complete(Lcom/microsoft/xbox/smartglass/TextAction;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 492
    :goto_0
    return-void

    .line 489
    :catch_0
    move-exception v0

    .line 490
    .local v0, "ex":Ljava/lang/Exception;
    const-string v1, "CompanionSession"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed to complete a text session with exception "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic lambda$sendTextInput$0(Ljava/lang/String;)V
    .locals 4
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 477
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTextManager()Lcom/microsoft/xbox/smartglass/TextManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/smartglass/TextManager;->updateText(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 481
    :goto_0
    return-void

    .line 478
    :catch_0
    move-exception v0

    .line 479
    .local v0, "ex":Ljava/lang/Exception;
    const-string v1, "CompanionSession"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed to updateText with exception "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private sendGamePad(Lcom/microsoft/xbox/smartglass/GamePad;Z)V
    .locals 4
    .param p1, "gamePad"    # Lcom/microsoft/xbox/smartglass/GamePad;
    .param p2, "simulateButtonPress"    # Z

    .prologue
    .line 540
    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/microsoft/xbox/smartglass/SessionManager;->sendGamePad(Lcom/microsoft/xbox/smartglass/GamePad;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 544
    :goto_0
    return-void

    .line 541
    :catch_0
    move-exception v0

    .line 542
    .local v0, "ex":Ljava/lang/Exception;
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendGamePad error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public TEST_getLastSentKeys()Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/microsoft/xbox/toolkit/FixedSizeLinkedList",
            "<",
            "Lcom/microsoft/xbox/service/model/LRCControlKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 375
    const-string v0, "you should not call this method in none debug build. "

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 376
    const/4 v0, 0x0

    return-object v0
.end method

.method public addChannelEstablishedListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IChannelEstablishedListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IChannelEstablishedListener;

    .prologue
    .line 287
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->channelEstablishedListenerList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 288
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->channelEstablishedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 289
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->channelEstablishedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->channelEstablishedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 292
    :cond_0
    monitor-exit v1

    .line 293
    return-void

    .line 288
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 292
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addCompanionSessionRequestCompleteListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionRequestCompleteListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionRequestCompleteListener;

    .prologue
    .line 272
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->requestCompleteListenerList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 273
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->requestCompleteListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 274
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->requestCompleteListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->requestCompleteListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 277
    :cond_0
    monitor-exit v1

    .line 278
    return-void

    .line 273
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 277
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addCompanionSessionStateListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionStateListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionStateListener;

    .prologue
    .line 188
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->sessionStateListenerList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 189
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->sessionStateListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 190
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->sessionStateListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->sessionStateListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 193
    :cond_0
    monitor-exit v1

    .line 194
    return-void

    .line 189
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 193
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addFirstTitleChangedListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangedListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangedListener;

    .prologue
    .line 227
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleChangedListenerList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 228
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleChangedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 229
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleChangedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleChangedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 232
    :cond_0
    monitor-exit v1

    .line 233
    return-void

    .line 228
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 232
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addMediaStateListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IMediaStateListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IMediaStateListener;

    .prologue
    .line 203
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->mediaTitleStateListenerList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 204
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->mediaTitleStateListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 205
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->mediaTitleStateListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->mediaTitleStateListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 208
    :cond_0
    monitor-exit v1

    .line 209
    return-void

    .line 204
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 208
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addPrimaryDeviceStateChangedListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IPrimaryDeviceStateChangedListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IPrimaryDeviceStateChangedListener;

    .prologue
    .line 302
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->primaryDeviceStateChangedListenerList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 303
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->primaryDeviceStateChangedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 304
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->primaryDeviceStateChangedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->primaryDeviceStateChangedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 307
    :cond_0
    monitor-exit v1

    .line 308
    return-void

    .line 303
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 307
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addTitleChangeCompletedListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangeCompletedListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangeCompletedListener;

    .prologue
    .line 242
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleChangeCompletedListenerList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 243
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleChangeCompletedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 244
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleChangeCompletedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleChangeCompletedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 247
    :cond_0
    monitor-exit v1

    .line 248
    return-void

    .line 243
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 247
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addTitleChangedListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangedListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangedListener;

    .prologue
    .line 218
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleChangedListenerList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 219
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleChangedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 220
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleChangedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleChangedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 223
    :cond_0
    monitor-exit v1

    .line 224
    return-void

    .line 219
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 223
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addTitleMessageListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleMessageListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleMessageListener;

    .prologue
    .line 257
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleMessageListenerList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 258
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleMessageListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 259
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleMessageListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleMessageListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 262
    :cond_0
    monitor-exit v1

    .line 263
    return-void

    .line 258
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 262
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public completeTextEntry(Lcom/microsoft/xbox/smartglass/TextAction;)V
    .locals 2
    .param p1, "action"    # Lcom/microsoft/xbox/smartglass/TextAction;

    .prologue
    .line 486
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NATIVE:Ljava/util/concurrent/ExecutorService;

    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/smartglass/TextAction;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 493
    return-void
.end method

.method protected convertConnectionStateToSessionState(Lcom/microsoft/xbox/smartglass/ConnectionState;)I
    .locals 2
    .param p1, "connectionState"    # Lcom/microsoft/xbox/smartglass/ConnectionState;

    .prologue
    .line 571
    const/4 v0, 0x0

    .line 572
    .local v0, "result":I
    sget-object v1, Lcom/microsoft/xbox/smartglass/ConnectionState;->Connecting:Lcom/microsoft/xbox/smartglass/ConnectionState;

    if-ne p1, v1, :cond_1

    .line 573
    const/4 v0, 0x1

    .line 584
    :cond_0
    :goto_0
    return v0

    .line 574
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/smartglass/ConnectionState;->Error:Lcom/microsoft/xbox/smartglass/ConnectionState;

    if-ne p1, v1, :cond_2

    .line 575
    const/4 v0, 0x3

    goto :goto_0

    .line 576
    :cond_2
    sget-object v1, Lcom/microsoft/xbox/smartglass/ConnectionState;->Disconnected:Lcom/microsoft/xbox/smartglass/ConnectionState;

    if-ne p1, v1, :cond_3

    .line 577
    const/4 v0, 0x0

    goto :goto_0

    .line 578
    :cond_3
    sget-object v1, Lcom/microsoft/xbox/smartglass/ConnectionState;->Disconnecting:Lcom/microsoft/xbox/smartglass/ConnectionState;

    if-ne p1, v1, :cond_4

    .line 579
    const/4 v0, 0x4

    goto :goto_0

    .line 580
    :cond_4
    sget-object v1, Lcom/microsoft/xbox/smartglass/ConnectionState;->Connected:Lcom/microsoft/xbox/smartglass/ConnectionState;

    if-ne p1, v1, :cond_0

    .line 582
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public getActiveTitleStates()Ljava/util/Hashtable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Hashtable",
            "<",
            "Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;",
            "Lcom/microsoft/xbox/smartglass/ActiveTitleState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->activeTitleStates:Ljava/util/Hashtable;

    return-object v0
.end method

.method protected getClientInformation()Lcom/microsoft/xbox/smartglass/ClientInformation;
    .locals 2

    .prologue
    .line 555
    new-instance v0, Lcom/microsoft/xbox/smartglass/ClientInformation;

    invoke-direct {v0}, Lcom/microsoft/xbox/smartglass/ClientInformation;-><init>()V

    .line 556
    .local v0, "clientInfo":Lcom/microsoft/xbox/smartglass/ClientInformation;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getVersionCode()I

    move-result v1

    iput v1, v0, Lcom/microsoft/xbox/smartglass/ClientInformation;->version:I

    .line 557
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v1, v0, Lcom/microsoft/xbox/smartglass/ClientInformation;->osMajorVersion:I

    .line 558
    const/4 v1, 0x0

    iput v1, v0, Lcom/microsoft/xbox/smartglass/ClientInformation;->osMinorVersion:I

    .line 559
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->PackageName:Ljava/lang/String;

    iput-object v1, v0, Lcom/microsoft/xbox/smartglass/ClientInformation;->displayName:Ljava/lang/String;

    .line 560
    sget-object v1, Lcom/microsoft/xbox/smartglass/DeviceCapabilities;->SupportsAll:Lcom/microsoft/xbox/smartglass/DeviceCapabilities;

    invoke-static {v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    iput-object v1, v0, Lcom/microsoft/xbox/smartglass/ClientInformation;->capabilities:Ljava/util/EnumSet;

    .line 561
    return-object v0
.end method

.method public getCurrentSessionState()I
    .locals 1

    .prologue
    .line 317
    iget v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->sessionState:I

    return v0
.end method

.method public getLastErrorCode()J
    .locals 2

    .prologue
    .line 321
    iget-wide v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->lastErrorCode:J

    return-wide v0
.end method

.method public getMediaState(I)Lcom/microsoft/xbox/smartglass/MediaState;
    .locals 2
    .param p1, "titleId"    # I

    .prologue
    .line 359
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->currentMediaStates:Ljava/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/MediaState;

    return-object v0
.end method

.method public getPairedIdentityState()Lcom/microsoft/xbox/smartglass/PairedIdentityState;
    .locals 1

    .prologue
    .line 520
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/SessionManager;->getPairedIdentityState()Lcom/microsoft/xbox/smartglass/PairedIdentityState;

    move-result-object v0

    return-object v0
.end method

.method public getSessionId()Ljava/util/UUID;
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->sessionId:Ljava/util/UUID;

    return-object v0
.end method

.method public abstract getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;
.end method

.method protected abstract getSessionManagerListener()Lcom/microsoft/xbox/smartglass/SessionManagerListener;
.end method

.method protected abstract getTag()Ljava/lang/String;
.end method

.method public getTitleIdWithFocus()I
    .locals 6

    .prologue
    .line 331
    const/4 v1, -0x1

    .line 334
    .local v1, "titleId":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getActiveTitleStates()Ljava/util/Hashtable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    .line 335
    .local v0, "ats":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    iget-boolean v4, v0, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->hasFocus:Z

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    sget-object v5, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Fill:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-eq v4, v5, :cond_1

    iget-object v4, v0, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    sget-object v5, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Full:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-eq v4, v5, :cond_1

    iget-object v4, v0, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleLocation:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    sget-object v5, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Snapped:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-ne v4, v5, :cond_0

    .line 336
    :cond_1
    iget v1, v0, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    .line 342
    .end local v0    # "ats":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    :cond_2
    const/4 v3, -0x1

    if-eq v1, v3, :cond_3

    move v2, v1

    .line 355
    .end local v1    # "titleId":I
    .local v2, "titleId":I
    :goto_0
    return v2

    .line 348
    .end local v2    # "titleId":I
    .restart local v1    # "titleId":I
    :cond_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getActiveTitleStates()Ljava/util/Hashtable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/ActiveTitleState;

    .line 349
    .restart local v0    # "ats":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    iget-boolean v4, v0, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->hasFocus:Z

    if-eqz v4, :cond_4

    .line 350
    iget v1, v0, Lcom/microsoft/xbox/smartglass/ActiveTitleState;->titleId:I

    .end local v0    # "ats":Lcom/microsoft/xbox/smartglass/ActiveTitleState;
    :cond_5
    move v2, v1

    .line 355
    .end local v1    # "titleId":I
    .restart local v2    # "titleId":I
    goto :goto_0
.end method

.method public joinSession(Ljava/lang/String;)V
    .locals 6
    .param p1, "consoleIpAddress"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 381
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->lastErrorCode:J

    .line 383
    sget-object v3, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "connect to console "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    :try_start_0
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getMSATicketForConsolePairing()Ljava/lang/String;

    move-result-object v2

    .line 387
    .local v2, "ticket":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 388
    new-instance v1, Lcom/microsoft/xbox/smartglass/AuthInfo;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getCurrentSandboxId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/AuthInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    .local v1, "sgAuthInfo":Lcom/microsoft/xbox/smartglass/AuthInfo;
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTokenManager()Lcom/microsoft/xbox/smartglass/TokenManager;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v1, v4}, Lcom/microsoft/xbox/smartglass/TokenManager;->setAuthInfo(Lcom/microsoft/xbox/smartglass/AuthInfo;Z)V

    .line 393
    .end local v1    # "sgAuthInfo":Lcom/microsoft/xbox/smartglass/AuthInfo;
    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->sessionId:Ljava/util/UUID;

    .line 394
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v3

    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getClientResolution()Lcom/microsoft/xbox/smartglass/ClientResolution;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, p1, v4, v5}, Lcom/microsoft/xbox/smartglass/SessionManager;->connect(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/ClientResolution;Lcom/microsoft/xbox/smartglass/ReconnectPolicy;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 399
    return-void

    .line 395
    .end local v2    # "ticket":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 396
    .local v0, "ex":Ljava/lang/Exception;
    new-instance v3, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v4, 0x7d1

    invoke-direct {v3, v4, v5, v0}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v3
.end method

.method public launchUri(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)Z
    .locals 6
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "location"    # Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    .prologue
    .line 463
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2, p1}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackConsolePlayTo(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v1

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->getValue()I

    move-result v2

    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->fromInt(I)Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/microsoft/xbox/smartglass/SessionManager;->launch(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 469
    :goto_0
    const/4 v1, 0x2

    const/4 v2, 0x0

    const-wide/16 v4, 0x0

    invoke-virtual {p0, v1, v2, v4, v5}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->onRequestCompleted(IIJ)V

    .line 470
    const/4 v1, 0x1

    return v1

    .line 466
    :catch_0
    move-exception v0

    .line 467
    .local v0, "ex":Ljava/lang/Exception;
    const-string v1, "CompanionSession"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed to launchUri with exception "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected notifySessionStateChanged(Lcom/microsoft/xbox/smartglass/ConnectionState;Lcom/microsoft/xbox/smartglass/SGResult;)V
    .locals 2
    .param p1, "state"    # Lcom/microsoft/xbox/smartglass/ConnectionState;
    .param p2, "result"    # Lcom/microsoft/xbox/smartglass/SGResult;

    .prologue
    .line 589
    sget-object v0, Lcom/microsoft/xbox/smartglass/ConnectionState;->Connected:Lcom/microsoft/xbox/smartglass/ConnectionState;

    if-ne p1, v0, :cond_0

    .line 590
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getInstance()Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;->connected:Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->start(Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;)V

    .line 591
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getInstance()Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;->companionConnected:Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->start(Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;)V

    .line 594
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$2;-><init>(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;Lcom/microsoft/xbox/smartglass/ConnectionState;Lcom/microsoft/xbox/smartglass/SGResult;)V

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 637
    return-void
.end method

.method public onRequestCompleted(IIJ)V
    .locals 11
    .param p1, "operation"    # I
    .param p2, "state"    # I
    .param p3, "error"    # J

    .prologue
    .line 438
    iget-object v8, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->datalock:Ljava/lang/Object;

    monitor-enter v8

    .line 439
    :try_start_0
    const-string v1, "CompanionSession"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "OnRequestCompleted: operation %d, state %d, error %d"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v2, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    iget-object v9, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->requestCompleteListenerList:Ljava/util/LinkedList;

    monitor-enter v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 442
    :try_start_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->requestCompleteListenerList:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionRequestCompleteListener;

    .line 443
    .local v0, "listener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionRequestCompleteListener;
    move-object v3, v0

    .line 444
    .local v3, "finalListener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionRequestCompleteListener;
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$1;

    move-object v2, p0

    move v4, p1

    move v5, p2

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$1;-><init>(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionRequestCompleteListener;IIJ)V

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 451
    .end local v0    # "listener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionRequestCompleteListener;
    .end local v3    # "finalListener":Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionRequestCompleteListener;
    :catchall_0
    move-exception v1

    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1

    .line 452
    :catchall_1
    move-exception v1

    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1

    .line 451
    :cond_0
    :try_start_3
    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 452
    :try_start_4
    monitor-exit v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 453
    return-void
.end method

.method public removeChannelEstablishedListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IChannelEstablishedListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IChannelEstablishedListener;

    .prologue
    .line 296
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->channelEstablishedListenerList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 297
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->channelEstablishedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 298
    monitor-exit v1

    .line 299
    return-void

    .line 298
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeCompanionSessionRequestCompleteListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionRequestCompleteListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionRequestCompleteListener;

    .prologue
    .line 281
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->requestCompleteListenerList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 282
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->requestCompleteListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 283
    monitor-exit v1

    .line 284
    return-void

    .line 283
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeCompanionSessionStateListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionStateListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ICompanionSessionStateListener;

    .prologue
    .line 197
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->sessionStateListenerList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 198
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->sessionStateListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 199
    monitor-exit v1

    .line 200
    return-void

    .line 199
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeMediaStateListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IMediaStateListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IMediaStateListener;

    .prologue
    .line 212
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->mediaTitleStateListenerList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 213
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->mediaTitleStateListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 214
    monitor-exit v1

    .line 215
    return-void

    .line 214
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removePrimaryDeviceStateChangedListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IPrimaryDeviceStateChangedListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$IPrimaryDeviceStateChangedListener;

    .prologue
    .line 311
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->primaryDeviceStateChangedListenerList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 312
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->primaryDeviceStateChangedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 313
    monitor-exit v1

    .line 314
    return-void

    .line 313
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeTitleChangeCompletedListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangeCompletedListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangeCompletedListener;

    .prologue
    .line 251
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleChangeCompletedListenerList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 252
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleChangeCompletedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 253
    monitor-exit v1

    .line 254
    return-void

    .line 253
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeTitleChangedListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangedListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleChangedListener;

    .prologue
    .line 236
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleChangedListenerList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 237
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleChangedListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 238
    monitor-exit v1

    .line 239
    return-void

    .line 238
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeTitleMessageListener(Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleMessageListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$ITitleMessageListener;

    .prologue
    .line 266
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleMessageListenerList:Ljava/util/LinkedList;

    monitor-enter v1

    .line 267
    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->titleMessageListenerList:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 268
    monitor-exit v1

    .line 269
    return-void

    .line 268
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public reportSessionDropped(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;)V
    .locals 6
    .param p1, "mode"    # Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;

    .prologue
    .line 407
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getInstance()Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;->companionConnected:Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->stopMetric(Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;)V

    .line 410
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->getInstance()Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;->connected:Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics;->stopMetricAndGetElapsedMs(Lcom/microsoft/xbox/service/network/managers/xblshared/ApplicationMetrics$Metric;)J

    move-result-wide v0

    .line 412
    .local v0, "elapsed":J
    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->sessionId:Ljava/util/UUID;

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    long-to-int v3, v0

    int-to-long v4, v3

    invoke-static {p1, v2, v4, v5}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection;->trackDisconnectSuccess(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;Ljava/lang/String;J)V

    .line 413
    return-void
.end method

.method public sendKey(Lcom/microsoft/xbox/smartglass/GamePadButtons;)V
    .locals 8
    .param p1, "key"    # Lcom/microsoft/xbox/smartglass/GamePadButtons;

    .prologue
    const/4 v2, 0x0

    .line 529
    new-instance v0, Lcom/microsoft/xbox/smartglass/GamePad;

    invoke-static {p1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/xbox/smartglass/GamePad;-><init>(Ljava/util/EnumSet;FFFFFF)V

    .line 530
    .local v0, "gamePad":Lcom/microsoft/xbox/smartglass/GamePad;
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->sendGamePad(Lcom/microsoft/xbox/smartglass/GamePad;Z)V

    .line 531
    return-void
.end method

.method public sendKey(Lcom/microsoft/xbox/smartglass/GamePadButtons;Z)V
    .locals 8
    .param p1, "key"    # Lcom/microsoft/xbox/smartglass/GamePadButtons;
    .param p2, "simulateButtonPress"    # Z

    .prologue
    const/4 v2, 0x0

    .line 534
    new-instance v0, Lcom/microsoft/xbox/smartglass/GamePad;

    invoke-static {p1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/xbox/smartglass/GamePad;-><init>(Ljava/util/EnumSet;FFFFFF)V

    .line 535
    .local v0, "gamePad":Lcom/microsoft/xbox/smartglass/GamePad;
    invoke-direct {p0, v0, p2}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->sendGamePad(Lcom/microsoft/xbox/smartglass/GamePad;Z)V

    .line 536
    return-void
.end method

.method public sendTextInput(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 475
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NATIVE:Ljava/util/concurrent/ExecutorService;

    invoke-static {p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession$$Lambda$1;->lambdaFactory$(Ljava/lang/String;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 482
    return-void
.end method

.method public sendTitleMessage(Lcom/microsoft/xbox/smartglass/MessageTarget;Ljava/lang/String;)V
    .locals 7
    .param p1, "target"    # Lcom/microsoft/xbox/smartglass/MessageTarget;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 548
    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v1

    invoke-virtual {v1, p2, p1}, Lcom/microsoft/xbox/smartglass/SessionManager;->sendTitleMessage(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/MessageTarget;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 552
    :goto_0
    return-void

    .line 549
    :catch_0
    move-exception v0

    .line 550
    .local v0, "ex":Ljava/lang/Exception;
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->TAG:Ljava/lang/String;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "sendTitleMessage(%d, %s) exception: %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p1, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p1, Lcom/microsoft/xbox/smartglass/MessageTarget;->service:Lcom/microsoft/xbox/smartglass/ServiceChannel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/smartglass/ServiceChannel;->name()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public shutdownSession()V
    .locals 1

    .prologue
    .line 403
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;->MANUAL:Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->shutdownSession(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;)V

    .line 404
    return-void
.end method

.method public shutdownSession(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;)V
    .locals 3
    .param p1, "mode"    # Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;

    .prologue
    .line 420
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getCurrentSessionState()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getCurrentSessionState()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 421
    :cond_0
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->reportSessionDropped(Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCConnection$DisconnectionMode;)V

    .line 423
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->TAG:Ljava/lang/String;

    const-string v1, "shutdown session is called"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/SessionManager;->disconnect()V

    .line 431
    :goto_0
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->lastErrorCode:J

    .line 432
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->activeTitleStates:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    .line 433
    return-void

    .line 427
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "session state not connected or connecting, ignore "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getCurrentSessionState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public startChannel(Lcom/microsoft/xbox/smartglass/MessageTarget;I)V
    .locals 7
    .param p1, "target"    # Lcom/microsoft/xbox/smartglass/MessageTarget;
    .param p2, "activityId"    # I

    .prologue
    .line 497
    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/microsoft/xbox/smartglass/SessionManager;->startChannel(Lcom/microsoft/xbox/smartglass/MessageTarget;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 501
    :goto_0
    return-void

    .line 498
    :catch_0
    move-exception v0

    .line 499
    .local v0, "ex":Ljava/lang/Exception;
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->TAG:Ljava/lang/String;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "startChannel(%d) exception: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p1, Lcom/microsoft/xbox/smartglass/MessageTarget;->titleId:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public stopChannel(Lcom/microsoft/xbox/smartglass/MessageTarget;)V
    .locals 4
    .param p1, "target"    # Lcom/microsoft/xbox/smartglass/MessageTarget;

    .prologue
    .line 505
    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/smartglass/SessionManager;->stopChannel(Lcom/microsoft/xbox/smartglass/MessageTarget;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 509
    :goto_0
    return-void

    .line 506
    :catch_0
    move-exception v0

    .line 507
    .local v0, "ex":Ljava/lang/Exception;
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stopChannel error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public unSnap()V
    .locals 4

    .prologue
    .line 513
    :try_start_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->unsnap()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 517
    :goto_0
    return-void

    .line 514
    :catch_0
    move-exception v0

    .line 515
    .local v0, "ex":Ljava/lang/Exception;
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/CompanionSession;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unSnap error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
