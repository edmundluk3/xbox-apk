.class Lcom/microsoft/xbox/smartglass/controls/GamePad;
.super Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;
.source "GamePad.java"


# static fields
.field public static final ComponentName:Ljava/lang/String; = "GamePad"

.field private static final SendGamePadMethod:Ljava/lang/String; = "Send"

.field private static s_gamePadCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    sput v0, Lcom/microsoft/xbox/smartglass/controls/GamePad;->s_gamePadCount:I

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V
    .locals 1
    .param p1, "canvasState"    # Lcom/microsoft/xbox/smartglass/controls/CanvasState;

    .prologue
    .line 19
    const-string v0, "GamePad"

    invoke-direct {p0, v0, p1}, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/smartglass/controls/CanvasState;)V

    .line 21
    const-string v0, "Send"

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/smartglass/controls/GamePad;->registerMethod(Ljava/lang/String;)V

    .line 22
    return-void
.end method

.method private sendGamePad(ILcom/microsoft/xbox/smartglass/controls/JsonGamePad;)V
    .locals 9
    .param p1, "requestId"    # I
    .param p2, "jsonGamePad"    # Lcom/microsoft/xbox/smartglass/controls/JsonGamePad;

    .prologue
    .line 43
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/SessionManager;->getConnectionState()Lcom/microsoft/xbox/smartglass/ConnectionState;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/smartglass/ConnectionState;->Connected:Lcom/microsoft/xbox/smartglass/ConnectionState;

    if-eq v1, v2, :cond_0

    .line 44
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/GamePad;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    const-string v2, "Session is disconnected."

    invoke-interface {v1, p1, v2}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    .line 67
    :goto_0
    return-void

    .line 49
    :cond_0
    :try_start_0
    new-instance v0, Lcom/microsoft/xbox/smartglass/GamePad;

    iget-object v1, p2, Lcom/microsoft/xbox/smartglass/controls/JsonGamePad;->buttons:Ljava/util/EnumSet;

    iget v2, p2, Lcom/microsoft/xbox/smartglass/controls/JsonGamePad;->leftTrigger:F

    iget v3, p2, Lcom/microsoft/xbox/smartglass/controls/JsonGamePad;->rightTrigger:F

    iget v4, p2, Lcom/microsoft/xbox/smartglass/controls/JsonGamePad;->leftThumbstickX:F

    iget v5, p2, Lcom/microsoft/xbox/smartglass/controls/JsonGamePad;->leftThumbstickY:F

    iget v6, p2, Lcom/microsoft/xbox/smartglass/controls/JsonGamePad;->rightThumbstickX:F

    iget v7, p2, Lcom/microsoft/xbox/smartglass/controls/JsonGamePad;->rightThumbstickY:F

    invoke-direct/range {v0 .. v7}, Lcom/microsoft/xbox/smartglass/GamePad;-><init>(Ljava/util/EnumSet;FFFFFF)V

    .line 59
    .local v0, "gamePad":Lcom/microsoft/xbox/smartglass/GamePad;
    sget v1, Lcom/microsoft/xbox/smartglass/controls/GamePad;->s_gamePadCount:I

    add-int/lit8 v2, v1, 0x1

    sput v2, Lcom/microsoft/xbox/smartglass/controls/GamePad;->s_gamePadCount:I

    rem-int/lit8 v1, v1, 0xa

    if-nez v1, :cond_1

    .line 60
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getMetricsManager()Lcom/microsoft/xbox/smartglass/MetricsManager;

    move-result-object v1

    const-string v2, "Send"

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/smartglass/controls/GamePad;->getMetricName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/smartglass/controls/CanvasComponent;->Origin:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/smartglass/MetricsManager;->recordEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getSessionManager()Lcom/microsoft/xbox/smartglass/SessionManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/microsoft/xbox/smartglass/SessionManager;->sendGamePad(Lcom/microsoft/xbox/smartglass/GamePad;Z)V

    .line 63
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/GamePad;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    invoke-interface {v1, p1}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->completeRequest(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 64
    .end local v0    # "gamePad":Lcom/microsoft/xbox/smartglass/GamePad;
    :catch_0
    move-exception v8

    .line 65
    .local v8, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/GamePad;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to send gamepad input. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public invoke(ILjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "requestId"    # I
    .param p2, "methodName"    # Ljava/lang/String;
    .param p3, "json"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/smartglass/controls/GamePad;->validateMethod(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 27
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/smartglass/controls/GamePad;->failRequestUnknownMethod(ILjava/lang/String;)V

    .line 38
    :goto_0
    return-void

    .line 33
    :cond_0
    :try_start_0
    new-instance v1, Lcom/microsoft/xbox/smartglass/controls/JsonGamePad;

    invoke-direct {v1, p3}, Lcom/microsoft/xbox/smartglass/controls/JsonGamePad;-><init>(Ljava/lang/String;)V

    .line 34
    .local v1, "jsonGamePad":Lcom/microsoft/xbox/smartglass/controls/JsonGamePad;
    invoke-direct {p0, p1, v1}, Lcom/microsoft/xbox/smartglass/controls/GamePad;->sendGamePad(ILcom/microsoft/xbox/smartglass/controls/JsonGamePad;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 35
    .end local v1    # "jsonGamePad":Lcom/microsoft/xbox/smartglass/controls/JsonGamePad;
    :catch_0
    move-exception v0

    .line 36
    .local v0, "ex":Lorg/json/JSONException;
    iget-object v2, p0, Lcom/microsoft/xbox/smartglass/controls/GamePad;->_container:Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid JSON. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, p1, v3}, Lcom/microsoft/xbox/smartglass/controls/WebComponentContainer;->failRequest(ILjava/lang/String;)V

    goto :goto_0
.end method
