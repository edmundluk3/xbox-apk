.class public interface abstract Lcom/microsoft/xbox/service/clubs/IClubAccountsService;
.super Ljava/lang/Object;
.source "IClubAccountsService.java"


# virtual methods
.method public abstract changeName(JLjava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    .param p1    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x4L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method

.method public abstract createClub(Ljava/lang/String;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x4L
        .end annotation
    .end param
    .param p2    # Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method

.method public abstract deleteClub(J)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    .param p1    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method

.method public abstract getClubDetails(J)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    .param p1    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method

.method public abstract getClubsByOwner(J)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubsByOwnerResponse;
    .param p1    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method

.method public abstract keepClubForActor(JLcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;)Z
    .param p1    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3    # Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubSuspensionActor;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method

.method public abstract reserveClubName(Ljava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubNameReservationResponse;
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x4L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method

.method public abstract transferOwnership(JLjava/lang/String;)Lcom/microsoft/xbox/service/clubs/ClubAccountsDataTypes$ClubAccountsResponse;
    .param p1    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x1L
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation
.end method
