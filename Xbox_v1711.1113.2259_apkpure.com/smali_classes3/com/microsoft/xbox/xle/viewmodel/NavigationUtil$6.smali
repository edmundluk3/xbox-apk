.class final Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$6;
.super Ljava/lang/Object;
.source "NavigationUtil.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->attachItemListeners(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/xle/app/adapter/PeopleActivityFeedListAdapterUtil$ViewHolder;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$ItemNavigation;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

.field final synthetic val$bi:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

.field final synthetic val$onPreClick:Ljava/lang/Runnable;

.field final synthetic val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;


# direct methods
.method constructor <init>(Ljava/lang/Runnable;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;)V
    .locals 0

    .prologue
    .line 546
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$6;->val$onPreClick:Ljava/lang/Runnable;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$6;->val$bi:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

    iput-object p3, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$6;->val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    iput-object p4, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$6;->val$authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 549
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$6;->val$onPreClick:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->access$000(Ljava/lang/Runnable;)V

    .line 550
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$6;->val$bi:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$FeedItemBiMap;

    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;->ITEM_STATUS_POST:Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->access$100(Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiMap;Lcom/microsoft/xbox/xle/viewmodel/BiEvents$BiPoint;)V

    .line 551
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$6;->val$viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil$6;->val$authorInfo:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/NavigationUtil;->navigateToProfile(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;)V

    .line 552
    return-void
.end method
