.class Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$3;
.super Ljava/lang/Object;
.source "ClubSearchScreenAdapter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/TagArrayAdapter$OnTagSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;

.field final synthetic val$viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;

    .prologue
    .line 193
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$3;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$3;->val$viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isTagSelected(Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)Z
    .locals 1
    .param p1, "socialTag"    # Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 202
    const/4 v0, 0x0

    return v0
.end method

.method public onTagSelected(Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)V
    .locals 1
    .param p1, "socialTag"    # Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 196
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 197
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenAdapter$3;->val$viewModel:Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubSearchScreenViewModel;->selectTag(Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$ISocialTag;)V

    .line 198
    return-void
.end method
