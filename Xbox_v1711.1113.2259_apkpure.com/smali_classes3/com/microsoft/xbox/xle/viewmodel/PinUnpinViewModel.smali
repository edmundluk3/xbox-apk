.class public Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.source "PinUnpinViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;,
        Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private loadPinsTask:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;

.field private loadingPinsState:Lcom/microsoft/xbox/toolkit/network/ListState;

.field private final mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel",
            "<+",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            "+",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private final onPinListener:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;

.field private final pinUnpinButtonId:I

.field private pinUnpinState:Lcom/microsoft/xbox/toolkit/network/ListState;

.field private pinUnpinTask:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 3
    .param p1, "pinUnpinButtonId"    # I

    .prologue
    const/4 v2, 0x0

    .line 47
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;-><init>()V

    .line 40
    invoke-static {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;)Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->onPinListener:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;

    .line 42
    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->loadPinsTask:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;

    .line 43
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->loadingPinsState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 44
    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinTask:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;

    .line 45
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 48
    iput p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinButtonId:I

    .line 49
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;

    iget v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinButtonId:I

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;I)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 50
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 51
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->getModel(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    .line 52
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->onPinsLoaded(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->onPinnedUnpinned(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$402(Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;Lcom/microsoft/xbox/toolkit/network/ListState;)Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/network/ListState;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object p1
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinWithProvider(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)V

    return-void
.end method

.method private cancelAllTasks()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 153
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->loadPinsTask:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->loadPinsTask:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;->cancel()V

    .line 155
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->loadPinsTask:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;

    .line 156
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->loadingPinsState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinTask:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;

    if-eqz v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinTask:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;->cancel()V

    .line 161
    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinTask:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;

    .line 162
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 164
    :cond_1
    return-void
.end method

.method public static ensureMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 2
    .param p0, "mi"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .line 140
    instance-of v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    if-eqz v1, :cond_0

    move-object v0, p0

    .line 142
    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    .line 143
    .local v0, "miTrack":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->getAlbum()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    move-result-object p0

    .line 145
    .end local v0    # "miTrack":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;
    :cond_0
    return-object p0
.end method

.method private findPin(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/pins/PinItem;
    .locals 2
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 168
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 170
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    invoke-static {}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getInstance()Lcom/microsoft/xbox/service/model/pins/PinsModel;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getPin(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/pins/PinItem;

    move-result-object v0

    .line 175
    :goto_0
    return-object v0

    .line 172
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getPackageFamilyName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 173
    invoke-static {}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getInstance()Lcom/microsoft/xbox/service/model/pins/PinsModel;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getPackageFamilyName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getPin(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/pins/PinItem;

    move-result-object v0

    goto :goto_0

    .line 175
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static fromMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)Lcom/microsoft/xbox/service/model/pins/PinItem;
    .locals 6
    .param p0, "mi"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .param p1, "provider"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    .prologue
    const/4 v5, 0x0

    .line 274
    new-instance v3, Lcom/microsoft/xbox/service/model/pins/PinItem;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/model/pins/PinItem;-><init>()V

    .line 275
    .local v3, "pi":Lcom/microsoft/xbox/service/model/pins/PinItem;
    iput-object v5, v3, Lcom/microsoft/xbox/service/model/pins/PinItem;->AltImageUrl:Ljava/lang/String;

    .line 276
    const-string v4, "XboxOne"

    iput-object v4, v3, Lcom/microsoft/xbox/service/model/pins/PinItem;->DeviceType:Ljava/lang/String;

    .line 277
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/microsoft/xbox/service/model/pins/PinItem;->Locale:Ljava/lang/String;

    .line 278
    iput-object v5, v3, Lcom/microsoft/xbox/service/model/pins/PinItem;->SubTitle:Ljava/lang/String;

    .line 279
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/pins/PinItem;->setLocallyAdded(Z)V

    .line 281
    move-object v2, p1

    .line 282
    .local v2, "p":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    if-eqz p0, :cond_1

    .line 283
    instance-of v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    if-eqz v4, :cond_0

    .line 284
    sget-object v4, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->TAG:Ljava/lang/String;

    const-string v5, "converting track to album."

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, p0

    .line 286
    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;

    .line 287
    .local v1, "miTrack":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->getAlbum()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicAlbumMediaItem;

    move-result-object p0

    .line 288
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;->getImageUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setImageUrl(Ljava/lang/String;)V

    .line 289
    if-eqz v2, :cond_0

    .line 291
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;-><init>()V

    .line 292
    .local v0, "ap":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setTitleId(J)V

    .line 293
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getCanonicalId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setCanonicalId(Ljava/lang/String;)V

    .line 294
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setName(Ljava/lang/String;)V

    .line 295
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    iput-object v4, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->ProviderMediaId:Ljava/lang/String;

    .line 296
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getZuneId()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->ProductId:Ljava/lang/String;

    .line 297
    move-object v2, v0

    .line 300
    .end local v0    # "ap":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    .end local v1    # "miTrack":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MusicTrackMediaItem;
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    iput-object v4, v3, Lcom/microsoft/xbox/service/model/pins/PinItem;->ContentType:Ljava/lang/String;

    .line 301
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getImageUrl()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/microsoft/xbox/service/model/pins/PinItem;->ImageUrl:Ljava/lang/String;

    .line 302
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v4

    :goto_0
    iput-object v4, v3, Lcom/microsoft/xbox/service/model/pins/PinItem;->ItemId:Ljava/lang/String;

    .line 303
    const-string v4, "Canonical"

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/pins/PinItem;->setIdType(Ljava/lang/String;)V

    .line 304
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitle()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/microsoft/xbox/service/model/pins/PinItem;->Title:Ljava/lang/String;

    .line 306
    :cond_1
    if-eqz v2, :cond_2

    .line 307
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->titleIdToProvider(J)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/microsoft/xbox/service/model/pins/PinItem;->Provider:Ljava/lang/String;

    .line 308
    iget-object v4, v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->ProviderMediaId:Ljava/lang/String;

    iput-object v4, v3, Lcom/microsoft/xbox/service/model/pins/PinItem;->ProviderId:Ljava/lang/String;

    .line 309
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/pins/PinItem;->setProviderName(Ljava/lang/String;)V

    .line 310
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/microsoft/xbox/service/model/pins/PinItem;->setProviderTitleId(J)V

    .line 313
    :cond_2
    iget-object v4, v3, Lcom/microsoft/xbox/service/model/pins/PinItem;->Provider:Ljava/lang/String;

    if-eqz v4, :cond_3

    iget-object v4, v3, Lcom/microsoft/xbox/service/model/pins/PinItem;->ProviderId:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 314
    const-string v4, "ScopedMediaId"

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/service/model/pins/PinItem;->setIdType(Ljava/lang/String;)V

    .line 317
    :cond_3
    return-object v3

    .line 302
    :cond_4
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getPackageFamilyName()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private hasPins()Z
    .locals 1

    .prologue
    .line 149
    invoke-static {}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getInstance()Lcom/microsoft/xbox/service/model/pins/PinsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->getPins()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private itemHasValidId()Z
    .locals 2

    .prologue
    .line 92
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->ensureMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    .line 93
    .local v0, "selectedMediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getPackageFamilyName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private onPinnedUnpinned(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 199
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPinnedUnpinned: status = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 213
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinTask:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;

    .line 214
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->updateView()V

    .line 215
    return-void

    .line 204
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinTask:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;->isPinning()Z

    move-result v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->isPinned()Z

    move-result v1

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 208
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    .line 209
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 200
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onPinsLoaded(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 180
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPinsLoaded: status = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 194
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->loadPinsTask:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;

    .line 195
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->updateView()V

    .line 196
    return-void

    .line 185
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->hasPins()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->loadingPinsState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1

    .line 189
    :pswitch_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->loadingPinsState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    .line 190
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->loadingPinsState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 181
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private pinWithProvider(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)V
    .locals 4
    .param p1, "provider"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    .prologue
    .line 222
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getActionMenuDialog()Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/dialog/MultiScreenPopupDialog;->dismiss()V

    .line 224
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    .line 225
    .local v0, "selectedMediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinTask:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;

    if-eqz v1, :cond_0

    .line 226
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinTask:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;->cancel()V

    .line 229
    :cond_0
    new-instance v1, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->fromMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)Lcom/microsoft/xbox/service/model/pins/PinItem;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v1, p0, v2, v3}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;Lcom/microsoft/xbox/service/model/pins/PinItem;Z)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinTask:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;

    .line 230
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v1

    const-string v2, "Pin"

    iget-object v3, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinTask:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;->load(Z)V

    .line 232
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->updateView()V

    .line 233
    return-void
.end method

.method private updateView()V
    .locals 0

    .prologue
    .line 218
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->updateAdapter()V

    .line 219
    return-void
.end method


# virtual methods
.method public isBusy()Z
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->loadingPinsState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPinUnpinEnabled()Z
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->isBusy()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->itemHasValidId()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPinned()Z
    .locals 2

    .prologue
    .line 83
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->ensureMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    .line 84
    .local v0, "selectedMediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->findPin(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/pins/PinItem;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->cancelAllTasks()V

    .line 78
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->loadPinsTask:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;

    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->loadPinsTask:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$LoadPinsTask;->load(Z)V

    .line 80
    return-void
.end method

.method public onRehydrate()V
    .locals 2

    .prologue
    .line 56
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;

    iget v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinButtonId:I

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/adapter/PinUnpinScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 57
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->cancelAllTasks()V

    .line 66
    return-void
.end method

.method public pinTapped()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    const/4 v5, 0x0

    .line 97
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinTask:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;

    if-eqz v2, :cond_0

    .line 98
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinTask:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;->cancel()V

    .line 99
    iput-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinTask:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;

    .line 100
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 102
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->ensureMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    .line 103
    .local v1, "selectedMediaItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    if-eqz v1, :cond_3

    .line 104
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->findPin(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/pins/PinItem;

    move-result-object v6

    .line 105
    .local v6, "p":Lcom/microsoft/xbox/service/model/pins/PinItem;
    if-eqz v6, :cond_4

    .line 107
    iget-object v0, v6, Lcom/microsoft/xbox/service/model/pins/PinItem;->DeviceType:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, v6, Lcom/microsoft/xbox/service/model/pins/PinItem;->DeviceType:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 108
    :cond_1
    const-string v0, "XboxOne"

    iput-object v0, v6, Lcom/microsoft/xbox/service/model/pins/PinItem;->DeviceType:Ljava/lang/String;

    .line 110
    :cond_2
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;

    invoke-direct {v0, p0, v6, v5}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;Lcom/microsoft/xbox/service/model/pins/PinItem;Z)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinTask:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;

    .line 111
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v2, "Unpin"

    iget-object v3, v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinUnpinTask:Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;

    invoke-virtual {v0, v5}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel$PinUnpinTask;->load(Z)V

    .line 113
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->updateView()V

    .line 137
    .end local v6    # "p":Lcom/microsoft/xbox/service/model/pins/PinItem;
    :cond_3
    :goto_0
    return-void

    .line 114
    .restart local v6    # "p":Lcom/microsoft/xbox/service/model/pins/PinItem;
    :cond_4
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v2

    const/16 v3, 0x2328

    if-eq v2, v3, :cond_5

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v2

    const/16 v3, 0x2329

    if-ne v2, v3, :cond_7

    .line 115
    :cond_5
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->TAG:Ljava/lang/String;

    const-string v3, "app or game type, just pin"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getProviders()Ljava/util/ArrayList;

    move-result-object v8

    .line 117
    .local v8, "providers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;>;"
    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_6

    :goto_1
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 118
    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    .line 119
    .local v7, "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    invoke-direct {p0, v7}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinWithProvider(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)V

    goto :goto_0

    .end local v7    # "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    :cond_6
    move v0, v5

    .line 117
    goto :goto_1

    .line 121
    .end local v8    # "providers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;>;"
    :cond_7
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v2

    const/16 v3, 0x3f1

    if-eq v2, v3, :cond_8

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v2

    const/16 v3, 0x3ee

    if-ne v2, v3, :cond_a

    .line 122
    :cond_8
    sget-object v2, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->TAG:Ljava/lang/String;

    const-string v3, "music type, just pin"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getProviders()Ljava/util/ArrayList;

    move-result-object v8

    .line 124
    .restart local v8    # "providers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;>;"
    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_9

    :goto_2
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 125
    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    .line 126
    .restart local v7    # "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    invoke-direct {p0, v7}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinWithProvider(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)V

    goto :goto_0

    .end local v7    # "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    :cond_9
    move v0, v5

    .line 124
    goto :goto_2

    .line 127
    .end local v8    # "providers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;>;"
    :cond_a
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v0

    const/16 v2, 0x232c

    if-eq v0, v2, :cond_b

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v0

    const/16 v2, 0x232b

    if-ne v0, v2, :cond_c

    .line 128
    :cond_b
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->TAG:Ljava/lang/String;

    const-string v2, "DLC type, just pin"

    invoke-static {v0, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->pinWithProvider(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)V

    goto :goto_0

    .line 132
    :cond_c
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->TAG:Ljava/lang/String;

    const-string v2, "show provider picker dialog"

    invoke-static {v0, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getProjectSpecificInstance()Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    move-result-object v0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PinUnpinViewModel;->onPinListener:Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;

    const v3, 0x7f070695

    const v4, 0x7f07068c

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showProviderPickerDialogScreen(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/xle/viewmodel/ProviderPickerDialogViewModel$OnProviderClickListener;IIZ)V

    goto/16 :goto_0
.end method
