.class synthetic Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$2;
.super Ljava/lang/Object;
.source "HomeScreenNowPlayingAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$microsoft$xbox$service$model$pins$ContentUtil$HasState:[I

.field static final synthetic $SwitchMap$com$microsoft$xbox$xle$app$adapter$HomeScreenNowPlayingAdapter$FullState:[I

.field static final synthetic $SwitchMap$com$microsoft$xbox$xle$app$adapter$HomeScreenNowPlayingAdapter$RecentsState:[I

.field static final synthetic $SwitchMap$com$microsoft$xbox$xle$app$adapter$HomeScreenNowPlayingAdapter$ViewType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 676
    invoke-static {}, Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;->values()[Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$2;->$SwitchMap$com$microsoft$xbox$service$model$pins$ContentUtil$HasState:[I

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$2;->$SwitchMap$com$microsoft$xbox$service$model$pins$ContentUtil$HasState:[I

    sget-object v1, Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;->HAS_COMPANION:Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    .line 636
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewType;->values()[Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$2;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$HomeScreenNowPlayingAdapter$ViewType:[I

    :try_start_1
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$2;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$HomeScreenNowPlayingAdapter$ViewType:[I

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewType;->RECENT:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_1
    :try_start_2
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$2;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$HomeScreenNowPlayingAdapter$ViewType:[I

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewType;->SNAP:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewType;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    .line 262
    :goto_2
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;->values()[Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$2;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$HomeScreenNowPlayingAdapter$RecentsState:[I

    :try_start_3
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$2;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$HomeScreenNowPlayingAdapter$RecentsState:[I

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;->PROMO:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_3
    :try_start_4
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$2;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$HomeScreenNowPlayingAdapter$RecentsState:[I

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;->RECENTS:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    .line 220
    :goto_4
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->values()[Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$2;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$HomeScreenNowPlayingAdapter$FullState:[I

    :try_start_5
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$2;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$HomeScreenNowPlayingAdapter$FullState:[I

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->APP:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_5
    :try_start_6
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$2;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$HomeScreenNowPlayingAdapter$FullState:[I

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->GAME:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_6
    :try_start_7
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$2;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$HomeScreenNowPlayingAdapter$FullState:[I

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->NO_STATS:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_7
    :try_start_8
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$2;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$HomeScreenNowPlayingAdapter$FullState:[I

    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->PROMO:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_8
    return-void

    :catch_0
    move-exception v0

    goto :goto_8

    :catch_1
    move-exception v0

    goto :goto_7

    :catch_2
    move-exception v0

    goto :goto_6

    :catch_3
    move-exception v0

    goto :goto_5

    .line 262
    :catch_4
    move-exception v0

    goto :goto_4

    :catch_5
    move-exception v0

    goto :goto_3

    .line 636
    :catch_6
    move-exception v0

    goto :goto_2

    :catch_7
    move-exception v0

    goto :goto_1

    .line 676
    :catch_8
    move-exception v0

    goto/16 :goto_0
.end method
