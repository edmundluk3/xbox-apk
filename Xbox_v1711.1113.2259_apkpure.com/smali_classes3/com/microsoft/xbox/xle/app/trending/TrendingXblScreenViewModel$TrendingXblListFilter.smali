.class public final enum Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;
.super Ljava/lang/Enum;
.source "TrendingXblScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TrendingXblListFilter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;",
        ">;",
        "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

.field public static final enum Achievements:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

.field public static final enum Broadcasts:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

.field public static final enum Events:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

.field public static final enum Everything:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

.field public static final enum Gameclips:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

.field public static final enum Games:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

.field public static final enum People:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

.field public static final enum Posts:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

.field public static final enum Screenshots:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

.field public static final enum TextPosts:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

.field public static final enum Topics:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

.field public static final enum Tournaments:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

.field public static final enum Unknown:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;


# instance fields
.field private final resId:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field private final telemetryName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 286
    new-instance v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    const-string v1, "Everything"

    const v2, 0x7f0705b7

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Everything:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    .line 287
    new-instance v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    const-string v1, "Unknown"

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v1, v6, v5, v2}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Unknown:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    .line 288
    new-instance v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    const-string v1, "Gameclips"

    const v2, 0x7f0705b8

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Gameclips:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    .line 289
    new-instance v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    const-string v1, "Screenshots"

    const v2, 0x7f0705b9

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Screenshots:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    .line 290
    new-instance v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    const-string v1, "Broadcasts"

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v1, v9, v5, v2}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Broadcasts:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    .line 291
    new-instance v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    const-string v1, "Posts"

    const/4 v2, 0x5

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v1, v2, v5, v3}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Posts:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    .line 292
    new-instance v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    const-string v1, "Topics"

    const/4 v2, 0x6

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v1, v2, v5, v3}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Topics:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    .line 293
    new-instance v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    const-string v1, "People"

    const/4 v2, 0x7

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v1, v2, v5, v3}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->People:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    .line 294
    new-instance v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    const-string v1, "Events"

    const/16 v2, 0x8

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v1, v2, v5, v3}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Events:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    .line 295
    new-instance v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    const-string v1, "Tournaments"

    const/16 v2, 0x9

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v1, v2, v5, v3}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Tournaments:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    .line 296
    new-instance v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    const-string v1, "Games"

    const/16 v2, 0xa

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v1, v2, v5, v3}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Games:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    .line 297
    new-instance v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    const-string v1, "TextPosts"

    const/16 v2, 0xb

    const v3, 0x7f070cab

    const-string v4, "UNKNOWN"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->TextPosts:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    .line 298
    new-instance v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    const-string v1, "Achievements"

    const/16 v2, 0xc

    const v3, 0x7f070a8c

    const-string v4, "UNKNOWN"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Achievements:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    .line 285
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    sget-object v1, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Everything:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Unknown:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Gameclips:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    aput-object v1, v0, v7

    sget-object v1, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Screenshots:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    aput-object v1, v0, v8

    sget-object v1, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Broadcasts:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Posts:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Topics:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->People:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Events:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Tournaments:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Games:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->TextPosts:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Achievements:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->$VALUES:[Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p3, "resId"    # I
    .param p4, "telemetryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 304
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 305
    invoke-static {p4}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 307
    iput p3, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->resId:I

    .line 308
    iput-object p4, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->telemetryName:Ljava/lang/String;

    .line 309
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 285
    const-class v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;
    .locals 1

    .prologue
    .line 285
    sget-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->$VALUES:[Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    return-object v0
.end method


# virtual methods
.method public getDisplayName()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 314
    iget v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->resId:I

    if-nez v0, :cond_0

    .line 315
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "String not set for TrendingXblListFilter."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 316
    const-string v0, ""

    .line 319
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    iget v1, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->resId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTelemetryName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 324
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->telemetryName:Ljava/lang/String;

    return-object v0
.end method

.method public isEqualToActivityItemType(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;)Z
    .locals 1
    .param p1, "activityItemType"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    .prologue
    .line 328
    sget-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Everything:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    if-eq p0, v0, :cond_6

    sget-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Unknown:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    if-ne p0, v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Unknown:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    if-eq p1, v0, :cond_6

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Gameclips:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    if-ne p0, v0, :cond_1

    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->GameDVR:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    if-eq p1, v0, :cond_6

    :cond_1
    sget-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Screenshots:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    if-ne p0, v0, :cond_2

    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Screenshot:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    if-eq p1, v0, :cond_6

    :cond_2
    sget-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Broadcasts:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    if-ne p0, v0, :cond_3

    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->BroadcastStart:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    if-eq p1, v0, :cond_6

    :cond_3
    sget-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->TextPosts:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    if-ne p0, v0, :cond_4

    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->TextPost:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    if-eq p1, v0, :cond_6

    :cond_4
    sget-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Achievements:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    if-ne p0, v0, :cond_5

    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->Achievement:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    if-eq p1, v0, :cond_6

    :cond_5
    sget-object v0, Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;->Achievements:Lcom/microsoft/xbox/xle/app/trending/TrendingXblScreenViewModel$TrendingXblListFilter;

    if-ne p0, v0, :cond_7

    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->LegacyAchievement:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    if-ne p1, v0, :cond_7

    :cond_6
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_0
.end method
