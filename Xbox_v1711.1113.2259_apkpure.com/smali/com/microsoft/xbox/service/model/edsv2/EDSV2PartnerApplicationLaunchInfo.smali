.class public Lcom/microsoft/xbox/service/model/edsv2/EDSV2PartnerApplicationLaunchInfo;
.super Ljava/lang/Object;
.source "EDSV2PartnerApplicationLaunchInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/edsv2/EDSV2PartnerApplicationLaunchInfo$HexLongJSONDeserializer;
    }
.end annotation


# instance fields
.field private deepLinkInfo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "DeepLinkInfo"
    .end annotation
.end field

.field private launchType:Lcom/microsoft/xbox/service/model/LaunchType;

.field private titleId:J
    .annotation runtime Lcom/google/gson/annotations/JsonAdapter;
        value = Lcom/microsoft/xbox/service/model/edsv2/EDSV2PartnerApplicationLaunchInfo$HexLongJSONDeserializer;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "TitleId"
    .end annotation
.end field

.field private titleType:Lcom/microsoft/xbox/service/model/JTitleType;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    sget-object v0, Lcom/microsoft/xbox/service/model/LaunchType;->AppLaunchType:Lcom/microsoft/xbox/service/model/LaunchType;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2PartnerApplicationLaunchInfo;->launchType:Lcom/microsoft/xbox/service/model/LaunchType;

    .line 24
    sget-object v0, Lcom/microsoft/xbox/service/model/JTitleType;->Application:Lcom/microsoft/xbox/service/model/JTitleType;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2PartnerApplicationLaunchInfo;->titleType:Lcom/microsoft/xbox/service/model/JTitleType;

    return-void
.end method


# virtual methods
.method public getDeepLinkInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2PartnerApplicationLaunchInfo;->deepLinkInfo:Ljava/lang/String;

    return-object v0
.end method

.method public getLaunchType()Lcom/microsoft/xbox/service/model/LaunchType;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2PartnerApplicationLaunchInfo;->launchType:Lcom/microsoft/xbox/service/model/LaunchType;

    return-object v0
.end method

.method public getTitleId()J
    .locals 2

    .prologue
    .line 35
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2PartnerApplicationLaunchInfo;->titleId:J

    return-wide v0
.end method

.method public getTitleType()Lcom/microsoft/xbox/service/model/JTitleType;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2PartnerApplicationLaunchInfo;->titleType:Lcom/microsoft/xbox/service/model/JTitleType;

    return-object v0
.end method

.method public setDeepLinkInfo(Ljava/lang/String;)V
    .locals 0
    .param p1, "link"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2PartnerApplicationLaunchInfo;->deepLinkInfo:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public setLaunchType(Lcom/microsoft/xbox/service/model/LaunchType;)V
    .locals 0
    .param p1, "type"    # Lcom/microsoft/xbox/service/model/LaunchType;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2PartnerApplicationLaunchInfo;->launchType:Lcom/microsoft/xbox/service/model/LaunchType;

    .line 44
    return-void
.end method

.method public setTitleId(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 39
    iput-wide p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2PartnerApplicationLaunchInfo;->titleId:J

    .line 40
    return-void
.end method

.method public setTitleType(Lcom/microsoft/xbox/service/model/JTitleType;)V
    .locals 0
    .param p1, "type"    # Lcom/microsoft/xbox/service/model/JTitleType;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2PartnerApplicationLaunchInfo;->titleType:Lcom/microsoft/xbox/service/model/JTitleType;

    .line 52
    return-void
.end method
