.class public abstract Lcom/microsoft/xbox/data/service/messaging/SkypeError;
.super Ljava/lang/Object;
.source "SkypeError.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/data/service/messaging/SkypeError;",
            ">;"
        }
    .end annotation

    .prologue
    .line 16
    new-instance v0, Lcom/microsoft/xbox/data/service/messaging/AutoValue_SkypeError$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/data/service/messaging/AutoValue_SkypeError$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract errorCode()I
.end method

.method public abstract message()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
