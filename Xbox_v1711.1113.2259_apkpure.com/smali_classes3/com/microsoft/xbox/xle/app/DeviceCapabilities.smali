.class public Lcom/microsoft/xbox/xle/app/DeviceCapabilities;
.super Ljava/lang/Object;
.source "DeviceCapabilities.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/DeviceCapabilities$SensorListener;
    }
.end annotation


# static fields
.field private static instance:Lcom/microsoft/xbox/xle/app/DeviceCapabilities;


# instance fields
.field private sensorListener:Lcom/microsoft/xbox/xle/app/DeviceCapabilities$SensorListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/microsoft/xbox/xle/app/DeviceCapabilities;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/DeviceCapabilities;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/app/DeviceCapabilities;->instance:Lcom/microsoft/xbox/xle/app/DeviceCapabilities;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Lcom/microsoft/xbox/xle/app/DeviceCapabilities$SensorListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/DeviceCapabilities$SensorListener;-><init>(Lcom/microsoft/xbox/xle/app/DeviceCapabilities;Lcom/microsoft/xbox/xle/app/DeviceCapabilities$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/DeviceCapabilities;->sensorListener:Lcom/microsoft/xbox/xle/app/DeviceCapabilities$SensorListener;

    .line 50
    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/xle/app/DeviceCapabilities;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/microsoft/xbox/xle/app/DeviceCapabilities;->instance:Lcom/microsoft/xbox/xle/app/DeviceCapabilities;

    return-object v0
.end method

.method private hasSensorSupport(ILjava/lang/String;)Z
    .locals 9
    .param p1, "sensorType"    # I
    .param p2, "packageFeatureName"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 98
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    sget-object v8, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThread:Ljava/lang/Thread;

    if-ne v5, v8, :cond_0

    move v5, v6

    :goto_0
    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 99
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v8, "sensor"

    invoke-virtual {v5, v8}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/hardware/SensorManager;

    .line 102
    .local v3, "sensorManager":Landroid/hardware/SensorManager;
    invoke-virtual {v3, p1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    .line 103
    .local v2, "sensor":Landroid/hardware/Sensor;
    if-nez v2, :cond_1

    .line 104
    const-string v5, "DeviceCapabilities"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Sensor is not available for this device: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    :goto_1
    return v7

    .end local v2    # "sensor":Landroid/hardware/Sensor;
    .end local v3    # "sensorManager":Landroid/hardware/SensorManager;
    :cond_0
    move v5, v7

    .line 98
    goto :goto_0

    .line 109
    .restart local v2    # "sensor":Landroid/hardware/Sensor;
    .restart local v3    # "sensorManager":Landroid/hardware/SensorManager;
    :cond_1
    sget-object v5, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v5}, Lcom/microsoft/xbox/XLEApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 110
    .local v1, "manager":Landroid/content/pm/PackageManager;
    invoke-virtual {v1, p2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    .line 112
    .local v0, "featureAvail":Z
    if-nez v0, :cond_2

    .line 113
    const-string v5, "DeviceCapabilities"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Feature is not available for this application package: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 118
    :cond_2
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/DeviceCapabilities;->sensorListener:Lcom/microsoft/xbox/xle/app/DeviceCapabilities$SensorListener;

    const/4 v8, 0x2

    invoke-virtual {v3, v5, v2, v8}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v4

    .line 119
    .local v4, "success":Z
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/DeviceCapabilities;->sensorListener:Lcom/microsoft/xbox/xle/app/DeviceCapabilities$SensorListener;

    invoke-virtual {v3, v5}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 121
    if-nez v4, :cond_3

    .line 122
    const-string v5, "DeviceCapabilities"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to register sensor listener: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 125
    :cond_3
    const-string v5, "DeviceCapabilities"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Sensor is available for this device: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    move v7, v6

    .line 126
    goto :goto_1
.end method

.method private updateDeviceCapabilities()V
    .locals 0

    .prologue
    .line 206
    return-void
.end method


# virtual methods
.method public checkDeviceRequirements(I)Z
    .locals 1
    .param p1, "requiredCapabilities"    # I

    .prologue
    .line 63
    const/4 v0, 0x1

    .line 86
    .local v0, "success":Z
    return v0
.end method

.method public hasAccelerometerSupport()Z
    .locals 2

    .prologue
    .line 90
    const/4 v0, 0x1

    const-string v1, "android.hardware.sensor.accelerometer"

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/DeviceCapabilities;->hasSensorSupport(ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasGyroscopeSupport()Z
    .locals 2

    .prologue
    .line 94
    const/4 v0, 0x4

    const-string v1, "android.hardware.sensor.gyroscope"

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/xle/app/DeviceCapabilities;->hasSensorSupport(ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasHapticSupport()Z
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 162
    :try_start_0
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v7, "vibrator"

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Vibrator;

    .line 164
    .local v3, "vibratorService":Landroid/os/Vibrator;
    invoke-virtual {v3}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v6

    if-nez v6, :cond_0

    .line 165
    const-string v5, "DeviceCapabilities"

    const-string v6, "Device can\'t vibrate."

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    .end local v3    # "vibratorService":Landroid/os/Vibrator;
    :goto_0
    return v4

    .line 170
    .restart local v3    # "vibratorService":Landroid/os/Vibrator;
    :cond_0
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v6}, Lcom/microsoft/xbox/XLEApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "haptic_feedback_enabled"

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 171
    .local v2, "systemHapticSetting":I
    if-ne v2, v5, :cond_1

    move v1, v5

    .line 172
    .local v1, "hapticEnabled":Z
    :goto_1
    if-nez v1, :cond_2

    .line 173
    const-string v5, "DeviceCapabilities"

    const-string v6, "Haptic feature is not available for this device."

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 179
    .end local v1    # "hapticEnabled":Z
    .end local v2    # "systemHapticSetting":I
    .end local v3    # "vibratorService":Landroid/os/Vibrator;
    :catch_0
    move-exception v0

    .line 181
    .local v0, "ex":Ljava/lang/Exception;
    const-string v5, "DeviceCapabilities"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error occurred while testing for haptic support: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v2    # "systemHapticSetting":I
    .restart local v3    # "vibratorService":Landroid/os/Vibrator;
    :cond_1
    move v1, v4

    .line 171
    goto :goto_1

    .line 176
    .restart local v1    # "hapticEnabled":Z
    :cond_2
    :try_start_1
    const-string v6, "DeviceCapabilities"

    const-string v7, "Haptic feature is available for this device."

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move v4, v5

    .line 177
    goto :goto_0
.end method

.method public hasLocationSupport()Z
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 133
    :try_start_0
    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v7}, Lcom/microsoft/xbox/XLEApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 134
    .local v4, "manager":Landroid/content/pm/PackageManager;
    const-string v7, "android.hardware.location"

    invoke-virtual {v4, v7}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    .line 136
    .local v2, "featureAvail":Z
    if-nez v2, :cond_0

    .line 137
    const-string v6, "DeviceCapabilities"

    const-string v7, "Location feature is not available for this application package."

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    .end local v2    # "featureAvail":Z
    .end local v4    # "manager":Landroid/content/pm/PackageManager;
    :goto_0
    return v5

    .line 142
    .restart local v2    # "featureAvail":Z
    .restart local v4    # "manager":Landroid/content/pm/PackageManager;
    :cond_0
    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    const-string v8, "location"

    invoke-virtual {v7, v8}, Lcom/microsoft/xbox/XLEApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/location/LocationManager;

    .line 143
    .local v3, "locationManager":Landroid/location/LocationManager;
    const-string v7, "gps"

    invoke-virtual {v3, v7}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    const-string v7, "network"

    invoke-virtual {v3, v7}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    :cond_1
    move v0, v6

    .line 145
    .local v0, "enabled":Z
    :goto_1
    if-nez v0, :cond_3

    .line 146
    const-string v6, "DeviceCapabilities"

    const-string v7, "Location service is not enabled."

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 152
    .end local v0    # "enabled":Z
    .end local v2    # "featureAvail":Z
    .end local v3    # "locationManager":Landroid/location/LocationManager;
    .end local v4    # "manager":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v1

    .line 154
    .local v1, "ex":Ljava/lang/Exception;
    const-string v6, "DeviceCapabilities"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error occurred while testing for location support: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .end local v1    # "ex":Ljava/lang/Exception;
    .restart local v2    # "featureAvail":Z
    .restart local v3    # "locationManager":Landroid/location/LocationManager;
    .restart local v4    # "manager":Landroid/content/pm/PackageManager;
    :cond_2
    move v0, v5

    .line 143
    goto :goto_1

    .line 149
    .restart local v0    # "enabled":Z
    :cond_3
    :try_start_1
    const-string v7, "DeviceCapabilities"

    const-string v8, "Location service is available for this device."

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move v5, v6

    .line 150
    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/DeviceCapabilities;->updateDeviceCapabilities()V

    .line 60
    return-void
.end method
