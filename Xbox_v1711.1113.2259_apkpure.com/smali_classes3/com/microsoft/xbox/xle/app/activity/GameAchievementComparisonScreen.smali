.class public Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "GameAchievementComparisonScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen$GameAchievementComparisonAdapterProvider;
    }
.end annotation


# instance fields
.field protected adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    const-string v0, "Compare Achievements"

    return-object v0
.end method

.method public getRelativeId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    .line 39
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
    if-eqz v0, :cond_0

    .line 40
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->getRelativeId()Ljava/lang/String;

    move-result-object v1

    .line 43
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getRelativeIdKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    const-string v0, "TitleId"

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 18
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 20
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen;->onCreateContentView()V

    .line 22
    new-instance v1, Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen$GameAchievementComparisonAdapterProvider;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen$GameAchievementComparisonAdapterProvider;-><init>(Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen;Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen$1;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    .line 23
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen;->adapterProvider:Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;-><init>(Lcom/microsoft/xbox/xle/app/adapter/IAdapterProvider;)V

    .line 24
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->setAsPivotPane()V

    .line 25
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameAchievementComparisonViewModel;->onCreate()V

    .line 26
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 27
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 53
    const v0, 0x7f03010e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/activity/GameAchievementComparisonScreen;->setContentView(I)V

    .line 54
    return-void
.end method
