.class final synthetic Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$2;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/BiFunction;


# static fields
.field private static final instance:Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$2;

    invoke-direct {v0}, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$2;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$2;->instance:Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$2;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static lambdaFactory$()Lio/reactivex/functions/BiFunction;
    .locals 1

    sget-object v0, Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$2;->instance:Lcom/microsoft/xbox/data/repository/clubs/ClubWatchRepositoryImpl$$Lambda$2;

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    check-cast p2, Lcom/google/common/collect/ImmutableList;

    invoke-static {p1, p2}, Landroid/support/v4/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/support/v4/util/Pair;

    move-result-object v0

    return-object v0
.end method
