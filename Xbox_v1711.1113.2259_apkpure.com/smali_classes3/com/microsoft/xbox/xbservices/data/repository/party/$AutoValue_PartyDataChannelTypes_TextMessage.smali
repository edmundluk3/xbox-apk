.class abstract Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_TextMessage;
.super Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$TextMessage;
.source "$AutoValue_PartyDataChannelTypes_TextMessage.java"


# instance fields
.field private final data:Ljava/lang/String;

.field private final from:I

.field private final type:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 2
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "from"    # I
    .param p3, "data"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$TextMessage;-><init>()V

    .line 18
    if-nez p1, :cond_0

    .line 19
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null type"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 21
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_TextMessage;->type:Ljava/lang/String;

    .line 22
    iput p2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_TextMessage;->from:I

    .line 23
    if-nez p3, :cond_1

    .line 24
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null data"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_1
    iput-object p3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_TextMessage;->data:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method public data()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_TextMessage;->data:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 57
    if-ne p1, p0, :cond_1

    .line 66
    :cond_0
    :goto_0
    return v1

    .line 60
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$TextMessage;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 61
    check-cast v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$TextMessage;

    .line 62
    .local v0, "that":Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$TextMessage;
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_TextMessage;->type:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$TextMessage;->type()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_TextMessage;->from:I

    .line 63
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$TextMessage;->from()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_TextMessage;->data:Ljava/lang/String;

    .line 64
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$TextMessage;->data()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xbservices/data/repository/party/PartyDataChannelTypes$TextMessage;
    :cond_3
    move v1, v2

    .line 66
    goto :goto_0
.end method

.method public from()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_TextMessage;->from:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 71
    const/4 v0, 0x1

    .line 72
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 73
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_TextMessage;->type:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 74
    mul-int/2addr v0, v2

    .line 75
    iget v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_TextMessage;->from:I

    xor-int/2addr v0, v1

    .line 76
    mul-int/2addr v0, v2

    .line 77
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_TextMessage;->data:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 78
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TextMessage{type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_TextMessage;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", from="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_TextMessage;->from:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_TextMessage;->data:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public type()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyDataChannelTypes_TextMessage;->type:Ljava/lang/String;

    return-object v0
.end method
