.class public abstract Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;
.super Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;
.source "HomeScreenNowPlayingAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;,
        Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;,
        Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewType;,
        Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$SnapImageListener;,
        Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ImageListener;,
        Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;
    }
.end annotation


# static fields
.field private static final RECENT_CELLS:I = 0x4

.field protected static final TAG:Ljava/lang/String;


# instance fields
.field private final adp:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;

.field protected final adpView:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

.field private final batImageAppOrNoStatListener:Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListener;

.field protected final cellListener:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerRecents;

.field protected final pinsViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

.field private final postAnimationJobs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field protected final root:Landroid/view/View;

.field private final spRecents:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field protected final viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V
    .locals 3
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;
    .param p2, "pinsViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .prologue
    .line 73
    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getContainerResourceId()I

    move-result v1

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getPosition()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;-><init>(II)V

    .line 54
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->postAnimationJobs:Ljava/util/List;

    .line 714
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->batImageAppOrNoStatListener:Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListener;

    .line 74
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    .line 75
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->pinsViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .line 76
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->section:Landroid/view/View;

    const v2, 0x7f0e06fe

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->root:Landroid/view/View;

    .line 77
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->section:Landroid/view/View;

    const v2, 0x7f0e0701

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->spRecents:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 78
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->section:Landroid/view/View;

    const v2, 0x7f0e0702

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->adpView:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    .line 81
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->adpView:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    iget v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->cellMargin:I

    mul-int/lit8 v2, v2, 0x2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setItemMargin(I)V

    .line 82
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->container:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 83
    .local v0, "ctx":Landroid/content/Context;
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;-><init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->adp:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;

    .line 84
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->adpView:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->adp:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 85
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerRecents;

    invoke-direct {v1, p2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerRecents;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->cellListener:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerRecents;

    .line 86
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;Landroid/view/View;Landroid/view/View;)V
    .locals 3
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;
    .param p2, "pinsViewModel"    # Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;
    .param p3, "container"    # Landroid/view/View;
    .param p4, "section"    # Landroid/view/View;

    .prologue
    .line 57
    invoke-direct {p0, p3, p4}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;-><init>(Landroid/view/View;Landroid/view/View;)V

    .line 54
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->postAnimationJobs:Ljava/util/List;

    .line 714
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->batImageAppOrNoStatListener:Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListener;

    .line 58
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    .line 59
    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->pinsViewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;

    .line 60
    const v1, 0x7f0e06fe

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->root:Landroid/view/View;

    .line 61
    const v1, 0x7f0e0701

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->spRecents:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 62
    const v1, 0x7f0e0702

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->adpView:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    .line 65
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->adpView:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    iget v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->cellMargin:I

    mul-int/lit8 v2, v2, 0x2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setItemMargin(I)V

    .line 66
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 67
    .local v0, "ctx":Landroid/content/Context;
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;-><init>(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->adp:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;

    .line 68
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->adpView:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->adp:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 69
    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerRecents;

    invoke-direct {v1, p2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerRecents;-><init>(Lcom/microsoft/xbox/xle/viewmodel/HomeScreenPinsViewModel;)V

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->cellListener:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerRecents;

    .line 70
    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->getFullContainer()Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method protected static adjustHeightForBitmapAR(Landroid/view/View;FILandroid/graphics/Bitmap;)I
    .locals 3
    .param p0, "container"    # Landroid/view/View;
    .param p1, "ratio"    # F
    .param p2, "height"    # I
    .param p3, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 343
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float v0, p1, v1

    .line 344
    .local v0, "width":F
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    return v1
.end method

.method private bindAllViews()V
    .locals 3

    .prologue
    .line 283
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->getFullState()Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    move-result-object v0

    .line 284
    .local v0, "fullState":Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;
    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$2;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$HomeScreenNowPlayingAdapter$FullState:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 297
    :goto_0
    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$2;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$HomeScreenNowPlayingAdapter$RecentsState:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->getRecentsState()Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    .line 304
    :goto_1
    :pswitch_0
    return-void

    .line 286
    :pswitch_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->bindBatApp()V

    goto :goto_0

    .line 289
    :pswitch_2
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->bindBatGame()V

    goto :goto_0

    .line 292
    :pswitch_3
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->bindBatNoStats()V

    goto :goto_0

    .line 301
    :pswitch_4
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->bindRecentsRecents()V

    goto :goto_1

    .line 284
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 297
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private bindBatApp()V
    .locals 3

    .prologue
    .line 153
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getNPState()Lcom/microsoft/xbox/xle/viewmodel/NPState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->getLiBat()Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    move-result-object v1

    .line 154
    .local v1, "liBatItem":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    if-eqz v1, :cond_0

    .line 155
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->getBatView()Landroid/view/View;

    move-result-object v0

    .line 156
    .local v0, "container":Landroid/view/View;
    invoke-interface {v1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->isNPM()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 157
    invoke-interface {v1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getNPM()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->bindBatAppNPM(Landroid/view/View;Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V

    .line 158
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    .end local v0    # "container":Landroid/view/View;
    :cond_0
    :goto_0
    return-void

    .line 160
    .restart local v0    # "container":Landroid/view/View;
    :cond_1
    invoke-interface {v1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getRecent()Lcom/microsoft/xbox/service/model/recents/Recent;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->bindBatAppRecent(Landroid/view/View;Lcom/microsoft/xbox/service/model/recents/Recent;)V

    goto :goto_0
.end method

.method private bindBatAppNPM(Landroid/view/View;Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V
    .locals 8
    .param p1, "container"    # Landroid/view/View;
    .param p2, "npm"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .prologue
    .line 354
    const v6, 0x7f0e06e8

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 356
    .local v3, "img":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v6, p2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getImageUri(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Ljava/lang/String;

    move-result-object v5

    .line 357
    .local v5, "uri":Ljava/lang/String;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v6, p2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getBATDefaultImageRid(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)I

    move-result v1

    .line 358
    .local v1, "defaultRid":I
    const v6, 0x7f0e001d

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->batImageAppOrNoStatListener:Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListener;

    invoke-virtual {v3, v6, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setTag(ILjava/lang/Object;)V

    .line 359
    invoke-virtual {v3, v5, v1, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 362
    if-nez v5, :cond_0

    const/4 v6, 0x4

    :goto_0
    invoke-virtual {v3, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 364
    const v6, 0x7f0e06ea

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 365
    .local v4, "title":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v6, p2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getHeader(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 367
    const v6, 0x7f0e06eb

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 368
    .local v2, "heroStat":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getHeroStat()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 370
    const v6, 0x7f0e06ee

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 371
    .local v0, "achievements":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getAchievementsEarned()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 373
    const v6, 0x7f0e06ef

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    invoke-static {p2, v6}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->bindLaunchableItem(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Landroid/widget/TextView;)V

    .line 374
    return-void

    .line 362
    .end local v0    # "achievements":Landroid/widget/TextView;
    .end local v2    # "heroStat":Landroid/widget/TextView;
    .end local v4    # "title":Landroid/widget/TextView;
    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private bindBatAppRecent(Landroid/view/View;Lcom/microsoft/xbox/service/model/recents/Recent;)V
    .locals 9
    .param p1, "container"    # Landroid/view/View;
    .param p2, "recent"    # Lcom/microsoft/xbox/service/model/recents/Recent;

    .prologue
    .line 378
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 380
    const v7, 0x7f0e001b

    invoke-virtual {p1, v7, p2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 381
    const v7, 0x7f0e0069

    const-string v8, "Home BAT"

    invoke-virtual {p1, v7, v8}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 382
    const v7, 0x7f0e0018

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->hasStats()Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {p1, v7, v8}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 383
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->cellListener:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerRecents;

    invoke-virtual {p1, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 385
    const v7, 0x7f0e06e8

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 387
    .local v3, "img":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    iget-object v7, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->toMediaItem(Lcom/microsoft/xbox/service/model/recents/RecentItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v4

    .line 388
    .local v4, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v7, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v6, v7, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ImageUrl:Ljava/lang/String;

    .line 389
    .local v6, "uri":Ljava/lang/String;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v7

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v1

    .line 390
    .local v1, "defaultRid":I
    :goto_0
    const v7, 0x7f0e001d

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->batImageAppOrNoStatListener:Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListener;

    invoke-virtual {v3, v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setTag(ILjava/lang/Object;)V

    .line 391
    invoke-virtual {v3, v6, v1, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 394
    if-nez v6, :cond_1

    const/4 v7, 0x4

    :goto_1
    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 396
    const v7, 0x7f0e06ea

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 397
    .local v5, "title":Landroid/widget/TextView;
    iget-object v7, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v7, v7, Lcom/microsoft/xbox/service/model/recents/RecentItem;->Title:Ljava/lang/String;

    invoke-static {v5, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 399
    const v7, 0x7f0e06eb

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 400
    .local v2, "heroStat":Landroid/widget/TextView;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getHeroStat()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 402
    const v7, 0x7f0e06ee

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 403
    .local v0, "achievements":Landroid/widget/TextView;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getAchievementsEarned()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-static {v0, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 405
    iget-object v8, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    const v7, 0x7f0e06ef

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    invoke-static {v8, v7}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->bindLaunchableItem(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Landroid/widget/TextView;)V

    .line 406
    return-void

    .line 389
    .end local v0    # "achievements":Landroid/widget/TextView;
    .end local v1    # "defaultRid":I
    .end local v2    # "heroStat":Landroid/widget/TextView;
    .end local v5    # "title":Landroid/widget/TextView;
    :cond_0
    sget v1, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    goto :goto_0

    .line 394
    .restart local v1    # "defaultRid":I
    :cond_1
    const/4 v7, 0x0

    goto :goto_1
.end method

.method private bindBatGame()V
    .locals 3

    .prologue
    .line 166
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getNPState()Lcom/microsoft/xbox/xle/viewmodel/NPState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->getLiBat()Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    move-result-object v1

    .line 167
    .local v1, "liBatItem":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    if-eqz v1, :cond_0

    .line 168
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->getBatView()Landroid/view/View;

    move-result-object v0

    .line 169
    .local v0, "container":Landroid/view/View;
    invoke-interface {v1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->isNPM()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 170
    invoke-interface {v1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getNPM()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->bindBatGameNPM(Landroid/view/View;Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V

    .line 171
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 176
    .end local v0    # "container":Landroid/view/View;
    :cond_0
    :goto_0
    return-void

    .line 173
    .restart local v0    # "container":Landroid/view/View;
    :cond_1
    invoke-interface {v1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getRecent()Lcom/microsoft/xbox/service/model/recents/Recent;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->bindBatGameRecent(Landroid/view/View;Lcom/microsoft/xbox/service/model/recents/Recent;)V

    goto :goto_0
.end method

.method private bindBatGameNPM(Landroid/view/View;Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V
    .locals 9
    .param p1, "container"    # Landroid/view/View;
    .param p2, "npm"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .prologue
    const/4 v8, 0x0

    .line 409
    const v7, 0x7f0e06e8

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 411
    .local v3, "img":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getBatGameUri()Ljava/lang/String;

    move-result-object v6

    .line 412
    .local v6, "uri":Ljava/lang/String;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v7, p2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getBATDefaultImageRid(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)I

    move-result v1

    .line 413
    .local v1, "defaultRid":I
    invoke-virtual {v3, v6, v1, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 416
    if-nez v6, :cond_0

    const/4 v7, 0x4

    :goto_0
    invoke-virtual {v3, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 418
    const v7, 0x7f0e06ea

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 419
    .local v5, "title":Landroid/widget/TextView;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v7, p2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getHeader(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 421
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->hasStats()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 422
    const/4 v7, 0x1

    invoke-direct {p0, v7}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->setStatsControlsVisible(Z)V

    .line 424
    const v7, 0x7f0e06eb

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 425
    .local v2, "heroStat":Landroid/widget/TextView;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getHeroStat()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 427
    const v7, 0x7f0e06f0

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/xle/ui/LabeledArcView;

    .line 428
    .local v4, "progressWheel":Lcom/microsoft/xbox/xle/ui/LabeledArcView;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getProgressPercentage()I

    move-result v7

    invoke-virtual {v4, v7}, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->setPercentage(I)V

    .line 430
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->bindGamerScore(Landroid/view/View;)V

    .line 432
    const v7, 0x7f0e06ee

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 433
    .local v0, "achievements":Landroid/widget/TextView;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getAchievementsEarned()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-static {v0, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 438
    .end local v0    # "achievements":Landroid/widget/TextView;
    .end local v2    # "heroStat":Landroid/widget/TextView;
    .end local v4    # "progressWheel":Lcom/microsoft/xbox/xle/ui/LabeledArcView;
    :goto_1
    const v7, 0x7f0e06ef

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    invoke-static {p2, v7}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->bindLaunchableItem(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Landroid/widget/TextView;)V

    .line 439
    return-void

    .end local v5    # "title":Landroid/widget/TextView;
    :cond_0
    move v7, v8

    .line 416
    goto :goto_0

    .line 435
    .restart local v5    # "title":Landroid/widget/TextView;
    :cond_1
    invoke-direct {p0, v8}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->setStatsControlsVisible(Z)V

    goto :goto_1
.end method

.method private bindBatGameRecent(Landroid/view/View;Lcom/microsoft/xbox/service/model/recents/Recent;)V
    .locals 11
    .param p1, "container"    # Landroid/view/View;
    .param p2, "recent"    # Lcom/microsoft/xbox/service/model/recents/Recent;

    .prologue
    const/4 v9, 0x0

    .line 443
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 445
    const v8, 0x7f0e001b

    invoke-virtual {p1, v8, p2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 446
    const v8, 0x7f0e0069

    const-string v10, "Home BAT"

    invoke-virtual {p1, v8, v10}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 447
    const v8, 0x7f0e0018

    iget-object v10, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v10}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->hasStats()Z

    move-result v10

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {p1, v8, v10}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 448
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->cellListener:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerRecents;

    invoke-virtual {p1, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 450
    const v8, 0x7f0e06e8

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 452
    .local v3, "img":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    iget-object v8, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-static {v8}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->toMediaItem(Lcom/microsoft/xbox/service/model/recents/RecentItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v4

    .line 453
    .local v4, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getBatGameUri()Ljava/lang/String;

    move-result-object v7

    .line 454
    .local v7, "uri":Ljava/lang/String;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v8

    invoke-static {v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v1

    .line 455
    .local v1, "defaultRid":I
    :goto_0
    invoke-virtual {v3, v7, v1, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 458
    if-nez v7, :cond_1

    const/4 v8, 0x4

    :goto_1
    invoke-virtual {v3, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 460
    const v8, 0x7f0e06ea

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 461
    .local v6, "title":Landroid/widget/TextView;
    iget-object v8, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v8, v8, Lcom/microsoft/xbox/service/model/recents/RecentItem;->Title:Ljava/lang/String;

    invoke-static {v6, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 463
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->hasStats()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 464
    const/4 v8, 0x1

    invoke-direct {p0, v8}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->setStatsControlsVisible(Z)V

    .line 466
    const v8, 0x7f0e06eb

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 467
    .local v2, "heroStat":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getHeroStat()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 469
    const v8, 0x7f0e06f0

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/xle/ui/LabeledArcView;

    .line 470
    .local v5, "progressWheel":Lcom/microsoft/xbox/xle/ui/LabeledArcView;
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getProgressPercentage()I

    move-result v8

    invoke-virtual {v5, v8}, Lcom/microsoft/xbox/xle/ui/LabeledArcView;->setPercentage(I)V

    .line 472
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->bindGamerScore(Landroid/view/View;)V

    .line 474
    const v8, 0x7f0e06ee

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 475
    .local v0, "achievements":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getAchievementsEarned()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-static {v0, v8}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 480
    .end local v0    # "achievements":Landroid/widget/TextView;
    .end local v2    # "heroStat":Landroid/widget/TextView;
    .end local v5    # "progressWheel":Lcom/microsoft/xbox/xle/ui/LabeledArcView;
    :goto_2
    iget-object v9, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    const v8, 0x7f0e06ef

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    invoke-static {v9, v8}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->bindLaunchableItem(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Landroid/widget/TextView;)V

    .line 481
    return-void

    .line 454
    .end local v1    # "defaultRid":I
    .end local v6    # "title":Landroid/widget/TextView;
    :cond_0
    sget v1, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    goto :goto_0

    .restart local v1    # "defaultRid":I
    :cond_1
    move v8, v9

    .line 458
    goto :goto_1

    .line 477
    .restart local v6    # "title":Landroid/widget/TextView;
    :cond_2
    invoke-direct {p0, v9}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->setStatsControlsVisible(Z)V

    goto :goto_2
.end method

.method private bindBatNoStats()V
    .locals 3

    .prologue
    .line 179
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getNPState()Lcom/microsoft/xbox/xle/viewmodel/NPState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->getLiBat()Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    move-result-object v1

    .line 180
    .local v1, "liBatItem":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;
    if-eqz v1, :cond_0

    .line 181
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->getBatView()Landroid/view/View;

    move-result-object v0

    .line 182
    .local v0, "container":Landroid/view/View;
    invoke-interface {v1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->isNPM()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 183
    invoke-interface {v1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getNPM()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->bindBatNoStatsNPM(Landroid/view/View;Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V

    .line 184
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    .end local v0    # "container":Landroid/view/View;
    :cond_0
    :goto_0
    return-void

    .line 186
    .restart local v0    # "container":Landroid/view/View;
    :cond_1
    invoke-interface {v1}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getRecent()Lcom/microsoft/xbox/service/model/recents/Recent;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->bindBatNoStatsRecent(Landroid/view/View;Lcom/microsoft/xbox/service/model/recents/Recent;)V

    goto :goto_0
.end method

.method private bindBatNoStatsNPM(Landroid/view/View;Lcom/microsoft/xbox/xle/model/NowPlayingModel;)V
    .locals 13
    .param p1, "container"    # Landroid/view/View;
    .param p2, "npm"    # Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    .prologue
    const/16 v12, 0x8

    const/4 v7, 0x0

    .line 506
    const v6, 0x7f0e06e8

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 508
    .local v1, "img":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v6, p2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getImageUri(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Ljava/lang/String;

    move-result-object v5

    .line 509
    .local v5, "uri":Ljava/lang/String;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v6, p2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getBATDefaultImageRid(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)I

    move-result v0

    .line 510
    .local v0, "defaultRid":I
    const v6, 0x7f0e001d

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->batImageAppOrNoStatListener:Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListener;

    invoke-virtual {v1, v6, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setTag(ILjava/lang/Object;)V

    .line 511
    invoke-virtual {v1, v5, v0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 514
    if-nez v5, :cond_1

    const/4 v6, 0x4

    :goto_0
    invoke-virtual {v1, v6}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 516
    const v6, 0x7f0e06ea

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 517
    .local v4, "title":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v6, p2}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getHeader(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 519
    const v6, 0x7f0e06f7

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 520
    .local v3, "oneGuideIcon":Landroid/widget/TextView;
    const v6, 0x7f0e06f8

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 521
    .local v2, "newFeatureText":Landroid/widget/TextView;
    invoke-virtual {p2}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getProviderTitleId()J

    move-result-wide v8

    const-wide/32 v10, 0x162615ad

    cmp-long v6, v8, v10

    if-nez v6, :cond_2

    .line 522
    invoke-static {v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 523
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v6}, Lcom/microsoft/xbox/XLEApplication;->shouldShowStreamingButton()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 524
    invoke-static {v2, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 531
    :cond_0
    :goto_1
    const v6, 0x7f0e06ef

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    invoke-static {p2, v6}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->bindLaunchableItem(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Landroid/widget/TextView;)V

    .line 532
    return-void

    .end local v2    # "newFeatureText":Landroid/widget/TextView;
    .end local v3    # "oneGuideIcon":Landroid/widget/TextView;
    .end local v4    # "title":Landroid/widget/TextView;
    :cond_1
    move v6, v7

    .line 514
    goto :goto_0

    .line 527
    .restart local v2    # "newFeatureText":Landroid/widget/TextView;
    .restart local v3    # "oneGuideIcon":Landroid/widget/TextView;
    .restart local v4    # "title":Landroid/widget/TextView;
    :cond_2
    invoke-static {v3, v12}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 528
    invoke-static {v2, v12}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_1
.end method

.method private bindBatNoStatsRecent(Landroid/view/View;Lcom/microsoft/xbox/service/model/recents/Recent;)V
    .locals 12
    .param p1, "container"    # Landroid/view/View;
    .param p2, "recent"    # Lcom/microsoft/xbox/service/model/recents/Recent;

    .prologue
    .line 536
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 538
    const v7, 0x7f0e001b

    invoke-virtual {p1, v7, p2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 539
    const v7, 0x7f0e0069

    const-string v8, "Home BAT"

    invoke-virtual {p1, v7, v8}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 540
    const v7, 0x7f0e0018

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->hasStats()Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {p1, v7, v8}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 541
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->cellListener:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapterUtil$CellListenerRecents;

    invoke-virtual {p1, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 543
    const v7, 0x7f0e06e8

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;

    .line 545
    .local v1, "img":Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;
    iget-object v7, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-static {v7}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->toMediaItem(Lcom/microsoft/xbox/service/model/recents/RecentItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v2

    .line 546
    .local v2, "mi":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iget-object v7, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v6, v7, Lcom/microsoft/xbox/service/model/recents/RecentItem;->ImageUrl:Ljava/lang/String;

    .line 547
    .local v6, "uri":Ljava/lang/String;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v7

    invoke-static {v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->getMediaItemDefaultRid(I)I

    move-result v0

    .line 548
    .local v0, "defaultRid":I
    :goto_0
    const v7, 0x7f0e001d

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->batImageAppOrNoStatListener:Lcom/microsoft/xbox/toolkit/ui/OnBitmapSetListener;

    invoke-virtual {v1, v7, v8}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setTag(ILjava/lang/Object;)V

    .line 549
    invoke-virtual {v1, v6, v0, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 552
    if-nez v6, :cond_2

    const/4 v7, 0x4

    :goto_1
    invoke-virtual {v1, v7}, Lcom/microsoft/xbox/toolkit/ui/XLEUniversalImageView;->setVisibility(I)V

    .line 554
    const v7, 0x7f0e06ea

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 555
    .local v5, "title":Landroid/widget/TextView;
    iget-object v7, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    iget-object v7, v7, Lcom/microsoft/xbox/service/model/recents/RecentItem;->Title:Ljava/lang/String;

    invoke-static {v5, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 557
    const v7, 0x7f0e06f7

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 558
    .local v4, "oneGuideIcon":Landroid/widget/TextView;
    const v7, 0x7f0e06f8

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 559
    .local v3, "newFeatureText":Landroid/widget/TextView;
    iget-object v7, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/recents/RecentItem;->getProviderTitleId()J

    move-result-wide v8

    const-wide/32 v10, 0x162615ad

    cmp-long v7, v8, v10

    if-nez v7, :cond_3

    .line 560
    const/4 v7, 0x0

    invoke-static {v4, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 561
    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v7}, Lcom/microsoft/xbox/XLEApplication;->shouldShowStreamingButton()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 562
    const/4 v7, 0x0

    invoke-static {v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 569
    :cond_0
    :goto_2
    iget-object v8, p2, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    const v7, 0x7f0e06ef

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    invoke-static {v8, v7}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->bindLaunchableItem(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Landroid/widget/TextView;)V

    .line 570
    return-void

    .line 547
    .end local v0    # "defaultRid":I
    .end local v3    # "newFeatureText":Landroid/widget/TextView;
    .end local v4    # "oneGuideIcon":Landroid/widget/TextView;
    .end local v5    # "title":Landroid/widget/TextView;
    :cond_1
    sget v0, Lcom/microsoft/xbox/toolkit/ImageLoader;->NO_RES:I

    goto :goto_0

    .line 552
    .restart local v0    # "defaultRid":I
    :cond_2
    const/4 v7, 0x0

    goto :goto_1

    .line 565
    .restart local v3    # "newFeatureText":Landroid/widget/TextView;
    .restart local v4    # "oneGuideIcon":Landroid/widget/TextView;
    .restart local v5    # "title":Landroid/widget/TextView;
    :cond_3
    const/16 v7, 0x8

    invoke-static {v4, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 566
    const/16 v7, 0x8

    invoke-static {v3, v7}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    goto :goto_2
.end method

.method private bindGamerScore(Landroid/view/View;)V
    .locals 6
    .param p1, "container"    # Landroid/view/View;

    .prologue
    .line 491
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getGamerScoreSummary()Landroid/util/Pair;

    move-result-object v3

    .line 492
    .local v3, "score":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    const v4, 0x7f0e06f3

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 493
    .local v0, "box":Landroid/view/View;
    if-nez v3, :cond_0

    .line 495
    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 503
    :goto_0
    return-void

    .line 497
    :cond_0
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 498
    const v4, 0x7f0e06f4

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 499
    .local v1, "gamerScore":Landroid/widget/TextView;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 500
    const v4, 0x7f0e06f5

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 501
    .local v2, "maxGamerScore":Landroid/widget/TextView;
    iget-object v4, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateTextIfNotNull(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected static bindLaunchableItem(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;Landroid/widget/TextView;)V
    .locals 2
    .param p0, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
    .param p1, "smartglassIcon"    # Landroid/widget/TextView;

    .prologue
    .line 675
    if-eqz p0, :cond_0

    .line 676
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$2;->$SwitchMap$com$microsoft$xbox$service$model$pins$ContentUtil$HasState:[I

    invoke-static {p0}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->computeHasStateForBat(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/pins/ContentUtil$HasState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 682
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 686
    :cond_0
    :goto_0
    return-void

    .line 678
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 679
    const v0, 0x7f071014

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 676
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected static computeEffectiveContainerHeight(Landroid/view/View;Landroid/widget/ImageView;)I
    .locals 4
    .param p0, "container"    # Landroid/view/View;
    .param p1, "img"    # Landroid/widget/ImageView;

    .prologue
    .line 332
    invoke-virtual {p1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 333
    .local v1, "lpImg":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int/2addr v2, v3

    iget v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v0, v2, v3

    .line 334
    .local v0, "height":I
    const/4 v2, 0x0

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    return v2
.end method

.method protected static computeFullSizeImage(Landroid/view/View;FLandroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 6
    .param p0, "container"    # Landroid/view/View;
    .param p1, "ratio"    # F
    .param p2, "img"    # Landroid/widget/ImageView;
    .param p3, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    const/16 v5, 0xf

    const/16 v4, 0xa

    const/4 v3, 0x0

    .line 311
    invoke-static {p0, p2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->computeEffectiveContainerHeight(Landroid/view/View;Landroid/widget/ImageView;)I

    move-result v0

    .line 312
    .local v0, "height":I
    if-lez v0, :cond_0

    .line 313
    invoke-virtual {p2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 315
    .local v1, "lpImg":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {p3}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->shouldAdjustImageHeight(Landroid/graphics/Bitmap;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 316
    invoke-static {p0, p1, v0, p3}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->adjustHeightForBitmapAR(Landroid/view/View;FILandroid/graphics/Bitmap;)I

    move-result v0

    .line 318
    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 319
    invoke-virtual {v1}, Landroid/widget/RelativeLayout$LayoutParams;->getRules()[I

    move-result-object v2

    aput v3, v2, v4

    .line 325
    :goto_0
    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 326
    if-nez p3, :cond_2

    move v2, v0

    :goto_1
    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 327
    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 329
    .end local v1    # "lpImg":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    return-void

    .line 322
    .restart local v1    # "lpImg":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 323
    invoke-virtual {v1}, Landroid/widget/RelativeLayout$LayoutParams;->getRules()[I

    move-result-object v2

    aput v3, v2, v5

    goto :goto_0

    .line 326
    :cond_2
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    mul-int/2addr v2, v0

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/2addr v2, v3

    goto :goto_1
.end method

.method private getFullContainer()Landroid/view/ViewGroup;
    .locals 2

    .prologue
    .line 248
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->section:Landroid/view/View;

    const v1, 0x7f0e0700

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private getRecentsView()Landroid/view/View;
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->spRecents:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method static synthetic lambda$bindBatApp$0(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 158
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->tappedAtBat()V

    return-void
.end method

.method static synthetic lambda$bindBatGame$1(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 171
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->tappedAtBat()V

    return-void
.end method

.method static synthetic lambda$bindBatNoStats$2(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 184
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->tappedAtBat()V

    return-void
.end method

.method private resizeContainers()V
    .locals 2

    .prologue
    .line 261
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->getFullContainer()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->resizeFullContainer(Landroid/view/View;)V

    .line 262
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$2;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$HomeScreenNowPlayingAdapter$RecentsState:[I

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->getRecentsState()Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 271
    :goto_0
    return-void

    .line 264
    :pswitch_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->getRecentsView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->resizeRecents(Landroid/view/View;)V

    .line 265
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->getRecentsView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->resizePromoImage(Landroid/view/View;)V

    goto :goto_0

    .line 268
    :pswitch_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->getRecentsView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->resizeRecents(Landroid/view/View;)V

    goto :goto_0

    .line 262
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private resizePromoImage(Landroid/view/View;)V
    .locals 4
    .param p1, "promoContainer"    # Landroid/view/View;

    .prologue
    .line 274
    const v2, 0x7f0e0704

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 275
    .local v0, "img":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 276
    .local v1, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v3

    if-eq v2, v3, :cond_0

    .line 277
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 278
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 280
    :cond_0
    return-void
.end method

.method private setBatState(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)V
    .locals 1
    .param p1, "li"    # Lcom/microsoft/xbox/service/model/pins/LaunchableItem;

    .prologue
    .line 115
    invoke-static {p1}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isGame(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->GAME:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->setFullState(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;)V

    .line 126
    :goto_0
    return-void

    .line 117
    :cond_0
    invoke-static {p1}, Lcom/microsoft/xbox/service/model/pins/ContentUtil;->isApp(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 118
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->hasStats()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 119
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->APP:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->setFullState(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;)V

    goto :goto_0

    .line 121
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->NO_STATS:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->setFullState(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;)V

    goto :goto_0

    .line 124
    :cond_2
    sget-object v0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->NO_STATS:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->setFullState(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;)V

    goto :goto_0
.end method

.method private setStatsControlsVisible(Z)V
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    .line 484
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 485
    .local v0, "visibility":I
    :goto_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->container:Landroid/view/View;

    const v2, 0x7f0e06eb

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 486
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->container:Landroid/view/View;

    const v2, 0x7f0e06f0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 487
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->container:Landroid/view/View;

    const v2, 0x7f0e06f1

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/app/XLEUtil;->updateVisibilityIfNotNull(Landroid/view/View;I)V

    .line 488
    return-void

    .line 484
    .end local v0    # "visibility":I
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method protected static shouldAdjustImageHeight(Landroid/graphics/Bitmap;)Z
    .locals 2
    .param p0, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 338
    if-eqz p0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    const v1, 0x3f666666    # 0.9f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected bindRecentsRecents()V
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->adp:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$ViewAdapter;->notifyDataSetChanged()V

    .line 308
    return-void
.end method

.method protected computeEffectiveContainerWidth(Landroid/view/View;Landroid/widget/ImageView;)I
    .locals 4
    .param p1, "container"    # Landroid/view/View;
    .param p2, "img"    # Landroid/widget/ImageView;

    .prologue
    .line 348
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 349
    .local v0, "lpCont":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-virtual {p2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 350
    .local v1, "lpImg":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v2, v3

    iget v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v2, v3

    return v2
.end method

.method protected getBatView()Landroid/view/View;
    .locals 2

    .prologue
    .line 252
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->getFullContainer()Landroid/view/ViewGroup;

    move-result-object v0

    .line 253
    .local v0, "vg":Landroid/view/ViewGroup;
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method protected getFullState()Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;
    .locals 5

    .prologue
    .line 197
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->getFullContainer()Landroid/view/ViewGroup;

    move-result-object v2

    .line 198
    .local v2, "vg":Landroid/view/ViewGroup;
    const v3, 0x7f0e0004

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    .line 199
    .local v1, "state":Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;
    if-nez v1, :cond_0

    .line 200
    sget-object v1, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->PROMO:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    .line 201
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 202
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030147

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 204
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    return-object v1
.end method

.method protected getRecentsState()Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;
    .locals 2

    .prologue
    .line 244
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;->values()[Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->spRecents:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->getState()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public onAnimateInCompleted()V
    .locals 3

    .prologue
    .line 141
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->onAnimateInCompleted()V

    .line 142
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->postAnimationJobs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 143
    .local v0, "r":Ljava/lang/Runnable;
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 145
    .end local v0    # "r":Ljava/lang/Runnable;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->postAnimationJobs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 146
    return-void
.end method

.method protected abstract resizeFullContainer(Landroid/view/View;)V
.end method

.method protected abstract resizeRecents(Landroid/view/View;)V
.end method

.method protected setFullState(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;)V
    .locals 5
    .param p1, "state"    # Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    .prologue
    const v3, 0x7f0e0004

    const/4 v4, 0x1

    .line 213
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->getFullContainer()Landroid/view/ViewGroup;

    move-result-object v1

    .line 214
    .local v1, "vg":Landroid/view/ViewGroup;
    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    if-eq p1, v2, :cond_0

    .line 215
    invoke-virtual {v1, v3, p1}, Landroid/view/ViewGroup;->setTag(ILjava/lang/Object;)V

    .line 217
    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 219
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 220
    .local v0, "inflater":Landroid/view/LayoutInflater;
    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$2;->$SwitchMap$com$microsoft$xbox$xle$app$adapter$HomeScreenNowPlayingAdapter$FullState:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 235
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    :goto_0
    return-void

    .line 222
    .restart local v0    # "inflater":Landroid/view/LayoutInflater;
    :pswitch_0
    const v2, 0x7f030144

    invoke-virtual {v0, v2, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    goto :goto_0

    .line 225
    :pswitch_1
    const v2, 0x7f030145

    invoke-virtual {v0, v2, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    goto :goto_0

    .line 228
    :pswitch_2
    const v2, 0x7f030146

    invoke-virtual {v0, v2, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    goto :goto_0

    .line 231
    :pswitch_3
    const v2, 0x7f030147

    invoke-virtual {v0, v2, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    goto :goto_0

    .line 220
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected setRecentsState(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;)V
    .locals 2
    .param p1, "state"    # Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;

    .prologue
    .line 238
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->spRecents:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->getState()I

    move-result v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;->ordinal()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 239
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->spRecents:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 241
    :cond_0
    return-void
.end method

.method public setSize(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 130
    invoke-super {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenAdapter;->setSize(II)V

    .line 131
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->resizeContainers()V

    .line 134
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->isBindViewOnResize()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->bindAllViews()V

    .line 137
    :cond_0
    return-void
.end method

.method protected updateViewOverride()V
    .locals 4

    .prologue
    .line 90
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->getNPState()Lcom/microsoft/xbox/xle/viewmodel/NPState;

    move-result-object v2

    .line 91
    .local v2, "state":Lcom/microsoft/xbox/xle/viewmodel/NPState;
    if-eqz v2, :cond_0

    .line 92
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->getLiBat()Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 93
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->getLiBat()Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    move-result-object v3

    invoke-interface {v3}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->isNPM()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 95
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->getLiBat()Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    move-result-object v3

    invoke-interface {v3}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getNPM()Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v0

    .line 96
    .local v0, "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/xle/viewmodel/HomeScreenNowPlayingViewModel;->isOOBE(Lcom/microsoft/xbox/xle/model/NowPlayingModel;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 97
    sget-object v3, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->PROMO:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->setFullState(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;)V

    .line 109
    .end local v0    # "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :goto_0
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->hasRecents()Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v3, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;->RECENTS:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;

    :goto_1
    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->setRecentsState(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;)V

    .line 111
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->bindAllViews()V

    .line 112
    return-void

    .line 99
    .restart local v0    # "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :cond_1
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->setBatState(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)V

    goto :goto_0

    .line 103
    .end local v0    # "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :cond_2
    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/NPState;->getLiBat()Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;

    move-result-object v3

    invoke-interface {v3}, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;->getRecent()Lcom/microsoft/xbox/service/model/recents/Recent;

    move-result-object v1

    .line 104
    .local v1, "r":Lcom/microsoft/xbox/service/model/recents/Recent;
    iget-object v3, v1, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-direct {p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->setBatState(Lcom/microsoft/xbox/service/model/pins/LaunchableItem;)V

    goto :goto_0

    .line 107
    .end local v1    # "r":Lcom/microsoft/xbox/service/model/recents/Recent;
    :cond_3
    sget-object v3, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;->PROMO:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter;->setFullState(Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$FullState;)V

    goto :goto_0

    .line 109
    :cond_4
    sget-object v3, Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;->PROMO:Lcom/microsoft/xbox/xle/app/adapter/HomeScreenNowPlayingAdapter$RecentsState;

    goto :goto_1
.end method
