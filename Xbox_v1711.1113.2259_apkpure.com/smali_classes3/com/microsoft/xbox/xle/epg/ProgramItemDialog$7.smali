.class Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$7;
.super Ljava/lang/Object;
.source "ProgramItemDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->updateChannelItemExtra()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;

    .prologue
    .line 362
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$7;->this$0:Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 365
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/SessionModel;->getSessionState()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 366
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$7;->this$0:Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->access$300(Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;)Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel$ChannelItem;->tuneToShow()Z

    move-result v0

    .line 367
    .local v0, "result":Z
    if-nez v0, :cond_0

    .line 368
    const v1, 0x7f070455

    invoke-static {v3, v1, v3}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;->show(IIZ)V

    .line 370
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog$7;->this$0:Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/ProgramItemDialog;->safeDismiss()V

    .line 374
    .end local v0    # "result":Z
    :goto_0
    return-void

    .line 372
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showConnectDialog()V

    goto :goto_0
.end method
