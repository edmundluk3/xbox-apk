.class Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "TVSeriesOverviewScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadActivitiesTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field private activitiesModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$1;

    .prologue
    .line 117
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;)V

    return-void
.end method

.method private onLoadActivitiesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 170
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->activitiesModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->access$102(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;Z)Z

    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->activitiesModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getActivitiesList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->access$202(Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->isLoadingDetail:Z

    .line 176
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->updateAdapter()V

    .line 177
    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->activitiesModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    if-nez v0, :cond_0

    .line 125
    :goto_0
    return v1

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->shouldRefresh()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->activitiesModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->shouldRefresh()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 6

    .prologue
    .line 152
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    iget-object v4, v4, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;

    iget-boolean v5, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->forceLoad:Z

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v3

    .line 154
    .local v3, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->getIsFail(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 155
    sget-object v4, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 165
    :goto_0
    return-object v4

    .line 158
    :cond_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    iget-object v4, v4, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    .line 159
    .local v1, "mediaId":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    iget-object v4, v4, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v4

    check-cast v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;

    iget-object v2, v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesMediaItem;->MediaItemType:Ljava/lang/String;

    .line 160
    .local v2, "mediaType":Ljava/lang/String;
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    iget-object v4, v4, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->mediaModel:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    check-cast v4, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2TVSeriesBrowseSeasonModel;->getMediaGroup()I

    move-result v0

    .line 161
    .local v0, "mediaGroup":I
    invoke-static {v1, v2, v0}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->getModel(Ljava/lang/String;Ljava/lang/String;I)Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->activitiesModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    .line 162
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->activitiesModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    if-eqz v4, :cond_1

    .line 163
    iget-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->activitiesModel:Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;

    iget-boolean v5, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->forceLoad:Z

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/service/model/MergedActivitySummaryModel;->loadSync(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v4

    goto :goto_0

    .line 165
    :cond_1
    sget-object v4, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 147
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->onLoadActivitiesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 132
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->onLoadActivitiesCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 143
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 117
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->isLoadingDetail:Z

    .line 137
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel$LoadActivitiesTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/TVSeriesOverviewScreenViewModel;->updateAdapter()V

    .line 138
    return-void
.end method
