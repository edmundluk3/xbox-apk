.class Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter$1;
.super Ljava/lang/Object;
.source "ClubInvitationsScreenAdapter.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/clubs/ClubDiscoveryInvitationListAdapter$InviteClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;-><init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAcceptClick(J)V
    .locals 1
    .param p1, "xuid"    # J

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->acceptInvite(J)V

    .line 54
    return-void
.end method

.method public onIgnoreClick(J)V
    .locals 1
    .param p1, "xuid"    # J

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->ignoreInvite(J)V

    .line 59
    return-void
.end method

.method public onProfileClick(J)V
    .locals 1
    .param p1, "xuid"    # J

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInvitationsScreenViewModel;->goToClub(J)V

    .line 49
    return-void
.end method
