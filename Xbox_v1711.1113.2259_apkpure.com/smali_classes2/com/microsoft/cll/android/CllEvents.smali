.class public Lcom/microsoft/cll/android/CllEvents;
.super Ljava/lang/Object;
.source "CllEvents.java"

# interfaces
.implements Lcom/microsoft/cll/android/ICllEvents;


# instance fields
.field private final clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

.field private final cll:Lcom/microsoft/cll/android/Cll;

.field private final partA:Lcom/microsoft/cll/android/PartA;


# direct methods
.method public constructor <init>(Lcom/microsoft/cll/android/PartA;Lcom/microsoft/cll/android/ClientTelemetry;Lcom/microsoft/cll/android/Cll;)V
    .locals 0
    .param p1, "partA"    # Lcom/microsoft/cll/android/PartA;
    .param p2, "clientTelemetry"    # Lcom/microsoft/cll/android/ClientTelemetry;
    .param p3, "cll"    # Lcom/microsoft/cll/android/Cll;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/microsoft/cll/android/CllEvents;->partA:Lcom/microsoft/cll/android/PartA;

    .line 15
    iput-object p2, p0, Lcom/microsoft/cll/android/CllEvents;->clientTelemetry:Lcom/microsoft/cll/android/ClientTelemetry;

    .line 16
    iput-object p3, p0, Lcom/microsoft/cll/android/CllEvents;->cll:Lcom/microsoft/cll/android/Cll;

    .line 17
    return-void
.end method


# virtual methods
.method public eventDropped(Ljava/lang/String;)V
    .locals 0
    .param p1, "event"    # Ljava/lang/String;

    .prologue
    .line 32
    return-void
.end method

.method public sendComplete()V
    .locals 0

    .prologue
    .line 22
    return-void
.end method

.method public stopped()V
    .locals 0

    .prologue
    .line 27
    return-void
.end method
