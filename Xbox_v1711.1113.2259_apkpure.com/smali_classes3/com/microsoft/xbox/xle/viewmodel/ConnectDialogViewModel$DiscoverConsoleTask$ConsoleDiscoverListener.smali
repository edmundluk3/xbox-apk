.class Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask$ConsoleDiscoverListener;
.super Lcom/microsoft/xbox/smartglass/DiscoveryManagerListener;
.source "ConnectDialogViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConsoleDiscoverListener"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;)V
    .locals 0

    .prologue
    .line 451
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask$ConsoleDiscoverListener;->this$1:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/DiscoveryManagerListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$1;

    .prologue
    .line 451
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask$ConsoleDiscoverListener;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;)V

    return-void
.end method


# virtual methods
.method public onDiscoverCompleted()V
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask$ConsoleDiscoverListener;->this$1:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->access$700(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask$ConsoleDiscoverListener;->this$1:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->access$800(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;)I

    move-result v0

    if-lez v0, :cond_1

    .line 472
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask$ConsoleDiscoverListener;->this$1:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->access$810(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;)I

    .line 473
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask$ConsoleDiscoverListener;->this$1:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->access$000(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;)Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/AvailableConsolesModel;->getIsDiscovering()Z

    move-result v0

    if-nez v0, :cond_0

    .line 475
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getDiscoveryManager()Lcom/microsoft/xbox/smartglass/DiscoveryManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/DiscoveryManager;->discover()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 482
    :cond_0
    :goto_0
    return-void

    .line 480
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask$ConsoleDiscoverListener;->this$1:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->access$900(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;)Lcom/microsoft/xbox/toolkit/Ready;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/Ready;->setReady()V

    goto :goto_0

    .line 476
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onPresenceUpdated(Lcom/microsoft/xbox/smartglass/PrimaryDevice;)V
    .locals 4
    .param p1, "device"    # Lcom/microsoft/xbox/smartglass/PrimaryDevice;

    .prologue
    const/4 v0, 0x1

    .line 454
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask$ConsoleDiscoverListener;->this$1:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->access$500(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 455
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask$ConsoleDiscoverListener;->this$1:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->access$600(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;)Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v1

    iget-object v1, v1, Lcom/microsoft/xbox/xle/model/ConsoleData;->id:Ljava/lang/String;

    iget-object v2, p1, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->status:Lcom/microsoft/xbox/smartglass/DeviceStatus;

    sget-object v2, Lcom/microsoft/xbox/smartglass/DeviceStatus;->Available:Lcom/microsoft/xbox/smartglass/DeviceStatus;

    if-ne v1, v2, :cond_0

    .line 456
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask$ConsoleDiscoverListener;->this$1:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->access$702(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;Z)Z

    .line 458
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask$ConsoleDiscoverListener;->this$1:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->access$700(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 459
    const-string v0, "ConnectDialog"

    const-string v1, "Power On succeeded"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    :cond_1
    :goto_0
    return-void

    .line 462
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask$ConsoleDiscoverListener;->this$1:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask$ConsoleDiscoverListener;->this$1:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->access$600(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;)Lcom/microsoft/xbox/xle/model/ConsoleData;

    move-result-object v2

    iget-object v2, v2, Lcom/microsoft/xbox/xle/model/ConsoleData;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p1, Lcom/microsoft/xbox/smartglass/PrimaryDevice;->status:Lcom/microsoft/xbox/smartglass/DeviceStatus;

    sget-object v3, Lcom/microsoft/xbox/smartglass/DeviceStatus;->Unavailable:Lcom/microsoft/xbox/smartglass/DeviceStatus;

    if-ne v2, v3, :cond_3

    :goto_1
    invoke-static {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->access$702(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;Z)Z

    .line 463
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask$ConsoleDiscoverListener;->this$1:Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;->access$700(Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel$DiscoverConsoleTask;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 464
    const-string v0, "ConnectDialog"

    const-string v1, "Power Off succeeded"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 462
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
