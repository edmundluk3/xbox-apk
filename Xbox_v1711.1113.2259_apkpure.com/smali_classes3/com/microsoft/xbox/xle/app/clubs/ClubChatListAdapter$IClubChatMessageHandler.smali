.class public interface abstract Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter$IClubChatMessageHandler;
.super Ljava/lang/Object;
.source "ClubChatListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubChatListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IClubChatMessageHandler"
.end annotation


# virtual methods
.method public abstract canDeleteMessage()Z
.end method

.method public abstract deleteMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)V
.end method

.method public abstract getUserIsMemberOf()Z
.end method

.method public abstract lazyQueryPersonSummary(Ljava/lang/String;)V
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
.end method

.method public abstract navigateToEnforcement(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;ZLjava/lang/Long;)V
    .param p1    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract navigateToUserProfile(Ljava/lang/String;)V
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method
