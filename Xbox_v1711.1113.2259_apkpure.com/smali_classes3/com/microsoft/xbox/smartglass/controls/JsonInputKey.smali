.class Lcom/microsoft/xbox/smartglass/controls/JsonInputKey;
.super Ljava/lang/Object;
.source "JsonInputKey.java"


# instance fields
.field public final key:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 5
    .param p1, "json"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 16
    .local v1, "jsonObject":Lorg/json/JSONObject;
    const-string v3, "key"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 18
    .local v2, "rawKey":I
    :try_start_0
    invoke-static {v2}, Lcom/microsoft/xbox/smartglass/controls/CanvasKey;->fromInt(I)Lcom/microsoft/xbox/smartglass/controls/CanvasKey;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/JsonInputKey;->key:Lcom/microsoft/xbox/smartglass/controls/CanvasKey;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 22
    return-void

    .line 19
    :catch_0
    move-exception v0

    .line 20
    .local v0, "ex":Ljava/lang/IndexOutOfBoundsException;
    new-instance v3, Lorg/json/JSONException;

    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v3
.end method
