.class public abstract Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;
.super Ljava/lang/Object;
.source "TitleHubDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "AchievementNotification"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 714
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/titleHub/TitleHubDataTypes$AchievementNotification;",
            ">;"
        }
    .end annotation

    .prologue
    .line 734
    new-instance v0, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/titleHub/AutoValue_TitleHubDataTypes_AchievementNotification$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract achievementIconUrl()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract achievementName()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract gamerscore()J
.end method

.method public abstract rarityCategory()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract rarityPercentage()F
.end method

.method public abstract titleId()J
.end method

.method public abstract titleName()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
