.class final Lcom/jakewharton/rxbinding2/view/ViewTreeObserverPreDrawObservable$Listener;
.super Lio/reactivex/android/MainThreadDisposable;
.source "ViewTreeObserverPreDrawObservable.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jakewharton/rxbinding2/view/ViewTreeObserverPreDrawObservable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Listener"
.end annotation


# instance fields
.field private final observer:Lio/reactivex/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observer",
            "<-",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final proceedDrawingPass:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final view:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;Ljava/util/concurrent/Callable;Lio/reactivex/Observer;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/concurrent/Callable",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lio/reactivex/Observer",
            "<-",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p2, "proceedDrawingPass":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<Ljava/lang/Boolean;>;"
    .local p3, "observer":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-Ljava/lang/Object;>;"
    invoke-direct {p0}, Lio/reactivex/android/MainThreadDisposable;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/jakewharton/rxbinding2/view/ViewTreeObserverPreDrawObservable$Listener;->view:Landroid/view/View;

    .line 39
    iput-object p2, p0, Lcom/jakewharton/rxbinding2/view/ViewTreeObserverPreDrawObservable$Listener;->proceedDrawingPass:Ljava/util/concurrent/Callable;

    .line 40
    iput-object p3, p0, Lcom/jakewharton/rxbinding2/view/ViewTreeObserverPreDrawObservable$Listener;->observer:Lio/reactivex/Observer;

    .line 41
    return-void
.end method


# virtual methods
.method protected onDispose()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/jakewharton/rxbinding2/view/ViewTreeObserverPreDrawObservable$Listener;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 58
    return-void
.end method

.method public onPreDraw()Z
    .locals 3

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/jakewharton/rxbinding2/view/ViewTreeObserverPreDrawObservable$Listener;->isDisposed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 45
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/view/ViewTreeObserverPreDrawObservable$Listener;->observer:Lio/reactivex/Observer;

    sget-object v2, Lcom/jakewharton/rxbinding2/internal/Notification;->INSTANCE:Lcom/jakewharton/rxbinding2/internal/Notification;

    invoke-interface {v1, v2}, Lio/reactivex/Observer;->onNext(Ljava/lang/Object;)V

    .line 47
    :try_start_0
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/view/ViewTreeObserverPreDrawObservable$Listener;->proceedDrawingPass:Ljava/util/concurrent/Callable;

    invoke-interface {v1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 53
    :goto_0
    return v1

    .line 48
    :catch_0
    move-exception v0

    .line 49
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/jakewharton/rxbinding2/view/ViewTreeObserverPreDrawObservable$Listener;->observer:Lio/reactivex/Observer;

    invoke-interface {v1, v0}, Lio/reactivex/Observer;->onError(Ljava/lang/Throwable;)V

    .line 50
    invoke-virtual {p0}, Lcom/jakewharton/rxbinding2/view/ViewTreeObserverPreDrawObservable$Listener;->dispose()V

    .line 53
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method
