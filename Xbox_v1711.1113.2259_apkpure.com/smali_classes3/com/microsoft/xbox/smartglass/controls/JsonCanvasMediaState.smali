.class Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;
.super Lorg/json/JSONObject;
.source "JsonCanvasMediaState.java"


# instance fields
.field private _assetId:Ljava/lang/String;

.field private _aumId:Ljava/lang/String;

.field private _capabilities:I

.field private _duration:J

.field private _enabledCommands:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/MediaControlCommands;",
            ">;"
        }
    .end annotation
.end field

.field private _endPosition:J

.field private _maxSeekPos:J

.field private _metaData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;",
            ">;"
        }
    .end annotation
.end field

.field private _minSeekPos:J

.field private _playbackSoundLevel:Lcom/microsoft/xbox/smartglass/SoundLevel;

.field private _playbackStatus:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

.field private _playbackType:Lcom/microsoft/xbox/smartglass/MediaType;

.field private _position:J

.field private _rate:F

.field private _startPosition:J

.field private _state:I

.field private _titleId:I


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 93
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 94
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->populateDefaultValues()V

    .line 95
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->createJSON()V

    .line 96
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/smartglass/MediaState;)V
    .locals 7
    .param p1, "mediaState"    # Lcom/microsoft/xbox/smartglass/MediaState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 98
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 99
    if-eqz p1, :cond_0

    .line 100
    iget v1, p1, Lcom/microsoft/xbox/smartglass/MediaState;->titleId:I

    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_titleId:I

    .line 101
    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/MediaState;->assetId:Ljava/lang/String;

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_assetId:Ljava/lang/String;

    .line 102
    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/MediaState;->aumId:Ljava/lang/String;

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_aumId:Ljava/lang/String;

    .line 103
    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/MediaState;->playbackType:Lcom/microsoft/xbox/smartglass/MediaType;

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_playbackType:Lcom/microsoft/xbox/smartglass/MediaType;

    .line 104
    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/MediaState;->soundLevel:Lcom/microsoft/xbox/smartglass/SoundLevel;

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_playbackSoundLevel:Lcom/microsoft/xbox/smartglass/SoundLevel;

    .line 105
    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/MediaState;->enabledCommands:Ljava/util/EnumSet;

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_enabledCommands:Ljava/util/EnumSet;

    .line 106
    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/MediaState;->playbackStatus:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_playbackStatus:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    .line 107
    iget-wide v2, p1, Lcom/microsoft/xbox/smartglass/MediaState;->mediaStart:J

    iput-wide v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_startPosition:J

    .line 108
    iget-wide v2, p1, Lcom/microsoft/xbox/smartglass/MediaState;->mediaEnd:J

    iput-wide v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_endPosition:J

    .line 109
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p1, Lcom/microsoft/xbox/smartglass/MediaState;->metaData:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_metaData:Ljava/util/ArrayList;

    .line 112
    iget-wide v2, p1, Lcom/microsoft/xbox/smartglass/MediaState;->mediaEnd:J

    iget-wide v4, p1, Lcom/microsoft/xbox/smartglass/MediaState;->mediaStart:J

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_duration:J

    .line 113
    iget-wide v2, p1, Lcom/microsoft/xbox/smartglass/MediaState;->position:J

    iput-wide v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_position:J

    .line 114
    iget-wide v2, p1, Lcom/microsoft/xbox/smartglass/MediaState;->minSeekTicks:J

    iput-wide v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_minSeekPos:J

    .line 115
    iget-wide v2, p1, Lcom/microsoft/xbox/smartglass/MediaState;->maxSeekTicks:J

    iput-wide v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_maxSeekPos:J

    .line 116
    iget v1, p1, Lcom/microsoft/xbox/smartglass/MediaState;->playbackRate:F

    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_rate:F

    .line 119
    sget-object v1, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState$1;->$SwitchMap$com$microsoft$xbox$smartglass$MediaPlaybackStatus:[I

    iget-object v2, p1, Lcom/microsoft/xbox/smartglass/MediaState;->playbackStatus:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    invoke-virtual {v2}, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 134
    iput v6, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_state:I

    .line 139
    :goto_0
    iput v6, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_capabilities:I

    .line 140
    iget-object v1, p1, Lcom/microsoft/xbox/smartglass/MediaState;->enabledCommands:Ljava/util/EnumSet;

    invoke-virtual {v1}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    .line 141
    .local v0, "command":Lcom/microsoft/xbox/smartglass/MediaControlCommands;
    sget-object v2, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState$1;->$SwitchMap$com$microsoft$xbox$smartglass$MediaControlCommands:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    goto :goto_1

    .line 143
    :pswitch_0
    iget v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_capabilities:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_capabilities:I

    goto :goto_1

    .line 121
    .end local v0    # "command":Lcom/microsoft/xbox/smartglass/MediaControlCommands;
    :pswitch_1
    const/4 v1, -0x1

    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_state:I

    goto :goto_0

    .line 124
    :pswitch_2
    const/4 v1, 0x5

    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_state:I

    goto :goto_0

    .line 128
    :pswitch_3
    iget-object v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_playbackStatus:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    invoke-virtual {v1}, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->getValue()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_state:I

    goto :goto_0

    .line 131
    :pswitch_4
    const/4 v1, 0x1

    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_state:I

    goto :goto_0

    .line 146
    .restart local v0    # "command":Lcom/microsoft/xbox/smartglass/MediaControlCommands;
    :pswitch_5
    iget v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_capabilities:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_capabilities:I

    goto :goto_1

    .line 149
    :pswitch_6
    iget v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_capabilities:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_capabilities:I

    goto :goto_1

    .line 152
    :pswitch_7
    iget v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_capabilities:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_capabilities:I

    goto :goto_1

    .line 155
    :pswitch_8
    iget v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_capabilities:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_capabilities:I

    goto :goto_1

    .line 158
    :pswitch_9
    iget v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_capabilities:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_capabilities:I

    goto :goto_1

    .line 161
    :pswitch_a
    iget v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_capabilities:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_capabilities:I

    goto :goto_1

    .line 177
    .end local v0    # "command":Lcom/microsoft/xbox/smartglass/MediaControlCommands;
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->populateDefaultValues()V

    .line 180
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->createJSON()V

    .line 181
    return-void

    .line 119
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 141
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method private createJSON()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 65
    const-string v3, "titleId"

    iget v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_titleId:I

    invoke-virtual {p0, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 66
    const-string v3, "assetId"

    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_assetId:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 67
    const-string v3, "aumId"

    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_aumId:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 68
    const-string v3, "playbackType"

    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_playbackType:Lcom/microsoft/xbox/smartglass/MediaType;

    invoke-virtual {v4}, Lcom/microsoft/xbox/smartglass/MediaType;->getValue()I

    move-result v4

    invoke-virtual {p0, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 69
    const-string v3, "playbackSoundLevel"

    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_playbackSoundLevel:Lcom/microsoft/xbox/smartglass/SoundLevel;

    invoke-virtual {v4}, Lcom/microsoft/xbox/smartglass/SoundLevel;->getValue()I

    move-result v4

    invoke-virtual {p0, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 70
    const-string v3, "enabledCommands"

    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_enabledCommands:Ljava/util/EnumSet;

    invoke-static {v4}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->getValue(Ljava/util/EnumSet;)I

    move-result v4

    invoke-virtual {p0, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 71
    const-string v3, "playbackStatus"

    iget-object v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_playbackStatus:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    invoke-virtual {v4}, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->getValue()I

    move-result v4

    invoke-virtual {p0, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 72
    const-string v3, "startPosition"

    iget-wide v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_startPosition:J

    invoke-virtual {p0, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 73
    const-string v3, "endPosition"

    iget-wide v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_endPosition:J

    invoke-virtual {p0, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 75
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 76
    .local v1, "metadata":Lorg/json/JSONArray;
    iget-object v3, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_metaData:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;

    .line 77
    .local v0, "entry":Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 78
    .local v2, "nameValue":Lorg/json/JSONObject;
    const-string v4, "Name"

    iget-object v5, v0, Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;->name:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 79
    const-string v4, "Value"

    iget-object v5, v0, Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;->value:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 80
    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 82
    .end local v0    # "entry":Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;
    .end local v2    # "nameValue":Lorg/json/JSONObject;
    :cond_0
    const-string v3, "metaData"

    invoke-virtual {p0, v3, v1}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 84
    const-string v3, "duration"

    iget-wide v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_duration:J

    invoke-virtual {p0, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 85
    const-string v3, "position"

    iget-wide v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_position:J

    invoke-virtual {p0, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 86
    const-string v3, "minSeekPos"

    iget-wide v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_minSeekPos:J

    invoke-virtual {p0, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 87
    const-string v3, "maxSeekPos"

    iget-wide v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_maxSeekPos:J

    invoke-virtual {p0, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 88
    const-string v3, "rate"

    iget v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_rate:F

    float-to-double v4, v4

    invoke-virtual {p0, v3, v4, v5}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 89
    const-string v3, "state"

    iget v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_state:I

    invoke-virtual {p0, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 90
    const-string v3, "capabilities"

    iget v4, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_capabilities:I

    invoke-virtual {p0, v3, v4}, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 91
    return-void
.end method

.method private populateDefaultValues()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 43
    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_titleId:I

    .line 44
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_assetId:Ljava/lang/String;

    .line 45
    const-string v0, ""

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_aumId:Ljava/lang/String;

    .line 46
    sget-object v0, Lcom/microsoft/xbox/smartglass/MediaType;->Unknown:Lcom/microsoft/xbox/smartglass/MediaType;

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_playbackType:Lcom/microsoft/xbox/smartglass/MediaType;

    .line 47
    sget-object v0, Lcom/microsoft/xbox/smartglass/SoundLevel;->Muted:Lcom/microsoft/xbox/smartglass/SoundLevel;

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_playbackSoundLevel:Lcom/microsoft/xbox/smartglass/SoundLevel;

    .line 48
    sget-object v0, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->None:Lcom/microsoft/xbox/smartglass/MediaControlCommands;

    invoke-virtual {v0}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->getValue()I

    move-result v0

    invoke-static {v0}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->fromInt(I)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_enabledCommands:Ljava/util/EnumSet;

    .line 49
    sget-object v0, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->Closed:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_playbackStatus:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    .line 50
    iput-wide v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_startPosition:J

    .line 51
    iput-wide v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_endPosition:J

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_metaData:Ljava/util/ArrayList;

    .line 55
    iput-wide v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_duration:J

    .line 56
    iput-wide v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_position:J

    .line 57
    iput-wide v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_minSeekPos:J

    .line 58
    iput-wide v2, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_maxSeekPos:J

    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_rate:F

    .line 60
    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_state:I

    .line 61
    iput v1, p0, Lcom/microsoft/xbox/smartglass/controls/JsonCanvasMediaState;->_capabilities:I

    .line 62
    return-void
.end method
