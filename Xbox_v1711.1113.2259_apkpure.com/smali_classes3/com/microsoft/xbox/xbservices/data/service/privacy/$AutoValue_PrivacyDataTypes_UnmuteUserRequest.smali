.class abstract Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_UnmuteUserRequest;
.super Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UnmuteUserRequest;
.source "$AutoValue_PrivacyDataTypes_UnmuteUserRequest.java"


# instance fields
.field private final operation:Ljava/lang/String;

.field private final users:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/common/collect/ImmutableList;)V
    .locals 2
    .param p1, "operation"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 16
    .local p2, "users":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;>;"
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UnmuteUserRequest;-><init>()V

    .line 17
    if-nez p1, :cond_0

    .line 18
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null operation"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_UnmuteUserRequest;->operation:Ljava/lang/String;

    .line 21
    if-nez p2, :cond_1

    .line 22
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null users"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 24
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_UnmuteUserRequest;->users:Lcom/google/common/collect/ImmutableList;

    .line 25
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 49
    if-ne p1, p0, :cond_1

    .line 57
    :cond_0
    :goto_0
    return v1

    .line 52
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UnmuteUserRequest;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 53
    check-cast v0, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UnmuteUserRequest;

    .line 54
    .local v0, "that":Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UnmuteUserRequest;
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_UnmuteUserRequest;->operation:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UnmuteUserRequest;->operation()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_UnmuteUserRequest;->users:Lcom/google/common/collect/ImmutableList;

    .line 55
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UnmuteUserRequest;->users()Lcom/google/common/collect/ImmutableList;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/common/collect/ImmutableList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UnmuteUserRequest;
    :cond_3
    move v1, v2

    .line 57
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const v2, 0xf4243

    .line 62
    const/4 v0, 0x1

    .line 63
    .local v0, "h":I
    mul-int/2addr v0, v2

    .line 64
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_UnmuteUserRequest;->operation:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 65
    mul-int/2addr v0, v2

    .line 66
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_UnmuteUserRequest;->users:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 67
    return v0
.end method

.method public operation()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_UnmuteUserRequest;->operation:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UnmuteUserRequest{operation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_UnmuteUserRequest;->operation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", users="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_UnmuteUserRequest;->users:Lcom/google/common/collect/ImmutableList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public users()Lcom/google/common/collect/ImmutableList;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/xbservices/data/service/privacy/PrivacyDataTypes$UserXuid;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/service/privacy/$AutoValue_PrivacyDataTypes_UnmuteUserRequest;->users:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method
