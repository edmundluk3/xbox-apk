.class Lcom/microsoft/playready/networkdevice/Native_Class4;
.super Ljava/lang/Object;
.source "Native_Class4.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/playready/networkdevice/Native_Class4$IHandler_1;
    }
.end annotation


# instance fields
.field protected mField1:J

.field protected mField2:J

.field private mHandler1:Lcom/microsoft/playready/networkdevice/Native_Class4$IHandler_1;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 16
    invoke-static {}, Lcom/microsoft/playready/networkdevice/NativeLoadLibrary;->loadLibrary()V

    .line 17
    return-void
.end method

.method constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class4;->mHandler1:Lcom/microsoft/playready/networkdevice/Native_Class4$IHandler_1;

    .line 31
    iput-wide v2, p0, Lcom/microsoft/playready/networkdevice/Native_Class4;->mField1:J

    .line 34
    iput-wide v2, p0, Lcom/microsoft/playready/networkdevice/Native_Class4;->mField2:J

    .line 40
    invoke-virtual {p0}, Lcom/microsoft/playready/networkdevice/Native_Class4;->_method_1()V

    .line 41
    return-void
.end method


# virtual methods
.method protected native _method_1()V
.end method

.method protected native _method_2()V
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/microsoft/playready/networkdevice/Native_Class4;->_method_2()V

    .line 49
    return-void
.end method

.method internal_method_1()J
    .locals 2

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class4;->mField2:J

    return-wide v0
.end method

.method public native method_3()V
.end method

.method r_method_1(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "arg1"    # Ljava/lang/String;
    .param p2, "arg2"    # Ljava/lang/String;
    .param p3, "arg3"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class4;->mHandler1:Lcom/microsoft/playready/networkdevice/Native_Class4$IHandler_1;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class4;->mHandler1:Lcom/microsoft/playready/networkdevice/Native_Class4$IHandler_1;

    invoke-interface {v0, p1, p2, p3}, Lcom/microsoft/playready/networkdevice/Native_Class4$IHandler_1;->call1(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    :cond_0
    return-void
.end method

.method r_method_2(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "arg1"    # Ljava/lang/String;
    .param p2, "arg2"    # Ljava/lang/String;
    .param p3, "arg3"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class4;->mHandler1:Lcom/microsoft/playready/networkdevice/Native_Class4$IHandler_1;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/microsoft/playready/networkdevice/Native_Class4;->mHandler1:Lcom/microsoft/playready/networkdevice/Native_Class4$IHandler_1;

    invoke-interface {v0, p1, p2, p3}, Lcom/microsoft/playready/networkdevice/Native_Class4$IHandler_1;->call2(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    :cond_0
    return-void
.end method

.method public setHandler1(Lcom/microsoft/playready/networkdevice/Native_Class4$IHandler_1;)V
    .locals 0
    .param p1, "handler"    # Lcom/microsoft/playready/networkdevice/Native_Class4$IHandler_1;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/microsoft/playready/networkdevice/Native_Class4;->mHandler1:Lcom/microsoft/playready/networkdevice/Native_Class4$IHandler_1;

    .line 54
    return-void
.end method
