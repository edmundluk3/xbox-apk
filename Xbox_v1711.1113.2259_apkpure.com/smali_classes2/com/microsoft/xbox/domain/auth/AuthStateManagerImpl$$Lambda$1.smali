.class final synthetic Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$$Lambda$1;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/BiFunction;


# static fields
.field private static final instance:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$$Lambda$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$$Lambda$1;

    invoke-direct {v0}, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$$Lambda$1;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$$Lambda$1;->instance:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$$Lambda$1;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static lambdaFactory$()Lio/reactivex/functions/BiFunction;
    .locals 1

    sget-object v0, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$$Lambda$1;->instance:Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$$Lambda$1;

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/microsoft/xbox/domain/auth/AuthStateManagerImpl$AuthAction;

    check-cast p2, Lcom/microsoft/xbox/domain/auth/AuthState;

    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method
