.class public final Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_TournamentNotificationDataTypes_GameTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;",
        ">;"
    }
.end annotation


# instance fields
.field private defaultEra:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

.field private defaultUwpDesktop:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

.field private defaultUwpXboxOne:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

.field private final eraAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final uwpDesktopAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final uwpXboxOneAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 1
    .param p1, "gson"    # Lcom/google/gson/Gson;

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;->defaultUwpDesktop:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    .line 24
    iput-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;->defaultUwpXboxOne:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    .line 25
    iput-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;->defaultEra:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    .line 27
    const-class v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;->uwpDesktopAdapter:Lcom/google/gson/TypeAdapter;

    .line 28
    const-class v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;->uwpXboxOneAdapter:Lcom/google/gson/TypeAdapter;

    .line 29
    const-class v0, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    invoke-virtual {p1, v0}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;->eraAdapter:Lcom/google/gson/TypeAdapter;

    .line 30
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;
    .locals 6
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v4

    sget-object v5, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v4, v5, :cond_0

    .line 63
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 64
    const/4 v4, 0x0

    .line 95
    :goto_0
    return-object v4

    .line 66
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 67
    iget-object v2, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;->defaultUwpDesktop:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    .line 68
    .local v2, "uwpDesktop":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;
    iget-object v3, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;->defaultUwpXboxOne:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    .line 69
    .local v3, "uwpXboxOne":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;
    iget-object v1, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;->defaultEra:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    .line 70
    .local v1, "era":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 71
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v4

    sget-object v5, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v4, v5, :cond_1

    .line 73
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 76
    :cond_1
    const/4 v4, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v4, :pswitch_data_0

    .line 90
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 76
    :sswitch_0
    const-string v5, "uwp_desktop"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x0

    goto :goto_2

    :sswitch_1
    const-string v5, "uwp_xboxone"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x1

    goto :goto_2

    :sswitch_2
    const-string v5, "era"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v4, 0x2

    goto :goto_2

    .line 78
    :pswitch_0
    iget-object v4, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;->uwpDesktopAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v4, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "uwpDesktop":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;
    check-cast v2, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    .line 79
    .restart local v2    # "uwpDesktop":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;
    goto :goto_1

    .line 82
    :pswitch_1
    iget-object v4, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;->uwpXboxOneAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v4, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "uwpXboxOne":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;
    check-cast v3, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    .line 83
    .restart local v3    # "uwpXboxOne":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;
    goto :goto_1

    .line 86
    :pswitch_2
    iget-object v4, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;->eraAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v4, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "era":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;
    check-cast v1, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    .line 87
    .restart local v1    # "era":Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;
    goto :goto_1

    .line 94
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 95
    new-instance v4, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes;

    invoke-direct {v4, v2, v3, v1}, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes;-><init>(Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;)V

    goto :goto_0

    .line 76
    nop

    :sswitch_data_0
    .sparse-switch
        0x18954 -> :sswitch_2
        0x61f90eeb -> :sswitch_0
        0x7ea49e02 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultEra(Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;)Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultEra"    # Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;->defaultEra:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    .line 41
    return-object p0
.end method

.method public setDefaultUwpDesktop(Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;)Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultUwpDesktop"    # Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;->defaultUwpDesktop:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    .line 33
    return-object p0
.end method

.method public setDefaultUwpXboxOne(Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;)Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultUwpXboxOne"    # Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;->defaultUwpXboxOne:Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    .line 37
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .param p2, "object"    # Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    if-nez p2, :cond_0

    .line 47
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 58
    :goto_0
    return-void

    .line 50
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 51
    const-string v0, "uwp_desktop"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;->uwpDesktopAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;->uwpDesktop()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 53
    const-string v0, "uwp_xboxone"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 54
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;->uwpXboxOneAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;->uwpXboxOne()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 55
    const-string v0, "era"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 56
    iget-object v0, p0, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;->eraAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;->era()Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$PlatformInfo;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 57
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    check-cast p2, Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/domain/tournaments/AutoValue_TournamentNotificationDataTypes_GameTypes$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/domain/tournaments/TournamentNotificationDataTypes$GameTypes;)V

    return-void
.end method
