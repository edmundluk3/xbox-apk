.class public Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;
.source "ActivityFeedShareToFeedScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$LoadFeedItemAsyncTask;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String;

.field private static final userPostsModel:Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;


# instance fields
.field private caption:Ljava/lang/String;

.field private final clubId:J

.field private final entityModel:Lcom/microsoft/xbox/service/model/entity/EntityModel;

.field private lastShareErrorCode:J

.field private loadFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$LoadFeedItemAsyncTask;

.field private final originatingScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

.field private final profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private shareFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->TAG:Ljava/lang/String;

    .line 30
    sget-object v0, Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;->INSTANCE:Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->userPostsModel:Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 41
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;-><init>()V

    .line 39
    iput-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->lastShareErrorCode:J

    .line 42
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v4

    iput-object v4, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 43
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v1

    .line 44
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getActivityFeedItemLocator()Ljava/lang/String;

    move-result-object v0

    .line 45
    .local v0, "locator":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedClubId()Ljava/lang/Long;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getSelectedClubId()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    :cond_0
    iput-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->clubId:J

    .line 47
    if-nez v0, :cond_1

    const/4 v2, 0x0

    :goto_0
    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->entityModel:Lcom/microsoft/xbox/service/model/entity/EntityModel;

    .line 48
    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getFromScreen()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->originatingScreen:Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .line 49
    return-void

    .line 47
    :cond_1
    invoke-static {v0}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->getInstance(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/entity/EntityModel;

    move-result-object v2

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->closeScreen()V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;)Lcom/microsoft/xbox/service/model/entity/EntityModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->entityModel:Lcom/microsoft/xbox/service/model/entity/EntityModel;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->onFeedItemComplete(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
    .param p2, "x2"    # Z

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->onShareComplete(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;)J
    .locals 2
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;

    .prologue
    .line 27
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->clubId:J

    return-wide v0
.end method

.method static synthetic access$600()Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->userPostsModel:Lcom/microsoft/xbox/service/model/activityfeed/UserPostsModel;

    return-object v0
.end method

.method private closeScreen()V
    .locals 0

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->goBack()V

    .line 200
    return-void
.end method

.method private createAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;
    .locals 1

    .prologue
    .line 195
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getActivityFeedShareToFeedScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    return-object v0
.end method

.method private onFeedItemComplete(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 203
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$2;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 212
    :pswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->updateAdapter()V

    .line 213
    return-void

    .line 203
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private onShareComplete(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;Z)V
    .locals 4
    .param p1, "result"    # Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;
    .param p2, "pin"    # Z

    .prologue
    const/4 v3, 0x0

    .line 216
    if-eqz p1, :cond_2

    .line 217
    if-eqz p2, :cond_1

    .line 218
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->pinFeedItem(Lcom/microsoft/xbox/service/activityFeed/UserPostsDataTypes$PostResponse;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 219
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to find timelineUri for pinning after posting"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0700dc

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 222
    iput v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->errorResId:I

    .line 223
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->goBack()V

    .line 232
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->updateAdapter()V

    .line 233
    return-void

    .line 226
    :cond_1
    iput v3, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->errorResId:I

    .line 227
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->goBack()V

    goto :goto_0

    .line 230
    :cond_2
    const v0, 0x7f070b6c

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->errorResId:I

    goto :goto_0
.end method

.method private showWarning()V
    .locals 4

    .prologue
    .line 178
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 180
    .local v0, "bldr":Landroid/app/AlertDialog$Builder;
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070c1f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070c1e

    .line 181
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    .line 182
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070738

    .line 183
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v2

    new-instance v3, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$1;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$1;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070736

    .line 189
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->applyTypeface(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 190
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 192
    return-void
.end method


# virtual methods
.method public canShare()Z
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->entityModel:Lcom/microsoft/xbox/service/model/entity/EntityModel;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->isSharing()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public checkPinAndSubmitShare(Z)V
    .locals 1
    .param p1, "pin"    # Z

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->getItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v0

    if-nez v0, :cond_0

    .line 130
    const v0, 0x7f070b6f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->showError(I)V

    .line 140
    :goto_0
    return-void

    .line 134
    :cond_0
    if-eqz p1, :cond_1

    .line 135
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->checkPin()V

    .line 136
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->updateAdapter()V

    goto :goto_0

    .line 138
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->postStatus(Z)V

    goto :goto_0
.end method

.method public discardShare()V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->caption:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->closeScreen()V

    .line 125
    :goto_0
    return-void

    .line 123
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->showWarning()V

    goto :goto_0
.end method

.method public getActivityFeedHeaderStringId()I
    .locals 1

    .prologue
    .line 52
    const v0, 0x7f070c20

    return v0
.end method

.method public getCaption()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->caption:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->caption:Ljava/lang/String;

    goto :goto_0
.end method

.method public getErrorStringResId()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->errorResId:I

    return v0
.end method

.method public getItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    .locals 3

    .prologue
    .line 109
    const/4 v1, 0x0

    .line 110
    .local v1, "item":Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->entityModel:Lcom/microsoft/xbox/service/model/entity/EntityModel;

    if-eqz v2, :cond_0

    .line 111
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->entityModel:Lcom/microsoft/xbox/service/model/entity/EntityModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/entity/EntityModel;->getEntity()Lcom/microsoft/xbox/service/model/entity/Entity;

    move-result-object v0

    .line 112
    .local v0, "entity":Lcom/microsoft/xbox/service/model/entity/Entity;
    if-eqz v0, :cond_0

    .line 113
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/entity/Entity;->getActivityFeedItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v1

    .line 116
    .end local v0    # "entity":Lcom/microsoft/xbox/service/model/entity/Entity;
    :cond_0
    return-object v1
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->isLoadingFeedItem()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->isSharing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->isPinning()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLoadingFeedItem()Z
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->loadFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$LoadFeedItemAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->loadFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$LoadFeedItemAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$LoadFeedItemAsyncTask;->getIsBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSharing()Z
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->shareFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->shareFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;->getIsBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->profileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadAsync(Z)V

    .line 169
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->entityModel:Lcom/microsoft/xbox/service/model/entity/EntityModel;

    if-eqz v0, :cond_0

    .line 170
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->isLoadingFeedItem()Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$LoadFeedItemAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$LoadFeedItemAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->loadFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$LoadFeedItemAsyncTask;

    .line 172
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->loadFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$LoadFeedItemAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$LoadFeedItemAsyncTask;->load(Z)V

    .line 175
    :cond_0
    return-void
.end method

.method public onBackButtonPressed()V
    .locals 0

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->discardShare()V

    .line 90
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 73
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedPostScreenViewModelBase;->onDestroy()V

    .line 74
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->isLoadingFeedItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->loadFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$LoadFeedItemAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$LoadFeedItemAsyncTask;->cancel()V

    .line 77
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->isSharing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->shareFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;->cancel()V

    .line 80
    :cond_1
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->createAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 65
    return-void
.end method

.method protected onStartOverride()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    if-nez v0, :cond_0

    .line 58
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->createAdapter()Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 60
    :cond_0
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method protected postStatus(Z)V
    .locals 2
    .param p1, "pin"    # Z

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->isSharing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->shareFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;->cancel()V

    .line 151
    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->getItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v0

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->userXuid:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->getItem()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;->getActivityItemType(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem$ActivityItemType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCActivityFeed;->trackShareFeedPost(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;Z)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->shareFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;

    .line 154
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->shareFeedItemTask:Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel$ShareFeedItemAsyncTask;->load(Z)V

    .line 155
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->updateAdapter()V

    .line 156
    return-void
.end method

.method public setCaption(Ljava/lang/String;)V
    .locals 0
    .param p1, "caption"    # Ljava/lang/String;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/ActivityFeedShareToFeedScreenViewModel;->caption:Ljava/lang/String;

    .line 102
    return-void
.end method
