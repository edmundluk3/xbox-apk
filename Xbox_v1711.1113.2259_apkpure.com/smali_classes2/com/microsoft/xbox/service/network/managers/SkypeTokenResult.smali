.class public abstract Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;
.super Ljava/lang/Object;
.source "SkypeTokenResult.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static typeAdapter(Lcom/google/gson/Gson;)Lcom/google/gson/TypeAdapter;
    .locals 1
    .param p0, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/SkypeTokenResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/AutoValue_SkypeTokenResult$GsonTypeAdapter;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/AutoValue_SkypeTokenResult$GsonTypeAdapter;-><init>(Lcom/google/gson/Gson;)V

    return-object v0
.end method


# virtual methods
.method public abstract expiresIn()I
.end method

.method public abstract skypetoken()Ljava/lang/String;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method
