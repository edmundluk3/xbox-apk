.class Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter$1;
.super Ljava/lang/Object;
.source "CreateLfgDetailsAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;-><init>(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v2, 0x0

    .line 89
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->access$000(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;)Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    .line 90
    .local v0, "visibilityType":Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;
    if-eqz v0, :cond_0

    .line 91
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->access$100(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;)Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel;->setSelectedVisibility(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;)V

    .line 92
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->access$200(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 93
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->access$200(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    move-result-object v3

    sget-object v1, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;->Private:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsViewModel$LfgVisibilityType;

    if-eq v0, v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setEnabled(Z)V

    .line 94
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter$1;->this$0:Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;->access$200(Lcom/microsoft/xbox/xle/app/lfg/CreateLfgDetailsAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLECheckBox;->setChecked(Z)V

    .line 97
    :cond_0
    return-void

    :cond_1
    move v1, v2

    .line 93
    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 102
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
