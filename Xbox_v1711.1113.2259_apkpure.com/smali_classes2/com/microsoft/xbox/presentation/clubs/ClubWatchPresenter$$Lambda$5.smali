.class final synthetic Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter$$Lambda$5;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;

.field private final arg$2:Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;

.field private final arg$3:Lcom/microsoft/xbox/presentation/beam/BeamNavigator;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;Lcom/microsoft/xbox/presentation/beam/BeamNavigator;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter$$Lambda$5;->arg$1:Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;

    iput-object p2, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter$$Lambda$5;->arg$2:Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;

    iput-object p3, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter$$Lambda$5;->arg$3:Lcom/microsoft/xbox/presentation/beam/BeamNavigator;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;Lcom/microsoft/xbox/presentation/beam/BeamNavigator;)Lio/reactivex/functions/Function;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter$$Lambda$5;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter$$Lambda$5;-><init>(Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;Lcom/microsoft/xbox/presentation/beam/BeamNavigator;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter$$Lambda$5;->arg$1:Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter$$Lambda$5;->arg$2:Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;

    iget-object v2, p0, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter$$Lambda$5;->arg$3:Lcom/microsoft/xbox/presentation/beam/BeamNavigator;

    check-cast p1, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    invoke-static {v0, v1, v2, p1}, Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;->lambda$null$4(Lcom/microsoft/xbox/presentation/clubs/ClubWatchPresenter;Lcom/microsoft/xbox/domain/clubs/ClubWatchInteractor;Lcom/microsoft/xbox/presentation/beam/BeamNavigator;Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)Lio/reactivex/ObservableSource;

    move-result-object v0

    return-object v0
.end method
