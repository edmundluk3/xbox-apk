.class final synthetic Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask$$Lambda$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/microsoft/xbox/toolkit/java8/Predicate;


# static fields
.field private static final instance:Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask$$Lambda$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask$$Lambda$1;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask$$Lambda$1;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask$$Lambda$1;->instance:Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask$$Lambda$1;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static lambdaFactory$()Lcom/microsoft/xbox/toolkit/java8/Predicate;
    .locals 1

    sget-object v0, Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask$$Lambda$1;->instance:Lcom/microsoft/xbox/service/model/gcm/GcmModel$RegisterChatTask$$Lambda$1;

    return-object v0
.end method


# virtual methods
.method public test(Ljava/lang/Object;)Z
    .locals 1

    check-cast p1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->userIsMemberOf()Z

    move-result v0

    return v0
.end method
