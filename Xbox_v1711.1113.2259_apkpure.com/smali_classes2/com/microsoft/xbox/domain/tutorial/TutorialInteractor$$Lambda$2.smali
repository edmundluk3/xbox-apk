.class final synthetic Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor$$Lambda$2;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

.field private final arg$2:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

.field private final arg$3:Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor$$Lambda$2;->arg$1:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    iput-object p2, p0, Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor$$Lambda$2;->arg$2:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    iput-object p3, p0, Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor$$Lambda$2;->arg$3:Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;)Lio/reactivex/functions/Function;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor$$Lambda$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor$$Lambda$2;-><init>(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor$$Lambda$2;->arg$1:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    iget-object v1, p0, Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor$$Lambda$2;->arg$2:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    iget-object v2, p0, Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor$$Lambda$2;->arg$3:Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;

    check-cast p1, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;

    invoke-static {v0, v1, v2, p1}, Lcom/microsoft/xbox/domain/tutorial/TutorialInteractor;->lambda$null$2(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/microsoft/xbox/toolkit/SchedulerProvider;Lcom/microsoft/xbox/data/repository/tutorial/WelcomeCardCompletionRepository;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;)Lio/reactivex/ObservableSource;

    move-result-object v0

    return-object v0
.end method
