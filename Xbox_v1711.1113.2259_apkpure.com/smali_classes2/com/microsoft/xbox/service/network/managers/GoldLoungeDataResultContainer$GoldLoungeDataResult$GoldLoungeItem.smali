.class public Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;
.super Ljava/lang/Object;
.source "GoldLoungeDataResultContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GoldLoungeItem"
.end annotation


# instance fields
.field public DiscountedPriceText:Ljava/lang/String;

.field public GoldWeekendText:Ljava/lang/String;

.field public ID:Ljava/lang/String;

.field public ListPriceText:Ljava/lang/String;

.field public MediaItemType:Ljava/lang/String;

.field public ReleaseDate:Ljava/util/Date;

.field public SubscriptionImage:Ljava/lang/String;

.field public TitleId:Ljava/lang/String;

.field public TitleImage:Ljava/lang/String;

.field public TitleName:Ljava/lang/String;

.field public bigCatProductId:Ljava/lang/String;

.field private section:Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;

.field private showHeaderTextDealsWithGold:Z

.field private showHeaderTextGamesWithGold:Z

.field public titleImageUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 3
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;->Unspecified:Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->section:Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;

    .line 66
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 69
    iput-object v2, p0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->GoldWeekendText:Ljava/lang/String;

    .line 71
    iput-object v2, p0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->SubscriptionImage:Ljava/lang/String;

    .line 74
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getListPrice()Ljava/math/BigDecimal;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 76
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getListPrice()Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_1

    .line 77
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070b00

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->DiscountedPriceText:Ljava/lang/String;

    .line 83
    :goto_0
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMsrp()Ljava/math/BigDecimal;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMsrp()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getListPrice()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const-string v0, ""

    .line 84
    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->ListPriceText:Ljava/lang/String;

    .line 89
    :goto_2
    iput-object v2, p0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->ReleaseDate:Ljava/util/Date;

    .line 90
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->TitleId:Ljava/lang/String;

    .line 91
    iput-object v2, p0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->TitleImage:Ljava/lang/String;

    .line 92
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->TitleName:Ljava/lang/String;

    .line 93
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->MediaItemType:Ljava/lang/String;

    .line 94
    iget-object v0, p1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->ID:Ljava/lang/String;

    .line 96
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBigCatProductId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->bigCatProductId:Ljava/lang/String;

    .line 97
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->titleImageUrl:Ljava/lang/String;

    .line 98
    return-void

    .line 79
    :cond_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getListPrice()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCurrencyCode()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->getPriceText(Ljava/math/BigDecimal;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->DiscountedPriceText:Ljava/lang/String;

    goto :goto_0

    .line 84
    :cond_2
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMsrp()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCurrencyCode()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->getPriceText(Ljava/math/BigDecimal;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 86
    :cond_3
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMsrp()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCurrencyCode()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->getPriceText(Ljava/math/BigDecimal;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->ListPriceText:Ljava/lang/String;

    goto :goto_2
.end method


# virtual methods
.method public getGoldResultItemSection()Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->section:Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;

    return-object v0
.end method

.method public setDealsWithGoldHeaderText(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->showHeaderTextDealsWithGold:Z

    .line 55
    return-void
.end method

.method public setGamesWithGoldHeaderText(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->showHeaderTextGamesWithGold:Z

    .line 47
    return-void
.end method

.method public setGoldResultItemSection(Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;)V
    .locals 0
    .param p1, "section"    # Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->section:Lcom/microsoft/xbox/service/network/managers/GoldResultItemSection;

    .line 63
    return-void
.end method

.method public showDealsWithGoldHeaderText()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->showHeaderTextDealsWithGold:Z

    return v0
.end method

.method public showGamesWithGoldHeaderText()Z
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/GoldLoungeDataResultContainer$GoldLoungeDataResult$GoldLoungeItem;->showHeaderTextGamesWithGold:Z

    return v0
.end method
