.class public interface abstract Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog$DialogScreen;
.super Ljava/lang/Object;
.source "XLEMultiScreenDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/XLEMultiScreenDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DialogScreen"
.end annotation


# virtual methods
.method public abstract isCreated()Z
.end method

.method public abstract isStarted()Z
.end method

.method public abstract onCreate()V
.end method

.method public abstract onDestroy()V
.end method

.method public abstract onStart()V
.end method

.method public abstract onStop()V
.end method
