.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction$GameHub;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GameHub"
.end annotation


# static fields
.field public static final Achievements:Ljava/lang/String; = "Game Hub - Achievements"

.field public static final Activity:Ljava/lang/String; = "Game Hub - Activity"

.field public static final Captures:Ljava/lang/String; = "Game Hub - Captures"

.field public static final Compare:Ljava/lang/String; = "Game Hub - Compare With Friend"

.field public static final FindLFGForAchievement:Ljava/lang/String; = "Achievement - Find LFG For Achievement"

.field public static final Follow:Ljava/lang/String; = "Game Hub - Follow"

.field public static final Friends:Ljava/lang/String; = "Game Hub - Friends"

.field public static final Info:Ljava/lang/String; = "Game Hub - Info"

.field public static final LFG:Ljava/lang/String; = "Game Hub - LFG"

.field public static final NavigateToSuggestedClub:Ljava/lang/String; = "Clubs - Game Hub Navigate to Suggested Club"

.field public static final OwnedClub:Ljava/lang/String; = "Game Hub - Navigate To Owned Club"

.field public static final Play:Ljava/lang/String; = "Game Hub - Play"

.field public static final SeeAllClubs:Ljava/lang/String; = "Clubs - Game Hub See All Clubs"

.field public static final ShowAchievement:Ljava/lang/String; = "Game Hub - Show Achievement Details"

.field public static final ShowFriendProfile:Ljava/lang/String; = "Game Hub - Show Friend Profile"

.field public static final ShowGameStatistic:Ljava/lang/String; = "Game Hub - Show Game Statistic"

.field public static final ShowLeaderBoard:Ljava/lang/String; = "Game Hub - Show Leaderboard Details"

.field public static final SpotlightSelected:Ljava/lang/String; = "Game Hub - Spotlight Selected"

.field public static final Store:Ljava/lang/String; = "Game Hub - Show in store"

.field public static final SuggestedClub:Ljava/lang/String; = "Game Hub - Navigate To Suggested Club"

.field public static final Unfollow:Ljava/lang/String; = "Game Hub - Unfollow"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 787
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
