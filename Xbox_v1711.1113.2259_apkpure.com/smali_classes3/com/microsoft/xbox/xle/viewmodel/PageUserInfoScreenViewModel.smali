.class public Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;
.source "PageUserInfoScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadFollowingStateAsyncTask;,
        Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private description:Ljava/lang/String;

.field private isFollowUnfollowLoading:Z

.field private isFollowingPage:Z

.field private isLoadingFollowingState:Z

.field private isLoadingInfo:Z

.field private loadFollowingStateAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadFollowingStateAsyncTask;

.field private loadPageInfoAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;

.field private pageColor:I

.field private pageIconURI:Ljava/lang/String;

.field private pageId:Ljava/lang/String;

.field private pageInfo:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;

.field private pageModel:Lcom/microsoft/xbox/service/model/PagesModel$PageModel;

.field private pageName:Ljava/lang/String;

.field private toggleFollowUnfollowAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;

.field private viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 3
    .param p1, "screen"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 30
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 40
    const-string v1, ""

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageId:Ljava/lang/String;

    .line 42
    sget v1, Lcom/microsoft/xbox/service/model/ProfileModel;->DEFAULT_PROFILE_PRIMARY_COLOR:I

    iput v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageColor:I

    .line 44
    const-string v1, ""

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->description:Ljava/lang/String;

    .line 49
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getPageUserInfoScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 50
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getActivityParameters()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    move-result-object v0

    .line 51
    .local v0, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->getPageUserAuthorInfo()Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->updatePageInfo(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;)V

    .line 52
    invoke-static {}, Lcom/microsoft/xbox/service/model/PagesModel;->getInstance()Lcom/microsoft/xbox/service/model/PagesModel;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/PagesModel;->getPageModel(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/PagesModel$PageModel;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageModel:Lcom/microsoft/xbox/service/model/PagesModel$PageModel;

    .line 53
    return-void
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->isFollowingPage:Z

    return v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->onLoadPageInfoCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;)Lcom/microsoft/xbox/service/model/PagesModel$PageModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageModel:Lcom/microsoft/xbox/service/model/PagesModel$PageModel;

    return-object v0
.end method

.method static synthetic access$502(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->isLoadingInfo:Z

    return p1
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->onLoadFollowingStateCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method static synthetic access$702(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->isLoadingFollowingState:Z

    return p1
.end method

.method static synthetic access$802(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;
    .param p1, "x1"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->isFollowUnfollowLoading:Z

    return p1
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->onToggleFollowUnfollowCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method private onLoadFollowingStateCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 185
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "onLoadFollowingStateCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->isLoadingFollowingState:Z

    .line 187
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 198
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->updateAdapter()V

    .line 199
    return-void

    .line 191
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->isFollowingPage(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->isFollowingPage:Z

    goto :goto_0

    .line 195
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to load page following state"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 187
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onLoadPageInfoCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 164
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "onLoadPageInfoCompleted"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->isLoadingInfo:Z

    .line 167
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 181
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->updateAdapter()V

    .line 182
    return-void

    .line 171
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageModel:Lcom/microsoft/xbox/service/model/PagesModel$PageModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/PagesModel$PageModel;->getResult()Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageInfo:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;

    .line 172
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->updateViewMdoelState()V

    .line 173
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->updatePageInfo()V

    goto :goto_0

    .line 177
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 178
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->TAG:Ljava/lang/String;

    const-string v1, "Failed to load page info"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 167
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private onToggleFollowUnfollowCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    const/4 v0, 0x0

    .line 202
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "onToggleFollowUnfollowCompleted"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->isFollowUnfollowLoading:Z

    .line 204
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 214
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->updateAdapter()V

    .line 215
    return-void

    .line 206
    :pswitch_0
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->isFollowingPage:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->isFollowingPage:Z

    goto :goto_0

    .line 210
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to change following state, page id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageId:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v1

    const v2, 0x7f0705cb

    invoke-virtual {v1, v2, v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->showToast(II)V

    goto :goto_0

    .line 204
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private updatePageInfo()V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageInfo:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageInfo:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;->profileName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageName:Ljava/lang/String;

    .line 145
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageInfo:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;->bgColor()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->convertColorFromString(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageColor:I

    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageInfo:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;->description()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->description:Ljava/lang/String;

    .line 148
    :cond_0
    return-void
.end method

.method private updatePageInfo(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;

    .prologue
    .line 133
    if-eqz p1, :cond_1

    .line 134
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->pageId:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageId:Ljava/lang/String;

    .line 135
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->pageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageName:Ljava/lang/String;

    .line 136
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->color:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo;->color:Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->getPrimaryColor()I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageColor:I

    .line 140
    :goto_1
    return-void

    .line 136
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageColor:I

    goto :goto_0

    .line 138
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_1
.end method

.method private updateViewMdoelState()V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageInfo:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;

    if-nez v0, :cond_1

    .line 152
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->isBusy()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 160
    :goto_0
    return-void

    .line 155
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0

    .line 158
    :cond_1
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    goto :goto_0
.end method


# virtual methods
.method public followUnfollow()V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->toggleFollowUnfollowAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->toggleFollowUnfollowAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;->cancel()V

    .line 128
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->toggleFollowUnfollowAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;

    .line 129
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->toggleFollowUnfollowAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$ToggleFollowUnfollowAsyncTask;->load(Z)V

    .line 130
    return-void
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getPageColor()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageColor:I

    return v0
.end method

.method public getPageIconURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageIconURI:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageInfo:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageInfo:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;->iconImage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageInfo:Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/socialTags/EditorialDataTypes$XboxNewsInfo;->iconImage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageIconURI:Ljava/lang/String;

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageIconURI:Ljava/lang/String;

    return-object v0
.end method

.method public getPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->pageName:Ljava/lang/String;

    return-object v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->isLoadingInfo:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->isLoadingFollowingState:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->isFollowUnfollowLoading:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFollowingPage()Z
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->isFollowingPage:Z

    return v0
.end method

.method public load(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->loadPageInfoAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->loadPageInfoAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;->cancel()V

    .line 66
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->loadPageInfoAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->loadPageInfoAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;->load(Z)V

    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->loadFollowingStateAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadFollowingStateAsyncTask;

    if-eqz v0, :cond_1

    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->loadFollowingStateAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadFollowingStateAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadFollowingStateAsyncTask;->cancel()V

    .line 72
    :cond_1
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadFollowingStateAsyncTask;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadFollowingStateAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->loadFollowingStateAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadFollowingStateAsyncTask;

    .line 73
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->loadFollowingStateAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadFollowingStateAsyncTask;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadFollowingStateAsyncTask;->load(Z)V

    .line 74
    return-void
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 93
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getPageUserInfoScreenAdapter(Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 94
    return-void
.end method

.method protected onStartOverride()V
    .locals 0

    .prologue
    .line 79
    return-void
.end method

.method protected onStopOverride()V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->loadPageInfoAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->loadPageInfoAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadPageInfoAsyncTask;->cancel()V

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->loadFollowingStateAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadFollowingStateAsyncTask;

    if-eqz v0, :cond_1

    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel;->loadFollowingStateAsyncTask:Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadFollowingStateAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/PageUserInfoScreenViewModel$LoadFollowingStateAsyncTask;->cancel()V

    .line 89
    :cond_1
    return-void
.end method
