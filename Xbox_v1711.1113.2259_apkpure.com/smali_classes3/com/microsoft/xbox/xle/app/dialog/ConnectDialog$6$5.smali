.class Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$5;
.super Ljava/lang/Object;
.source "ConnectDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->bindConsoleRow(Lcom/microsoft/xbox/xle/ui/ConnectDialogContainerLayout;Lcom/microsoft/xbox/xle/model/ConsoleData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;

.field final synthetic val$autoConnectCheckbox:Landroid/widget/CheckBox;

.field final synthetic val$console:Lcom/microsoft/xbox/xle/model/ConsoleData;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;Lcom/microsoft/xbox/xle/model/ConsoleData;Landroid/widget/CheckBox;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;

    .prologue
    .line 513
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$5;->this$1:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;

    iput-object p2, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$5;->val$console:Lcom/microsoft/xbox/xle/model/ConsoleData;

    iput-object p3, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$5;->val$autoConnectCheckbox:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 516
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$5;->val$console:Lcom/microsoft/xbox/xle/model/ConsoleData;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$5;->val$autoConnectCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/model/ConsoleData;->setAutoConnect(Z)V

    .line 517
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$5;->this$1:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$1000(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)V

    .line 519
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "Disconnect"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackPageAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$5;->this$1:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$700(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$5;->val$console:Lcom/microsoft/xbox/xle/model/ConsoleData;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ConnectDialogViewModel;->disconnect(Lcom/microsoft/xbox/xle/model/ConsoleData;)V

    .line 521
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6$5;->this$1:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;

    iget-object v0, v0, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog$6;->this$0:Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;->access$800(Lcom/microsoft/xbox/xle/app/dialog/ConnectDialog;)Landroid/widget/BaseAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 522
    return-void
.end method
