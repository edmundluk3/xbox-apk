.class public interface abstract Lcom/microsoft/playready/MediaInfo;
.super Ljava/lang/Object;
.source "MediaInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/playready/MediaInfo$MediaType;
    }
.end annotation


# virtual methods
.method public abstract getAudioInfo()Lcom/microsoft/playready/AudioInfo;
.end method

.method public abstract getFourCC()Ljava/lang/String;
.end method

.method public abstract getLanguage()Ljava/lang/String;
.end method

.method public abstract getMime()Ljava/lang/String;
.end method

.method public abstract getSchemeId()Ljava/lang/String;
.end method

.method public abstract getType()Lcom/microsoft/playready/MediaInfo$MediaType;
.end method

.method public abstract getVideoInfo()Lcom/microsoft/playready/VideoInfo;
.end method
