.class public Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;
.super Ljava/lang/Object;
.source "ActivitiesServiceManager.java"

# interfaces
.implements Lcom/microsoft/xbox/service/network/managers/IActivitiesServiceManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;
    }
.end annotation


# static fields
.field private static final PROVIDERID_PARAM:Ljava/lang/String; = "&providerIds=%s"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private geRequestHeader()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/Header;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 99
    .local v0, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    new-instance v1, Lorg/apache/http/message/BasicHeader;

    const-string v2, "Accept"

    const-string v3, "application/json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    return-object v0
.end method

.method private parseActivityList(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;
    .locals 8
    .param p1, "json"    # Ljava/lang/String;

    .prologue
    .line 104
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;-><init>(Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;)V

    .line 106
    .local v1, "companionList":Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;
    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 107
    .local v5, "jo":Lorg/json/JSONObject;
    const-string v6, "DefaultCompanionIndex"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 109
    .local v4, "ja":Lorg/json/JSONArray;
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-lez v6, :cond_0

    .line 110
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lorg/json/JSONArray;->getInt(I)I

    move-result v6

    iput v6, v1, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;->defaultCompanionIndex:I

    .line 113
    :cond_0
    const-string v6, "Items"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 114
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v3, v6, :cond_2

    .line 115
    invoke-virtual {v4, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;->parseSingleActivity(Lorg/json/JSONObject;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v0

    .line 116
    .local v0, "activity":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    if-eqz v0, :cond_1

    .line 117
    iget-object v6, v1, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;->companions:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 120
    .end local v0    # "activity":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .end local v3    # "i":I
    .end local v4    # "ja":Lorg/json/JSONArray;
    .end local v5    # "jo":Lorg/json/JSONObject;
    :catch_0
    move-exception v2

    .line 121
    .local v2, "e":Lorg/json/JSONException;
    const-string v6, "ActivitiesServiceManager"

    invoke-virtual {v2}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    .end local v2    # "e":Lorg/json/JSONException;
    :cond_2
    return-object v1
.end method

.method private parseAllowedUrls(Lorg/json/JSONObject;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;)V
    .locals 6
    .param p1, "jo"    # Lorg/json/JSONObject;
    .param p2, "launchInfo"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;

    .prologue
    .line 322
    :try_start_0
    const-string v4, "AllowedUrls"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 323
    .local v2, "ja":Lorg/json/JSONArray;
    if-eqz v2, :cond_1

    .line 324
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    new-array v3, v4, [Ljava/lang/String;

    .line 325
    .local v3, "urls":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 326
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    .line 325
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 328
    :cond_0
    invoke-virtual {p2, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->setWhitelistUrls([Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 333
    .end local v1    # "i":I
    .end local v2    # "ja":Lorg/json/JSONArray;
    .end local v3    # "urls":[Ljava/lang/String;
    :cond_1
    :goto_1
    return-void

    .line 330
    :catch_0
    move-exception v0

    .line 331
    .local v0, "e":Lorg/json/JSONException;
    const-string v4, "ActivitiesServiceManager"

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private parseDescription(Lorg/json/JSONObject;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V
    .locals 2
    .param p1, "jo"    # Lorg/json/JSONObject;
    .param p2, "result"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .prologue
    .line 388
    const-string v1, "ReducedDescription"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 389
    .local v0, "description":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 390
    const-string v1, "Description"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 392
    :cond_0
    iput-object v0, p2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->Description:Ljava/lang/String;

    .line 393
    return-void
.end method

.method private parseImages(Lorg/json/JSONObject;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V
    .locals 11
    .param p1, "jo"    # Lorg/json/JSONObject;
    .param p2, "result"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .prologue
    .line 338
    :try_start_0
    const-string v9, "Images"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 339
    .local v6, "ja":Lorg/json/JSONArray;
    const v1, 0x4479c000    # 999.0f

    .line 340
    .local v1, "currentAspectRatioDiff":F
    const/4 v2, 0x0

    .line 341
    .local v2, "currentImageIsLandscape":Z
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_0
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v5, v9, :cond_3

    .line 342
    invoke-virtual {v6, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 343
    .local v7, "temp":Lorg/json/JSONObject;
    const-string v9, "Width"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v8

    .line 344
    .local v8, "width":I
    const-string v9, "Height"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 345
    .local v4, "height":I
    int-to-float v9, v8

    int-to-float v10, v4

    div-float/2addr v9, v10

    const/high16 v10, 0x40000000    # 2.0f

    sub-float v0, v9, v10

    .line 346
    .local v0, "aspectRatioDiff":F
    const/4 v9, 0x0

    cmpl-float v9, v0, v9

    if-lez v9, :cond_2

    .line 348
    if-eqz v2, :cond_0

    cmpg-float v9, v0, v1

    if-gez v9, :cond_1

    .line 349
    :cond_0
    const-string v9, "ResizeUrl"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p2, v9, v8, v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->setIcon2x1Url(Ljava/lang/String;II)V

    .line 350
    move v1, v0

    .line 351
    const/4 v2, 0x1

    .line 341
    :cond_1
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 355
    :cond_2
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 356
    if-nez v2, :cond_1

    cmpg-float v9, v0, v1

    if-gez v9, :cond_1

    .line 357
    const-string v9, "ResizeUrl"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p2, v9, v8, v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->setIcon2x1Url(Ljava/lang/String;II)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 358
    move v1, v0

    .line 359
    const/4 v2, 0x0

    goto :goto_1

    .line 363
    .end local v0    # "aspectRatioDiff":F
    .end local v1    # "currentAspectRatioDiff":F
    .end local v2    # "currentImageIsLandscape":Z
    .end local v4    # "height":I
    .end local v5    # "j":I
    .end local v6    # "ja":Lorg/json/JSONArray;
    .end local v7    # "temp":Lorg/json/JSONObject;
    .end local v8    # "width":I
    :catch_0
    move-exception v3

    .line 364
    .local v3, "e":Lorg/json/JSONException;
    const-string v9, "ActivitiesServiceManager"

    invoke-virtual {v3}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    .end local v3    # "e":Lorg/json/JSONException;
    :cond_3
    return-void
.end method

.method private parseSingleActivity(Lorg/json/JSONObject;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .locals 4
    .param p1, "jo"    # Lorg/json/JSONObject;

    .prologue
    .line 398
    :try_start_0
    const-string v2, "MediaItemType"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 399
    .local v1, "type":Ljava/lang/String;
    const-string v2, "DActivity"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 400
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;->parseSingleDActivity(Lorg/json/JSONObject;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v2

    .line 410
    .end local v1    # "type":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 401
    .restart local v1    # "type":Ljava/lang/String;
    :cond_0
    const-string v2, "DVideoActivity"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 402
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;->parseSingleVideoActivity(Lorg/json/JSONObject;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v2

    goto :goto_0

    .line 404
    :cond_1
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;->parseSingleNativeActivity(Lorg/json/JSONObject;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 406
    .end local v1    # "type":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 407
    .local v0, "e":Lorg/json/JSONException;
    const-string v2, "ActivitiesServiceManager"

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private parseSingleDActivity(Lorg/json/JSONObject;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .locals 11
    .param p1, "jo"    # Lorg/json/JSONObject;

    .prologue
    .line 271
    const/4 v5, 0x0

    .line 274
    .local v5, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    const/4 v1, 0x0

    .line 276
    .local v1, "i":I
    :try_start_0
    new-instance v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-direct {v6}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 277
    .end local v5    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .local v6, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    :try_start_1
    const-string v9, "ID"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->ID:Ljava/lang/String;

    .line 278
    const-string v9, "ImpressionGuid"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->ImpressionGuid:Ljava/lang/String;

    .line 279
    const-string v9, "ReducedName"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->Name:Ljava/lang/String;

    .line 280
    const-string v9, "MediaItemType"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->MediaItemType:Ljava/lang/String;

    .line 282
    invoke-direct {p0, p1, v6}, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;->parseDescription(Lorg/json/JSONObject;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    .line 285
    new-instance v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;

    invoke-direct {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;-><init>()V

    .line 287
    .local v3, "launchInfo":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;
    const-string v9, "Packages"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 288
    .local v2, "ja":Lorg/json/JSONArray;
    const/4 v9, 0x0

    invoke-virtual {v2, v9}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 289
    .local v7, "temp":Lorg/json/JSONObject;
    const-string v9, "HostedActivityUrl"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 290
    .local v8, "url":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 291
    invoke-static {v8}, Lcom/microsoft/xbox/toolkit/UrlUtil;->getUri(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->setActivityUrl(Ljava/net/URI;)V

    .line 294
    :cond_0
    invoke-direct {p0, p1, v3}, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;->parseAllowedUrls(Lorg/json/JSONObject;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;)V

    .line 296
    invoke-virtual {v6, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->setActivityLaunchInfo(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;)V

    .line 299
    const-string v9, "RelatedMedia"

    invoke-virtual {p1, v9}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 300
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v1, v9, :cond_1

    .line 301
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 302
    const-string v9, "RelationType"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 303
    .local v4, "relation":Ljava/lang/String;
    const-string v9, "Parent"

    invoke-static {v4, v9}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 304
    const-string v9, "ID"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->setParentId(Ljava/lang/String;)V

    .line 309
    .end local v4    # "relation":Ljava/lang/String;
    :cond_1
    invoke-direct {p0, p1, v6}, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;->parseImages(Lorg/json/JSONObject;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    .line 311
    const-string v9, "RelatedMedia"

    const-string v10, "HexTitleId"

    invoke-direct {p0, p1, v6, v9, v10}, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;->parseWhiteListTitleIds(Lorg/json/JSONObject;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v5, v6

    .line 317
    .end local v2    # "ja":Lorg/json/JSONArray;
    .end local v3    # "launchInfo":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;
    .end local v6    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .end local v7    # "temp":Lorg/json/JSONObject;
    .end local v8    # "url":Ljava/lang/String;
    .restart local v5    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    :goto_1
    return-object v5

    .line 300
    .end local v5    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .restart local v2    # "ja":Lorg/json/JSONArray;
    .restart local v3    # "launchInfo":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;
    .restart local v4    # "relation":Ljava/lang/String;
    .restart local v6    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .restart local v7    # "temp":Lorg/json/JSONObject;
    .restart local v8    # "url":Ljava/lang/String;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 313
    .end local v2    # "ja":Lorg/json/JSONArray;
    .end local v3    # "launchInfo":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;
    .end local v4    # "relation":Ljava/lang/String;
    .end local v6    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .end local v7    # "temp":Lorg/json/JSONObject;
    .end local v8    # "url":Ljava/lang/String;
    .restart local v5    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    :catch_0
    move-exception v0

    .line 314
    .local v0, "e":Lorg/json/JSONException;
    :goto_2
    const-string v9, "ActivitiesServiceManager"

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 313
    .end local v0    # "e":Lorg/json/JSONException;
    .end local v5    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .restart local v6    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    :catch_1
    move-exception v0

    move-object v5, v6

    .end local v6    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .restart local v5    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    goto :goto_2
.end method

.method private parseSingleNativeActivity(Lorg/json/JSONObject;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .locals 19
    .param p1, "jo"    # Lorg/json/JSONObject;

    .prologue
    .line 191
    const/4 v12, 0x0

    .line 194
    .local v12, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->getDeviceType()Ljava/lang/String;

    move-result-object v4

    .line 195
    .local v4, "deviceType":Ljava/lang/String;
    const/4 v2, 0x0

    .line 196
    .local v2, "contentId":Ljava/lang/String;
    const-string v17, "Availabilities"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 197
    .local v7, "ja":Lorg/json/JSONArray;
    const/4 v6, 0x0

    .line 198
    .local v6, "i":I
    :goto_0
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v17

    move/from16 v0, v17

    if-ge v6, v0, :cond_1

    .line 199
    invoke-virtual {v7, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v17

    const-string v18, "Devices"

    invoke-virtual/range {v17 .. v18}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v14

    .line 200
    .local v14, "temp":Lorg/json/JSONArray;
    const/16 v16, 0x0

    .line 201
    .local v16, "x":I
    :goto_1
    invoke-virtual {v14}, Lorg/json/JSONArray;->length()I

    move-result v17

    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_0

    .line 202
    move/from16 v0, v16

    invoke-virtual {v14, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v17

    const-string v18, "Name"

    invoke-virtual/range {v17 .. v18}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v0, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 203
    invoke-virtual {v7, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v17

    const-string v18, "ContentId"

    invoke-virtual/range {v17 .. v18}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 207
    :cond_0
    invoke-virtual {v14}, Lorg/json/JSONArray;->length()I

    move-result v17

    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_6

    .line 212
    .end local v14    # "temp":Lorg/json/JSONArray;
    .end local v16    # "x":I
    :cond_1
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v17

    move/from16 v0, v17

    if-ge v6, v0, :cond_4

    .line 214
    new-instance v13, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-direct {v13}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 215
    .end local v12    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .local v13, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    :try_start_1
    const-string v17, "ID"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v13, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->ID:Ljava/lang/String;

    .line 216
    const-string v17, "ImpressionGuid"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v13, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->ImpressionGuid:Ljava/lang/String;

    .line 217
    const-string v17, "ReducedName"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v13, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->Name:Ljava/lang/String;

    .line 218
    const-string v17, "MediaItemType"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v13, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->MediaItemType:Ljava/lang/String;

    .line 220
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13}, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;->parseDescription(Lorg/json/JSONObject;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    .line 223
    new-instance v8, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;

    invoke-direct {v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;-><init>()V

    .line 225
    .local v8, "launchInfo":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;
    const-string v17, "Packages"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 226
    const/4 v6, 0x0

    :goto_2
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v17

    move/from16 v0, v17

    if-ge v6, v0, :cond_2

    .line 227
    invoke-virtual {v7, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v14

    .line 228
    .local v14, "temp":Lorg/json/JSONObject;
    const-string v17, "ContentId"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v0, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 229
    const-string v17, "DeeplinkUrl"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 230
    .local v15, "url":Ljava/lang/String;
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_2

    .line 232
    const-string/jumbo v17, "|"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v10

    .line 233
    .local v10, "pos":I
    if-lez v10, :cond_7

    .line 234
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v15, v0, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 235
    .local v9, "packageName":Ljava/lang/String;
    add-int/lit8 v17, v10, 0x1

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 236
    .local v3, "deepLink":Ljava/lang/String;
    invoke-virtual {v8, v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->setDeepLinkUrl(Ljava/lang/String;)V

    .line 237
    invoke-virtual {v8, v9}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->setPackageName(Ljava/lang/String;)V

    .line 248
    .end local v3    # "deepLink":Ljava/lang/String;
    .end local v9    # "packageName":Ljava/lang/String;
    .end local v10    # "pos":I
    .end local v14    # "temp":Lorg/json/JSONObject;
    .end local v15    # "url":Ljava/lang/String;
    :cond_2
    :goto_3
    invoke-virtual {v13, v8}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->setActivityLaunchInfo(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;)V

    .line 251
    const-string v17, "RelatedMedia"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 252
    const/4 v6, 0x0

    :goto_4
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v17

    move/from16 v0, v17

    if-ge v6, v0, :cond_3

    .line 253
    invoke-virtual {v7, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v14

    .line 254
    .restart local v14    # "temp":Lorg/json/JSONObject;
    const-string v17, "RelationType"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 255
    .local v11, "relation":Ljava/lang/String;
    const-string v17, "Parent"

    move-object/from16 v0, v17

    invoke-static {v11, v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 256
    const-string v17, "ID"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->setParentId(Ljava/lang/String;)V

    .line 261
    .end local v11    # "relation":Ljava/lang/String;
    .end local v14    # "temp":Lorg/json/JSONObject;
    :cond_3
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v13}, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;->parseImages(Lorg/json/JSONObject;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    move-object v12, v13

    .line 267
    .end local v2    # "contentId":Ljava/lang/String;
    .end local v4    # "deviceType":Ljava/lang/String;
    .end local v6    # "i":I
    .end local v7    # "ja":Lorg/json/JSONArray;
    .end local v8    # "launchInfo":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;
    .end local v13    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .restart local v12    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    :cond_4
    :goto_5
    return-object v12

    .line 201
    .restart local v2    # "contentId":Ljava/lang/String;
    .restart local v4    # "deviceType":Ljava/lang/String;
    .restart local v6    # "i":I
    .restart local v7    # "ja":Lorg/json/JSONArray;
    .local v14, "temp":Lorg/json/JSONArray;
    .restart local v16    # "x":I
    :cond_5
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_1

    .line 198
    :cond_6
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 241
    .end local v12    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .end local v16    # "x":I
    .restart local v8    # "launchInfo":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;
    .restart local v10    # "pos":I
    .restart local v13    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .local v14, "temp":Lorg/json/JSONObject;
    .restart local v15    # "url":Ljava/lang/String;
    :cond_7
    const-string v17, "market://details?id=com.microsoft.smartglass"

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->setDeepLinkUrl(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 263
    .end local v8    # "launchInfo":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;
    .end local v10    # "pos":I
    .end local v14    # "temp":Lorg/json/JSONObject;
    .end local v15    # "url":Ljava/lang/String;
    :catch_0
    move-exception v5

    move-object v12, v13

    .line 264
    .end local v2    # "contentId":Ljava/lang/String;
    .end local v4    # "deviceType":Ljava/lang/String;
    .end local v6    # "i":I
    .end local v7    # "ja":Lorg/json/JSONArray;
    .end local v13    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .local v5, "e":Lorg/json/JSONException;
    .restart local v12    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    :goto_6
    const-string v17, "ActivitiesServiceManager"

    invoke-virtual {v5}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 226
    .end local v5    # "e":Lorg/json/JSONException;
    .end local v12    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .restart local v2    # "contentId":Ljava/lang/String;
    .restart local v4    # "deviceType":Ljava/lang/String;
    .restart local v6    # "i":I
    .restart local v7    # "ja":Lorg/json/JSONArray;
    .restart local v8    # "launchInfo":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;
    .restart local v13    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .restart local v14    # "temp":Lorg/json/JSONObject;
    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_2

    .line 252
    .restart local v11    # "relation":Ljava/lang/String;
    :cond_9
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 263
    .end local v2    # "contentId":Ljava/lang/String;
    .end local v4    # "deviceType":Ljava/lang/String;
    .end local v6    # "i":I
    .end local v7    # "ja":Lorg/json/JSONArray;
    .end local v8    # "launchInfo":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;
    .end local v11    # "relation":Ljava/lang/String;
    .end local v13    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .end local v14    # "temp":Lorg/json/JSONObject;
    .restart local v12    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    :catch_1
    move-exception v5

    goto :goto_6
.end method

.method private parseSingleVideoActivity(Lorg/json/JSONObject;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .locals 20
    .param p1, "jo"    # Lorg/json/JSONObject;

    .prologue
    .line 127
    const/4 v14, 0x0

    .line 130
    .local v14, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->getDeviceType()Ljava/lang/String;

    move-result-object v4

    .line 131
    .local v4, "deviceType":Ljava/lang/String;
    const-string v18, "Devices"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    .line 134
    .local v9, "ja":Lorg/json/JSONArray;
    const/4 v6, 0x0

    .line 135
    .local v6, "i":I
    :goto_0
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v18

    move/from16 v0, v18

    if-ge v6, v0, :cond_0

    .line 136
    invoke-virtual {v9, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v18

    const-string v19, "Name"

    invoke-virtual/range {v18 .. v19}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v0, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 141
    :cond_0
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v18

    move/from16 v0, v18

    if-ge v6, v0, :cond_3

    .line 143
    new-instance v15, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    invoke-direct {v15}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    .end local v14    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .local v15, "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    :try_start_1
    const-string v18, "ID"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v15, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->ID:Ljava/lang/String;

    .line 145
    const-string v18, "ImpressionGuid"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v15, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->ImpressionGuid:Ljava/lang/String;

    .line 146
    const-string v18, "ReducedName"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v15, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->Name:Ljava/lang/String;

    .line 147
    const-string v18, "MediaItemType"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v15, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->MediaItemType:Ljava/lang/String;

    .line 148
    const-string v18, "Description"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v15, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->Description:Ljava/lang/String;

    .line 150
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v15}, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;->parseDescription(Lorg/json/JSONObject;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    .line 153
    new-instance v10, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;

    invoke-direct {v10}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;-><init>()V

    .line 155
    .local v10, "launchInfo":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;
    const-string v18, "Url"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 156
    .local v17, "url":Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_1

    .line 158
    const-string v18, " "

    const-string v19, ""

    invoke-virtual/range {v17 .. v19}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/microsoft/xbox/toolkit/UrlUtil;->getUri(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->setActivityUrl(Ljava/net/URI;)V

    .line 161
    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v10}, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;->parseAllowedUrls(Lorg/json/JSONObject;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;)V

    .line 163
    invoke-virtual {v15, v10}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->setActivityLaunchInfo(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;)V

    .line 166
    const-string v18, "ParentItems"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    .line 167
    const/4 v8, 0x0

    .local v8, "j":I
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v18

    move/from16 v0, v18

    if-ge v8, v0, :cond_2

    .line 168
    invoke-virtual {v9, v8}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v16

    .line 169
    .local v16, "temp":Lorg/json/JSONObject;
    const-string v18, "MediaGroup"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 170
    .local v11, "pGroup":Ljava/lang/String;
    const-string v18, "MediaItemType"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 171
    .local v13, "pType":Ljava/lang/String;
    const-string v18, "ID"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 172
    .local v12, "pID":Ljava/lang/String;
    new-instance v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-direct {v7}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 173
    .local v7, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    iput-object v12, v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    .line 174
    iput-object v13, v7, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    .line 178
    .end local v7    # "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .end local v11    # "pGroup":Ljava/lang/String;
    .end local v12    # "pID":Ljava/lang/String;
    .end local v13    # "pType":Ljava/lang/String;
    .end local v16    # "temp":Lorg/json/JSONObject;
    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v15}, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;->parseImages(Lorg/json/JSONObject;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    .line 180
    const-string v18, "AllowedProviders"

    const-string v19, "ID"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v18

    move-object/from16 v3, v19

    invoke-direct {v0, v1, v15, v2, v3}, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;->parseWhiteListTitleIds(Lorg/json/JSONObject;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v14, v15

    .line 186
    .end local v4    # "deviceType":Ljava/lang/String;
    .end local v6    # "i":I
    .end local v8    # "j":I
    .end local v9    # "ja":Lorg/json/JSONArray;
    .end local v10    # "launchInfo":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;
    .end local v15    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .end local v17    # "url":Ljava/lang/String;
    .restart local v14    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    :cond_3
    :goto_1
    return-object v14

    .line 135
    .restart local v4    # "deviceType":Ljava/lang/String;
    .restart local v6    # "i":I
    .restart local v9    # "ja":Lorg/json/JSONArray;
    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 182
    .end local v4    # "deviceType":Ljava/lang/String;
    .end local v6    # "i":I
    .end local v9    # "ja":Lorg/json/JSONArray;
    :catch_0
    move-exception v5

    .line 183
    .local v5, "e":Lorg/json/JSONException;
    :goto_2
    const-string v18, "ActivitiesServiceManager"

    invoke-virtual {v5}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 182
    .end local v5    # "e":Lorg/json/JSONException;
    .end local v14    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .restart local v4    # "deviceType":Ljava/lang/String;
    .restart local v6    # "i":I
    .restart local v9    # "ja":Lorg/json/JSONArray;
    .restart local v15    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    :catch_1
    move-exception v5

    move-object v14, v15

    .end local v15    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .restart local v14    # "result":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    goto :goto_2
.end method

.method private parseWhiteListTitleIds(Lorg/json/JSONObject;Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "jo"    # Lorg/json/JSONObject;
    .param p2, "result"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .param p3, "jsonArrayKey"    # Ljava/lang/String;
    .param p4, "jsonObjectKey"    # Ljava/lang/String;

    .prologue
    .line 370
    :try_start_0
    invoke-virtual {p1, p3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 371
    .local v3, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 372
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 373
    .local v4, "temp":Lorg/json/JSONObject;
    invoke-virtual {v4, p4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 374
    .local v1, "hexTitleId":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 375
    const-string v5, "0x"

    invoke-virtual {v1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 377
    const/4 v5, 0x2

    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 379
    :cond_0
    const/16 v5, 0x10

    invoke-static {v1, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {p2, v5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->addAllowedTitleId(I)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 371
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 382
    .end local v1    # "hexTitleId":Ljava/lang/String;
    .end local v2    # "j":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "temp":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 383
    .local v0, "e":Lorg/json/JSONException;
    const-string v5, "ActivitiesServiceManager"

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    .end local v0    # "e":Lorg/json/JSONException;
    :cond_2
    return-void
.end method


# virtual methods
.method public getCompanions(Ljava/lang/String;Ljava/lang/String;I)Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;
    .locals 6
    .param p1, "mediaId"    # Ljava/lang/String;
    .param p2, "mediaItemType"    # Ljava/lang/String;
    .param p3, "mediaGroup"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 37
    const-wide/16 v4, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;->getCompanions(Ljava/lang/String;Ljava/lang/String;IJ)Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;

    move-result-object v0

    return-object v0
.end method

.method public getCompanions(Ljava/lang/String;Ljava/lang/String;IJ)Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;
    .locals 16
    .param p1, "mediaItemId"    # Ljava/lang/String;
    .param p2, "mediaItemType"    # Ljava/lang/String;
    .param p3, "mediaGroup"    # I
    .param p4, "providerTitleId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 45
    const/4 v6, 0x0

    .line 93
    :cond_0
    :goto_0
    return-object v6

    .line 48
    :cond_1
    invoke-static/range {p3 .. p3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaGroup;->getGroupStringName(I)Ljava/lang/String;

    move-result-object v4

    .line 49
    .local v4, "mediaGroupString":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 50
    const-string v9, "ActivitiesServiceManager"

    const-string v10, "getCompanions mediaGroup is unknown!"

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    :cond_2
    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getCompanionUrlFormat()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x5

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v13

    invoke-virtual {v13}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    aput-object p1, v11, v12

    const/4 v12, 0x2

    aput-object v4, v11, v12

    const/4 v12, 0x3

    aput-object p2, v11, v12

    const/4 v12, 0x4

    .line 53
    invoke-static {}, Lcom/microsoft/xbox/toolkit/system/SystemUtil;->getDeviceType()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    .line 52
    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 55
    .local v8, "url":Ljava/lang/String;
    const-wide/16 v10, -0x1

    cmp-long v9, p4, v10

    if-eqz v9, :cond_3

    .line 56
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v11, "&providerIds=%s"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    move-wide/from16 v0, p4

    long-to-int v14, v0

    const/16 v15, 0x10

    invoke-static {v14, v15}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 59
    :cond_3
    const/4 v6, 0x0

    .line 61
    .local v6, "result":Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;
    invoke-direct/range {p0 .. p0}, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;->geRequestHeader()Ljava/util/ArrayList;

    move-result-object v3

    .line 62
    .local v3, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/apache/http/Header;>;"
    const/4 v7, 0x0

    .line 63
    .local v7, "statusAndStream":Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;
    const-string v9, "ActivitiesServiceManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "requesting "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    :try_start_0
    invoke-static {v8, v3}, Lcom/microsoft/xbox/service/network/managers/ServiceCommon;->getStreamAndStatus(Ljava/lang/String;Ljava/util/List;)Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;

    move-result-object v7

    .line 66
    iget v9, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->statusCode:I

    const/16 v10, 0xc8

    if-ne v9, v10, :cond_4

    .line 67
    iget-object v9, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-static {v9}, Lcom/microsoft/xbox/toolkit/StreamUtil;->ReadAsString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v5

    .line 68
    .local v5, "response":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager;->parseActivityList(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/ActivitiesServiceManager$CompanionList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 85
    if-eqz v7, :cond_0

    iget-object v9, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v9, :cond_0

    .line 87
    :try_start_1
    iget-object v9, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v9}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 88
    :catch_0
    move-exception v9

    goto/16 :goto_0

    .line 70
    .end local v5    # "response":Ljava/lang/String;
    :cond_4
    :try_start_2
    const-string v9, "ActivitiesServiceManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "failed for "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    new-instance v9, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v10, 0xfb6

    invoke-direct {v9, v10, v11}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    throw v9
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 74
    :catch_1
    move-exception v2

    .line 75
    .local v2, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v9, "ActivitiesServiceManager"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    instance-of v9, v2, Lcom/microsoft/xbox/toolkit/XLEException;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v9, :cond_5

    .line 78
    :try_start_4
    throw v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 79
    :catch_2
    move-exception v9

    .line 82
    :cond_5
    :try_start_5
    new-instance v9, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v10, 0xfb6

    invoke-direct {v9, v10, v11, v2}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(JLjava/lang/Throwable;)V

    throw v9
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 85
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v9

    if-eqz v7, :cond_6

    iget-object v10, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    if-eqz v10, :cond_6

    .line 87
    :try_start_6
    iget-object v10, v7, Lcom/microsoft/xbox/toolkit/network/XLEHttpStatusAndStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v10}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    .line 89
    :cond_6
    :goto_1
    throw v9

    .line 88
    :catch_3
    move-exception v10

    goto :goto_1
.end method
