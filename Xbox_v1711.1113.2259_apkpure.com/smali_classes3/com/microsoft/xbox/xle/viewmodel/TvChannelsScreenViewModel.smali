.class public abstract Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;
.super Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;
.source "TvChannelsScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel;
.implements Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;
.implements Lcom/microsoft/xbox/xle/epg/AppChannelsModel$IListener;


# static fields
.field public static ALL_CHANNELS:I

.field public static ALL_FAVORITE_CHANNELS:I

.field public static APP_CHANNELS:I

.field public static FAVORITES_ONLY:I

.field public static TV_CHANNELS:I


# instance fields
.field protected appChannels:[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

.field protected appChannelsHashCode:I

.field protected contentState:Lcom/microsoft/xbox/toolkit/network/ListState;

.field protected diagTag:Ljava/lang/String;

.field protected flags:I

.field protected isLoading:Z

.field protected listeners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;",
            ">;"
        }
    .end annotation
.end field

.field protected modelChanged:Z

.field protected tvChannels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

.field protected tvChannelsHashCode:I

.field protected waitingForProfileUpdate:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    const/4 v0, 0x1

    sput v0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->TV_CHANNELS:I

    .line 34
    const/4 v0, 0x2

    sput v0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->APP_CHANNELS:I

    .line 35
    const/4 v0, 0x4

    sput v0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->FAVORITES_ONLY:I

    .line 36
    sget v0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->TV_CHANNELS:I

    sget v1, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->APP_CHANNELS:I

    or-int/2addr v0, v1

    sput v0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->ALL_CHANNELS:I

    .line 37
    sget v0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->ALL_CHANNELS:I

    sget v1, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->FAVORITES_ONLY:I

    or-int/2addr v0, v1

    sput v0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->ALL_FAVORITE_CHANNELS:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "flags"    # I

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;-><init>()V

    .line 41
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->contentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 54
    iput p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->flags:I

    .line 55
    const-string v0, "TvChannelsScreenViewModel"

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    .line 56
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->listeners:Ljava/util/HashSet;

    .line 57
    return-void
.end method

.method private hasErrors()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 184
    sget v1, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->TV_CHANNELS:I

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->hasFlag(I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getIteratorFactory()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->isError()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 192
    :cond_0
    :goto_0
    return v0

    .line 188
    :cond_1
    sget v1, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->APP_CHANNELS:I

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->hasFlag(I)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->getDefault()Lcom/microsoft/xbox/xle/epg/AppChannelsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->isError()Z

    move-result v1

    if-nez v1, :cond_0

    .line 192
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setLoading(Z)V
    .locals 3
    .param p1, "loading"    # Z

    .prologue
    .line 196
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->isLoading:Z

    if-ne v0, p1, :cond_1

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Set loading "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->isLoading:Z

    .line 202
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    instance-of v0, v0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->updateViewLoadingIndicator()V

    goto :goto_0
.end method


# virtual methods
.method public addListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;

    .prologue
    .line 532
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->listeners:Ljava/util/HashSet;

    if-eqz v0, :cond_0

    .line 533
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->listeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 534
    :cond_0
    return-void
.end method

.method public getChannel(I)Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Channel;
    .locals 2
    .param p1, "index"    # I

    .prologue
    const/4 v0, 0x0

    .line 476
    if-gez p1, :cond_1

    .line 493
    :cond_0
    :goto_0
    return-object v0

    .line 479
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->tvChannels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    if-eqz v1, :cond_3

    .line 481
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->tvChannels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    array-length v1, v1

    if-ge p1, v1, :cond_2

    .line 482
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->tvChannels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    aget-object v1, v1, p1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewLiveTVChannelModel;-><init>(Lcom/microsoft/xbox/service/model/epg/EPGChannel;)V

    goto :goto_0

    .line 484
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->tvChannels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    array-length v1, v1

    sub-int/2addr p1, v1

    .line 487
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->appChannels:[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    if-eqz v1, :cond_0

    .line 489
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->appChannels:[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    array-length v1, v1

    if-ge p1, v1, :cond_0

    .line 490
    new-instance v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewAppChannelModel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->appChannels:[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    aget-object v1, v1, p1

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewAppChannelModel;-><init>(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;)V

    goto :goto_0
.end method

.method public getChannelCount()I
    .locals 2

    .prologue
    .line 453
    const/4 v0, 0x0

    .line 455
    .local v0, "count":I
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->tvChannels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    if-eqz v1, :cond_0

    .line 456
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->tvChannels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    array-length v1, v1

    add-int/2addr v0, v1

    .line 458
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->appChannels:[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    if-eqz v1, :cond_1

    .line 459
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->appChannels:[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    array-length v1, v1

    add-int/2addr v0, v1

    .line 461
    :cond_1
    return v0
.end method

.method public getHasData()Z
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->getChannelCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLiveTvChannelCount()I
    .locals 2

    .prologue
    .line 466
    const/4 v0, 0x0

    .line 468
    .local v0, "count":I
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->tvChannels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    if-eqz v1, :cond_0

    .line 469
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->tvChannels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    array-length v1, v1

    add-int/2addr v0, v1

    .line 471
    :cond_0
    return v0
.end method

.method public getProgramEnumerator(IJZ)Lcom/microsoft/xbox/service/model/epg/EPGIterator;
    .locals 4
    .param p1, "index"    # I
    .param p2, "time"    # J
    .param p4, "forward"    # Z

    .prologue
    const/4 v1, 0x0

    .line 498
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->tvChannels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v2

    if-nez v2, :cond_1

    .line 510
    :cond_0
    :goto_0
    return-object v1

    .line 501
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->tvChannels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    array-length v2, v2

    if-ge p1, v2, :cond_0

    .line 504
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->tvChannels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    aget-object v0, v2, p1

    .line 507
    .local v0, "channel":Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getIsFoundChannel()Z

    move-result v2

    if-nez v2, :cond_0

    .line 510
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getIteratorFactory()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    move-result-object v1

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGChannel;->getChannelOrdinal()I

    move-result v2

    long-to-int v3, p2

    invoke-virtual {v1, v2, v3, p4}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->getEPGIterator(IIZ)Lcom/microsoft/xbox/service/model/epg/EPGIterator;

    move-result-object v1

    goto :goto_0
.end method

.method public getProviderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    sget v0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->TV_CHANNELS:I

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->hasFlag(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v0

    if-nez v0, :cond_1

    .line 89
    :cond_0
    const/4 v0, 0x0

    .line 91
    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getProviderName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTimeSpan()I
    .locals 1

    .prologue
    .line 448
    const v0, 0xfd200

    return v0
.end method

.method public getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->contentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    return-object v0
.end method

.method protected hasFlag(I)Z
    .locals 1
    .param p1, "flag"    # I

    .prologue
    .line 60
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->flags:I

    and-int/2addr v0, p1

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBusy()Z
    .locals 1

    .prologue
    .line 318
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->isLoading:Z

    return v0
.end method

.method public load(Z)V
    .locals 5
    .param p1, "forceRefresh"    # Z

    .prologue
    const/4 v4, 0x1

    .line 367
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Load "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    if-eqz p1, :cond_0

    .line 370
    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->isActive:Z

    if-nez v1, :cond_1

    .line 386
    :cond_0
    :goto_0
    return-void

    .line 374
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->getInstance()Lcom/microsoft/xbox/xle/urc/net/BranchSession;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/urc/net/BranchSession;->requestHeadend()Z

    .line 377
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 378
    .local v0, "currentProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_2

    .line 379
    iput-boolean v4, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->waitingForProfileUpdate:Z

    .line 380
    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadAsync(Z)V

    .line 383
    :cond_2
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->setViewModelState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 384
    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->refresh(Z)V

    goto :goto_0
.end method

.method public onActiveProviderChanged(Lcom/microsoft/xbox/service/model/epg/EPGProvider;)V
    .locals 1
    .param p1, "newProvider"    # Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    .prologue
    .line 393
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->updateViewModelState()V

    .line 394
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->refresh(Z)V

    .line 395
    return-void
.end method

.method public onDataChanged()V
    .locals 2

    .prologue
    .line 439
    const/4 v0, 0x0

    const v1, 0x7fffffff

    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->processDataChange(II)V

    .line 440
    return-void
.end method

.method public onDataChanged(II)V
    .locals 0
    .param p1, "startChannel"    # I
    .param p2, "endChannel"    # I

    .prologue
    .line 404
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->processDataChange(II)V

    .line 405
    return-void
.end method

.method public onFavoritesError(Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;)V
    .locals 3
    .param p1, "result"    # Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;

    .prologue
    const/4 v2, 0x0

    .line 409
    sget-object v0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel$1;->$SwitchMap$com$microsoft$xbox$service$model$epg$EPGChannel$SetFavoriteResult:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPGChannel$SetFavoriteResult;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 423
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onFavoritesError called with unknown result: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    :goto_0
    :pswitch_0
    return-void

    .line 411
    :pswitch_1
    const v0, 0x7f070448

    invoke-static {v2, v0, v2}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;->show(IIZ)V

    goto :goto_0

    .line 414
    :pswitch_2
    const v0, 0x7f070451

    invoke-static {v2, v0, v2}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;->show(IIZ)V

    goto :goto_0

    .line 417
    :pswitch_3
    const v0, 0x7f070452

    invoke-static {v2, v0, v2}, Lcom/microsoft/xbox/xle/epg/EPGErrorDialog;->show(IIZ)V

    goto :goto_0

    .line 409
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method public onFetchingStatusChanged()V
    .locals 0

    .prologue
    .line 399
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->updateViewModelState()V

    .line 400
    return-void
.end method

.method public onSetActive()V
    .locals 2

    .prologue
    .line 359
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    const-string v1, "VM onSetActive"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/PivotViewModelBase;->onSetActive()V

    .line 362
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->refresh(Z)V

    .line 363
    return-void
.end method

.method protected onStartOverride()V
    .locals 3

    .prologue
    .line 323
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    const-string v2, "VM onStartOverride"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    sget v1, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->TV_CHANNELS:I

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->hasFlag(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 326
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->addListener(Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;)V

    .line 328
    :cond_0
    sget v1, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->APP_CHANNELS:I

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->hasFlag(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 329
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->getDefault()Lcom/microsoft/xbox/xle/epg/AppChannelsModel;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->start(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$IListener;)V

    .line 332
    :cond_1
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->updateLoadingStatus()V

    .line 334
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 335
    .local v0, "currentProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_2

    .line 336
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/ProfileModel;->addObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 338
    :cond_2
    return-void
.end method

.method public onStateChanged()V
    .locals 0

    .prologue
    .line 434
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->updateLoadingStatus()V

    .line 435
    return-void
.end method

.method protected onStopOverride()V
    .locals 3

    .prologue
    .line 342
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    const-string v2, "VM onStopOverride"

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    sget v1, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->TV_CHANNELS:I

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->hasFlag(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 345
    invoke-static {p0}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->removeListener(Lcom/microsoft/xbox/service/model/epg/EPGModel$IActiveListener;)V

    .line 347
    :cond_0
    sget v1, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->APP_CHANNELS:I

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->hasFlag(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 348
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->getDefault()Lcom/microsoft/xbox/xle/epg/AppChannelsModel;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->stop(Lcom/microsoft/xbox/xle/epg/AppChannelsModel$IListener;)V

    .line 351
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 352
    .local v0, "currentProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_2

    .line 353
    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/service/model/ProfileModel;->removeObserver(Lcom/microsoft/xbox/toolkit/XLEObserver;)Z

    .line 355
    :cond_2
    return-void
.end method

.method protected processDataChange(II)V
    .locals 5
    .param p1, "startChannel"    # I
    .param p2, "endChannel"    # I

    .prologue
    .line 293
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    .line 295
    .local v1, "previousState":Lcom/microsoft/xbox/toolkit/network/ListState;
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->updateViewModelState()V

    .line 298
    iget-boolean v3, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->modelChanged:Z

    if-nez v3, :cond_1

    .line 301
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v3, v4, :cond_1

    .line 305
    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v1, v3, :cond_0

    const/4 v2, 0x1

    .line 306
    .local v2, "refresh":Z
    :goto_0
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->listeners:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;

    .line 307
    .local v0, "listener":Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;
    invoke-interface {v0, p1, p2, v2}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;->dataArrived(IIZ)V

    goto :goto_1

    .line 305
    .end local v0    # "listener":Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;
    .end local v2    # "refresh":Z
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 311
    :cond_1
    return-void
.end method

.method protected refresh(Z)V
    .locals 5
    .param p1, "isExplicit"    # Z

    .prologue
    .line 159
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    const-string v1, "Refresh"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v0

    if-nez v0, :cond_1

    sget v0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->APP_CHANNELS:I

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->hasFlag(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 163
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->isLoading:Z

    .line 164
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->setViewModelState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 181
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->TV_CHANNELS:I

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->hasFlag(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 170
    if-eqz p1, :cond_2

    .line 171
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->getInstance()Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;

    move-result-object v0

    const-string v1, "EpgLoad"

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getHeadend()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    const-string v4, ""

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/microsoft/xbox/service/network/managers/VortexServiceManager;->trackOneGuideEpgPageAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getIteratorFactory()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->refresh()V

    .line 178
    :cond_3
    sget v0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->APP_CHANNELS:I

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->hasFlag(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->getDefault()Lcom/microsoft/xbox/xle/epg/AppChannelsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->refresh()V

    goto :goto_0
.end method

.method public removeListener(Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;

    .prologue
    .line 538
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->listeners:Ljava/util/HashSet;

    if-eqz v0, :cond_0

    .line 539
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->listeners:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 540
    :cond_0
    return-void
.end method

.method protected setViewModelState(Lcom/microsoft/xbox/toolkit/network/ListState;)V
    .locals 3
    .param p1, "state"    # Lcom/microsoft/xbox/toolkit/network/ListState;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->contentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v0, p1, :cond_1

    .line 85
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->contentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Set state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->isActive:Z

    if-eqz v0, :cond_0

    .line 83
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->updateAdapter()V

    goto :goto_0
.end method

.method public showDetailsView(Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;)V
    .locals 5
    .param p1, "source"    # Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;

    .prologue
    .line 515
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;-><init>()V

    .line 516
    .local v0, "item":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getContentType()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    .line 517
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowGuid()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->ID:Ljava/lang/String;

    .line 519
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Guid "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowGuid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getContentType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowImageUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 522
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 528
    :goto_0
    return-void

    .line 524
    :cond_0
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v1}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 525
    .local v1, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/epg/EPGIterator$ProgramData;->getShowImageUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putImageUrl(Ljava/lang/String;)V

    .line 526
    invoke-virtual {p0, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->navigateToAppOrMediaDetails(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0
.end method

.method protected updateAppChannels()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 129
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->getDefault()Lcom/microsoft/xbox/xle/epg/AppChannelsModel;

    move-result-object v3

    sget v4, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->FAVORITES_ONLY:I

    invoke-virtual {p0, v4}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->hasFlag(I)Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->getChannels(Z)[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    move-result-object v0

    .line 131
    .local v0, "channels":[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;
    if-eqz v0, :cond_2

    .line 134
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->appChannels:[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->appChannelsHashCode:I

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v4

    if-eq v3, v4, :cond_1

    .line 136
    :cond_0
    array-length v1, v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->appChannels:[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    .line 137
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->appChannelsHashCode:I

    move v1, v2

    .line 151
    :cond_1
    :goto_0
    return v1

    .line 143
    :cond_2
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->appChannels:[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    if-eqz v3, :cond_1

    .line 144
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->appChannels:[Lcom/microsoft/xbox/xle/epg/AppChannelsModel$Channel;

    .line 145
    iput v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->appChannelsHashCode:I

    move v1, v2

    .line 147
    goto :goto_0
.end method

.method protected updateLoadingStatus()V
    .locals 2

    .prologue
    .line 230
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->waitingForProfileUpdate:Z

    .line 232
    .local v0, "loading":Z
    sget v1, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->TV_CHANNELS:I

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->hasFlag(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 233
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getIteratorFactory()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->isFetching()Z

    move-result v1

    or-int/2addr v0, v1

    .line 235
    :cond_0
    sget v1, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->APP_CHANNELS:I

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->hasFlag(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 236
    invoke-static {}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->getDefault()Lcom/microsoft/xbox/xle/epg/AppChannelsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/epg/AppChannelsModel;->isLoading()Z

    move-result v1

    or-int/2addr v0, v1

    .line 238
    :cond_1
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->setLoading(Z)V

    .line 239
    return-void
.end method

.method protected updateOverride(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/model/UpdateData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 209
    .local p1, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/model/UpdateData;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 227
    :cond_0
    :goto_0
    return-void

    .line 215
    :cond_1
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/UpdateData;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/UpdateData;->getUpdateType()Lcom/microsoft/xbox/service/model/UpdateType;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/service/model/UpdateType;->ProfileData:Lcom/microsoft/xbox/service/model/UpdateType;

    if-ne v0, v1, :cond_0

    .line 216
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->waitingForProfileUpdate:Z

    .line 221
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    instance-of v0, v0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;

    if-eqz v0, :cond_2

    .line 222
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    check-cast v0, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/TvChannelsScreenAdapter;->refresh()V

    .line 225
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->updateViewModelState()V

    goto :goto_0
.end method

.method protected updateTvChannels()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 100
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 101
    sget v3, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->FAVORITES_ONLY:I

    invoke-virtual {p0, v3}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->hasFlag(I)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getIteratorFactory()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->getFavoriteChannels()[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    move-result-object v0

    .line 106
    .local v0, "channels":[Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    :goto_0
    if-eqz v0, :cond_4

    .line 109
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->tvChannels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->tvChannelsHashCode:I

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v4

    if-eq v3, v4, :cond_1

    .line 111
    :cond_0
    array-length v1, v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->tvChannels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    .line 112
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    iput v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->tvChannelsHashCode:I

    move v1, v2

    .line 125
    :cond_1
    :goto_1
    return v1

    .line 101
    .end local v0    # "channels":[Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->getActiveProvider()Lcom/microsoft/xbox/service/model/epg/EPGProvider;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGProvider;->getIteratorFactory()Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/epg/EPGIteratorFactory;->getChannels()[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    move-result-object v0

    goto :goto_0

    .line 103
    :cond_3
    const/4 v0, 0x0

    .restart local v0    # "channels":[Lcom/microsoft/xbox/service/model/epg/EPGChannel;
    goto :goto_0

    .line 117
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->tvChannels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    if-eqz v3, :cond_1

    .line 118
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->tvChannels:[Lcom/microsoft/xbox/service/model/epg/EPGChannel;

    .line 119
    iput v1, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->tvChannelsHashCode:I

    move v1, v2

    .line 121
    goto :goto_1
.end method

.method protected updateViewModelState()V
    .locals 4

    .prologue
    .line 243
    sget v2, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->APP_CHANNELS:I

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->hasFlag(I)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Lcom/microsoft/xbox/service/model/epg/EPGModel;->hasProviders()Z

    move-result v2

    if-nez v2, :cond_1

    .line 244
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    const-string v3, "No providers found, set NoContentState"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->setViewModelState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    .line 290
    :cond_0
    :goto_0
    return-void

    .line 250
    :cond_1
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->modelChanged:Z

    .line 253
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->updateLoadingStatus()V

    .line 256
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->isLoading:Z

    if-nez v2, :cond_0

    .line 261
    :cond_2
    sget v2, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->TV_CHANNELS:I

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->hasFlag(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 262
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->modelChanged:Z

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->updateTvChannels()Z

    move-result v3

    or-int/2addr v2, v3

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->modelChanged:Z

    .line 265
    :cond_3
    sget v2, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->APP_CHANNELS:I

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->hasFlag(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 266
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->modelChanged:Z

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->updateAppChannels()Z

    move-result v3

    or-int/2addr v2, v3

    iput-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->modelChanged:Z

    .line 270
    :cond_4
    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->modelChanged:Z

    if-eqz v2, :cond_5

    .line 271
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->listeners:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;

    .line 272
    .local v0, "listener":Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;
    invoke-interface {v0}, Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;->modelChanged()V

    goto :goto_1

    .line 278
    .end local v0    # "listener":Lcom/microsoft/xbox/xle/viewmodel/EPGViewModel$Listener;
    :cond_5
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->hasErrors()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 279
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    const-string v3, "Errors detected, set ErrorState"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 289
    .local v1, "state":Lcom/microsoft/xbox/toolkit/network/ListState;
    :goto_2
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->setViewModelState(Lcom/microsoft/xbox/toolkit/network/ListState;)V

    goto :goto_0

    .line 281
    .end local v1    # "state":Lcom/microsoft/xbox/toolkit/network/ListState;
    :cond_6
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->getChannelCount()I

    move-result v2

    if-nez v2, :cond_7

    .line 282
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    const-string v3, "No channels found, set NoContentState"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .restart local v1    # "state":Lcom/microsoft/xbox/toolkit/network/ListState;
    goto :goto_2

    .line 285
    .end local v1    # "state":Lcom/microsoft/xbox/toolkit/network/ListState;
    :cond_7
    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/TvChannelsScreenViewModel;->diagTag:Ljava/lang/String;

    const-string v3, "No problems, set ValidContentState"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .restart local v1    # "state":Lcom/microsoft/xbox/toolkit/network/ListState;
    goto :goto_2
.end method
