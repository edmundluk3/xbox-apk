.class public Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody;
.super Ljava/lang/Object;
.source "EDFRegistrationBody.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$ClientDescription;,
        Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$GCM;,
        Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$Transports;
    }
.end annotation


# instance fields
.field public clientDescription:Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$ClientDescription;

.field public nodeId:Ljava/lang/String;

.field public registrationId:Ljava/lang/String;

.field public transports:Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$Transports;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "nodeId"    # Ljava/lang/String;
    .param p2, "edfRegId"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody;->nodeId:Ljava/lang/String;

    .line 34
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody;->registrationId:Ljava/lang/String;

    .line 35
    new-instance v1, Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$ClientDescription;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$ClientDescription;-><init>(Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody;)V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody;->clientDescription:Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$ClientDescription;

    .line 36
    new-instance v1, Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$Transports;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$Transports;-><init>(Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody;)V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody;->transports:Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$Transports;

    .line 37
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody;->transports:Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$Transports;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$Transports;->gcm:Ljava/util/ArrayList;

    .line 38
    new-instance v0, Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$GCM;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$GCM;-><init>(Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody;)V

    .line 39
    .local v0, "gcmEntry":Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$GCM;
    const-string v1, ""

    iput-object v1, v0, Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$GCM;->context:Ljava/lang/String;

    .line 40
    iput-object p3, v0, Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$GCM;->path:Ljava/lang/String;

    .line 41
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody;->transports:Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$Transports;

    iget-object v1, v1, Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody$Transports;->gcm:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    return-void
.end method

.method public static getEDFRegistrationBody(Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody;)Ljava/lang/String;
    .locals 4
    .param p0, "edfRegistrationBody"    # Lcom/microsoft/xbox/service/model/serialization/EDFRegistrationBody;

    .prologue
    .line 46
    :try_start_0
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 51
    :goto_0
    return-object v1

    .line 47
    :catch_0
    move-exception v0

    .line 48
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "EDFRegistrationBody"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error in serialzation"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    const/4 v1, 0x0

    goto :goto_0
.end method
