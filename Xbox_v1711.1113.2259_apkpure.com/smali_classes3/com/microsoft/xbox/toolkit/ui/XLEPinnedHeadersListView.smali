.class public Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;
.super Landroid/widget/ListView;
.source "XLEPinnedHeadersListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView$PinnedHeadersSectionIndexer;
    }
.end annotation


# static fields
.field private static final NUM_OF_CACHED_HEADERS:I = 0x1e


# instance fields
.field private currentHeader:Landroid/view/View;

.field private currentSection:I

.field private headers:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private indexer:Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView$PinnedHeadersSectionIndexer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method private computeTranslation()I
    .locals 12

    .prologue
    .line 135
    const/4 v8, 0x0

    .line 136
    .local v8, "translation":I
    iget-object v9, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->indexer:Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView$PinnedHeadersSectionIndexer;

    invoke-interface {v9}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView$PinnedHeadersSectionIndexer;->getSections()[Ljava/lang/Object;

    move-result-object v7

    .line 137
    .local v7, "sections":[Ljava/lang/Object;
    if-eqz v7, :cond_1

    array-length v9, v7

    if-lez v9, :cond_1

    .line 138
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getFirstVisiblePosition()I

    move-result v3

    .line 139
    .local v3, "fvp":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getLastVisiblePosition()I

    move-result v4

    .line 141
    .local v4, "lvp":I
    iget v9, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentSection:I

    array-length v10, v7

    add-int/lit8 v10, v10, -0x1

    if-eq v9, v10, :cond_0

    .line 142
    iget-object v9, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->indexer:Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView$PinnedHeadersSectionIndexer;

    iget v10, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentSection:I

    add-int/lit8 v10, v10, 0x1

    invoke-interface {v9, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView$PinnedHeadersSectionIndexer;->getPositionForSection(I)I

    move-result v6

    .line 143
    .local v6, "nextHeaderPosition":I
    if-gt v3, v6, :cond_0

    if-gt v6, v4, :cond_0

    .line 144
    sub-int v9, v6, v3

    invoke-virtual {p0, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 145
    .local v5, "nextHeader":Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v9

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getPaddingTop()I

    move-result v10

    iget-object v11, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentHeader:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getHeight()I

    move-result v11

    add-int/2addr v10, v11

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getDividerHeight()I

    move-result v11

    add-int/2addr v10, v11

    if-gt v9, v10, :cond_0

    .line 148
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v9

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getPaddingTop()I

    move-result v10

    sub-int/2addr v9, v10

    iget-object v10, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentHeader:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getHeight()I

    move-result v10

    sub-int/2addr v9, v10

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getDividerHeight()I

    move-result v10

    sub-int v8, v9, v10

    .line 153
    .end local v5    # "nextHeader":Landroid/view/View;
    .end local v6    # "nextHeaderPosition":I
    :cond_0
    if-nez v8, :cond_1

    .line 154
    iget-object v9, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->indexer:Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView$PinnedHeadersSectionIndexer;

    iget v10, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentSection:I

    invoke-interface {v9, v10}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView$PinnedHeadersSectionIndexer;->getPositionForSection(I)I

    move-result v1

    .line 155
    .local v1, "currHeaderPosition":I
    if-gt v3, v1, :cond_1

    if-gt v1, v4, :cond_1

    .line 156
    sub-int v9, v1, v3

    invoke-virtual {p0, v9}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 157
    .local v0, "currHeader":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v9

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getPaddingTop()I

    move-result v10

    sub-int v2, v9, v10

    .line 158
    .local v2, "currTop":I
    if-ltz v2, :cond_1

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getDividerHeight()I

    move-result v9

    if-gt v2, v9, :cond_1

    .line 161
    move v8, v2

    .line 166
    .end local v0    # "currHeader":Landroid/view/View;
    .end local v1    # "currHeaderPosition":I
    .end local v2    # "currTop":I
    .end local v3    # "fvp":I
    .end local v4    # "lvp":I
    :cond_1
    return v8
.end method

.method private ensureHeaderLayout()V
    .locals 8

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v7, 0x0

    .line 170
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentHeader:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->isLayoutRequested()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 171
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getWidth()I

    move-result v2

    .line 172
    .local v2, "width":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getPaddingLeft()I

    move-result v4

    sub-int v4, v2, v4

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getPaddingRight()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 174
    .local v3, "widthSpec":I
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentHeader:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 175
    .local v1, "lp":Landroid/view/ViewGroup$LayoutParams;
    if-eqz v1, :cond_1

    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-lez v4, :cond_1

    .line 176
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 180
    .local v0, "heihgtSpec":I
    :goto_0
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentHeader:Landroid/view/View;

    invoke-virtual {v4, v3, v0}, Landroid/view/View;->measure(II)V

    .line 181
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentHeader:Landroid/view/View;

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentHeader:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentHeader:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    invoke-virtual {v4, v7, v7, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 183
    .end local v0    # "heihgtSpec":I
    .end local v1    # "lp":Landroid/view/ViewGroup$LayoutParams;
    .end local v2    # "width":I
    .end local v3    # "widthSpec":I
    :cond_0
    return-void

    .line 178
    .restart local v1    # "lp":Landroid/view/ViewGroup$LayoutParams;
    .restart local v2    # "width":I
    .restart local v3    # "widthSpec":I
    :cond_1
    invoke-static {v7, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .restart local v0    # "heihgtSpec":I
    goto :goto_0
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 88
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentHeader:Landroid/view/View;

    if-nez v5, :cond_0

    .line 89
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 132
    :goto_0
    return-void

    .line 91
    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->ensureHeaderLayout()V

    .line 92
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->computeTranslation()I

    move-result v4

    .line 95
    .local v4, "translation":I
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    .line 96
    .local v3, "saveCount":I
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 97
    .local v1, "clip":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getPaddingTop()I

    move-result v5

    iput v5, v1, Landroid/graphics/Rect;->top:I

    .line 98
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getHeight()I

    move-result v5

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getPaddingBottom()I

    move-result v6

    sub-int/2addr v5, v6

    iput v5, v1, Landroid/graphics/Rect;->bottom:I

    .line 99
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 100
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getPaddingLeft()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getPaddingTop()I

    move-result v6

    add-int/2addr v6, v4

    int-to-float v6, v6

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 101
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentHeader:Landroid/view/View;

    invoke-virtual {v5, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 102
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 104
    if-lez v4, :cond_1

    .line 107
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getDivider()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 108
    .local v2, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v2, :cond_1

    .line 109
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getPaddingTop()I

    move-result v5

    add-int v0, v5, v4

    .line 110
    .local v0, "bottom":I
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    .line 111
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 112
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getPaddingTop()I

    move-result v5

    iput v5, v1, Landroid/graphics/Rect;->top:I

    .line 113
    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 114
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 117
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getPaddingLeft()I

    move-result v5

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getDividerHeight()I

    move-result v6

    sub-int v6, v0, v6

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getWidth()I

    move-result v7

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getPaddingRight()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {v2, v5, v6, v7, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 118
    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 119
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 124
    .end local v0    # "bottom":I
    .end local v2    # "d":Landroid/graphics/drawable/Drawable;
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    .line 125
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 126
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getPaddingTop()I

    move-result v5

    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentHeader:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v5, v6

    add-int/2addr v5, v4

    iput v5, v1, Landroid/graphics/Rect;->top:I

    .line 127
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 128
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 129
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto/16 :goto_0
.end method

.method protected onScrollChanged(IIII)V
    .locals 7
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "oldl"    # I
    .param p4, "oldt"    # I

    .prologue
    .line 48
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->onScrollChanged(IIII)V

    .line 49
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->indexer:Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView$PinnedHeadersSectionIndexer;

    if-eqz v3, :cond_0

    .line 50
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->indexer:Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView$PinnedHeadersSectionIndexer;

    invoke-interface {v3}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView$PinnedHeadersSectionIndexer;->getSections()[Ljava/lang/Object;

    move-result-object v2

    .line 51
    .local v2, "sections":[Ljava/lang/Object;
    if-eqz v2, :cond_0

    array-length v3, v2

    if-lez v3, :cond_0

    .line 52
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->getFirstVisiblePosition()I

    move-result v0

    .line 53
    .local v0, "fvp":I
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->indexer:Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView$PinnedHeadersSectionIndexer;

    invoke-interface {v3, v0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView$PinnedHeadersSectionIndexer;->getSectionForPosition(I)I

    move-result v1

    .line 54
    .local v1, "sectionIndex":I
    iget v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentSection:I

    if-eq v1, v3, :cond_0

    .line 55
    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentSection:I

    .line 56
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->indexer:Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView$PinnedHeadersSectionIndexer;

    iget v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentSection:I

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->headers:Landroid/util/LruCache;

    iget v6, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentSection:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    invoke-interface {v4, v5, v3, p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView$PinnedHeadersSectionIndexer;->getHeaderView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentHeader:Landroid/view/View;

    .line 57
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->headers:Landroid/util/LruCache;

    iget v4, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentSection:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentHeader:Landroid/view/View;

    invoke-virtual {v3, v4, v5}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    .end local v0    # "fvp":I
    .end local v1    # "sectionIndex":I
    .end local v2    # "sections":[Ljava/lang/Object;
    :cond_0
    return-void
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 4
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 65
    invoke-super {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 66
    instance-of v1, p1, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView$PinnedHeadersSectionIndexer;

    if-eqz v1, :cond_1

    .line 67
    check-cast p1, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView$PinnedHeadersSectionIndexer;

    .end local p1    # "adapter":Landroid/widget/ListAdapter;
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->indexer:Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView$PinnedHeadersSectionIndexer;

    .line 68
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->indexer:Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView$PinnedHeadersSectionIndexer;

    invoke-interface {v1}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView$PinnedHeadersSectionIndexer;->getSections()[Ljava/lang/Object;

    move-result-object v0

    .line 69
    .local v0, "sections":[Ljava/lang/Object;
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 70
    new-instance v1, Landroid/util/LruCache;

    const/16 v2, 0x1e

    invoke-direct {v1, v2}, Landroid/util/LruCache;-><init>(I)V

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->headers:Landroid/util/LruCache;

    .line 71
    const/4 v1, 0x0

    iput v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentSection:I

    .line 72
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->indexer:Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView$PinnedHeadersSectionIndexer;

    iget v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentSection:I

    invoke-interface {v1, v2, v3, p0}, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView$PinnedHeadersSectionIndexer;->getHeaderView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentHeader:Landroid/view/View;

    .line 84
    .end local v0    # "sections":[Ljava/lang/Object;
    :goto_0
    return-void

    .line 74
    .restart local v0    # "sections":[Ljava/lang/Object;
    :cond_0
    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->headers:Landroid/util/LruCache;

    .line 75
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentSection:I

    .line 76
    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentHeader:Landroid/view/View;

    goto :goto_0

    .line 79
    .end local v0    # "sections":[Ljava/lang/Object;
    .restart local p1    # "adapter":Landroid/widget/ListAdapter;
    :cond_1
    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->indexer:Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView$PinnedHeadersSectionIndexer;

    .line 80
    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->headers:Landroid/util/LruCache;

    .line 81
    iput v2, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentSection:I

    .line 82
    iput-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/XLEPinnedHeadersListView;->currentHeader:Landroid/view/View;

    goto :goto_0
.end method
