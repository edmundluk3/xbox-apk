.class public abstract Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$MaximizedChangedResult;
.super Ljava/lang/Object;
.source "HoverChatResultDataTypes.java"

# interfaces
.implements Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "MaximizedChangedResult"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(ZLcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;)Lcom/microsoft/xbox/domain/hoverchat/HoverChatResultDataTypes$MaximizedChangedResult;
    .locals 1
    .param p0, "isMaximized"    # Z
    .param p1, "position"    # Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 60
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 61
    new-instance v0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_HoverChatResultDataTypes_MaximizedChangedResult;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_HoverChatResultDataTypes_MaximizedChangedResult;-><init>(ZLcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;)V

    return-object v0
.end method


# virtual methods
.method public abstract isMaximized()Z
.end method

.method public abstract position()Lcom/microsoft/xbox/presentation/hoverchat/HoverChatHeadViewStateDataTypes$Position;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
