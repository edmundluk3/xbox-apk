.class public Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
.super Ljava/lang/Object;
.source "TitleInfo.java"


# instance fields
.field private isSelected:Z

.field private titleData:Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;)V
    .locals 0
    .param p1, "titleData"    # Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 24
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->titleData:Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;

    .line 25
    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;Z)V
    .locals 0
    .param p1, "titleData"    # Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "selected"    # Z

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->titleData:Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;

    .line 30
    iput-boolean p2, p0, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->isSelected:Z

    .line 31
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 70
    if-ne p1, p0, :cond_0

    .line 71
    const/4 v1, 0x1

    .line 76
    :goto_0
    return v1

    .line 72
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;

    if-nez v1, :cond_1

    .line 73
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 75
    check-cast v0, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;

    .line 76
    .local v0, "other":Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->titleData:Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;

    iget-object v2, v0, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->titleData:Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getBoxArt()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->titleData:Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;->getBoxArt()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->titleData:Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getScid()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->titleData:Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;->getScid()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitleId()J
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->titleData:Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;

    invoke-interface {v0}, Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;->getTitleId()J

    move-result-wide v0

    return-wide v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 82
    const/16 v0, 0x11

    .line 83
    .local v0, "hashCode":I
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->titleData:Lcom/microsoft/xbox/xle/app/lfg/ITitleSearchResult;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 84
    return v0
.end method

.method public isSelected()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->isSelected:Z

    return v0
.end method

.method public setSelected(Z)V
    .locals 0
    .param p1, "isSelected"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->isSelected:Z

    .line 62
    return-void
.end method

.method public toggleSelection()V
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->isSelected:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/microsoft/xbox/xle/app/lfg/TitleInfo;->isSelected:Z

    .line 58
    return-void

    .line 57
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
