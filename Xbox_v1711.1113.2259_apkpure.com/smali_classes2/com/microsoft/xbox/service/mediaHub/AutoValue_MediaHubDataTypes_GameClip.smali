.class final Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip;
.super Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_GameClip;
.source "AutoValue_MediaHubDataTypes_GameClip.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/mediaHub/AutoValue_MediaHubDataTypes_GameClip$GsonTypeAdapter;
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;)V
    .locals 0
    .param p1, "clipName"    # Ljava/lang/String;
    .param p2, "contentId"    # Ljava/lang/String;
    .param p5, "durationInSeconds"    # Ljava/lang/Integer;
    .param p6, "embeddedText"    # Ljava/lang/String;
    .param p7, "frameRate"    # Ljava/lang/Integer;
    .param p8, "greatestMomentId"    # Ljava/lang/String;
    .param p9, "localId"    # Ljava/lang/String;
    .param p10, "ownerXuid"    # Ljava/lang/Long;
    .param p11, "resolutionHeight"    # Ljava/lang/Integer;
    .param p12, "resolutionWidth"    # Ljava/lang/Integer;
    .param p13, "sandboxId"    # Ljava/lang/String;
    .param p14, "titleData"    # Ljava/lang/String;
    .param p15, "titleId"    # Ljava/lang/String;
    .param p16, "titleName"    # Ljava/lang/String;
    .param p17, "uploadDate"    # Ljava/util/Date;
    .param p18, "uploadLanguage"    # Ljava/lang/String;
    .param p19, "uploadRegion"    # Ljava/lang/String;
    .param p20, "uploadTitleId"    # Ljava/lang/Long;
    .param p21, "uploadDeviceType"    # Ljava/lang/String;
    .param p22, "userCaption"    # Ljava/lang/String;
    .param p23, "commentCount"    # Ljava/lang/Integer;
    .param p24, "likeCount"    # Ljava/lang/Integer;
    .param p25, "shareCount"    # Ljava/lang/Integer;
    .param p26, "viewCount"    # Ljava/lang/Integer;
    .param p27, "contentState"    # Ljava/lang/String;
    .param p28, "enforcementState"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;",
            ">;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Session;",
            ">;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Stat;",
            ">;",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p3, "contentLocators":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Locator;>;"
    .local p4, "contentSegments":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$ContentSegment;>;"
    .local p29, "sessions":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Session;>;"
    .local p30, "stats":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Stat;>;"
    .local p31, "tournaments":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<Lcom/microsoft/xbox/service/mediaHub/MediaHubDataTypes$Tournament;>;"
    invoke-direct/range {p0 .. p31}, Lcom/microsoft/xbox/service/mediaHub/$AutoValue_MediaHubDataTypes_GameClip;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;Lcom/google/common/collect/ImmutableList;)V

    .line 31
    return-void
.end method
