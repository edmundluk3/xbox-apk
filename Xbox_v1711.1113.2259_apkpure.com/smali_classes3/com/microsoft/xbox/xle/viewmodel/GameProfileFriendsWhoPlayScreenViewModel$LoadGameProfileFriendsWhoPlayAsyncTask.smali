.class public Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameProfileFriendsWhoPlayScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "LoadGameProfileFriendsWhoPlayAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;


# direct methods
.method protected constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 4

    .prologue
    .line 145
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 147
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    iget-wide v0, v0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->gameTitleId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 148
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGameProfileFriendsWhoPlayModel()Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    iget-wide v2, v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->gameTitleId:J

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;->shouldRefresh(J)Z

    move-result v0

    .line 150
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 6

    .prologue
    .line 178
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 180
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadFollowingProfile(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    .line 182
    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    iget-wide v2, v1, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->gameTitleId:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 183
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGameProfileFriendsWhoPlayModel()Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;

    move-result-object v1

    iget-boolean v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;->forceLoad:Z

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    iget-wide v4, v3, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->gameTitleId:J

    invoke-virtual {v1, v2, v4, v5}, Lcom/microsoft/xbox/service/model/GameProfileFriendsWhoPlayModel;->load(ZJ)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 186
    :cond_0
    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 173
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 156
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->onLoadGameProfileFriendsWhoPlayCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 158
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->onLoadGameProfileFriendsWhoPlayCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 169
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 141
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 162
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel$LoadGameProfileFriendsWhoPlayAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileFriendsWhoPlayScreenViewModel;->updateAdapter()V

    .line 164
    return-void
.end method
