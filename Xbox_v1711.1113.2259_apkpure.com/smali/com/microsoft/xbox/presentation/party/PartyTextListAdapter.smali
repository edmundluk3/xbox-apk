.class public Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;
.super Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;
.source "PartyTextListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;,
        Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextSelfViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray",
        "<",
        "Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field myProfileProvider:Lcom/microsoft/xbox/toolkit/MyProfileProvider;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/RecyclerViewAdapterWithArray;-><init>()V

    .line 36
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v0}, Lcom/microsoft/xbox/XLEApplication;->getComponent()Lcom/microsoft/xbox/XLEComponent;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/microsoft/xbox/XLEComponent;->inject(Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;)V

    .line 37
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;->getTimeStamp(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getTimeStamp(I)Ljava/lang/String;
    .locals 9
    .param p1, "position"    # I

    .prologue
    const/16 v8, 0xc

    .line 76
    if-eqz p1, :cond_0

    .line 77
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;->timeStamp()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;->timeStamp()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    const-wide/32 v6, 0xea60

    add-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    .line 78
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;->timeStamp()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;->timeStamp()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-eq v2, v1, :cond_1

    .line 79
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Instance:Lcom/microsoft/xbox/XLEApplication;

    invoke-virtual {v1}, Lcom/microsoft/xbox/XLEApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 80
    .local v0, "dateFormat":Ljava/text/DateFormat;
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;->timeStamp()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 83
    .end local v0    # "dateFormat":Ljava/text/DateFormat;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 61
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;

    .line 62
    .local v0, "message":Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;->member()Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xbservices/domain/party/PartyMember;->xuid()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 63
    const v1, 0x7f0300bf

    .line 65
    :goto_0
    return v1

    :cond_0
    const v1, 0x7f0300be

    goto :goto_0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 2
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 55
    move-object v0, p1

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;

    .line 56
    .local v0, "textViewHolder":Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;, "Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase<Lcom/microsoft/xbox/xbservices/domain/party/PartyMessage;>;"
    invoke-virtual {p0, p2}, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;->getDataItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/RecyclerView/ViewHolderBase;->onBind(Ljava/lang/Object;)V

    .line 57
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 41
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 42
    .local v0, "v":Landroid/view/View;
    packed-switch p2, :pswitch_data_0

    .line 49
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown view type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 50
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 44
    :pswitch_0
    new-instance v1, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextSelfViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextSelfViewHolder;-><init>(Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 46
    :pswitch_1
    new-instance v1, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter$PartyTextOtherViewHolder;-><init>(Lcom/microsoft/xbox/presentation/party/PartyTextListAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 42
    :pswitch_data_0
    .packed-switch 0x7f0300be
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
