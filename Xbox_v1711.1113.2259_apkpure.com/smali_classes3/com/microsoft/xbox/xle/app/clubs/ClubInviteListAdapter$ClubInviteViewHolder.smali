.class public final Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "ClubInviteListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "ClubInviteViewHolder"
.end annotation


# instance fields
.field private favoriteIcon:Landroid/widget/TextView;

.field private gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

.field private gamerTag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private onlineIcon:Landroid/view/View;

.field private presence:Landroid/widget/TextView;

.field private realName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private selectedIcon:Landroid/view/View;

.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;Landroid/view/View;)V
    .locals 2
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;

    .line 89
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 91
    const v1, 0x7f0e0396

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->onlineIcon:Landroid/view/View;

    .line 92
    const v1, 0x7f0e0397

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->favoriteIcon:Landroid/widget/TextView;

    .line 93
    const v1, 0x7f0e0398

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    .line 94
    const v1, 0x7f0e0399

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->gamerTag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 95
    const v1, 0x7f0e039a

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->realName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 96
    const v1, 0x7f0e039b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->presence:Landroid/widget/TextView;

    .line 97
    const v1, 0x7f0e0395

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->selectedIcon:Landroid/view/View;

    .line 99
    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;->access$000()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;->access$000()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_1

    .line 100
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    .line 101
    .local v0, "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPreferedColor()I

    move-result v1

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;->access$002(Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 103
    .end local v0    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_1
    return-void

    .line 101
    .restart local v0    # "meProfile":Lcom/microsoft/xbox/service/model/ProfileModel;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private itemViewContentDescription(Lcom/microsoft/xbox/service/model/FollowersData;ZLandroid/view/View;)V
    .locals 6
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/FollowersData;
    .param p2, "selected"    # Z
    .param p3, "itemView"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 141
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamertag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamerRealName()Ljava/lang/String;

    move-result-object v1

    const-string v3, ""

    invoke-static {v1, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/microsoft/xbox/service/model/FollowersData;->presenceString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 142
    .local v0, "content":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 143
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f07006e

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 147
    :goto_0
    return-void

    .line 145
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f070072

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic lambda$bindTo$0(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;ZLcom/microsoft/xbox/service/model/FollowersData;Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$SelectionListener;Landroid/view/View;)V
    .locals 4
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;
    .param p1, "isSelected"    # Z
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "item"    # Lcom/microsoft/xbox/service/model/FollowersData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "selectionListener"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$SelectionListener;
    .param p4, "v"    # Landroid/view/View;

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->getAdapterPosition()I

    move-result v1

    .line 129
    .local v1, "position":I
    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 131
    .local v0, "newSelected":Z
    :goto_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->itemView:Landroid/view/View;

    invoke-direct {p0, p2, v0, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->itemViewContentDescription(Lcom/microsoft/xbox/service/model/FollowersData;ZLandroid/view/View;)V

    .line 132
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;)Landroid/util/SparseBooleanArray;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 133
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;->notifyItemChanged(I)V

    .line 135
    invoke-interface {p3, v1, v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$SelectionListener;->onSelectedChanged(IZ)V

    .line 136
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->itemView:Landroid/view/View;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 137
    return-void

    .line 129
    .end local v0    # "newSelected":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bindTo(Lcom/microsoft/xbox/service/model/FollowersData;Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$SelectionListener;)V
    .locals 6
    .param p1, "item"    # Lcom/microsoft/xbox/service/model/FollowersData;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "selectionListener"    # Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$SelectionListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const v5, 0x7f020125

    const/4 v3, 0x0

    .line 106
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 107
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 109
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->onlineIcon:Landroid/view/View;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getIsOnline()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p1, Lcom/microsoft/xbox/service/model/FollowersData;->isFavorite:Z

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v4, v2}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 110
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->favoriteIcon:Landroid/widget/TextView;

    iget-boolean v4, p1, Lcom/microsoft/xbox/service/model/FollowersData;->isFavorite:Z

    invoke-static {v2, v4}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 112
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getIsOnline()Z

    move-result v2

    if-eqz v2, :cond_2

    const v0, 0x7f0c000c

    .line 113
    .local v0, "favoriteIconColor":I
    :goto_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->favoriteIcon:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->favoriteIcon:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 115
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->gamerPic:Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamerPicUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4, v5, v5}, Lcom/microsoft/xbox/toolkit/ui/XLERoundedUniversalImageView;->setImageURI2(Ljava/lang/String;II)V

    .line 116
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->gamerTag:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamertag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->realName:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/FollowersData;->getGamerRealName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->presence:Landroid/widget/TextView;

    iget-object v4, p1, Lcom/microsoft/xbox/service/model/FollowersData;->presenceString:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;->access$100(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;)Landroid/util/SparseBooleanArray;

    move-result-object v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->getAdapterPosition()I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    .line 122
    .local v1, "isSelected":Z
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->itemView:Landroid/view/View;

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter;->access$000()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    :cond_0
    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 123
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->selectedIcon:Landroid/view/View;

    invoke-static {v2, v1}, Lcom/microsoft/xbox/xle/app/XLEUtil;->setVisibility(Landroid/view/View;Z)V

    .line 125
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->itemView:Landroid/view/View;

    invoke-direct {p0, p1, v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->itemViewContentDescription(Lcom/microsoft/xbox/service/model/FollowersData;ZLandroid/view/View;)V

    .line 127
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;->itemView:Landroid/view/View;

    invoke-static {p0, v1, p1, p2}, Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$ClubInviteViewHolder;ZLcom/microsoft/xbox/service/model/FollowersData;Lcom/microsoft/xbox/xle/app/clubs/ClubInviteListAdapter$SelectionListener;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    return-void

    .end local v0    # "favoriteIconColor":I
    .end local v1    # "isSelected":Z
    :cond_1
    move v2, v3

    .line 109
    goto :goto_0

    .line 112
    :cond_2
    const v0, 0x7f0c0012

    goto :goto_1
.end method
