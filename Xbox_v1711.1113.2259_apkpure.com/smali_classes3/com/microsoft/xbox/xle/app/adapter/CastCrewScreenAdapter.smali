.class public Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.source "CastCrewScreenAdapter.java"


# instance fields
.field private items:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;",
            ">;"
        }
    .end annotation
.end field

.field private listAdapter:Lcom/microsoft/xbox/xle/app/adapter/CastCrewListAdapter;

.field private switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;)V
    .locals 4
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>()V

    .line 27
    const v0, 0x7f0e07ac

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->screenBody:Landroid/view/View;

    .line 29
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;

    .line 30
    const v0, 0x7f0e07ae

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->setListView(Lcom/microsoft/xbox/toolkit/ui/XLEListView;)V

    .line 31
    const v0, 0x7f0e07ad

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    .line 32
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f030175

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;

    invoke-direct {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/adapter/CastCrewListAdapter;-><init>(Landroid/content/Context;ILcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/CastCrewListAdapter;

    .line 33
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/CastCrewListAdapter;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 34
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter$1;-><init>(Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 40
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;

    return-object v0
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;

    return-object v0
.end method

.method public updateViewOverride()V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;->isBusy()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->updateLoadingIndicator(Z)V

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->items:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;->getData()Ljava/util/ArrayList;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;->getData()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->items:Ljava/util/ArrayList;

    .line 47
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/CastCrewListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/adapter/CastCrewListAdapter;->clear()V

    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->items:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/CastCrewListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->items:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/adapter/CastCrewListAdapter;->addAll(Ljava/util/Collection;)V

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onDataUpdated()V

    .line 55
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->switchPanel:Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/CastCrewScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/CastCrewScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;->setState(I)V

    .line 56
    return-void
.end method
