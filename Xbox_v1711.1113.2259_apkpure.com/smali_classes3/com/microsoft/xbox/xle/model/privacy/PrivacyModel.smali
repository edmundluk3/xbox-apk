.class public Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;
.super Ljava/lang/Object;
.source "PrivacyModel.java"


# static fields
.field private static final instance:Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    new-instance v0, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->instance:Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;->instance:Lcom/microsoft/xbox/xle/model/privacy/PrivacyModel;

    return-object v0
.end method


# virtual methods
.method public canCommentOnItem()Z
    .locals 1

    .prologue
    .line 32
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToCommunicateVoiceAndText()Z

    move-result v0

    return v0
.end method

.method public canDeleteFeedItem(Ljava/lang/String;)Z
    .locals 1
    .param p1, "xuidItemAuthor"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-static {p1}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeXuid(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public canPostStatus()Z
    .locals 1

    .prologue
    .line 40
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToCommunicateVoiceAndText()Z

    move-result v0

    return v0
.end method

.method public canReportFeedItem(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;Ljava/lang/String;)Z
    .locals 1
    .param p1, "authorType"    # Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "xuidItemAuthor"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 25
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 26
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 28
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;->User:Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$AuthorInfo$AuthorType;

    if-ne p1, v0, :cond_0

    invoke-static {p2}, Lcom/microsoft/xbox/service/model/ProfileModel;->isMeXuid(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canShareItem()Z
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->hasPrivilegeToShareContent()Z

    move-result v0

    return v0
.end method
