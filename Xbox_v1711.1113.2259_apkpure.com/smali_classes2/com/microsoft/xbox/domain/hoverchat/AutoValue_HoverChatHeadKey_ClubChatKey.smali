.class final Lcom/microsoft/xbox/domain/hoverchat/AutoValue_HoverChatHeadKey_ClubChatKey;
.super Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;
.source "AutoValue_HoverChatHeadKey_ClubChatKey.java"


# instance fields
.field private final clubId:J


# direct methods
.method constructor <init>(J)V
    .locals 1
    .param p1, "clubId"    # J

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;-><init>()V

    .line 13
    iput-wide p1, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_HoverChatHeadKey_ClubChatKey;->clubId:J

    .line 14
    return-void
.end method


# virtual methods
.method public clubId()J
    .locals 2

    .prologue
    .line 18
    iget-wide v0, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_HoverChatHeadKey_ClubChatKey;->clubId:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 30
    if-ne p1, p0, :cond_1

    .line 37
    :cond_0
    :goto_0
    return v1

    .line 33
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    if-eqz v3, :cond_2

    move-object v0, p1

    .line 34
    check-cast v0, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;

    .line 35
    .local v0, "that":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;
    iget-wide v4, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_HoverChatHeadKey_ClubChatKey;->clubId:J

    invoke-virtual {v0}, Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;->clubId()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/domain/hoverchat/HoverChatHeadKey$ClubChatKey;
    :cond_2
    move v1, v2

    .line 37
    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    .line 42
    const/4 v0, 0x1

    .line 43
    .local v0, "h":I
    const v1, 0xf4243

    mul-int/2addr v0, v1

    .line 44
    int-to-long v2, v0

    iget-wide v4, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_HoverChatHeadKey_ClubChatKey;->clubId:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    iget-wide v6, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_HoverChatHeadKey_ClubChatKey;->clubId:J

    xor-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 45
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 23
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ClubChatKey{clubId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/microsoft/xbox/domain/hoverchat/AutoValue_HoverChatHeadKey_ClubChatKey;->clubId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
