.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyName$Global;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$KeyName;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Global"
.end annotation


# static fields
.field public static final ActivityType:Ljava/lang/String; = "ActivityType"

.field public static final BigCatId:Ljava/lang/String; = "BigCatId"

.field public static final ClubId:Ljava/lang/String; = "ClubId"

.field public static final ContactsLinkedStatus:Ljava/lang/String; = "ContactsLinkedStatus"

.field public static final CurrentLocale:Ljava/lang/String; = "CurrentLocale"

.field public static final Enabled:Ljava/lang/String; = "Enabled"

.field public static final ExperimentTreatments:Ljava/lang/String; = "ExperimentTreatments"

.field public static final FacebookLinkedStatus:Ljava/lang/String; = "FacebookLinkedStatus"

.field public static final Filter:Ljava/lang/String; = "Filter"

.field public static final GenericErrorData:Ljava/lang/String; = "GenericErrorData"

.field public static final GlobalIndex:Ljava/lang/String; = "GlobalIndex"

.field public static final LaunchURI:Ljava/lang/String; = "LaunchUri"

.field public static final ListIndex:Ljava/lang/String; = "ListIndex"

.field public static final MediaId:Ljava/lang/String; = "MediaId"

.field public static final MediaType:Ljava/lang/String; = "MediaType"

.field public static final Pivot:Ljava/lang/String; = "Pivot"

.field public static final PreviousLocale:Ljava/lang/String; = "PreviousLocale"

.field public static final ProfileType:Ljava/lang/String; = "ProfileType"

.field public static final Result:Ljava/lang/String; = "Result"

.field public static final SearchText:Ljava/lang/String; = "SearchText"

.field public static final SelectedSuggestion:Ljava/lang/String; = "SelectedSuggestion"

.field public static final Tags:Ljava/lang/String; = "Tags"

.field public static final TargetXuid:Ljava/lang/String; = "TargetXuid"

.field public static final TargetXuids:Ljava/lang/String; = "TargetXuids"

.field public static final TextChanged:Ljava/lang/String; = "WasTextChanged"

.field public static final TitleId:Ljava/lang/String; = "TitleId"

.field public static final TitleIds:Ljava/lang/String; = "TitleIds"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
