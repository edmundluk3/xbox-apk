.class public abstract Lcom/microsoft/xbox/presentation/common/CommonViewIntents$CancelledIntent;
.super Ljava/lang/Object;
.source "CommonViewIntents.java"

# interfaces
.implements Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/presentation/common/CommonViewIntents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "CancelledIntent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 97
    .local p0, "this":Lcom/microsoft/xbox/presentation/common/CommonViewIntents$CancelledIntent;, "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$CancelledIntent<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)Lcom/microsoft/xbox/presentation/common/CommonViewIntents$CancelledIntent;
    .locals 1
    .param p0    # Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;",
            ">(TT;)",
            "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$CancelledIntent",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 102
    .local p0, "intent":Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;, "TT;"
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 103
    new-instance v0, Lcom/microsoft/xbox/presentation/common/AutoValue_CommonViewIntents_CancelledIntent;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/presentation/common/AutoValue_CommonViewIntents_CancelledIntent;-><init>(Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;)V

    return-object v0
.end method


# virtual methods
.method public abstract cancelledIntent()Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public toLogString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 108
    .local p0, "this":Lcom/microsoft/xbox/presentation/common/CommonViewIntents$CancelledIntent;, "Lcom/microsoft/xbox/presentation/common/CommonViewIntents$CancelledIntent<TT;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$CancelledIntent;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$CancelledIntent;->cancelledIntent()Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;

    move-result-object v1

    invoke-interface {v1}, Lcom/microsoft/xbox/presentation/common/CommonViewIntents$BaseViewIntent;->toLogString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
