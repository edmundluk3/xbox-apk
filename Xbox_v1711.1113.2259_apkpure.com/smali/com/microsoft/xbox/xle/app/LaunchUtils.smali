.class public Lcom/microsoft/xbox/xle/app/LaunchUtils;
.super Ljava/lang/Object;
.source "LaunchUtils.java"


# static fields
.field private static final HELP_FAQ_URL:Ljava/lang/String; = "http://go.microsoft.com/fwlink/?LinkID=391430"

.field public static final HelpLaunchFormat:Ljava/lang/String; = "https://start.ui%s.xboxlive.com/help/sg/v1/?TitleId=%X&ProductId=%s&sandboxId=%s&MarketplaceId=%s&Region=%s"

.field public static final MarketplayceFormat:Ljava/lang/String; = "marketplace://deeplink?destination=details&id=%s"

.field public static final MusicLaunchFormat:Ljava/lang/String; = "ms-xbl-%08X://media-playback?ContentID=%s&ContentType=%s"

.field private static final TAG:Ljava/lang/String;

.field public static final googlePlayAppUrl:Ljava/lang/String; = "market://details?id="

.field private static final googlePlayWebUri:Ljava/lang/String; = "https://play.google.com/store/apps/details?id="

.field private static final launchRemoteRunnable:Ljava/lang/Runnable;

.field private static silentSignInActivityParams:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

.field private static final silentSignInCallbacks:Lcom/microsoft/xbox/idp/jobs/MSAJob$Callbacks;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-class v0, Lcom/microsoft/xbox/xle/app/LaunchUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/LaunchUtils;->TAG:Ljava/lang/String;

    .line 68
    new-instance v0, Lcom/microsoft/xbox/xle/app/LaunchUtils$1;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/LaunchUtils$1;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/app/LaunchUtils;->launchRemoteRunnable:Ljava/lang/Runnable;

    .line 616
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/app/LaunchUtils;->silentSignInActivityParams:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    .line 618
    new-instance v0, Lcom/microsoft/xbox/xle/app/LaunchUtils$11;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/LaunchUtils$11;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/app/LaunchUtils;->silentSignInCallbacks:Lcom/microsoft/xbox/idp/jobs/MSAJob$Callbacks;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static LaunchHelp(JLjava/lang/String;Ljava/lang/String;)V
    .locals 12
    .param p0, "titleId"    # J
    .param p2, "productId"    # Ljava/lang/String;
    .param p3, "titleName"    # Ljava/lang/String;

    .prologue
    .line 251
    invoke-static {p0, p1, p2, p3}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->prepareHelp(JLjava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    move-result-object v7

    .line 252
    .local v7, "activityItem":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    new-instance v0, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/xle/app/LaunchUtils;->silentSignInActivityParams:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    .line 253
    sget-object v0, Lcom/microsoft/xbox/xle/app/LaunchUtils;->silentSignInActivityParams:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-virtual {v0, v7}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedActivityData(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    .line 256
    new-instance v10, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;

    invoke-direct {v10}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;-><init>()V

    .line 257
    .local v10, "model":Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;
    const-string v0, "CompanionUrl"

    invoke-virtual {v7}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getActivityLaunchInfo()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->getActivityUrlString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;->addValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 258
    const-string v0, "Launch Help"

    invoke-static {v0, v10}, Lcom/microsoft/xbox/service/network/managers/utctelemetry/UTCPageAction;->track(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCAdditionalInfoModel;)V

    .line 260
    :try_start_0
    new-instance v8, Lcom/microsoft/xbox/idp/interop/LocalConfig;

    invoke-direct {v8}, Lcom/microsoft/xbox/idp/interop/LocalConfig;-><init>()V

    .line 261
    .local v8, "cfg":Lcom/microsoft/xbox/idp/interop/LocalConfig;
    new-instance v0, Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/xle/app/LaunchUtils;->TAG:Ljava/lang/String;

    sget-object v3, Lcom/microsoft/xbox/xle/app/LaunchUtils;->silentSignInCallbacks:Lcom/microsoft/xbox/idp/jobs/MSAJob$Callbacks;

    .line 264
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getXboxLiveScopeForAccessToken()Ljava/lang/String;

    move-result-object v4

    const-string v5, "MBI_SSL"

    .line 266
    invoke-virtual {v8}, Lcom/microsoft/xbox/idp/interop/LocalConfig;->getCid()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;Lcom/microsoft/xbox/idp/jobs/MSAJob$Callbacks;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;->start()Lcom/microsoft/xbox/idp/jobs/JobSilentSignIn;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 271
    .end local v8    # "cfg":Lcom/microsoft/xbox/idp/interop/LocalConfig;
    :goto_0
    return-void

    .line 268
    :catch_0
    move-exception v9

    .line 269
    .local v9, "ex":Ljava/lang/Exception;
    sget-object v0, Lcom/microsoft/xbox/xle/app/LaunchUtils;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "failed to launch help: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v9}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static LaunchProvider(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;Ljava/lang/String;IILcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)V
    .locals 6
    .param p0, "provider"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    .param p1, "contentId"    # Ljava/lang/String;
    .param p2, "mediaType"    # I
    .param p3, "mediaGroup"    # I
    .param p4, "location"    # Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    .prologue
    .line 116
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchProvider(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;Ljava/lang/String;IILcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 117
    return-void
.end method

.method public static LaunchProvider(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;Ljava/lang/String;IILcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 10
    .param p0, "provider"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    .param p1, "contentId"    # Ljava/lang/String;
    .param p2, "mediaType"    # I
    .param p3, "mediaGroup"    # I
    .param p4, "location"    # Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;
    .param p5, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    const-wide/32 v4, 0x18ffc9f4

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 125
    const/4 v0, 0x0

    .line 126
    .local v0, "launchUri":Ljava/lang/String;
    const/4 v8, 0x0

    .line 128
    .local v8, "postAction":Ljava/lang/Runnable;
    const/4 v9, 0x0

    .line 129
    .local v9, "providerRunning":Z
    sparse-switch p2, :sswitch_data_0

    .line 181
    const-string v1, "Launch"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "don\'t know how to launch "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 185
    invoke-static {v0, p4, v8}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchUri(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;Ljava/lang/Runnable;)V

    .line 187
    :cond_0
    return-void

    .line 134
    :sswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getAppLaunchUri()Ljava/lang/String;

    move-result-object v0

    .line 135
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v4

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->getLaunchPostActionRunnable(Ljava/lang/String;IIJZ)Ljava/lang/Runnable;

    move-result-object v8

    .line 136
    goto :goto_0

    .line 139
    :sswitch_1
    if-eqz p5, :cond_2

    .line 140
    invoke-virtual {p5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getApplicationId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 141
    invoke-virtual {p5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getPackageFamilyName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 143
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "appx:%1$s!%2$s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getPackageFamilyName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {p5}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getApplicationId()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 149
    :goto_1
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v2

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v4

    long-to-int v3, v4

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->isTitleRunning(J)Z

    move-result v9

    .line 150
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v4

    if-nez v9, :cond_1

    move v6, v1

    :cond_1
    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->getLaunchPostActionRunnable(Ljava/lang/String;IIJZ)Ljava/lang/Runnable;

    move-result-object v8

    .line 151
    goto :goto_0

    .line 146
    :cond_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getAppLaunchUri()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 158
    :sswitch_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v2

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->ProviderMediaId:Ljava/lang/String;

    invoke-static {v2, v3, p1, v4}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->isTitlePlayingSameContent(JLjava/lang/String;Ljava/lang/String;)Z

    move-result v7

    .line 159
    .local v7, "mediaPlaying":Z
    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getMediaLaunchUri(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 160
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v4

    if-nez v7, :cond_3

    move v6, v1

    :cond_3
    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->getLaunchPostActionRunnable(Ljava/lang/String;IIJZ)Ljava/lang/Runnable;

    move-result-object v8

    .line 161
    goto/16 :goto_0

    .line 165
    .end local v7    # "mediaPlaying":Z
    :sswitch_3
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getAlbumLaunchUri()Ljava/lang/String;

    move-result-object v0

    .line 166
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->isTitleRunning(J)Z

    move-result v9

    .line 167
    if-eqz v9, :cond_4

    move-object v8, v2

    .line 168
    :goto_2
    goto/16 :goto_0

    .line 167
    :cond_4
    sget-object v8, Lcom/microsoft/xbox/xle/app/LaunchUtils;->launchRemoteRunnable:Ljava/lang/Runnable;

    goto :goto_2

    .line 170
    :sswitch_4
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTrackLaunchUri()Ljava/lang/String;

    move-result-object v0

    .line 171
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->isTitleRunning(J)Z

    move-result v9

    .line 172
    if-eqz v9, :cond_5

    move-object v8, v2

    .line 173
    :goto_3
    goto/16 :goto_0

    .line 172
    :cond_5
    sget-object v8, Lcom/microsoft/xbox/xle/app/LaunchUtils;->launchRemoteRunnable:Ljava/lang/Runnable;

    goto :goto_3

    .line 175
    :sswitch_5
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getArtistLaunchUri()Ljava/lang/String;

    move-result-object v0

    .line 176
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->isTitleRunning(J)Z

    move-result v9

    .line 177
    if-eqz v9, :cond_6

    move-object v8, v2

    .line 178
    :goto_4
    goto/16 :goto_0

    .line 177
    :cond_6
    sget-object v8, Lcom/microsoft/xbox/xle/app/LaunchUtils;->launchRemoteRunnable:Ljava/lang/Runnable;

    goto :goto_4

    .line 129
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3e8 -> :sswitch_2
        0x3ea -> :sswitch_2
        0x3eb -> :sswitch_2
        0x3ec -> :sswitch_2
        0x3ed -> :sswitch_2
        0x3ee -> :sswitch_3
        0x3ef -> :sswitch_4
        0x3f1 -> :sswitch_5
        0x2328 -> :sswitch_1
        0x2329 -> :sswitch_0
        0x232a -> :sswitch_0
    .end sparse-switch
.end method

.method public static LaunchUri(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;Ljava/lang/Runnable;)V
    .locals 4
    .param p0, "uri"    # Ljava/lang/String;
    .param p1, "location"    # Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;
    .param p2, "postAction"    # Ljava/lang/Runnable;

    .prologue
    .line 190
    sget-object v0, Lcom/microsoft/xbox/xle/app/LaunchUtils;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "launch url "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    invoke-static {}, Lcom/microsoft/xbox/service/model/SessionModel;->getInstance()Lcom/microsoft/xbox/service/model/SessionModel;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lcom/microsoft/xbox/service/model/SessionModel;->launchUriOnXboxOne(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;Ljava/lang/Runnable;)V

    .line 195
    new-instance v0, Lcom/microsoft/xbox/xle/app/LaunchUtils$2;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/LaunchUtils$2;-><init>()V

    const-wide/16 v2, 0x7530

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPostDelayed(Ljava/lang/Runnable;J)V

    .line 211
    return-void
.end method

.method public static LaunchUriThenRemote(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)V
    .locals 1
    .param p0, "uri"    # Ljava/lang/String;
    .param p1, "location"    # Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    .prologue
    .line 215
    sget-object v0, Lcom/microsoft/xbox/xle/app/LaunchUtils;->launchRemoteRunnable:Ljava/lang/Runnable;

    invoke-static {p0, p1, v0}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchUri(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;Ljava/lang/Runnable;)V

    .line 216
    return-void
.end method

.method public static LaunchUriThenRemoteIfTitleNotPlaying(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;J)V
    .locals 2
    .param p0, "uri"    # Ljava/lang/String;
    .param p1, "location"    # Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;
    .param p2, "titleId"    # J

    .prologue
    .line 220
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->isTitleRunning(J)Z

    move-result v0

    .line 222
    .local v0, "providerRunning":Z
    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-static {p0, p1, v1}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->LaunchUri(Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;Ljava/lang/Runnable;)V

    .line 223
    return-void

    .line 222
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/xle/app/LaunchUtils;->launchRemoteRunnable:Ljava/lang/Runnable;

    goto :goto_0
.end method

.method static synthetic access$000()Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/microsoft/xbox/xle/app/LaunchUtils;->silentSignInActivityParams:Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/microsoft/xbox/xle/app/LaunchUtils;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Intent;

    .prologue
    .line 59
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$300(Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->onMSATicketAcquired(Ljava/lang/String;)V

    return-void
.end method

.method public static ensureLaunchRateApp()Z
    .locals 14

    .prologue
    const/4 v8, 0x0

    .line 571
    const/4 v7, 0x0

    .line 574
    .local v7, "result":Z
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/app/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 575
    .local v6, "packageName":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "market://details?id="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 576
    .local v5, "marketUri":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "https://play.google.com/store/apps/details?id="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 577
    .local v4, "httpsUri":Ljava/lang/String;
    new-instance v3, Landroid/content/Intent;

    const-string v9, "android.intent.action.VIEW"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-direct {v3, v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 578
    .local v3, "goToMarketViaPlay":Landroid/content/Intent;
    new-instance v2, Landroid/content/Intent;

    const-string v9, "android.intent.action.VIEW"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-direct {v2, v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 580
    .local v2, "goToMarketViaHttps":Landroid/content/Intent;
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/app/MainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v3, v10}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_0

    .line 582
    const/4 v7, 0x1

    .line 583
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v9

    const-string v10, "market://details?id="

    invoke-virtual {v9, v10}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setRateAppUri(Ljava/lang/String;)V

    .line 584
    sget-object v9, Lcom/microsoft/xbox/xle/app/LaunchUtils;->TAG:Ljava/lang/String;

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v11, "PreferredUri:%s"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v5, v12, v13

    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v9

    invoke-virtual {v9, v7}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setRateAppEnable(Z)V

    .line 611
    .end local v2    # "goToMarketViaHttps":Landroid/content/Intent;
    .end local v3    # "goToMarketViaPlay":Landroid/content/Intent;
    .end local v4    # "httpsUri":Ljava/lang/String;
    .end local v5    # "marketUri":Ljava/lang/String;
    .end local v6    # "packageName":Ljava/lang/String;
    .end local v7    # "result":Z
    :goto_1
    return v7

    .line 585
    .restart local v2    # "goToMarketViaHttps":Landroid/content/Intent;
    .restart local v3    # "goToMarketViaPlay":Landroid/content/Intent;
    .restart local v4    # "httpsUri":Ljava/lang/String;
    .restart local v5    # "marketUri":Ljava/lang/String;
    .restart local v6    # "packageName":Ljava/lang/String;
    .restart local v7    # "result":Z
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v9

    invoke-virtual {v9}, Lcom/microsoft/xbox/xle/app/MainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v2, v10}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_1

    .line 587
    const/4 v7, 0x1

    .line 588
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v9

    const-string v10, "https://play.google.com/store/apps/details?id="

    invoke-virtual {v9, v10}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setRateAppUri(Ljava/lang/String;)V

    .line 589
    sget-object v9, Lcom/microsoft/xbox/xle/app/LaunchUtils;->TAG:Ljava/lang/String;

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v11, "PreferredUri:%s"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v4, v12, v13

    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 599
    .end local v2    # "goToMarketViaHttps":Landroid/content/Intent;
    .end local v3    # "goToMarketViaPlay":Landroid/content/Intent;
    .end local v4    # "httpsUri":Ljava/lang/String;
    .end local v5    # "marketUri":Ljava/lang/String;
    .end local v6    # "packageName":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 602
    .local v0, "e":Ljava/lang/Exception;
    sget-object v9, Lcom/microsoft/xbox/xle/app/LaunchUtils;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    const-string v1, "Failed to evaluate app rating intent parameters"

    .line 606
    .local v1, "errorCode":Ljava/lang/String;
    const-string v9, "Failed to evaluate app rating intent parameters"

    invoke-static {v9, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->trackException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 609
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v9

    invoke-virtual {v9, v8}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setRateAppEnable(Z)V

    move v7, v8

    .line 611
    goto :goto_1

    .line 592
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "errorCode":Ljava/lang/String;
    .restart local v2    # "goToMarketViaHttps":Landroid/content/Intent;
    .restart local v3    # "goToMarketViaPlay":Landroid/content/Intent;
    .restart local v4    # "httpsUri":Ljava/lang/String;
    .restart local v5    # "marketUri":Ljava/lang/String;
    .restart local v6    # "packageName":Ljava/lang/String;
    :cond_1
    const/4 v7, 0x0

    .line 593
    :try_start_1
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setRateAppUri(Ljava/lang/String;)V

    .line 594
    sget-object v9, Lcom/microsoft/xbox/xle/app/LaunchUtils;->TAG:Ljava/lang/String;

    const-string v10, "Cannot Rate App is disabled"

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private static getLaunchPostActionRunnable(Ljava/lang/String;IIJZ)Ljava/lang/Runnable;
    .locals 11
    .param p0, "contentId"    # Ljava/lang/String;
    .param p1, "mediaType"    # I
    .param p2, "mediaGroup"    # I
    .param p3, "providerTitleId"    # J
    .param p5, "launchRemoteControl"    # Z

    .prologue
    .line 436
    const/4 v0, 0x0

    .line 438
    .local v0, "action":Ljava/lang/Runnable;
    const-wide/32 v2, 0x3d705025

    cmp-long v1, v2, p3

    if-eqz v1, :cond_0

    .line 439
    invoke-static {p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaType;->getMediaTypeString(I)Ljava/lang/String;

    move-result-object v5

    .line 441
    .local v5, "mediaTypeString":Ljava/lang/String;
    new-instance v8, Lcom/microsoft/xbox/xle/app/LaunchUtils$9;

    sget-object v9, Lcom/microsoft/xbox/toolkit/network/XLEExecutorService;->NETWORK:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/microsoft/xbox/xle/app/LaunchUtils$8;

    move-wide v2, p3

    move-object v4, p0

    move v6, p2

    move/from16 v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/microsoft/xbox/xle/app/LaunchUtils$8;-><init>(JLjava/lang/String;Ljava/lang/String;IZ)V

    invoke-direct {v8, v9, v1}, Lcom/microsoft/xbox/xle/app/LaunchUtils$9;-><init>(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;)V

    .line 473
    .local v8, "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    new-instance v0, Lcom/microsoft/xbox/xle/app/LaunchUtils$10;

    .end local v0    # "action":Ljava/lang/Runnable;
    invoke-direct {v0, v8}, Lcom/microsoft/xbox/xle/app/LaunchUtils$10;-><init>(Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;)V

    .line 483
    .end local v5    # "mediaTypeString":Ljava/lang/String;
    .end local v8    # "task":Lcom/microsoft/xbox/toolkit/XLEFireAndForgetTask;
    .restart local v0    # "action":Ljava/lang/Runnable;
    :cond_0
    return-object v0
.end method

.method public static getProviderLocation(J)Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;
    .locals 6
    .param p0, "titleId"    # J

    .prologue
    .line 78
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getModels()[Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 79
    .local v0, "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getProviderTitleId()J

    move-result-wide v4

    cmp-long v4, v4, p0

    if-nez v4, :cond_0

    .line 81
    sget-object v4, Lcom/microsoft/xbox/xle/app/LaunchUtils$12;->$SwitchMap$com$microsoft$xbox$smartglass$ActiveTitleLocation:[I

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v5

    invoke-virtual {v5}, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 78
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 83
    :pswitch_0
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Full:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    .line 94
    .end local v0    # "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :goto_1
    return-object v1

    .line 85
    .restart local v0    # "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Fill:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    goto :goto_1

    .line 87
    :pswitch_2
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;->Snapped:Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    goto :goto_1

    .line 94
    .end local v0    # "model":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 81
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getProviderLocation(JLcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;
    .locals 2
    .param p0, "titleId"    # J
    .param p2, "defaultLocation"    # Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    .prologue
    .line 98
    invoke-static {p0, p1}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->getProviderLocation(J)Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    move-result-object v0

    .line 99
    .local v0, "location":Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;
    if-nez v0, :cond_0

    .end local p2    # "defaultLocation":Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;
    :goto_0
    return-object p2

    .restart local p2    # "defaultLocation":Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;
    :cond_0
    move-object p2, v0

    goto :goto_0
.end method

.method public static getProviderLocation(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;
    .locals 2
    .param p0, "provider"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    .prologue
    .line 103
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->getProviderLocation(J)Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    move-result-object v0

    goto :goto_0
.end method

.method public static getProviderLocation(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;)Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;
    .locals 1
    .param p0, "provider"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    .param p1, "defaultLocation"    # Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    .prologue
    .line 107
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->getProviderLocation(Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;)Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;

    move-result-object v0

    .line 108
    .local v0, "location":Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;
    if-nez v0, :cond_0

    .end local p1    # "defaultLocation":Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;
    :goto_0
    return-object p1

    .restart local p1    # "defaultLocation":Lcom/microsoft/xbox/service/network/managers/xblshared/TitleLocation;
    :cond_0
    move-object p1, v0

    goto :goto_0
.end method

.method public static isTitlePlayingSameContent(JLjava/lang/String;Ljava/lang/String;)Z
    .locals 12
    .param p0, "titleId"    # J
    .param p2, "bingId"    # Ljava/lang/String;
    .param p3, "providerMediaId"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 394
    invoke-static {}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getInstance()Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/microsoft/xbox/xle/model/NowPlayingGlobalModel;->getModels()[Lcom/microsoft/xbox/xle/model/NowPlayingModel;

    move-result-object v5

    .line 395
    .local v5, "npms":[Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    if-eqz v5, :cond_5

    .line 396
    array-length v9, v5

    move v8, v7

    :goto_0
    if-ge v8, v9, :cond_5

    aget-object v4, v5, v8

    .line 397
    .local v4, "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getCurrentTitleId()I

    move-result v10

    int-to-long v2, v10

    .line 399
    .local v2, "nowPlayingTitleId":J
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getProviderMediaId()Ljava/lang/String;

    move-result-object v0

    .line 400
    .local v0, "assetId":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getNowPlayingMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v10

    if-nez v10, :cond_1

    const/4 v1, 0x0

    .line 401
    .local v1, "mediaId":Ljava/lang/String;
    :goto_1
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getActiveTitleLocation()Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    move-result-object v10

    sget-object v11, Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;->Closed:Lcom/microsoft/xbox/smartglass/ActiveTitleLocation;

    if-ne v10, v11, :cond_2

    .line 396
    :cond_0
    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 400
    .end local v1    # "mediaId":Ljava/lang/String;
    :cond_1
    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getNowPlayingMediaItem()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v10

    invoke-virtual {v10}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 404
    .restart local v1    # "mediaId":Ljava/lang/String;
    :cond_2
    cmp-long v10, v2, p0

    if-nez v10, :cond_0

    .line 408
    sget-object v10, Lcom/microsoft/xbox/xle/app/LaunchUtils$12;->$SwitchMap$com$microsoft$xbox$xle$model$NowPlayingModel$NowPlayingState:[I

    invoke-virtual {v4}, Lcom/microsoft/xbox/xle/model/NowPlayingModel;->getNowPlayingState()Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;

    move-result-object v11

    invoke-virtual {v11}, Lcom/microsoft/xbox/xle/model/NowPlayingModel$NowPlayingState;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    goto :goto_2

    .line 431
    .end local v0    # "assetId":Ljava/lang/String;
    .end local v1    # "mediaId":Ljava/lang/String;
    .end local v2    # "nowPlayingTitleId":J
    .end local v4    # "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :goto_3
    :pswitch_0
    return v6

    .line 416
    .restart local v0    # "assetId":Ljava/lang/String;
    .restart local v1    # "mediaId":Ljava/lang/String;
    .restart local v2    # "nowPlayingTitleId":J
    .restart local v4    # "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :pswitch_1
    if-nez p3, :cond_3

    if-nez p2, :cond_3

    .line 417
    sget-object v7, Lcom/microsoft/xbox/xle/app/LaunchUtils;->TAG:Ljava/lang/String;

    const-string v8, "check playing same title media did not specific media, treated as playing"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 419
    :cond_3
    if-eqz p2, :cond_4

    .line 420
    invoke-virtual {p2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    goto :goto_3

    .line 421
    :cond_4
    if-eqz p3, :cond_0

    .line 422
    invoke-virtual {p3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    goto :goto_3

    .end local v0    # "assetId":Ljava/lang/String;
    .end local v1    # "mediaId":Ljava/lang/String;
    .end local v2    # "nowPlayingTitleId":J
    .end local v4    # "npm":Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    :cond_5
    move v6, v7

    .line 431
    goto :goto_3

    .line 408
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static launchBroadcastingVideo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "broadcastProvider"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p1, "broadcastId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param

    .prologue
    .line 501
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 502
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 504
    const-string v1, "Twitch"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 505
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/deeplink/TwitchDeepLinker;->launchStream(Ljava/lang/String;)Z

    .line 510
    :cond_0
    :goto_0
    return-void

    .line 506
    :cond_1
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 507
    new-instance v0, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker;-><init>()V

    .line 508
    .local v0, "beamDeepLinker":Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker;->launchStream(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/deeplink/BeamDeepLinker$LaunchResult;

    goto :goto_0
.end method

.method public static launchConnectionFaq()V
    .locals 4

    .prologue
    .line 489
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 490
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "http://go.microsoft.com/fwlink/?LinkID=391430"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 492
    :try_start_0
    invoke-static {v1}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 496
    :goto_0
    return-void

    .line 493
    :catch_0
    move-exception v0

    .line 494
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v2, Lcom/microsoft/xbox/xle/app/LaunchUtils;->TAG:Ljava/lang/String;

    const-string v3, "Error navigating to http://go.microsoft.com/fwlink/?LinkID=391430"

    invoke-static {v2, v3, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static launchHtmlCompanion(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V
    .locals 9
    .param p0, "activityData"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .prologue
    .line 290
    new-instance v8, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;

    invoke-direct {v8}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;-><init>()V

    .line 291
    .local v8, "params":Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;
    invoke-virtual {v8, p0}, Lcom/microsoft/xbox/toolkit/ui/ActivityParameters;->putSelectedActivityData(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    .line 293
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/app/activity/CanvasWebViewActivity;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->IsScreenOnStack(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 294
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/app/activity/CanvasWebViewActivity;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->CountPopsToScreen(Ljava/lang/Class;)I

    move-result v6

    .line 295
    .local v6, "count":I
    new-instance v7, Lcom/microsoft/xbox/xle/app/LaunchUtils$4;

    invoke-direct {v7, v6, v8}, Lcom/microsoft/xbox/xle/app/LaunchUtils$4;-><init>(ILcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    .line 305
    .local v7, "gotoCanvas":Ljava/lang/Runnable;
    if-nez v6, :cond_0

    .line 307
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f070344

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070343

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0707c7

    .line 308
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/microsoft/xbox/xle/app/LaunchUtils$5;

    invoke-direct {v3, v7}, Lcom/microsoft/xbox/xle/app/LaunchUtils$5;-><init>(Ljava/lang/Runnable;)V

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f07060d

    .line 313
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 307
    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 320
    .end local v6    # "count":I
    .end local v7    # "gotoCanvas":Ljava/lang/Runnable;
    :goto_0
    return-void

    .line 315
    .restart local v6    # "count":I
    .restart local v7    # "gotoCanvas":Ljava/lang/Runnable;
    :cond_0
    invoke-interface {v7}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 318
    .end local v6    # "count":I
    .end local v7    # "gotoCanvas":Ljava/lang/Runnable;
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/app/activity/CanvasWebViewActivity;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, v8}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;ZLcom/microsoft/xbox/toolkit/ui/ActivityParameters;)V

    goto :goto_0
.end method

.method private static launchNativeCompanion(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p0, "companionName"    # Ljava/lang/String;
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "deepLink"    # Ljava/lang/String;

    .prologue
    .line 323
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070689

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-static {v0, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 324
    .local v1, "description":Ljava/lang/String;
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070d8b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f0707c7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/microsoft/xbox/xle/app/LaunchUtils$6;

    invoke-direct {v3, p1, p2}, Lcom/microsoft/xbox/xle/app/LaunchUtils$6;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070611

    .line 354
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 324
    invoke-static/range {v0 .. v5}, Lcom/microsoft/xbox/xle/app/XLEUtil;->showOkCancelDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 355
    return-void
.end method

.method public static launchNativeOrHtmlCompanion(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V
    .locals 4
    .param p0, "activityData"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getActivityLaunchInfo()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 275
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->isNativeCompanion()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 276
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->getActivityLaunchInfo()Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;

    move-result-object v1

    .line 277
    .local v1, "info":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;
    if-eqz v1, :cond_0

    .line 278
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 279
    .local v2, "packageName":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->getDeepLinkUrl()Ljava/lang/String;

    move-result-object v0

    .line 280
    .local v0, "deepLink":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->Name:Ljava/lang/String;

    invoke-static {v3, v2, v0}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->launchNativeCompanion(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    .end local v0    # "deepLink":Ljava/lang/String;
    .end local v1    # "info":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;
    .end local v2    # "packageName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 283
    :cond_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/DialogManager;->dismissAppBar()V

    .line 284
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/LaunchUtils;->launchHtmlCompanion(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;)V

    goto :goto_0
.end method

.method public static launchRateApp()V
    .locals 13

    .prologue
    const/high16 v12, 0x80000

    const/4 v11, 0x0

    .line 519
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getRateAppEnable()Z

    move-result v6

    if-nez v6, :cond_0

    .line 520
    sget-object v6, Lcom/microsoft/xbox/xle/app/LaunchUtils;->TAG:Ljava/lang/String;

    const-string v7, "Rate App is disabled"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    .local v4, "intentUri":Ljava/lang/String;
    :goto_0
    return-void

    .line 526
    .end local v4    # "intentUri":Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getRateAppUri()Ljava/lang/String;

    move-result-object v4

    .line 528
    .restart local v4    # "intentUri":Ljava/lang/String;
    sget-object v6, Lcom/microsoft/xbox/xle/app/LaunchUtils;->TAG:Ljava/lang/String;

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "Uri:%s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v4, v9, v10

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    const-string v6, "market://details?id="

    if-eq v4, v6, :cond_1

    const-string v6, "https://play.google.com/store/apps/details?id="

    if-eq v4, v6, :cond_1

    .line 530
    sget-object v6, Lcom/microsoft/xbox/xle/app/LaunchUtils;->TAG:Ljava/lang/String;

    const-string v7, "Not an approved Uri to launch app store listing"

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setRateAppEnable(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 557
    :catch_0
    move-exception v0

    .line 559
    .local v0, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/microsoft/xbox/xle/app/LaunchUtils;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    const-string v1, "Failed to launch app rating intent"

    .line 563
    .local v1, "errorCode":Ljava/lang/String;
    const-string v6, "Failed to launch app rating intent"

    invoke-static {v6, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClientError;->trackException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 566
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v6

    invoke-virtual {v6, v11}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->setRateAppEnable(Z)V

    goto :goto_0

    .line 539
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "errorCode":Ljava/lang/String;
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v6

    invoke-virtual {v6}, Lcom/microsoft/xbox/xle/app/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 540
    .local v5, "packageName":Ljava/lang/String;
    sget-object v6, Lcom/microsoft/xbox/xle/app/LaunchUtils;->TAG:Ljava/lang/String;

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "PackageName:%s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v5, v9, v10

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/microsoft/xbox/toolkit/XLELog;->Info(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    new-instance v3, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v3, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 546
    .local v3, "goToAppListing":Landroid/content/Intent;
    const/high16 v2, 0x48000000    # 131072.0f

    .line 547
    .local v2, "flags":I
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x15

    if-lt v6, v7, :cond_2

    .line 548
    or-int/2addr v2, v12

    .line 553
    :goto_1
    invoke-virtual {v3, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 556
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/microsoft/xbox/xle/app/MainActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 551
    :cond_2
    or-int/2addr v2, v12

    goto :goto_1
.end method

.method private static onMSATicketAcquired(Ljava/lang/String;)V
    .locals 4
    .param p0, "ticket"    # Ljava/lang/String;

    .prologue
    .line 230
    sget-object v1, Lcom/microsoft/xbox/xle/app/LaunchUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Ticket: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 233
    new-instance v0, Lcom/microsoft/xbox/smartglass/AuthInfo;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getCurrentSandboxId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/smartglass/AuthInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    .local v0, "sgAuthInfo":Lcom/microsoft/xbox/smartglass/AuthInfo;
    invoke-static {}, Lcom/microsoft/xbox/smartglass/SGPlatform;->getTokenManager()Lcom/microsoft/xbox/smartglass/TokenManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/microsoft/xbox/smartglass/TokenManager;->setAuthInfo(Lcom/microsoft/xbox/smartglass/AuthInfo;Z)V

    .line 237
    .end local v0    # "sgAuthInfo":Lcom/microsoft/xbox/smartglass/AuthInfo;
    :cond_0
    new-instance v1, Lcom/microsoft/xbox/xle/app/LaunchUtils$3;

    invoke-direct {v1}, Lcom/microsoft/xbox/xle/app/LaunchUtils$3;-><init>()V

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/ThreadManager;->UIThreadPost(Ljava/lang/Runnable;)V

    .line 248
    return-void
.end method

.method private static prepareHelp(JLjava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .locals 12
    .param p0, "titleId"    # J
    .param p2, "productId"    # Ljava/lang/String;
    .param p3, "titleName"    # Ljava/lang/String;

    .prologue
    .line 367
    invoke-static {}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->Instance()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment;->getEnvironment()Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    move-result-object v7

    sget-object v8, Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;->PROD:Lcom/microsoft/xbox/toolkit/network/XboxLiveEnvironment$Environment;

    if-ne v7, v8, :cond_0

    const-string v1, ""

    .line 368
    .local v1, "envPrefix":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getCurrentSandboxId()Ljava/lang/String;

    move-result-object v6

    .line 369
    .local v6, "sandboxId":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getLegalLocale()Ljava/lang/String;

    move-result-object v3

    .line 370
    .local v3, "locale":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/XleProjectSpecificDataProvider;->getRegion()Ljava/lang/String;

    move-result-object v5

    .line 371
    .local v5, "region":Ljava/lang/String;
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "https://start.ui%s.xboxlive.com/help/sg/v1/?TitleId=%X&ProductId=%s&sandboxId=%s&MarketplaceId=%s&Region=%s"

    const/4 v9, 0x6

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v1, v9, v10

    const/4 v10, 0x1

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    aput-object p2, v9, v10

    const/4 v10, 0x3

    aput-object v6, v9, v10

    const/4 v10, 0x4

    aput-object v3, v9, v10

    const/4 v10, 0x5

    aput-object v5, v9, v10

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 375
    .local v4, "playUrl":Ljava/lang/String;
    const/4 v0, 0x0

    .line 376
    .local v0, "act":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    new-instance v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;

    invoke-direct {v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;-><init>()V

    .line 378
    .local v2, "launchInfo":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getHelpCompanionWhiteListUrls()[Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->setWhitelistUrls([Ljava/lang/String;)V

    .line 379
    invoke-static {v4}, Ljava/net/URI;->create(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;->setActivityUrl(Ljava/net/URI;)V

    .line 380
    new-instance v0, Lcom/microsoft/xbox/xle/app/LaunchUtils$7;

    .end local v0    # "act":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/LaunchUtils$7;-><init>()V

    .line 386
    .restart local v0    # "act":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    iput-object p3, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->Name:Ljava/lang/String;

    .line 387
    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->setActivityLaunchInfo(Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;)V

    .line 388
    invoke-virtual {v0, p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;->setNowPlayingTitleId(J)V

    .line 390
    return-object v0

    .line 367
    .end local v0    # "act":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityItem;
    .end local v1    # "envPrefix":Ljava/lang/String;
    .end local v2    # "launchInfo":Lcom/microsoft/xbox/service/model/edsv2/EDSV2ActivityLaunchInfo;
    .end local v3    # "locale":Ljava/lang/String;
    .end local v4    # "playUrl":Ljava/lang/String;
    .end local v5    # "region":Ljava/lang/String;
    .end local v6    # "sandboxId":Ljava/lang/String;
    :cond_0
    const-string v1, ".dnet"

    goto :goto_0
.end method

.method public static showRemoteControl()V
    .locals 1

    .prologue
    .line 226
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showRemoteControl()V

    .line 227
    return-void
.end method

.method private static startActivity(Landroid/content/Intent;)V
    .locals 6
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 359
    :try_start_0
    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/microsoft/xbox/xle/app/MainActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 364
    :goto_0
    return-void

    .line 360
    :catch_0
    move-exception v0

    .line 361
    .local v0, "ex":Landroid/content/ActivityNotFoundException;
    invoke-static {}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v3, 0x7f070697

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v4, 0x7f0707ba

    .line 362
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f0707c7

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 361
    invoke-interface {v1, v2, v3, v4, v5}, Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;->showNonFatalAlertDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V

    goto :goto_0
.end method
