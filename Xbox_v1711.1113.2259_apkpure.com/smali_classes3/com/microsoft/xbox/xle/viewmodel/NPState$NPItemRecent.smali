.class public Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;
.super Ljava/lang/Object;
.source "NPState.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/NPState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NPItemRecent"
.end annotation


# instance fields
.field private volatile transient hashCode:I

.field private final itemId:Ljava/lang/String;

.field private final providerTitleId:J

.field private final recent:Lcom/microsoft/xbox/service/model/recents/Recent;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/recents/Recent;)V
    .locals 2
    .param p1, "recent"    # Lcom/microsoft/xbox/service/model/recents/Recent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 279
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 281
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;->recent:Lcom/microsoft/xbox/service/model/recents/Recent;

    .line 282
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;->recent:Lcom/microsoft/xbox/service/model/recents/Recent;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;->recent:Lcom/microsoft/xbox/service/model/recents/Recent;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/recents/RecentItem;->getProviderTitleId()J

    move-result-wide v0

    :goto_0
    iput-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;->providerTitleId:J

    .line 283
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;->recent:Lcom/microsoft/xbox/service/model/recents/Recent;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;->recent:Lcom/microsoft/xbox/service/model/recents/Recent;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/recents/RecentItem;->getItemId()Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;->itemId:Ljava/lang/String;

    .line 284
    return-void

    .line 282
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 283
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 349
    if-ne p1, p0, :cond_1

    .line 355
    :cond_0
    :goto_0
    return v1

    .line 351
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;

    if-nez v3, :cond_2

    move v1, v2

    .line 352
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 354
    check-cast v0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;

    .line 355
    .local v0, "that":Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;
    iget-wide v4, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;->providerTitleId:J

    iget-wide v6, v0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;->providerTitleId:J

    cmp-long v3, v4, v6

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;->itemId:Ljava/lang/String;

    iget-object v4, v0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;->itemId:Ljava/lang/String;

    .line 356
    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->stringsEqualCaseInsensitive(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getItemId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 300
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;->itemId:Ljava/lang/String;

    return-object v0
.end method

.method public getLi()Lcom/microsoft/xbox/service/model/pins/LaunchableItem;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 289
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;->recent:Lcom/microsoft/xbox/service/model/recents/Recent;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/recents/Recent;->Item:Lcom/microsoft/xbox/service/model/recents/RecentItem;

    return-object v0
.end method

.method public getNPM()Lcom/microsoft/xbox/xle/model/NowPlayingModel;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 322
    const/4 v0, 0x0

    return-object v0
.end method

.method public getProviderTitleId()J
    .locals 2

    .prologue
    .line 294
    iget-wide v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;->providerTitleId:J

    return-wide v0
.end method

.method public getRecent()Lcom/microsoft/xbox/service/model/recents/Recent;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 311
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;->recent:Lcom/microsoft/xbox/service/model/recents/Recent;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 338
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;->hashCode:I

    if-nez v0, :cond_0

    .line 339
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;->hashCode:I

    .line 340
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;->providerTitleId:J

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;->hashCode:I

    .line 341
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;->itemId:Ljava/lang/String;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;->hashCode:I

    .line 344
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;->hashCode:I

    return v0
.end method

.method public isNPM()Z
    .locals 1

    .prologue
    .line 316
    const/4 v0, 0x0

    return v0
.end method

.method public isRecent()Z
    .locals 1

    .prologue
    .line 305
    const/4 v0, 0x1

    return v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 327
    const/4 v0, 0x1

    return v0
.end method

.method public toRecent()Lcom/microsoft/xbox/xle/viewmodel/NPState$NPItemRecent;
    .locals 0
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 333
    return-object p0
.end method
