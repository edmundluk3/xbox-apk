.class public abstract Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaSubscriptionResponse;
.super Ljava/lang/Object;
.source "RtaDataTypes.java"

# interfaces
.implements Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaResponse;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "RtaSubscriptionResponse"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract errorReason()Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract errorType()Lcom/microsoft/xbox/xbservices/rta/RtaDataTypes$RtaErrorType;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public abstract sequenceNumber()I
.end method
