.class public Lcom/microsoft/xbox/xbservices/toolkit/gson/GsonUtil;
.super Ljava/lang/Object;
.source "GsonUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createMinimumGsonBuilder()Lcom/google/gson/GsonBuilder;
    .locals 4

    .prologue
    .line 11
    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const/16 v3, 0x80

    aput v3, v1, v2

    .line 12
    invoke-virtual {v0, v1}, Lcom/google/gson/GsonBuilder;->excludeFieldsWithModifiers([I)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    .line 13
    invoke-static {}, Lcom/microsoft/xbox/xbservices/toolkit/gson/AutoValueGsonAdapterFactory;->create()Lcom/google/gson/TypeAdapterFactory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/gson/GsonBuilder;->registerTypeAdapterFactory(Lcom/google/gson/TypeAdapterFactory;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    new-instance v1, Lcom/microsoft/xbox/xbservices/toolkit/gson/PostProcessingEnablerGson;

    invoke-direct {v1}, Lcom/microsoft/xbox/xbservices/toolkit/gson/PostProcessingEnablerGson;-><init>()V

    .line 14
    invoke-virtual {v0, v1}, Lcom/google/gson/GsonBuilder;->registerTypeAdapterFactory(Lcom/google/gson/TypeAdapterFactory;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    const-class v1, Lcom/google/common/collect/ImmutableList;

    new-instance v2, Lcom/microsoft/xbox/xbservices/toolkit/gson/ImmutableListDeserializer;

    invoke-direct {v2}, Lcom/microsoft/xbox/xbservices/toolkit/gson/ImmutableListDeserializer;-><init>()V

    .line 15
    invoke-virtual {v0, v1, v2}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    const-class v1, Lcom/google/common/collect/ImmutableSet;

    new-instance v2, Lcom/microsoft/xbox/xbservices/toolkit/gson/ImmutableSetDeserializer;

    invoke-direct {v2}, Lcom/microsoft/xbox/xbservices/toolkit/gson/ImmutableSetDeserializer;-><init>()V

    .line 16
    invoke-virtual {v0, v1, v2}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    .line 11
    return-object v0
.end method
