.class public Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;
.super Landroid/support/design/widget/CoordinatorLayout;
.source "ViewPagerWithTabs.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;,
        Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;,
        Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;
    }
.end annotation


# static fields
.field public static final TELEMETRYPAGEACTION:Ljava/lang/String; = "pageaction"

.field public static final TELEMETRYPAGENAME:Ljava/lang/String; = "pagename"

.field public static final TELEMETRYPAGETRANSITIONORDER:Ljava/lang/String; = "pagetransitionorder"

.field public static final TELEMETRYUNKNOWN:Ljava/lang/String; = "unknown"


# instance fields
.field private final INITIALPAGETRANSITIONSTATE:Ljava/lang/String;

.field private actionNames:[Ljava/lang/String;

.field private headerView:Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

.field private pageTitleFunction:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/functions/GenericFunction",
            "<+",
            "Ljava/lang/CharSequence;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private pageTransitions:Ljava/lang/String;

.field private final params:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;

.field private sectionsPagerAdapter:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;

.field private tabLayout:Landroid/support/design/widget/TabLayout;

.field private tabNames:[Ljava/lang/String;

.field private telemetryData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private telemetryFunction:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/functions/GenericFunction",
            "<",
            "Ljava/lang/Void;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private viewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Landroid/support/design/widget/CoordinatorLayout;-><init>(Landroid/content/Context;)V

    .line 29
    const-string v0, "0"

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->INITIALPAGETRANSITIONSTATE:Ljava/lang/String;

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->telemetryData:Ljava/util/HashMap;

    .line 38
    const-string v0, "0"

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->pageTransitions:Ljava/lang/String;

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->params:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/CoordinatorLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    const-string v0, "0"

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->INITIALPAGETRANSITIONSTATE:Ljava/lang/String;

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->telemetryData:Ljava/util/HashMap;

    .line 38
    const-string v0, "0"

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->pageTransitions:Ljava/lang/String;

    .line 60
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->captureHeaderParams(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->params:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Landroid/support/design/widget/CoordinatorLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    const-string v0, "0"

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->INITIALPAGETRANSITIONSTATE:Ljava/lang/String;

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->telemetryData:Ljava/util/HashMap;

    .line 38
    const-string v0, "0"

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->pageTransitions:Ljava/lang/String;

    .line 65
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->captureHeaderParams(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->params:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "params"    # Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Landroid/support/design/widget/CoordinatorLayout;-><init>(Landroid/content/Context;)V

    .line 29
    const-string v0, "0"

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->INITIALPAGETRANSITIONSTATE:Ljava/lang/String;

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->telemetryData:Ljava/util/HashMap;

    .line 38
    const-string v0, "0"

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->pageTransitions:Ljava/lang/String;

    .line 55
    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->params:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;

    .line 56
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)Landroid/support/design/widget/TabLayout;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->tabLayout:Landroid/support/design/widget/TabLayout;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->headerView:Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->tabNames:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->actionNames:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->telemetryData:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)Lcom/microsoft/xbox/toolkit/functions/GenericFunction;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->telemetryFunction:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->pageTransitions:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$602(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->pageTransitions:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)Landroid/support/v4/view/ViewPager;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->viewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)Lcom/microsoft/xbox/toolkit/functions/GenericFunction;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->pageTitleFunction:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    return-object v0
.end method

.method private captureHeaderParams(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 308
    sget-object v3, Lcom/microsoft/xboxone/smartglass/R$styleable;->ViewPagerWithTabs:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 310
    .local v0, "a":Landroid/content/res/TypedArray;
    :try_start_0
    new-instance v1, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;

    const/4 v3, 0x0

    .line 311
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;-><init>(Ljava/lang/String;)V

    .line 312
    .local v1, "headerParams":Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;
    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;

    const/4 v3, 0x1

    .line 313
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;-><init>(Ljava/lang/String;)V

    .line 314
    .local v2, "subHeaderParams":Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;
    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;

    invoke-direct {v3, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;-><init>(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 316
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 314
    return-object v3

    .line 316
    .end local v1    # "headerParams":Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;
    .end local v2    # "subHeaderParams":Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;
    :catchall_0
    move-exception v3

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v3
.end method

.method private extractXmlChildren()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 288
    const/4 v2, 0x0

    .line 289
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 290
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 291
    .local v0, "child":Landroid/view/View;
    instance-of v3, v0, Landroid/support/design/widget/FloatingActionButton;

    if-nez v3, :cond_1

    .line 292
    if-nez v2, :cond_0

    .line 293
    new-instance v2, Ljava/util/LinkedList;

    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 295
    .restart local v2    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    :cond_0
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 289
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 298
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    if-eqz v2, :cond_3

    .line 299
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 300
    .restart local v0    # "child":Landroid/view/View;
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->removeView(Landroid/view/View;)V

    goto :goto_1

    .line 304
    .end local v0    # "child":Landroid/view/View;
    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    :cond_4
    return-object v2
.end method


# virtual methods
.method public addPage(Landroid/view/View;)Z
    .locals 5
    .param p1, "page"    # Landroid/view/View;

    .prologue
    .line 186
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->sectionsPagerAdapter:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->addPage(Landroid/view/View;)Z

    move-result v0

    .line 187
    .local v0, "ret":Z
    if-eqz v0, :cond_0

    .line 188
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->tabLayout:Landroid/support/design/widget/TabLayout;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->tabLayout:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v2}, Landroid/support/design/widget/TabLayout;->newTab()Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->sectionsPagerAdapter:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->sectionsPagerAdapter:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->getPageTitle(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/design/widget/TabLayout$Tab;->setText(Ljava/lang/CharSequence;)Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/design/widget/TabLayout;->addTab(Landroid/support/design/widget/TabLayout$Tab;)V

    .line 190
    :cond_0
    return v0
.end method

.method public addPageAt(ILandroid/view/View;)Z
    .locals 4
    .param p1, "index"    # I
    .param p2, "page"    # Landroid/view/View;

    .prologue
    .line 194
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->sectionsPagerAdapter:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;

    invoke-virtual {v1, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->addPageAt(ILandroid/view/View;)Z

    move-result v0

    .line 195
    .local v0, "ret":Z
    if-eqz v0, :cond_0

    .line 196
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->tabLayout:Landroid/support/design/widget/TabLayout;

    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->tabLayout:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v2}, Landroid/support/design/widget/TabLayout;->newTab()Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->sectionsPagerAdapter:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;

    invoke-virtual {v3, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->getPageTitle(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/design/widget/TabLayout$Tab;->setText(Ljava/lang/CharSequence;)Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Landroid/support/design/widget/TabLayout;->addTab(Landroid/support/design/widget/TabLayout$Tab;I)V

    .line 198
    :cond_0
    return v0
.end method

.method public addPages(Ljava/util/Collection;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Landroid/view/View;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 175
    .local p1, "pages":Ljava/util/Collection;, "Ljava/util/Collection<+Landroid/view/View;>;"
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->sectionsPagerAdapter:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->getCount()I

    move-result v0

    .line 176
    .local v0, "idx":I
    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->sectionsPagerAdapter:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;

    invoke-virtual {v4, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->addPages(Ljava/util/Collection;)Z

    move-result v3

    .line 177
    .local v3, "ret":Z
    if-eqz v3, :cond_0

    .line 178
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 179
    .local v2, "page":Landroid/view/View;
    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->tabLayout:Landroid/support/design/widget/TabLayout;

    iget-object v6, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->tabLayout:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v6}, Landroid/support/design/widget/TabLayout;->newTab()Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v6

    iget-object v7, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->sectionsPagerAdapter:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "idx":I
    .local v1, "idx":I
    invoke-virtual {v7, v0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->getPageTitle(I)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/support/design/widget/TabLayout$Tab;->setText(Ljava/lang/CharSequence;)Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/support/design/widget/TabLayout;->addTab(Landroid/support/design/widget/TabLayout$Tab;)V

    move v0, v1

    .line 180
    .end local v1    # "idx":I
    .restart local v0    # "idx":I
    goto :goto_0

    .line 182
    .end local v2    # "page":Landroid/view/View;
    :cond_0
    return v3
.end method

.method public ensureInitialized()V
    .locals 4

    .prologue
    .line 225
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->sectionsPagerAdapter:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;

    if-nez v2, :cond_1

    .line 226
    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->extractXmlChildren()Ljava/util/List;

    move-result-object v0

    .line 228
    .local v0, "children":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 229
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f03028d

    const/4 v3, 0x1

    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 231
    const v2, 0x7f0e0c13

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->headerView:Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

    .line 232
    const v2, 0x7f0e0c12

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/design/widget/TabLayout;

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->tabLayout:Landroid/support/design/widget/TabLayout;

    .line 233
    const v2, 0x7f0e0c14

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/ViewPager;

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 235
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->params:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;

    if-eqz v2, :cond_0

    .line 236
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->headerView:Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->params:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;->getHeaderParams()Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->setHeaderText(Ljava/lang/CharSequence;)V

    .line 237
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->headerView:Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->params:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$HeaderParams;->getSubHeaderParams()Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$TextParams;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->setSubHeaderText(Ljava/lang/CharSequence;)V

    .line 240
    :cond_0
    new-instance v2, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;-><init>(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)V

    iput-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->sectionsPagerAdapter:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;

    .line 241
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->viewPager:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->sectionsPagerAdapter:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 242
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->tabLayout:Landroid/support/design/widget/TabLayout;

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v3}, Landroid/support/design/widget/TabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 243
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->viewPager:Landroid/support/v4/view/ViewPager;

    new-instance v3, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;

    invoke-direct {v3, p0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$1;-><init>(Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 282
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->sectionsPagerAdapter:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->addPages(Ljava/util/Collection;)Z

    .line 283
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->tabLayout:Landroid/support/design/widget/TabLayout;

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->sectionsPagerAdapter:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;

    invoke-virtual {v2, v3}, Landroid/support/design/widget/TabLayout;->setTabsFromPagerAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 285
    .end local v0    # "children":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    :cond_1
    return-void
.end method

.method public getActivePage()Landroid/view/View;
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->sectionsPagerAdapter:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->getActivePageIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getActivePageIndex()I
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    return v0
.end method

.method public getHeaderText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->headerView:Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->getHeaderText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getPageAt(I)Landroid/view/View;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 171
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->sectionsPagerAdapter:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getPageContainer()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->viewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method public getPageCount()I
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->sectionsPagerAdapter:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method public getSubHeaderText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->headerView:Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->getSubHeaderText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 220
    invoke-super {p0}, Landroid/support/design/widget/CoordinatorLayout;->onFinishInflate()V

    .line 221
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->ensureInitialized()V

    .line 222
    return-void
.end method

.method public removePage(Landroid/view/View;)Z
    .locals 3
    .param p1, "page"    # Landroid/view/View;

    .prologue
    .line 202
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->sectionsPagerAdapter:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;

    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->getPageIndex(Landroid/view/View;)I

    move-result v0

    .line 203
    .local v0, "index":I
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->sectionsPagerAdapter:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;

    invoke-virtual {v2, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->removePage(Landroid/view/View;)Z

    move-result v1

    .line 204
    .local v1, "ret":Z
    if-eqz v1, :cond_0

    .line 205
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->tabLayout:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v2, v0}, Landroid/support/design/widget/TabLayout;->removeTabAt(I)V

    .line 207
    :cond_0
    return v1
.end method

.method public removePageAt(I)Landroid/view/View;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 211
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->sectionsPagerAdapter:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;

    invoke-virtual {v1, p1}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->removePageAt(I)Landroid/view/View;

    move-result-object v0

    .line 212
    .local v0, "ret":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 213
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->tabLayout:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v1, p1}, Landroid/support/design/widget/TabLayout;->removeTabAt(I)V

    .line 215
    :cond_0
    return-object v0
.end method

.method public setActivePageIndex(I)Z
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 154
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->sectionsPagerAdapter:Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs$SectionsPagerAdapter;->getCount()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 155
    :cond_0
    const/4 v0, 0x0

    .line 159
    :goto_0
    return v0

    .line 157
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 158
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->headerView:Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->tabLayout:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v1, p1}, Landroid/support/design/widget/TabLayout;->getTabAt(I)Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/design/widget/TabLayout$Tab;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->setSubHeaderText(Ljava/lang/CharSequence;)V

    .line 159
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setHeaderBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 95
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->headerView:Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->setBackgroundColor(I)V

    .line 96
    return-void
.end method

.method public setHeaderStyle(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 87
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->headerView:Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->setHeaderStyle(I)V

    .line 88
    return-void
.end method

.method public setHeaderText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->headerView:Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->setHeaderText(Ljava/lang/CharSequence;)V

    .line 84
    return-void
.end method

.method public setHeaderTypefaceSource(Ljava/lang/String;)V
    .locals 1
    .param p1, "typefaceSource"    # Ljava/lang/String;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->headerView:Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->setHeaderTypefaceSource(Ljava/lang/String;)V

    .line 92
    return-void
.end method

.method public setHeaderVisibility(Ljava/lang/Boolean;)V
    .locals 1
    .param p1, "headerVisibility"    # Ljava/lang/Boolean;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->headerView:Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->setHeaderVisibility(Ljava/lang/Boolean;)V

    .line 132
    return-void
.end method

.method public setPageTitleFunction(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/functions/GenericFunction",
            "<+",
            "Ljava/lang/CharSequence;",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p1, "pageTitleFunction":Lcom/microsoft/xbox/toolkit/functions/GenericFunction;, "Lcom/microsoft/xbox/toolkit/functions/GenericFunction<+Ljava/lang/CharSequence;Landroid/view/View;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->pageTitleFunction:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    .line 76
    return-void
.end method

.method public setSelectedTabIndicatorColor(I)V
    .locals 1
    .param p1, "color"    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 111
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->tabLayout:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/TabLayout;->setSelectedTabIndicatorColor(I)V

    .line 112
    return-void
.end method

.method public setSelectedTabIndicatorColorToProfileColor()V
    .locals 1

    .prologue
    .line 107
    invoke-static {}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getInstance()Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/ApplicationSettingManager;->getMePreferredColor()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->setSelectedTabIndicatorColor(I)V

    .line 108
    return-void
.end method

.method public setSubHeaderStyle(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->headerView:Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->setSubHeaderStyle(I)V

    .line 124
    return-void
.end method

.method public setSubHeaderText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->headerView:Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->setSubHeaderText(Ljava/lang/CharSequence;)V

    .line 120
    return-void
.end method

.method public setSubHeaderTypefaceSource(Ljava/lang/String;)V
    .locals 1
    .param p1, "typefaceSource"    # Ljava/lang/String;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->headerView:Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->setSubHeaderTypefaceSource(Ljava/lang/String;)V

    .line 128
    return-void
.end method

.method public setTabLayoutBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 99
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->tabLayout:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/TabLayout;->setBackgroundColor(I)V

    .line 100
    return-void
.end method

.method public setTabLayoutBackgroundResource(I)V
    .locals 1
    .param p1, "res"    # I

    .prologue
    .line 103
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->tabLayout:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/TabLayout;->setBackgroundResource(I)V

    .line 104
    return-void
.end method

.method public setTabLayoutBehaviorEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 135
    iget-object v2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->tabLayout:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v2}, Landroid/support/design/widget/TabLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;

    .line 136
    .local v1, "layoutParams":Landroid/support/design/widget/CoordinatorLayout$LayoutParams;
    invoke-virtual {v1}, Landroid/support/design/widget/CoordinatorLayout$LayoutParams;->getBehavior()Landroid/support/design/widget/CoordinatorLayout$Behavior;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;

    .line 137
    .local v0, "behavior":Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;
    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/behaviors/StackingBehavior;->setEnabled(Z)V

    .line 138
    return-void
.end method

.method public setTelemetryFunction(Lcom/microsoft/xbox/toolkit/functions/GenericFunction;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p2, "pageNames"    # [Ljava/lang/String;
    .param p3, "pageActions"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/functions/GenericFunction",
            "<",
            "Ljava/lang/Void;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;[",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 69
    .local p1, "function":Lcom/microsoft/xbox/toolkit/functions/GenericFunction;, "Lcom/microsoft/xbox/toolkit/functions/GenericFunction<Ljava/lang/Void;Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->telemetryFunction:Lcom/microsoft/xbox/toolkit/functions/GenericFunction;

    .line 70
    iput-object p2, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->tabNames:[Ljava/lang/String;

    .line 71
    iput-object p3, p0, Lcom/microsoft/xbox/toolkit/ui/ViewPagerWithTabs;->actionNames:[Ljava/lang/String;

    .line 72
    return-void
.end method
