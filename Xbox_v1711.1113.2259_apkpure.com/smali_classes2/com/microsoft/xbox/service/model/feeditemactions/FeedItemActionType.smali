.class public final enum Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
.super Ljava/lang/Enum;
.source "FeedItemActionType.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;",
        ">;",
        "Lcom/microsoft/xbox/xle/app/adapter/SpinnerArrayItem;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

.field public static final enum COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

.field public static final enum LIKE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

.field public static final enum SHARE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;


# instance fields
.field private final errorCode:J

.field private final query:Ljava/lang/String;

.field private final resId:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field

.field private final telemetryName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .prologue
    const/4 v14, 0x2

    const/4 v13, 0x1

    const/4 v3, 0x0

    .line 16
    new-instance v1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    const-string v2, "LIKE"

    const v4, 0x7f070725

    const-string v5, "likes"

    const-wide/16 v6, 0x232c

    const-string v8, "Like"

    invoke-direct/range {v1 .. v8}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;-><init>(Ljava/lang/String;IILjava/lang/String;JLjava/lang/String;)V

    sput-object v1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->LIKE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .line 18
    new-instance v5, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    const-string v6, "COMMENT"

    const v8, 0x7f070341

    const-string v9, "comments"

    const-wide/16 v10, 0x232b

    const-string v12, "Comment"

    move v7, v13

    invoke-direct/range {v5 .. v12}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;-><init>(Ljava/lang/String;IILjava/lang/String;JLjava/lang/String;)V

    sput-object v5, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .line 19
    new-instance v5, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    const-string v6, "SHARE"

    const v8, 0x7f070c27

    const-string v9, "shares"

    const-wide/16 v10, 0x232d

    const-string v12, "Share"

    move v7, v14

    invoke-direct/range {v5 .. v12}, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;-><init>(Ljava/lang/String;IILjava/lang/String;JLjava/lang/String;)V

    sput-object v5, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->SHARE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    .line 14
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    sget-object v1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->LIKE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->COMMENT:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    aput-object v1, v0, v13

    sget-object v1, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->SHARE:Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    aput-object v1, v0, v14

    sput-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->$VALUES:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;JLjava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p3, "resId"    # I
    .param p4, "query"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .param p5, "errorCode"    # J
    .param p7, "telemetryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 29
    invoke-static {p7}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 31
    iput p3, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->resId:I

    .line 32
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->query:Ljava/lang/String;

    .line 33
    iput-wide p5, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->errorCode:J

    .line 34
    iput-object p7, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->telemetryName:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 14
    const-class v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->$VALUES:[Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;

    return-object v0
.end method


# virtual methods
.method public getDisplayName()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 54
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    iget v1, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->resId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getErrorCode()J
    .locals 2

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->errorCode:J

    return-wide v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->query:Ljava/lang/String;

    return-object v0
.end method

.method public getTelemetryName()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/feeditemactions/FeedItemActionType;->telemetryName:Ljava/lang/String;

    return-object v0
.end method
