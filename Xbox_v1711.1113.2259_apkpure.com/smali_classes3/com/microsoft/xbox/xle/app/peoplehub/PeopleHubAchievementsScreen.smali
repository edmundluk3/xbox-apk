.class public Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreen;
.super Lcom/microsoft/xbox/xle/app/activity/ActivityBase;
.source "PeopleHubAchievementsScreen.java"

# interfaces
.implements Lcom/microsoft/xbox/xle/app/activity/HeaderProvider;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    return-void
.end method


# virtual methods
.method protected getActivityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    const-string v0, "PeopleHub Achievements"

    return-object v0
.end method

.method public getHeader()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreen;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0705dc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 18
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/activity/ActivityBase;->onCreate()V

    .line 20
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreen;->onCreateContentView()V

    .line 22
    new-instance v0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 23
    .local v0, "vm":Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreenViewModel;->setAsPivotPane()V

    .line 25
    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreen;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    .line 26
    return-void
.end method

.method public onCreateContentView()V
    .locals 1

    .prologue
    .line 30
    const v0, 0x7f0301c1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/peoplehub/PeopleHubAchievementsScreen;->setContentView(I)V

    .line 31
    return-void
.end method

.method protected shouldTrackPageVisit()Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method
