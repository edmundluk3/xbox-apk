.class public Lcom/microsoft/xbox/xle/app/adapter/GameProfilePivotScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;
.source "GameProfilePivotScreenAdapter.java"


# instance fields
.field private pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

.field private viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;)V
    .locals 1
    .param p1, "vm"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseNormal;-><init>(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V

    .line 15
    const v0, 0x7f0e067b

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/GameProfilePivotScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfilePivotScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    .line 16
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfilePivotScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;

    .line 17
    return-void
.end method


# virtual methods
.method protected updateViewOverride()V
    .locals 2

    .prologue
    .line 21
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfilePivotScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    if-eqz v0, :cond_0

    .line 22
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfilePivotScreenAdapter;->pivotWithTabs:Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/GameProfilePivotScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfilePivotScreenViewModel;->getGameTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/PivotWithTabs;->setHeaderText(Ljava/lang/CharSequence;)V

    .line 24
    :cond_0
    return-void
.end method
