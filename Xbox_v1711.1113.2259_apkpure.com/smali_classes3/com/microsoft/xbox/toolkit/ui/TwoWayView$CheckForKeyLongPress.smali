.class Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForKeyLongPress;
.super Lcom/microsoft/xbox/toolkit/ui/TwoWayView$WindowRunnnable;
.source "TwoWayView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckForKeyLongPress"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V
    .locals 1

    .prologue
    .line 6122
    iput-object p1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForKeyLongPress;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$WindowRunnnable;-><init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView;
    .param p2, "x1"    # Lcom/microsoft/xbox/toolkit/ui/TwoWayView$1;

    .prologue
    .line 6122
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForKeyLongPress;-><init>(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 6124
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForKeyLongPress;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->isPressed()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForKeyLongPress;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$2200(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I

    move-result v3

    if-gez v3, :cond_1

    .line 6149
    :cond_0
    :goto_0
    return-void

    .line 6128
    :cond_1
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForKeyLongPress;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$2200(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I

    move-result v3

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForKeyLongPress;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$3300(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I

    move-result v4

    sub-int v1, v3, v4

    .line 6129
    .local v1, "index":I
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForKeyLongPress;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v3, v1}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 6131
    .local v2, "v":Landroid/view/View;
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForKeyLongPress;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$500(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 6132
    const/4 v0, 0x0

    .line 6134
    .local v0, "handled":Z
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForKeyLongPress;->sameWindow()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 6135
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForKeyLongPress;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    iget-object v4, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForKeyLongPress;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$2200(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)I

    move-result v4

    iget-object v5, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForKeyLongPress;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-static {v5}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$2300(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;)J

    move-result-wide v6

    invoke-static {v3, v2, v4, v6, v7}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->access$4000(Lcom/microsoft/xbox/toolkit/ui/TwoWayView;Landroid/view/View;IJ)Z

    move-result v0

    .line 6138
    :cond_2
    if-eqz v0, :cond_0

    .line 6139
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForKeyLongPress;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v3, v8}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setPressed(Z)V

    .line 6140
    invoke-virtual {v2, v8}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 6143
    .end local v0    # "handled":Z
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/toolkit/ui/TwoWayView$CheckForKeyLongPress;->this$0:Lcom/microsoft/xbox/toolkit/ui/TwoWayView;

    invoke-virtual {v3, v8}, Lcom/microsoft/xbox/toolkit/ui/TwoWayView;->setPressed(Z)V

    .line 6145
    if-eqz v2, :cond_0

    .line 6146
    invoke-virtual {v2, v8}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0
.end method
