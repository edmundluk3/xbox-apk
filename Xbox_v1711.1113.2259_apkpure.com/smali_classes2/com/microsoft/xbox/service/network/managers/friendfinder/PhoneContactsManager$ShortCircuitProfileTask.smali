.class public final enum Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;
.super Ljava/lang/Enum;
.source "PhoneContactsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ShortCircuitProfileTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

.field public static final enum AddProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

.field public static final enum AddProfileViaVoice:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

.field public static final enum DeleteProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

.field public static final enum GetProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

.field public static final enum UpdateProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

.field public static final enum UploadContacts:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 57
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    const-string v1, "GetProfile"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->GetProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    .line 58
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    const-string v1, "AddProfile"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->AddProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    .line 59
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    const-string v1, "AddProfileViaVoice"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->AddProfileViaVoice:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    .line 60
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    const-string v1, "UpdateProfile"

    invoke-direct {v0, v1, v6}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->UpdateProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    .line 61
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    const-string v1, "UploadContacts"

    invoke-direct {v0, v1, v7}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->UploadContacts:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    .line 62
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    const-string v1, "DeleteProfile"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->DeleteProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    .line 56
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->GetProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->AddProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->AddProfileViaVoice:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    aput-object v1, v0, v5

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->UpdateProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    aput-object v1, v0, v6

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->UploadContacts:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->DeleteProfile:Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    aput-object v2, v0, v1

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 56
    const-class v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->$VALUES:[Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactsManager$ShortCircuitProfileTask;

    return-object v0
.end method
