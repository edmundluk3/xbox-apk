.class public final enum Lcom/microsoft/xbox/service/model/chat/ChatManager;
.super Ljava/lang/Enum;
.source "ChatManager.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$Callback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/service/model/chat/ChatManager;",
        ">;",
        "Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$Callback;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/service/model/chat/ChatManager;

.field private static final CHAT_SUB_PROTOCOL:Ljava/lang/String; = "chat"

.field private static final CHAT_WEBSOCKET_ENDPOINT:Ljava/lang/String; = "https://chat.xboxlive.com/chat/connect"

.field private static final CONNECT_TIMEOUT:I = 0xbb8

.field public static final enum INSTANCE:Lcom/microsoft/xbox/service/model/chat/ChatManager;

.field private static final LONG_BYTESIZE:I = 0x8

.field private static final RECONNECT_DELAY_MS:J = 0x1388L

.field private static final RECONNECT_TIMER_MS:J = 0x2710L

.field private static final TAG:Ljava/lang/String;

.field private static final TOKEN_AUDIENCE_URL:Ljava/lang/String; = "https://xboxlive.com"


# instance fields
.field private activeChannelCount:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final clubChatModelCache:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;",
            "Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;",
            ">;"
        }
    .end annotation
.end field

.field private connectStartTimestamp:J

.field private currentDisplayedChannel:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

.field private webSocket:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 36
    new-instance v0, Lcom/microsoft/xbox/service/model/chat/ChatManager;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/service/model/chat/ChatManager;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->INSTANCE:Lcom/microsoft/xbox/service/model/chat/ChatManager;

    .line 35
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/microsoft/xbox/service/model/chat/ChatManager;

    sget-object v1, Lcom/microsoft/xbox/service/model/chat/ChatManager;->INSTANCE:Lcom/microsoft/xbox/service/model/chat/ChatManager;

    aput-object v1, v0, v2

    sput-object v0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->$VALUES:[Lcom/microsoft/xbox/service/model/chat/ChatManager;

    .line 38
    const-class v0, Lcom/microsoft/xbox/service/model/chat/ChatManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 58
    new-instance v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;

    const-string v1, "https://chat.xboxlive.com/chat/connect"

    invoke-direct {v0, v1, p0}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$Callback;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->webSocket:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;

    .line 59
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->clubChatModelCache:Ljava/util/concurrent/ConcurrentMap;

    .line 60
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->activeChannelCount:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 61
    return-void
.end method

.method private connectInternal()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 6

    .prologue
    .line 116
    :try_start_0
    const-string v1, "en-US"

    .line 117
    .local v1, "locale":Ljava/lang/String;
    const-string v3, "https://xboxlive.com"

    invoke-static {v3}, Lcom/microsoft/xbox/service/network/managers/XTokenManager;->getXTokenString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 118
    .local v2, "token":Ljava/lang/String;
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->webSocket:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;

    const-string v4, "chat"

    const/16 v5, 0xbb8

    invoke-virtual {v3, v2, v1, v4, v5}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->connect(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Lcom/microsoft/xbox/toolkit/XLEException; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .end local v1    # "locale":Ljava/lang/String;
    .end local v2    # "token":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 120
    :catch_0
    move-exception v0

    .line 121
    .local v0, "ex":Lcom/microsoft/xbox/toolkit/XLEException;
    sget-object v3, Lcom/microsoft/xbox/service/model/chat/ChatManager;->TAG:Ljava/lang/String;

    const-string v4, "connect: exception thrown on getXTokenString"

    invoke-static {v3, v4, v0}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 122
    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method private handleConnected()V
    .locals 3

    .prologue
    .line 288
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->clubChatModelCache:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    .line 289
    .local v0, "model":Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->isActive()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 290
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->onConnected()V

    goto :goto_0

    .line 293
    .end local v0    # "model":Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;
    :cond_1
    return-void
.end method

.method private handleError()V
    .locals 3

    .prologue
    .line 278
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->activeChannelCount:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 280
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->clubChatModelCache:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    .line 281
    .local v0, "model":Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->isActive()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 282
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->onError()V

    goto :goto_0

    .line 285
    .end local v0    # "model":Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;
    :cond_1
    return-void
.end method

.method private handleMessage(Ljava/nio/ByteBuffer;)V
    .locals 21
    .param p1, "data"    # Ljava/nio/ByteBuffer;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 296
    invoke-static/range {p1 .. p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 298
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v18

    const/16 v19, 0x4c

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_1

    .line 299
    sget-object v18, Lcom/microsoft/xbox/service/model/chat/ChatManager;->TAG:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "handleMessage: ByteBuffer doesn\'t have enough data for ChatHeader, size:  "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    :cond_0
    :goto_0
    return-void

    .line 303
    :cond_1
    invoke-static/range {p1 .. p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->readObject(Ljava/nio/ByteBuffer;)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;

    move-result-object v5

    .line 304
    .local v5, "chatHeader":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->getMessageLength()I

    move-result v18

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_2

    .line 305
    sget-object v18, Lcom/microsoft/xbox/service/model/chat/ChatManager;->TAG:Ljava/lang/String;

    const-string v19, "handleMessage: ByteBuffer size doesn\'t equal to the value from header"

    invoke-static/range {v18 .. v19}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 309
    :cond_2
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->getChatChannelId()Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    move-result-object v4

    .line 310
    .local v4, "channelId":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->clubChatModelCache:Ljava/util/concurrent/ConcurrentMap;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    .line 312
    .local v6, "chatModel":Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->isActive()Z

    move-result v18

    if-nez v18, :cond_4

    .line 313
    :cond_3
    sget-object v18, Lcom/microsoft/xbox/service/model/chat/ChatManager;->TAG:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "handleMessage - no such chatModel or chatModel is not active: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 317
    :cond_4
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->isDeleted()Z

    move-result v18

    if-eqz v18, :cond_5

    .line 318
    invoke-virtual {v6, v5}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->onDeleted(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;)V

    goto :goto_0

    .line 322
    :cond_5
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->getMessageType()Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    move-result-object v15

    .line 323
    .local v15, "messageType":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->getSenderXuid()J

    move-result-wide v16

    .line 324
    .local v16, "messageXuid":J
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getInstance()Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/microsoft/xbox/toolkit/ProjectSpecificDataProvider;->getXuidString()Ljava/lang/String;

    move-result-object v12

    .line 325
    .local v12, "meXuidString":Ljava/lang/String;
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_6

    const-wide/16 v10, 0x0

    .line 327
    .local v10, "meXuid":J
    :goto_1
    sget-object v18, Lcom/microsoft/xbox/service/model/chat/ChatManager$1;->$SwitchMap$com$microsoft$xbox$service$chat$ChatDataTypes$ChatHeader$ChatMessageType:[I

    invoke-virtual {v15}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->ordinal()I

    move-result v19

    aget v18, v18, v19

    packed-switch v18, :pswitch_data_0

    .line 408
    sget-object v18, Lcom/microsoft/xbox/service/model/chat/ChatManager;->TAG:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "handleMessage - receive unrecognized message: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 325
    .end local v10    # "meXuid":J
    :cond_6
    invoke-static {v12}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    goto :goto_1

    .line 330
    .restart local v10    # "meXuid":J
    :pswitch_0
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->onTicketRequest()V

    goto/16 :goto_0

    .line 333
    :pswitch_1
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->onTicketRefresh()V

    goto/16 :goto_0

    .line 338
    :pswitch_2
    cmp-long v18, v16, v10

    if-nez v18, :cond_8

    .line 340
    invoke-virtual {v6}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->hasJoinedChannel()Z

    move-result v18

    if-nez v18, :cond_7

    .line 341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->activeChannelCount:Ljava/util/concurrent/atomic/AtomicInteger;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    .line 344
    :cond_7
    invoke-virtual {v5}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->getMessageId()J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-virtual {v6, v0, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->onJoined(J)V

    goto/16 :goto_0

    .line 350
    :cond_8
    :pswitch_3
    cmp-long v18, v16, v10

    if-eqz v18, :cond_0

    .line 357
    :pswitch_4
    const-string v14, ""

    .line 358
    .local v14, "messageText":Ljava/lang/String;
    sget-object v18, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->BasicText:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    move-object/from16 v0, v18

    if-eq v15, v0, :cond_9

    sget-object v18, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->RichText:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    move-object/from16 v0, v18

    if-ne v15, v0, :cond_a

    .line 359
    :cond_9
    const/16 v18, 0x4c

    sget-object v19, Lcom/microsoft/xbox/toolkit/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    move-object/from16 v0, p1

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatUtil;->getStringFromByteBuffer(Ljava/nio/ByteBuffer;ILjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v14

    .line 362
    :cond_a
    invoke-virtual {v6, v5, v14}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->onMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 366
    .end local v14    # "messageText":Ljava/lang/String;
    :pswitch_5
    const/16 v18, 0x4c

    sget-object v19, Lcom/microsoft/xbox/toolkit/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    move-object/from16 v0, p1

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatUtil;->getStringFromByteBuffer(Ljava/nio/ByteBuffer;ILjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v13

    .line 367
    .local v13, "messageOfTheDayText":Ljava/lang/String;
    invoke-virtual {v6, v5, v13}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->onMessageOfTheDay(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 371
    .end local v13    # "messageOfTheDayText":Ljava/lang/String;
    :pswitch_6
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v18

    const/16 v19, 0x54

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_b

    .line 374
    sget-object v18, Lcom/microsoft/xbox/service/model/chat/ChatManager;->TAG:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "handleMessage: ByteBuffer doesn\'t have enough data for ChatDirectMentionHeader, size:  "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 378
    :cond_b
    const/16 v18, 0x4c

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;->readObject(Ljava/nio/ByteBuffer;I)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;

    move-result-object v8

    .line 379
    .local v8, "directMentionHeader":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v18

    iget v0, v8, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;->numMentions:I

    move/from16 v19, v0

    mul-int/lit8 v19, v19, 0x8

    add-int/lit8 v19, v19, 0x54

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_c

    .line 383
    sget-object v18, Lcom/microsoft/xbox/service/model/chat/ChatManager;->TAG:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "handleMessage: ByteBuffer doesn\'t have enough data for mentioned xuid list, size: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 387
    :cond_c
    iget v0, v8, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;->numMentions:I

    move/from16 v18, v0

    mul-int/lit8 v18, v18, 0x8

    add-int/lit8 v18, v18, 0x54

    sget-object v19, Lcom/microsoft/xbox/toolkit/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    move-object/from16 v0, p1

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatUtil;->getStringFromByteBuffer(Ljava/nio/ByteBuffer;ILjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v9

    .line 392
    .local v9, "directMentionText":Ljava/lang/String;
    invoke-virtual {v6, v5, v9}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->onMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 396
    .end local v8    # "directMentionHeader":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;
    .end local v9    # "directMentionText":Ljava/lang/String;
    :pswitch_7
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v18

    const/16 v19, 0x88

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_d

    .line 399
    sget-object v18, Lcom/microsoft/xbox/service/model/chat/ChatManager;->TAG:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "handleMessage: ByteBuffer doesn\'t have enough data for ChatTicket, size: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 403
    :cond_d
    const/16 v18, 0x4c

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;->readObject(Ljava/nio/ByteBuffer;I)Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;

    move-result-object v7

    .line 404
    .local v7, "chatTicket":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;
    invoke-virtual {v6, v7}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;->onUserInfo(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatTicket;)V

    goto/16 :goto_0

    .line 327
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method static synthetic lambda$onCloseError$0(Lcom/microsoft/xbox/service/model/chat/ChatManager;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/service/model/chat/ChatManager;

    .prologue
    .line 443
    sget-object v0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->TAG:Ljava/lang/String;

    const-string v1, "websocket onCloseError() reconnect starts"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->connectStartTimestamp:J

    .line 446
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->reconnect()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_0

    .line 447
    const-wide/16 v0, 0x2724

    invoke-static {v0, v1}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackChatServiceError(J)V

    .line 448
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->handleError()V

    .line 450
    :cond_0
    return-void
.end method

.method private reconnect()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 93
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 95
    new-instance v0, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;

    const-string v1, "https://chat.xboxlive.com/chat/connect"

    invoke-direct {v0, v1, p0}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;-><init>(Ljava/lang/String;Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$Callback;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->webSocket:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;

    .line 97
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->connectInternal()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method private sendLeaveMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)V
    .locals 4
    .param p1, "channelId"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 156
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 158
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->isConnected()Z

    move-result v2

    if-nez v2, :cond_0

    .line 159
    sget-object v2, Lcom/microsoft/xbox/service/model/chat/ChatManager;->TAG:Ljava/lang/String;

    const-string v3, "sendLeaveMessage - websocket is not connected"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    :goto_0
    return-void

    .line 163
    :cond_0
    const/16 v2, 0x4c

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 165
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    new-instance v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    sget-object v3, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->LeaveChannel:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    invoke-direct {v1, v2, v3, p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;-><init>(ILcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)V

    .line 166
    .local v1, "header":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->writeObject(Ljava/nio/ByteBuffer;)V

    .line 168
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->webSocket:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->sendData(Ljava/nio/ByteBuffer;)V

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/service/model/chat/ChatManager;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 35
    const-class v0, Lcom/microsoft/xbox/service/model/chat/ChatManager;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/chat/ChatManager;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/service/model/chat/ChatManager;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->$VALUES:[Lcom/microsoft/xbox/service/model/chat/ChatManager;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/service/model/chat/ChatManager;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/service/model/chat/ChatManager;

    return-object v0
.end method


# virtual methods
.method public closeWebSocketIfNoActiveChannels()V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->activeChannelCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-gtz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->webSocket:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->closeIfOpen()V

    .line 153
    :cond_0
    return-void
.end method

.method public connect()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 2

    .prologue
    .line 101
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsNotUIThread()V

    .line 103
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 104
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->connectStartTimestamp:J

    .line 106
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->connectInternal()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 109
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    goto :goto_0
.end method

.method public getClubChatModel(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;
    .locals 3
    .param p1, "channelId"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 64
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 66
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->clubChatModelCache:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    .line 68
    .local v0, "model":Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;
    if-nez v0, :cond_0

    .line 70
    new-instance v1, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    invoke-direct {v1, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;-><init>(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)V

    .line 71
    .local v1, "newModel":Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->clubChatModelCache:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v2, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "model":Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;
    check-cast v0, Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;

    .line 72
    .restart local v0    # "model":Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;
    if-nez v0, :cond_0

    .line 73
    move-object v0, v1

    .line 77
    .end local v1    # "newModel":Lcom/microsoft/xbox/service/model/clubs/ClubChatModel;
    :cond_0
    return-object v0
.end method

.method public getCurrentDisplayedChannel()Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->currentDisplayedChannel:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    return-object v0
.end method

.method public isConnected()Z
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->webSocket:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->webSocket:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->getState()Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;->Open:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket$XLEWebSocketState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public leaveJoinedChannel(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)V
    .locals 1
    .param p1, "channelId"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 140
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 142
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->sendLeaveMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)V

    .line 143
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->activeChannelCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    .line 145
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->closeWebSocketIfNoActiveChannels()V

    .line 146
    return-void
.end method

.method public onCloseError(Ljava/lang/Exception;)V
    .locals 5
    .param p1, "ex"    # Ljava/lang/Exception;

    .prologue
    .line 439
    sget-object v0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->TAG:Ljava/lang/String;

    const-string v1, "websocket onCloseError()"

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 442
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/service/model/chat/ChatManager$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/service/model/chat/ChatManager;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x1388

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 452
    return-void
.end method

.method public onClosed()V
    .locals 2

    .prologue
    .line 420
    sget-object v0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->TAG:Ljava/lang/String;

    const-string v1, "websocket onClosed()"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    return-void
.end method

.method public onConnectError(Ljava/lang/Exception;)V
    .locals 6
    .param p1, "ex"    # Ljava/lang/Exception;

    .prologue
    .line 425
    sget-object v2, Lcom/microsoft/xbox/service/model/chat/ChatManager;->TAG:Ljava/lang/String;

    const-string v3, "websocket onConnectError()"

    invoke-static {v2, v3, p1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 427
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 429
    .local v0, "currentTimestamp":J
    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->connectStartTimestamp:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x2710

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 430
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->reconnect()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v2, v3, :cond_1

    .line 432
    :cond_0
    const-wide/16 v2, 0x271a

    invoke-static {v2, v3}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackChatServiceError(J)V

    .line 433
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->handleError()V

    .line 435
    :cond_1
    return-void
.end method

.method public onConnected()V
    .locals 2

    .prologue
    .line 414
    sget-object v0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->TAG:Ljava/lang/String;

    const-string v1, "websocket onConnected()"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->handleConnected()V

    .line 416
    return-void
.end method

.method public onDataReceived(Ljava/lang/String;)V
    .locals 0
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 470
    return-void
.end method

.method public onDataReceived(Ljava/nio/ByteBuffer;)V
    .locals 2
    .param p1, "data"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 461
    if-eqz p1, :cond_0

    .line 462
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->handleMessage(Ljava/nio/ByteBuffer;)V

    .line 466
    :goto_0
    return-void

    .line 464
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->TAG:Ljava/lang/String;

    const-string v1, "websocket onDataReceived() data is null"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onPong(Ljava/lang/String;)V
    .locals 0
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 456
    return-void
.end method

.method public sendDirectMentionMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;Ljava/util/List;)V
    .locals 10
    .param p1, "channelId"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "chatMessage"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;",
            "Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 214
    .local p3, "directMentionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 215
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 216
    invoke-static {p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/util/Collection;)V

    .line 218
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->isConnected()Z

    move-result v7

    if-nez v7, :cond_0

    .line 219
    sget-object v7, Lcom/microsoft/xbox/service/model/chat/ChatManager;->TAG:Ljava/lang/String;

    const-string v8, "sendDirectMentionMessage - websocket is not connected"

    invoke-static {v7, v8}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    :goto_0
    return-void

    .line 223
    :cond_0
    iget-object v7, p2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageText:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 224
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v4

    .line 226
    .local v4, "numDirectMention":I
    iget-object v7, p2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageText:Ljava/lang/String;

    sget-object v8, Lcom/microsoft/xbox/toolkit/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v7, v8}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v3

    .line 227
    .local v3, "messageBytes":[B
    mul-int/lit8 v7, v4, 0x8

    add-int/lit8 v7, v7, 0x54

    array-length v8, v3

    add-int/2addr v7, v8

    invoke-static {v7}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 233
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    const/4 v5, 0x0

    .line 234
    .local v5, "offset":I
    new-instance v2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v7

    invoke-direct {v2, v7, p2, p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;-><init>(ILcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)V

    .line 235
    .local v2, "header":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;
    invoke-virtual {v2, v0, v5}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->writeObject(Ljava/nio/ByteBuffer;I)V

    .line 236
    add-int/lit8 v5, v5, 0x4c

    .line 238
    new-instance v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;

    invoke-direct {v1, v4}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;-><init>(I)V

    .line 239
    .local v1, "directMentionHeader":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;
    invoke-virtual {v1, v0, v5}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;->writeObject(Ljava/nio/ByteBuffer;I)V

    .line 240
    add-int/lit8 v5, v5, 0x8

    .line 242
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 243
    .local v6, "xuid":Ljava/lang/String;
    invoke-static {v6}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v0, v5, v8, v9}, Ljava/nio/ByteBuffer;->putLong(IJ)Ljava/nio/ByteBuffer;

    .line 244
    add-int/lit8 v5, v5, 0x8

    .line 245
    goto :goto_1

    .line 247
    .end local v6    # "xuid":Ljava/lang/String;
    :cond_1
    invoke-static {v3, v0, v5}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatUtil;->putBytesToByteBuffer([BLjava/nio/ByteBuffer;I)V

    .line 249
    iget-object v7, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->webSocket:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;

    invoke-virtual {v7, v0}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->sendData(Ljava/nio/ByteBuffer;)V

    goto :goto_0

    .line 251
    .end local v0    # "buffer":Ljava/nio/ByteBuffer;
    .end local v1    # "directMentionHeader":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatDirectMentionHeader;
    .end local v2    # "header":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;
    .end local v3    # "messageBytes":[B
    .end local v4    # "numDirectMention":I
    .end local v5    # "offset":I
    :cond_2
    const-string v7, "sendDirectMentionMessage called with empty message"

    invoke-static {v7}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public sendIsTypingMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)V
    .locals 4
    .param p1, "channelId"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 172
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 174
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->isConnected()Z

    move-result v2

    if-nez v2, :cond_0

    .line 175
    sget-object v2, Lcom/microsoft/xbox/service/model/chat/ChatManager;->TAG:Ljava/lang/String;

    const-string v3, "sendIsTypingMessage - websocket is not connected"

    invoke-static {v2, v3}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    :goto_0
    return-void

    .line 179
    :cond_0
    const/16 v2, 0x4c

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 181
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    new-instance v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    sget-object v3, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->IsTyping:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    invoke-direct {v1, v2, v3, p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;-><init>(ILcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)V

    .line 182
    .local v1, "header":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->writeObject(Ljava/nio/ByteBuffer;)V

    .line 184
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->webSocket:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->sendData(Ljava/nio/ByteBuffer;)V

    goto :goto_0
.end method

.method public sendJoinMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)V
    .locals 4
    .param p1, "channelId"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 129
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 131
    const/16 v2, 0x4c

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 133
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    new-instance v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    sget-object v3, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->JoinChannel:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    invoke-direct {v1, v2, v3, p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;-><init>(ILcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)V

    .line 134
    .local v1, "header":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->writeObject(Ljava/nio/ByteBuffer;)V

    .line 136
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->webSocket:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;

    invoke-virtual {v2, v0}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->sendData(Ljava/nio/ByteBuffer;)V

    .line 137
    return-void
.end method

.method public sendMessage(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;)V
    .locals 5
    .param p1, "channelId"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "chatMessage"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 188
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 189
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 191
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->isConnected()Z

    move-result v3

    if-nez v3, :cond_0

    .line 192
    sget-object v3, Lcom/microsoft/xbox/service/model/chat/ChatManager;->TAG:Ljava/lang/String;

    const-string v4, "sendMessage - websocket is not connected"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    :goto_0
    return-void

    .line 196
    :cond_0
    iget-object v3, p2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageText:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 197
    iget-object v3, p2, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;->messageText:Ljava/lang/String;

    sget-object v4, Lcom/microsoft/xbox/toolkit/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    .line 198
    .local v2, "messageBytes":[B
    array-length v3, v2

    add-int/lit8 v3, v3, 0x4c

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 200
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    new-instance v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v3

    invoke-direct {v1, v3, p2, p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;-><init>(ILcom/microsoft/xbox/service/chat/ChatDataTypes/ChatMessage;Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)V

    .line 201
    .local v1, "header":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->writeObject(Ljava/nio/ByteBuffer;)V

    .line 202
    const/16 v3, 0x4c

    invoke-static {v2, v0, v3}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatUtil;->putBytesToByteBuffer([BLjava/nio/ByteBuffer;I)V

    .line 204
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->webSocket:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;

    invoke-virtual {v3, v0}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->sendData(Ljava/nio/ByteBuffer;)V

    goto :goto_0

    .line 207
    .end local v0    # "buffer":Ljava/nio/ByteBuffer;
    .end local v1    # "header":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;
    .end local v2    # "messageBytes":[B
    :cond_1
    const-string v3, "sendMessage called with empty message"

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setCurrentDisplayedChannel(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)V
    .locals 0
    .param p1, "currentDisplayedChannel"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->currentDisplayedChannel:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;

    .line 86
    return-void
.end method

.method public setMessageOfTheDay(Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;Ljava/lang/String;)V
    .locals 6
    .param p1, "channelId"    # Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 256
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 258
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/chat/ChatManager;->isConnected()Z

    move-result v4

    if-nez v4, :cond_0

    .line 259
    sget-object v4, Lcom/microsoft/xbox/service/model/chat/ChatManager;->TAG:Ljava/lang/String;

    const-string v5, "setMessageOfTheDay - websocket is not connected"

    invoke-static {v4, v5}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    :goto_0
    return-void

    .line 264
    :cond_0
    sget-object v4, Lcom/microsoft/xbox/toolkit/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p2, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    .line 265
    .local v2, "messageBytes":[B
    array-length v4, v2

    add-int/lit8 v4, v4, 0x4c

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 267
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    sget-object v3, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;->MessageOfTheDay:Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;

    .line 268
    .local v3, "messageType":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;
    new-instance v1, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v4

    invoke-direct {v1, v4, v3, p1}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;-><init>(ILcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader$ChatMessageType;Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatChannelId;)V

    .line 269
    .local v1, "header":Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;
    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatHeader;->writeObject(Ljava/nio/ByteBuffer;)V

    .line 270
    const/16 v4, 0x4c

    invoke-static {v2, v0, v4}, Lcom/microsoft/xbox/service/chat/ChatDataTypes/ChatUtil;->putBytesToByteBuffer([BLjava/nio/ByteBuffer;I)V

    .line 272
    iget-object v4, p0, Lcom/microsoft/xbox/service/model/chat/ChatManager;->webSocket:Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;

    invoke-virtual {v4, v0}, Lcom/microsoft/xbox/toolkit/network/XLEWebSocket;->sendData(Ljava/nio/ByteBuffer;)V

    goto :goto_0
.end method
