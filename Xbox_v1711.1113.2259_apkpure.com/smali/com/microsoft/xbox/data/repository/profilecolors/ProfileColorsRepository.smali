.class public Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;
.super Ljava/lang/Object;
.source "ProfileColorsRepository.java"


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# instance fields
.field private final colorCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;",
            ">;"
        }
    .end annotation
.end field

.field private final colorIdMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private hasAllCached:Z

.field private final profileColorService:Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorService;

.field private final schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorService;Lcom/microsoft/xbox/toolkit/SchedulerProvider;)V
    .locals 1
    .param p1, "profileColorService"    # Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorService;
    .param p2, "schedulerProvider"    # Lcom/microsoft/xbox/toolkit/SchedulerProvider;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;->profileColorService:Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorService;

    .line 35
    iput-object p2, p0, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 36
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;->colorCache:Ljava/util/Map;

    .line 37
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;->colorIdMap:Ljava/util/Map;

    .line 38
    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;->getIdFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$lambda$1(Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;->getColorFromCacheOrDownload(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method private downloadAndCacheColor(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;->profileColorService:Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorService;

    invoke-interface {v0, p1}, Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorService;->getProfileColor(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {p0, p1}, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository$$Lambda$5;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;Ljava/lang/String;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 63
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    .line 62
    return-object v0
.end method

.method private getAllProfileColorsFromCache()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;->colorCache:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method private getAllProfileColorsFromService()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;->profileColorService:Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorService;

    invoke-interface {v0}, Lcom/microsoft/xbox/data/service/profilecolors/ProfileColorService;->getProfileColorList()Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository$$Lambda$1;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 51
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flattenAsFlowable(Lio/reactivex/functions/Function;)Lio/reactivex/Flowable;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lio/reactivex/Flowable;->parallel()Lio/reactivex/parallel/ParallelFlowable;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 53
    invoke-interface {v1}, Lcom/microsoft/xbox/toolkit/SchedulerProvider;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/parallel/ParallelFlowable;->runOn(Lio/reactivex/Scheduler;)Lio/reactivex/parallel/ParallelFlowable;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 54
    invoke-virtual {v0, v1}, Lio/reactivex/parallel/ParallelFlowable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/parallel/ParallelFlowable;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Lio/reactivex/parallel/ParallelFlowable;->sequentialDelayError()Lio/reactivex/Flowable;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lio/reactivex/Flowable;->toList()Lio/reactivex/Single;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository$$Lambda$3;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 57
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository$$Lambda$4;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 58
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    .line 50
    return-object v0
.end method

.method private getColorFromCacheOrDownload(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    iget-object v1, p0, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;->colorCache:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    .line 84
    .local v0, "color":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    if-nez v0, :cond_0

    .line 85
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;->downloadAndCacheColor(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v1

    .line 87
    :goto_0
    return-object v1

    :cond_0
    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v1

    goto :goto_0
.end method

.method private getIdFromUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 99
    const-string v1, "/"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 100
    .local v0, "urlParts":[Ljava/lang/String;
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v1, v0, v1

    const-string v2, "\\."

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    return-object v1
.end method

.method static synthetic lambda$downloadAndCacheColor$3(Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;Ljava/lang/String;Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "color"    # Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;->colorCache:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;->colorIdMap:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    return-void
.end method

.method static synthetic lambda$getAllProfileColorsFromService$0(Lcom/microsoft/xbox/service/network/managers/ProfileColorList;)Ljava/lang/Iterable;
    .locals 1
    .param p0, "profileColorList"    # Lcom/microsoft/xbox/service/network/managers/ProfileColorList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/ProfileColorList;->colors:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic lambda$getAllProfileColorsFromService$1(Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;Lcom/microsoft/xbox/service/network/managers/ProfileColorList$ProfileColorItem;)Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;
    .param p1, "color"    # Lcom/microsoft/xbox/service/network/managers/ProfileColorList$ProfileColorItem;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p1, Lcom/microsoft/xbox/service/network/managers/ProfileColorList$ProfileColorItem;->id:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;->downloadAndCacheColor(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    return-object v0
.end method

.method static synthetic lambda$getAllProfileColorsFromService$2(Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;Lcom/google/common/collect/ImmutableList;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;
    .param p1, "ignore"    # Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;->hasAllCached:Z

    return-void
.end method


# virtual methods
.method public getAllProfileColors()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;->hasAllCached:Z

    if-eqz v0, :cond_0

    .line 42
    invoke-direct {p0}, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;->getAllProfileColorsFromCache()Lio/reactivex/Single;

    move-result-object v0

    .line 44
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;->getAllProfileColorsFromService()Lio/reactivex/Single;

    move-result-object v0

    goto :goto_0
.end method

.method public getIdForColor(Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;)Ljava/lang/String;
    .locals 1
    .param p1, "color"    # Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 105
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 106
    iget-object v0, p0, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;->colorIdMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getProfileColorForUrl(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .param p1, "url"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 76
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 77
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {p0}, Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository$$Lambda$7;->lambdaFactory$(Lcom/microsoft/xbox/data/repository/profilecolors/ProfileColorsRepository;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 78
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 76
    return-object v0
.end method
