.class final enum Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;
.super Ljava/lang/Enum;
.source "RemoteControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/remote/RemoteControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "AutoConnectionState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

.field public static final enum Connected:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

.field public static final enum Connecting:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

.field public static final enum ConnectionFailed:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 45
    new-instance v0, Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    const-string v1, "Connected"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;->Connected:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    new-instance v0, Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    const-string v1, "Connecting"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;->Connecting:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    new-instance v0, Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    const-string v1, "ConnectionFailed"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;->ConnectionFailed:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    .line 44
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    sget-object v1, Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;->Connected:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;->Connecting:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;->ConnectionFailed:Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;->$VALUES:[Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 44
    const-class v0, Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;->$VALUES:[Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/remote/RemoteControl$AutoConnectionState;

    return-object v0
.end method
