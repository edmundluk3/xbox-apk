.class public Lcom/microsoft/xbox/smartglass/MediaState;
.super Ljava/lang/Object;
.source "MediaState.java"


# instance fields
.field public final assetId:Ljava/lang/String;

.field public final aumId:Ljava/lang/String;

.field public final enabledCommands:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/microsoft/xbox/smartglass/MediaControlCommands;",
            ">;"
        }
    .end annotation
.end field

.field public final maxSeekTicks:J

.field public final mediaEnd:J

.field public final mediaStart:J

.field public final metaData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;",
            ">;"
        }
    .end annotation
.end field

.field public final minSeekTicks:J

.field public final playbackRate:F

.field public final playbackStatus:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

.field public final playbackType:Lcom/microsoft/xbox/smartglass/MediaType;

.field public final position:J

.field public final soundLevel:Lcom/microsoft/xbox/smartglass/SoundLevel;

.field public final titleId:I


# direct methods
.method constructor <init>(ILjava/lang/String;Ljava/lang/String;IIIIFJJJJJ[Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;)V
    .locals 3
    .param p1, "titleId"    # I
    .param p2, "assetId"    # Ljava/lang/String;
    .param p3, "aumId"    # Ljava/lang/String;
    .param p4, "mediaType"    # I
    .param p5, "soundLevel"    # I
    .param p6, "enabledCommands"    # I
    .param p7, "playbackStatus"    # I
    .param p8, "playbackRate"    # F
    .param p9, "position"    # J
    .param p11, "mediaStart"    # J
    .param p13, "mediaEnd"    # J
    .param p15, "minSeekTicks"    # J
    .param p17, "maxSeekTicks"    # J
    .param p19, "metaData"    # [Lcom/microsoft/xbox/smartglass/MediaMetaDataEntry;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput p1, p0, Lcom/microsoft/xbox/smartglass/MediaState;->titleId:I

    .line 45
    iput-object p2, p0, Lcom/microsoft/xbox/smartglass/MediaState;->assetId:Ljava/lang/String;

    .line 46
    iput-object p3, p0, Lcom/microsoft/xbox/smartglass/MediaState;->aumId:Ljava/lang/String;

    .line 47
    invoke-static {p4}, Lcom/microsoft/xbox/smartglass/MediaType;->fromInt(I)Lcom/microsoft/xbox/smartglass/MediaType;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/smartglass/MediaState;->playbackType:Lcom/microsoft/xbox/smartglass/MediaType;

    .line 48
    invoke-static {p5}, Lcom/microsoft/xbox/smartglass/SoundLevel;->fromInt(I)Lcom/microsoft/xbox/smartglass/SoundLevel;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/smartglass/MediaState;->soundLevel:Lcom/microsoft/xbox/smartglass/SoundLevel;

    .line 49
    invoke-static {p6}, Lcom/microsoft/xbox/smartglass/MediaControlCommands;->fromInt(I)Ljava/util/EnumSet;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/smartglass/MediaState;->enabledCommands:Ljava/util/EnumSet;

    .line 50
    invoke-static {p7}, Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;->fromInt(I)Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/smartglass/MediaState;->playbackStatus:Lcom/microsoft/xbox/smartglass/MediaPlaybackStatus;

    .line 51
    iput p8, p0, Lcom/microsoft/xbox/smartglass/MediaState;->playbackRate:F

    .line 52
    iput-wide p9, p0, Lcom/microsoft/xbox/smartglass/MediaState;->position:J

    .line 53
    iput-wide p11, p0, Lcom/microsoft/xbox/smartglass/MediaState;->mediaStart:J

    .line 54
    move-wide/from16 v0, p13

    iput-wide v0, p0, Lcom/microsoft/xbox/smartglass/MediaState;->mediaEnd:J

    .line 55
    move-wide/from16 v0, p15

    iput-wide v0, p0, Lcom/microsoft/xbox/smartglass/MediaState;->minSeekTicks:J

    .line 56
    move-wide/from16 v0, p17

    iput-wide v0, p0, Lcom/microsoft/xbox/smartglass/MediaState;->maxSeekTicks:J

    .line 57
    invoke-static/range {p19 .. p19}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/smartglass/MediaState;->metaData:Ljava/util/List;

    .line 58
    return-void
.end method
