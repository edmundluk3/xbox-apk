.class public abstract Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
.super Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;
.source "EDSV2MediaItemDetailModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetFutureShowtimeEDSV2MediaItemListRunner;,
        Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;,
        Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetIncludedContentEDSV2MediaItemListRunner;,
        Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;,
        Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetStoreCollectionItemRunner;,
        Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBigCatItemDetailFromLegacyIdRunner;,
        Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBigCatItemDetailFromTitleIdRunner;,
        Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBigCatItemDetailFromProductIdRunner;,
        Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetEDSV2MediaItemDetailRunner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        "U:",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
        ">",
        "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field public static final XBOX_MUSIC_LAUNCH_PARAM:Ljava/lang/String; = "app:5848085B:MusicHomePage"

.field public static final XBOX_MUSIC_TITLE_STRING:Ljava/lang/String;

.field public static final XBOX_VIDEO_LAUNCH_PARAM:Ljava/lang/String; = "app:5848085b:VideoHomePage"

.field public static final XBOX_VIDEO_TITLE_STRING:Ljava/lang/String;


# instance fields
.field private actors:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;",
            ">;"
        }
    .end annotation
.end field

.field private bundles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TU;>;"
        }
    .end annotation
.end field

.field private bundlesLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

.field protected detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field protected detailRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetEDSV2MediaItemDetailRunner;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel",
            "<TT;TU;>.GetEDSV2MediaItemDetailRunner;"
        }
    .end annotation
.end field

.field private futureShowtimes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;",
            ">;"
        }
    .end annotation
.end field

.field private futureShowtimesLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

.field protected getBundlesRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel",
            "<TT;TU;>.GetBundlesEDSV2MediaItem",
            "ListRunner;"
        }
    .end annotation
.end field

.field protected getFutureShowtimesRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetFutureShowtimeEDSV2MediaItemListRunner;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel",
            "<TT;TU;>.GetFutureShowtimeEDSV2MediaItem",
            "ListRunner;"
        }
    .end annotation
.end field

.field protected getIncludedContentRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetIncludedContentEDSV2MediaItemListRunner;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel",
            "<TT;TU;>.GetIncludedContentEDSV2MediaItem",
            "ListRunner;"
        }
    .end annotation
.end field

.field protected getRelatedRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel",
            "<TT;TU;>.GetRelatedEDSV2MediaItem",
            "ListRunner;"
        }
    .end annotation
.end field

.field private includedContent:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TU;>;"
        }
    .end annotation
.end field

.field private includedContentLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

.field protected isLoadingBundles:Z

.field protected isLoadingFutureShowtimes:Z

.field protected isLoadingIncludedContent:Z

.field protected isLoadingRelated:Z

.field private lastRefreshBundlesTime:Ljava/util/Date;

.field private lastRefreshFutureShowtimesTime:Ljava/util/Date;

.field private lastRefreshIncludedContentTime:Ljava/util/Date;

.field private lastRefreshRelatedTime:Ljava/util/Date;

.field protected platformType:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

.field private related:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TU;>;"
        }
    .end annotation
.end field

.field private relatedLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

.field private storeCollectionItem:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;

.field protected final storeService:Lcom/microsoft/xbox/service/store/IStoreService;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    const-class v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->TAG:Ljava/lang/String;

    .line 52
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0710a1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->XBOX_VIDEO_TITLE_STRING:Ljava/lang/String;

    .line 53
    sget-object v0, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v1, 0x7f0710a0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->XBOX_MUSIC_TITLE_STRING:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 3
    .param p1, "mediaItem"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 87
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;-><init>()V

    .line 63
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getRelatedRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;

    .line 64
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetIncludedContentEDSV2MediaItemListRunner;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetIncludedContentEDSV2MediaItemListRunner;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getIncludedContentRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetIncludedContentEDSV2MediaItemListRunner;

    .line 65
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBundlesRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;

    .line 66
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetFutureShowtimeEDSV2MediaItemListRunner;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetFutureShowtimeEDSV2MediaItemListRunner;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getFutureShowtimesRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetFutureShowtimeEDSV2MediaItemListRunner;

    .line 67
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->isLoadingRelated:Z

    .line 68
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->isLoadingIncludedContent:Z

    .line 69
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->isLoadingBundles:Z

    .line 70
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->isLoadingFutureShowtimes:Z

    .line 71
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetEDSV2MediaItemDetailRunner;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetEDSV2MediaItemDetailRunner;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetEDSV2MediaItemDetailRunner;

    .line 81
    sget-object v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->Xbox:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->platformType:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    .line 88
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 89
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->relatedLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 90
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->includedContentLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 91
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->bundlesLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 92
    new-instance v0, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->futureShowtimesLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    .line 93
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getStoreService()Lcom/microsoft/xbox/service/store/IStoreService;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->storeService:Lcom/microsoft/xbox/service/store/IStoreService;

    .line 94
    return-void
.end method

.method static synthetic access$1000(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->onGetBundlesCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->onGetFutureShowtimesCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getDetailFromBigCatProduct(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->onGetStoreCollectionItemCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->onGetRelatedCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncResult;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->onGetIncludedContentCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    return-void
.end method

.method private getDetailFromBigCatProduct(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 3
    .param p1, "productsList"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 649
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->getProducts()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 651
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->getProducts()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;

    .line 652
    .local v0, "item":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->mediaItemFromBigCat(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v1

    .line 654
    .end local v0    # "item":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getImageForPlatform()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;",
            ">;"
        }
    .end annotation

    .prologue
    .line 370
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->platformType:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getSlideshowsForPlatform(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;)Ljava/util/List;

    move-result-object v0

    .line 372
    .local v0, "screenshots":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;>;"
    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 373
    sget-object v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$1;->$SwitchMap$com$microsoft$xbox$service$store$StoreDataTypes$StoreItemDetailResponse$PlatformType:[I

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->platformType:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 383
    :cond_0
    :goto_0
    return-object v0

    .line 375
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    sget-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->Desktop:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getSlideshowsForPlatform(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;)Ljava/util/List;

    move-result-object v0

    .line 376
    goto :goto_0

    .line 378
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    sget-object v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;->Xbox:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getSlideshowsForPlatform(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 373
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private onGetBundlesCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<TU;>;>;)V"
        }
    .end annotation

    .prologue
    .line 622
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<TU;>;>;"
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->isLoadingBundles:Z

    .line 623
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v2, v3, :cond_2

    .line 624
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 625
    .local v1, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TU;>;"
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->lastRefreshBundlesTime:Ljava/util/Date;

    .line 626
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->bundles:Ljava/util/ArrayList;

    .line 627
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 628
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 629
    .local v0, "bundle":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;, "TU;"
    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->DGame:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->name()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->Xbox360Game:Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV3MediaType;->name()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->MediaItemType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 630
    :cond_1
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->hasValidAvailability()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 631
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->bundles:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 636
    .end local v0    # "bundle":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;, "TU;"
    .end local v1    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TU;>;"
    :cond_2
    return-void
.end method

.method private onGetFutureShowtimesCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 639
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;>;>;"
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->isLoadingFutureShowtimes:Z

    .line 640
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v2, :cond_0

    .line 641
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 642
    .local v0, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;>;"
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->lastRefreshFutureShowtimesTime:Ljava/util/Date;

    .line 643
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->futureShowtimes:Ljava/util/ArrayList;

    .line 645
    .end local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;>;"
    :cond_0
    return-void
.end method

.method private onGetIncludedContentCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<TU;>;>;)V"
        }
    .end annotation

    .prologue
    .line 613
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<TU;>;>;"
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->isLoadingIncludedContent:Z

    .line 614
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v2, :cond_0

    .line 615
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 616
    .local v0, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TU;>;"
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->lastRefreshIncludedContentTime:Ljava/util/Date;

    .line 617
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->includedContent:Ljava/util/ArrayList;

    .line 619
    .end local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TU;>;"
    :cond_0
    return-void
.end method

.method private onGetRelatedCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<TU;>;>;)V"
        }
    .end annotation

    .prologue
    .line 604
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/ArrayList<TU;>;>;"
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->isLoadingRelated:Z

    .line 605
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v1, v2, :cond_0

    .line 606
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 607
    .local v0, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TU;>;"
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->lastRefreshRelatedTime:Ljava/util/Date;

    .line 608
    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->related:Ljava/util/ArrayList;

    .line 610
    .end local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<TU;>;"
    :cond_0
    return-void
.end method

.method private onGetStoreCollectionItemCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 596
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;>;"
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v0, v1, :cond_0

    .line 597
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->storeCollectionItem:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;

    .line 601
    :goto_0
    return-void

    .line 599
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->TAG:Ljava/lang/String;

    const-string v1, "get store collection item failed"

    invoke-static {v0, v1}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateXboxMusicAndVideoProviderName()V
    .locals 6

    .prologue
    .line 577
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getProviders()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 578
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getProviders()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;

    .line 579
    .local v0, "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->getTitleId()J

    move-result-wide v2

    const-wide/32 v4, 0x5848085b

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 580
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 587
    :pswitch_1
    sget-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->XBOX_VIDEO_TITLE_STRING:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setName(Ljava/lang/String;)V

    goto :goto_0

    .line 583
    :pswitch_2
    sget-object v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->XBOX_MUSIC_TITLE_STRING:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;->setName(Ljava/lang/String;)V

    goto :goto_0

    .line 593
    .end local v0    # "provider":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;
    :cond_1
    return-void

    .line 580
    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected abstract createMediaItem(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;",
            ")TT;"
        }
    .end annotation
.end method

.method public getActors()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 154
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->actors:Ljava/util/ArrayList;

    if-nez v2, :cond_2

    .line 155
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 156
    .local v1, "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Contributors:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 157
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    iget-object v2, v2, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Contributors:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;

    .line 158
    .local v0, "con":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;
    iget-object v3, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;->Role:Ljava/lang/String;

    const-string v4, "Actor"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 159
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 164
    .end local v0    # "con":Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;
    :cond_1
    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 165
    iput-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->actors:Ljava/util/ArrayList;

    .line 169
    .end local v1    # "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;>;"
    :cond_2
    iget-object v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->actors:Ljava/util/ArrayList;

    return-object v2
.end method

.method public getAffirmationId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 292
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getAffirmationId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAirings()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;",
            ">;"
        }
    .end annotation

    .prologue
    .line 241
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 242
    .local v0, "combinedAirings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getAirings()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 243
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getAirings()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 245
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->futureShowtimes:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 246
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->futureShowtimes:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 248
    :cond_1
    return-object v0
.end method

.method public getArtistBackgroundImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 204
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getArtistBackgroundImageUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAvailabilityId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 272
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getAvailabilityId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAverageUserRating()F
    .locals 1

    .prologue
    .line 268
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getAverageUserRating()F

    move-result v0

    return v0
.end method

.method public getBackgroundImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 200
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBigCatProductId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBigCatProductId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBigCatProductType()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;
    .locals 1

    .prologue
    .line 134
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBigCatProductType()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$BigCatProductType;

    move-result-object v0

    return-object v0
.end method

.method public getBundlePrimaryItemId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->BundlePrimaryItemId:Ljava/lang/String;

    return-object v0
.end method

.method public getBundlePrimaryItemTitleId()J
    .locals 2

    .prologue
    .line 686
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBundlePrimaryItemTitleId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getBundles()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<TU;>;"
        }
    .end annotation

    .prologue
    .line 331
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->bundles:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCanonicalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCastCrew()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Contributor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    iget-object v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->Contributors:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCurrencyCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 288
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCurrencyCode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getDescription()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDurationInMinutes()I
    .locals 1

    .prologue
    .line 217
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getDuration()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getDuration()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XBLSharedUtil;->HHMMSSStringToMinutes(Ljava/lang/String;)I

    move-result v0

    .line 220
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGenres()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Genre;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getGeneres()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getImageUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getImpressionGuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 264
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getImpressionGuid()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIncludedContent()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<TU;>;"
        }
    .end annotation

    .prologue
    .line 327
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->includedContent:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getIsBundle()Z
    .locals 1

    .prologue
    .line 138
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->IsBundle:Z

    return v0
.end method

.method public getIsLoadingBundles()Z
    .locals 1

    .prologue
    .line 532
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getIsLoading()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->isLoadingBundles:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIsLoadingIncludedContent()Z
    .locals 1

    .prologue
    .line 528
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getIsLoading()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->isLoadingIncludedContent:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIsLoadingRelated()Z
    .locals 1

    .prologue
    .line 524
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getIsLoading()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->isLoadingRelated:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIsPartOfAnyBundle()Z
    .locals 1

    .prologue
    .line 142
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->IsPartOfAnyBundle:Z

    return v0
.end method

.method public abstract getLaunchType()Lcom/microsoft/xbox/service/model/LaunchType;
.end method

.method public getLegacyId()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 107
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getLegacyId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getListFromBigCatProduct(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;)Ljava/util/ArrayList;
    .locals 2
    .param p1, "productsList"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;",
            ")",
            "Ljava/util/ArrayList",
            "<TU;>;"
        }
    .end annotation

    .prologue
    .line 659
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 660
    .local v0, "productsLists":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;>;"
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 662
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getListFromBigCatProductLists(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v1

    return-object v1
.end method

.method protected getListFromBigCatProductLists(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 7
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;",
            ">;)",
            "Ljava/util/ArrayList",
            "<TU;>;"
        }
    .end annotation

    .prologue
    .line 667
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    .local p1, "productsLists":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;>;"
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 668
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 669
    .local v1, "products":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;TU;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;

    .line 670
    .local v2, "productsList":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->getProducts()Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 671
    invoke-virtual {v2}, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;->getProducts()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;

    .line 672
    .local v0, "item":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    iget-object v5, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->productId:Ljava/lang/String;

    invoke-interface {v1, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 673
    iget-object v5, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;->productId:Ljava/lang/String;

    invoke-static {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2ModelFactory;->mediaItemFromBigCat(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;)Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    move-result-object v6

    invoke-interface {v1, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 678
    .end local v0    # "item":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$StoreItemDetail;
    .end local v2    # "productsList":Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreProductsResponse$StoreProductsList;
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v3
.end method

.method public getListPrice()Ljava/math/BigDecimal;
    .locals 1

    .prologue
    .line 280
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getListPrice()Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public abstract getMediaGroup()I
.end method

.method public getMediaItemDetailData()Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 111
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    return-object v0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 179
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMediaType()I

    move-result v0

    return v0
.end method

.method public getMsrp()Ljava/math/BigDecimal;
    .locals 1

    .prologue
    .line 284
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getMsrp()Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public getParentCanonicalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 252
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getParentCanonicalId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParentMediaType()I
    .locals 1

    .prologue
    .line 260
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getParentMediaType()I

    move-result v0

    return v0
.end method

.method public getParentName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 256
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getParentName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParentalRating()Ljava/lang/String;
    .locals 1

    .prologue
    .line 229
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getParentalRating()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParentalRatings()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2ParentalRating;",
            ">;"
        }
    .end annotation

    .prologue
    .line 225
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getParentalRatings()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPartnerMediaId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getPartnerMediaId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPlatformType()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 356
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->platformType:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    return-object v0
.end method

.method public getPosterImageUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 208
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getPosterImageUrl()Ljava/lang/String;

    move-result-object v0

    .line 209
    .local v0, "posterImage":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 213
    .end local v0    # "posterImage":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "posterImage":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getProductId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getProductId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProviders()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Provider;",
            ">;"
        }
    .end annotation

    .prologue
    .line 233
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getProviders()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getRelated()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<TU;>;"
        }
    .end annotation

    .prologue
    .line 323
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->related:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected getRelatedCanonicalId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 315
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getRelatedMediaType()I
    .locals 1

    .prologue
    .line 319
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaType()I

    move-result v0

    return v0
.end method

.method public getReleaseDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 191
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getReleaseDate()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getScreenshots()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Image;",
            ">;"
        }
    .end annotation

    .prologue
    .line 361
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->isXPA()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getImageForPlatform()Ljava/util/List;

    move-result-object v0

    .line 364
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getSlideShow()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public getShouldCheckActivity()Z
    .locals 1

    .prologue
    .line 303
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaGroup()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 310
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 308
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 303
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getSkuId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 276
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getSkuId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStoreCollectionItem()Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 388
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->storeCollectionItem:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;

    return-object v0
.end method

.method public getSubscriptionType()Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;
    .locals 1

    .prologue
    .line 296
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getSubscriptionType()Lcom/microsoft/xbox/service/store/StoreDataTypes/SubscriptionType;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitleId()J
    .locals 2

    .prologue
    .line 125
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getTitleId()J

    move-result-wide v0

    return-wide v0
.end method

.method public abstract getTitleType()Lcom/microsoft/xbox/service/model/JTitleType;
.end method

.method public hasValidData()Z
    .locals 1

    .prologue
    .line 116
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 117
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getCanonicalId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getBigCatProductId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 116
    :goto_0
    return v0

    .line 117
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGameType()Z
    .locals 1

    .prologue
    .line 393
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 404
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 402
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 393
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_0
        0x13 -> :sswitch_0
        0x15 -> :sswitch_0
        0x17 -> :sswitch_0
        0x25 -> :sswitch_0
        0x2329 -> :sswitch_0
        0x232a -> :sswitch_0
    .end sparse-switch
.end method

.method public isXPA()Z
    .locals 1

    .prologue
    .line 346
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    iget-boolean v0, v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->isXPA:Z

    return v0
.end method

.method public load(Z)V
    .locals 0
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 415
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    return-void
.end method

.method public loadBigCatItemDetailFromLegacyId(ZLjava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 1
    .param p1, "forceRefresh"    # Z
    .param p2, "legacyId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 450
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    invoke-static {p2}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 451
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBigCatItemDetailFromLegacyIdRunner;

    invoke-direct {v0, p0, p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBigCatItemDetailFromLegacyIdRunner;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;Ljava/lang/String;)V

    invoke-super {p0, p1, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public loadBigCatItemDetailFromProductId(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 5
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    const/4 v4, 0x0

    .line 438
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 439
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBigCatItemDetailFromProductIdRunner;

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBigCatItemDetailFromProductIdRunner;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;Ljava/lang/String;)V

    invoke-super {p0, p1, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    .line 441
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/toolkit/XLEException;

    const-wide/16 v2, 0xfae

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    invoke-direct {v0, v4, v4, v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    goto :goto_0
.end method

.method public loadBigCatItemDetailFromTitleId(ZJ)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 2
    .param p1, "forceRefresh"    # Z
    .param p2, "titleId"    # J
        .annotation build Landroid/support/annotation/IntRange;
            from = 0x0L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZJ)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 445
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    const-wide/16 v0, 0x0

    invoke-static {v0, v1, p2, p3}, Lcom/microsoft/xbox/toolkit/Preconditions;->intRangeFrom(JJ)V

    .line 446
    new-instance v0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBigCatItemDetailFromTitleIdRunner;

    invoke-direct {v0, p0, p2, p3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBigCatItemDetailFromTitleIdRunner;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;J)V

    invoke-super {p0, p1, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public loadBundles(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<TU;>;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    const/4 v6, 0x0

    .line 473
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 476
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 478
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 481
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v0, v1, :cond_2

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v0, v1, :cond_2

    .line 482
    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->lifetime:J

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->lastRefreshBundlesTime:Ljava/util/Date;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->bundlesLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBundlesRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;

    move v1, p1

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    .line 484
    :goto_0
    return-object v1

    :cond_2
    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v2, Lcom/microsoft/xbox/toolkit/XLEException;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBundlesRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetBundlesEDSV2MediaItemListRunner;->getDefaultErrorCode()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    invoke-direct {v1, v6, v6, v2, v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    goto :goto_0
.end method

.method public loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 1
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 434
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetEDSV2MediaItemDetailRunner;

    invoke-super {p0, p1, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;->loadData(ZLcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method public loadFutureShowtimes(Z[Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "forceRefresh"    # Z
    .param p2, "headendIdList"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z[",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/model/edsv2/EDSV2Airing;",
            ">;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    const/4 v6, 0x0

    .line 503
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 504
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 506
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 509
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v0, v1, :cond_2

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v0, v1, :cond_2

    .line 510
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getFutureShowtimesRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetFutureShowtimeEDSV2MediaItemListRunner;

    invoke-virtual {v1, p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetFutureShowtimeEDSV2MediaItemListRunner;->setHeadendList([Ljava/lang/String;)V

    .line 511
    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->lifetime:J

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->lastRefreshFutureShowtimesTime:Ljava/util/Date;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->futureShowtimesLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getFutureShowtimesRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetFutureShowtimeEDSV2MediaItemListRunner;

    move v1, p1

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    .line 513
    :goto_0
    return-object v1

    :cond_2
    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v2, Lcom/microsoft/xbox/toolkit/XLEException;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getFutureShowtimesRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetFutureShowtimeEDSV2MediaItemListRunner;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetFutureShowtimeEDSV2MediaItemListRunner;->getDefaultErrorCode()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    invoke-direct {v1, v6, v6, v2, v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    goto :goto_0
.end method

.method public loadIncludedContent(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<TU;>;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    const/4 v6, 0x0

    .line 489
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 490
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getCanonicalId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 492
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 495
    :cond_1
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v0, v1, :cond_2

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v0, v1, :cond_2

    .line 496
    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->lifetime:J

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->lastRefreshIncludedContentTime:Ljava/util/Date;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->includedContentLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getIncludedContentRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetIncludedContentEDSV2MediaItemListRunner;

    move v1, p1

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    .line 498
    :goto_0
    return-object v1

    :cond_2
    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v2, Lcom/microsoft/xbox/toolkit/XLEException;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getIncludedContentRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetIncludedContentEDSV2MediaItemListRunner;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetIncludedContentEDSV2MediaItemListRunner;->getDefaultErrorCode()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    invoke-direct {v1, v6, v6, v2, v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    goto :goto_0
.end method

.method public loadRelated(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "forceRefresh"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/util/ArrayList",
            "<TU;>;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    const/4 v6, 0x0

    .line 460
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .line 461
    .local v0, "status":Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getBigCatProductId()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 462
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->loadDetail(Z)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    .line 465
    :cond_0
    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_OP_FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-eq v0, v1, :cond_1

    .line 466
    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->lifetime:J

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->lastRefreshRelatedTime:Ljava/util/Date;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->relatedLoadingStatus:Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getRelatedRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;

    move v1, p1

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v1

    .line 468
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v2, Lcom/microsoft/xbox/toolkit/XLEException;

    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getRelatedRunner:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetRelatedEDSV2MediaItemListRunner;->getDefaultErrorCode()J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lcom/microsoft/xbox/toolkit/XLEException;-><init>(J)V

    invoke-direct {v1, v6, v6, v2, v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    goto :goto_0
.end method

.method public loadStoreCollectionItem(Ljava/lang/String;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 7
    .param p1, "id"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation

        .annotation build Landroid/support/annotation/Size;
            min = 0x1L
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreCollectionResponse$StoreCollectionItem;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    const/4 v4, 0x0

    .line 455
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonEmpty(Ljava/lang/CharSequence;)V

    .line 456
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->lifetime:J

    new-instance v5, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;

    invoke-direct {v5}, Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;-><init>()V

    new-instance v6, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetStoreCollectionItemRunner;

    invoke-direct {v6, p0, p1, v4}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$GetStoreCollectionItemRunner;-><init>(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;Ljava/lang/String;Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel$1;)V

    invoke-static/range {v1 .. v6}, Lcom/microsoft/xbox/toolkit/DataLoadUtil;->Load(ZJLjava/util/Date;Lcom/microsoft/xbox/toolkit/SingleEntryLoadingStatus;Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method protected onGetMediaItemDetailCompleted(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 551
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<TT;>;"
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->updateDataForBrowser(Lcom/microsoft/xbox/toolkit/AsyncResult;)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object p1

    .line 553
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->isLoading:Z

    .line 554
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->SUCCESS:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    if-ne v3, v4, :cond_0

    .line 555
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    iput-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->lastRefreshTime:Ljava/util/Date;

    .line 556
    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 558
    .local v1, "data":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;, "TT;"
    if-eqz v1, :cond_0

    .line 559
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 560
    .local v0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v0, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 563
    iget-object v3, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->getPartnerMediaId()Ljava/lang/String;

    move-result-object v2

    .line 565
    .local v2, "partnerMediaId":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->updateWithNewData(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V

    .line 566
    invoke-static {p0, v2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->updateModelInCache(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemModel;Ljava/lang/String;)V

    .line 568
    invoke-direct {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->updateXboxMusicAndVideoProviderName()V

    .line 574
    .end local v0    # "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v1    # "data":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;, "TT;"
    .end local v2    # "partnerMediaId":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 570
    .restart local v0    # "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .restart local v1    # "data":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;, "TT;"
    :cond_1
    sget-object v3, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->TAG:Ljava/lang/String;

    const-string v4, "Service returned media item detail data that does not match current media item detail model"

    invoke-static {v3, v4}, Lcom/microsoft/xbox/toolkit/XLELog;->Error(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setBundlePrimaryItemTitleId(J)V
    .locals 1
    .param p1, "bundlePrimaryItemTitleId"    # J

    .prologue
    .line 682
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;->setBundlePrimaryItemTitleId(J)V

    .line 683
    return-void
.end method

.method public setPlatformType(Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;)V
    .locals 4
    .param p1, "platformType"    # Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    .prologue
    .line 350
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->platformType:Lcom/microsoft/xbox/service/store/StoreDataTypes/StoreItemDetailResponse$PlatformType;

    .line 351
    new-instance v0, Lcom/microsoft/xbox/toolkit/AsyncResult;

    new-instance v1, Lcom/microsoft/xbox/service/model/UpdateData;

    sget-object v2, Lcom/microsoft/xbox/service/model/UpdateType;->MediaItemDetail:Lcom/microsoft/xbox/service/model/UpdateType;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/microsoft/xbox/service/model/UpdateData;-><init>(Lcom/microsoft/xbox/service/model/UpdateType;Z)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, p0, v2}, Lcom/microsoft/xbox/toolkit/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/microsoft/xbox/toolkit/XLEException;)V

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->notifyObservers(Lcom/microsoft/xbox/toolkit/AsyncResult;)V

    .line 352
    return-void
.end method

.method public shouldGetProviderActivities()Z
    .locals 1

    .prologue
    .line 336
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public shouldRefreshBundles()Z
    .locals 4

    .prologue
    .line 426
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->lastRefreshBundlesTime:Ljava/util/Date;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->lifetime:J

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->shouldRefresh(Ljava/util/Date;J)Z

    move-result v0

    return v0
.end method

.method public shouldRefreshFutureShowtimes()Z
    .locals 4

    .prologue
    .line 430
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->lastRefreshFutureShowtimesTime:Ljava/util/Date;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->lifetime:J

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->shouldRefresh(Ljava/util/Date;J)Z

    move-result v0

    return v0
.end method

.method public shouldRefreshIncludedContent()Z
    .locals 4

    .prologue
    .line 422
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->lastRefreshIncludedContentTime:Ljava/util/Date;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->lifetime:J

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->shouldRefresh(Ljava/util/Date;J)Z

    move-result v0

    return v0
.end method

.method public shouldRefreshRelated()Z
    .locals 4

    .prologue
    .line 418
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->lastRefreshRelatedTime:Ljava/util/Date;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->lifetime:J

    invoke-static {v0, v2, v3}, Lcom/microsoft/xbox/toolkit/JavaUtil;->shouldRefresh(Ljava/util/Date;J)Z

    move-result v0

    return v0
.end method

.method public supportsRelated()Z
    .locals 2

    .prologue
    .line 519
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaType()I

    move-result v0

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaType()I

    move-result v0

    const/16 v1, 0x3ec

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaType()I

    move-result v0

    const/16 v1, 0x3ed

    if-eq v0, v1, :cond_0

    .line 520
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->getMediaType()I

    move-result v0

    const/16 v1, 0x3eb

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->isGameType()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 519
    :goto_0
    return v0

    .line 520
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected updateDataForBrowser(Lcom/microsoft/xbox/toolkit/AsyncResult;)Lcom/microsoft/xbox/toolkit/AsyncResult;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<TT;>;)",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 546
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<TT;>;"
    return-object p1
.end method

.method protected updateWithNewData(Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 537
    .local p0, "this":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;, "Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel<TT;TU;>;"
    .local p1, "data":Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;, "TT;"
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->lastRefreshTime:Ljava/util/Date;

    .line 538
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItemDetailModel;->detailData:Lcom/microsoft/xbox/service/model/edsv2/EDSV2MediaItem;

    .line 539
    return-void
.end method
