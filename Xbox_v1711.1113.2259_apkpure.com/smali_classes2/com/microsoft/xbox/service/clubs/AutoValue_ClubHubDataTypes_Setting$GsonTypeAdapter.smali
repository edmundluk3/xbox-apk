.class public final Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "AutoValue_ClubHubDataTypes_Setting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GsonTypeAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/gson/TypeAdapter",
        "<",
        "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field private final allowedValuesAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private final canViewerActAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final canViewerChangeSettingAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private defaultAllowedValues:Lcom/google/common/collect/ImmutableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/collect/ImmutableList",
            "<TT;>;"
        }
    .end annotation
.end field

.field private defaultCanViewerAct:Z

.field private defaultCanViewerChangeSetting:Z

.field private defaultValue:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final valueAdapter:Lcom/google/gson/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/TypeAdapter",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;Lcom/google/gson/reflect/TypeToken;)V
    .locals 6
    .param p1, "gson"    # Lcom/google/gson/Gson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            "Lcom/google/gson/reflect/TypeToken",
            "<+",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;, "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter<TT;>;"
    .local p2, "typeToken":Lcom/google/gson/reflect/TypeToken;, "Lcom/google/gson/reflect/TypeToken<+Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<TT;>;>;"
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 32
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    .line 28
    iput-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->defaultValue:Ljava/lang/Object;

    .line 29
    iput-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->defaultAllowedValues:Lcom/google/common/collect/ImmutableList;

    .line 30
    iput-boolean v5, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->defaultCanViewerAct:Z

    .line 31
    iput-boolean v5, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->defaultCanViewerChangeSetting:Z

    .line 33
    invoke-virtual {p2}, Lcom/google/gson/reflect/TypeToken;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    .line 34
    .local v0, "type":Ljava/lang/reflect/ParameterizedType;
    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v1

    .line 35
    .local v1, "typeArgs":[Ljava/lang/reflect/Type;
    aget-object v2, v1, v5

    invoke-static {v2}, Lcom/google/gson/reflect/TypeToken;->get(Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->valueAdapter:Lcom/google/gson/TypeAdapter;

    .line 36
    const-class v2, Lcom/google/common/collect/ImmutableList;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/reflect/Type;

    aget-object v4, v1, v5

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lcom/google/gson/reflect/TypeToken;->getParameterized(Ljava/lang/reflect/Type;[Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/gson/Gson;->getAdapter(Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->allowedValuesAdapter:Lcom/google/gson/TypeAdapter;

    .line 37
    const-class v2, Ljava/lang/Boolean;

    invoke-virtual {p1, v2}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->canViewerActAdapter:Lcom/google/gson/TypeAdapter;

    .line 38
    const-class v2, Ljava/lang/Boolean;

    invoke-virtual {p1, v2}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    move-result-object v2

    iput-object v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->canViewerChangeSettingAdapter:Lcom/google/gson/TypeAdapter;

    .line 39
    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;
    .locals 7
    .param p1, "jsonReader"    # Lcom/google/gson/stream/JsonReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/stream/JsonReader;",
            ")",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;, "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter<TT;>;"
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v5

    sget-object v6, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v5, v6, :cond_0

    .line 77
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    .line 78
    const/4 v5, 0x0

    .line 114
    :goto_0
    return-object v5

    .line 80
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->beginObject()V

    .line 81
    iget-object v4, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->defaultValue:Ljava/lang/Object;

    .line 82
    .local v4, "value":Ljava/lang/Object;, "TT;"
    iget-object v1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->defaultAllowedValues:Lcom/google/common/collect/ImmutableList;

    .line 83
    .local v1, "allowedValues":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<TT;>;"
    iget-boolean v2, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->defaultCanViewerAct:Z

    .line 84
    .local v2, "canViewerAct":Z
    iget-boolean v3, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->defaultCanViewerChangeSetting:Z

    .line 85
    .local v3, "canViewerChangeSetting":Z
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 86
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v0

    .line 87
    .local v0, "_name":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v5

    sget-object v6, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v5, v6, :cond_1

    .line 88
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextNull()V

    goto :goto_1

    .line 91
    :cond_1
    const/4 v5, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_2
    :goto_2
    packed-switch v5, :pswitch_data_0

    .line 109
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->skipValue()V

    goto :goto_1

    .line 91
    :sswitch_0
    const-string v6, "value"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x0

    goto :goto_2

    :sswitch_1
    const-string v6, "allowedValues"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x1

    goto :goto_2

    :sswitch_2
    const-string v6, "canViewerAct"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x2

    goto :goto_2

    :sswitch_3
    const-string v6, "canViewerChangeSetting"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v5, 0x3

    goto :goto_2

    .line 93
    :pswitch_0
    iget-object v5, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->valueAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v4

    .line 94
    goto :goto_1

    .line 97
    :pswitch_1
    iget-object v5, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->allowedValuesAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "allowedValues":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<TT;>;"
    check-cast v1, Lcom/google/common/collect/ImmutableList;

    .line 98
    .restart local v1    # "allowedValues":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<TT;>;"
    goto :goto_1

    .line 101
    :pswitch_2
    iget-object v5, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->canViewerActAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 102
    goto :goto_1

    .line 105
    :pswitch_3
    iget-object v5, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->canViewerChangeSettingAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {v5, p1}, Lcom/google/gson/TypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 106
    goto :goto_1

    .line 113
    .end local v0    # "_name":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->endObject()V

    .line 114
    new-instance v5, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting;

    invoke-direct {v5, v4, v1, v2, v3}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting;-><init>(Ljava/lang/Object;Lcom/google/common/collect/ImmutableList;ZZ)V

    goto/16 :goto_0

    .line 91
    nop

    :sswitch_data_0
    .sparse-switch
        -0x7b355316 -> :sswitch_1
        0x6ac9171 -> :sswitch_0
        0x2c138d9e -> :sswitch_3
        0x75ec2730 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;, "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter<TT;>;"
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultAllowedValues(Lcom/google/common/collect/ImmutableList;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/collect/ImmutableList",
            "<TT;>;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;, "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter<TT;>;"
    .local p1, "defaultAllowedValues":Lcom/google/common/collect/ImmutableList;, "Lcom/google/common/collect/ImmutableList<TT;>;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->defaultAllowedValues:Lcom/google/common/collect/ImmutableList;

    .line 46
    return-object p0
.end method

.method public setDefaultCanViewerAct(Z)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultCanViewerAct"    # Z

    .prologue
    .line 49
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;, "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter<TT;>;"
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->defaultCanViewerAct:Z

    .line 50
    return-object p0
.end method

.method public setDefaultCanViewerChangeSetting(Z)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;
    .locals 0
    .param p1, "defaultCanViewerChangeSetting"    # Z

    .prologue
    .line 53
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;, "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter<TT;>;"
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->defaultCanViewerChangeSetting:Z

    .line 54
    return-object p0
.end method

.method public setDefaultValue(Ljava/lang/Object;)Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;"
        }
    .end annotation

    .prologue
    .line 41
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;, "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter<TT;>;"
    .local p1, "defaultValue":Ljava/lang/Object;, "TT;"
    iput-object p1, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->defaultValue:Ljava/lang/Object;

    .line 42
    return-object p0
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)V
    .locals 2
    .param p1, "jsonWriter"    # Lcom/google/gson/stream/JsonWriter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/stream/JsonWriter;",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting",
            "<TT;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;, "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter<TT;>;"
    .local p2, "object":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;, "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting<TT;>;"
    if-nez p2, :cond_0

    .line 60
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->nullValue()Lcom/google/gson/stream/JsonWriter;

    .line 73
    :goto_0
    return-void

    .line 63
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->beginObject()Lcom/google/gson/stream/JsonWriter;

    .line 64
    const-string v0, "value"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->valueAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->value()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 66
    const-string v0, "allowedValues"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->allowedValuesAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->allowedValues()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 68
    const-string v0, "canViewerAct"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->canViewerActAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->canViewerAct()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 70
    const-string v0, "canViewerChangeSetting"

    invoke-virtual {p1, v0}, Lcom/google/gson/stream/JsonWriter;->name(Ljava/lang/String;)Lcom/google/gson/stream/JsonWriter;

    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->canViewerChangeSettingAdapter:Lcom/google/gson/TypeAdapter;

    invoke-virtual {p2}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;->canViewerChangeSetting()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/TypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    .line 72
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonWriter;->endObject()Lcom/google/gson/stream/JsonWriter;

    goto :goto_0
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23
    .local p0, "this":Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;, "Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter<TT;>;"
    check-cast p2, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/xbox/service/clubs/AutoValue_ClubHubDataTypes_Setting$GsonTypeAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Setting;)V

    return-void
.end method
