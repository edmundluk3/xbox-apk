.class final synthetic Lcom/microsoft/xbox/service/model/clubs/ClubModel$$Lambda$4;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

.field private final arg$2:Ljava/util/List;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$$Lambda$4;->arg$1:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    iput-object p2, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$$Lambda$4;->arg$2:Ljava/util/List;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Ljava/util/List;)Lio/reactivex/functions/Consumer;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$$Lambda$4;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel$$Lambda$4;-><init>(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Ljava/util/List;)V

    return-object v0
.end method


# virtual methods
.method public accept(Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$$Lambda$4;->arg$1:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/clubs/ClubModel$$Lambda$4;->arg$2:Ljava/util/List;

    check-cast p1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->lambda$rxLoad$2(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Ljava/util/List;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;)V

    return-void
.end method
