.class public Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
.super Ljava/lang/Object;
.source "FacebookManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$GetPeopleHubRecommendationsAsyncTask;,
        Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$GetPrivacyValueAsyncTask;,
        Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;,
        Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;,
        Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static facebookManagerReady:Lcom/microsoft/xbox/toolkit/Ready;

.field private static instance:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;


# instance fields
.field private final SHARE_TO_FACEBOOK_LINK:Ljava/lang/String;

.field private callbackManager:Lcom/facebook/CallbackManager;

.field private currentAction:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

.field private detailErrorCode:J

.field private dialog:Landroid/app/Dialog;

.field private facebookPermission:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private firstLoginWithReadOnly:Z

.field private friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

.field private getPrivacyValueTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$GetPrivacyValueAsyncTask;

.field private getRecommendationsTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$GetPeopleHubRecommendationsAsyncTask;

.field private loginBehavior:Lcom/facebook/login/LoginBehavior;

.field private loginResult:Lcom/facebook/FacebookCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/FacebookCallback",
            "<",
            "Lcom/facebook/login/LoginResult;",
            ">;"
        }
    .end annotation
.end field

.field private meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

.field private optInAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;

.field private optOutAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;

.field private privacyValue:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

.field private recommendations:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

.field private shareDialog:Lcom/facebook/share/widget/ShareDialog;

.field private shareResult:Lcom/facebook/FacebookCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/FacebookCallback",
            "<",
            "Lcom/facebook/share/Sharer$Result;",
            ">;"
        }
    .end annotation
.end field

.field private shouldNavigateToSuggesionList:Z

.field private token:Lcom/facebook/AccessToken;

.field private tokenString:Ljava/lang/String;

.field private userAuthRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    const-class v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->TAG:Ljava/lang/String;

    .line 76
    new-instance v0, Lcom/microsoft/xbox/toolkit/Ready;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/Ready;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->facebookManagerReady:Lcom/microsoft/xbox/toolkit/Ready;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    const-string v1, "https://go.microsoft.com/fwlink/?LinkId=698852"

    iput-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->SHARE_TO_FACEBOOK_LINK:Ljava/lang/String;

    .line 90
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->firstLoginWithReadOnly:Z

    .line 93
    iput-boolean v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->shouldNavigateToSuggesionList:Z

    .line 96
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->detailErrorCode:J

    .line 100
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$1;-><init>(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)V

    iput-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->shareResult:Lcom/facebook/FacebookCallback;

    .line 130
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$2;-><init>(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)V

    iput-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->loginResult:Lcom/facebook/FacebookCallback;

    .line 176
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->userAuthRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 177
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->facebookManagerReady:Lcom/microsoft/xbox/toolkit/Ready;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/Ready;->reset()V

    .line 178
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/xblshared/ProtectedRunnable;

    new-instance v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$3;-><init>(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)V

    invoke-direct {v0, v1}, Lcom/microsoft/xbox/service/network/managers/xblshared/ProtectedRunnable;-><init>(Ljava/lang/Runnable;)V

    .line 191
    .local v0, "runnable":Ljava/lang/Runnable;
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 196
    const-string v1, "retail"

    const-string v2, "retail"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 197
    sget-object v1, Lcom/facebook/login/LoginBehavior;->NATIVE_WITH_FALLBACK:Lcom/facebook/login/LoginBehavior;

    iput-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->loginBehavior:Lcom/facebook/login/LoginBehavior;

    .line 202
    :goto_0
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->facebookManagerReady:Lcom/microsoft/xbox/toolkit/Ready;

    invoke-virtual {v1}, Lcom/microsoft/xbox/toolkit/Ready;->setReady()V

    .line 203
    return-void

    .line 199
    :cond_0
    sget-object v1, Lcom/facebook/login/LoginBehavior;->WEB_ONLY:Lcom/facebook/login/LoginBehavior;

    iput-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->loginBehavior:Lcom/facebook/login/LoginBehavior;

    goto :goto_0
.end method

.method static synthetic access$002(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/facebook/share/widget/ShareDialog;)Lcom/facebook/share/widget/ShareDialog;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
    .param p1, "x1"    # Lcom/facebook/share/widget/ShareDialog;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->shareDialog:Lcom/facebook/share/widget/ShareDialog;

    return-object p1
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->userAuthRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method static synthetic access$1000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->needUpdatePrivacy()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;J)J
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
    .param p1, "x1"    # J

    .prologue
    .line 61
    iput-wide p1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->detailErrorCode:J

    return-wide p1
.end method

.method static synthetic access$1400(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->dialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
    .param p1, "x1"    # Landroid/app/Dialog;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->dialog:Landroid/app/Dialog;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->resetDetailErrorCode()V

    return-void
.end method

.method static synthetic access$1602(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;)Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
    .param p1, "x1"    # Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->privacyValue:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lcom/microsoft/xbox/service/model/ProfileModel;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    return-object v0
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lcom/facebook/AccessToken;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->token:Lcom/facebook/AccessToken;

    return-object v0
.end method

.method static synthetic access$202(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/facebook/AccessToken;)Lcom/facebook/AccessToken;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
    .param p1, "x1"    # Lcom/facebook/AccessToken;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->token:Lcom/facebook/AccessToken;

    return-object p1
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->firstLoginWithReadOnly:Z

    return v0
.end method

.method static synthetic access$302(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
    .param p1, "x1"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->firstLoginWithReadOnly:Z

    return p1
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->tokenString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->tokenString:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->currentAction:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    return-object v0
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->showShareDialog()V

    return-void
.end method

.method static synthetic access$702(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->facebookPermission:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$800(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lcom/facebook/CallbackManager;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->callbackManager:Lcom/facebook/CallbackManager;

    return-object v0
.end method

.method static synthetic access$802(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/facebook/CallbackManager;)Lcom/facebook/CallbackManager;
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
    .param p1, "x1"    # Lcom/facebook/CallbackManager;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->callbackManager:Lcom/facebook/CallbackManager;

    return-object p1
.end method

.method static synthetic access$900(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)Lcom/facebook/FacebookCallback;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->loginResult:Lcom/facebook/FacebookCallback;

    return-object v0
.end method

.method public static getFacebookManagerReady()Lcom/microsoft/xbox/toolkit/Ready;
    .locals 1

    .prologue
    .line 206
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->facebookManagerReady:Lcom/microsoft/xbox/toolkit/Ready;

    return-object v0
.end method

.method private getFacebookNameOrGamertag(Ljava/util/List;I)Ljava/lang/String;
    .locals 4
    .param p2, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;I)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 504
    .local p1, "people":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    const-string v0, ""

    .line 505
    .local v0, "name":Ljava/lang/String;
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge p2, v2, :cond_1

    .line 506
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 507
    .local v1, "person":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    if-eqz v1, :cond_1

    iget-object v2, v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->recommendation:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubRecommendation;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->recommendation:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubRecommendation;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubRecommendation;->Reasons:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->recommendation:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubRecommendation;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubRecommendation;->Reasons:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 510
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->recommendation:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubRecommendation;

    iget-object v2, v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubRecommendation;->Reasons:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "name":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 511
    .restart local v0    # "name":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 512
    :cond_0
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    iget-object v0, v2, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->gamertag:Ljava/lang/String;

    .line 516
    .end local v1    # "person":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    :cond_1
    return-object v0
.end method

.method public static declared-synchronized getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
    .locals 2

    .prologue
    .line 210
    const-class v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->instance:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    if-nez v0, :cond_0

    .line 211
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->instance:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;

    .line 213
    :cond_0
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->instance:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 210
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private loadUpdateFacebookOptInStatusAsyncTask(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V
    .locals 4
    .param p1, "nextAction"    # Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    .prologue
    const/4 v3, 0x1

    .line 285
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$4;->$SwitchMap$com$microsoft$xbox$service$model$friendfinder$OptInStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 305
    :goto_0
    return-void

    .line 287
    :pswitch_0
    invoke-static {}, Lcom/facebook/AccessToken;->getCurrentAccessToken()Lcom/facebook/AccessToken;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->token:Lcom/facebook/AccessToken;

    .line 288
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->token:Lcom/facebook/AccessToken;

    if-eqz v1, :cond_0

    .line 289
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->token:Lcom/facebook/AccessToken;

    invoke-virtual {v1}, Lcom/facebook/AccessToken;->getToken()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->tokenString:Ljava/lang/String;

    .line 291
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->optInAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->cancelUpdateFacebookOptInStatusAsyncTask(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;)V

    .line 292
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;-><init>(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V

    iput-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->optInAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;

    .line 293
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->optInAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->load(Z)V

    goto :goto_0

    .line 296
    :pswitch_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->optOutAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->cancelUpdateFacebookOptInStatusAsyncTask(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;)V

    .line 297
    new-instance v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;

    invoke-direct {v1, p0, p1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;-><init>(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V

    iput-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->optOutAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;

    .line 298
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->optOutAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;

    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->load(Z)V

    goto :goto_0

    .line 301
    :pswitch_2
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;

    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->ShowPrompt:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;-><init>(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V

    .line 302
    .local v0, "resetTask":Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;
    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->load(Z)V

    goto :goto_0

    .line 285
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private varargs multiLineText([I)Ljava/lang/String;
    .locals 5
    .param p1, "textIds"    # [I

    .prologue
    .line 653
    array-length v2, p1

    if-nez v2, :cond_1

    .line 654
    const-string v1, ""

    .line 661
    :cond_0
    return-object v1

    .line 657
    :cond_1
    sget-object v2, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const/4 v3, 0x0

    aget v3, p1, v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 658
    .local v1, "text":Ljava/lang/String;
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 659
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    aget v4, p1, v0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 658
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private needUpdatePrivacy()Z
    .locals 2

    .prologue
    .line 534
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->privacyValue:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    sget-object v1, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->NotSet:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->privacyValue:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    sget-object v1, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->Blocked:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private resetDetailErrorCode()V
    .locals 2

    .prologue
    .line 649
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->detailErrorCode:J

    .line 650
    return-void
.end method

.method private showShareDialog()V
    .locals 6

    .prologue
    .line 350
    new-instance v3, Lcom/facebook/share/widget/ShareDialog;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/share/widget/ShareDialog;-><init>(Landroid/app/Activity;)V

    iput-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->shareDialog:Lcom/facebook/share/widget/ShareDialog;

    .line 351
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->shareDialog:Lcom/facebook/share/widget/ShareDialog;

    iget-object v4, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->callbackManager:Lcom/facebook/CallbackManager;

    iget-object v5, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->shareResult:Lcom/facebook/FacebookCallback;

    invoke-virtual {v3, v4, v5}, Lcom/facebook/share/widget/ShareDialog;->registerCallback(Lcom/facebook/CallbackManager;Lcom/facebook/FacebookCallback;)V

    .line 352
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v3

    invoke-static {v3}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertNotNull(Ljava/lang/Object;)V

    .line 353
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v2

    .line 354
    .local v2, "meProfileModel":Lcom/microsoft/xbox/service/model/ProfileModel;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/ProfileModel;->getGamerPicImageUrl()Ljava/lang/String;

    move-result-object v1

    .line 356
    .local v1, "imageUrl":Ljava/lang/String;
    :goto_0
    const-class v3, Lcom/facebook/share/model/ShareLinkContent;

    invoke-static {v3}, Lcom/facebook/share/widget/ShareDialog;->canShow(Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 357
    new-instance v3, Lcom/facebook/share/model/ShareLinkContent$Builder;

    invoke-direct {v3}, Lcom/facebook/share/model/ShareLinkContent$Builder;-><init>()V

    .line 358
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/share/model/ShareLinkContent$Builder;->setImageUrl(Landroid/net/Uri;)Lcom/facebook/share/model/ShareLinkContent$Builder;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070517

    .line 359
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/share/model/ShareLinkContent$Builder;->setContentTitle(Ljava/lang/String;)Lcom/facebook/share/model/ShareLinkContent$Builder;

    move-result-object v3

    sget-object v4, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v5, 0x7f070516

    .line 360
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/share/model/ShareLinkContent$Builder;->setContentDescription(Ljava/lang/String;)Lcom/facebook/share/model/ShareLinkContent$Builder;

    move-result-object v3

    const-string v4, "https://go.microsoft.com/fwlink/?LinkId=698852"

    .line 361
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/share/model/ShareLinkContent$Builder;->setContentUrl(Landroid/net/Uri;)Lcom/facebook/share/model/ShareContent$Builder;

    move-result-object v3

    check-cast v3, Lcom/facebook/share/model/ShareLinkContent$Builder;

    .line 362
    invoke-virtual {v3}, Lcom/facebook/share/model/ShareLinkContent$Builder;->build()Lcom/facebook/share/model/ShareLinkContent;

    move-result-object v0

    .line 364
    .local v0, "content":Lcom/facebook/share/model/ShareLinkContent;
    iget-object v3, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->shareDialog:Lcom/facebook/share/widget/ShareDialog;

    invoke-virtual {v3, v0}, Lcom/facebook/share/widget/ShareDialog;->show(Ljava/lang/Object;)V

    .line 366
    .end local v0    # "content":Lcom/facebook/share/model/ShareLinkContent;
    :cond_0
    return-void

    .line 354
    .end local v1    # "imageUrl":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public cancelAsyncTasks()V
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->optInAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->cancelUpdateFacebookOptInStatusAsyncTask(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;)V

    .line 370
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->optOutAsyncTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->cancelUpdateFacebookOptInStatusAsyncTask(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;)V

    .line 371
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->cancelGetPrivacyValueAsyncTask()V

    .line 372
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->cancelGetPeopleHubRecommendationsAsyncTask()V

    .line 373
    return-void
.end method

.method public cancelGetPeopleHubRecommendationsAsyncTask()V
    .locals 1

    .prologue
    .line 618
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getRecommendationsTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$GetPeopleHubRecommendationsAsyncTask;

    if-eqz v0, :cond_0

    .line 619
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getRecommendationsTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$GetPeopleHubRecommendationsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$GetPeopleHubRecommendationsAsyncTask;->cancel()V

    .line 620
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getRecommendationsTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$GetPeopleHubRecommendationsAsyncTask;

    .line 622
    :cond_0
    return-void
.end method

.method public cancelGetPrivacyValueAsyncTask()V
    .locals 1

    .prologue
    .line 590
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getPrivacyValueTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$GetPrivacyValueAsyncTask;

    if-eqz v0, :cond_0

    .line 591
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getPrivacyValueTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$GetPrivacyValueAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$GetPrivacyValueAsyncTask;->cancel()V

    .line 592
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getPrivacyValueTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$GetPrivacyValueAsyncTask;

    .line 594
    :cond_0
    return-void
.end method

.method public cancelUpdateFacebookOptInStatusAsyncTask(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;)V
    .locals 0
    .param p1, "task"    # Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;

    .prologue
    .line 539
    if-eqz p1, :cond_0

    .line 540
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$UpdateFacebookOptInStatusAsyncTask;->cancel()V

    .line 541
    const/4 p1, 0x0

    .line 543
    :cond_0
    return-void
.end method

.method public getCurrentAction()Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->currentAction:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    return-object v0
.end method

.method public getDialogBodyTextId(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)Ljava/lang/String;
    .locals 4
    .param p1, "nextStatus"    # Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    .prologue
    const/4 v3, 0x3

    .line 380
    const-string v0, ""

    .line 381
    .local v0, "text":Ljava/lang/String;
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$4;->$SwitchMap$com$microsoft$xbox$service$model$friendfinder$OptInStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 386
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$4;->$SwitchMap$com$microsoft$xbox$service$model$privacy$PrivacySettings$PrivacySettingValue:[I

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->privacyValue:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    invoke-virtual {v2}, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    .line 397
    sget-object v1, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v2, 0x7f070525

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 403
    :goto_0
    return-object v0

    .line 383
    :pswitch_0
    new-array v1, v3, [I

    fill-array-data v1, :array_0

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->multiLineText([I)Ljava/lang/String;

    move-result-object v0

    .line 384
    goto :goto_0

    .line 388
    :pswitch_1
    new-array v1, v3, [I

    fill-array-data v1, :array_1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->multiLineText([I)Ljava/lang/String;

    move-result-object v0

    .line 389
    goto :goto_0

    .line 391
    :pswitch_2
    new-array v1, v3, [I

    fill-array-data v1, :array_2

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->multiLineText([I)Ljava/lang/String;

    move-result-object v0

    .line 392
    goto :goto_0

    .line 394
    :pswitch_3
    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_3

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->multiLineText([I)Ljava/lang/String;

    move-result-object v0

    .line 395
    goto :goto_0

    .line 381
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch

    .line 386
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 383
    :array_0
    .array-data 4
        0x7f070553
        0x7f070555
        0x7f070554
    .end array-data

    .line 388
    :array_1
    .array-data 4
        0x7f070525
        0x7f070527
        0x7f070526
    .end array-data

    .line 391
    :array_2
    .array-data 4
        0x7f070525
        0x7f070524
        0x7f070523
    .end array-data

    .line 394
    :array_3
    .array-data 4
        0x7f070525
        0x7f070528
    .end array-data
.end method

.method public getDialogErrorTextId(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)I
    .locals 6
    .param p1, "nextStatus"    # Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    .prologue
    .line 419
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$4;->$SwitchMap$com$microsoft$xbox$service$model$friendfinder$OptInStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 433
    const v0, 0x7f07052a

    .line 436
    .local v0, "errorId":I
    :goto_0
    return v0

    .line 421
    .end local v0    # "errorId":I
    :pswitch_0
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->currentAction:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    sget-object v2, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->TokenRenewalRequired:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-ne v1, v2, :cond_0

    .line 422
    const v0, 0x7f07054e

    .restart local v0    # "errorId":I
    goto :goto_0

    .line 423
    .end local v0    # "errorId":I
    :cond_0
    iget-wide v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->detailErrorCode:J

    const-wide/16 v4, 0x3ef

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 424
    const v0, 0x7f070515

    .restart local v0    # "errorId":I
    goto :goto_0

    .line 426
    .end local v0    # "errorId":I
    :cond_1
    const v0, 0x7f07052b

    .line 428
    .restart local v0    # "errorId":I
    goto :goto_0

    .line 430
    .end local v0    # "errorId":I
    :pswitch_1
    const v0, 0x7f070558

    .line 431
    .restart local v0    # "errorId":I
    goto :goto_0

    .line 419
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getFacebookCallbackManager()Lcom/facebook/CallbackManager;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->callbackManager:Lcom/facebook/CallbackManager;

    return-object v0
.end method

.method public getFacebookFriendFinderState()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    return-object v0
.end method

.method public getFacebookFriendsCount()I
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->recommendations:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->recommendations:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;->recommendationSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationSummary;

    if-eqz v0, :cond_0

    .line 441
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->recommendations:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;->recommendationSummary:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationSummary;

    iget v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$RecommendationSummary;->facebookFriend:I

    .line 443
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFacebookFriendsList()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 447
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->recommendations:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->recommendations:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    iget-object v0, v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;->people:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/JavaUtil;->isNullOrEmpty(Ljava/lang/Iterable;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 449
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->recommendations:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    iget-object v1, v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;->people:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookFriendsCount()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 451
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFacebookUpsellDescriptionLineOne()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 491
    const-string v0, ""

    .line 492
    .local v0, "text":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookFriendsCount()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 497
    new-array v1, v2, [I

    fill-array-data v1, :array_0

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->multiLineText([I)Ljava/lang/String;

    move-result-object v0

    .line 500
    :goto_0
    return-object v0

    .line 494
    :pswitch_0
    new-array v1, v2, [I

    fill-array-data v1, :array_1

    invoke-direct {p0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->multiLineText([I)Ljava/lang/String;

    move-result-object v0

    .line 495
    goto :goto_0

    .line 492
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    .line 497
    :array_0
    .array-data 4
        0x7f07051a
        0x7f070518
    .end array-data

    .line 494
    :array_1
    .array-data 4
        0x7f070519
        0x7f070518
    .end array-data
.end method

.method public getFacebookUpsellTitle()Ljava/lang/String;
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 455
    const-string v5, ""

    .line 456
    .local v5, "titleText":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookFriendsCount()I

    move-result v0

    .line 457
    .local v0, "findFriendsCount":I
    if-ltz v0, :cond_0

    .line 459
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookFriendsList()Ljava/util/List;

    move-result-object v2

    .line 460
    .local v2, "peopleList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    invoke-direct {p0, v2, v9}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookNameOrGamertag(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v1

    .line 461
    .local v1, "firstFriend":Ljava/lang/String;
    invoke-direct {p0, v2, v10}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookNameOrGamertag(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v3

    .line 462
    .local v3, "secondFriend":Ljava/lang/String;
    invoke-direct {p0, v2, v11}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getFacebookNameOrGamertag(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v4

    .line 464
    .local v4, "thirdFriend":Ljava/lang/String;
    packed-switch v0, :pswitch_data_0

    .line 478
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v8, 0x7f07051c

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    new-array v8, v12, [Ljava/lang/Object;

    aput-object v1, v8, v9

    aput-object v3, v8, v10

    add-int/lit8 v9, v0, -0x2

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 482
    .end local v1    # "firstFriend":Ljava/lang/String;
    .end local v2    # "peopleList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    .end local v3    # "secondFriend":Ljava/lang/String;
    .end local v4    # "thirdFriend":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v5

    .line 466
    .restart local v1    # "firstFriend":Ljava/lang/String;
    .restart local v2    # "peopleList":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    .restart local v3    # "secondFriend":Ljava/lang/String;
    .restart local v4    # "thirdFriend":Ljava/lang/String;
    :pswitch_0
    sget-object v6, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v7, 0x7f07051d

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 467
    goto :goto_0

    .line 469
    :pswitch_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v8, 0x7f07051e

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    new-array v8, v10, [Ljava/lang/Object;

    aput-object v1, v8, v9

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 470
    goto :goto_0

    .line 472
    :pswitch_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v8, 0x7f070520

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    new-array v8, v11, [Ljava/lang/Object;

    aput-object v1, v8, v9

    aput-object v3, v8, v10

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 473
    goto :goto_0

    .line 475
    :pswitch_3
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    sget-object v7, Lcom/microsoft/xbox/XLEApplication;->Resources:Landroid/content/res/Resources;

    const v8, 0x7f07051f

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    new-array v8, v12, [Ljava/lang/Object;

    aput-object v1, v8, v9

    aput-object v3, v8, v10

    aput-object v4, v8, v11

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 476
    goto :goto_0

    .line 464
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getLoginBehavior()Lcom/facebook/login/LoginBehavior;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->loginBehavior:Lcom/facebook/login/LoginBehavior;

    return-object v0
.end method

.method public getUserAuthEvents()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 217
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->userAuthRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method public isFacebookFriendFinderOptedIn()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 340
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    .line 341
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;->getLinkedAccountOptInStatus()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountOptInStatus;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    .line 342
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;->getLinkedAccountTokenStatus()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountTokenStatus;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountTokenStatus;->OK:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountTokenStatus;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isLoggedIn()Z
    .locals 1

    .prologue
    .line 221
    invoke-static {}, Lcom/facebook/AccessToken;->getCurrentAccessToken()Lcom/facebook/AccessToken;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPostingToFacebook()Z
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->shareDialog:Lcom/facebook/share/widget/ShareDialog;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadGetPrivacyValueAsyncTask()V
    .locals 2

    .prologue
    .line 597
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->cancelGetPrivacyValueAsyncTask()V

    .line 598
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$GetPrivacyValueAsyncTask;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$GetPrivacyValueAsyncTask;-><init>(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getPrivacyValueTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$GetPrivacyValueAsyncTask;

    .line 599
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getPrivacyValueTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$GetPrivacyValueAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$GetPrivacyValueAsyncTask;->load(Z)V

    .line 601
    return-void
.end method

.method public loadPeopleHubFriendFinderState()V
    .locals 2

    .prologue
    .line 376
    invoke-static {}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->getInstance()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderModel;->loadAsync(Z)V

    .line 377
    return-void
.end method

.method public loadPeopleHubRecommendationsAsyncTask()V
    .locals 2

    .prologue
    .line 625
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->cancelGetPeopleHubRecommendationsAsyncTask()V

    .line 626
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    .line 627
    new-instance v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$GetPeopleHubRecommendationsAsyncTask;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$GetPeopleHubRecommendationsAsyncTask;-><init>(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getRecommendationsTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$GetPeopleHubRecommendationsAsyncTask;

    .line 628
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->getRecommendationsTask:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$GetPeopleHubRecommendationsAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$GetPeopleHubRecommendationsAsyncTask;->load(Z)V

    .line 629
    return-void
.end method

.method public login()V
    .locals 3

    .prologue
    .line 238
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->firstLoginWithReadOnly:Z

    .line 239
    sget-object v0, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->currentAction:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    .line 240
    invoke-static {}, Lcom/facebook/login/LoginManager;->getInstance()Lcom/facebook/login/LoginManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->loginBehavior:Lcom/facebook/login/LoginBehavior;

    invoke-virtual {v0, v1}, Lcom/facebook/login/LoginManager;->setLoginBehavior(Lcom/facebook/login/LoginBehavior;)Lcom/facebook/login/LoginManager;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->facebookPermission:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/login/LoginManager;->logInWithReadPermissions(Landroid/app/Activity;Ljava/util/Collection;)V

    .line 241
    return-void
.end method

.method public loginToRepair()V
    .locals 3

    .prologue
    .line 244
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->firstLoginWithReadOnly:Z

    .line 245
    sget-object v0, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->TokenRenewalRequired:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->currentAction:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    .line 246
    invoke-static {}, Lcom/facebook/login/LoginManager;->getInstance()Lcom/facebook/login/LoginManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->loginBehavior:Lcom/facebook/login/LoginBehavior;

    invoke-virtual {v0, v1}, Lcom/facebook/login/LoginManager;->setLoginBehavior(Lcom/facebook/login/LoginBehavior;)Lcom/facebook/login/LoginManager;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->facebookPermission:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/login/LoginManager;->logInWithReadPermissions(Landroid/app/Activity;Ljava/util/Collection;)V

    .line 247
    return-void
.end method

.method public logout()V
    .locals 2

    .prologue
    .line 255
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->userAuthRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->Logout:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 256
    sget-object v0, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedOut:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->currentAction:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    .line 257
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->resetFacebookToken(Z)V

    .line 258
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->currentAction:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->loadUpdateFacebookOptInStatusAsyncTask(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V

    .line 259
    return-void
.end method

.method public navigateToSuggestionList(Z)V
    .locals 3
    .param p1, "forceNavigate"    # Z

    .prologue
    .line 524
    iget-boolean v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->shouldNavigateToSuggesionList:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    .line 525
    :cond_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    instance-of v0, v0, Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen;

    if-eqz v0, :cond_2

    .line 526
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getCurrentActivity()Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;->forceRefresh()V

    .line 531
    :cond_1
    :goto_0
    return-void

    .line 528
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->getInstance()Lcom/microsoft/xbox/toolkit/ui/NavigationManager;

    move-result-object v0

    const-class v1, Lcom/microsoft/xbox/xle/app/activity/SuggestionsPeopleScreen;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/toolkit/ui/NavigationManager;->NavigateTo(Ljava/lang/Class;Z)V

    goto :goto_0
.end method

.method public notInterested()V
    .locals 1

    .prologue
    .line 262
    sget-object v0, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->DontShow:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->currentAction:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    .line 263
    sget-object v0, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedOut:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->loadUpdateFacebookOptInStatusAsyncTask(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V

    .line 264
    return-void
.end method

.method protected onGetPeopleHubRecommendationsAsyncTaskCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 632
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$4;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 646
    :cond_0
    :goto_0
    return-void

    .line 636
    :pswitch_0
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->meProfileModel:Lcom/microsoft/xbox/service/model/ProfileModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/ProfileModel;->getPeopleHubRecommendationsRawData()Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->recommendations:Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPeopleSummary;

    .line 637
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->currentAction:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedIn:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-ne v0, v1, :cond_0

    .line 638
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;->GET_PEOPLEHUB_RECOMMENDATIONS:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyFriendFinderConfirmDialogAsyncTaskCompleted(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;)V

    goto :goto_0

    .line 643
    :pswitch_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;->GET_PEOPLEHUB_RECOMMENDATIONS:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyFriendFinderConfirmDialogAsyncTaskFailed(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;)V

    goto :goto_0

    .line 632
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected onGetPrivacyValueAsyncTaskCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 2
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 604
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$4;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 615
    :goto_0
    return-void

    .line 608
    :pswitch_0
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;->GET_PRIVACY_VALUE:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyFriendFinderConfirmDialogAsyncTaskCompleted(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;)V

    goto :goto_0

    .line 612
    :pswitch_1
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;->GET_PRIVACY_VALUE:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyFriendFinderConfirmDialogAsyncTaskFailed(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;)V

    goto :goto_0

    .line 604
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected onUpdateFacebookOptInStatusCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V
    .locals 4
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .param p2, "optInStatus"    # Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    .prologue
    const/4 v3, 0x0

    .line 546
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$4;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 587
    :cond_0
    :goto_0
    return-void

    .line 555
    :pswitch_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->loadPeopleHubFriendFinderState()V

    .line 556
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->currentAction:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->DontShow:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-eq v0, v1, :cond_1

    .line 557
    invoke-static {}, Lcom/microsoft/xbox/service/model/ProfileModel;->getMeProfileModel()Lcom/microsoft/xbox/service/model/ProfileModel;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/service/model/ProfileModel;->loadAsync(Z)V

    .line 558
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->loadPeopleHubRecommendationsAsyncTask()V

    .line 560
    :cond_1
    invoke-direct {p0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->needUpdatePrivacy()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 562
    sget-object v0, Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;->FriendCategoryShareIdentity:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->privacyValue:Lcom/microsoft/xbox/service/model/privacy/PrivacySettings$PrivacySettingValue;

    .line 564
    :cond_2
    iget-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->currentAction:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    sget-object v1, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->OptedOut:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    if-ne v0, v1, :cond_0

    .line 565
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->dismissFriendFinderConfirmDialog()V

    goto :goto_0

    .line 570
    :pswitch_1
    sget-object v0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$4;->$SwitchMap$com$microsoft$xbox$service$model$friendfinder$OptInStatus:[I

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->currentAction:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    :pswitch_2
    goto :goto_0

    .line 574
    :pswitch_3
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;->UPDATE_FACEBOOK_OPTIN_STATUS:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->notifyFriendFinderConfirmDialogAsyncTaskFailed(Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;)V

    goto :goto_0

    .line 579
    :pswitch_4
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->currentAction:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;->NONE:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showFriendFinderConfirmDialog(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;Z)V

    goto :goto_0

    .line 582
    :pswitch_5
    invoke-static {}, Lcom/microsoft/xbox/toolkit/DialogManager;->getInstance()Lcom/microsoft/xbox/toolkit/DialogManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/DialogManager;->getManager()Lcom/microsoft/xbox/toolkit/IProjectSpecificDialogManager;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->currentAction:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;->TOKEN_RENEWAL:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/SGProjectSpecificDialogManager;->showFriendFinderConfirmDialog(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$AsyncTaskType;Z)V

    goto :goto_0

    .line 546
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 570
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public resetFacebookFriendFinderState()V
    .locals 1

    .prologue
    .line 267
    sget-object v0, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->ShowPrompt:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->currentAction:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    .line 268
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->resetFacebookToken(Z)V

    .line 269
    sget-object v0, Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;->ShowPrompt:Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->loadUpdateFacebookOptInStatusAsyncTask(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V

    .line 270
    return-void
.end method

.method public resetFacebookToken(Z)V
    .locals 1
    .param p1, "forceResetLoginToken"    # Z

    .prologue
    const/4 v0, 0x0

    .line 308
    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->token:Lcom/facebook/AccessToken;

    .line 309
    iput-object v0, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->tokenString:Ljava/lang/String;

    .line 310
    if-eqz p1, :cond_0

    .line 312
    invoke-static {}, Lcom/facebook/login/LoginManager;->getInstance()Lcom/facebook/login/LoginManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/login/LoginManager;->logOut()V

    .line 314
    :cond_0
    return-void
.end method

.method public setFacebookFriendFinderState(Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;)V
    .locals 4
    .param p1, "result"    # Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    .prologue
    .line 326
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->friendsFinderStateResult:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;

    .line 328
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$FriendsFinderStateResult;->getLinkedAccountTokenStatus()Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountTokenStatus;

    move-result-object v1

    sget-object v2, Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountTokenStatus;->OK:Lcom/microsoft/xbox/service/model/friendfinder/FriendFinderState$LinkedAccountTokenStatus;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 329
    .local v0, "userLoggedIn":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 330
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->userAuthRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->LoginSuccess:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 334
    :goto_1
    sget-object v1, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Initial firing of log in relay: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Diagnostic(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    return-void

    .line 328
    .end local v0    # "userLoggedIn":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 332
    .restart local v0    # "userLoggedIn":Z
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->userAuthRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v2, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;->Logout:Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager$FacebookCallbackResult;

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public setLoginBehavior(Lcom/facebook/login/LoginBehavior;)V
    .locals 0
    .param p1, "behavior"    # Lcom/facebook/login/LoginBehavior;

    .prologue
    .line 233
    iput-object p1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->loginBehavior:Lcom/facebook/login/LoginBehavior;

    .line 234
    return-void
.end method

.method public setShouldNavigateToSuggestionList(Z)V
    .locals 0
    .param p1, "doNavigation"    # Z

    .prologue
    .line 520
    iput-boolean p1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->shouldNavigateToSuggesionList:Z

    .line 521
    return-void
.end method

.method public shareToFacebook()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 250
    iput-boolean v4, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->firstLoginWithReadOnly:Z

    .line 251
    invoke-static {}, Lcom/facebook/login/LoginManager;->getInstance()Lcom/facebook/login/LoginManager;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->loginBehavior:Lcom/facebook/login/LoginBehavior;

    invoke-virtual {v0, v1}, Lcom/facebook/login/LoginManager;->setLoginBehavior(Lcom/facebook/login/LoginBehavior;)Lcom/facebook/login/LoginManager;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "publish_actions"

    aput-object v3, v2, v4

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/login/LoginManager;->logInWithPublishPermissions(Landroid/app/Activity;Ljava/util/Collection;)V

    .line 252
    return-void
.end method

.method public startUpdateOptInStatusAsyncTask(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V
    .locals 0
    .param p1, "taskStatus"    # Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;

    .prologue
    .line 276
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/service/network/managers/friendfinder/FacebookManager;->loadUpdateFacebookOptInStatusAsyncTask(Lcom/microsoft/xbox/service/model/friendfinder/OptInStatus;)V

    .line 277
    return-void
.end method
