.class public Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;
.super Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;
.source "ClubAdminRequestsScreenViewModel.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;,
        Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestHeaderItem;,
        Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;",
        "Lcom/microsoft/xbox/toolkit/incrementalload/IncrementalLoader",
        "<",
        "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOAD_INCREMENT:I = 0x32

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final allRosterRequests:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;",
            ">;"
        }
    .end annotation
.end field

.field private getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

.field private final headerItem:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestHeaderItem;

.field private final isLoadingMoreItems:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final listenerManager:Lcom/microsoft/xbox/toolkit/ListenerManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/ListenerManager",
            "<",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/Collection",
            "<+",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final loadedRequestModels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;",
            ">;"
        }
    .end annotation
.end field

.field private final personSummaries:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;"
        }
    .end annotation
.end field

.field private final personSummaryCompletedTask:Lcom/microsoft/xbox/toolkit/generics/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V
    .locals 1
    .param p1, "screenLayout"    # Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;-><init>(Lcom/microsoft/xbox/toolkit/ui/ScreenLayout;)V

    .line 70
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 71
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubAdminRequestsScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->allRosterRequests:Ljava/util/List;

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->loadedRequestModels:Ljava/util/List;

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->personSummaries:Ljava/util/Map;

    .line 75
    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->personSummaryCompletedTask:Lcom/microsoft/xbox/toolkit/generics/Action;

    .line 76
    new-instance v0, Lcom/microsoft/xbox/toolkit/ListenerManager;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/ListenerManager;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->listenerManager:Lcom/microsoft/xbox/toolkit/ListenerManager;

    .line 77
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->isLoadingMoreItems:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 78
    new-instance v0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestHeaderItem;

    invoke-direct {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestHeaderItem;-><init>()V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->headerItem:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestHeaderItem;

    .line 79
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;I)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;
    .param p1, "x1"    # I

    .prologue
    .line 49
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->showError(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->updateAdapter()V

    return-void
.end method

.method static synthetic access$200(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;I)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;
    .param p1, "x1"    # I

    .prologue
    .line 49
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->showError(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->updateAdapter()V

    return-void
.end method

.method static synthetic access$400(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;I)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;
    .param p1, "x1"    # I

    .prologue
    .line 49
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->showError(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->updateAdapter()V

    return-void
.end method

.method static synthetic access$600(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;I)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;
    .param p1, "x1"    # I

    .prologue
    .line 49
    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->showError(I)V

    return-void
.end method

.method static synthetic access$700(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->updateAdapter()V

    return-void
.end method

.method static synthetic access$lambda$0(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->onMoreItemsLoaded(Ljava/util/List;)V

    return-void
.end method

.method private declared-synchronized ensureLoadedRequestsUpdated()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 295
    monitor-enter p0

    :try_start_0
    iget-object v6, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->allRosterRequests:Ljava/util/List;

    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->loadedRequestModels:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    iget-object v8, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->allRosterRequests:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    invoke-interface {v6, v7, v8}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    .line 296
    .local v2, "itemsToCheck":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    invoke-direct {v4, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 298
    .local v4, "newItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;

    .line 299
    .local v1, "item":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->personSummaries:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->xuid()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 300
    .local v5, "requesterSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->personSummaries:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->actorXuid()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 302
    .local v0, "actorSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    if-eqz v5, :cond_0

    if-eqz v0, :cond_0

    .line 303
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->getCreatedDate()Ljava/util/Date;

    move-result-object v7

    invoke-static {v5, v0, v7}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;->with(Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;Ljava/util/Date;)Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;

    move-result-object v3

    .line 305
    .local v3, "newItem":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;
    iget-object v7, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->loadedRequestModels:Ljava/util/List;

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 306
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 295
    .end local v0    # "actorSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    .end local v1    # "item":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    .end local v2    # "itemsToCheck":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    .end local v3    # "newItem":Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestListItem;
    .end local v4    # "newItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;>;"
    .end local v5    # "requesterSummary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 313
    .restart local v2    # "itemsToCheck":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    .restart local v4    # "newItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;>;"
    :cond_0
    monitor-exit p0

    return-object v4
.end method

.method private getAllXuids()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157
    new-instance v1, Ljava/util/HashSet;

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->allRosterRequests:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(I)V

    .line 159
    .local v1, "xuids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->allRosterRequests:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;

    .line 160
    .local v0, "item":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->xuid()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 163
    .end local v0    # "item":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v2
.end method

.method private declared-synchronized getXuidsToLoad(Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 317
    .local p1, "requestsToLoad":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    monitor-enter p0

    :try_start_0
    new-instance v3, Ljava/util/HashSet;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/HashSet;-><init>(I)V

    .line 319
    .local v3, "xuidsToLoad":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;

    .line 320
    .local v1, "item":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->xuid()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 321
    .local v2, "xuid":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->actorXuid()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 323
    .local v0, "actorXuid":Ljava/lang/String;
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->personSummaries:Ljava/util/Map;

    invoke-interface {v5, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 324
    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 327
    :cond_1
    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->personSummaries:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 328
    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 317
    .end local v0    # "actorXuid":Ljava/lang/String;
    .end local v1    # "item":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    .end local v2    # "xuid":Ljava/lang/String;
    .end local v3    # "xuidsToLoad":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 332
    .restart local v3    # "xuidsToLoad":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_2
    :try_start_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v4
.end method

.method static synthetic lambda$acceptRequest$0(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->updateAdapter()V

    return-void
.end method

.method static synthetic lambda$ignoreRequest$1(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;)V
    .locals 0
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->updateAdapter()V

    return-void
.end method

.method static synthetic lambda$onClubModelChanged$2(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;)I
    .locals 2
    .param p0, "lhs"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;
    .param p1, "rhs"    # Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;

    .prologue
    .line 232
    invoke-virtual {p1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->createdDate()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;->createdDate()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic lambda$onMoreItemsLoaded$3(Ljava/util/List;Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 0
    .param p0, "newItems"    # Ljava/util/List;
    .param p1, "collectionAction"    # Lcom/microsoft/xbox/toolkit/generics/Action;

    .prologue
    .line 344
    invoke-interface {p1, p0}, Lcom/microsoft/xbox/toolkit/generics/Action;->run(Ljava/lang/Object;)V

    return-void
.end method

.method private declared-synchronized onMoreItemsLoaded(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 336
    .local p1, "summaries":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;>;"
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;

    .line 337
    .local v1, "summary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->personSummaries:Ljava/util/Map;

    iget-object v4, v1, Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;->xuid:Ljava/lang/String;

    invoke-interface {v3, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 336
    .end local v1    # "summary":Lcom/microsoft/xbox/service/network/managers/IPeopleHubResult$PeopleHubPersonSummary;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 340
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->ensureLoadedRequestsUpdated()Ljava/util/List;

    move-result-object v0

    .line 342
    .local v0, "newItems":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->isLoadingMoreItems:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 344
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->listenerManager:Lcom/microsoft/xbox/toolkit/ListenerManager;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$$Lambda$5;->lambdaFactory$(Ljava/util/List;)Lcom/microsoft/xbox/toolkit/generics/Action;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ListenerManager;->run(Lcom/microsoft/xbox/toolkit/generics/Action;)V

    .line 346
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->loadedRequestModels:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    :goto_1
    iput-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 347
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->updateAdapter()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 348
    monitor-exit p0

    return-void

    .line 346
    :cond_1
    :try_start_2
    sget-object v2, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private stopActiveTasks()V
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->cancel()V

    .line 203
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    .line 205
    :cond_0
    return-void
.end method


# virtual methods
.method public acceptRequest(J)V
    .locals 5
    .param p1, "xuid"    # J

    .prologue
    .line 168
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    .line 169
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v3

    const v4, 0x7f070207

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;-><init>(Ljava/lang/Runnable;I)V

    .line 168
    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->sendInvites(Ljava/util/List;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 171
    return-void
.end method

.method public declared-synchronized addOnMoreItemsLoadedListener(Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/Collection",
            "<+",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 352
    .local p1, "listener":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Ljava/util/Collection<+Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;>;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->listenerManager:Lcom/microsoft/xbox/toolkit/ListenerManager;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ListenerManager;->addListener(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 353
    monitor-exit p0

    return-void

    .line 352
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getLoadedRequestItems()Ljava/util/List;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->loadedRequestModels:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 89
    .local v0, "items":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->loadedRequestModels:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 90
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->headerItem:Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$ClubRequestHeaderItem;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->loadedRequestModels:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    :cond_0
    monitor-exit p0

    return-object v0

    .line 87
    .end local v0    # "items":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;>;"
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getNumOfRequests()I
    .locals 1

    .prologue
    .line 82
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->allRosterRequests:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized hasMoreItemsToLoad()Z
    .locals 2

    .prologue
    .line 263
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->loadedRequestModels:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->allRosterRequests:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public ignoreAll()V
    .locals 4

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->getAllXuids()Ljava/util/List;

    move-result-object v0

    .line 129
    .local v0, "xuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertFalse(Z)V

    .line 131
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 132
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminRequestsIgnoreAll(JLjava/util/List;)V

    .line 133
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    new-instance v2, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$2;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$2;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;)V

    invoke-virtual {v1, v0, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->ignoreInvites(Ljava/util/List;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 154
    :cond_0
    return-void
.end method

.method public ignoreRequest(J)V
    .locals 5
    .param p1, "xuid"    # J

    .prologue
    .line 175
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    .line 176
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;)Ljava/lang/Runnable;

    move-result-object v3

    const v4, 0x7f070228

    invoke-direct {v2, v3, v4}, Lcom/microsoft/xbox/service/model/clubs/SingleFailureRosterChangeCallback;-><init>(Ljava/lang/Runnable;I)V

    .line 175
    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->ignoreInvites(Ljava/util/List;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 178
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminRequestsIgnore(JLjava/lang/String;)V

    .line 179
    return-void
.end method

.method public isBusy()Z
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->isLoadingMoreItems:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized loadMoreItemsAsync()V
    .locals 7

    .prologue
    .line 268
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->isLoadingMoreItems:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 269
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->updateAdapter()V

    .line 271
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->hasMoreItemsToLoad()Z

    move-result v4

    invoke-static {v4}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertTrue(Z)V

    .line 273
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->hasMoreItemsToLoad()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 274
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->ensureLoadedRequestsUpdated()Ljava/util/List;

    .line 275
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->loadedRequestModels:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    .line 276
    .local v2, "startIndexToLoad":I
    add-int/lit8 v4, v2, 0x32

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->allRosterRequests:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 278
    .local v0, "endIndexToLoad":I
    new-instance v1, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->allRosterRequests:Ljava/util/List;

    invoke-interface {v4, v2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 280
    .local v1, "requestsToLoad":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    invoke-direct {p0, v1}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->getXuidsToLoad(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 282
    .local v3, "xuidsToLoad":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 283
    new-instance v4, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    iget-object v5, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->personSummaryCompletedTask:Lcom/microsoft/xbox/toolkit/generics/Action;

    invoke-direct {v4, v3, v5}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;-><init>(Ljava/util/Collection;Lcom/microsoft/xbox/toolkit/generics/Action;)V

    iput-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    .line 284
    iget-object v4, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->getPersonSummariesTask:Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/microsoft/xbox/toolkit/commonTasks/GetPersonSummariesAsyncTask;->load(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 290
    .end local v0    # "endIndexToLoad":I
    .end local v1    # "requestsToLoad":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    .end local v2    # "startIndexToLoad":I
    .end local v3    # "xuidsToLoad":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 286
    .restart local v0    # "endIndexToLoad":I
    .restart local v1    # "requestsToLoad":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    .restart local v2    # "startIndexToLoad":I
    .restart local v3    # "xuidsToLoad":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    :try_start_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->onMoreItemsLoaded(Ljava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 268
    .end local v0    # "endIndexToLoad":I
    .end local v1    # "requestsToLoad":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRosterItem;>;"
    .end local v2    # "startIndexToLoad":I
    .end local v3    # "xuidsToLoad":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method protected loadOverride(Z)V
    .locals 2
    .param p1, "forceRefresh"    # Z

    .prologue
    .line 209
    sget-object v0, Lcom/microsoft/xbox/toolkit/network/ListState;->LoadingState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 210
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    sget-object v1, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;->Roster:Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubHubRequestFilter;

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->loadAsync(ZLjava/util/List;)V

    .line 211
    return-void
.end method

.method protected declared-synchronized onClubModelChanged(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 3
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 220
    monitor-enter p0

    :try_start_0
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$3;->$SwitchMap$com$microsoft$xbox$toolkit$AsyncActionStatus:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v1, :pswitch_data_0

    .line 259
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 224
    :pswitch_0
    :try_start_1
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getData()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;->roster()Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;

    move-result-object v0

    .line 226
    .local v0, "roster":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    :goto_1
    invoke-static {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->isLoaded(Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 227
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->allRosterRequests:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 228
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->loadedRequestModels:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 229
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->allRosterRequests:Ljava/util/List;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->recommended()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 230
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->allRosterRequests:Ljava/util/List;

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;->requestedToJoin()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 232
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->allRosterRequests:Ljava/util/List;

    invoke-static {}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$$Lambda$4;->lambdaFactory$()Ljava/util/Comparator;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 234
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->ensureLoadedRequestsUpdated()Ljava/util/List;

    .line 236
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->loadedRequestModels:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 237
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ValidContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 238
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->updateAdapter()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 220
    .end local v0    # "roster":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 224
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 239
    .restart local v0    # "roster":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    :cond_2
    :try_start_2
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->hasMoreItemsToLoad()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 240
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->loadMoreItemsAsync()V

    goto :goto_0

    .line 242
    :cond_3
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->NoContentState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 243
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->updateAdapter()V

    goto :goto_0

    .line 251
    .end local v0    # "roster":Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$ClubRoster;
    :pswitch_1
    sget-object v1, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->TAG:Ljava/lang/String;

    const-string v2, "Club model changed to invalid state."

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    sget-object v1, Lcom/microsoft/xbox/toolkit/network/ListState;->ErrorState:Lcom/microsoft/xbox/toolkit/network/ListState;

    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->viewModelState:Lcom/microsoft/xbox/toolkit/network/ListState;

    .line 254
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->allRosterRequests:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 255
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->loadedRequestModels:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 256
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->updateAdapter()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 220
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onRehydrate()V
    .locals 1

    .prologue
    .line 183
    invoke-static {}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getInstance()Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/microsoft/xbox/xle/app/adapter/AdapterFactory;->getClubAdminRequestsScreenAdapter(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;)Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->adapter:Lcom/microsoft/xbox/xle/viewmodel/AdapterBase;

    .line 184
    return-void
.end method

.method public onSetActive()V
    .locals 3

    .prologue
    .line 188
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->getIsActive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 189
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onSetActive()V

    .line 190
    sget-object v0, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->INSTANCE:Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    sget-object v2, Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;->InClub:Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;

    invoke-virtual {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubPresenceHeartbeat;->setClubState(Lcom/microsoft/xbox/service/model/clubs/ClubModel;Lcom/microsoft/xbox/service/clubs/ClubDataTypes$ClubPresenceState;)V

    .line 192
    :cond_0
    return-void
.end method

.method protected onStopOverride()V
    .locals 0

    .prologue
    .line 196
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/clubs/ClubViewModelBase;->onStopOverride()V

    .line 197
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->stopActiveTasks()V

    .line 198
    return-void
.end method

.method public declared-synchronized removeOnMoreItemsLoadedListener(Lcom/microsoft/xbox/toolkit/generics/Action;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/generics/Action",
            "<",
            "Ljava/util/Collection",
            "<+",
            "Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 357
    .local p1, "listener":Lcom/microsoft/xbox/toolkit/generics/Action;, "Lcom/microsoft/xbox/toolkit/generics/Action<Ljava/util/Collection<+Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$IClubRequestListItem;>;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->listenerManager:Lcom/microsoft/xbox/toolkit/ListenerManager;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ListenerManager;->removeListener(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 358
    monitor-exit p0

    return-void

    .line 357
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public sendAllInvites()V
    .locals 4

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->getAllXuids()Ljava/util/List;

    move-result-object v0

    .line 100
    .local v0, "xuids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertFalse(Z)V

    .line 102
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 103
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->getId()J

    move-result-wide v2

    invoke-static {v2, v3, v0}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCClubs;->trackAdminSendAllInvites(JLjava/util/List;)V

    .line 104
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;->clubModel:Lcom/microsoft/xbox/service/model/clubs/ClubModel;

    new-instance v2, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$1;

    invoke-direct {v2, p0}, Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel$1;-><init>(Lcom/microsoft/xbox/xle/app/clubs/admin/ClubAdminRequestsScreenViewModel;)V

    invoke-virtual {v1, v0, v2}, Lcom/microsoft/xbox/service/model/clubs/ClubModel;->sendInvites(Ljava/util/List;Lcom/microsoft/xbox/service/model/clubs/ClubModel$RosterChangeCallback;)V

    .line 125
    :cond_0
    return-void
.end method
