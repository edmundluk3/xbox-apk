.class public Lxbox/smartglass/CommonData;
.super LMicrosoft/Telemetry/Domain;
.source "CommonData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lxbox/smartglass/CommonData$Schema;
    }
.end annotation


# instance fields
.field private accessibilitySettings:Ljava/lang/String;

.field private additionalInfo:Ljava/lang/String;

.field private appSessionId:Ljava/lang/String;

.field private attributionId:Ljava/lang/String;

.field private clientLanguage:Ljava/lang/String;

.field private consoleId:Ljava/lang/String;

.field private deviceFormFactor:I

.field private deviceModel:Ljava/lang/String;

.field private eventVersion:Ljava/lang/String;

.field private memoryKb:J

.field private network:I

.field private release:I

.field private sandboxId:Ljava/lang/String;

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 622
    invoke-direct {p0}, LMicrosoft/Telemetry/Domain;-><init>()V

    .line 624
    return-void
.end method

.method public static getRuntimeSchema()Lcom/microsoft/bond/SchemaDef;
    .locals 1

    .prologue
    .line 617
    sget-object v0, Lxbox/smartglass/CommonData$Schema;->schemaDef:Lcom/microsoft/bond/SchemaDef;

    return-object v0
.end method


# virtual methods
.method public clone()Lcom/microsoft/bond/BondSerializable;
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 24
    invoke-virtual {p0}, Lxbox/smartglass/CommonData;->clone()Lcom/microsoft/bond/BondSerializable;

    move-result-object v0

    return-object v0
.end method

.method public createInstance(Lcom/microsoft/bond/StructDef;)Lcom/microsoft/bond/BondMirror;
    .locals 1
    .param p1, "structDef"    # Lcom/microsoft/bond/StructDef;

    .prologue
    .line 601
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getAccessibilitySettings()Ljava/lang/String;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lxbox/smartglass/CommonData;->accessibilitySettings:Ljava/lang/String;

    return-object v0
.end method

.method public final getAdditionalInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lxbox/smartglass/CommonData;->additionalInfo:Ljava/lang/String;

    return-object v0
.end method

.method public final getAppSessionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lxbox/smartglass/CommonData;->appSessionId:Ljava/lang/String;

    return-object v0
.end method

.method public final getAttributionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lxbox/smartglass/CommonData;->attributionId:Ljava/lang/String;

    return-object v0
.end method

.method public final getClientLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lxbox/smartglass/CommonData;->clientLanguage:Ljava/lang/String;

    return-object v0
.end method

.method public final getConsoleId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lxbox/smartglass/CommonData;->consoleId:Ljava/lang/String;

    return-object v0
.end method

.method public final getDeviceFormFactor()I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lxbox/smartglass/CommonData;->deviceFormFactor:I

    return v0
.end method

.method public final getDeviceModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lxbox/smartglass/CommonData;->deviceModel:Ljava/lang/String;

    return-object v0
.end method

.method public final getEventVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lxbox/smartglass/CommonData;->eventVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getField(Lcom/microsoft/bond/FieldDef;)Ljava/lang/Object;
    .locals 2
    .param p1, "fieldDef"    # Lcom/microsoft/bond/FieldDef;

    .prologue
    .line 511
    invoke-virtual {p1}, Lcom/microsoft/bond/FieldDef;->getId()S

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 541
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 513
    :sswitch_0
    iget-object v0, p0, Lxbox/smartglass/CommonData;->eventVersion:Ljava/lang/String;

    goto :goto_0

    .line 515
    :sswitch_1
    iget-object v0, p0, Lxbox/smartglass/CommonData;->deviceModel:Ljava/lang/String;

    goto :goto_0

    .line 517
    :sswitch_2
    iget v0, p0, Lxbox/smartglass/CommonData;->deviceFormFactor:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 519
    :sswitch_3
    iget v0, p0, Lxbox/smartglass/CommonData;->release:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 521
    :sswitch_4
    iget-object v0, p0, Lxbox/smartglass/CommonData;->clientLanguage:Ljava/lang/String;

    goto :goto_0

    .line 523
    :sswitch_5
    iget v0, p0, Lxbox/smartglass/CommonData;->network:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 525
    :sswitch_6
    iget-object v0, p0, Lxbox/smartglass/CommonData;->sandboxId:Ljava/lang/String;

    goto :goto_0

    .line 527
    :sswitch_7
    iget-object v0, p0, Lxbox/smartglass/CommonData;->consoleId:Ljava/lang/String;

    goto :goto_0

    .line 529
    :sswitch_8
    iget-object v0, p0, Lxbox/smartglass/CommonData;->xuid:Ljava/lang/String;

    goto :goto_0

    .line 531
    :sswitch_9
    iget-object v0, p0, Lxbox/smartglass/CommonData;->appSessionId:Ljava/lang/String;

    goto :goto_0

    .line 533
    :sswitch_a
    iget-wide v0, p0, Lxbox/smartglass/CommonData;->memoryKb:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 535
    :sswitch_b
    iget-object v0, p0, Lxbox/smartglass/CommonData;->additionalInfo:Ljava/lang/String;

    goto :goto_0

    .line 537
    :sswitch_c
    iget-object v0, p0, Lxbox/smartglass/CommonData;->accessibilitySettings:Ljava/lang/String;

    goto :goto_0

    .line 539
    :sswitch_d
    iget-object v0, p0, Lxbox/smartglass/CommonData;->attributionId:Ljava/lang/String;

    goto :goto_0

    .line 511
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
        0x28 -> :sswitch_3
        0x32 -> :sswitch_4
        0x3c -> :sswitch_5
        0x46 -> :sswitch_6
        0x50 -> :sswitch_7
        0x5a -> :sswitch_8
        0x64 -> :sswitch_9
        0x6e -> :sswitch_a
        0x78 -> :sswitch_b
        0x82 -> :sswitch_c
        0x8c -> :sswitch_d
    .end sparse-switch
.end method

.method public final getMemoryKb()J
    .locals 2

    .prologue
    .line 219
    iget-wide v0, p0, Lxbox/smartglass/CommonData;->memoryKb:J

    return-wide v0
.end method

.method public final getNetwork()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lxbox/smartglass/CommonData;->network:I

    return v0
.end method

.method public final getRelease()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lxbox/smartglass/CommonData;->release:I

    return v0
.end method

.method public final getSandboxId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lxbox/smartglass/CommonData;->sandboxId:Ljava/lang/String;

    return-object v0
.end method

.method public getSchema()Lcom/microsoft/bond/SchemaDef;
    .locals 1

    .prologue
    .line 609
    invoke-static {}, Lxbox/smartglass/CommonData;->getRuntimeSchema()Lcom/microsoft/bond/SchemaDef;

    move-result-object v0

    return-object v0
.end method

.method public final getXuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lxbox/smartglass/CommonData;->xuid:Ljava/lang/String;

    return-object v0
.end method

.method public marshal(Lcom/microsoft/bond/ProtocolWriter;)V
    .locals 0
    .param p1, "writer"    # Lcom/microsoft/bond/ProtocolWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 908
    invoke-static {p0, p1}, Lcom/microsoft/bond/internal/Marshaler;->marshal(Lcom/microsoft/bond/BondSerializable;Lcom/microsoft/bond/ProtocolWriter;)V

    .line 909
    return-void
.end method

.method public memberwiseCompare(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 995
    if-nez p1, :cond_1

    .line 1001
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 999
    check-cast v0, Lxbox/smartglass/CommonData;

    .line 1001
    .local v0, "that":Lxbox/smartglass/CommonData;
    invoke-virtual {p0, v0}, Lxbox/smartglass/CommonData;->memberwiseCompareQuick(Lxbox/smartglass/CommonData;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v0}, Lxbox/smartglass/CommonData;->memberwiseCompareDeep(Lxbox/smartglass/CommonData;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected memberwiseCompareDeep(Lxbox/smartglass/CommonData;)Z
    .locals 5
    .param p1, "that"    # Lxbox/smartglass/CommonData;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1035
    const/4 v0, 0x1

    .line 1036
    .local v0, "equals":Z
    if-eqz v0, :cond_a

    invoke-super {p0, p1}, LMicrosoft/Telemetry/Domain;->memberwiseCompareDeep(LMicrosoft/Telemetry/Domain;)Z

    move-result v3

    if-eqz v3, :cond_a

    move v0, v1

    .line 1037
    :goto_0
    if-eqz v0, :cond_c

    iget-object v3, p0, Lxbox/smartglass/CommonData;->eventVersion:Ljava/lang/String;

    if-nez v3, :cond_b

    :cond_0
    move v0, v1

    .line 1038
    :goto_1
    if-eqz v0, :cond_e

    iget-object v3, p0, Lxbox/smartglass/CommonData;->deviceModel:Ljava/lang/String;

    if-nez v3, :cond_d

    :cond_1
    move v0, v1

    .line 1039
    :goto_2
    if-eqz v0, :cond_10

    iget-object v3, p0, Lxbox/smartglass/CommonData;->clientLanguage:Ljava/lang/String;

    if-nez v3, :cond_f

    :cond_2
    move v0, v1

    .line 1040
    :goto_3
    if-eqz v0, :cond_12

    iget-object v3, p0, Lxbox/smartglass/CommonData;->sandboxId:Ljava/lang/String;

    if-nez v3, :cond_11

    :cond_3
    move v0, v1

    .line 1041
    :goto_4
    if-eqz v0, :cond_14

    iget-object v3, p0, Lxbox/smartglass/CommonData;->consoleId:Ljava/lang/String;

    if-nez v3, :cond_13

    :cond_4
    move v0, v1

    .line 1042
    :goto_5
    if-eqz v0, :cond_16

    iget-object v3, p0, Lxbox/smartglass/CommonData;->xuid:Ljava/lang/String;

    if-nez v3, :cond_15

    :cond_5
    move v0, v1

    .line 1043
    :goto_6
    if-eqz v0, :cond_18

    iget-object v3, p0, Lxbox/smartglass/CommonData;->appSessionId:Ljava/lang/String;

    if-nez v3, :cond_17

    :cond_6
    move v0, v1

    .line 1044
    :goto_7
    if-eqz v0, :cond_1a

    iget-object v3, p0, Lxbox/smartglass/CommonData;->additionalInfo:Ljava/lang/String;

    if-nez v3, :cond_19

    :cond_7
    move v0, v1

    .line 1045
    :goto_8
    if-eqz v0, :cond_1c

    iget-object v3, p0, Lxbox/smartglass/CommonData;->accessibilitySettings:Ljava/lang/String;

    if-nez v3, :cond_1b

    :cond_8
    move v0, v1

    .line 1046
    :goto_9
    if-eqz v0, :cond_1e

    iget-object v3, p0, Lxbox/smartglass/CommonData;->attributionId:Ljava/lang/String;

    if-nez v3, :cond_1d

    :cond_9
    move v0, v1

    .line 1047
    :goto_a
    return v0

    :cond_a
    move v0, v2

    .line 1036
    goto :goto_0

    .line 1037
    :cond_b
    iget-object v3, p0, Lxbox/smartglass/CommonData;->eventVersion:Ljava/lang/String;

    iget-object v4, p1, Lxbox/smartglass/CommonData;->eventVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_c
    move v0, v2

    goto :goto_1

    .line 1038
    :cond_d
    iget-object v3, p0, Lxbox/smartglass/CommonData;->deviceModel:Ljava/lang/String;

    iget-object v4, p1, Lxbox/smartglass/CommonData;->deviceModel:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_e
    move v0, v2

    goto :goto_2

    .line 1039
    :cond_f
    iget-object v3, p0, Lxbox/smartglass/CommonData;->clientLanguage:Ljava/lang/String;

    iget-object v4, p1, Lxbox/smartglass/CommonData;->clientLanguage:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_10
    move v0, v2

    goto :goto_3

    .line 1040
    :cond_11
    iget-object v3, p0, Lxbox/smartglass/CommonData;->sandboxId:Ljava/lang/String;

    iget-object v4, p1, Lxbox/smartglass/CommonData;->sandboxId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    :cond_12
    move v0, v2

    goto :goto_4

    .line 1041
    :cond_13
    iget-object v3, p0, Lxbox/smartglass/CommonData;->consoleId:Ljava/lang/String;

    iget-object v4, p1, Lxbox/smartglass/CommonData;->consoleId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    :cond_14
    move v0, v2

    goto :goto_5

    .line 1042
    :cond_15
    iget-object v3, p0, Lxbox/smartglass/CommonData;->xuid:Ljava/lang/String;

    iget-object v4, p1, Lxbox/smartglass/CommonData;->xuid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    :cond_16
    move v0, v2

    goto :goto_6

    .line 1043
    :cond_17
    iget-object v3, p0, Lxbox/smartglass/CommonData;->appSessionId:Ljava/lang/String;

    iget-object v4, p1, Lxbox/smartglass/CommonData;->appSessionId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    :cond_18
    move v0, v2

    goto :goto_7

    .line 1044
    :cond_19
    iget-object v3, p0, Lxbox/smartglass/CommonData;->additionalInfo:Ljava/lang/String;

    iget-object v4, p1, Lxbox/smartglass/CommonData;->additionalInfo:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    :cond_1a
    move v0, v2

    goto :goto_8

    .line 1045
    :cond_1b
    iget-object v3, p0, Lxbox/smartglass/CommonData;->accessibilitySettings:Ljava/lang/String;

    iget-object v4, p1, Lxbox/smartglass/CommonData;->accessibilitySettings:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    :cond_1c
    move v0, v2

    goto :goto_9

    .line 1046
    :cond_1d
    iget-object v3, p0, Lxbox/smartglass/CommonData;->attributionId:Ljava/lang/String;

    iget-object v4, p1, Lxbox/smartglass/CommonData;->attributionId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    :cond_1e
    move v0, v2

    goto :goto_a
.end method

.method protected memberwiseCompareQuick(Lxbox/smartglass/CommonData;)Z
    .locals 8
    .param p1, "that"    # Lxbox/smartglass/CommonData;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1005
    const/4 v0, 0x1

    .line 1006
    .local v0, "equals":Z
    if-eqz v0, :cond_a

    invoke-super {p0, p1}, LMicrosoft/Telemetry/Domain;->memberwiseCompareQuick(LMicrosoft/Telemetry/Domain;)Z

    move-result v1

    if-eqz v1, :cond_a

    move v0, v2

    .line 1007
    :goto_0
    if-eqz v0, :cond_d

    iget-object v1, p0, Lxbox/smartglass/CommonData;->eventVersion:Ljava/lang/String;

    if-nez v1, :cond_b

    move v1, v2

    :goto_1
    iget-object v4, p1, Lxbox/smartglass/CommonData;->eventVersion:Ljava/lang/String;

    if-nez v4, :cond_c

    move v4, v2

    :goto_2
    if-ne v1, v4, :cond_d

    move v0, v2

    .line 1008
    :goto_3
    if-eqz v0, :cond_f

    iget-object v1, p0, Lxbox/smartglass/CommonData;->eventVersion:Ljava/lang/String;

    if-nez v1, :cond_e

    :cond_0
    move v0, v2

    .line 1009
    :goto_4
    if-eqz v0, :cond_12

    iget-object v1, p0, Lxbox/smartglass/CommonData;->deviceModel:Ljava/lang/String;

    if-nez v1, :cond_10

    move v1, v2

    :goto_5
    iget-object v4, p1, Lxbox/smartglass/CommonData;->deviceModel:Ljava/lang/String;

    if-nez v4, :cond_11

    move v4, v2

    :goto_6
    if-ne v1, v4, :cond_12

    move v0, v2

    .line 1010
    :goto_7
    if-eqz v0, :cond_14

    iget-object v1, p0, Lxbox/smartglass/CommonData;->deviceModel:Ljava/lang/String;

    if-nez v1, :cond_13

    :cond_1
    move v0, v2

    .line 1011
    :goto_8
    if-eqz v0, :cond_15

    iget v1, p0, Lxbox/smartglass/CommonData;->deviceFormFactor:I

    iget v4, p1, Lxbox/smartglass/CommonData;->deviceFormFactor:I

    if-ne v1, v4, :cond_15

    move v0, v2

    .line 1012
    :goto_9
    if-eqz v0, :cond_16

    iget v1, p0, Lxbox/smartglass/CommonData;->release:I

    iget v4, p1, Lxbox/smartglass/CommonData;->release:I

    if-ne v1, v4, :cond_16

    move v0, v2

    .line 1013
    :goto_a
    if-eqz v0, :cond_19

    iget-object v1, p0, Lxbox/smartglass/CommonData;->clientLanguage:Ljava/lang/String;

    if-nez v1, :cond_17

    move v1, v2

    :goto_b
    iget-object v4, p1, Lxbox/smartglass/CommonData;->clientLanguage:Ljava/lang/String;

    if-nez v4, :cond_18

    move v4, v2

    :goto_c
    if-ne v1, v4, :cond_19

    move v0, v2

    .line 1014
    :goto_d
    if-eqz v0, :cond_1b

    iget-object v1, p0, Lxbox/smartglass/CommonData;->clientLanguage:Ljava/lang/String;

    if-nez v1, :cond_1a

    :cond_2
    move v0, v2

    .line 1015
    :goto_e
    if-eqz v0, :cond_1c

    iget v1, p0, Lxbox/smartglass/CommonData;->network:I

    iget v4, p1, Lxbox/smartglass/CommonData;->network:I

    if-ne v1, v4, :cond_1c

    move v0, v2

    .line 1016
    :goto_f
    if-eqz v0, :cond_1f

    iget-object v1, p0, Lxbox/smartglass/CommonData;->sandboxId:Ljava/lang/String;

    if-nez v1, :cond_1d

    move v1, v2

    :goto_10
    iget-object v4, p1, Lxbox/smartglass/CommonData;->sandboxId:Ljava/lang/String;

    if-nez v4, :cond_1e

    move v4, v2

    :goto_11
    if-ne v1, v4, :cond_1f

    move v0, v2

    .line 1017
    :goto_12
    if-eqz v0, :cond_21

    iget-object v1, p0, Lxbox/smartglass/CommonData;->sandboxId:Ljava/lang/String;

    if-nez v1, :cond_20

    :cond_3
    move v0, v2

    .line 1018
    :goto_13
    if-eqz v0, :cond_24

    iget-object v1, p0, Lxbox/smartglass/CommonData;->consoleId:Ljava/lang/String;

    if-nez v1, :cond_22

    move v1, v2

    :goto_14
    iget-object v4, p1, Lxbox/smartglass/CommonData;->consoleId:Ljava/lang/String;

    if-nez v4, :cond_23

    move v4, v2

    :goto_15
    if-ne v1, v4, :cond_24

    move v0, v2

    .line 1019
    :goto_16
    if-eqz v0, :cond_26

    iget-object v1, p0, Lxbox/smartglass/CommonData;->consoleId:Ljava/lang/String;

    if-nez v1, :cond_25

    :cond_4
    move v0, v2

    .line 1020
    :goto_17
    if-eqz v0, :cond_29

    iget-object v1, p0, Lxbox/smartglass/CommonData;->xuid:Ljava/lang/String;

    if-nez v1, :cond_27

    move v1, v2

    :goto_18
    iget-object v4, p1, Lxbox/smartglass/CommonData;->xuid:Ljava/lang/String;

    if-nez v4, :cond_28

    move v4, v2

    :goto_19
    if-ne v1, v4, :cond_29

    move v0, v2

    .line 1021
    :goto_1a
    if-eqz v0, :cond_2b

    iget-object v1, p0, Lxbox/smartglass/CommonData;->xuid:Ljava/lang/String;

    if-nez v1, :cond_2a

    :cond_5
    move v0, v2

    .line 1022
    :goto_1b
    if-eqz v0, :cond_2e

    iget-object v1, p0, Lxbox/smartglass/CommonData;->appSessionId:Ljava/lang/String;

    if-nez v1, :cond_2c

    move v1, v2

    :goto_1c
    iget-object v4, p1, Lxbox/smartglass/CommonData;->appSessionId:Ljava/lang/String;

    if-nez v4, :cond_2d

    move v4, v2

    :goto_1d
    if-ne v1, v4, :cond_2e

    move v0, v2

    .line 1023
    :goto_1e
    if-eqz v0, :cond_30

    iget-object v1, p0, Lxbox/smartglass/CommonData;->appSessionId:Ljava/lang/String;

    if-nez v1, :cond_2f

    :cond_6
    move v0, v2

    .line 1024
    :goto_1f
    if-eqz v0, :cond_31

    iget-wide v4, p0, Lxbox/smartglass/CommonData;->memoryKb:J

    iget-wide v6, p1, Lxbox/smartglass/CommonData;->memoryKb:J

    cmp-long v1, v4, v6

    if-nez v1, :cond_31

    move v0, v2

    .line 1025
    :goto_20
    if-eqz v0, :cond_34

    iget-object v1, p0, Lxbox/smartglass/CommonData;->additionalInfo:Ljava/lang/String;

    if-nez v1, :cond_32

    move v1, v2

    :goto_21
    iget-object v4, p1, Lxbox/smartglass/CommonData;->additionalInfo:Ljava/lang/String;

    if-nez v4, :cond_33

    move v4, v2

    :goto_22
    if-ne v1, v4, :cond_34

    move v0, v2

    .line 1026
    :goto_23
    if-eqz v0, :cond_36

    iget-object v1, p0, Lxbox/smartglass/CommonData;->additionalInfo:Ljava/lang/String;

    if-nez v1, :cond_35

    :cond_7
    move v0, v2

    .line 1027
    :goto_24
    if-eqz v0, :cond_39

    iget-object v1, p0, Lxbox/smartglass/CommonData;->accessibilitySettings:Ljava/lang/String;

    if-nez v1, :cond_37

    move v1, v2

    :goto_25
    iget-object v4, p1, Lxbox/smartglass/CommonData;->accessibilitySettings:Ljava/lang/String;

    if-nez v4, :cond_38

    move v4, v2

    :goto_26
    if-ne v1, v4, :cond_39

    move v0, v2

    .line 1028
    :goto_27
    if-eqz v0, :cond_3b

    iget-object v1, p0, Lxbox/smartglass/CommonData;->accessibilitySettings:Ljava/lang/String;

    if-nez v1, :cond_3a

    :cond_8
    move v0, v2

    .line 1029
    :goto_28
    if-eqz v0, :cond_3e

    iget-object v1, p0, Lxbox/smartglass/CommonData;->attributionId:Ljava/lang/String;

    if-nez v1, :cond_3c

    move v1, v2

    :goto_29
    iget-object v4, p1, Lxbox/smartglass/CommonData;->attributionId:Ljava/lang/String;

    if-nez v4, :cond_3d

    move v4, v2

    :goto_2a
    if-ne v1, v4, :cond_3e

    move v0, v2

    .line 1030
    :goto_2b
    if-eqz v0, :cond_40

    iget-object v1, p0, Lxbox/smartglass/CommonData;->attributionId:Ljava/lang/String;

    if-nez v1, :cond_3f

    :cond_9
    move v0, v2

    .line 1031
    :goto_2c
    return v0

    :cond_a
    move v0, v3

    .line 1006
    goto/16 :goto_0

    :cond_b
    move v1, v3

    .line 1007
    goto/16 :goto_1

    :cond_c
    move v4, v3

    goto/16 :goto_2

    :cond_d
    move v0, v3

    goto/16 :goto_3

    .line 1008
    :cond_e
    iget-object v1, p0, Lxbox/smartglass/CommonData;->eventVersion:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v4, p1, Lxbox/smartglass/CommonData;->eventVersion:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v1, v4, :cond_0

    :cond_f
    move v0, v3

    goto/16 :goto_4

    :cond_10
    move v1, v3

    .line 1009
    goto/16 :goto_5

    :cond_11
    move v4, v3

    goto/16 :goto_6

    :cond_12
    move v0, v3

    goto/16 :goto_7

    .line 1010
    :cond_13
    iget-object v1, p0, Lxbox/smartglass/CommonData;->deviceModel:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v4, p1, Lxbox/smartglass/CommonData;->deviceModel:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v1, v4, :cond_1

    :cond_14
    move v0, v3

    goto/16 :goto_8

    :cond_15
    move v0, v3

    .line 1011
    goto/16 :goto_9

    :cond_16
    move v0, v3

    .line 1012
    goto/16 :goto_a

    :cond_17
    move v1, v3

    .line 1013
    goto/16 :goto_b

    :cond_18
    move v4, v3

    goto/16 :goto_c

    :cond_19
    move v0, v3

    goto/16 :goto_d

    .line 1014
    :cond_1a
    iget-object v1, p0, Lxbox/smartglass/CommonData;->clientLanguage:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v4, p1, Lxbox/smartglass/CommonData;->clientLanguage:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v1, v4, :cond_2

    :cond_1b
    move v0, v3

    goto/16 :goto_e

    :cond_1c
    move v0, v3

    .line 1015
    goto/16 :goto_f

    :cond_1d
    move v1, v3

    .line 1016
    goto/16 :goto_10

    :cond_1e
    move v4, v3

    goto/16 :goto_11

    :cond_1f
    move v0, v3

    goto/16 :goto_12

    .line 1017
    :cond_20
    iget-object v1, p0, Lxbox/smartglass/CommonData;->sandboxId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v4, p1, Lxbox/smartglass/CommonData;->sandboxId:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v1, v4, :cond_3

    :cond_21
    move v0, v3

    goto/16 :goto_13

    :cond_22
    move v1, v3

    .line 1018
    goto/16 :goto_14

    :cond_23
    move v4, v3

    goto/16 :goto_15

    :cond_24
    move v0, v3

    goto/16 :goto_16

    .line 1019
    :cond_25
    iget-object v1, p0, Lxbox/smartglass/CommonData;->consoleId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v4, p1, Lxbox/smartglass/CommonData;->consoleId:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v1, v4, :cond_4

    :cond_26
    move v0, v3

    goto/16 :goto_17

    :cond_27
    move v1, v3

    .line 1020
    goto/16 :goto_18

    :cond_28
    move v4, v3

    goto/16 :goto_19

    :cond_29
    move v0, v3

    goto/16 :goto_1a

    .line 1021
    :cond_2a
    iget-object v1, p0, Lxbox/smartglass/CommonData;->xuid:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v4, p1, Lxbox/smartglass/CommonData;->xuid:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v1, v4, :cond_5

    :cond_2b
    move v0, v3

    goto/16 :goto_1b

    :cond_2c
    move v1, v3

    .line 1022
    goto/16 :goto_1c

    :cond_2d
    move v4, v3

    goto/16 :goto_1d

    :cond_2e
    move v0, v3

    goto/16 :goto_1e

    .line 1023
    :cond_2f
    iget-object v1, p0, Lxbox/smartglass/CommonData;->appSessionId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v4, p1, Lxbox/smartglass/CommonData;->appSessionId:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v1, v4, :cond_6

    :cond_30
    move v0, v3

    goto/16 :goto_1f

    :cond_31
    move v0, v3

    .line 1024
    goto/16 :goto_20

    :cond_32
    move v1, v3

    .line 1025
    goto/16 :goto_21

    :cond_33
    move v4, v3

    goto/16 :goto_22

    :cond_34
    move v0, v3

    goto/16 :goto_23

    .line 1026
    :cond_35
    iget-object v1, p0, Lxbox/smartglass/CommonData;->additionalInfo:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v4, p1, Lxbox/smartglass/CommonData;->additionalInfo:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v1, v4, :cond_7

    :cond_36
    move v0, v3

    goto/16 :goto_24

    :cond_37
    move v1, v3

    .line 1027
    goto/16 :goto_25

    :cond_38
    move v4, v3

    goto/16 :goto_26

    :cond_39
    move v0, v3

    goto/16 :goto_27

    .line 1028
    :cond_3a
    iget-object v1, p0, Lxbox/smartglass/CommonData;->accessibilitySettings:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v4, p1, Lxbox/smartglass/CommonData;->accessibilitySettings:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v1, v4, :cond_8

    :cond_3b
    move v0, v3

    goto/16 :goto_28

    :cond_3c
    move v1, v3

    .line 1029
    goto/16 :goto_29

    :cond_3d
    move v4, v3

    goto/16 :goto_2a

    :cond_3e
    move v0, v3

    goto/16 :goto_2b

    .line 1030
    :cond_3f
    iget-object v1, p0, Lxbox/smartglass/CommonData;->attributionId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v4, p1, Lxbox/smartglass/CommonData;->attributionId:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v1, v4, :cond_9

    :cond_40
    move v0, v3

    goto/16 :goto_2c
.end method

.method public read(Lcom/microsoft/bond/ProtocolReader;)V
    .locals 0
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 669
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readBegin()V

    .line 670
    invoke-virtual {p0, p1}, Lxbox/smartglass/CommonData;->readNested(Lcom/microsoft/bond/ProtocolReader;)V

    .line 671
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readEnd()V

    .line 672
    return-void
.end method

.method public read(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondSerializable;)V
    .locals 0
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .param p2, "schema"    # Lcom/microsoft/bond/BondSerializable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 690
    return-void
.end method

.method public readNested(Lcom/microsoft/bond/ProtocolReader;)V
    .locals 2
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 678
    sget-object v0, Lcom/microsoft/bond/ProtocolCapability;->TAGGED:Lcom/microsoft/bond/ProtocolCapability;

    invoke-virtual {p1, v0}, Lcom/microsoft/bond/ProtocolReader;->hasCapability(Lcom/microsoft/bond/ProtocolCapability;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 679
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/CommonData;->readUntagged(Lcom/microsoft/bond/ProtocolReader;Z)V

    .line 683
    :cond_0
    :goto_0
    return-void

    .line 680
    :cond_1
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/CommonData;->readTagged(Lcom/microsoft/bond/ProtocolReader;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 681
    invoke-static {p1}, Lcom/microsoft/bond/internal/ReadHelper;->skipPartialStruct(Lcom/microsoft/bond/ProtocolReader;)V

    goto :goto_0
.end method

.method protected readTagged(Lcom/microsoft/bond/ProtocolReader;Z)Z
    .locals 6
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .param p2, "isBase"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 815
    invoke-virtual {p1, p2}, Lcom/microsoft/bond/ProtocolReader;->readStructBegin(Z)V

    .line 817
    invoke-super {p0, p1, v1}, LMicrosoft/Telemetry/Domain;->readTagged(Lcom/microsoft/bond/ProtocolReader;Z)Z

    move-result v3

    if-nez v3, :cond_0

    .line 900
    :goto_0
    return v2

    .line 834
    .local v0, "fieldTag":Lcom/microsoft/bond/ProtocolReader$FieldTag;
    :sswitch_0
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readWString(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lxbox/smartglass/CommonData;->eventVersion:Ljava/lang/String;

    .line 894
    :goto_1
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldEnd()V

    .line 824
    .end local v0    # "fieldTag":Lcom/microsoft/bond/ProtocolReader$FieldTag;
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldBegin()Lcom/microsoft/bond/ProtocolReader$FieldTag;

    move-result-object v0

    .line 826
    .restart local v0    # "fieldTag":Lcom/microsoft/bond/ProtocolReader$FieldTag;
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    sget-object v4, Lcom/microsoft/bond/BondDataType;->BT_STOP:Lcom/microsoft/bond/BondDataType;

    if-eq v3, v4, :cond_1

    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    sget-object v4, Lcom/microsoft/bond/BondDataType;->BT_STOP_BASE:Lcom/microsoft/bond/BondDataType;

    if-ne v3, v4, :cond_3

    .line 828
    :cond_1
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    sget-object v4, Lcom/microsoft/bond/BondDataType;->BT_STOP_BASE:Lcom/microsoft/bond/BondDataType;

    if-ne v3, v4, :cond_2

    .line 897
    .local v1, "isPartial":Z
    :goto_2
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readStructEnd()V

    move v2, v1

    .line 900
    goto :goto_0

    .end local v1    # "isPartial":Z
    :cond_2
    move v1, v2

    .line 828
    goto :goto_2

    .line 832
    :cond_3
    iget v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->id:I

    sparse-switch v3, :sswitch_data_0

    .line 890
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-virtual {p1, v3}, Lcom/microsoft/bond/ProtocolReader;->skip(Lcom/microsoft/bond/BondDataType;)V

    goto :goto_1

    .line 838
    :sswitch_1
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readWString(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lxbox/smartglass/CommonData;->deviceModel:Ljava/lang/String;

    goto :goto_1

    .line 842
    :sswitch_2
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readUInt32(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)I

    move-result v3

    iput v3, p0, Lxbox/smartglass/CommonData;->deviceFormFactor:I

    goto :goto_1

    .line 846
    :sswitch_3
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readUInt32(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)I

    move-result v3

    iput v3, p0, Lxbox/smartglass/CommonData;->release:I

    goto :goto_1

    .line 850
    :sswitch_4
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readWString(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lxbox/smartglass/CommonData;->clientLanguage:Ljava/lang/String;

    goto :goto_1

    .line 854
    :sswitch_5
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readUInt32(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)I

    move-result v3

    iput v3, p0, Lxbox/smartglass/CommonData;->network:I

    goto :goto_1

    .line 858
    :sswitch_6
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readWString(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lxbox/smartglass/CommonData;->sandboxId:Ljava/lang/String;

    goto :goto_1

    .line 862
    :sswitch_7
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readWString(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lxbox/smartglass/CommonData;->consoleId:Ljava/lang/String;

    goto :goto_1

    .line 866
    :sswitch_8
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readWString(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lxbox/smartglass/CommonData;->xuid:Ljava/lang/String;

    goto :goto_1

    .line 870
    :sswitch_9
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readWString(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lxbox/smartglass/CommonData;->appSessionId:Ljava/lang/String;

    goto :goto_1

    .line 874
    :sswitch_a
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readUInt64(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)J

    move-result-wide v4

    iput-wide v4, p0, Lxbox/smartglass/CommonData;->memoryKb:J

    goto/16 :goto_1

    .line 878
    :sswitch_b
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readWString(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lxbox/smartglass/CommonData;->additionalInfo:Ljava/lang/String;

    goto/16 :goto_1

    .line 882
    :sswitch_c
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readWString(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lxbox/smartglass/CommonData;->accessibilitySettings:Ljava/lang/String;

    goto/16 :goto_1

    .line 886
    :sswitch_d
    iget-object v3, v0, Lcom/microsoft/bond/ProtocolReader$FieldTag;->type:Lcom/microsoft/bond/BondDataType;

    invoke-static {p1, v3}, Lcom/microsoft/bond/internal/ReadHelper;->readWString(Lcom/microsoft/bond/ProtocolReader;Lcom/microsoft/bond/BondDataType;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lxbox/smartglass/CommonData;->attributionId:Ljava/lang/String;

    goto/16 :goto_1

    .line 832
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
        0x28 -> :sswitch_3
        0x32 -> :sswitch_4
        0x3c -> :sswitch_5
        0x46 -> :sswitch_6
        0x50 -> :sswitch_7
        0x5a -> :sswitch_8
        0x64 -> :sswitch_9
        0x6e -> :sswitch_a
        0x78 -> :sswitch_b
        0x82 -> :sswitch_c
        0x8c -> :sswitch_d
    .end sparse-switch
.end method

.method protected readUntagged(Lcom/microsoft/bond/ProtocolReader;Z)V
    .locals 4
    .param p1, "reader"    # Lcom/microsoft/bond/ProtocolReader;
    .param p2, "isBase"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 693
    sget-object v1, Lcom/microsoft/bond/ProtocolCapability;->CAN_OMIT_FIELDS:Lcom/microsoft/bond/ProtocolCapability;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolReader;->hasCapability(Lcom/microsoft/bond/ProtocolCapability;)Z

    move-result v0

    .line 695
    .local v0, "canOmitFields":Z
    invoke-virtual {p1, p2}, Lcom/microsoft/bond/ProtocolReader;->readStructBegin(Z)V

    .line 696
    const/4 v1, 0x1

    invoke-super {p0, p1, v1}, LMicrosoft/Telemetry/Domain;->readUntagged(Lcom/microsoft/bond/ProtocolReader;Z)V

    .line 698
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_1

    .line 699
    :cond_0
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readWString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lxbox/smartglass/CommonData;->eventVersion:Ljava/lang/String;

    .line 706
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_3

    .line 707
    :cond_2
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readWString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lxbox/smartglass/CommonData;->deviceModel:Ljava/lang/String;

    .line 714
    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_5

    .line 715
    :cond_4
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readUInt32()I

    move-result v1

    iput v1, p0, Lxbox/smartglass/CommonData;->deviceFormFactor:I

    .line 722
    :cond_5
    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_7

    .line 723
    :cond_6
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readUInt32()I

    move-result v1

    iput v1, p0, Lxbox/smartglass/CommonData;->release:I

    .line 730
    :cond_7
    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_9

    .line 731
    :cond_8
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readWString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lxbox/smartglass/CommonData;->clientLanguage:Ljava/lang/String;

    .line 738
    :cond_9
    if-eqz v0, :cond_a

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_b

    .line 739
    :cond_a
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readUInt32()I

    move-result v1

    iput v1, p0, Lxbox/smartglass/CommonData;->network:I

    .line 746
    :cond_b
    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_d

    .line 747
    :cond_c
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readWString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lxbox/smartglass/CommonData;->sandboxId:Ljava/lang/String;

    .line 754
    :cond_d
    if-eqz v0, :cond_e

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_f

    .line 755
    :cond_e
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readWString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lxbox/smartglass/CommonData;->consoleId:Ljava/lang/String;

    .line 762
    :cond_f
    if-eqz v0, :cond_10

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_11

    .line 763
    :cond_10
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readWString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lxbox/smartglass/CommonData;->xuid:Ljava/lang/String;

    .line 770
    :cond_11
    if-eqz v0, :cond_12

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_13

    .line 771
    :cond_12
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readWString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lxbox/smartglass/CommonData;->appSessionId:Ljava/lang/String;

    .line 778
    :cond_13
    if-eqz v0, :cond_14

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_15

    .line 779
    :cond_14
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lxbox/smartglass/CommonData;->memoryKb:J

    .line 786
    :cond_15
    if-eqz v0, :cond_16

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_17

    .line 787
    :cond_16
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readWString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lxbox/smartglass/CommonData;->additionalInfo:Ljava/lang/String;

    .line 794
    :cond_17
    if-eqz v0, :cond_18

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_19

    .line 795
    :cond_18
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readWString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lxbox/smartglass/CommonData;->accessibilitySettings:Ljava/lang/String;

    .line 802
    :cond_19
    if-eqz v0, :cond_1a

    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readFieldOmitted()Z

    move-result v1

    if-nez v1, :cond_1b

    .line 803
    :cond_1a
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readWString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lxbox/smartglass/CommonData;->attributionId:Ljava/lang/String;

    .line 809
    :cond_1b
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolReader;->readStructEnd()V

    .line 810
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 630
    const-string v0, "CommonData"

    const-string v1, "xbox.smartglass.CommonData"

    invoke-virtual {p0, v0, v1}, Lxbox/smartglass/CommonData;->reset(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    return-void
.end method

.method protected reset(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "qualifiedName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 634
    invoke-super {p0, p1, p2}, LMicrosoft/Telemetry/Domain;->reset(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    const-string v0, ""

    iput-object v0, p0, Lxbox/smartglass/CommonData;->eventVersion:Ljava/lang/String;

    .line 636
    const-string v0, ""

    iput-object v0, p0, Lxbox/smartglass/CommonData;->deviceModel:Ljava/lang/String;

    .line 637
    iput v1, p0, Lxbox/smartglass/CommonData;->deviceFormFactor:I

    .line 638
    iput v1, p0, Lxbox/smartglass/CommonData;->release:I

    .line 639
    const-string v0, ""

    iput-object v0, p0, Lxbox/smartglass/CommonData;->clientLanguage:Ljava/lang/String;

    .line 640
    iput v1, p0, Lxbox/smartglass/CommonData;->network:I

    .line 641
    const-string v0, ""

    iput-object v0, p0, Lxbox/smartglass/CommonData;->sandboxId:Ljava/lang/String;

    .line 642
    const-string v0, ""

    iput-object v0, p0, Lxbox/smartglass/CommonData;->consoleId:Ljava/lang/String;

    .line 643
    const-string v0, ""

    iput-object v0, p0, Lxbox/smartglass/CommonData;->xuid:Ljava/lang/String;

    .line 644
    const-string v0, ""

    iput-object v0, p0, Lxbox/smartglass/CommonData;->appSessionId:Ljava/lang/String;

    .line 645
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lxbox/smartglass/CommonData;->memoryKb:J

    .line 646
    const-string v0, ""

    iput-object v0, p0, Lxbox/smartglass/CommonData;->additionalInfo:Ljava/lang/String;

    .line 647
    const-string v0, ""

    iput-object v0, p0, Lxbox/smartglass/CommonData;->accessibilitySettings:Ljava/lang/String;

    .line 648
    const-string v0, ""

    iput-object v0, p0, Lxbox/smartglass/CommonData;->attributionId:Ljava/lang/String;

    .line 649
    return-void
.end method

.method public final setAccessibilitySettings(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 254
    iput-object p1, p0, Lxbox/smartglass/CommonData;->accessibilitySettings:Ljava/lang/String;

    .line 255
    return-void
.end method

.method public final setAdditionalInfo(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 240
    iput-object p1, p0, Lxbox/smartglass/CommonData;->additionalInfo:Ljava/lang/String;

    .line 241
    return-void
.end method

.method public final setAppSessionId(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 212
    iput-object p1, p0, Lxbox/smartglass/CommonData;->appSessionId:Ljava/lang/String;

    .line 213
    return-void
.end method

.method public final setAttributionId(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 268
    iput-object p1, p0, Lxbox/smartglass/CommonData;->attributionId:Ljava/lang/String;

    .line 269
    return-void
.end method

.method public final setClientLanguage(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 142
    iput-object p1, p0, Lxbox/smartglass/CommonData;->clientLanguage:Ljava/lang/String;

    .line 143
    return-void
.end method

.method public final setConsoleId(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 184
    iput-object p1, p0, Lxbox/smartglass/CommonData;->consoleId:Ljava/lang/String;

    .line 185
    return-void
.end method

.method public final setDeviceFormFactor(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 114
    iput p1, p0, Lxbox/smartglass/CommonData;->deviceFormFactor:I

    .line 115
    return-void
.end method

.method public final setDeviceModel(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 100
    iput-object p1, p0, Lxbox/smartglass/CommonData;->deviceModel:Ljava/lang/String;

    .line 101
    return-void
.end method

.method public final setEventVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 86
    iput-object p1, p0, Lxbox/smartglass/CommonData;->eventVersion:Ljava/lang/String;

    .line 87
    return-void
.end method

.method public setField(Lcom/microsoft/bond/FieldDef;Ljava/lang/Object;)V
    .locals 2
    .param p1, "fieldDef"    # Lcom/microsoft/bond/FieldDef;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 550
    invoke-virtual {p1}, Lcom/microsoft/bond/FieldDef;->getId()S

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 594
    .end local p2    # "value":Ljava/lang/Object;
    :goto_0
    return-void

    .line 552
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_0
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lxbox/smartglass/CommonData;->eventVersion:Ljava/lang/String;

    goto :goto_0

    .line 555
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_1
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lxbox/smartglass/CommonData;->deviceModel:Ljava/lang/String;

    goto :goto_0

    .line 558
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_2
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lxbox/smartglass/CommonData;->deviceFormFactor:I

    goto :goto_0

    .line 561
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_3
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lxbox/smartglass/CommonData;->release:I

    goto :goto_0

    .line 564
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_4
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lxbox/smartglass/CommonData;->clientLanguage:Ljava/lang/String;

    goto :goto_0

    .line 567
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_5
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lxbox/smartglass/CommonData;->network:I

    goto :goto_0

    .line 570
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_6
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lxbox/smartglass/CommonData;->sandboxId:Ljava/lang/String;

    goto :goto_0

    .line 573
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_7
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lxbox/smartglass/CommonData;->consoleId:Ljava/lang/String;

    goto :goto_0

    .line 576
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_8
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lxbox/smartglass/CommonData;->xuid:Ljava/lang/String;

    goto :goto_0

    .line 579
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_9
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lxbox/smartglass/CommonData;->appSessionId:Ljava/lang/String;

    goto :goto_0

    .line 582
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_a
    check-cast p2, Ljava/lang/Long;

    .end local p2    # "value":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lxbox/smartglass/CommonData;->memoryKb:J

    goto :goto_0

    .line 585
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_b
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lxbox/smartglass/CommonData;->additionalInfo:Ljava/lang/String;

    goto :goto_0

    .line 588
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_c
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lxbox/smartglass/CommonData;->accessibilitySettings:Ljava/lang/String;

    goto :goto_0

    .line 591
    .restart local p2    # "value":Ljava/lang/Object;
    :sswitch_d
    check-cast p2, Ljava/lang/String;

    .end local p2    # "value":Ljava/lang/Object;
    iput-object p2, p0, Lxbox/smartglass/CommonData;->attributionId:Ljava/lang/String;

    goto :goto_0

    .line 550
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
        0x1e -> :sswitch_2
        0x28 -> :sswitch_3
        0x32 -> :sswitch_4
        0x3c -> :sswitch_5
        0x46 -> :sswitch_6
        0x50 -> :sswitch_7
        0x5a -> :sswitch_8
        0x64 -> :sswitch_9
        0x6e -> :sswitch_a
        0x78 -> :sswitch_b
        0x82 -> :sswitch_c
        0x8c -> :sswitch_d
    .end sparse-switch
.end method

.method public final setMemoryKb(J)V
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 226
    iput-wide p1, p0, Lxbox/smartglass/CommonData;->memoryKb:J

    .line 227
    return-void
.end method

.method public final setNetwork(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 156
    iput p1, p0, Lxbox/smartglass/CommonData;->network:I

    .line 157
    return-void
.end method

.method public final setRelease(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 128
    iput p1, p0, Lxbox/smartglass/CommonData;->release:I

    .line 129
    return-void
.end method

.method public final setSandboxId(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 170
    iput-object p1, p0, Lxbox/smartglass/CommonData;->sandboxId:Ljava/lang/String;

    .line 171
    return-void
.end method

.method public final setXuid(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 198
    iput-object p1, p0, Lxbox/smartglass/CommonData;->xuid:Ljava/lang/String;

    .line 199
    return-void
.end method

.method public unmarshal(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 655
    invoke-static {p1, p0}, Lcom/microsoft/bond/internal/Marshaler;->unmarshal(Ljava/io/InputStream;Lcom/microsoft/bond/BondSerializable;)V

    .line 656
    return-void
.end method

.method public unmarshal(Ljava/io/InputStream;Lcom/microsoft/bond/BondSerializable;)V
    .locals 0
    .param p1, "input"    # Ljava/io/InputStream;
    .param p2, "schema"    # Lcom/microsoft/bond/BondSerializable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 662
    check-cast p2, Lcom/microsoft/bond/SchemaDef;

    .end local p2    # "schema":Lcom/microsoft/bond/BondSerializable;
    invoke-static {p1, p2, p0}, Lcom/microsoft/bond/internal/Marshaler;->unmarshal(Ljava/io/InputStream;Lcom/microsoft/bond/BondSerializable;Lcom/microsoft/bond/BondSerializable;)V

    .line 663
    return-void
.end method

.method public write(Lcom/microsoft/bond/ProtocolWriter;)V
    .locals 2
    .param p1, "writer"    # Lcom/microsoft/bond/ProtocolWriter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 915
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeBegin()V

    .line 917
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->getFirstPassWriter()Lcom/microsoft/bond/ProtocolWriter;

    move-result-object v0

    .local v0, "firstPassWriter":Lcom/microsoft/bond/ProtocolWriter;
    if-eqz v0, :cond_0

    .line 919
    invoke-virtual {p0, v0, v1}, Lxbox/smartglass/CommonData;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    .line 920
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/CommonData;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    .line 926
    :goto_0
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeEnd()V

    .line 927
    return-void

    .line 924
    :cond_0
    invoke-virtual {p0, p1, v1}, Lxbox/smartglass/CommonData;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    goto :goto_0
.end method

.method public writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V
    .locals 4
    .param p1, "writer"    # Lcom/microsoft/bond/ProtocolWriter;
    .param p2, "isBase"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 930
    sget-object v1, Lcom/microsoft/bond/ProtocolCapability;->CAN_OMIT_FIELDS:Lcom/microsoft/bond/ProtocolCapability;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->hasCapability(Lcom/microsoft/bond/ProtocolCapability;)Z

    move-result v0

    .line 931
    .local v0, "canOmitFields":Z
    sget-object v1, Lxbox/smartglass/CommonData$Schema;->metadata:Lcom/microsoft/bond/Metadata;

    invoke-virtual {p1, v1, p2}, Lcom/microsoft/bond/ProtocolWriter;->writeStructBegin(Lcom/microsoft/bond/BondSerializable;Z)V

    .line 932
    const/4 v1, 0x1

    invoke-super {p0, p1, v1}, LMicrosoft/Telemetry/Domain;->writeNested(Lcom/microsoft/bond/ProtocolWriter;Z)V

    .line 934
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0xa

    invoke-static {}, Lxbox/smartglass/CommonData$Schema;->access$000()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 935
    iget-object v1, p0, Lxbox/smartglass/CommonData;->eventVersion:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeWString(Ljava/lang/String;)V

    .line 936
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 938
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x14

    invoke-static {}, Lxbox/smartglass/CommonData$Schema;->access$100()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 939
    iget-object v1, p0, Lxbox/smartglass/CommonData;->deviceModel:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeWString(Ljava/lang/String;)V

    .line 940
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 942
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_UINT32:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x1e

    invoke-static {}, Lxbox/smartglass/CommonData$Schema;->access$200()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 943
    iget v1, p0, Lxbox/smartglass/CommonData;->deviceFormFactor:I

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeUInt32(I)V

    .line 944
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 946
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_UINT32:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x28

    invoke-static {}, Lxbox/smartglass/CommonData$Schema;->access$300()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 947
    iget v1, p0, Lxbox/smartglass/CommonData;->release:I

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeUInt32(I)V

    .line 948
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 950
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x32

    invoke-static {}, Lxbox/smartglass/CommonData$Schema;->access$400()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 951
    iget-object v1, p0, Lxbox/smartglass/CommonData;->clientLanguage:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeWString(Ljava/lang/String;)V

    .line 952
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 954
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_UINT32:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x3c

    invoke-static {}, Lxbox/smartglass/CommonData$Schema;->access$500()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 955
    iget v1, p0, Lxbox/smartglass/CommonData;->network:I

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeUInt32(I)V

    .line 956
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 958
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x46

    invoke-static {}, Lxbox/smartglass/CommonData$Schema;->access$600()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 959
    iget-object v1, p0, Lxbox/smartglass/CommonData;->sandboxId:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeWString(Ljava/lang/String;)V

    .line 960
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 962
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x50

    invoke-static {}, Lxbox/smartglass/CommonData$Schema;->access$700()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 963
    iget-object v1, p0, Lxbox/smartglass/CommonData;->consoleId:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeWString(Ljava/lang/String;)V

    .line 964
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 966
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x5a

    invoke-static {}, Lxbox/smartglass/CommonData$Schema;->access$800()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 967
    iget-object v1, p0, Lxbox/smartglass/CommonData;->xuid:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeWString(Ljava/lang/String;)V

    .line 968
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 970
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x64

    invoke-static {}, Lxbox/smartglass/CommonData$Schema;->access$900()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 971
    iget-object v1, p0, Lxbox/smartglass/CommonData;->appSessionId:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeWString(Ljava/lang/String;)V

    .line 972
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 974
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_UINT64:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x6e

    invoke-static {}, Lxbox/smartglass/CommonData$Schema;->access$1000()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 975
    iget-wide v2, p0, Lxbox/smartglass/CommonData;->memoryKb:J

    invoke-virtual {p1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeUInt64(J)V

    .line 976
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 978
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x78

    invoke-static {}, Lxbox/smartglass/CommonData$Schema;->access$1100()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 979
    iget-object v1, p0, Lxbox/smartglass/CommonData;->additionalInfo:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeWString(Ljava/lang/String;)V

    .line 980
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 982
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x82

    invoke-static {}, Lxbox/smartglass/CommonData$Schema;->access$1200()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 983
    iget-object v1, p0, Lxbox/smartglass/CommonData;->accessibilitySettings:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeWString(Ljava/lang/String;)V

    .line 984
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 986
    sget-object v1, Lcom/microsoft/bond/BondDataType;->BT_WSTRING:Lcom/microsoft/bond/BondDataType;

    const/16 v2, 0x8c

    invoke-static {}, Lxbox/smartglass/CommonData$Schema;->access$1300()Lcom/microsoft/bond/Metadata;

    move-result-object v3

    invoke-virtual {p1, v1, v2, v3}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldBegin(Lcom/microsoft/bond/BondDataType;ILcom/microsoft/bond/BondSerializable;)V

    .line 987
    iget-object v1, p0, Lxbox/smartglass/CommonData;->attributionId:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/microsoft/bond/ProtocolWriter;->writeWString(Ljava/lang/String;)V

    .line 988
    invoke-virtual {p1}, Lcom/microsoft/bond/ProtocolWriter;->writeFieldEnd()V

    .line 990
    invoke-virtual {p1, p2}, Lcom/microsoft/bond/ProtocolWriter;->writeStructEnd(Z)V

    .line 991
    return-void
.end method
