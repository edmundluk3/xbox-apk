.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;
.super Ljava/lang/Object;
.source "ContentRatingsDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContentRatingsResponse"
.end annotation


# instance fields
.field private volatile transient hashCode:I

.field private final ratingBoards:Ljava/util/Map;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "RatingBoards"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 1
    .param p1    # Ljava/util/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 32
    .local p1, "ratingBoards":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p1}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;->ratingBoards:Ljava/util/Map;

    .line 35
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 44
    if-ne p1, p0, :cond_0

    .line 45
    const/4 v1, 0x1

    .line 50
    :goto_0
    return v1

    .line 46
    :cond_0
    instance-of v1, p1, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;

    if-nez v1, :cond_1

    .line 47
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 49
    check-cast v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;

    .line 50
    .local v0, "other":Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;
    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;->ratingBoards:Ljava/util/Map;

    iget-object v2, v0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;->ratingBoards:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getRatingBoards()Ljava/util/Map;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$RatingBoard;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;->ratingBoards:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 56
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;->hashCode:I

    if-nez v0, :cond_0

    .line 57
    const/16 v0, 0x11

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;->hashCode:I

    .line 58
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;->hashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;->ratingBoards:Ljava/util/Map;

    invoke-static {v1}, Lcom/microsoft/xbox/toolkit/HashCoder;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;->hashCode:I

    .line 61
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/service/store/StoreDataTypes/ContentRatingsDataTypes$ContentRatingsResponse;->hashCode:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/gson/GsonUtil;->toJsonString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
