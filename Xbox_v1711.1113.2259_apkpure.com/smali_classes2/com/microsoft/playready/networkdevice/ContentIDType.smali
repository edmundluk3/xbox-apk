.class public final enum Lcom/microsoft/playready/networkdevice/ContentIDType;
.super Ljava/lang/Enum;
.source "ContentIDType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/playready/networkdevice/ContentIDType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/playready/networkdevice/ContentIDType;

.field public static final enum Custom:Lcom/microsoft/playready/networkdevice/ContentIDType;

.field public static final enum Invalid:Lcom/microsoft/playready/networkdevice/ContentIDType;

.field public static final enum KID:Lcom/microsoft/playready/networkdevice/ContentIDType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17
    new-instance v0, Lcom/microsoft/playready/networkdevice/ContentIDType;

    const-string v1, "Invalid"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/playready/networkdevice/ContentIDType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/ContentIDType;->Invalid:Lcom/microsoft/playready/networkdevice/ContentIDType;

    .line 20
    new-instance v0, Lcom/microsoft/playready/networkdevice/ContentIDType;

    const-string v1, "KID"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/playready/networkdevice/ContentIDType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/ContentIDType;->KID:Lcom/microsoft/playready/networkdevice/ContentIDType;

    .line 23
    new-instance v0, Lcom/microsoft/playready/networkdevice/ContentIDType;

    const-string v1, "Custom"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/playready/networkdevice/ContentIDType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/playready/networkdevice/ContentIDType;->Custom:Lcom/microsoft/playready/networkdevice/ContentIDType;

    .line 14
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/microsoft/playready/networkdevice/ContentIDType;

    sget-object v1, Lcom/microsoft/playready/networkdevice/ContentIDType;->Invalid:Lcom/microsoft/playready/networkdevice/ContentIDType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/playready/networkdevice/ContentIDType;->KID:Lcom/microsoft/playready/networkdevice/ContentIDType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/playready/networkdevice/ContentIDType;->Custom:Lcom/microsoft/playready/networkdevice/ContentIDType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/microsoft/playready/networkdevice/ContentIDType;->$VALUES:[Lcom/microsoft/playready/networkdevice/ContentIDType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/playready/networkdevice/ContentIDType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 14
    const-class v0, Lcom/microsoft/playready/networkdevice/ContentIDType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/playready/networkdevice/ContentIDType;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/playready/networkdevice/ContentIDType;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/microsoft/playready/networkdevice/ContentIDType;->$VALUES:[Lcom/microsoft/playready/networkdevice/ContentIDType;

    invoke-virtual {v0}, [Lcom/microsoft/playready/networkdevice/ContentIDType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/playready/networkdevice/ContentIDType;

    return-object v0
.end method
