.class Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$2;
.super Ljava/lang/Object;
.source "ClubChatScreenAdapter.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    .prologue
    .line 448
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 6
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 461
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->access$000(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceEditText;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 462
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->access$400(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setEnabled(Z)V

    .line 464
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 465
    .local v0, "currentTimestamp":J
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->access$500(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)J

    move-result-wide v2

    sub-long v2, v0, v2

    const-wide/16 v4, 0x7d0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 466
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->access$600(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenViewModel;->sendIsTypingMessage()V

    .line 467
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    invoke-static {v2, v0, v1}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->access$502(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;J)J

    .line 471
    .end local v0    # "currentTimestamp":J
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->access$700(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)V

    .line 472
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 451
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 456
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter$2;->this$0:Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;->access$300(Lcom/microsoft/xbox/xle/app/clubs/ClubChatScreenAdapter;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 457
    return-void
.end method
