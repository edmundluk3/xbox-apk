.class final enum Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;
.super Ljava/lang/Enum;
.source "FeaturedList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/app/activity/FeaturedList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "TouchState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

.field public static final enum LONG_PRESS:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

.field public static final enum PRESSED:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

.field public static final enum RESTING:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

.field public static final enum SCROLLING:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 32
    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    const-string v1, "RESTING"

    invoke-direct {v0, v1, v2}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;->RESTING:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    const-string v1, "PRESSED"

    invoke-direct {v0, v1, v3}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;->PRESSED:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    const-string v1, "SCROLLING"

    invoke-direct {v0, v1, v4}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;->SCROLLING:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    new-instance v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    const-string v1, "LONG_PRESS"

    invoke-direct {v0, v1, v5}, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;->LONG_PRESS:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    .line 31
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    sget-object v1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;->RESTING:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;->PRESSED:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;->SCROLLING:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;->LONG_PRESS:Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;->$VALUES:[Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    const-class v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    return-object v0
.end method

.method public static values()[Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;->$VALUES:[Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    invoke-virtual {v0}, [Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/microsoft/xbox/xle/app/activity/FeaturedList$TouchState;

    return-object v0
.end method
