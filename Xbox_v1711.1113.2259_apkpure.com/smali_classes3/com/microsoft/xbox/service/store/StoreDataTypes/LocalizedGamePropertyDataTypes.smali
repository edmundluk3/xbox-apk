.class public final Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes;
.super Ljava/lang/Object;
.source "LocalizedGamePropertyDataTypes.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedValue;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$DisplayDataEntry;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreData;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$GenreCategoryTree;,
        Lcom/microsoft/xbox/service/store/StoreDataTypes/LocalizedGamePropertyDataTypes$LocalizedGamePropertyResponse;
    }
.end annotation


# static fields
.field public static final DISPLAY_DATA_HW_FIELDS_KEY:Ljava/lang/String; = "hardwarefields"

.field public static final DISPLAY_DATA_HW_FIELD_VALUES_KEY:Ljava/lang/String; = "hardwarefieldvalues"


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Type shouldn\'t be instantiated"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
