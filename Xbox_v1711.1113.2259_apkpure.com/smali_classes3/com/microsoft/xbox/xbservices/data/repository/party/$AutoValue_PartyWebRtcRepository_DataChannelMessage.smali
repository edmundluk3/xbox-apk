.class abstract Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;
.super Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;
.source "$AutoValue_PartyWebRtcRepository_DataChannelMessage.java"


# instance fields
.field private final data:Ljava/lang/String;

.field private final from:Ljava/lang/String;

.field private final mpsdMemberIndex:Ljava/lang/Integer;

.field private final name:Ljava/lang/String;

.field private final ssrc:Ljava/lang/Integer;

.field private final type:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "mpsdMemberIndex"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "ssrc"    # Ljava/lang/Integer;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "from"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "data"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;-><init>()V

    .line 25
    if-nez p1, :cond_0

    .line 26
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Null type"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 28
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->type:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->name:Ljava/lang/String;

    .line 30
    iput-object p3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->mpsdMemberIndex:Ljava/lang/Integer;

    .line 31
    iput-object p4, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->ssrc:Ljava/lang/Integer;

    .line 32
    iput-object p5, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->from:Ljava/lang/String;

    .line 33
    iput-object p6, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->data:Ljava/lang/String;

    .line 34
    return-void
.end method


# virtual methods
.method public data()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->data:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 86
    if-ne p1, p0, :cond_1

    .line 98
    :cond_0
    :goto_0
    return v1

    .line 89
    :cond_1
    instance-of v3, p1, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;

    if-eqz v3, :cond_8

    move-object v0, p1

    .line 90
    check-cast v0, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;

    .line 91
    .local v0, "that":Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->type:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;->type()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->name:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 92
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;->name()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_1
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->mpsdMemberIndex:Ljava/lang/Integer;

    if-nez v3, :cond_4

    .line 93
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;->mpsdMemberIndex()Ljava/lang/Integer;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_2
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->ssrc:Ljava/lang/Integer;

    if-nez v3, :cond_5

    .line 94
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;->ssrc()Ljava/lang/Integer;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_3
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->from:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 95
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;->from()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_2

    :goto_4
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->data:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 96
    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;->data()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 92
    :cond_3
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->name:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    .line 93
    :cond_4
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->mpsdMemberIndex:Ljava/lang/Integer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;->mpsdMemberIndex()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_2

    .line 94
    :cond_5
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->ssrc:Ljava/lang/Integer;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;->ssrc()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_3

    .line 95
    :cond_6
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->from:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;->from()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_4

    .line 96
    :cond_7
    iget-object v3, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->data:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;->data()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto/16 :goto_0

    .end local v0    # "that":Lcom/microsoft/xbox/xbservices/data/repository/party/PartyWebRtcRepository$DataChannelMessage;
    :cond_8
    move v1, v2

    .line 98
    goto/16 :goto_0
.end method

.method public from()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->from:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    const v3, 0xf4243

    .line 103
    const/4 v0, 0x1

    .line 104
    .local v0, "h":I
    mul-int/2addr v0, v3

    .line 105
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->type:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 106
    mul-int/2addr v0, v3

    .line 107
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->name:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    xor-int/2addr v0, v1

    .line 108
    mul-int/2addr v0, v3

    .line 109
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->mpsdMemberIndex:Ljava/lang/Integer;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    xor-int/2addr v0, v1

    .line 110
    mul-int/2addr v0, v3

    .line 111
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->ssrc:Ljava/lang/Integer;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    xor-int/2addr v0, v1

    .line 112
    mul-int/2addr v0, v3

    .line 113
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->from:Ljava/lang/String;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    xor-int/2addr v0, v1

    .line 114
    mul-int/2addr v0, v3

    .line 115
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->data:Ljava/lang/String;

    if-nez v1, :cond_4

    :goto_4
    xor-int/2addr v0, v2

    .line 116
    return v0

    .line 107
    :cond_0
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 109
    :cond_1
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->mpsdMemberIndex:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    .line 111
    :cond_2
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->ssrc:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2

    .line 113
    :cond_3
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->from:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    .line 115
    :cond_4
    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->data:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_4
.end method

.method public mpsdMemberIndex()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->mpsdMemberIndex:Ljava/lang/Integer;

    return-object v0
.end method

.method public name()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->name:Ljava/lang/String;

    return-object v0
.end method

.method public ssrc()Ljava/lang/Integer;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->ssrc:Ljava/lang/Integer;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DataChannelMessage{type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mpsdMemberIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->mpsdMemberIndex:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ssrc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->ssrc:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", from="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->from:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->data:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public type()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/microsoft/xbox/xbservices/data/repository/party/$AutoValue_PartyWebRtcRepository_DataChannelMessage;->type:Ljava/lang/String;

    return-object v0
.end method
