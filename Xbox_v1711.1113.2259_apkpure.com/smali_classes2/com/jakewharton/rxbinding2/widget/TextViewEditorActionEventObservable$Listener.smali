.class final Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEventObservable$Listener;
.super Lio/reactivex/android/MainThreadDisposable;
.source "TextViewEditorActionEventObservable.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEventObservable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "Listener"
.end annotation


# instance fields
.field private final handled:Lio/reactivex/functions/Predicate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/functions/Predicate",
            "<-",
            "Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final observer:Lio/reactivex/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observer",
            "<-",
            "Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final view:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/widget/TextView;Lio/reactivex/Observer;Lio/reactivex/functions/Predicate;)V
    .locals 0
    .param p1, "view"    # Landroid/widget/TextView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TextView;",
            "Lio/reactivex/Observer",
            "<-",
            "Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEvent;",
            ">;",
            "Lio/reactivex/functions/Predicate",
            "<-",
            "Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p2, "observer":Lio/reactivex/Observer;, "Lio/reactivex/Observer<-Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEvent;>;"
    .local p3, "handled":Lio/reactivex/functions/Predicate;, "Lio/reactivex/functions/Predicate<-Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEvent;>;"
    invoke-direct {p0}, Lio/reactivex/android/MainThreadDisposable;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEventObservable$Listener;->view:Landroid/widget/TextView;

    .line 41
    iput-object p2, p0, Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEventObservable$Listener;->observer:Lio/reactivex/Observer;

    .line 42
    iput-object p3, p0, Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEventObservable$Listener;->handled:Lio/reactivex/functions/Predicate;

    .line 43
    return-void
.end method


# virtual methods
.method protected onDispose()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEventObservable$Listener;->view:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 63
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    .line 47
    iget-object v2, p0, Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEventObservable$Listener;->view:Landroid/widget/TextView;

    invoke-static {v2, p2, p3}, Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEvent;->create(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEvent;

    move-result-object v1

    .line 49
    .local v1, "event":Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEvent;
    :try_start_0
    invoke-virtual {p0}, Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEventObservable$Listener;->isDisposed()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEventObservable$Listener;->handled:Lio/reactivex/functions/Predicate;

    invoke-interface {v2, v1}, Lio/reactivex/functions/Predicate;->test(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 50
    iget-object v2, p0, Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEventObservable$Listener;->observer:Lio/reactivex/Observer;

    invoke-interface {v2, v1}, Lio/reactivex/Observer;->onNext(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    const/4 v2, 0x1

    .line 57
    :goto_0
    return v2

    .line 53
    :catch_0
    move-exception v0

    .line 54
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEventObservable$Listener;->observer:Lio/reactivex/Observer;

    invoke-interface {v2, v0}, Lio/reactivex/Observer;->onError(Ljava/lang/Throwable;)V

    .line 55
    invoke-virtual {p0}, Lcom/jakewharton/rxbinding2/widget/TextViewEditorActionEventObservable$Listener;->dispose()V

    .line 57
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
