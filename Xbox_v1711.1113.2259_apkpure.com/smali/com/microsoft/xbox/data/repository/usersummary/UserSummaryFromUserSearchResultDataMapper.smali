.class public Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromUserSearchResultDataMapper;
.super Ljava/lang/Object;
.source "UserSummaryFromUserSearchResultDataMapper.java"

# interfaces
.implements Lcom/microsoft/xbox/data/repository/DataMapper;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/microsoft/xbox/data/repository/DataMapper",
        "<",
        "Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;",
        "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;)Lio/reactivex/Observable;
    .locals 12
    .param p1, "data"    # Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    iget-object v9, p1, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->id:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 26
    .local v0, "xuid":J
    iget-object v2, p1, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->gamertag:Ljava/lang/String;

    .line 27
    .local v2, "gamertag":Ljava/lang/String;
    const-string v3, ""

    .line 28
    .local v3, "realName":Ljava/lang/String;
    iget-object v9, p1, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;->displayPicUri:Ljava/lang/String;

    const-string v10, ""

    invoke-static {v9, v10}, Lcom/microsoft/xbox/toolkit/JavaUtil;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 29
    .local v4, "displayPicUri":Ljava/lang/String;
    const-wide/16 v6, 0x0

    .line 30
    .local v6, "gamerScore":J
    const-string v5, ""

    .line 31
    .local v5, "presenceText":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;->defaultProfileColor()Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;

    move-result-object v8

    .line 33
    .local v8, "colors":Lcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;
    const-wide/16 v10, 0x0

    cmp-long v9, v0, v10

    if-lez v9, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 34
    const/4 v9, 0x0

    invoke-static/range {v0 .. v9}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;->with(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/microsoft/xbox/service/network/managers/ProfilePreferredColor;Z)Lcom/microsoft/xbox/data/repository/usersummary/UserSummary;

    move-result-object v9

    invoke-static {v9}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v9

    .line 37
    :goto_0
    return-object v9

    .line 36
    :cond_0
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Failed to map user summary: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 37
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v9

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Lio/reactivex/Observable;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 17
    check-cast p1, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromUserSearchResultDataMapper;->apply(Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 17
    check-cast p1, Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/data/repository/usersummary/UserSummaryFromUserSearchResultDataMapper;->apply(Lcom/microsoft/xbox/service/userSearch/UserSearchDataTypes$UserSearchResultData;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
