.class Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask$1;
.super Ljava/lang/Object;
.source "EpgConnectorFragmentManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;)V
    .locals 0
    .param p1, "this$1"    # Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask$1;->this$1:Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 82
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask$1;->this$1:Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;->access$000(Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;)Ljava/lang/ref/WeakReference;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;

    .line 83
    .local v0, "fragmentMgrPtr":Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;
    if-eqz v0, :cond_0

    .line 84
    const-string v1, "EpgConnectorFragmentManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Datagram timed out: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask$1;->this$1:Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;

    invoke-static {v3}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;->access$100(Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    iget-object v1, p0, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask$1;->this$1:Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;->access$100(Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager$TTLTimerTask;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;->access$200(Lcom/microsoft/xbox/xle/epg/EpgConnectorFragmentManager;I)V

    .line 87
    :cond_0
    return-void
.end method
