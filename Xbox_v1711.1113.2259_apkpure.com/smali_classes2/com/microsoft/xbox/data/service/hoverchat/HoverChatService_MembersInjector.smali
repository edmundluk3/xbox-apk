.class public final Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService_MembersInjector;
.super Ljava/lang/Object;
.source "HoverChatService_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector",
        "<",
        "Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final authStateManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/auth/AuthStateManager;",
            ">;"
        }
    .end annotation
.end field

.field private final hoverChatManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;",
            ">;"
        }
    .end annotation
.end field

.field private final hoverChatRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final schedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService_MembersInjector;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService_MembersInjector;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/auth/AuthStateManager;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p1, "hoverChatManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;>;"
    .local p2, "hoverChatRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;>;"
    .local p3, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    .local p4, "authStateManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/auth/AuthStateManager;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    sget-boolean v0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 30
    :cond_0
    iput-object p1, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService_MembersInjector;->hoverChatManagerProvider:Ljavax/inject/Provider;

    .line 31
    sget-boolean v0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 32
    :cond_1
    iput-object p2, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService_MembersInjector;->hoverChatRepositoryProvider:Ljavax/inject/Provider;

    .line 33
    sget-boolean v0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 34
    :cond_2
    iput-object p3, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService_MembersInjector;->schedulerProvider:Ljavax/inject/Provider;

    .line 35
    sget-boolean v0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService_MembersInjector;->$assertionsDisabled:Z

    if-nez v0, :cond_3

    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 36
    :cond_3
    iput-object p4, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService_MembersInjector;->authStateManagerProvider:Ljavax/inject/Provider;

    .line 37
    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/auth/AuthStateManager;",
            ">;)",
            "Ldagger/MembersInjector",
            "<",
            "Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    .local p0, "hoverChatManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;>;"
    .local p1, "hoverChatRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;>;"
    .local p2, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    .local p3, "authStateManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/auth/AuthStateManager;>;"
    new-instance v0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService_MembersInjector;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectAuthStateManager(Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/domain/auth/AuthStateManager;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 79
    .local p1, "authStateManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/domain/auth/AuthStateManager;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    .line 80
    return-void
.end method

.method public static injectHoverChatManager(Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p1, "hoverChatManagerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->hoverChatManager:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;

    .line 65
    return-void
.end method

.method public static injectHoverChatRepository(Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 69
    .local p1, "hoverChatRepositoryProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->hoverChatRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    .line 70
    return-void
.end method

.method public static injectSchedulerProvider(Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;Ljavax/inject/Provider;)V
    .locals 1
    .param p0, "instance"    # Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;",
            "Ljavax/inject/Provider",
            "<",
            "Lcom/microsoft/xbox/toolkit/SchedulerProvider;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 74
    .local p1, "schedulerProvider":Ljavax/inject/Provider;, "Ljavax/inject/Provider<Lcom/microsoft/xbox/toolkit/SchedulerProvider;>;"
    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    iput-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 75
    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;)V
    .locals 2
    .param p1, "instance"    # Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;

    .prologue
    .line 53
    if-nez p1, :cond_0

    .line 54
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Cannot inject members into a null reference"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService_MembersInjector;->hoverChatManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;

    iput-object v0, p1, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->hoverChatManager:Lcom/microsoft/xbox/presentation/hoverchat/HoverChatManager;

    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService_MembersInjector;->hoverChatRepositoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    iput-object v0, p1, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->hoverChatRepository:Lcom/microsoft/xbox/data/repository/hoverchat/HoverChatHeadRepository;

    .line 58
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService_MembersInjector;->schedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    iput-object v0, p1, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->schedulerProvider:Lcom/microsoft/xbox/toolkit/SchedulerProvider;

    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService_MembersInjector;->authStateManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    iput-object v0, p1, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;->authStateManager:Lcom/microsoft/xbox/domain/auth/AuthStateManager;

    .line 60
    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 11
    check-cast p1, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService_MembersInjector;->injectMembers(Lcom/microsoft/xbox/data/service/hoverchat/HoverChatService;)V

    return-void
.end method
