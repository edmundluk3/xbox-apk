.class public Lcom/microsoft/xbox/toolkit/XLEErrorCode;
.super Ljava/lang/Object;
.source "XLEErrorCode.java"


# static fields
.field public static final ASSERT_CONDITION_FAILED:J = 0x2710L

.field public static final ASSERT_FORCED_FAILURE:J = 0x2711L

.field public static final Authentication_AccountBanned:J = 0x3ffL

.field public static final Authentication_ContentIsolation:J = 0x402L

.field public static final Authentication_FailedToSignIn:J = 0x3e9L

.field public static final Authentication_GamerSignedOut:J = 0x3f0L

.field public static final Authentication_NeedAccountCreation:J = 0x400L

.field public static final Authentication_NeedGamertagChange:J = 0x403L

.field public static final Authentication_NewTermsOfUse:J = 0x401L

.field public static final Authentication_NoNetwork:J = 0x405L

.field public static final Authentication_OAuthLoginTimedOut:J = 0x3f1L

.field public static final Authentication_ReceivedEmptyRpsTicket:J = 0x3ebL

.field public static final Authentication_SignInCancelled:J = 0x3efL

.field public static final BAD_REQUEST:J = 0xfL

.field public static final CONFLICT:J = 0x16L

.field public static final DATA_OUT_OF_DATE:J = 0xaL

.field public static final DISCONNECTED_FROM_CHAT_SERVICE:J = 0x2724L

.field public static final EMPTY_RESPONSE:J = 0x7L

.field public static final ERROR_SERIALIZATION:J = 0x9L

.field public static final FAILED_CONNECT_CONSOLE:J = 0x1b59L

.field public static final FAILED_DEV_ERROR:J = 0x13L

.field public static final FAILED_TO_ADD_APP_CHANNEL_FAVORITES:J = 0xfc1L

.field public static final FAILED_TO_ADD_FRIEND:J = 0xbc3L

.field public static final FAILED_TO_ADD_MEMBER_TO_CONVERSATION:J = 0x2648L

.field public static final FAILED_TO_ADD_PINS:J = 0xfbbL

.field public static final FAILED_TO_ADD_USER_TO_FAVORITELIST:J = 0xf9aL

.field public static final FAILED_TO_BLOCK_USER:J = 0xf9cL

.field public static final FAILED_TO_BROWSE_MEDIA_ITEM_LIST:J = 0xfafL

.field public static final FAILED_TO_CHECK_UPDATE:J = 0x14L

.field public static final FAILED_TO_CONNECT_TO_CHAT_SERVICE:J = 0x271aL

.field public static final FAILED_TO_CONNECT_TO_CONSOLE:J = 0x7d1L

.field public static final FAILED_TO_CREATE_ENABLE_NOTIFICATIONS_REQUEST:J = 0x1fa5L

.field public static final FAILED_TO_DELETE_ACTIVITY_FEED_COMMENT:J = 0xc89L

.field public static final FAILED_TO_DELETE_ACTIVITY_FEED_ITEM:J = 0xc88L

.field public static final FAILED_TO_DELETE_APP_CHANNEL_FAVORITES:J = 0xfc2L

.field public static final FAILED_TO_DELETE_CONVERSATION:J = 0x264cL

.field public static final FAILED_TO_DELETE_MESSAGE:J = 0xbedL

.field public static final FAILED_TO_DELETE_PINS:J = 0xfbcL

.field public static final FAILED_TO_DELETE_REFRESH_TOKEN:J = 0x3f7L

.field public static final FAILED_TO_DELETE_SHOWCASE_ITEM:J = 0xc8aL

.field public static final FAILED_TO_DISABLE_NOTIFICATIONS:J = 0x1fa8L

.field public static final FAILED_TO_DISCOVER_CONSOLES:J = 0x1b5aL

.field public static final FAILED_TO_ENABLE_NOTIFICATIONS:J = 0x1fa6L

.field public static final FAILED_TO_ENCRYPT_RPS_TICKET:J = 0x3f9L

.field public static final FAILED_TO_FETCH_PIN_INFO:J = 0xc8eL

.field public static final FAILED_TO_FOLLOW_UNFOLLOW:J = 0x2454L

.field public static final FAILED_TO_GENERATE_X509CERTIFICATE:J = 0x3faL

.field public static final FAILED_TO_GET_ACCESS_TOKEN:J = 0x3eaL

.field public static final FAILED_TO_GET_ACHIEVEMENTS:J = 0xbd7L

.field public static final FAILED_TO_GET_ACTIVITY_DETAIL:J = 0xfb7L

.field public static final FAILED_TO_GET_ACTIVITY_FEED:J = 0xc85L

.field public static final FAILED_TO_GET_ACTIVITY_SUMMARY:J = 0xfb6L

.field public static final FAILED_TO_GET_ALBUM_DETAILS:J = 0xfabL

.field public static final FAILED_TO_GET_APP_CHANNEL_FAVORITES:J = 0xfc0L

.field public static final FAILED_TO_GET_APP_DETAILS:J = 0xfaaL

.field public static final FAILED_TO_GET_ARTIST_ALBUM:J = 0xfacL

.field public static final FAILED_TO_GET_ARTIST_BIOGRAPH:J = 0xfadL

.field public static final FAILED_TO_GET_ARTIST_DETAILS:J = 0xfa9L

.field public static final FAILED_TO_GET_AUTO_SUGGEST_DATA:J = 0xfb8L

.field public static final FAILED_TO_GET_BACK_COMPAT_ITEMS:J = 0xfc7L

.field public static final FAILED_TO_GET_BATCH_STATS:J = 0xc80L

.field public static final FAILED_TO_GET_BEAM_CHANNELS:J = 0x26f2L

.field public static final FAILED_TO_GET_BEAM_CHANNEL_DETAILS:J = 0x26f2L

.field public static final FAILED_TO_GET_BUNDLES:J = 0x2199L

.field public static final FAILED_TO_GET_CHAT_HISTORY:J = 0x26acL

.field public static final FAILED_TO_GET_CLUB_DATA:J = 0x2580L

.field public static final FAILED_TO_GET_CURRENTGAMER:J = 0xc81L

.field public static final FAILED_TO_GET_DTITLEACHIEVEMENTS:J = 0xc82L

.field public static final FAILED_TO_GET_ENTITY:J = 0xbefL

.field public static final FAILED_TO_GET_EPIX:J = 0x1068L

.field public static final FAILED_TO_GET_FAMILY_SETTINGS:J = 0xbc8L

.field public static final FAILED_TO_GET_FEATURED_CHALLENGES_DATA:J = 0xbf4L

.field public static final FAILED_TO_GET_FEED_ITEM_COMMENTS:J = 0x232bL

.field public static final FAILED_TO_GET_FEED_ITEM_LIKES:J = 0x232cL

.field public static final FAILED_TO_GET_FEED_ITEM_SHARES:J = 0x232dL

.field public static final FAILED_TO_GET_FOLLOWERS_INFO:J = 0xbc2L

.field public static final FAILED_TO_GET_FOLLOWING_INFO:J = 0xc84L

.field public static final FAILED_TO_GET_FOLLOW_INFO:J = 0x247cL

.field public static final FAILED_TO_GET_FRIENDS_ACHIEVEMENT_DATA:J = 0xbdbL

.field public static final FAILED_TO_GET_FRIENDS_ACTIVITYFEED:J = 0xc86L

.field public static final FAILED_TO_GET_FRIENDS_WHO_PLAY:J = 0x23f0L

.field public static final FAILED_TO_GET_FUTURE_SHOWTIMES:J = 0x219aL

.field public static final FAILED_TO_GET_GAMEPROGRESS_360_ACHIEVEMENTS_DATA:J = 0xbddL

.field public static final FAILED_TO_GET_GAMEPROGRESS_360_EARNED_ACHIEVEMENTS_DATA:J = 0xbdeL

.field public static final FAILED_TO_GET_GAMEPROGRESS_ACHIEVEMENTS_DATA:J = 0xbdcL

.field public static final FAILED_TO_GET_GAMEPROGRESS_COMPARE_ACHIEVEMENTS_DATA:J = 0xbe2L

.field public static final FAILED_TO_GET_GAMEPROGRESS_LIMITED_TIME_CHALLENGES:J = 0xbe0L

.field public static final FAILED_TO_GET_GAMEPROGRESS_TITLE_IMAGES:J = 0xbdfL

.field public static final FAILED_TO_GET_GAMER_CONTEXT:J = 0xbbdL

.field public static final FAILED_TO_GET_GAMES:J = 0xbd6L

.field public static final FAILED_TO_GET_GAME_DETAILS:J = 0xfa7L

.field public static final FAILED_TO_GET_INCLUDED_CONTENT:J = 0x2198L

.field public static final FAILED_TO_GET_LEADERBOARD:J = 0x2134L

.field public static final FAILED_TO_GET_LEGALLOCALE:J = 0x1771L

.field public static final FAILED_TO_GET_LIKE_STATUSES:J = 0xd1bL

.field public static final FAILED_TO_GET_LIST_CONTAINED_ITEMS:J = 0xfbfL

.field public static final FAILED_TO_GET_MEDIA_ITEM_DETAILS:J = 0xfaeL

.field public static final FAILED_TO_GET_MESSAGE_DETAIL:J = 0xbebL

.field public static final FAILED_TO_GET_MESSAGE_SUMMARY:J = 0xbeaL

.field public static final FAILED_TO_GET_METADATA:J = 0xfbeL

.field public static final FAILED_TO_GET_ME_PROFILE:J = 0xbbbL

.field public static final FAILED_TO_GET_ME_XUID:J = 0xbb9L

.field public static final FAILED_TO_GET_MORE_SEARCH_SUMMARY_DATA:J = 0xfb4L

.field public static final FAILED_TO_GET_MOVIE_DETAILS:J = 0xfa8L

.field public static final FAILED_TO_GET_MPSD_SESSIONS:J = 0x251cL

.field public static final FAILED_TO_GET_MSA_TICKET_FOR_LIVE_DOT_COM:J = 0xbf2L

.field public static final FAILED_TO_GET_NEVERLIST_DATA:J = 0xc83L

.field public static final FAILED_TO_GET_NOW_PLAYING_APP_INFO:J = 0xfb1L

.field public static final FAILED_TO_GET_NOW_PLAYING_MEDIA_INFO:J = 0xfb2L

.field public static final FAILED_TO_GET_PINS:J = 0xfbaL

.field public static final FAILED_TO_GET_POPULAR_GAMES_WITH_FRIENDS:J = 0x238cL

.field public static final FAILED_TO_GET_POPULAR_SEARCH_DATA:J = 0xfb5L

.field public static final FAILED_TO_GET_PREFERRED_COLOR:J = 0xfc3L

.field public static final FAILED_TO_GET_PREVIEW_BATCH:J = 0x264eL

.field public static final FAILED_TO_GET_PROFILE_PRESENCE_DATA:J = 0xbc5L

.field public static final FAILED_TO_GET_PROFILE_PRIVACY_SETTINGS:J = 0x2166L

.field public static final FAILED_TO_GET_PROFILE_SHOWCASE_DATA:J = 0xbc0L

.field public static final FAILED_TO_GET_PROFILE_USERS:J = 0x232eL

.field public static final FAILED_TO_GET_PROGRAMMINGCONTENT:J = 0xfa1L

.field public static final FAILED_TO_GET_RATING:J = 0x1004L

.field public static final FAILED_TO_GET_RECENTS:J = 0xfbdL

.field public static final FAILED_TO_GET_RECENT_360_PROGRESS_AND_ACHIEVEMENT_DATA:J = 0xbd8L

.field public static final FAILED_TO_GET_RECENT_GAMES:J = 0xfc8L

.field public static final FAILED_TO_GET_RECENT_PLAYERS:J = 0xbc9L

.field public static final FAILED_TO_GET_RECENT_PROGRESS_AND_ACHIEVEMENT_DATA:J = 0xbd9L

.field public static final FAILED_TO_GET_REFRESH_TOKEN:J = 0x3f2L

.field public static final FAILED_TO_GET_RELATED:J = 0xfb0L

.field public static final FAILED_TO_GET_REQUIRED_ROLE:J = 0x26e9L

.field public static final FAILED_TO_GET_SANPPABLE_APPS:J = 0x106aL

.field public static final FAILED_TO_GET_SEARCH_DETAILS:J = 0xfa6L

.field public static final FAILED_TO_GET_SEARCH_SUMMARY_DATA:J = 0xfb3L

.field public static final FAILED_TO_GET_SETTINGS:J = 0x1389L

.field public static final FAILED_TO_GET_SHOWCASE_ITEMS:J = 0xc8bL

.field public static final FAILED_TO_GET_SKYPE_TOKEN:J = 0xbf0L

.field public static final FAILED_TO_GET_SOCIAL_SUMMARIES:J = 0xd1cL

.field public static final FAILED_TO_GET_STORE_ADDONS:J = 0x2329L

.field public static final FAILED_TO_GET_STORE_ADDON_PARENTS:J = 0x2334L

.field public static final FAILED_TO_GET_STORE_COLLECTION_ITEM:J = 0xfc6L

.field public static final FAILED_TO_GET_STORE_GAMES:J = 0x2328L

.field public static final FAILED_TO_GET_STORE_GOLD:J = 0x2332L

.field public static final FAILED_TO_GET_SUGGESTED_PEOPLE:J = 0xc87L

.field public static final FAILED_TO_GET_SYSTEM_TAGS:J = 0x25e4L

.field public static final FAILED_TO_GET_TITLE_PURCHASE_INFO:J = 0xfc5L

.field public static final FAILED_TO_GET_TITLE_SHOWCASE_DATA:J = 0xbdaL

.field public static final FAILED_TO_GET_TRENDING:J = 0x1069L

.field public static final FAILED_TO_GET_TRENDING_TAGS:J = 0x25e5L

.field public static final FAILED_TO_GET_TV_EPISODE_OVERVIEW_DETAILS:J = 0xfa5L

.field public static final FAILED_TO_GET_TV_SEASON_DETAILS:J = 0xfa4L

.field public static final FAILED_TO_GET_TV_SERIES_DETAILS:J = 0xfa2L

.field public static final FAILED_TO_GET_TV_SERIES_OVERVIEW_DETAILS:J = 0xfa3L

.field public static final FAILED_TO_GET_USER_PROFILE_INFO:J = 0xbbaL

.field public static final FAILED_TO_GET_USER_STATISTICS_INFO:J = 0xbf6L

.field public static final FAILED_TO_GET_USER_TITLE_HISTORY:J = 0xbd8L

.field public static final FAILED_TO_GET_XSTS_TOKEN:J = 0x3ecL

.field public static final FAILED_TO_GET_XTOKEN:J = 0x3f5L

.field public static final FAILED_TO_GET_XTOKEN_PUBLICKEY_DATA:J = 0x3f3L

.field public static final FAILED_TO_GET_XUSERTOKEN_RESPONSE_DATA:J = 0x3f4L

.field public static final FAILED_TO_GET_YOU_INFO:J = 0xbbcL

.field public static final FAILED_TO_HIDE_UNHIDE_ACTIVITY_FEED_ITEM:J = 0xc8cL

.field public static final FAILED_TO_INCREMENT_GAME_CLIP_VIEW_COUNT:J = 0xbe1L

.field public static final FAILED_TO_INCREMENT_SCREENSHOT_VIEW_COUNT:J = 0xbe7L

.field public static final FAILED_TO_LEAVE_CONVERSATION:J = 0x2649L

.field public static final FAILED_TO_MOVE_PIN:J = 0xfc4L

.field public static final FAILED_TO_MUTE_CONVERSATION:J = 0x264aL

.field public static final FAILED_TO_PARSE_AUTOSUGGEST_RESPONSE:J = 0xfb9L

.field public static final FAILED_TO_PARSE_ENABLE_NOTIFICATIONS_RESPONSE:J = 0x1fa7L

.field public static final FAILED_TO_PARSE_XTOKEN_PUBLICKEY_RESPONSE:J = 0x3fdL

.field public static final FAILED_TO_PARSE_XUSERTOKEN_RESPONSE:J = 0x3feL

.field public static final FAILED_TO_PIN_UNPIN_ACTIVITY_FEED_ITEM:J = 0xc8dL

.field public static final FAILED_TO_POST_COMMENT:J = 0x232fL

.field public static final FAILED_TO_PUT_CLUB_DATA:J = 0x2581L

.field public static final FAILED_TO_READ_REFRESH_TOKEN:J = 0x3f6L

.field public static final FAILED_TO_REMOVE_FRIEND:J = 0xbc4L

.field public static final FAILED_TO_REMOVE_USER_FROM_FAVORITELIST:J = 0xf9bL

.field public static final FAILED_TO_REMOVE_USER_FROM_NEVERLIST:J = 0xf9dL

.field public static final FAILED_TO_RENAME_CONVERSATION_TOPIC:J = 0x264dL

.field public static final FAILED_TO_SAVE_REFRESH_TOKEN:J = 0x3f8L

.field public static final FAILED_TO_SEARCH_GAMERTAG:J = 0xbc6L

.field public static final FAILED_TO_SEND_GROUP_MESSAGE_PERMISSIONS:J = 0x26e8L

.field public static final FAILED_TO_SEND_MESSAGE:J = 0xbecL

.field public static final FAILED_TO_SET_LIKE:J = 0xd1dL

.field public static final FAILED_TO_SET_RATING:J = 0x1005L

.field public static final FAILED_TO_SHARE_SHOWCASE_FULL:J = 0x2333L

.field public static final FAILED_TO_SHARE_TO_FEED:J = 0x2330L

.field public static final FAILED_TO_SHARE_TO_SHOWCASE:J = 0x2331L

.field public static final FAILED_TO_SKYPE_LONG_POLL:J = 0xbe9L

.field public static final FAILED_TO_SUBSCRIBE_LONG_POLL:J = 0xbe8L

.field public static final FAILED_TO_UPDATE_CONSUMPTION_HORIZON:J = 0x264bL

.field public static final FAILED_TO_UPDATE_STATUS:J = 0x232aL

.field public static final FAILED_TO_UPLOAD_PRESENCE_HEART_BEAT:J = 0x26deL

.field public static final FILE_READ_ERROR:J = 0x10L

.field public static final FILE_WRITE_ERROR:J = 0x11L

.field public static final FRIENDS_LIST_LIMIT_EXCEEDED:J = 0xbc7L

.field public static final Home_Editorial_Failed:J = 0x1f41L

.field public static final INVALID_ACCESS_TOKEN:J = 0x3edL

.field public static final INVALID_ARGUMENT:J = 0xcL

.field public static final INVALID_OPERATION:J = 0x5L

.field public static final INVALID_REFRESH_TOKEN:J = 0x404L

.field public static final INVALID_TOKEN:J = 0x3eeL

.field public static final INVALID_XTOKEN:J = 0x3fbL

.field public static final IO_EXCEPTION:J = 0x8L

.field public static final NETWORK_ERROR:J = 0x4L

.field public static final NOT_AUTHORIZED:J = 0x3fcL

.field public static final NOT_FOUND:J = 0x15L

.field public static final NO_NETWORK:J = 0x2L

.field public static final NO_RESPONSE:J = 0x6L

.field public static final SERVICE_ERROR:J = 0xdL

.field public static final SERVICE_UNAVAILABLE:J = 0x12L

.field public static final SLS_FailedToGetGamerAchievements:J = 0xbbeL

.field public static final SLS_GameClip_FailedToGetData:J = 0xbc1L

.field public static final SLS_Message_FailedToRegisterAPNS:J = 0xbeeL

.field public static final SLS_People_FailedToGetSummary:J = 0xbf5L

.field public static final SLS_Privacy_FailedToGetData:J = 0xf9eL

.field public static final SLS_Profile_FailedToGetData:J = 0xbbfL

.field public static final SLS_Social_FailedToGetData:J = 0xf9fL

.field public static final Success:J = 0x0L

.field public static final TIMEOUT:J = 0x3L

.field public static final UNSUPPORTED:J = 0xbL

.field public static final UPDATE_REQUIRED:J = 0xeL

.field public static final Unknown:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
