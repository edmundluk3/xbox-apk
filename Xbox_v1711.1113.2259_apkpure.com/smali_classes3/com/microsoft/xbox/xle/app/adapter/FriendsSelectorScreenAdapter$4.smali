.class Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$4;
.super Ljava/lang/Object;
.source "FriendsSelectorScreenAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->onStart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;


# direct methods
.method constructor <init>(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    .prologue
    .line 199
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$4;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 202
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$4;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->saveSelection()V

    .line 203
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$4;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->isFromMessage()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$4;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->getSelectionCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$4;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->navigateToComposeMessage()V

    .line 208
    :goto_0
    return-void

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter$4;->this$0:Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;

    invoke-static {v0}, Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;->access$400(Lcom/microsoft/xbox/xle/app/adapter/FriendsSelectorScreenAdapter;)Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/FriendsSelectorActivityViewModel;->confirm()V

    goto :goto_0
.end method
