.class Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressXboxoneCompareAchievementRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "TitleModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/TitleModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetGameProgressXboxoneCompareAchievementRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;",
        ">;"
    }
.end annotation


# instance fields
.field private caller:Lcom/microsoft/xbox/service/model/TitleModel;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/TitleModel;

.field private titleId:Ljava/lang/String;

.field private xuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/service/model/TitleModel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "titleModel"    # Lcom/microsoft/xbox/service/model/TitleModel;
    .param p3, "xuid"    # Ljava/lang/String;
    .param p4, "titleId"    # Ljava/lang/String;

    .prologue
    .line 1055
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressXboxoneCompareAchievementRunner;->this$0:Lcom/microsoft/xbox/service/model/TitleModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 1056
    iput-object p2, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressXboxoneCompareAchievementRunner;->caller:Lcom/microsoft/xbox/service/model/TitleModel;

    .line 1057
    iput-object p3, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressXboxoneCompareAchievementRunner;->xuid:Ljava/lang/String;

    .line 1058
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressXboxoneCompareAchievementRunner;->titleId:Ljava/lang/String;

    .line 1059
    return-void
.end method


# virtual methods
.method public buildData()Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1063
    const/4 v0, 0x0

    .line 1065
    .local v0, "continuationToken":Ljava/lang/String;
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressXboxoneCompareAchievementRunner;->xuid:Ljava/lang/String;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressXboxoneCompareAchievementRunner;->titleId:Ljava/lang/String;

    .line 1066
    invoke-interface {v3, v4, v5, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getGameProgressXboxoneAchievementsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    move-result-object v2

    .line 1068
    .local v2, "result":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    if-eqz v2, :cond_0

    iget-object v3, v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->pagingInfo:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$PagingInfo;

    if-eqz v3, :cond_0

    .line 1069
    iget-object v3, v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->pagingInfo:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$PagingInfo;

    iget-object v0, v3, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$PagingInfo;->continuationToken:Ljava/lang/String;

    .line 1070
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1071
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getSLSServiceManager()Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;

    move-result-object v3

    iget-object v4, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressXboxoneCompareAchievementRunner;->xuid:Ljava/lang/String;

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressXboxoneCompareAchievementRunner;->titleId:Ljava/lang/String;

    .line 1072
    invoke-interface {v3, v4, v5, v0}, Lcom/microsoft/xbox/service/network/managers/xblshared/ISLSServiceManager;->getGameProgressXboxoneAchievementsInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    move-result-object v1

    .line 1074
    .local v1, "moreResult":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    if-nez v1, :cond_1

    .line 1090
    .end local v1    # "moreResult":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    :cond_0
    return-object v2

    .line 1078
    .restart local v1    # "moreResult":Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;
    :cond_1
    iget-object v3, v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->achievements:Ljava/util/List;

    if-eqz v3, :cond_2

    iget-object v3, v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->achievements:Ljava/util/List;

    if-eqz v3, :cond_2

    .line 1079
    iget-object v3, v2, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->achievements:Ljava/util/List;

    iget-object v4, v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->achievements:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1082
    :cond_2
    iget-object v3, v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->pagingInfo:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$PagingInfo;

    if-eqz v3, :cond_3

    .line 1083
    iget-object v3, v1, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;->pagingInfo:Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$PagingInfo;

    iget-object v0, v3, Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$PagingInfo;->continuationToken:Ljava/lang/String;

    goto :goto_0

    .line 1085
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 1049
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressXboxoneCompareAchievementRunner;->buildData()Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 1104
    const-wide/16 v0, 0xbe2

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1099
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Lcom/microsoft/xbox/service/network/managers/GameProgressXboxoneAchievementsResultContainer$GameProgressXboxoneAchievementsResult;>;"
    iget-object v0, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressXboxoneCompareAchievementRunner;->caller:Lcom/microsoft/xbox/service/model/TitleModel;

    iget-object v1, p0, Lcom/microsoft/xbox/service/model/TitleModel$GetGameProgressXboxoneCompareAchievementRunner;->xuid:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Lcom/microsoft/xbox/service/model/TitleModel;->access$500(Lcom/microsoft/xbox/service/model/TitleModel;Lcom/microsoft/xbox/toolkit/AsyncResult;Ljava/lang/String;)V

    .line 1100
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 1095
    return-void
.end method
