.class public Lcom/microsoft/playready/HttpRedirectRequestedException;
.super Lcom/microsoft/playready/HttpException;
.source "HttpRedirectRequestedException.java"


# instance fields
.field mRedirectUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "httpStatusCode"    # I
    .param p2, "httpStatusMessage"    # Ljava/lang/String;
    .param p3, "origionalUrl"    # Ljava/lang/String;
    .param p4, "redirectUrl"    # Ljava/lang/String;

    .prologue
    .line 13
    .line 14
    const-string v0, "Http request to url %s returned a redirect to url %s HttpStatus %d: %s"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 15
    aput-object p3, v1, v2

    const/4 v2, 0x1

    .line 16
    aput-object p4, v1, v2

    const/4 v2, 0x2

    .line 17
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    .line 18
    aput-object p2, v1, v2

    .line 14
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/microsoft/playready/HttpException;-><init>(ILjava/lang/String;)V

    .line 20
    iput-object p4, p0, Lcom/microsoft/playready/HttpRedirectRequestedException;->mRedirectUrl:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public getRedirectUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/microsoft/playready/HttpRedirectRequestedException;->mRedirectUrl:Ljava/lang/String;

    return-object v0
.end method
