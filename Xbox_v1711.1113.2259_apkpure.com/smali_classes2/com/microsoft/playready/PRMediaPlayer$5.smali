.class Lcom/microsoft/playready/PRMediaPlayer$5;
.super Ljava/lang/Object;
.source "PRMediaPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/playready/PRMediaPlayer;-><init>(Lcom/microsoft/playready/PlayReadyFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/playready/PRMediaPlayer;


# direct methods
.method constructor <init>(Lcom/microsoft/playready/PRMediaPlayer;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/microsoft/playready/PRMediaPlayer$5;->this$0:Lcom/microsoft/playready/PRMediaPlayer;

    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 1
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 124
    iget-object v0, p0, Lcom/microsoft/playready/PRMediaPlayer$5;->this$0:Lcom/microsoft/playready/PRMediaPlayer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/microsoft/playready/PRMediaPlayer;->notifyOnVideoSizeChangedListeners(Landroid/media/MediaPlayer;II)V

    .line 125
    return-void
.end method
