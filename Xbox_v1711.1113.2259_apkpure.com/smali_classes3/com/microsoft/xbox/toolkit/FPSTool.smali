.class public Lcom/microsoft/xbox/toolkit/FPSTool;
.super Ljava/lang/Object;
.source "FPSTool.java"


# static fields
.field private static final FPS_MAX:I = 0x3c

.field private static instance:Lcom/microsoft/xbox/toolkit/FPSTool;


# instance fields
.field private FPSSum:I

.field private minFPS:I

.field private samples:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4
    new-instance v0, Lcom/microsoft/xbox/toolkit/FPSTool;

    invoke-direct {v0}, Lcom/microsoft/xbox/toolkit/FPSTool;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/toolkit/FPSTool;->instance:Lcom/microsoft/xbox/toolkit/FPSTool;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput v0, p0, Lcom/microsoft/xbox/toolkit/FPSTool;->samples:I

    .line 11
    iput v0, p0, Lcom/microsoft/xbox/toolkit/FPSTool;->FPSSum:I

    .line 12
    iput v0, p0, Lcom/microsoft/xbox/toolkit/FPSTool;->minFPS:I

    return-void
.end method

.method public static getInstance()Lcom/microsoft/xbox/toolkit/FPSTool;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/microsoft/xbox/toolkit/FPSTool;->instance:Lcom/microsoft/xbox/toolkit/FPSTool;

    return-object v0
.end method


# virtual methods
.method public addFPS(I)V
    .locals 1
    .param p1, "fps"    # I

    .prologue
    .line 22
    const/16 v0, 0x3c

    if-le p1, v0, :cond_0

    .line 30
    :goto_0
    return-void

    .line 27
    :cond_0
    iget v0, p0, Lcom/microsoft/xbox/toolkit/FPSTool;->samples:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/microsoft/xbox/toolkit/FPSTool;->samples:I

    .line 28
    iget v0, p0, Lcom/microsoft/xbox/toolkit/FPSTool;->FPSSum:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/microsoft/xbox/toolkit/FPSTool;->FPSSum:I

    .line 29
    iget v0, p0, Lcom/microsoft/xbox/toolkit/FPSTool;->minFPS:I

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/microsoft/xbox/toolkit/FPSTool;->minFPS:I

    goto :goto_0
.end method

.method public clearFPS()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16
    iput v0, p0, Lcom/microsoft/xbox/toolkit/FPSTool;->samples:I

    .line 17
    iput v0, p0, Lcom/microsoft/xbox/toolkit/FPSTool;->FPSSum:I

    .line 18
    const v0, 0x7fffffff

    iput v0, p0, Lcom/microsoft/xbox/toolkit/FPSTool;->minFPS:I

    .line 19
    return-void
.end method

.method public getAverageFPS()I
    .locals 2

    .prologue
    .line 33
    iget v0, p0, Lcom/microsoft/xbox/toolkit/FPSTool;->FPSSum:I

    int-to-float v0, v0

    iget v1, p0, Lcom/microsoft/xbox/toolkit/FPSTool;->samples:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public getMinFPS()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/microsoft/xbox/toolkit/FPSTool;->minFPS:I

    return v0
.end method
