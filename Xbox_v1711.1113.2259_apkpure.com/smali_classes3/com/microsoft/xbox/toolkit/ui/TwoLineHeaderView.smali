.class public Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;
.super Landroid/widget/RelativeLayout;
.source "TwoLineHeaderView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView$Behavior;
    }
.end annotation


# instance fields
.field private headerText:Ljava/lang/String;

.field private headerView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private subHeaderText:Ljava/lang/String;

.field private subHeaderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->initialize(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->initialize(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method private initialize(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 49
    sget-object v1, Lcom/microsoft/xboxone/smartglass/R$styleable;->TwoLineHeaderView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 51
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->headerText:Ljava/lang/String;

    .line 52
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->subHeaderText:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 56
    return-void

    .line 54
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v1
.end method


# virtual methods
.method public getHeaderText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->headerView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getHeaderView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->headerView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method public getSubHeaderText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->subHeaderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getSubHeaderView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->subHeaderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 39
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 40
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03028c

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 41
    const v0, 0x7f0e0c10

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->headerView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 42
    const v0, 0x7f0e0c11

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->subHeaderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 44
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->headerView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->headerText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->subHeaderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->subHeaderText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    return-void
.end method

.method public setHeaderStyle(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 77
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->headerView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/util/LibCompat;->setTextAppearance(Landroid/widget/TextView;I)V

    .line 79
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->invalidate()V

    .line 80
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->requestLayout()V

    .line 82
    :cond_0
    return-void
.end method

.method public setHeaderText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->headerView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->invalidate()V

    .line 73
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->requestLayout()V

    .line 74
    return-void
.end method

.method public setHeaderTypefaceSource(Ljava/lang/String;)V
    .locals 2
    .param p1, "typefaceSource"    # Ljava/lang/String;

    .prologue
    .line 85
    if-eqz p1, :cond_0

    .line 86
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-static {v1, p1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 87
    .local v0, "tf":Landroid/graphics/Typeface;
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->headerView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 88
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->invalidate()V

    .line 89
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->requestLayout()V

    .line 91
    .end local v0    # "tf":Landroid/graphics/Typeface;
    :cond_0
    return-void
.end method

.method public setHeaderVisibility(Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "isVisible"    # Ljava/lang/Boolean;

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 121
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->headerView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 123
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->subHeaderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 128
    :goto_0
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->invalidate()V

    .line 129
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->requestLayout()V

    .line 130
    return-void

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->headerView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    .line 126
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->subHeaderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setSubHeaderStyle(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 104
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->subHeaderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/util/LibCompat;->setTextAppearance(Landroid/widget/TextView;I)V

    .line 106
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->invalidate()V

    .line 107
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->requestLayout()V

    .line 109
    :cond_0
    return-void
.end method

.method public setSubHeaderText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->subHeaderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v0, p1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->invalidate()V

    .line 100
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->requestLayout()V

    .line 101
    return-void
.end method

.method public setSubHeaderTypefaceSource(Ljava/lang/String;)V
    .locals 2
    .param p1, "typefaceSource"    # Ljava/lang/String;

    .prologue
    .line 112
    if-eqz p1, :cond_0

    .line 113
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-static {v1, p1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 114
    .local v0, "tf":Landroid/graphics/Typeface;
    iget-object v1, p0, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->subHeaderView:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 115
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->invalidate()V

    .line 116
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/TwoLineHeaderView;->requestLayout()V

    .line 118
    .end local v0    # "tf":Landroid/graphics/Typeface;
    :cond_0
    return-void
.end method
