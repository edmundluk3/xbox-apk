.class public Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment;
.super Lcom/microsoft/xbox/idp/compat/BaseFragment;
.source "ErrorButtonsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment$Callbacks;
    }
.end annotation


# static fields
.field public static final ARG_LEFT_ERROR_BUTTON_STRING_ID:Ljava/lang/String; = "ARG_LEFT_ERROR_BUTTON_STRING_ID"

.field private static final NO_OP_CALLBACKS:Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment$Callbacks;


# instance fields
.field private callbacks:Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment$Callbacks;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    new-instance v0, Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment$1;

    invoke-direct {v0}, Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment$1;-><init>()V

    sput-object v0, Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment;->NO_OP_CALLBACKS:Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment$Callbacks;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/microsoft/xbox/idp/compat/BaseFragment;-><init>()V

    .line 19
    sget-object v0, Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment;->NO_OP_CALLBACKS:Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment$Callbacks;

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment$Callbacks;

    return-void
.end method


# virtual methods
.method public onAttach(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    invoke-super {p0, p1}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onAttach(Landroid/content/Context;)V

    .line 24
    check-cast p1, Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment$Callbacks;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment$Callbacks;

    .line 25
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 52
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 53
    .local v0, "id":I
    sget v1, Lcom/microsoft/xbox/idp/R$id;->xbid_error_left_button:I

    if-ne v0, v1, :cond_1

    .line 54
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment$Callbacks;

    invoke-interface {v1}, Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment$Callbacks;->onClickedLeftButton()V

    .line 58
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    sget v1, Lcom/microsoft/xbox/idp/R$id;->xbid_error_right_button:I

    if-ne v0, v1, :cond_0

    .line 56
    iget-object v1, p0, Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment$Callbacks;

    invoke-interface {v1}, Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment$Callbacks;->onClickedRightButton()V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    sget v0, Lcom/microsoft/xbox/idp/R$layout;->xbid_fragment_error_buttons:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 29
    invoke-super {p0}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onDetach()V

    .line 30
    sget-object v0, Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment;->NO_OP_CALLBACKS:Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment$Callbacks;

    iput-object v0, p0, Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment;->callbacks:Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment$Callbacks;

    .line 31
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    invoke-super {p0, p1, p2}, Lcom/microsoft/xbox/idp/compat/BaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 41
    sget v2, Lcom/microsoft/xbox/idp/R$id;->xbid_error_left_button:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 42
    .local v1, "leftButton":Landroid/widget/Button;
    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 43
    sget v2, Lcom/microsoft/xbox/idp/R$id;->xbid_error_right_button:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    invoke-virtual {p0}, Lcom/microsoft/xbox/idp/ui/ErrorButtonsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 45
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    const-string v2, "ARG_LEFT_ERROR_BUTTON_STRING_ID"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 46
    const-string v2, "ARG_LEFT_ERROR_BUTTON_STRING_ID"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 48
    :cond_0
    return-void
.end method
