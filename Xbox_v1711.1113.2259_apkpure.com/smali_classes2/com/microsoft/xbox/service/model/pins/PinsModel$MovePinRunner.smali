.class Lcom/microsoft/xbox/service/model/pins/PinsModel$MovePinRunner;
.super Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;
.source "PinsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/model/pins/PinsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MovePinRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final beforeDstItem:Z

.field private final dstItem:Lcom/microsoft/xbox/service/model/pins/PinItem;

.field private final requestId:J

.field private final srcItem:Lcom/microsoft/xbox/service/model/pins/PinItem;

.field final synthetic this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/service/model/pins/PinsModel;JLcom/microsoft/xbox/service/model/pins/PinItem;Lcom/microsoft/xbox/service/model/pins/PinItem;Z)V
    .locals 0
    .param p2, "requestId"    # J
    .param p4, "srcItem"    # Lcom/microsoft/xbox/service/model/pins/PinItem;
    .param p5, "dstItem"    # Lcom/microsoft/xbox/service/model/pins/PinItem;
    .param p6, "beforeDstItem"    # Z

    .prologue
    .line 444
    iput-object p1, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$MovePinRunner;->this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/network/IDataLoaderRunnable;-><init>()V

    .line 445
    iput-wide p2, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$MovePinRunner;->requestId:J

    .line 446
    iput-object p4, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$MovePinRunner;->srcItem:Lcom/microsoft/xbox/service/model/pins/PinItem;

    .line 447
    iput-object p5, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$MovePinRunner;->dstItem:Lcom/microsoft/xbox/service/model/pins/PinItem;

    .line 448
    iput-boolean p6, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$MovePinRunner;->beforeDstItem:Z

    .line 449
    return-void
.end method


# virtual methods
.method public buildData()Ljava/lang/Boolean;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 457
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getInstance()Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/microsoft/xbox/service/network/managers/ServiceManagerFactory;->getESServiceManager()Lcom/microsoft/xbox/service/network/managers/IESServiceManager;

    move-result-object v0

    .line 458
    .local v0, "manager":Lcom/microsoft/xbox/service/network/managers/IESServiceManager;
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$MovePinRunner;->srcItem:Lcom/microsoft/xbox/service/model/pins/PinItem;

    iget-object v2, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$MovePinRunner;->dstItem:Lcom/microsoft/xbox/service/model/pins/PinItem;

    iget-boolean v3, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$MovePinRunner;->beforeDstItem:Z

    invoke-interface {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/network/managers/IESServiceManager;->move(Lcom/microsoft/xbox/service/model/pins/PinItem;Lcom/microsoft/xbox/service/model/pins/PinItem;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic buildData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/microsoft/xbox/toolkit/XLEException;
        }
    .end annotation

    .prologue
    .line 438
    invoke-virtual {p0}, Lcom/microsoft/xbox/service/model/pins/PinsModel$MovePinRunner;->buildData()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultErrorCode()J
    .locals 2

    .prologue
    .line 468
    const-wide/16 v0, 0xfbc

    return-wide v0
.end method

.method public onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncResult;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/microsoft/xbox/toolkit/AsyncResult",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 463
    .local p1, "result":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/lang/Boolean;>;"
    iget-object v1, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$MovePinRunner;->this$0:Lcom/microsoft/xbox/service/model/pins/PinsModel;

    iget-wide v2, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$MovePinRunner;->requestId:J

    iget-object v5, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$MovePinRunner;->srcItem:Lcom/microsoft/xbox/service/model/pins/PinItem;

    iget-object v6, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$MovePinRunner;->dstItem:Lcom/microsoft/xbox/service/model/pins/PinItem;

    iget-boolean v7, p0, Lcom/microsoft/xbox/service/model/pins/PinsModel$MovePinRunner;->beforeDstItem:Z

    move-object v4, p1

    invoke-static/range {v1 .. v7}, Lcom/microsoft/xbox/service/model/pins/PinsModel;->access$1100(Lcom/microsoft/xbox/service/model/pins/PinsModel;JLcom/microsoft/xbox/toolkit/AsyncResult;Lcom/microsoft/xbox/service/model/pins/PinItem;Lcom/microsoft/xbox/service/model/pins/PinItem;Z)V

    .line 464
    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .prologue
    .line 453
    return-void
.end method
