.class public Lcom/microsoft/xbox/xle/app/FriendFinderSettings;
.super Ljava/lang/Object;
.source "FriendFinderSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/FriendFinderSettings$IconImageSize;
    }
.end annotation


# static fields
.field private static icons:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/microsoft/xbox/service/model/friendfinder/RecommendationTypeIcon;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public ICONS:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getIconBySize(Ljava/lang/String;Lcom/microsoft/xbox/xle/app/FriendFinderSettings$IconImageSize;)Ljava/lang/String;
    .locals 3
    .param p0, "type"    # Ljava/lang/String;
    .param p1, "size"    # Lcom/microsoft/xbox/xle/app/FriendFinderSettings$IconImageSize;

    .prologue
    .line 49
    sget-object v1, Lcom/microsoft/xbox/xle/app/FriendFinderSettings;->icons:Ljava/util/HashMap;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/microsoft/xbox/xle/app/FriendFinderSettings;->icons:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 50
    sget-object v1, Lcom/microsoft/xbox/xle/app/FriendFinderSettings;->icons:Ljava/util/HashMap;

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/model/friendfinder/RecommendationTypeIcon;

    .line 51
    .local v0, "icon":Lcom/microsoft/xbox/service/model/friendfinder/RecommendationTypeIcon;
    if-eqz v0, :cond_0

    .line 52
    sget-object v1, Lcom/microsoft/xbox/xle/app/FriendFinderSettings$2;->$SwitchMap$com$microsoft$xbox$xle$app$FriendFinderSettings$IconImageSize:[I

    invoke-virtual {p1}, Lcom/microsoft/xbox/xle/app/FriendFinderSettings$IconImageSize;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 64
    .end local v0    # "icon":Lcom/microsoft/xbox/service/model/friendfinder/RecommendationTypeIcon;
    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 54
    .restart local v0    # "icon":Lcom/microsoft/xbox/service/model/friendfinder/RecommendationTypeIcon;
    :pswitch_0
    iget-object v1, v0, Lcom/microsoft/xbox/service/model/friendfinder/RecommendationTypeIcon;->small:Ljava/lang/String;

    goto :goto_0

    .line 56
    :pswitch_1
    iget-object v1, v0, Lcom/microsoft/xbox/service/model/friendfinder/RecommendationTypeIcon;->medium:Ljava/lang/String;

    goto :goto_0

    .line 58
    :pswitch_2
    iget-object v1, v0, Lcom/microsoft/xbox/service/model/friendfinder/RecommendationTypeIcon;->large:Ljava/lang/String;

    goto :goto_0

    .line 52
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static hasIcons()Z
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/microsoft/xbox/xle/app/FriendFinderSettings;->icons:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/microsoft/xbox/xle/app/FriendFinderSettings;->icons:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getIconsFromJson(Ljava/lang/String;)V
    .locals 8
    .param p1, "jsonStr"    # Ljava/lang/String;

    .prologue
    .line 32
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    sput-object v5, Lcom/microsoft/xbox/xle/app/FriendFinderSettings;->icons:Ljava/util/HashMap;

    .line 34
    :try_start_0
    new-instance v1, Lcom/google/gson/Gson;

    invoke-direct {v1}, Lcom/google/gson/Gson;-><init>()V

    .line 35
    .local v1, "gson":Lcom/google/gson/Gson;
    new-instance v5, Lcom/microsoft/xbox/xle/app/FriendFinderSettings$1;

    invoke-direct {v5, p0}, Lcom/microsoft/xbox/xle/app/FriendFinderSettings$1;-><init>(Lcom/microsoft/xbox/xle/app/FriendFinderSettings;)V

    .line 36
    invoke-virtual {v5}, Lcom/microsoft/xbox/xle/app/FriendFinderSettings$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v4

    .line 37
    .local v4, "resultType":Ljava/lang/reflect/Type;
    invoke-virtual {v1, p1, v4}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    .line 38
    .local v3, "iconList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/friendfinder/RecommendationTypeIcon;>;"
    if-eqz v3, :cond_0

    .line 39
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/microsoft/xbox/service/model/friendfinder/RecommendationTypeIcon;

    .line 40
    .local v2, "icon":Lcom/microsoft/xbox/service/model/friendfinder/RecommendationTypeIcon;
    sget-object v6, Lcom/microsoft/xbox/xle/app/FriendFinderSettings;->icons:Ljava/util/HashMap;

    iget-object v7, v2, Lcom/microsoft/xbox/service/model/friendfinder/RecommendationTypeIcon;->type:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 43
    .end local v1    # "gson":Lcom/google/gson/Gson;
    .end local v2    # "icon":Lcom/microsoft/xbox/service/model/friendfinder/RecommendationTypeIcon;
    .end local v3    # "iconList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/microsoft/xbox/service/model/friendfinder/RecommendationTypeIcon;>;"
    .end local v4    # "resultType":Ljava/lang/reflect/Type;
    :catch_0
    move-exception v0

    .line 44
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "IFriendFinderSettingsResult"

    const-string v6, "failed to get recommendation type icons from Json string"

    invoke-static {v5, v6}, Lcom/microsoft/xbox/toolkit/XLELog;->Warning(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    return-void
.end method
