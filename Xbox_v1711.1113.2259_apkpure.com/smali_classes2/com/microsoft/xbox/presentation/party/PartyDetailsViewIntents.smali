.class public Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents;
.super Ljava/lang/Object;
.source "PartyDetailsViewIntents.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$NavigateToUserProfileIntent;,
        Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$KickMemberIntent;,
        Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$ToggleMemberMuteIntent;,
        Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$SetAllowBroadcastIntent;,
        Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyMuteIntent;,
        Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TogglePartyJoinabilityIntent;,
        Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$LeavePartyIntent;,
        Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$TextChatIntent;,
        Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$InviteIntent;,
        Lcom/microsoft/xbox/presentation/party/PartyDetailsViewIntents$NavigateToLfgIntent;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
