.class public Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;
.super Ljava/lang/Object;
.source "MyClubsInteractor.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final initialLoadTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;)V
    .locals 1
    .param p1, "xuidProvider"    # Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;
    .param p2, "clubModelManager"    # Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p2, p1}, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;->initialLoadTransformer:Lio/reactivex/ObservableTransformer;

    .line 45
    return-void
.end method

.method static synthetic lambda$new$4(Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "clubModelManager"    # Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;
    .param p1, "xuidProvider"    # Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;
    .param p2, "actions"    # Lio/reactivex/Observable;

    .prologue
    .line 29
    invoke-static {p0, p1}, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 30
    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 29
    return-object v0
.end method

.method static synthetic lambda$null$0(Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;)Ljava/util/List;
    .locals 4
    .param p0, "clubModelManager"    # Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;
    .param p1, "xuidProvider"    # Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 32
    invoke-interface {p1}, Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;->getMyXuid()J

    move-result-wide v2

    invoke-interface {p0, v2, v3}, Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;->loadUserClubs(J)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    .line 34
    .local v0, "asyncResult":Lcom/microsoft/xbox/toolkit/AsyncResult;, "Lcom/microsoft/xbox/toolkit/AsyncResult<Ljava/util/List<Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;>;>;"
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 35
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getException()Lcom/microsoft/xbox/toolkit/XLEException;

    move-result-object v1

    throw v1

    .line 37
    :cond_0
    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getResult()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method static synthetic lambda$null$1(Ljava/util/List;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;
    .locals 1
    .param p0, "list"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 40
    invoke-static {p0}, Lcom/google/common/collect/ImmutableList;->copyOf(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-static {v0}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->withContent(Ljava/lang/Object;)Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$2(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;)V
    .locals 1
    .param p0, "action"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .param p1, "result"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 43
    sget-object v0, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;->TAG:Ljava/lang/String;

    invoke-static {v0, p0, p1}, Lcom/microsoft/xbox/toolkit/logging/MviLogger;->logResult(Ljava/lang/String;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseAction;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$BaseResult;)V

    return-void
.end method

.method static synthetic lambda$null$3(Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p0, "clubModelManager"    # Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;
    .param p1, "xuidProvider"    # Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;
    .param p2, "action"    # Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 31
    invoke-static {p0, p1}, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/service/model/clubs/IClubModelManager;Lcom/microsoft/xbox/xbservices/toolkit/MyXuidProvider;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor$$Lambda$4;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 40
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {}, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor$$Lambda$5;->lambdaFactory$()Lio/reactivex/functions/Function;

    move-result-object v1

    .line 41
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 42
    invoke-static {}, Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;->inFlightInstance()Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {p2}, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor$$Lambda$6;->lambdaFactory$(Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    .line 43
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 31
    return-object v0
.end method


# virtual methods
.method public initialLoadTransformer()Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/ObservableTransformer",
            "<",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadAction;",
            "Lcom/microsoft/xbox/domain/common/CommonActionsAndResults$InitialLoadResult",
            "<",
            "Lcom/google/common/collect/ImmutableList",
            "<",
            "Lcom/microsoft/xbox/service/clubs/ClubHubDataTypes$Club;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/microsoft/xbox/domain/clubs/MyClubsInteractor;->initialLoadTransformer:Lio/reactivex/ObservableTransformer;

    return-object v0
.end method
