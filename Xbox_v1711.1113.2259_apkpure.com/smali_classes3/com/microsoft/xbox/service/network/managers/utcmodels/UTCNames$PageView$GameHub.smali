.class public Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageView$GameHub;
.super Ljava/lang/Object;
.source "UTCNames.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/service/network/managers/utcmodels/UTCNames$PageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GameHub"
.end annotation


# static fields
.field public static final AchievementsView:Ljava/lang/String; = "Game Hub - Achievements View"

.field public static final ActivityView:Ljava/lang/String; = "Game Hub - Activity View"

.field public static final CapturesView:Ljava/lang/String; = "Game Hub - Captures View"

.field public static final FriendsView:Ljava/lang/String; = "Game Hub - Friends View"

.field public static final GameHubView:Ljava/lang/String; = "Game Hub"

.field public static final InfoView:Ljava/lang/String; = "Game Hub - Info View"

.field public static final LFGView:Ljava/lang/String; = "Game Hub - LFG View"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
