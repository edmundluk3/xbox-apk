.class public Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;
.super Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;
.source "UnsharedActivityFeedScreenAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter$UnsharedActivityFeedListAdapter;
    }
.end annotation


# instance fields
.field private final closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private final header:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;"
        }
    .end annotation
.end field

.field private final listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ArrayAdapterWithScroll;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/microsoft/xbox/xle/app/adapter/ArrayAdapterWithScroll",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;",
            ">;"
        }
    .end annotation
.end field

.field private final swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

.field private final viewModel:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;


# direct methods
.method public constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;)V
    .locals 3
    .param p1, "viewModel"    # Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;

    .line 36
    const v0, 0x7f0e016f

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->screenBody:Landroid/view/View;

    .line 37
    const v0, 0x7f0e0170

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 38
    const v0, 0x7f0e0171

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->header:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    .line 39
    const v0, 0x7f0e0172

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 40
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x4

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 43
    :cond_0
    const v0, 0x7f0e0173

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->setListView(Lcom/microsoft/xbox/toolkit/ui/XLEListView;)V

    .line 44
    new-instance v0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter$UnsharedActivityFeedListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter$UnsharedActivityFeedListAdapter;-><init>(Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ArrayAdapterWithScroll;

    .line 45
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ArrayAdapterWithScroll;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 46
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter$$Lambda$1;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 47
    return-void

    .line 41
    nop

    :array_0
    .array-data 4
        0x7f0c0108
        0x7f0c0109
        0x7f0c010a
        0x7f0c010b
    .end array-data
.end method

.method static synthetic lambda$new$0(Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 46
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;

    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ArrayAdapterWithScroll;

    invoke-virtual {v0, p3}, Lcom/microsoft/xbox/xle/app/adapter/ArrayAdapterWithScroll;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;

    invoke-virtual {v1, v0}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;->onItemClick(Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;)V

    return-void
.end method

.method static synthetic lambda$onStart$1(Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;Landroid/view/View;)V
    .locals 1
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;->closeDialog()V

    return-void
.end method

.method static synthetic lambda$onStart$2(Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;)V
    .locals 2
    .param p0, "this"    # Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;->load(Z)V

    return-void
.end method


# virtual methods
.method protected getSwitchPanel()Lcom/microsoft/xbox/toolkit/ui/SwitchPanel;
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;

    return-object v0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->onStart()V

    .line 52
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter$$Lambda$2;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-static {p0}, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter$$Lambda$3;->lambdaFactory$(Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;)Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;)V

    .line 59
    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 63
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->closeButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_1

    .line 68
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;)V

    .line 70
    :cond_1
    invoke-super {p0}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->onStop()V

    .line 71
    return-void
.end method

.method protected updateLoadingIndicator(Z)V
    .locals 2
    .param p1, "isLoading"    # Z

    .prologue
    .line 121
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-nez v0, :cond_0

    .line 122
    invoke-super {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/AdapterBaseWithList;->updateLoadingIndicator(Z)V

    .line 127
    :goto_0
    return-void

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 125
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected updateViewOverride()V
    .locals 5

    .prologue
    .line 86
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;->isBusy()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->updateLoadingIndicator(Z)V

    .line 88
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;->getData()Ljava/util/List;

    move-result-object v1

    .line 89
    .local v1, "newData":Ljava/util/List;, "Ljava/util/List<Lcom/microsoft/xbox/service/network/managers/ProfileRecentsResultContainer$ProfileRecentItem;>;"
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->items:Ljava/util/List;

    if-ne v2, v1, :cond_0

    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;->drainFeedDataChangedInternally()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 90
    :cond_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ArrayAdapterWithScroll;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/app/adapter/ArrayAdapterWithScroll;->clear()V

    .line 91
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->listAdapter:Lcom/microsoft/xbox/xle/app/adapter/ArrayAdapterWithScroll;

    invoke-virtual {v2, v1}, Lcom/microsoft/xbox/xle/app/adapter/ArrayAdapterWithScroll;->addAll(Ljava/util/Collection;)V

    .line 92
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->listView:Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    invoke-virtual {v2}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->onDataUpdated()V

    .line 94
    iput-object v1, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->items:Ljava/util/List;

    .line 97
    :cond_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v2, :cond_2

    .line 98
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;

    invoke-virtual {v2}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;->isBusy()Z

    move-result v0

    .line 99
    .local v0, "busy":Z
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v2, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 100
    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->swipeContainer:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-nez v0, :cond_3

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v3, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 103
    .end local v0    # "busy":Z
    :cond_2
    sget-object v2, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter$1;->$SwitchMap$com$microsoft$xbox$toolkit$network$ListState:[I

    iget-object v3, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->viewModel:Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;

    invoke-virtual {v3}, Lcom/microsoft/xbox/xle/viewmodel/UnsharedActivityFeedScreenViewModel;->getViewModelState()Lcom/microsoft/xbox/toolkit/network/ListState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/microsoft/xbox/toolkit/network/ListState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 117
    :goto_1
    return-void

    .line 100
    .restart local v0    # "busy":Z
    :cond_3
    const/4 v2, 0x0

    goto :goto_0

    .line 105
    .end local v0    # "busy":Z
    :pswitch_0
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->header:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    const v4, 0x7f070d20

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 108
    :pswitch_1
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->header:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    const v4, 0x7f070d1f

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 111
    :pswitch_2
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->header:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    const v4, 0x7f070d1e

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 114
    :pswitch_3
    iget-object v2, p0, Lcom/microsoft/xbox/xle/app/adapter/UnsharedActivityFeedScreenAdapter;->header:Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v3

    const v4, 0x7f07013a

    invoke-virtual {v3, v4}, Lcom/microsoft/xbox/xle/app/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 103
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
