.class Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;
.super Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;
.source "GameProfileAchievementsScreenViewModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadTitleDataAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/microsoft/xbox/toolkit/NetworkAsyncTask",
        "<",
        "Lcom/microsoft/xbox/toolkit/AsyncActionStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;)V
    .locals 0

    .prologue
    .line 753
    iput-object p1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-direct {p0}, Lcom/microsoft/xbox/toolkit/NetworkAsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;
    .param p2, "x1"    # Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$1;

    .prologue
    .line 753
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;-><init>(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;)V

    return-void
.end method


# virtual methods
.method protected checkShouldExecute()Z
    .locals 4

    .prologue
    .line 757
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 758
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-static {v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/microsoft/xbox/service/model/TitleHubModel;->shouldRefresh(J)Z

    move-result v0

    return v0
.end method

.method protected loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 4

    .prologue
    .line 785
    invoke-static {}, Lcom/microsoft/xbox/service/model/TitleHubModel;->instance()Lcom/microsoft/xbox/service/model/TitleHubModel;

    move-result-object v0

    iget-boolean v1, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;->forceLoad:Z

    iget-object v2, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-static {v2}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$200(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/microsoft/xbox/service/model/TitleHubModel;->load(ZJ)Lcom/microsoft/xbox/toolkit/AsyncResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/toolkit/AsyncResult;->getStatus()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic loadDataInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 753
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;->loadDataInBackground()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;
    .locals 1

    .prologue
    .line 780
    sget-object v0, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->FAIL:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    return-object v0
.end method

.method protected bridge synthetic onError()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 753
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;->onError()Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    move-result-object v0

    return-object v0
.end method

.method protected onNoAction()V
    .locals 2

    .prologue
    .line 763
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 764
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    sget-object v1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;->NO_CHANGE:Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$900(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 765
    return-void
.end method

.method protected onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "result"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 775
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    invoke-static {v0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$900(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    .line 776
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 753
    check-cast p1, Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    invoke-virtual {p0, p1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;->onPostExecute(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 769
    invoke-static {}, Lcom/microsoft/xbox/toolkit/XLEAssert;->assertIsUIThread()V

    .line 770
    iget-object v0, p0, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel$LoadTitleDataAsyncTask;->this$0:Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;->access$1002(Lcom/microsoft/xbox/xle/viewmodel/GameProfileAchievementsScreenViewModel;Z)Z

    .line 771
    return-void
.end method
