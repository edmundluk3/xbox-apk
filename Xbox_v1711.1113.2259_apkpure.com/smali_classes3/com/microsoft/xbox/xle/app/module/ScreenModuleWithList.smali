.class public abstract Lcom/microsoft/xbox/xle/app/module/ScreenModuleWithList;
.super Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;
.source "ScreenModuleWithList.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    return-void
.end method

.method private saveListPosition()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleWithList;->getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v3

    .line 33
    .local v3, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleWithList;->getListView()Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 34
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleWithList;->getListView()Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->getFirstVisiblePosition()I

    move-result v0

    .line 35
    .local v0, "index":I
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleWithList;->getListView()Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 36
    .local v2, "v":Landroid/view/View;
    if-nez v2, :cond_1

    .line 37
    .local v1, "offset":I
    :goto_0
    invoke-virtual {v3, v0, v1}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->setListPosition(II)V

    .line 40
    .end local v0    # "index":I
    .end local v1    # "offset":I
    .end local v2    # "v":Landroid/view/View;
    :cond_0
    return-void

    .line 36
    .restart local v0    # "index":I
    .restart local v2    # "v":Landroid/view/View;
    :cond_1
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method public abstract getListView()Lcom/microsoft/xbox/toolkit/ui/XLEListView;
.end method

.method public abstract getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 44
    invoke-super {p0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleLayout;->onStop()V

    .line 45
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleWithList;->saveListPosition()V

    .line 46
    return-void
.end method

.method protected restoreListPosition()V
    .locals 4

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleWithList;->getViewModel()Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;

    move-result-object v0

    .line 26
    .local v0, "vm":Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleWithList;->getListView()Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 27
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/module/ScreenModuleWithList;->getListView()Lcom/microsoft/xbox/toolkit/ui/XLEListView;

    move-result-object v1

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->getAndResetListPosition()I

    move-result v2

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;->getAndResetListOffset()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/microsoft/xbox/xle/app/XLEUtil;->scrollToPositionWithOffset(Landroid/widget/ListView;II)V

    .line 29
    :cond_0
    return-void
.end method

.method public abstract setViewModel(Lcom/microsoft/xbox/xle/viewmodel/ViewModelBase;)V
.end method

.method public abstract updateView()V
.end method
