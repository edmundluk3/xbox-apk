.class public abstract Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewState;
.super Ljava/lang/Object;
.source "LanguageSettingsViewState.java"

# interfaces
.implements Lcom/microsoft/xbox/toolkit/logging/Loggable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static with(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;)Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsViewState;
    .locals 1
    .param p0, "prefs"    # Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 16
    invoke-static {p0}, Lcom/microsoft/xbox/toolkit/Preconditions;->nonNull(Ljava/lang/Object;)V

    .line 17
    new-instance v0, Lcom/microsoft/xbox/presentation/settings/language/AutoValue_LanguageSettingsViewState;

    invoke-direct {v0, p0}, Lcom/microsoft/xbox/presentation/settings/language/AutoValue_LanguageSettingsViewState;-><init>(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;)V

    return-object v0
.end method


# virtual methods
.method public abstract prefs()Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsPrefs;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end method

.method public toLogString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
