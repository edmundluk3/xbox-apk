.class public Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;
.super Landroid/support/v7/widget/AppCompatTextView;
.source "CustomTypefaceTextView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/AppCompatTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->isInEditMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 23
    sget-object v1, Lcom/microsoft/xboxone/smartglass/R$styleable;->CustomTypeface:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 24
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->applyCustomStyleAttributes(Landroid/content/res/TypedArray;Z)V

    .line 26
    .end local v0    # "a":Landroid/content/res/TypedArray;
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/AppCompatTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->isInEditMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 32
    sget-object v1, Lcom/microsoft/xboxone/smartglass/R$styleable;->CustomTypeface:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 33
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->applyCustomStyleAttributes(Landroid/content/res/TypedArray;Z)V

    .line 35
    .end local v0    # "a":Landroid/content/res/TypedArray;
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "typeface"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Landroid/support/v7/widget/AppCompatTextView;-><init>(Landroid/content/Context;)V

    .line 16
    invoke-direct {p0, p2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->applyCustomTypeface(Ljava/lang/String;)V

    .line 17
    return-void
.end method

.method private applyCustomStyleAttributes(Landroid/content/res/TypedArray;Z)V
    .locals 3
    .param p1, "array"    # Landroid/content/res/TypedArray;
    .param p2, "updateUppercase"    # Z

    .prologue
    .line 38
    if-eqz p2, :cond_0

    .line 39
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 40
    .local v1, "uppercaseText":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 41
    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setText(Ljava/lang/CharSequence;)V

    .line 45
    .end local v1    # "uppercaseText":Ljava/lang/String;
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 46
    .local v0, "typeface":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->applyCustomTypeface(Ljava/lang/String;)V

    .line 48
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 49
    return-void
.end method

.method private applyCustomTypeface(Ljava/lang/String;)V
    .locals 3
    .param p1, "typefaceSource"    # Ljava/lang/String;

    .prologue
    .line 52
    if-eqz p1, :cond_0

    .line 53
    invoke-static {}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->Instance()Lcom/microsoft/xbox/toolkit/ui/FontManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/microsoft/xbox/toolkit/ui/FontManager;->getTypeface(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 54
    .local v0, "tf":Landroid/graphics/Typeface;
    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 59
    .end local v0    # "tf":Landroid/graphics/Typeface;
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->setCursorVisible(Z)V

    .line 60
    return-void
.end method


# virtual methods
.method public setClickable(Z)V
    .locals 1
    .param p1, "clickable"    # Z

    .prologue
    .line 79
    if-eqz p1, :cond_0

    .line 80
    const-string v0, "If you want CustomTypefaceTextView to be clickable, use XLEButton instead."

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 82
    :cond_0
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 74
    const-string v0, "If you want CustomTypefaceTextView to be clickable, use XLEButton instead."

    invoke-static {v0}, Lcom/microsoft/xbox/toolkit/XLEAssert;->fail(Ljava/lang/String;)V

    .line 75
    return-void
.end method

.method public setTextAppearance(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resId"    # I

    .prologue
    .line 64
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/AppCompatTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 66
    invoke-virtual {p0}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->isInEditMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 67
    sget-object v1, Lcom/microsoft/xboxone/smartglass/R$styleable;->CustomTypeface:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 68
    .local v0, "arr":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/microsoft/xbox/toolkit/ui/CustomTypefaceTextView;->applyCustomStyleAttributes(Landroid/content/res/TypedArray;Z)V

    .line 70
    .end local v0    # "arr":Landroid/content/res/TypedArray;
    :cond_0
    return-void
.end method
