.class public Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;
.super Lcom/microsoft/xbox/toolkit/XLEManagedDialog;
.source "PhoneContactInviteFriendsDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$PhoneContactSelectorListAdapter;,
        Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$UploadContactsAsyncTask;
    }
.end annotation


# instance fields
.field private busyIndicator:Landroid/view/View;

.field private cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

.field private errorPanel:Landroid/view/ViewGroup;

.field private listAdapter:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$PhoneContactSelectorListAdapter;

.field private listOrGridView:Landroid/widget/AbsListView;

.field private messagePanel:Landroid/view/ViewGroup;

.field private phoneFriendsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private uploadContactsAsyncTask:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$UploadContactsAsyncTask;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    const v0, 0x7f080238

    invoke-direct {p0, p1, v0}, Lcom/microsoft/xbox/toolkit/XLEManagedDialog;-><init>(Landroid/content/Context;I)V

    .line 53
    const v0, 0x7f0301ce

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->setContentView(I)V

    .line 55
    const v0, 0x7f0e0956

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->messagePanel:Landroid/view/ViewGroup;

    .line 56
    const v0, 0x7f0e0958

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->errorPanel:Landroid/view/ViewGroup;

    .line 57
    const v0, 0x7f0e095c

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 58
    const v0, 0x7f0e095d

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 59
    const v0, 0x7f0e0955

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 60
    const v0, 0x7f0e095e

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->busyIndicator:Landroid/view/View;

    .line 62
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$PhoneContactSelectorListAdapter;

    invoke-static {}, Lcom/microsoft/xbox/XLEApplication;->getMainActivity()Lcom/microsoft/xbox/xle/app/MainActivity;

    move-result-object v1

    const v2, 0x7f0301cf

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$PhoneContactSelectorListAdapter;-><init>(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;Landroid/app/Activity;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->listAdapter:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$PhoneContactSelectorListAdapter;

    .line 63
    const v0, 0x7f0e05d1

    invoke-virtual {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView;

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->listOrGridView:Landroid/widget/AbsListView;

    .line 64
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->listOrGridView:Landroid/widget/AbsListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    .line 65
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->listOrGridView:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->listAdapter:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$PhoneContactSelectorListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 66
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->listOrGridView:Landroid/widget/AbsListView;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$1;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$1;-><init>(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 79
    return-void
.end method

.method static synthetic access$000(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;)Landroid/widget/AbsListView;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->listOrGridView:Landroid/widget/AbsListView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->phoneFriendsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 0
    .param p0, "x0"    # Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;
    .param p1, "x1"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->onUploadContactsTaskCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V

    return-void
.end method

.method private loadUpdateContactsAsyncTask()V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->uploadContactsAsyncTask:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$UploadContactsAsyncTask;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->uploadContactsAsyncTask:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$UploadContactsAsyncTask;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$UploadContactsAsyncTask;->cancel()V

    .line 179
    :goto_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->uploadContactsAsyncTask:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$UploadContactsAsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$UploadContactsAsyncTask;->load(Z)V

    .line 180
    return-void

    .line 176
    :cond_0
    new-instance v0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$UploadContactsAsyncTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$UploadContactsAsyncTask;-><init>(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$1;)V

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->uploadContactsAsyncTask:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$UploadContactsAsyncTask;

    goto :goto_0
.end method

.method private onUploadContactsTaskCompleted(Lcom/microsoft/xbox/toolkit/AsyncActionStatus;)V
    .locals 1
    .param p1, "status"    # Lcom/microsoft/xbox/toolkit/AsyncActionStatus;

    .prologue
    .line 191
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->setBusy(Z)V

    .line 192
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->updateFriendsList()V

    .line 193
    return-void
.end method

.method private setBusy(Z)V
    .locals 2
    .param p1, "isBusy"    # Z

    .prologue
    .line 169
    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->busyIndicator:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 170
    return-void

    .line 169
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private updateFriendsList()V
    .locals 2

    .prologue
    .line 183
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getContacts()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->phoneFriendsList:Ljava/util/ArrayList;

    .line 185
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->listAdapter:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$PhoneContactSelectorListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$PhoneContactSelectorListAdapter;->clear()V

    .line 186
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->listAdapter:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$PhoneContactSelectorListAdapter;

    iget-object v1, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->phoneFriendsList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$PhoneContactSelectorListAdapter;->addAll(Ljava/util/Collection;)V

    .line 187
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->listAdapter:Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$PhoneContactSelectorListAdapter;

    invoke-virtual {v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$PhoneContactSelectorListAdapter;->notifyDataSetChanged()V

    .line 188
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->dismiss()V

    .line 88
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 92
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/utchelpers/UTCContactFriendFinder;->trackContactsInviteFriendsView()V

    .line 93
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$2;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$2;-><init>(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 128
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$3;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$3;-><init>(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_2

    .line 138
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    new-instance v1, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$4;

    invoke-direct {v1, p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog$4;-><init>(Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;)V

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    :cond_2
    invoke-static {}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->getInstance()Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/microsoft/xbox/service/network/managers/friendfinder/PhoneContactInfo;->isXboxContactsUpdated()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 148
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->updateFriendsList()V

    .line 153
    :goto_0
    return-void

    .line 150
    :cond_3
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->setBusy(Z)V

    .line 151
    invoke-direct {p0}, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->loadUpdateContactsAsyncTask()V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 157
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->confirmButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_1

    .line 161
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->cancelButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    if-eqz v0, :cond_2

    .line 164
    iget-object v0, p0, Lcom/microsoft/xbox/xle/app/dialog/PhoneContactInviteFriendsDialog;->closeDialogButton:Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    invoke-virtual {v0, v1}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 166
    :cond_2
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasFocus"    # Z

    .prologue
    .line 83
    return-void
.end method
