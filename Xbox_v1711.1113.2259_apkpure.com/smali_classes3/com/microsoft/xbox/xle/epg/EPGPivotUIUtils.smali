.class public Lcom/microsoft/xbox/xle/epg/EPGPivotUIUtils;
.super Ljava/lang/Object;
.source "EPGPivotUIUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static setupPivotArrowButtons(Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;Landroid/view/View;I)V
    .locals 5
    .param p0, "pivot"    # Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;
    .param p1, "arrowsParent"    # Landroid/view/View;
    .param p2, "currentPaneIndex"    # I

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 14
    const v2, 0x7f0e0b60

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 15
    .local v0, "pivotHeaderLeftArrow":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    const v2, 0x7f0e0b62

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/microsoft/xbox/toolkit/ui/XLEButton;

    .line 18
    .local v1, "pivotHeaderRightArrow":Lcom/microsoft/xbox/toolkit/ui/XLEButton;
    packed-switch p2, :pswitch_data_0

    .line 33
    :goto_0
    if-eqz v0, :cond_0

    .line 34
    new-instance v2, Lcom/microsoft/xbox/xle/epg/EPGPivotUIUtils$1;

    invoke-direct {v2, p0, p2}, Lcom/microsoft/xbox/xle/epg/EPGPivotUIUtils$1;-><init>(Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;I)V

    invoke-virtual {v0, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    :cond_0
    if-eqz v1, :cond_1

    .line 45
    new-instance v2, Lcom/microsoft/xbox/xle/epg/EPGPivotUIUtils$2;

    invoke-direct {v2, p0, p2}, Lcom/microsoft/xbox/xle/epg/EPGPivotUIUtils$2;-><init>(Lcom/microsoft/xbox/toolkit/ui/pivot/Pivot;I)V

    invoke-virtual {v1, v2}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    :cond_1
    return-void

    .line 20
    :pswitch_0
    invoke-virtual {v0, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 21
    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    goto :goto_0

    .line 24
    :pswitch_1
    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 25
    invoke-virtual {v1, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    goto :goto_0

    .line 28
    :pswitch_2
    invoke-virtual {v0, v3}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    .line 29
    invoke-virtual {v1, v4}, Lcom/microsoft/xbox/toolkit/ui/XLEButton;->setVisibility(I)V

    goto :goto_0

    .line 18
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
