.class final synthetic Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$7;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final arg$1:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;

.field private final arg$2:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;


# direct methods
.method private constructor <init>(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$7;->arg$1:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;

    iput-object p2, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$7;->arg$2:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;

    return-void
.end method

.method public static lambdaFactory$(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;)Lio/reactivex/functions/Function;
    .locals 1

    new-instance v0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$7;

    invoke-direct {v0, p0, p1}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$7;-><init>(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$7;->arg$1:Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;

    iget-object v1, p0, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter$$Lambda$7;->arg$2:Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;

    check-cast p1, Lio/reactivex/Observable;

    invoke-static {v0, v1, p1}, Lcom/microsoft/xbox/presentation/settings/language/LanguageSettingsPresenter;->lambda$null$5(Lcom/microsoft/xbox/domain/settings/language/LanguageSettingsInteractor;Lcom/microsoft/xbox/data/repository/settings/language/LanguageSettingsTelemetryService;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;

    move-result-object v0

    return-object v0
.end method
